/**
 * 
 */
package de.clusteval.data.distance;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

import de.clusteval.data.dataset.format.ConversionInputToStandardConfiguration;
import de.clusteval.data.dataset.format.IConversionInputToStandardConfiguration;
import de.clusteval.data.preprocessing.IDataPreprocessor;
import de.clusteval.utils.AbstractClustEvalTest;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.RNotAvailableException;
import dk.sdu.imada.compbio.utils.ArraysExt;
import dk.sdu.imada.compbio.utils.SimilarityMatrix.NUMBER_PRECISION;

/**
 * @author Christian Wiwie
 * 
 */
public class TestSpearmanCorrelationDistanceMeasure extends AbstractClustEvalTest {

	@Test
	public void test() throws UnknownDistanceMeasureException, RNotAvailableException, InterruptedException,
			DynamicComponentInitializationException {
		IDistanceMeasure measure = DistanceMeasure.parseFromString(getRepository(),
				"SpearmanCorrelationRDistanceMeasure");
		Assert.assertTrue(measure != null);

		IConversionInputToStandardConfiguration config = new ConversionInputToStandardConfiguration(measure,
				NUMBER_PRECISION.FLOAT, new ArrayList<IDataPreprocessor>(), new ArrayList<IDataPreprocessor>());

		double[][] matrix = new double[][]{new double[]{1, 2, 1}, new double[]{4, 5, 6}, new double[]{7, 8, 9},
				new double[]{7, 6, 5}};

		double[][] result = measure.getDistances(config, matrix).toArray();

		ArraysExt.print(result);

		Assert.assertArrayEquals(new double[][]{new double[]{0, 0.5, 0.5, 0.5}, new double[]{0.5, 0, 0, 1},
				new double[]{0.5, 0, 0, 1}, new double[]{0.5, 1, 1, 0}}, result);
	}
}
