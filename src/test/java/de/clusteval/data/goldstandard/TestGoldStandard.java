/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.data.goldstandard;

import java.io.File;
import java.io.FileNotFoundException;

import org.junit.Assert;
import org.junit.Test;

import de.clusteval.cluster.Cluster;
import de.clusteval.cluster.ClusterItem;
import de.clusteval.cluster.Clustering;
import de.clusteval.cluster.IClustering;
import de.clusteval.data.goldstandard.format.UnknownGoldStandardFormatException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.NoRepositoryFoundException;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RunResultRepository;
import de.clusteval.framework.repository.db.DatabaseConnectException;
import de.clusteval.framework.repository.db.StubSQLCommunicator;
import de.clusteval.framework.repository.parse.ParseException;
import de.clusteval.framework.repository.parse.Parser;
import de.clusteval.utils.AbstractClustEvalTest;

/**
 * @author Christian Wiwie
 * 
 */
public class TestGoldStandard extends AbstractClustEvalTest {

	/**
	 * @throws NoRepositoryFoundException
	 * @throws GoldStandardNotFoundException
	 * @throws RegisterException
	 * @throws ParseException
	 */
	@Test
	public void testParseFromFile() throws NoRepositoryFoundException,
			GoldStandardNotFoundException, RegisterException, ParseException {

		GoldStandard newObject = (GoldStandard) Parser.parseFromFile(
				IGoldStandard.class,
				new File(
						repository.getBasePath() + "/data/goldstandards/DS1/Zachary_karate_club_gold_standard.txt.v1")
								.getAbsoluteFile());
		Assert.assertEquals(newObject, new GoldStandard(getRepository(),
				new File(
						repository.getBasePath() + "/data/goldstandards/DS1/Zachary_karate_club_gold_standard.txt.v1")
								.lastModified(),
				new File(
						repository.getBasePath() + "/data/goldstandards/DS1/Zachary_karate_club_gold_standard.txt.v1")
								.getAbsoluteFile()));
	}

	/**
	 * @throws Throwable
	 */
	@Test(expected = FileNotFoundException.class)
	public void testParseFromNotExistingFile() throws Throwable {
		try {
			Parser.parseFromFile(IGoldStandard.class, new File(
					repository.getBasePath() + "/data/goldstandards/DS1/Zachary_karate_club_gold_standard2.txt.v1")
							.getAbsoluteFile());
		} catch (Exception e) {
			throw e.getCause();
		}
	}

	/**
	 * Registering a goldstandard of a runresult repository that is not present
	 * in the parent repository should not be possible.
	 * 
	 * @throws Throwable
	 * 
	 * @throws DatabaseConnectException
	 */
	@Test(expected = RegisterException.class)
	public void testRegisterRunResultRepositoryNotPresentInParent()
			throws Throwable {
		getRepository().initialize();
		try {
			try {
				IRepository runResultRepository = new RunResultRepository(
						new File(
								repository.getBasePath() + "/results/2016_08_12-14_05_42_tc_vs_DS1")
										.getAbsolutePath(),
						getRepository());
				runResultRepository.setSQLCommunicator(
						new StubSQLCommunicator(runResultRepository));
				runResultRepository.initialize();
				try {
					Parser.parseFromFile(IGoldStandard.class, new File(
							repository.getBasePath() + "/results/2016_08_12-14_05_42_tc_vs_DS1/goldstandards/DS1/testCaseGoldstandardNotPresentInParentRepository.txt.v1"));
				} finally {
					runResultRepository.terminateSupervisorThread();
				}
			} catch (DatabaseConnectException e) {
				// cannot happen
			}
		} catch (Exception e) {
			throw e.getCause();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see utils.TestRepositoryObject#testHashCode()
	 */
	public void testHashCode() throws NoRepositoryFoundException,
			GoldStandardNotFoundException, RegisterException, ParseException {
		this.repositoryObject = Parser.parseFromFile(IGoldStandard.class,
				new File(
						repository.getBasePath() + "/data/goldstandards/DS1/Zachary_karate_club_gold_standard.txt")
								.getAbsoluteFile());
		String absPath = new File(
				repository.getBasePath() + "/data/goldstandards/DS1/Zachary_karate_club_gold_standard.txt")
						.getAbsolutePath();
		Assert.assertEquals(
				(this.getRepository().toString() + absPath).hashCode(),
				this.repositoryObject.hashCode());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see utils.TestRepositoryObject#testGetRepository()
	 */
	public void testGetRepository() throws NoRepositoryFoundException,
			GoldStandardNotFoundException, RegisterException, ParseException {
		this.repositoryObject = Parser.parseFromFile(IGoldStandard.class,
				new File(
						repository.getBasePath() + "/data/goldstandards/DS1/Zachary_karate_club_gold_standard.txt")
								.getAbsoluteFile());
		Assert.assertEquals(this.getRepository(),
				this.repositoryObject.getRepository());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see utils.TestRepositoryObject#testRegister()
	 */
	public void testRegister() throws NoRepositoryFoundException,
			GoldStandardNotFoundException, RegisterException, ParseException {
		this.repositoryObject = Parser.parseFromFile(IGoldStandard.class,
				new File(
						repository.getBasePath() + "/data/goldstandards/DS1/Zachary_karate_club_gold_standard.txt")
								.getAbsoluteFile());
		Assert.assertEquals(this.repositoryObject, this.getRepository()
				.getRegisteredObject((GoldStandard) this.repositoryObject));

		// adding a gold standard equal to another one already registered does
		// not register the second object.
		this.repositoryObject = new GoldStandard(
				(GoldStandard) this.repositoryObject);
		Assert.assertEquals(
				this.getRepository().getRegisteredObject(
						(GoldStandard) this.repositoryObject),
				this.repositoryObject);
		Assert.assertFalse(this.getRepository().getRegisteredObject(
				(GoldStandard) this.repositoryObject) == this.repositoryObject);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see utils.TestRepositoryObject#testRegister()
	 */
	public void testUnregister() throws NoRepositoryFoundException,
			GoldStandardNotFoundException, RegisterException, ParseException {
		this.repositoryObject = Parser.parseFromFile(IGoldStandard.class,
				new File(
						repository.getBasePath() + "/data/goldstandards/DS1/Zachary_karate_club_gold_standard.txt")
								.getAbsoluteFile());
		Assert.assertEquals(this.repositoryObject, this.getRepository()
				.getRegisteredObject((GoldStandard) this.repositoryObject));
		this.repositoryObject.unregister();
		// is not registered anymore
		Assert.assertTrue(this.getRepository().getRegisteredObject(
				(GoldStandard) this.repositoryObject) == null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see utils.TestRepositoryObject#testEqualsObject()
	 */
	@Test
	public void testEqualsObject() throws RegisterException {
		File f = new File(
				repository.getBasePath() + "/data/goldstandards/DS1/Zachary_karate_club_gold_standard.txt")
						.getAbsoluteFile();
		this.repositoryObject = new GoldStandard(this.getRepository(),
				f.lastModified(), f);
		Assert.assertEquals(
				new GoldStandard(this.getRepository(), f.lastModified(), f),
				this.repositoryObject);

		File f2 = new File(
				repository.getBasePath() + "/data/goldstandards/sfld/sfld_brown_et_al_amidohydrolases_families_gold_standard.txt");
		Assert.assertFalse(this.repositoryObject.equals(
				new GoldStandard(this.getRepository(), f2.lastModified(), f2)));
	}

	/**
	 * @throws GoldStandardNotFoundException
	 * @throws NoRepositoryFoundException
	 * @throws UnknownGoldStandardFormatException
	 * @throws RegisterException
	 * @throws ParseException
	 * 
	 */
	@Test
	public void testLoadIntoMemory() throws NoRepositoryFoundException,
			GoldStandardNotFoundException, InvalidGoldStandardFormatException,
			RegisterException, ParseException {
		File f = new File(
				repository.getBasePath() + "/data/goldstandards/DS1/Zachary_karate_club_gold_standard.txt.v1")
						.getAbsoluteFile();
		this.repositoryObject = Parser.parseFromFile(IGoldStandard.class, f);
		boolean success = ((IGoldStandard) this.repositoryObject)
				.loadIntoMemory();
		Assert.assertTrue(success);

		IClustering clustering = ((IGoldStandard) this.repositoryObject)
				.getClustering();

		Assert.assertTrue(((IGoldStandard) this.repositoryObject).isInMemory());

		Assert.assertTrue(clustering != null);

		Clustering expected = new Clustering(this.getRepository(),
				System.currentTimeMillis(), new File(""));
		Cluster cluster1 = new Cluster("0");
		cluster1.add(new ClusterItem("0"), 1.0f);
		cluster1.add(new ClusterItem("1"), 1.0f);
		cluster1.add(new ClusterItem("2"), 1.0f);
		cluster1.add(new ClusterItem("3"), 1.0f);
		cluster1.add(new ClusterItem("4"), 1.0f);
		cluster1.add(new ClusterItem("5"), 1.0f);
		cluster1.add(new ClusterItem("6"), 1.0f);
		cluster1.add(new ClusterItem("7"), 1.0f);
		cluster1.add(new ClusterItem("10"), 1.0f);
		cluster1.add(new ClusterItem("11"), 1.0f);
		cluster1.add(new ClusterItem("12"), 1.0f);
		cluster1.add(new ClusterItem("13"), 1.0f);
		cluster1.add(new ClusterItem("16"), 1.0f);
		cluster1.add(new ClusterItem("17"), 1.0f);
		cluster1.add(new ClusterItem("19"), 1.0f);
		cluster1.add(new ClusterItem("21"), 1.0f);
		Cluster cluster2 = new Cluster("1");
		cluster2.add(new ClusterItem("8"), 1.0f);
		cluster2.add(new ClusterItem("9"), 1.0f);
		cluster2.add(new ClusterItem("14"), 1.0f);
		cluster2.add(new ClusterItem("15"), 1.0f);
		cluster2.add(new ClusterItem("18"), 1.0f);
		cluster2.add(new ClusterItem("20"), 1.0f);
		cluster2.add(new ClusterItem("22"), 1.0f);
		cluster2.add(new ClusterItem("23"), 1.0f);
		cluster2.add(new ClusterItem("24"), 1.0f);
		cluster2.add(new ClusterItem("25"), 1.0f);
		cluster2.add(new ClusterItem("26"), 1.0f);
		cluster2.add(new ClusterItem("27"), 1.0f);
		cluster2.add(new ClusterItem("28"), 1.0f);
		cluster2.add(new ClusterItem("29"), 1.0f);
		cluster2.add(new ClusterItem("30"), 1.0f);
		cluster2.add(new ClusterItem("31"), 1.0f);
		cluster2.add(new ClusterItem("32"), 1.0f);
		cluster2.add(new ClusterItem("33"), 1.0f);
		expected.addCluster(cluster1);
		expected.addCluster(cluster2);

		Assert.assertEquals(expected, clustering);
	}

	/**
	 * @throws GoldStandardNotFoundException
	 * @throws NoRepositoryFoundException
	 * @throws UnknownGoldStandardFormatException
	 * @throws RegisterException
	 * @throws ParseException
	 * 
	 */
	@Test
	public void testUnloadFromMemory() throws NoRepositoryFoundException,
			GoldStandardNotFoundException, InvalidGoldStandardFormatException,
			RegisterException, ParseException {
		File f = new File(
				repository.getBasePath() + "/data/goldstandards/DS1/Zachary_karate_club_gold_standard.txt.v1")
						.getAbsoluteFile();
		this.repositoryObject = Parser.parseFromFile(IGoldStandard.class, f);
		boolean success = ((IGoldStandard) this.repositoryObject)
				.loadIntoMemory();
		Assert.assertTrue(success);

		Assert.assertTrue(((IGoldStandard) this.repositoryObject).isInMemory());

		success = ((IGoldStandard) this.repositoryObject).unloadFromMemory();
		Assert.assertTrue(success);

		Assert.assertFalse(
				((IGoldStandard) this.repositoryObject).isInMemory());
	}

	/**
	 * @throws UnknownGoldStandardFormatException
	 * @throws GoldStandardNotFoundException
	 * @throws NoRepositoryFoundException
	 * @throws RegisterException
	 * @throws ParseException
	 * 
	 */
	@Test
	public void testIsInMemory() throws InvalidGoldStandardFormatException,
			NoRepositoryFoundException, GoldStandardNotFoundException,
			RegisterException, ParseException {
		File f = new File(
				repository.getBasePath() + "/data/goldstandards/DS1/Zachary_karate_club_gold_standard.txt.v1")
						.getAbsoluteFile();
		this.repositoryObject = Parser.parseFromFile(IGoldStandard.class, f);
		((IGoldStandard) this.repositoryObject).loadIntoMemory();

		Assert.assertTrue(((IGoldStandard) this.repositoryObject).isInMemory());

		((IGoldStandard) this.repositoryObject).unloadFromMemory();

		Assert.assertFalse(
				((IGoldStandard) this.repositoryObject).isInMemory());
	}

	/**
	 * @throws GoldStandardNotFoundException
	 * @throws NoRepositoryFoundException
	 * @throws UnknownGoldStandardFormatException
	 * @throws RegisterException
	 * @throws ParseException
	 * 
	 */
	@Test
	public void testSize() throws NoRepositoryFoundException,
			GoldStandardNotFoundException, InvalidGoldStandardFormatException,
			RegisterException, ParseException {
		File f = new File(
				repository.getBasePath() + "/data/goldstandards/DS1/Zachary_karate_club_gold_standard.txt.v1")
						.getAbsoluteFile();
		this.repositoryObject = Parser.parseFromFile(IGoldStandard.class, f);
		((IGoldStandard) this.repositoryObject).loadIntoMemory();

		IClustering clustering = ((IGoldStandard) this.repositoryObject)
				.getClustering();

		Assert.assertEquals(34, clustering.size());
	}

	/**
	 * @throws GoldStandardNotFoundException
	 * @throws NoRepositoryFoundException
	 * @throws UnknownGoldStandardFormatException
	 * @throws RegisterException
	 * @throws ParseException
	 * 
	 */
	@Test
	public void testFuzzySize() throws NoRepositoryFoundException,
			GoldStandardNotFoundException, InvalidGoldStandardFormatException,
			RegisterException, ParseException {
		File f = new File(
				repository.getBasePath() + "/data/goldstandards/DS1/Zachary_karate_club_gold_standard.txt.v1")
						.getAbsoluteFile();
		this.repositoryObject = Parser.parseFromFile(IGoldStandard.class, f);
		((IGoldStandard) this.repositoryObject).loadIntoMemory();

		IClustering clustering = ((IGoldStandard) this.repositoryObject)
				.getClustering();

		Assert.assertEquals(34f, clustering.fuzzySize(), 0.00001f);
	}

	/**
	 * Test method for {@link data.goldstandard.GoldStandard#getFullName()}.
	 * 
	 * @throws NoRepositoryFoundException
	 * @throws GoldStandardNotFoundException
	 * @throws RegisterException
	 * @throws ParseException
	 */
	@Test
	public void testGetFullName() throws NoRepositoryFoundException,
			GoldStandardNotFoundException, RegisterException, ParseException {
		this.repositoryObject = Parser.parseFromFile(IGoldStandard.class,
				new File(
						repository.getBasePath() + "/data/goldstandards/DS1/Zachary_karate_club_gold_standard.txt.v1")
								.getAbsoluteFile());
		Assert.assertEquals("DS1/Zachary_karate_club_gold_standard.txt",
				((IGoldStandard) this.repositoryObject).getFullName());
	}

	/**
	 * Test method for {@link data.goldstandard.GoldStandard#getMajorName()}.
	 * 
	 * @throws NoRepositoryFoundException
	 * 
	 * @throws RegisterException
	 * @throws GoldStandardNotFoundException
	 * @throws ParseException
	 */
	@Test
	public void testGetMajorName() throws NoRepositoryFoundException,
			RegisterException, GoldStandardNotFoundException, ParseException {
		this.repositoryObject = Parser.parseFromFile(IGoldStandard.class,
				new File(
						repository.getBasePath() + "/data/goldstandards/DS1/Zachary_karate_club_gold_standard.txt.v1")
								.getAbsoluteFile());
		Assert.assertEquals("DS1",
				((IGoldStandard) this.repositoryObject).getMajorName());
	}

	/**
	 * Test method for {@link data.goldstandard.GoldStandard#getMinorName()}.
	 * 
	 * @throws NoRepositoryFoundException
	 * @throws RegisterException
	 * @throws GoldStandardNotFoundException
	 * @throws ParseException
	 */
	@Test
	public void testGetMinorName() throws NoRepositoryFoundException,
			RegisterException, GoldStandardNotFoundException, ParseException {
		this.repositoryObject = Parser.parseFromFile(IGoldStandard.class,
				new File(
						repository.getBasePath() + "/data/goldstandards/DS1/Zachary_karate_club_gold_standard.txt.v1")
								.getAbsoluteFile());
		GoldStandard casted = ((GoldStandard) this.repositoryObject);
		Assert.assertEquals("Zachary_karate_club_gold_standard.txt",
				casted.getMinorName());
	}
}
