/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.data.dataset;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;

import org.apache.commons.configuration.ConfigurationException;
import org.junit.Test;

import de.clusteval.cluster.paramOptimization.IncompatibleParameterOptimizationMethodException;
import de.clusteval.cluster.paramOptimization.InvalidOptimizationParameterException;
import de.clusteval.cluster.paramOptimization.UnknownParameterOptimizationMethodException;
import de.clusteval.cluster.quality.UnknownClusteringQualityMeasureException;
import de.clusteval.context.IncompatibleContextException;
import de.clusteval.context.UnknownContextException;
import de.clusteval.data.DataConfigNotFoundException;
import de.clusteval.data.DataConfigurationException;
import de.clusteval.data.dataset.format.ConversionInputToStandardConfiguration;
import de.clusteval.data.dataset.format.ConversionStandardToInputConfiguration;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.dataset.type.UnknownDataSetTypeException;
import de.clusteval.data.distance.DistanceMeasure;
import de.clusteval.data.distance.UnknownDistanceMeasureException;
import de.clusteval.data.goldstandard.GoldStandardConfigNotFoundException;
import de.clusteval.data.goldstandard.GoldStandardConfigurationException;
import de.clusteval.data.goldstandard.GoldStandardNotFoundException;
import de.clusteval.data.preprocessing.IDataPreprocessor;
import de.clusteval.data.preprocessing.InvalidDataPreprocessorOptionsException;
import de.clusteval.data.preprocessing.UnknownDataPreprocessorException;
import de.clusteval.data.randomizer.UnknownDataRandomizerException;
import de.clusteval.data.statistics.UnknownDataStatisticException;
import de.clusteval.framework.repository.NoRepositoryFoundException;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryRemoveEvent;
import de.clusteval.framework.repository.RepositoryReplaceEvent;
import de.clusteval.framework.repository.parse.ParseException;
import de.clusteval.framework.repository.parse.Parser;
import de.clusteval.program.NoOptimizableProgramParameterException;
import de.clusteval.program.UnknownParameterType;
import de.clusteval.program.UnknownProgramParameterException;
import de.clusteval.program.UnknownProgramTypeException;
import de.clusteval.program.r.UnknownRProgramException;
import de.clusteval.run.RunException;
import de.clusteval.run.runresult.format.UnknownRunResultFormatException;
import de.clusteval.run.runresult.postprocessing.UnknownRunResultPostprocessorException;
import de.clusteval.run.statistics.UnknownRunDataStatisticException;
import de.clusteval.run.statistics.UnknownRunStatisticException;
import de.clusteval.utils.AbstractClustEvalTest;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.StubRepositoryObject;
import dk.sdu.imada.compbio.utils.SimilarityMatrix.NUMBER_PRECISION;
import junit.framework.Assert;

/**
 * @author Christian Wiwie
 * 
 */
public class TestDataSetConfig extends AbstractClustEvalTest {

	/**
	 * 
	 */
	public TestDataSetConfig() {
		super(false, true);
	}

	/**
	 * Test method for {@link data.dataset.DataSetConfig#register()}.
	 * 
	 * @throws DataSetNotFoundException
	 * @throws NoRepositoryFoundException
	 * @throws DataSetConfigurationException
	 * @throws UnknownDataSetFormatException
	 * @throws DataSetConfigNotFoundException
	 * @throws UnknownDistanceMeasureException
	 * @throws RegisterException
	 * @throws UnknownDataPreprocessorException
	 * @throws NumberFormatException
	 * @throws IncompatibleDataSetConfigPreprocessorException
	 * @throws UnknownRunDataStatisticException
	 *             , UnknownRunResultPostprocessorException
	 * @throws UnknownRunStatisticException
	 * @throws UnknownDataStatisticException
	 * @throws NoOptimizableProgramParameterException
	 * @throws UnknownParameterOptimizationMethodException
	 * @throws IncompatibleParameterOptimizationMethodException
	 * @throws UnknownRProgramException
	 * @throws UnknownProgramTypeException
	 * @throws UnknownProgramParameterException
	 * @throws InvalidOptimizationParameterException
	 * @throws UnknownRunResultFormatException
	 * @throws IncompatibleContextException
	 * @throws RunException
	 * @throws UnknownClusteringQualityMeasureException
	 * @throws UnknownParameterType
	 * @throws FileNotFoundException
	 * @throws UnknownContextException
	 * @throws ConfigurationException
	 * @throws DataConfigNotFoundException
	 * @throws DataConfigurationException
	 * @throws GoldStandardConfigNotFoundException
	 * @throws GoldStandardConfigurationException
	 * @throws GoldStandardNotFoundException
	 * @throws ParseException
	 */
	public void testRegister()
			throws DataSetConfigurationException, NoRepositoryFoundException,
			DataSetNotFoundException, UnknownDataSetFormatException,
			DataSetConfigNotFoundException, UnknownDistanceMeasureException,
			RegisterException, UnknownDataSetTypeException, NoDataSetException,
			NumberFormatException, UnknownDataPreprocessorException,
			IncompatibleDataSetConfigPreprocessorException,
			GoldStandardNotFoundException, GoldStandardConfigurationException,
			GoldStandardConfigNotFoundException, DataConfigurationException,
			DataConfigNotFoundException, ConfigurationException,
			UnknownContextException, FileNotFoundException,
			UnknownParameterType, UnknownClusteringQualityMeasureException,
			RunException, IncompatibleContextException,
			UnknownRunResultFormatException,
			InvalidOptimizationParameterException,
			UnknownProgramParameterException, UnknownProgramTypeException,
			UnknownRProgramException,
			IncompatibleParameterOptimizationMethodException,
			UnknownParameterOptimizationMethodException,
			NoOptimizableProgramParameterException,
			UnknownDataStatisticException, UnknownRunStatisticException,
			UnknownRunDataStatisticException,
			UnknownRunResultPostprocessorException,
			UnknownDataRandomizerException,
			InvalidDataPreprocessorOptionsException, ParseException {
		this.repositoryObject = Parser.parseFromFile(IDataSetConfig.class,
				new File(repository.getBasePath()
						+ "/data/datasets/configs/astral_1.dsconfig")
								.getAbsoluteFile());
		Assert.assertEquals(this.repositoryObject, this.getRepository()
				.getRegisteredObject((DataSetConfig) this.repositoryObject));

		// adding a DataSetConfig equal to another one already registered
		// does
		// not register the second object.
		this.repositoryObject = new DataSetConfig(
				(DataSetConfig) this.repositoryObject);
		Assert.assertEquals(
				this.getRepository().getRegisteredObject(
						(DataSetConfig) this.repositoryObject),
				this.repositoryObject);
		Assert.assertFalse(this.getRepository().getRegisteredObject(
				(DataSetConfig) this.repositoryObject) == this.repositoryObject);
	}

	/**
	 * Test method for {@link data.dataset.DataSetConfig#unregister()} .
	 * 
	 * @throws DataSetNotFoundException
	 * @throws NoRepositoryFoundException
	 * @throws DataSetConfigurationException
	 * @throws UnknownDataSetFormatException
	 * @throws UnknownDistanceMeasureException
	 * @throws RegisterException
	 * @throws UnknownDataPreprocessorException
	 * @throws NumberFormatException
	 * @throws IncompatibleDataSetConfigPreprocessorException
	 * @throws UnknownRunDataStatisticException
	 *             , UnknownRunResultPostprocessorException
	 * @throws UnknownRunStatisticException
	 * @throws UnknownDataStatisticException
	 * @throws NoOptimizableProgramParameterException
	 * @throws UnknownParameterOptimizationMethodException
	 * @throws IncompatibleParameterOptimizationMethodException
	 * @throws UnknownRProgramException
	 * @throws UnknownProgramTypeException
	 * @throws UnknownProgramParameterException
	 * @throws InvalidOptimizationParameterException
	 * @throws UnknownRunResultFormatException
	 * @throws IncompatibleContextException
	 * @throws RunException
	 * @throws UnknownClusteringQualityMeasureException
	 * @throws UnknownParameterType
	 * @throws FileNotFoundException
	 * @throws UnknownContextException
	 * @throws ConfigurationException
	 * @throws DataConfigNotFoundException
	 * @throws DataConfigurationException
	 * @throws GoldStandardConfigNotFoundException
	 * @throws GoldStandardConfigurationException
	 * @throws GoldStandardNotFoundException
	 * @throws ParseException
	 */
	public void testUnregister()
			throws DataSetConfigurationException, NoRepositoryFoundException,
			DataSetNotFoundException, UnknownDataSetFormatException,
			DataSetConfigNotFoundException, UnknownDistanceMeasureException,
			RegisterException, UnknownDataSetTypeException, NoDataSetException,
			NumberFormatException, UnknownDataPreprocessorException,
			IncompatibleDataSetConfigPreprocessorException,
			GoldStandardNotFoundException, GoldStandardConfigurationException,
			GoldStandardConfigNotFoundException, DataConfigurationException,
			DataConfigNotFoundException, ConfigurationException,
			UnknownContextException, FileNotFoundException,
			UnknownParameterType, UnknownClusteringQualityMeasureException,
			RunException, IncompatibleContextException,
			UnknownRunResultFormatException,
			InvalidOptimizationParameterException,
			UnknownProgramParameterException, UnknownProgramTypeException,
			UnknownRProgramException,
			IncompatibleParameterOptimizationMethodException,
			UnknownParameterOptimizationMethodException,
			NoOptimizableProgramParameterException,
			UnknownDataStatisticException, UnknownRunStatisticException,
			UnknownRunDataStatisticException,
			UnknownRunResultPostprocessorException,
			UnknownDataRandomizerException,
			InvalidDataPreprocessorOptionsException, ParseException {
		this.repositoryObject = Parser.parseFromFile(IDataSetConfig.class,
				new File(repository.getBasePath()
						+ "/data/datasets/configs/astral_1.dsconfig")
								.getAbsoluteFile());
		Assert.assertEquals(this.repositoryObject, this.getRepository()
				.getRegisteredObject((DataSetConfig) this.repositoryObject));
		this.repositoryObject.unregister();
		// is not registered anymore
		Assert.assertTrue(this.getRepository().getRegisteredObject(
				(DataSetConfig) this.repositoryObject) == null);
	}

	/**
	 * Test method for
	 * {@link data.dataset.DataSetConfig#notify(utils.RepositoryEvent)} .
	 * 
	 * @throws DataSetNotFoundException
	 * @throws NoRepositoryFoundException
	 * @throws DataSetConfigurationException
	 * @throws UnknownDataSetFormatException
	 * @throws DataSetConfigNotFoundException
	 * @throws UnknownDistanceMeasureException
	 * @throws RegisterException
	 * @throws UnknownDataPreprocessorException
	 * @throws NumberFormatException
	 * @throws IncompatibleDataSetConfigPreprocessorException
	 * @throws UnknownRunDataStatisticException
	 *             , UnknownRunResultPostprocessorException
	 * @throws UnknownRunStatisticException
	 * @throws UnknownDataStatisticException
	 * @throws NoOptimizableProgramParameterException
	 * @throws UnknownParameterOptimizationMethodException
	 * @throws IncompatibleParameterOptimizationMethodException
	 * @throws UnknownRProgramException
	 * @throws UnknownProgramTypeException
	 * @throws UnknownProgramParameterException
	 * @throws InvalidOptimizationParameterException
	 * @throws UnknownRunResultFormatException
	 * @throws IncompatibleContextException
	 * @throws RunException
	 * @throws UnknownClusteringQualityMeasureException
	 * @throws UnknownParameterType
	 * @throws FileNotFoundException
	 * @throws UnknownContextException
	 * @throws ConfigurationException
	 * @throws DataConfigNotFoundException
	 * @throws DataConfigurationException
	 * @throws GoldStandardConfigNotFoundException
	 * @throws GoldStandardConfigurationException
	 * @throws GoldStandardNotFoundException
	 * @throws ParseException
	 */
	@Test
	public void testNotifyRepositoryEvent()
			throws NoRepositoryFoundException, DataSetNotFoundException,
			DataSetConfigurationException, UnknownDataSetFormatException,
			DataSetConfigNotFoundException, UnknownDistanceMeasureException,
			RegisterException, UnknownDataSetTypeException, NoDataSetException,
			NumberFormatException, UnknownDataPreprocessorException,
			IncompatibleDataSetConfigPreprocessorException,
			GoldStandardNotFoundException, GoldStandardConfigurationException,
			GoldStandardConfigNotFoundException, DataConfigurationException,
			DataConfigNotFoundException, ConfigurationException,
			UnknownContextException, FileNotFoundException,
			UnknownParameterType, UnknownClusteringQualityMeasureException,
			RunException, IncompatibleContextException,
			UnknownRunResultFormatException,
			InvalidOptimizationParameterException,
			UnknownProgramParameterException, UnknownProgramTypeException,
			UnknownRProgramException,
			IncompatibleParameterOptimizationMethodException,
			UnknownParameterOptimizationMethodException,
			NoOptimizableProgramParameterException,
			UnknownDataStatisticException, UnknownRunStatisticException,
			UnknownRunDataStatisticException,
			UnknownRunResultPostprocessorException,
			UnknownDataRandomizerException,
			InvalidDataPreprocessorOptionsException, ParseException {

		/*
		 * REPLACE
		 */

		/*
		 * First check, whether listeners of DataSetconfigs are notified
		 * correctly when the DataSetconfig is replaced
		 */
		IDataSetConfig gsConfig = Parser.parseFromFile(IDataSetConfig.class,
				new File(repository.getBasePath()
						+ "/data/datasets/configs/astral_1.v1.dsconfig")
								.getAbsoluteFile());
		StubRepositoryObject child = new StubRepositoryObject(getRepository(),
				System.currentTimeMillis(),
				new File(repository.getBasePath() + "/Bla"));
		gsConfig.addListener(child);

		DataSetConfig gsConfig2 = new DataSetConfig((DataSetConfig) gsConfig);

		gsConfig.notify(new RepositoryReplaceEvent(gsConfig, gsConfig2));
		Assert.assertTrue(child.notified);

		/*
		 * Now check, whether DataSet configs update their references correctly,
		 * when their DataSet is replaced
		 */
		RelativeDataSet gs = (RelativeDataSet) (gsConfig.getDataSet());
		IDataSet gs2 = new RelativeDataSet(gs);

		gsConfig.notify(new RepositoryReplaceEvent(gs, gs2));

		Assert.assertFalse(gsConfig.getDataSet() == gs);
		Assert.assertTrue(gsConfig.getDataSet() == gs2);

		/*
		 * REMOVE
		 */

		/*
		 * First check, whether listeners of DataSetconfigs are notified
		 * correctly when the DataSetconfig is replaced
		 */
		child.notified = false;
		gsConfig.notify(new RepositoryRemoveEvent(gsConfig));
		Assert.assertTrue(child.notified);

		/*
		 * Now check, whether DataSet configs remove themselves when their
		 * DataSet is removed
		 */
		// gsconfig has to be registered
		Assert.assertTrue(
				getRepository().getRegisteredObject(gsConfig) == gsConfig);

		gsConfig.notify(new RepositoryRemoveEvent(gs2));

		// not registered anymore
		Assert.assertTrue(
				getRepository().getRegisteredObject(gsConfig) == null);
	}

	/**
	 * Test method for
	 * {@link data.dataset.DataSetConfig#parseFromFile(java.io.File)}.
	 * 
	 * @throws Throwable
	 */
	@Test(expected = DataSetConfigurationException.class)
	public void testParseFromFileDataSetNameMissing() throws Throwable {
		// create empty file
		File f = new File(repository.getBasePath()
				+ "/data/datasets/configs/testDataSetConfig.v1.dsconfig")
						.getAbsoluteFile();
		f.createNewFile();
		try {
			Parser.parseFromFile(IDataSetConfig.class, f);
		} catch (Exception e) {
			throw e.getCause();
		} finally {
			f.delete();
		}
	}

	/**
	 * Test method for
	 * {@link data.dataset.DataSetConfig#parseFromFile(java.io.File)}.
	 * 
	 * @throws DataSetNotFoundException
	 * @throws NoRepositoryFoundException
	 * @throws DataSetConfigurationException
	 * @throws UnknownDataSetFormatException
	 * @throws DataSetConfigNotFoundException
	 * @throws UnknownDistanceMeasureException
	 * @throws RegisterException
	 * @throws UnknownDataPreprocessorException
	 * @throws NumberFormatException
	 * @throws IncompatibleDataSetConfigPreprocessorException
	 * @throws UnknownRunDataStatisticException
	 *             , UnknownRunResultPostprocessorException
	 * @throws UnknownRunStatisticException
	 * @throws UnknownDataStatisticException
	 * @throws NoOptimizableProgramParameterException
	 * @throws UnknownParameterOptimizationMethodException
	 * @throws IncompatibleParameterOptimizationMethodException
	 * @throws UnknownRProgramException
	 * @throws UnknownProgramTypeException
	 * @throws UnknownProgramParameterException
	 * @throws InvalidOptimizationParameterException
	 * @throws UnknownRunResultFormatException
	 * @throws IncompatibleContextException
	 * @throws RunException
	 * @throws UnknownClusteringQualityMeasureException
	 * @throws UnknownParameterType
	 * @throws FileNotFoundException
	 * @throws UnknownContextException
	 * @throws ConfigurationException
	 * @throws DataConfigNotFoundException
	 * @throws DataConfigurationException
	 * @throws GoldStandardConfigNotFoundException
	 * @throws GoldStandardConfigurationException
	 * @throws GoldStandardNotFoundException
	 * @throws ParseException
	 */
	@Test
	public void testParseFromFile()
			throws DataSetConfigurationException, NoRepositoryFoundException,
			DataSetNotFoundException, UnknownDataSetFormatException,
			DataSetConfigNotFoundException, UnknownDistanceMeasureException,
			RegisterException, UnknownDataSetTypeException, NoDataSetException,
			NumberFormatException, UnknownDataPreprocessorException,
			IncompatibleDataSetConfigPreprocessorException,
			GoldStandardNotFoundException, GoldStandardConfigurationException,
			GoldStandardConfigNotFoundException, DataConfigurationException,
			DataConfigNotFoundException, ConfigurationException,
			UnknownContextException, FileNotFoundException,
			UnknownParameterType, UnknownClusteringQualityMeasureException,
			RunException, IncompatibleContextException,
			UnknownRunResultFormatException,
			InvalidOptimizationParameterException,
			UnknownProgramParameterException, UnknownProgramTypeException,
			UnknownRProgramException,
			IncompatibleParameterOptimizationMethodException,
			UnknownParameterOptimizationMethodException,
			NoOptimizableProgramParameterException,
			UnknownDataStatisticException, UnknownRunStatisticException,
			UnknownRunDataStatisticException,
			UnknownRunResultPostprocessorException,
			UnknownDataRandomizerException,
			InvalidDataPreprocessorOptionsException, ParseException,
			DynamicComponentInitializationException {
		IDataSetConfig gsConfig = Parser.parseFromFile(IDataSetConfig.class,
				new File(repository.getBasePath()
						+ "/data/datasets/configs/astral_1.v1.dsconfig")
								.getAbsoluteFile());
		Assert.assertEquals(
				new DataSetConfig(getRepository(),
						new File(repository.getBasePath()
								+ "/data/datasets/configs/astral_1.v1.dsconfig")
										.getAbsoluteFile().lastModified(),
						new File(repository
								.getBasePath()
								+ "/data/datasets/configs/astral_1.v1.dsconfig")
										.getAbsoluteFile(),
						Parser.parseFromFile(IDataSet.class, new File(repository
								.getBasePath()
								+ "/data/datasets/astral_1_161/blastResults.txt.v1")
										.getAbsoluteFile()),
						new ConversionInputToStandardConfiguration(
								DistanceMeasure.parseFromString(getRepository(),
										"EuclidianDistanceMeasure"),
								NUMBER_PRECISION.DOUBLE,
								new ArrayList<IDataPreprocessor>(),
								new ArrayList<IDataPreprocessor>()),
						new ConversionStandardToInputConfiguration()),
				gsConfig);
	}

	/**
	 * Test method for
	 * {@link data.dataset.DataSetConfig#parseFromFile(java.io.File)}.
	 * 
	 * @throws Throwable
	 */
	@Test(expected = DataSetConfigurationException.class)
	public void testParseFromFileDataSetFileMissing() throws Throwable {

		File f = new File(repository.getBasePath()
				+ "/data/datasets/configs/testDataSetConfig2.v1.dsconfig")
						.getAbsoluteFile();
		f.createNewFile();

		try {
			try {
				PrintWriter bw = new PrintWriter(new FileWriter(f));
				bw.println("datasetName = Test");
				bw.flush();
				bw.close();

				Parser.parseFromFile(IDataSetConfig.class, f);
			} finally {
				f.delete();
			}
		} catch (Exception e) {
			throw e.getCause();
		}
	}

	/**
	 * @throws Throwable
	 */
	@Test(expected = FileNotFoundException.class)
	public void testParseFromNotExistingFile() throws Throwable {
		try {
			Parser.parseFromFile(IDataSetConfig.class,
					new File(repository.getBasePath()
							+ "/data/datasets/configs/DS1_12.v1.gsconfig")
									.getAbsoluteFile());
		} catch (Exception e) {
			throw e.getCause();
		}
	}

	/**
	 * Test method for {@link data.dataset.DataSetConfig#parseDataSet()}.
	 * 
	 * @throws DataSetNotFoundException
	 * @throws NoRepositoryFoundException
	 * @throws DataSetConfigurationException
	 * @throws UnknownDataSetFormatException
	 * @throws DataSetConfigNotFoundException
	 * @throws UnknownDistanceMeasureException
	 * @throws RegisterException
	 * @throws UnknownDataPreprocessorException
	 * @throws NumberFormatException
	 * @throws IncompatibleDataSetConfigPreprocessorException
	 * @throws UnknownRunDataStatisticException
	 *             , UnknownRunResultPostprocessorException
	 * @throws UnknownRunStatisticException
	 * @throws UnknownDataStatisticException
	 * @throws NoOptimizableProgramParameterException
	 * @throws UnknownParameterOptimizationMethodException
	 * @throws IncompatibleParameterOptimizationMethodException
	 * @throws UnknownRProgramException
	 * @throws UnknownProgramTypeException
	 * @throws UnknownProgramParameterException
	 * @throws InvalidOptimizationParameterException
	 * @throws UnknownRunResultFormatException
	 * @throws IncompatibleContextException
	 * @throws RunException
	 * @throws UnknownClusteringQualityMeasureException
	 * @throws UnknownParameterType
	 * @throws FileNotFoundException
	 * @throws UnknownContextException
	 * @throws ConfigurationException
	 * @throws DataConfigNotFoundException
	 * @throws DataConfigurationException
	 * @throws GoldStandardConfigNotFoundException
	 * @throws GoldStandardConfigurationException
	 * @throws GoldStandardNotFoundException
	 * @throws ParseException
	 */
	@Test
	public void testGetDataSet()
			throws DataSetConfigurationException, NoRepositoryFoundException,
			DataSetNotFoundException, UnknownDataSetFormatException,
			DataSetConfigNotFoundException, UnknownDistanceMeasureException,
			RegisterException, UnknownDataSetTypeException, NoDataSetException,
			NumberFormatException, UnknownDataPreprocessorException,
			IncompatibleDataSetConfigPreprocessorException,
			GoldStandardNotFoundException, GoldStandardConfigurationException,
			GoldStandardConfigNotFoundException, DataConfigurationException,
			DataConfigNotFoundException, ConfigurationException,
			UnknownContextException, FileNotFoundException,
			UnknownParameterType, UnknownClusteringQualityMeasureException,
			RunException, IncompatibleContextException,
			UnknownRunResultFormatException,
			InvalidOptimizationParameterException,
			UnknownProgramParameterException, UnknownProgramTypeException,
			UnknownRProgramException,
			IncompatibleParameterOptimizationMethodException,
			UnknownParameterOptimizationMethodException,
			NoOptimizableProgramParameterException,
			UnknownDataStatisticException, UnknownRunStatisticException,
			UnknownRunDataStatisticException,
			UnknownRunResultPostprocessorException,
			UnknownDataRandomizerException,
			InvalidDataPreprocessorOptionsException, ParseException {
		IDataSetConfig dsConfig = Parser.parseFromFile(IDataSetConfig.class,
				new File(repository.getBasePath()
						+ "/data/datasets/configs/astral_1.v1.dsconfig")
								.getAbsoluteFile());
		IDataSet ds = dsConfig.getDataSet();
		IDataSet expected = Parser.parseFromFile(IDataSet.class,
				new File(repository.getBasePath()
						+ "/data/datasets/astral_1_161/blastResults.txt.v1")
								.getAbsoluteFile());
		Assert.assertEquals(expected, ds);
	}

	/**
	 * Test method for
	 * {@link data.dataset.DataSetConfig#setDataSet(data.dataset.DataSet)} .
	 * 
	 * @throws DataSetNotFoundException
	 * @throws NoRepositoryFoundException
	 * @throws DataSetConfigurationException
	 * @throws UnknownDataSetFormatException
	 * @throws DataSetConfigNotFoundException
	 * @throws UnknownDistanceMeasureException
	 * @throws RegisterException
	 * @throws UnknownDataSetTypeException
	 * @throws UnknownDataPreprocessorException
	 * @throws NumberFormatException
	 * @throws IncompatibleDataSetConfigPreprocessorException
	 * @throws UnknownRunDataStatisticException
	 *             , UnknownRunResultPostprocessorException
	 * @throws UnknownRunStatisticException
	 * @throws UnknownDataStatisticException
	 * @throws NoOptimizableProgramParameterException
	 * @throws UnknownParameterOptimizationMethodException
	 * @throws IncompatibleParameterOptimizationMethodException
	 * @throws UnknownRProgramException
	 * @throws UnknownProgramTypeException
	 * @throws UnknownProgramParameterException
	 * @throws InvalidOptimizationParameterException
	 * @throws UnknownRunResultFormatException
	 * @throws IncompatibleContextException
	 * @throws RunException
	 * @throws UnknownClusteringQualityMeasureException
	 * @throws UnknownParameterType
	 * @throws FileNotFoundException
	 * @throws UnknownContextException
	 * @throws ConfigurationException
	 * @throws DataConfigNotFoundException
	 * @throws DataConfigurationException
	 * @throws GoldStandardConfigNotFoundException
	 * @throws GoldStandardConfigurationException
	 * @throws GoldStandardNotFoundException
	 * @throws ParseException
	 */
	@Test
	public void testSetDataSet()
			throws DataSetConfigurationException, NoRepositoryFoundException,
			DataSetNotFoundException, UnknownDataSetFormatException,
			DataSetConfigNotFoundException, UnknownDistanceMeasureException,
			RegisterException, UnknownDataSetTypeException, NoDataSetException,
			NumberFormatException, UnknownDataPreprocessorException,
			IncompatibleDataSetConfigPreprocessorException,
			GoldStandardNotFoundException, GoldStandardConfigurationException,
			GoldStandardConfigNotFoundException, DataConfigurationException,
			DataConfigNotFoundException, ConfigurationException,
			UnknownContextException, FileNotFoundException,
			UnknownParameterType, UnknownClusteringQualityMeasureException,
			RunException, IncompatibleContextException,
			UnknownRunResultFormatException,
			InvalidOptimizationParameterException,
			UnknownProgramParameterException, UnknownProgramTypeException,
			UnknownRProgramException,
			IncompatibleParameterOptimizationMethodException,
			UnknownParameterOptimizationMethodException,
			NoOptimizableProgramParameterException,
			UnknownDataStatisticException, UnknownRunStatisticException,
			UnknownRunDataStatisticException,
			UnknownRunResultPostprocessorException,
			UnknownDataRandomizerException,
			InvalidDataPreprocessorOptionsException, ParseException {
		IDataSetConfig dsConfig = Parser.parseFromFile(IDataSetConfig.class,
				new File(repository.getBasePath()
						+ "/data/datasets/configs/astral_1.v1.dsconfig")
								.getAbsoluteFile());
		IDataSet ds = dsConfig.getDataSet();
		IDataSet expected = Parser.parseFromFile(IDataSet.class,
				new File(repository.getBasePath()
						+ "/data/datasets/astral_1_161/blastResults.txt.v1")
								.getAbsoluteFile());
		Assert.assertEquals(expected, ds);

		IDataSet override = Parser.parseFromFile(IDataSet.class,
				new File(repository.getBasePath()
						+ "/data/datasets/DS1/Zachary_karate_club_similarities.txt.v1")
								.getAbsoluteFile());
		dsConfig.setDataSet(override);
		Assert.assertEquals(override, dsConfig.getDataSet());
	}

	/**
	 * Test method for {@link data.dataset.DataSetConfig#toString()}.
	 * 
	 * @throws DataSetNotFoundException
	 * @throws NoRepositoryFoundException
	 * @throws DataSetConfigurationException
	 * @throws UnknownDataSetFormatException
	 * @throws DataSetConfigNotFoundException
	 * @throws UnknownDistanceMeasureException
	 * @throws RegisterException
	 * @throws UnknownDataPreprocessorException
	 * @throws NumberFormatException
	 * @throws IncompatibleDataSetConfigPreprocessorException
	 * @throws UnknownRunDataStatisticException
	 *             , UnknownRunResultPostprocessorException
	 * @throws UnknownRunStatisticException
	 * @throws UnknownDataStatisticException
	 * @throws NoOptimizableProgramParameterException
	 * @throws UnknownParameterOptimizationMethodException
	 * @throws IncompatibleParameterOptimizationMethodException
	 * @throws UnknownRProgramException
	 * @throws UnknownProgramTypeException
	 * @throws UnknownProgramParameterException
	 * @throws InvalidOptimizationParameterException
	 * @throws UnknownRunResultFormatException
	 * @throws IncompatibleContextException
	 * @throws RunException
	 * @throws UnknownClusteringQualityMeasureException
	 * @throws UnknownParameterType
	 * @throws FileNotFoundException
	 * @throws UnknownContextException
	 * @throws ConfigurationException
	 * @throws DataConfigNotFoundException
	 * @throws DataConfigurationException
	 * @throws GoldStandardConfigNotFoundException
	 * @throws GoldStandardConfigurationException
	 * @throws GoldStandardNotFoundException
	 * @throws ParseException
	 */
	@Test
	public void testToString()
			throws DataSetConfigurationException, NoRepositoryFoundException,
			DataSetNotFoundException, UnknownDataSetFormatException,
			DataSetConfigNotFoundException, UnknownDistanceMeasureException,
			RegisterException, UnknownDataSetTypeException, NoDataSetException,
			NumberFormatException, UnknownDataPreprocessorException,
			IncompatibleDataSetConfigPreprocessorException,
			GoldStandardNotFoundException, GoldStandardConfigurationException,
			GoldStandardConfigNotFoundException, DataConfigurationException,
			DataConfigNotFoundException, ConfigurationException,
			UnknownContextException, FileNotFoundException,
			UnknownParameterType, UnknownClusteringQualityMeasureException,
			RunException, IncompatibleContextException,
			UnknownRunResultFormatException,
			InvalidOptimizationParameterException,
			UnknownProgramParameterException, UnknownProgramTypeException,
			UnknownRProgramException,
			IncompatibleParameterOptimizationMethodException,
			UnknownParameterOptimizationMethodException,
			NoOptimizableProgramParameterException,
			UnknownDataStatisticException, UnknownRunStatisticException,
			UnknownRunDataStatisticException,
			UnknownRunResultPostprocessorException,
			UnknownDataRandomizerException,
			InvalidDataPreprocessorOptionsException, ParseException {
		IDataSetConfig gsConfig = Parser.parseFromFile(IDataSetConfig.class,
				new File(repository.getBasePath()
						+ "/data/datasets/configs/astral_1.v1.dsconfig")
								.getAbsoluteFile());
		Assert.assertEquals("astral_1:1", gsConfig.toString());

	}

}
