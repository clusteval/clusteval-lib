/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.program.r;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Test;

import de.clusteval.data.dataset.format.IncompatibleDataSetFormatException;
import de.clusteval.data.randomizer.UnknownDataRandomizerException;
import de.clusteval.framework.repository.InvalidRepositoryException;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.framework.repository.RepositoryAlreadyExistsException;
import de.clusteval.framework.repository.RepositoryCouldNotBeMigratedException;
import de.clusteval.framework.repository.RepositoryVersionTooNewException;
import de.clusteval.framework.repository.RepositoryVersionTooOldException;
import de.clusteval.framework.repository.config.RepositoryConfigNotFoundException;
import de.clusteval.framework.repository.config.RepositoryConfigurationException;
import de.clusteval.framework.repository.db.DatabaseConnectException;
import de.clusteval.framework.repository.maven.MavenRepositoryDuplicateException;
import de.clusteval.framework.threading.IRunSchedulerThread;
import de.clusteval.run.IRun;
import de.clusteval.run.RunInitializationException;
import de.clusteval.run.runnable.IRunRunnable;
import de.clusteval.run.runnable.RunRunnableInitializationException;
import de.clusteval.run.runresult.IRunResult;
import de.clusteval.utils.AbstractClustEvalTest;
import dk.sdu.imada.compbio.file.FileUtils;
import dk.sdu.imada.compbio.format.Formatter;

/**
 * @author Christian Wiwie
 * 
 */
public class TestKMeansClusteringRProgram extends AbstractClustEvalTest {

	/**
	 * 
	 */
	public TestKMeansClusteringRProgram() {
		super(false, true);
	}

	/**
	 * @throws RepositoryAlreadyExistsException
	 * @throws InvalidRepositoryException
	 * @throws RepositoryConfigNotFoundException
	 * @throws RepositoryConfigurationException
	 * @throws IOException
	 * @throws RunRunnableInitializationException
	 * @throws RunInitializationException
	 */
	@Test
	public void testApplyToRelativeDataSet()
			throws RepositoryAlreadyExistsException, InvalidRepositoryException,
			RepositoryConfigNotFoundException, RepositoryConfigurationException,
			IOException, RunRunnableInitializationException,
			InterruptedException, UnknownDataRandomizerException,
			RunInitializationException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException {
		IRunSchedulerThread scheduler = this.getRepository()
				.getSupervisorThread().getRunScheduler();

		IRun run = (IRun) this.getRepository()
				.getStaticObjectWithNameAndVersion(IRun.class,
						"test_kmeans_sfld_layered_f2");
		try {
			final String uniqueIdString = Formatter.currentTimeAsString(true,
					"MM_dd_yyyy-HH_mm_ss", Locale.UK) + "_" + run.getName();
			run.setRunIdentificationString(uniqueIdString);
			run.perform(scheduler);

			List<IRunRunnable> runnables = run.getRunRunnables();
			Assert.assertEquals(1, runnables.size());
			IRunRunnable runnable = runnables.get(0);
			List<Throwable> exceptions = runnable.getExceptions();
			Assert.assertEquals(1, exceptions.size());

			Throwable t = exceptions.get(0);
			t.printStackTrace();
			Assert.assertEquals(IncompatibleDataSetFormatException.class,
					t.getCause().getClass());
		} finally {
			FileUtils.delete(new File(FileUtils.buildPath(
					this.getRepository().getBasePath(IRunResult.class),
					run.getRunIdentificationString())));
		}
	}
}
