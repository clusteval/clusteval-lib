/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.program.r;

import org.junit.Test;

import de.clusteval.framework.threading.IRunSchedulerThread;
import de.clusteval.run.IRun;
import de.clusteval.run.Run;
import de.clusteval.utils.AbstractClustEvalTest;

/**
 * @author Christian Wiwie
 * 
 */
public class TestDBSCANClusteringRProgram extends AbstractClustEvalTest {

	/**
	 * 
	 */
	public TestDBSCANClusteringRProgram() {
		super(false, true);
	}

	/**
	 * @throws Exception 
	 */
	@Test
	public void testApplyToRelativeDataSet()
			throws Exception {
		IRunSchedulerThread scheduler = this.getRepository()
				.getSupervisorThread().getRunScheduler();

		Run<?> dbscan = (Run) repository.getStaticObjectWithNameAndVersion(
				IRun.class, "dbscan_bonemarrow");
		System.out.println(dbscan);
		dbscan.setRunIdentificationString(
				"01_20_2017-21_47_00_dbscan_bonemarrow");
		dbscan.perform(scheduler);
		if (!dbscan.getExceptions().isEmpty() && !dbscan.getExceptions().values().iterator().next().isEmpty())
			throw dbscan.getExceptions().values().iterator().next().iterator().next();
	}
}
