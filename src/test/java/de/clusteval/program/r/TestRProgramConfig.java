/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.program.r;

import java.io.FileNotFoundException;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.framework.repository.InvalidRepositoryException;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.framework.repository.RepositoryAlreadyExistsException;
import de.clusteval.framework.repository.RepositoryCouldNotBeMigratedException;
import de.clusteval.framework.repository.RepositoryVersionTooNewException;
import de.clusteval.framework.repository.RepositoryVersionTooOldException;
import de.clusteval.framework.repository.config.RepositoryConfigNotFoundException;
import de.clusteval.framework.repository.config.RepositoryConfigurationException;
import de.clusteval.framework.repository.db.DatabaseConnectException;
import de.clusteval.framework.repository.maven.MavenRepositoryDuplicateException;
import de.clusteval.program.IProgramConfig;
import de.clusteval.run.runresult.format.IRunResultFormat;
import de.clusteval.utils.AbstractClustEvalTest;

/**
 * @author Christian Wiwie
 * 
 */
public class TestRProgramConfig extends AbstractClustEvalTest {

	/**
	 * 
	 */
	public TestRProgramConfig() {
		super(false, true);
	}

	@Test
	public void testKMeansCompatibleDataSetFormats()
			throws FileNotFoundException, RepositoryAlreadyExistsException,
			InvalidRepositoryException, RepositoryConfigNotFoundException,
			RepositoryConfigurationException, InterruptedException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException {

		IProgramConfig programConfig = (IProgramConfig) this.getRepository()
				.getStaticObjectWithNameAndVersion(IProgramConfig.class,
						"KMeans_Clustering");
		List<IDataSetFormat> dataSetFormats = programConfig
				.getCompatibleDataSetFormats();
		Assert.assertEquals(1, dataSetFormats.size());
		IDataSetFormat format = dataSetFormats.get(0);
		Assert.assertEquals("MatrixDataSetFormat",
				format.getClass().getSimpleName());
	}

	@Test
	public void testKMeansRunResultFormat()
			throws FileNotFoundException, RepositoryAlreadyExistsException,
			InvalidRepositoryException, RepositoryConfigNotFoundException,
			RepositoryConfigurationException, InterruptedException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException {

		IProgramConfig programConfig = (IProgramConfig) this.getRepository()
				.getStaticObjectWithNameAndVersion(IProgramConfig.class,
						"KMeans_Clustering");
		IRunResultFormat format = programConfig.getOutputFormat();
		Assert.assertEquals("TabSeparatedRunResultFormat",
				format.getClass().getSimpleName());
	}
}
