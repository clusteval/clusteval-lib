/**
 * 
 */
package de.clusteval.program;

import java.io.File;

import junit.framework.Assert;
import junitx.framework.ArrayAssert;

import org.junit.Test;

import de.clusteval.utils.AbstractClustEvalTest;

/**
 * @author Christian Wiwie
 * 
 */
public class TestProgramParameter extends AbstractClustEvalTest {

	/**
	 * 
	 */
	public TestProgramParameter() {
		super(false, true);
	}

//	@Test
	// the options of RPrograms are no longer specified in the program configuration
	public void testOptionsString() throws UnknownProgramParameterException {
		IProgramConfig pc = (IProgramConfig) this.getRepository().getRegisteredObject(
				new File(repository.getBasePath() + "/programs/configs/testOptionsString.v1.config").getAbsoluteFile());
		IProgramParameter<?> param = pc.getParamWithId("method");
		Assert.assertEquals("", param.getMinValue());
		Assert.assertEquals("", param.getMaxValue());
		ArrayAssert.assertEquals(new String[]{"ward", "single", "complete"}, param.getOptions());
	}

//	@Test
	// the options of RPrograms are no longer specified in the program configuration
	public void testOptionsFloat() throws UnknownProgramParameterException {
		IProgramConfig pc = (IProgramConfig) this.getRepository().getRegisteredObject(
				new File(repository.getBasePath() + "/programs/configs/testOptionsFloat.v1.config").getAbsoluteFile());
		IProgramParameter<?> param = pc.getParamWithId("method");
		Assert.assertEquals("", param.getMinValue());
		Assert.assertEquals("", param.getMaxValue());
		ArrayAssert.assertEquals(new String[]{"0.0", "0.5", "1.0"}, param.getOptions());
	}

//	@Test
	// the options of RPrograms are no longer specified in the program configuration
	public void testOptionsInteger() throws UnknownProgramParameterException {
		IProgramConfig pc = (IProgramConfig) this.getRepository().getRegisteredObject(
				new File(repository.getBasePath() + "/programs/configs/testOptionsInteger.v1.config").getAbsoluteFile());
		IProgramParameter<?> param = pc.getParamWithId("method");
		Assert.assertEquals("", param.getMinValue());
		Assert.assertEquals("", param.getMaxValue());
		ArrayAssert.assertEquals(new String[]{"0", "1", "2"}, param.getOptions());
	}

}
