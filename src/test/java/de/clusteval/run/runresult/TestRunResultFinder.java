/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run.runresult;

import java.io.File;
import java.io.FileNotFoundException;
import java.security.NoSuchAlgorithmException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.InvalidRepositoryException;
import de.clusteval.framework.repository.NoRepositoryFoundException;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.Repository;
import de.clusteval.framework.repository.RepositoryAlreadyExistsException;
import de.clusteval.framework.repository.RepositoryCouldNotBeMigratedException;
import de.clusteval.framework.repository.RepositoryVersionTooNewException;
import de.clusteval.framework.repository.RepositoryVersionTooOldException;
import de.clusteval.framework.repository.config.RepositoryConfigNotFoundException;
import de.clusteval.framework.repository.config.RepositoryConfigurationException;
import de.clusteval.framework.repository.db.DatabaseConnectException;
import de.clusteval.framework.repository.db.DatabaseException;
import de.clusteval.framework.repository.db.ISQLCommunicator;
import de.clusteval.framework.repository.db.StubSQLCommunicator;
import de.clusteval.framework.threading.RepositorySupervisorThread;
import de.clusteval.framework.threading.SupervisorThread;
import de.clusteval.run.IRun;
import de.clusteval.run.RUN_STATUS;
import de.clusteval.utils.AbstractClustEvalTest;
import junit.framework.Assert;

/**
 * @author Christian Wiwie
 * 
 */
public class TestRunResultFinder extends AbstractClustEvalTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	// @Test
	public void testRunNotInProgressNotFound()
			throws FileNotFoundException, RepositoryAlreadyExistsException,
			InvalidRepositoryException, RepositoryConfigNotFoundException,
			RepositoryConfigurationException, NoRepositoryFoundException,
			InterruptedException, NoSuchAlgorithmException, DatabaseException,
			RepositoryVersionTooOldException, RepositoryVersionTooNewException,
			RepositoryCouldNotBeMigratedException {

		TestRepository repository = new TestRepository(
				new File(this.repository.getBasePath() + "").getAbsolutePath(),
				null);
		repository.setSQLCommunicator(new StubSQLCommunicator(repository));
		// IBackendServer framework = new AbstractClustevalServer(repository,
		// false);
		repository.initialize();
		try {
			while (!repository.isInitialized()) {
				Thread.sleep(100);
			}
			// We do not perform a new run, so we should not have the failed
			// assertion
			Assert.assertFalse(repository.assertionFailed);
		} finally {
			repository.terminateSupervisorThread();
		}
	}

}

class TestRepository extends Repository {

	protected boolean assertionFailed;

	/**
	 * @param basePath
	 * @param parent
	 * @throws FileNotFoundException
	 * @throws RepositoryAlreadyExistsException
	 * @throws InvalidRepositoryException
	 * @throws RepositoryConfigNotFoundException
	 * @throws RepositoryConfigurationException
	 * @throws NoRepositoryFoundException
	 * @throws NoSuchAlgorithmException
	 * @throws DatabaseConnectException
	 * @throws RepositoryVersionTooNewException
	 * @throws RepositoryVersionTooOldException
	 * @throws RepositoryCouldNotBeMigratedException
	 */
	public TestRepository(String basePath, IRepository parent)
			throws FileNotFoundException, RepositoryAlreadyExistsException,
			InvalidRepositoryException, RepositoryConfigNotFoundException,
			RepositoryConfigurationException, NoRepositoryFoundException,
			NoSuchAlgorithmException, DatabaseConnectException,
			RepositoryVersionTooOldException, RepositoryVersionTooNewException,
			RepositoryCouldNotBeMigratedException {
		super(basePath, parent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.repository.Repository#createSQLCommunicator()
	 */
	@Override
	protected ISQLCommunicator createSQLCommunicator() {
		return new StubSQLCommunicator(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.repository.Repository#createSupervisorThread()
	 */
	@Override
	protected SupervisorThread createSupervisorThread() {
		// no scanning for runresults
		RepositorySupervisorThread.CHECK_FOR_RUN_RESULTS = false;
		return new RepositorySupervisorThread(this,
				this.repositoryConfig.getThreadSleepTimes(), false);
	}

	public boolean register(RunResult object) throws RegisterException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException {
		String runIdent = object.runIdentString;
		IRun run = (IRun) this.getStaticObjectWithNameAndVersion(IRun.class,
				object.run.toString());
		if (!assertionFailed)
			assertionFailed = !(run.getStatus().equals(RUN_STATUS.FINISHED)
					|| run.getStatus().equals(RUN_STATUS.INACTIVE))
					&& (run.getRunIdentificationString() != null && run
							.getRunIdentificationString().equals(runIdent));
		return super.register(object);
	};

}
