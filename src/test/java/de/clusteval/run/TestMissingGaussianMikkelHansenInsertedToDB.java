/**
 * 
 */
package de.clusteval.run;

import java.io.File;
import java.io.IOException;

import org.apache.commons.configuration.ConfigurationException;

import de.clusteval.cluster.paramOptimization.IncompatibleParameterOptimizationMethodException;
import de.clusteval.cluster.paramOptimization.InvalidOptimizationParameterException;
import de.clusteval.cluster.paramOptimization.UnknownParameterOptimizationMethodException;
import de.clusteval.cluster.quality.UnknownClusteringQualityMeasureException;
import de.clusteval.context.IncompatibleContextException;
import de.clusteval.context.UnknownContextException;
import de.clusteval.data.DataConfigNotFoundException;
import de.clusteval.data.DataConfigurationException;
import de.clusteval.data.dataset.DataSetConfigNotFoundException;
import de.clusteval.data.dataset.DataSetConfigurationException;
import de.clusteval.data.dataset.DataSetNotFoundException;
import de.clusteval.data.dataset.IncompatibleDataSetConfigPreprocessorException;
import de.clusteval.data.dataset.NoDataSetException;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.dataset.type.UnknownDataSetTypeException;
import de.clusteval.data.distance.UnknownDistanceMeasureException;
import de.clusteval.data.goldstandard.GoldStandardConfigNotFoundException;
import de.clusteval.data.goldstandard.GoldStandardConfigurationException;
import de.clusteval.data.goldstandard.GoldStandardNotFoundException;
import de.clusteval.data.preprocessing.InvalidDataPreprocessorOptionsException;
import de.clusteval.data.preprocessing.UnknownDataPreprocessorException;
import de.clusteval.data.randomizer.UnknownDataRandomizerException;
import de.clusteval.data.statistics.UnknownDataStatisticException;
import de.clusteval.framework.repository.NoRepositoryFoundException;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.parse.ParseException;
import de.clusteval.framework.repository.parse.Parser;
import de.clusteval.program.NoOptimizableProgramParameterException;
import de.clusteval.program.UnknownParameterType;
import de.clusteval.program.UnknownProgramParameterException;
import de.clusteval.program.UnknownProgramTypeException;
import de.clusteval.program.r.UnknownRProgramException;
import de.clusteval.run.runresult.format.UnknownRunResultFormatException;
import de.clusteval.run.runresult.postprocessing.UnknownRunResultPostprocessorException;
import de.clusteval.run.statistics.UnknownRunDataStatisticException;
import de.clusteval.run.statistics.UnknownRunStatisticException;
import de.clusteval.utils.AbstractClustEvalTest;

/**
 * TODO: we have to think of a way to automatically test DB functionality
 * @author Christian Wiwie
 * 
 */
public class TestMissingGaussianMikkelHansenInsertedToDB extends AbstractClustEvalTest {

	/**
	 * 
	 */
	public TestMissingGaussianMikkelHansenInsertedToDB() {
		super(true, false);
	}
	
	//@Test
	public void test()
			throws UnknownDataSetFormatException, GoldStandardNotFoundException, GoldStandardConfigurationException,
			DataSetConfigurationException, DataSetNotFoundException, DataSetConfigNotFoundException,
			GoldStandardConfigNotFoundException, NoDataSetException, DataConfigurationException,
			DataConfigNotFoundException, NumberFormatException, ConfigurationException, RegisterException,
			UnknownContextException, UnknownParameterType, IOException, UnknownRunResultFormatException,
			UnknownClusteringQualityMeasureException, UnknownParameterOptimizationMethodException,
			NoOptimizableProgramParameterException, UnknownProgramParameterException, NoRepositoryFoundException,
			InvalidOptimizationParameterException, RunException, UnknownProgramTypeException, UnknownRProgramException,
			IncompatibleParameterOptimizationMethodException, UnknownDistanceMeasureException,
			UnknownDataSetTypeException, UnknownDataPreprocessorException,
			IncompatibleDataSetConfigPreprocessorException, IncompatibleContextException, UnknownDataStatisticException,
			UnknownRunStatisticException, UnknownRunDataStatisticException, UnknownRunResultPostprocessorException,
			UnknownDataRandomizerException,
			InvalidDataPreprocessorOptionsException, ParseException {
		IParameterOptimizationRun run = Parser.parseFromFile(IParameterOptimizationRun.class,
				new File(repository.getBasePath() + "/runs/missing_gaussians_mikkel_parameterOptimizationFlatting.run")
						.getAbsoluteFile());
		run.toString();
	}
}
