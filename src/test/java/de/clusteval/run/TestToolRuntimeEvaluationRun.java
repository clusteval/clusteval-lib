/**
 * 
 */
package de.clusteval.run;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import ch.qos.logback.classic.Level;
import de.clusteval.cluster.quality.UnknownClusteringQualityMeasureException;
import de.clusteval.context.UnknownContextException;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.dataset.type.UnknownDataSetTypeException;
import de.clusteval.data.distance.UnknownDistanceMeasureException;
import de.clusteval.framework.AbstractClustevalServer;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryObjectDumpException;
import de.clusteval.framework.threading.IRunSchedulerThread;
import de.clusteval.framework.threading.RepositorySupervisorThread;
import de.clusteval.program.IProgramConfig;
import de.clusteval.run.runnable.RunRunnableInitializationException;
import de.clusteval.run.runresult.IClusteringRunResult;
import de.clusteval.run.runresult.IRunResult;
import de.clusteval.utils.AbstractClustEvalTest;
import de.clusteval.utils.DynamicComponentInitializationException;
import dk.sdu.imada.compbio.format.Formatter;

/**
 * @author Christian Wiwie
 *
 *
 * @since Nov 6, 2017
 */
public class TestToolRuntimeEvaluationRun extends AbstractClustEvalTest {

	public TestToolRuntimeEvaluationRun() {
		super(false, true);
		// this.useMaven = true;
		AbstractClustevalServer.logLevel(Level.INFO);
		RepositorySupervisorThread.CHECK_FOR_RUN_RESULTS = true;
	}

	@Test
	public void testGetInstance() throws UnknownContextException,
			UnknownClusteringQualityMeasureException,
			UnknownDataSetFormatException, UnknownDataSetTypeException,
			UnknownDistanceMeasureException, RepositoryObjectDumpException,
			DynamicComponentInitializationException, RegisterException,
			IOException, URISyntaxException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException,
			RunRunnableInitializationException, RunInitializationException {
		for (String pcName : new String[]{"KMeans_Clustering",
				"TransClust_2"}) {
			System.out.println(pcName);
			IProgramConfig programConfig = repository
					.getStaticObjectWithNameAndVersion(IProgramConfig.class,
							pcName);
			Map<IDataConfig, IToolRuntimeEvaluationRun<?>> runs = ToolRuntimeEvaluationRun
					.getOrCreateRuntimeEvaluationRuns(getRepository(),
							programConfig);

			IRunSchedulerThread scheduler = this.getRepository()
					.getSupervisorThread().getRunScheduler();
			Map<String, Map<Integer, List<Long>>> datasetSizeToRuntime = new HashMap<>();
			datasetSizeToRuntime.put("resultconvtime", new HashMap<>());
			datasetSizeToRuntime.put("walltime", new HashMap<>());
			datasetSizeToRuntime.put("qualitytime", new HashMap<>());
			for (int i = 0; i < 3; i++) {
				for (IDataConfig dc : runs.keySet()) {
					if (dc.getDatasetConfig().getDataSet().getNumberOfSamples() > 250)
						continue;
					IToolRuntimeEvaluationRun<?> run = runs.get(dc);
					final String uniqueIdString = Formatter.currentTimeAsString(
							true, "MM_dd_yyyy-HH_mm_ss", Locale.UK) + "_"
							+ run.getName();
					run.setRunIdentificationString(uniqueIdString);
					run.perform(scheduler);

					int datasetSize = dc.getDatasetConfig().getDataSet()
							.getNumberOfSamples();
					if (!datasetSizeToRuntime.get("qualitytime")
							.containsKey(datasetSize)) {
						datasetSizeToRuntime.get("qualitytime").put(datasetSize,
								new ArrayList<>());
						datasetSizeToRuntime.get("walltime").put(datasetSize,
								new ArrayList<>());
						datasetSizeToRuntime.get("resultconvtime")
								.put(datasetSize, new ArrayList<>());
					}

					List<IRunResult> results = run.getResults();
					for (IRunResult r : results) {
						System.out.println(r);
						Map<Long, Map<String, String>> metaDataOfAllIterations = ((IClusteringRunResult) r)
								.getMetaDataOfAllIterations();
						System.out.println(metaDataOfAllIterations);

						for (Long iteration : metaDataOfAllIterations
								.keySet()) {
							if (metaDataOfAllIterations.get(iteration)
									.containsKey("walltime"))
								datasetSizeToRuntime.get("walltime")
										.get(datasetSize)
										.add(Long
												.valueOf(metaDataOfAllIterations
														.get(iteration)
														.get("walltime")));
							if (metaDataOfAllIterations.get(iteration)
									.containsKey("qualitytime"))
								datasetSizeToRuntime.get("qualitytime")
										.get(datasetSize)
										.add(Long
												.valueOf(metaDataOfAllIterations
														.get(iteration)
														.get("qualitytime")));

							if (metaDataOfAllIterations.get(iteration)
									.containsKey("resultconvtime"))
								datasetSizeToRuntime.get("resultconvtime")
										.get(datasetSize)
										.add(Long
												.valueOf(metaDataOfAllIterations
														.get(iteration)
														.get("resultconvtime")));

						}
					}
				}
			}

			for (String attr : datasetSizeToRuntime.keySet())
				System.out
						.println(attr + ":\t" + datasetSizeToRuntime.get(attr));
		}
	}

	@Test
	public void testGetProgramConfigsWithoutRuntimeInformation() {
		Set<IProgramConfig> programConfigsWithoutRuntimeInformation = ToolRuntimeEvaluationRun
				.getProgramConfigsWithoutRuntimeInformation(repository);
		assertEquals(Collections.EMPTY_SET,
				programConfigsWithoutRuntimeInformation);
	}

	@Test
	public void testGetProgramConfigsWithoutRuntimeInformation2()
			throws ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException {
		Set<IProgramConfig> programConfigsWithoutRuntimeInformation = ToolRuntimeEvaluationRun
				.getProgramConfigsWithoutRuntimeInformation(repository);
		assertEquals(
				new HashSet<IProgramConfig>(Arrays.asList(
						repository.getStaticObjectWithNameAndVersion(
								IProgramConfig.class, "TransClust_2"),
						repository.getStaticObjectWithNameAndVersion(
								IProgramConfig.class, "KMeans_Clustering"))),
				programConfigsWithoutRuntimeInformation);
	}

	@Test
	public void testGetProgramConfigsWithoutRuntimeInformation3()
			throws ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException {
		Set<IProgramConfig> programConfigsWithoutRuntimeInformation = ToolRuntimeEvaluationRun
				.getProgramConfigsWithoutRuntimeInformation(repository);
		assertEquals(Collections.EMPTY_SET,
				programConfigsWithoutRuntimeInformation);
	}
}
