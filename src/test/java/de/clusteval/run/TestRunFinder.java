/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;

import org.junit.Assert;
import org.junit.Test;

import com.google.common.io.Files;

import de.clusteval.context.Context;
import de.clusteval.framework.AbstractClustevalServer;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.InvalidRepositoryException;
import de.clusteval.framework.repository.MyRengine;
import de.clusteval.framework.repository.NoRepositoryFoundException;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.Repository;
import de.clusteval.framework.repository.RepositoryAlreadyExistsException;
import de.clusteval.framework.repository.RepositoryCouldNotBeMigratedException;
import de.clusteval.framework.repository.RepositoryVersionTooNewException;
import de.clusteval.framework.repository.RepositoryVersionTooOldException;
import de.clusteval.framework.repository.StaticRepositoryObjectCollection;
import de.clusteval.framework.repository.config.RepositoryConfigNotFoundException;
import de.clusteval.framework.repository.config.RepositoryConfigurationException;
import de.clusteval.framework.repository.db.DatabaseConnectException;
import de.clusteval.framework.repository.db.SQLConfig;
import de.clusteval.framework.repository.maven.MavenRepositoryDuplicateException;
import de.clusteval.framework.threading.RepositorySupervisorThread;
import de.clusteval.framework.threading.SupervisorThread;
import de.clusteval.run.runresult.IRunResult;
import de.clusteval.utils.AbstractClustEvalTest;
import de.clusteval.utils.StubRepositoryObject;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * @author Christian Wiwie
 * 
 */
public class TestRunFinder extends AbstractClustEvalTest {

	protected File f;

	/**
	 * 
	 */
	public TestRunFinder() {
		super(false, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.AbstractClustEvalTest#initRepositoryInstance()
	 */
	@Override
	protected void initRepositoryInstance()
			throws DatabaseConnectException, FileNotFoundException,
			RepositoryAlreadyExistsException, InvalidRepositoryException,
			RepositoryConfigNotFoundException, RepositoryConfigurationException,
			RepositoryVersionTooOldException, RepositoryVersionTooNewException,
			RepositoryCouldNotBeMigratedException {
		this.repository = new TestRepository(
				new File(String.format("testCaseRepositories/%s/%s/%s",
						this.getClass().getPackage().getName().replaceAll("\\.",
								"/"),
						this.getClass().getSimpleName(), name.getMethodName()))
								.getAbsolutePath(),
				null);
		this.repository.getRepositoryConfig().getThreadSleepTimes()
				.put("RunFinderThread", 500l);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.AbstractClustEvalTest#setUp()
	 */
	@Override
	public void setUp() throws Exception {
		super.setUp();

		if (RepositorySupervisorThread.CHECK_FOR_RUN_RESULTS) {
			while (!getRepository().isInitialized(IRunResult.class)) {
				Thread.sleep(100);
			}
		}

		repositoryObject = new StubRepositoryObject(this.getRepository(),
				System.currentTimeMillis(), new File("test"));
		context = Context.parseFromString(getRepository(), "ClusteringContext");

		// create a new run
		f = new File(FileUtils.buildPath(this.repository.getBasePath(), "runs",
				"testCase.v1.run"));
		if (f.exists())
			f.delete();
		PrintWriter bw;
		try {
			bw = new PrintWriter(new FileWriter(f));
			bw.println("programConfig = TransClust_2");
			bw.println("dataConfig = DS1");
			bw.println(
					"qualityMeasures = SilhouetteValueRClusteringQualityMeasure,TransClustF2ClusteringQualityMeasure");
			bw.println("mode = parameter_optimization");
			bw.println(
					"optimizationMethod = LayeredDivisiveParameterOptimizationMethod");
			bw.println(
					"optimizationCriterion = TransClustF2ClusteringQualityMeasure");
			bw.println("optimizationIterations = 100");
			bw.println("");
			bw.println("[TransClust_2]");
			bw.println("optimizationParameters = T");
			bw.flush();
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @throws RepositoryAlreadyExistsException
	 * @throws InvalidRepositoryException
	 * @throws RepositoryConfigNotFoundException
	 * @throws RepositoryConfigurationException
	 * @throws NoRepositoryFoundException
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws NoSuchAlgorithmException
	 * @throws DatabaseConnectException
	 */
	@SuppressWarnings("unused")
	@Test(timeout = 5000)
	public void testRunChangeNumberOfIterations()
			throws RepositoryAlreadyExistsException, InvalidRepositoryException,
			RepositoryConfigNotFoundException, RepositoryConfigurationException,
			NoRepositoryFoundException, IOException, InterruptedException,
			NoSuchAlgorithmException, DatabaseConnectException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException {
		File file = new File(
				FileUtils.buildPath(repository.getBasePath(IRun.class)),
				"testCase.v1.run.bak");
		File target = new File(
				FileUtils.buildPath(repository.getBasePath(IRun.class)),
				"testCase.v1.run");
		Files.copy(file, target);
		
		target.deleteOnExit();

		IRun run = null;
		while (run == null) {
			try {
				run = (IRun) this.repository.getStaticObjectWithNameAndVersion(
						IRun.class, "testCase");
			} catch (Exception e) {

			}
			Thread.sleep(500);
		}
		Assert.assertEquals(100, ((IParameterOptimizationRun) run)
				.getDesiredTotalIterationCount());
		Assert.assertEquals(100, ((IParameterOptimizationRun) run)
				.getOptimizationMethods().get(0).getTotalIterationCount());

		PrintWriter bw;
		f.delete();
		f.createNewFile();
		bw = new PrintWriter(new FileWriter(f));
		bw.println("programConfig = TransClust_2");
		bw.println("dataConfig = DS1");
		bw.println(
				"qualityMeasures = SilhouetteValueRClusteringQualityMeasure,TransClustF2ClusteringQualityMeasure");
		bw.println("mode = parameter_optimization");
		bw.println(
				"optimizationMethod = LayeredDivisiveParameterOptimizationMethod");
		bw.println(
				"optimizationCriterion = TransClustF2ClusteringQualityMeasure");
		bw.println("optimizationIterations = 1000");
		bw.println("");
		bw.println("[TransClust_2]");
		bw.println("optimizationParameters = T");
		bw.flush();
		bw.close();

		while (((TestRepository) this.repository).registeredTestCaseRun < 2) {
			Thread.sleep(100);
		}

		run = (IRun) this.repository
				.getStaticObjectWithNameAndVersion(IRun.class, "testCase");
		Assert.assertEquals(1000, ((IParameterOptimizationRun) run)
				.getDesiredTotalIterationCount());
		Assert.assertEquals(1000, ((IParameterOptimizationRun) run)
				.getOptimizationMethods().get(0).getTotalIterationCount());

		f.deleteOnExit();
	}
}

class TestRepository extends Repository {

	protected int registeredTestCaseRun = 0;

	/**
	 * @param basePath
	 * @param parent
	 * @throws FileNotFoundException
	 * @throws RepositoryAlreadyExistsException
	 * @throws InvalidRepositoryException
	 * @throws RepositoryConfigNotFoundException
	 * @throws RepositoryConfigurationException
	 * @throws DatabaseConnectException
	 * @throws RepositoryVersionTooNewException
	 * @throws RepositoryVersionTooOldException
	 * @throws RepositoryCouldNotBeMigratedException
	 */
	public TestRepository(String basePath, IRepository parent)
			throws FileNotFoundException, RepositoryAlreadyExistsException,
			InvalidRepositoryException, RepositoryConfigNotFoundException,
			RepositoryConfigurationException, DatabaseConnectException,
			RepositoryVersionTooOldException, RepositoryVersionTooNewException,
			RepositoryCouldNotBeMigratedException {
		super(basePath, parent);

		this.staticRepositoryEntities.put(IRun.class,
				new TestCaseRepositoryStaticRepositoryObjectCollection(this,
						null, this.getBasePath(IRun.class)));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.repository.Repository#createSupervisorThread()
	 */
	@Override
	protected SupervisorThread createSupervisorThread() {
		// no scanning for runresults
		RepositorySupervisorThread.CHECK_FOR_RUN_RESULTS = false;
		return new RepositorySupervisorThread(this,
				this.repositoryConfig.getThreadSleepTimes(), false);
	}

}

class TestCaseRepositoryStaticRepositoryObjectCollection
		extends
			StaticRepositoryObjectCollection {

	/**
	 * @param repository
	 * @param parent
	 * @param basePath
	 */
	public TestCaseRepositoryStaticRepositoryObjectCollection(
			IRepository repository, StaticRepositoryObjectCollection parent,
			String basePath) {
		super(repository, IRun.class, parent, basePath);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.RepositoryObjectEntity#register(de.
	 * clusteval.framework.repository.RepositoryObject)
	 */
	@Override
	public boolean register(IRepositoryObject object) throws RegisterException {
		boolean result = super.register(object);
		if (object.getAbsolutePath().contains("testCase.v1.run") && result)
			((TestRepository) repository).registeredTestCaseRun++;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.StaticRepositoryEntity#isVersioned()
	 */
	@Override
	public boolean isVersioned() {
		return false;
	}
}