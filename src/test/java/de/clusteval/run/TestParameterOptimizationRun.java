/**
 * 
 */
package de.clusteval.run;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalINIConfiguration;
import org.apache.commons.configuration.SubnodeConfiguration;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;

import com.google.common.io.Files;

import ch.qos.logback.classic.Level;
import de.clusteval.cluster.paramOptimization.IncompatibleParameterOptimizationMethodException;
import de.clusteval.cluster.paramOptimization.InvalidOptimizationParameterException;
import de.clusteval.cluster.paramOptimization.UnknownParameterOptimizationMethodException;
import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.cluster.quality.UnknownClusteringQualityMeasureException;
import de.clusteval.context.IncompatibleContextException;
import de.clusteval.context.UnknownContextException;
import de.clusteval.data.DataConfigNotFoundException;
import de.clusteval.data.DataConfigurationException;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.DataSetConfigNotFoundException;
import de.clusteval.data.dataset.DataSetConfigurationException;
import de.clusteval.data.dataset.DataSetNotFoundException;
import de.clusteval.data.dataset.IncompatibleDataSetConfigPreprocessorException;
import de.clusteval.data.dataset.NoDataSetException;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.dataset.type.UnknownDataSetTypeException;
import de.clusteval.data.distance.UnknownDistanceMeasureException;
import de.clusteval.data.goldstandard.GoldStandardConfigNotFoundException;
import de.clusteval.data.goldstandard.GoldStandardConfigurationException;
import de.clusteval.data.goldstandard.GoldStandardNotFoundException;
import de.clusteval.data.preprocessing.InvalidDataPreprocessorOptionsException;
import de.clusteval.data.preprocessing.UnknownDataPreprocessorException;
import de.clusteval.data.randomizer.UnknownDataRandomizerException;
import de.clusteval.data.statistics.UnknownDataStatisticException;
import de.clusteval.framework.AbstractClustevalServer;
import de.clusteval.framework.repository.InvalidRepositoryException;
import de.clusteval.framework.repository.NoRepositoryFoundException;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryAlreadyExistsException;
import de.clusteval.framework.repository.RepositoryCouldNotBeMigratedException;
import de.clusteval.framework.repository.RepositoryVersionTooNewException;
import de.clusteval.framework.repository.RepositoryVersionTooOldException;
import de.clusteval.framework.repository.config.RepositoryConfigNotFoundException;
import de.clusteval.framework.repository.config.RepositoryConfigurationException;
import de.clusteval.framework.repository.db.DatabaseConnectException;
import de.clusteval.framework.repository.maven.MavenRepositoryDuplicateException;
import de.clusteval.framework.repository.parse.ParameterOptimizationRunParser;
import de.clusteval.framework.repository.parse.ParseException;
import de.clusteval.framework.repository.parse.Parser;
import de.clusteval.framework.repository.parse.RepositoryObjectParseException;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.program.NoOptimizableProgramParameterException;
import de.clusteval.program.UnknownParameterType;
import de.clusteval.program.UnknownProgramParameterException;
import de.clusteval.program.UnknownProgramTypeException;
import de.clusteval.program.r.UnknownRProgramException;
import de.clusteval.run.runnable.IParameterOptimizationRunRunnable;
import de.clusteval.run.runresult.IParameterOptimizationResult;
import de.clusteval.run.runresult.IRunResult;
import de.clusteval.run.runresult.ParameterOptimizationResult;
import de.clusteval.run.runresult.format.UnknownRunResultFormatException;
import de.clusteval.run.runresult.postprocessing.UnknownRunResultPostprocessorException;
import de.clusteval.run.statistics.UnknownRunDataStatisticException;
import de.clusteval.run.statistics.UnknownRunStatisticException;
import de.clusteval.utils.AbstractClustEvalTest;
import dk.sdu.imada.compbio.format.Formatter;
import dk.sdu.imada.compbio.utils.Pair;

/**
 * @author Christian Wiwie
 * 
 */
public class TestParameterOptimizationRun extends AbstractClustEvalTest {

	/**
	 * 
	 */
	public TestParameterOptimizationRun() {
		super(false, true);
	}

	@Test
	public void test() throws UnknownDataSetFormatException,
			GoldStandardNotFoundException, GoldStandardConfigurationException,
			DataSetConfigurationException, DataSetNotFoundException,
			DataSetConfigNotFoundException, GoldStandardConfigNotFoundException,
			NoDataSetException, DataConfigurationException,
			DataConfigNotFoundException, NumberFormatException,
			ConfigurationException, RegisterException, UnknownContextException,
			UnknownParameterType, IOException, UnknownRunResultFormatException,
			UnknownClusteringQualityMeasureException,
			UnknownParameterOptimizationMethodException,
			NoOptimizableProgramParameterException,
			UnknownProgramParameterException, NoRepositoryFoundException,
			InvalidOptimizationParameterException, RunException,
			UnknownProgramTypeException, UnknownRProgramException,
			IncompatibleParameterOptimizationMethodException,
			UnknownDistanceMeasureException, UnknownDataSetTypeException,
			UnknownDataPreprocessorException,
			IncompatibleDataSetConfigPreprocessorException,
			IncompatibleContextException, UnknownDataStatisticException,
			UnknownRunStatisticException, UnknownRunDataStatisticException,
			UnknownRunResultPostprocessorException,
			UnknownDataRandomizerException,
			InvalidDataPreprocessorOptionsException, ParseException {
		IParameterOptimizationRun run = Parser
				.parseFromFile(IParameterOptimizationRun.class,
						new File(repository.getBasePath()
								+ "/runs/testTwiceTheParam.v1.run")
										.getAbsoluteFile());
		List<IProgramParameter<?>> paramList = new ArrayList<IProgramParameter<?>>();

		paramList.add(run.getOriginalProgramConfigs().get(0)
				.getParameterForName("T"));

		Map<IProgramConfig, List<IProgramParameter<?>>> expected = new HashMap<IProgramConfig, List<IProgramParameter<?>>>();
		expected.put(run.getOriginalProgramConfigs().get(0), paramList);
		Assert.assertEquals(expected, run.getOptimizationParameters());
	}

	/**
	 * @throws Exception
	 */
	@Test
	public void testNewParser() throws Exception {
		ParameterOptimizationRun run = (ParameterOptimizationRun) Parser
				.parseFromFile(IParameterOptimizationRun.class,
						new File(repository.getBasePath()
								+ "/runs/baechler2003.v1.run")
										.getAbsoluteFile());

		tearDown();
		setUp();

		ParameterOptimizationRun run2 = (ParameterOptimizationRun) Parser
				.parseFromFile(IParameterOptimizationRun.class,
						new File(repository.getBasePath()
								+ "/runs/baechler2003.v1.run")
										.getAbsoluteFile());

		Assert.assertEquals(run2.logFilePath, run.logFilePath);
		Assert.assertEquals(run2.runIdentString, run.runIdentString);
		Assert.assertEquals(run2.startTime, run.startTime);
		Assert.assertEquals(run2.progress, run.progress);
		Assert.assertEquals(run2.context, run.context);
		Assert.assertEquals(run2.results, run.results);
		Assert.assertEquals(run2.runnables, run.runnables);
		Assert.assertEquals(run2.originalDataConfigs, run.originalDataConfigs);
		Assert.assertEquals(run2.optimizationMethods, run.optimizationMethods);
		Assert.assertEquals(run2.optimizationParameters,
				run.optimizationParameters);
		Assert.assertEquals(run2.fixedParameterValues,
				run.fixedParameterValues);
		Assert.assertEquals(run2.originalProgramConfigs,
				run.originalProgramConfigs);
		Assert.assertEquals(run2.qualityMeasures, run.qualityMeasures);
		Assert.assertEquals(run2.clonedProgramAndDataConfigPairs,
				run.clonedProgramAndDataConfigPairs);
		Assert.assertEquals(run2.status, run.status);
	}

	/**
	 * @throws RepositoryObjectParseException
	 */
	@Test
	public void testInitRunPairsAreDifferentFromOriginals()
			throws RepositoryObjectParseException {
		ParameterOptimizationRun run = (ParameterOptimizationRun) Parser
				.parseFromFile(IParameterOptimizationRun.class,
						new File(repository.getBasePath()
								+ "/runs/astral_class_k.v8.run")
										.getAbsoluteFile());

		List<IProgramConfig> originalProgramConfigs = run
				.getOriginalProgramConfigs();
		List<IDataConfig> origianlDataConfigs = run.getOriginalDataConfigs();

		run.initClonedProgramAndDataConfigPairs(run.getOriginalProgramConfigs(),
				run.getOriginalDataConfigs());

		List<Pair<IProgramConfig, IDataConfig>> runPairs = run
				.getClonedProgramAndDataConfigPairs();
		for (Pair<IProgramConfig, IDataConfig> p : runPairs) {

			for (IProgramConfig opc : originalProgramConfigs)
				Assert.assertFalse(opc == p.getFirst());

			for (IDataConfig odc : origianlDataConfigs)
				Assert.assertFalse(odc == p.getSecond());
		}
	}

	/**
	 * @throws RepositoryObjectParseException
	 */
	@Test
	public void testInitRunPairsAreDifferentFromOriginals2()
			throws RepositoryObjectParseException {
		ParameterOptimizationRun run = (ParameterOptimizationRun) Parser
				.parseFromFile(IParameterOptimizationRun.class,
						new File(repository.getBasePath()
								+ "/runs/astral_class_k.v8.run")
										.getAbsoluteFile());

		List<IDataConfig> origianlDataConfigs = run.getOriginalDataConfigs();

		run.initClonedProgramAndDataConfigPairs(run.getOriginalProgramConfigs(),
				run.getOriginalDataConfigs());

		List<Pair<IProgramConfig, IDataConfig>> runPairs = run
				.getClonedProgramAndDataConfigPairs();
		for (Pair<IProgramConfig, IDataConfig> p : runPairs) {
			for (IDataConfig odc : origianlDataConfigs)
				Assert.assertFalse(odc.getDatasetConfig() == p.getSecond()
						.getDatasetConfig());
		}
	}

	/**
	 * @throws RepositoryObjectParseException
	 */
	@Test
	public void testInitRunPairsAreDifferentFromOriginals3()
			throws RepositoryObjectParseException {
		ParameterOptimizationRun run = (ParameterOptimizationRun) Parser
				.parseFromFile(IParameterOptimizationRun.class,
						new File(repository.getBasePath()
								+ "/runs/astral_class_k.v8.run")
										.getAbsoluteFile());

		List<IDataConfig> origianlDataConfigs = run.getOriginalDataConfigs();

		run.initClonedProgramAndDataConfigPairs(run.getOriginalProgramConfigs(),
				run.getOriginalDataConfigs());

		List<Pair<IProgramConfig, IDataConfig>> runPairs = run
				.getClonedProgramAndDataConfigPairs();
		for (Pair<IProgramConfig, IDataConfig> p : runPairs) {
			for (IDataConfig odc : origianlDataConfigs)
				Assert.assertFalse(odc.getDatasetConfig().getDataSet() == p
						.getSecond().getDatasetConfig().getDataSet());
		}
	}

	/**
	 * @throws RepositoryObjectParseException
	 */
	@Test
	public void testInitRunPairsAreDifferentFromOriginals4()
			throws RepositoryObjectParseException {
		ParameterOptimizationRun run = (ParameterOptimizationRun) Parser
				.parseFromFile(IParameterOptimizationRun.class,
						new File(repository.getBasePath()
								+ "/runs/astral_class_k.v8.run")
										.getAbsoluteFile());

		List<IDataConfig> origianlDataConfigs = run.getOriginalDataConfigs();

		run.initClonedProgramAndDataConfigPairs(run.getOriginalProgramConfigs(),
				run.getOriginalDataConfigs());

		List<Pair<IProgramConfig, IDataConfig>> runPairs = run
				.getClonedProgramAndDataConfigPairs();
		for (Pair<IProgramConfig, IDataConfig> p : runPairs) {
			for (IDataConfig odc : origianlDataConfigs)
				Assert.assertFalse(odc.getDatasetConfig().getDataSet()
						.getOriginalDataSet() == p.getSecond()
								.getDatasetConfig().getDataSet()
								.getOriginalDataSet());
		}
	}

	@Test
	public void testResume() throws Exception {
		String resultId = "2017_12_12-23_46_42_all_vs_DS1";
		File resFile = new File(String.format("%s/%s",
				repository.getBasePath(IRunResult.class), resultId)),
				backupFile = new File(String.format("%s/%s.bak",
						repository.getBasePath(IRunResult.class), resultId));

		FileUtils.deleteQuietly(resFile);
		FileUtils.copyDirectory(backupFile, resFile);

		List<IRunResult> results = new ArrayList<>();
		IRun run = ParameterOptimizationResult.parseFromRunResultFolder(
				repository, resFile, results, false, false, false);
		run.resume(repository.getSupervisorThread().getRunScheduler(),
				resultId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.AbstractClustEvalTest#setUpRepository()
	 */
	@Override
	protected void setUpRepository()
			throws DatabaseConnectException, FileNotFoundException,
			RepositoryAlreadyExistsException, InvalidRepositoryException,
			RepositoryConfigNotFoundException, RepositoryConfigurationException,
			RepositoryVersionTooOldException, RepositoryVersionTooNewException,
			RepositoryCouldNotBeMigratedException,
			MavenRepositoryDuplicateException {
		super.setUpRepository();
		repository.getRepositoryConfig().getThreadSleepTimes()
				.put("DataConfigFinderThread", 200l);
		repository.getRepositoryConfig().getThreadSleepTimes()
				.put("ProgramConfigFinderThread", 200l);
		repository.getRepositoryConfig().getThreadSleepTimes()
				.put("RunFinderThread", 200l);
		repository.getRepositoryConfig().getThreadSleepTimes()
				.put("ClusteringQualityMeasureFinderThread", 200l);

	}

	@Test
	public void testNotifyChangedDataConfig() throws Exception {
		File bak = new File(dk.sdu.imada.compbio.file.FileUtils.buildPath(
				repository.getBasePath(IDataConfig.class),
				"DS1.v1.dataconfig.bak"));
		File target = new File(dk.sdu.imada.compbio.file.FileUtils.buildPath(
				repository.getBasePath(IDataConfig.class),
				"DS1.v1.dataconfig"));
		Files.copy(bak, target);

		IExecutionRun<?> run = (IExecutionRun) this.repository
				.getStaticObjectWithNameAndVersion(IRun.class, "all_vs_DS1",
						false);
		IDataConfig dc1 = run.getOriginalDataConfigs().get(0);
		IDataConfig dc2 = run.getOriginalDataConfigs().get(1);

		BufferedWriter w = new BufferedWriter(
				new FileWriter(dc1.getFile(), false));
		BufferedReader r = new BufferedReader(new FileReader(dc2.getFile()));
		while (r.ready())
			w.write(r.read());
		r.close();
		w.close();

		Thread.sleep(500);
		for (IDataConfig newDC : run.getOriginalDataConfigs()) {
			assertEquals(dc2.getDatasetConfig(), newDC.getDatasetConfig());
			assertEquals(dc2.getGoldstandardConfig(),
					newDC.getGoldstandardConfig());
		}
	}

	@Test
	public void testNotifyChangedProgramConfig() throws Exception {
		File bak = new File(dk.sdu.imada.compbio.file.FileUtils.buildPath(
				repository.getBasePath(IProgramConfig.class),
				"APcluster_1.v1.config.bak"));
		File target = new File(dk.sdu.imada.compbio.file.FileUtils.buildPath(
				repository.getBasePath(IProgramConfig.class),
				"APcluster_1.v1.config"));
		Files.copy(bak, target);

		IExecutionRun<?> run = (IExecutionRun) this.repository
				.getStaticObjectWithNameAndVersion(IRun.class, "all_vs_DS1",
						false);
		IProgramConfig pc = run.getOriginalProgramConfigs().get(0);
		StringBuilder sb = new StringBuilder();

		BufferedReader r = new BufferedReader(new FileReader(pc.getFile()));
		while (r.ready()) {
			String s = r.readLine();
			if (s.startsWith("alias"))
				sb.append("alias = Blubb");
			else
				sb.append(s);
			sb.append(System.lineSeparator());
		}
		r.close();

		FileUtils.writeStringToFile(pc.getFile(), sb.toString());

		Thread.sleep(500);
		assertEquals("Blubb",
				run.getOriginalProgramConfigs().get(0).getProgramAlias());
	}

	@Test(expected = ObjectNotRegisteredException.class)
	public void testNotifyRunRemoved() throws Exception {
		File bak = new File(dk.sdu.imada.compbio.file.FileUtils.buildPath(
				repository.getBasePath(IRun.class), "all_vs_DS1.v1.run.bak"));
		File target = new File(dk.sdu.imada.compbio.file.FileUtils.buildPath(
				repository.getBasePath(IRun.class), "all_vs_DS1.v1.run"));
		Files.copy(bak, target);

		Thread.sleep(500);

		IExecutionRun run = (IExecutionRun) this.repository
				.getStaticObjectWithNameAndVersion(IRun.class, "all_vs_DS1",
						false);
		run.getFile().delete();

		Thread.sleep(500);

		run = (IExecutionRun) this.repository.getStaticObjectWithNameAndVersion(
				IRun.class, "all_vs_DS1", false);
	}

	@Test(expected = ObjectNotRegisteredException.class)
	public void testNotifyRemovedProgramConfig() throws Exception {
		File bak = new File(dk.sdu.imada.compbio.file.FileUtils.buildPath(
				repository.getBasePath(IProgramConfig.class),
				"TransClust_2.v1.config.bak"));
		File target = new File(dk.sdu.imada.compbio.file.FileUtils.buildPath(
				repository.getBasePath(IProgramConfig.class),
				"TransClust_2.v1.config"));
		Files.copy(bak, target);

		Thread.sleep(500);

		IExecutionRun<?> run = (IExecutionRun) this.repository
				.getStaticObjectWithNameAndVersion(IRun.class, "all_vs_DS1",
						false);

		IProgramConfig pc = run.getOriginalProgramConfigs().get(0);
		pc.getFile().delete();

		Thread.sleep(500);

		run = (IExecutionRun) this.repository.getStaticObjectWithNameAndVersion(
				IRun.class, "all_vs_DS1", false);
	}

	@Test(expected = ObjectNotRegisteredException.class)
	public void testNotifyRemovedDataConfig() throws Exception {
		File bak = new File(dk.sdu.imada.compbio.file.FileUtils.buildPath(
				repository.getBasePath(IDataConfig.class),
				"DS1.v1.dataconfig.bak"));
		File target = new File(dk.sdu.imada.compbio.file.FileUtils.buildPath(
				repository.getBasePath(IDataConfig.class),
				"DS1.v1.dataconfig"));
		Files.copy(bak, target);

		Thread.sleep(500);

		IExecutionRun<?> run = (IExecutionRun) this.repository
				.getStaticObjectWithNameAndVersion(IRun.class, "all_vs_DS1",
						false);

		IDataConfig dc = run.getOriginalDataConfigs().get(0);
		dc.getFile().delete();

		Thread.sleep(500);

		run = (IExecutionRun) this.repository.getStaticObjectWithNameAndVersion(
				IRun.class, "all_vs_DS1", false);
	}

	@Test(expected = ObjectNotRegisteredException.class)
	public void testNotifyRemovedQualityMeasure() throws Exception {
		File bak = new File(dk.sdu.imada.compbio.file.FileUtils.buildPath(
				repository.getBasePath(IClusteringQualityMeasure.class),
				"TransClustF2ClusteringQualityMeasure-3.jar.bak"));
		File target = new File(dk.sdu.imada.compbio.file.FileUtils.buildPath(
				repository.getBasePath(IClusteringQualityMeasure.class),
				"TransClustF2ClusteringQualityMeasure-3.jar"));
		Files.copy(bak, target);

		Thread.sleep(500);

		IExecutionRun run = (IExecutionRun) this.repository
				.getStaticObjectWithNameAndVersion(IRun.class, "all_vs_DS1",
						false);

		target.delete();

		Thread.sleep(500);

		run = (IExecutionRun) this.repository.getStaticObjectWithNameAndVersion(
				IRun.class, "all_vs_DS1", false);
	}

	@Test
	public void testDumpToFile() throws Exception {
		IParameterOptimizationRun run = (IParameterOptimizationRun) this.repository
				.getStaticObjectWithNameAndVersion(IRun.class, "all_vs_DS1",
						false);
		File origFile = run.getFile();
		run.setAbsolutePath(new File(run.getFile().getAbsolutePath()
				.replaceFirst(".v1.run$", "_2.v1.run")));
		run.dumpToFile();

		HierarchicalINIConfiguration conf = new HierarchicalINIConfiguration(
				run.getFile());
		assertTrue(conf.getSections().contains("APcluster_1:1"));
		SubnodeConfiguration apSection = conf.getSection("APcluster_1:1");
		assertTrue(apSection.containsKey("optimizationMethod"));

		ParameterOptimizationRunParser p = new ParameterOptimizationRunParser();
		p.parseFromFile(run.getFile());
		ParameterOptimizationRun result = p.getResult();

		assertEquals(
				run.getOptimizationMethods().stream().map(m -> m.toString())
						.collect(Collectors.toList()),
				result.getOptimizationMethods().stream().map(m -> m.toString())
						.collect(Collectors.toList()));
	}

	@Test
	public void testMetaData() throws Exception {
		String resultId = "2017_12_12-23_46_42_all_vs_DS1";
		File resFile = new File(String.format("%s/%s",
				repository.getBasePath(IRunResult.class), resultId)),
				backupFile = new File(String.format("%s/%s.bak",
						repository.getBasePath(IRunResult.class), resultId));

		FileUtils.deleteQuietly(resFile);
		FileUtils.copyDirectory(backupFile, resFile);

		List<IRunResult> results = new ArrayList<>();
		IParameterOptimizationRun run = (IParameterOptimizationRun) ParameterOptimizationResult
				.parseFromRunResultFolder(repository, resFile, results, false,
						false, false);
		run.resume(repository.getSupervisorThread().getRunScheduler(),
				resultId);

		List<IParameterOptimizationRunRunnable> runRunnables = run
				.getRunRunnables();
		for (IParameterOptimizationRunRunnable r : runRunnables) {
			Map<Long, Map<String, String>> allMeta = r.getResult()
					.getMetaDataOfAllIterations();
			r.getResult().loadIntoMemory();
			if (r.getResult().getProgramConfig().toString()
					.equals("TransClust_2:1"))
				assertEquals(new HashSet<>(r.getResult().getIterationNumbers()),
						allMeta.keySet());
			System.out.println(r.getDataConfig().getDatasetConfig().getDataSet()
					.getInStandardFormat().getNumberOfSamples());
			if (allMeta.isEmpty())
				System.out.println(String.format("%s:\tNaN / clustering",
						r.getProgramConfig()));
			else {
				double avg = allMeta.values().stream()
						.filter(m -> m.containsKey("totalwalltime"))
						.map(m -> Long.valueOf(m.get("totalwalltime")))
						.collect(Collectors.averagingLong(l -> l));
				System.out.println(String.format("%s:\t~%s / clustering",
						r.getProgramConfig(),
						Formatter.formatMsToDuration((long) avg)));
			}
		}
	}

	@Test
	public void testMetaDataWithoutExecuting() throws Exception {
		String resultId = "2017_12_12-23_46_42_all_vs_DS1";
		File resFile = new File(String.format("%s/%s",
				repository.getBasePath(IRunResult.class), resultId)),
				backupFile = new File(String.format("%s/%s.bak",
						repository.getBasePath(IRunResult.class), resultId));

		FileUtils.deleteQuietly(resFile);
		FileUtils.copyDirectory(backupFile, resFile);

		List<IRunResult> results = new ArrayList<>();
		IParameterOptimizationRun run = (IParameterOptimizationRun) ParameterOptimizationResult
				.parseFromRunResultFolder(repository, resFile, results, false,
						false, false);

		for (IRunResult r : results) {
			IParameterOptimizationResult result = (IParameterOptimizationResult) r;
			result.loadIntoMemory();
			Map<Long, Map<String, String>> allMeta = result
					.getMetaDataOfAllIterations();
			System.out.println(allMeta);
			System.out.println(result.getDataConfig().getDatasetConfig()
					.getDataSet().getNumberOfSamples());
			if (allMeta.isEmpty())
				System.out.println(String.format("%s:\tNaN / clustering",
						result.getProgramConfig()));
			else {
				double avg = allMeta.values().stream()
						.map(m -> Long.valueOf(m.get("walltime")))
						.collect(Collectors.averagingLong(l -> l));
				System.out.println(String.format("%s:\t~%s / clustering",
						result.getProgramConfig(),
						Formatter.formatMsToDuration((long) avg)));
				if (result.getProgramConfig().toString()
						.equals("TransClust_2:1"))
					assertEquals(1768.0, avg, 0.1);
				else
					assertEquals(Double.NaN, avg, 0.1);
			}
		}
	}
}
