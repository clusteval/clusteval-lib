/**
 * 
 */
package de.clusteval.run;

import java.io.File;

import org.junit.Test;

import de.clusteval.framework.repository.parse.Parser;
import de.clusteval.utils.AbstractClustEvalTest;
import junit.framework.Assert;

/**
 * @author Christian Wiwie
 * 
 */
public class TestClusteringRun extends AbstractClustEvalTest {

	/**
	 * 
	 */
	public TestClusteringRun() {
		super(false, true);
	}

	/**
	 * @throws Exception
	 */
	@Test
	public void testNewParser() throws Exception {
		ClusteringRun run = (ClusteringRun) Parser.parseFromFile(IClusteringRun.class,
				new File(repository.getBasePath() + "/runs/all_vs_DS1_clustering.v1.run").getAbsoluteFile());

		tearDown();
		setUp();

		ClusteringRun run2 = (ClusteringRun) Parser.parseFromFile(IClusteringRun.class,
				new File(repository.getBasePath() + "/runs/all_vs_DS1_clustering.v1.run").getAbsoluteFile());

		Assert.assertEquals(run2.logFilePath, run.logFilePath);
		Assert.assertEquals(run2.runIdentString, run.runIdentString);
		Assert.assertEquals(run2.startTime, run.startTime);
		Assert.assertEquals(run2.progress, run.progress);
		Assert.assertEquals(run2.context, run.context);
		Assert.assertEquals(run2.results, run.results);
		Assert.assertEquals(run2.runnables, run.runnables);
		Assert.assertEquals(run2.originalDataConfigs, run.originalDataConfigs);
		Assert.assertEquals(run2.fixedParameterValues, run.fixedParameterValues);
		Assert.assertEquals(run2.originalProgramConfigs, run.originalProgramConfigs);
		Assert.assertEquals(run2.qualityMeasures, run.qualityMeasures);
		Assert.assertEquals(run2.clonedProgramAndDataConfigPairs, run.clonedProgramAndDataConfigPairs);
		Assert.assertEquals(run2.status, run.status);
	}
}
