/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.data.IDataConfig;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.run.runresult.postprocessing.IRunResultPostprocessor;
import de.clusteval.utils.AbstractClustEvalTest;
import junit.framework.Assert;

/**
 * @author Christian Wiwie
 * 
 */
public class TestRun extends AbstractClustEvalTest {

	@Test
	public void testRun() throws RegisterException {
		/*
		 * Ensure that a run is registered in the constructor
		 */
		Run run = new ClusteringRun(this.getRepository(), context,
				System.currentTimeMillis(), new File("test"),
				new ArrayList<IProgramConfig>(), new ArrayList<IDataConfig>(),
				new ArrayList<IClusteringQualityMeasure>(),
				new HashMap<IProgramConfig, Map<IProgramParameter<?>, String>>(),
				new ArrayList<IRunResultPostprocessor>(),
				new HashMap<String, Integer>());
		run.register();
		Assert.assertTrue(run == this.getRepository().getRegisteredObject(run));
	}
}
