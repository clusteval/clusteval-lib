/**
 * 
 */
package de.clusteval.framework.repository;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Collection;

import org.junit.Test;

import ch.qos.logback.classic.Level;
import de.clusteval.cluster.quality.UnknownClusteringQualityMeasureException;
import de.clusteval.context.UnknownContextException;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.dataset.type.UnknownDataSetTypeException;
import de.clusteval.data.distance.UnknownDistanceMeasureException;
import de.clusteval.framework.AbstractClustevalServer;
import de.clusteval.framework.repository.config.RepositoryConfigNotFoundException;
import de.clusteval.framework.repository.config.RepositoryConfigurationException;
import de.clusteval.framework.repository.db.DatabaseConnectException;
import de.clusteval.framework.threading.RepositorySupervisorThread;
import de.clusteval.program.IProgramConfig;
import de.clusteval.run.RunInitializationException;
import de.clusteval.run.runnable.RunRunnableInitializationException;
import de.clusteval.run.runresult.IRunResult;
import de.clusteval.utils.AbstractClustEvalTest;
import de.clusteval.utils.DynamicComponentInitializationException;
import dk.sdu.imada.compbio.file.FileUtils;
import dk.sdu.imada.compbio.format.Formatter;

/**
 * @author Christian Wiwie
 *
 *
 * @since Nov 6, 2017
 */
public class TestRepository extends AbstractClustEvalTest {

	public TestRepository() {
		super(false, true);
		this.useMaven = true;
		AbstractClustevalServer.logLevel(Level.INFO);
		RepositorySupervisorThread.CHECK_FOR_RUN_RESULTS = true;
	}

	@Test
	public void testGetExpectedRuntime() throws DeserializationException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException,
			UnknownContextException, UnknownClusteringQualityMeasureException,
			UnknownDataSetFormatException, UnknownDataSetTypeException,
			UnknownDistanceMeasureException, RepositoryObjectDumpException,
			DynamicComponentInitializationException, RegisterException,
			IOException, URISyntaxException, RunRunnableInitializationException,
			RunInitializationException {
		for (String pcName : new String[]{"KMeans_Clustering",
				"TransClust_2"}) {
			System.out.println(pcName);
			IProgramConfig programConfig = repository
					.getStaticObjectWithNameAndVersion(IProgramConfig.class,
							pcName);

			for (int n : new int[]{25, 100, 250, 500, 750, 1000, 2500, 5000,
					6000, 7500, 10000, 15000})
				System.out.println(n + "\t"
						+ Formatter.formatMsToDuration(
								this.repository.getExpectedRuntimePerClustering(
										programConfig, n)));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.AbstractClustEvalTest#initRepositoryInstance()
	 */
	@Override
	protected void initRepositoryInstance()
			throws DatabaseConnectException, FileNotFoundException,
			RepositoryAlreadyExistsException, InvalidRepositoryException,
			RepositoryConfigNotFoundException, RepositoryConfigurationException,
			RepositoryVersionTooOldException, RepositoryVersionTooNewException,
			RepositoryCouldNotBeMigratedException {
		super.initRepositoryInstance();
		this.repository.getRepositoryConfig().getThreadSleepTimes()
				.put("RunResultFinderThread", 1000l);
	}

	@Test
	public void testGetRuntimeInformationForMissingRuns()
			throws UnknownContextException,
			UnknownClusteringQualityMeasureException,
			UnknownDataSetFormatException, UnknownDataSetTypeException,
			UnknownDistanceMeasureException, RepositoryObjectDumpException,
			DynamicComponentInitializationException, RegisterException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException,
			IOException, URISyntaxException, InterruptedException {
		for (File f : new File(
				FileUtils.buildPath(repository.getBasePath(IRunResult.class)))
						.listFiles())
			if (f.isDirectory())
				FileUtils.delete(f);
		while (!repository.getCollectionStaticEntities(IRunResult.class)
				.isEmpty())
			Thread.sleep(1000);
		this.repository.ensureRuntimeInformationForAllProgramConfigs();

		Collection<IProgramConfig> pcs = this.repository
				.getCollectionStaticEntities(IProgramConfig.class);
		for (IProgramConfig programConfig : pcs) {
			long expectedRuntime = this.repository
					.getExpectedRuntimePerClustering(programConfig, 100);
			System.out.println(
					String.format("%s\t%d", programConfig, expectedRuntime));
			assertTrue(expectedRuntime >= 0);
		}
	}
}
