/**
 * 
 */
package de.clusteval.framework.repository.maven;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.apache.maven.artifact.versioning.ComparableVersion;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.repository.ArtifactRepository;
import org.eclipse.aether.repository.RemoteRepository;
import org.eclipse.aether.resolution.MetadataRequest;
import org.eclipse.aether.resolution.VersionRangeRequest;
import org.junit.Test;

import de.clusteval.framework.repository.InvalidRepositoryException;
import de.clusteval.framework.repository.RepositoryAlreadyExistsException;
import de.clusteval.framework.repository.RepositoryCouldNotBeMigratedException;
import de.clusteval.framework.repository.RepositoryVersionTooNewException;
import de.clusteval.framework.repository.RepositoryVersionTooOldException;
import de.clusteval.framework.repository.config.RepositoryConfigNotFoundException;
import de.clusteval.framework.repository.config.RepositoryConfigurationException;
import de.clusteval.framework.repository.db.DatabaseConnectException;
import de.clusteval.utils.AbstractClustEvalTest;

/**
 * @author Christian Wiwie
 *
 */
public class TestMavenRepositoryResolverWithSnapshotVersions
		extends
			AbstractClustEvalTest {

	/**
	 * This test class requires SNAPSHOT as well as RELEASE versions in the
	 * maven repository.
	 * 
	 * Hence, at the time of creating this class, we expect the snapshot as well
	 * as the release repository available
	 */
	public TestMavenRepositoryResolverWithSnapshotVersions() {
		super();
		this.useMaven = true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.AbstractClustEvalTest#setUpRepository()
	 */
	@Override
	protected void setUpRepository()
			throws DatabaseConnectException, FileNotFoundException,
			RepositoryAlreadyExistsException, InvalidRepositoryException,
			RepositoryConfigNotFoundException, RepositoryConfigurationException,
			RepositoryVersionTooOldException, RepositoryVersionTooNewException,
			RepositoryCouldNotBeMigratedException,
			MavenRepositoryDuplicateException {
		super.setUpRepository();
		IMavenConfig mavenConfig = this.repository.getRepositoryConfig()
				.getMavenConfig();
		mavenConfig.setResolveSnapshotVersions(true);
		mavenConfig.addMavenRepository("compbio_snapshots",
				"http://maven.compbio.sdu.dk/repository/snapshots/");
		mavenConfig.addMavenRepository("compbio_release",
				"http://maven.compbio.sdu.dk/repository/internal/");
	}

	@Test
	public void testGetAvailableVersionsForF1Score()
			throws InvalidDynamicComponentNameException {
		IMavenRepositoryResolver mavenResolver = this.repository
				.getMavenResolver();
		Set<ComparableVersion> availableVersionsForDynamicComponent = mavenResolver
				.getAvailableVersionsForDynamicComponent(
						"TransClustFClusteringQualityMeasure");

		// we should find at least one RELEASE and SNAPSHOT
		boolean foundSnapshot = false, foundRelease = false;
		for (ComparableVersion v : availableVersionsForDynamicComponent) {
			boolean isSnapshot = Objects.toString(
					new DefaultArtifactVersion(v.toString()).getQualifier())
					.equals("SNAPSHOT");
			foundSnapshot |= isSnapshot;
			foundRelease |= !isSnapshot;
		}
		assertTrue(foundRelease);
		assertTrue(foundSnapshot);
	}

//	@Test
	public void testGetReleaseNotes() throws Exception {
		IMavenRepositoryResolver mavenResolver = this.repository
				.getMavenResolver();
		String releaseNotes = mavenResolver.getReleaseNotes(
				"de.clusteval.program.r", "AgnesRProgram", "3-SNAPSHOT");
		System.out.println(releaseNotes);

		String releaseTitle = mavenResolver.getReleaseTitle(
				"de.clusteval.program.r", "AgnesRProgram", "3-SNAPSHOT");
		System.out.println(releaseTitle);
	}
}
