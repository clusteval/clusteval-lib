/**
 * 
 */
package de.clusteval.framework.repository.parse;

import static org.junit.Assert.*;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ch.qos.logback.classic.Level;
import de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod;
import de.clusteval.framework.AbstractClustevalServer;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.run.IParameterOptimizationRun;
import de.clusteval.run.IRun;
import de.clusteval.run.ParameterOptimizationRun;
import de.clusteval.utils.AbstractClustEvalTest;

/**
 * @author Christian Wiwie
 *
 */
public class TestParameterOptimizationRunParser extends AbstractClustEvalTest {

	/**
	 * 
	 */
	public TestParameterOptimizationRunParser() {
		super(false, true);
	}

	@Test
	public void testCorrectParameterOptimizationMethodsIndexing()
			throws ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException,
			RepositoryObjectParseException {
		IParameterOptimizationRun run = (IParameterOptimizationRun) this.repository
				.getStaticObjectWithNameAndVersion(IRun.class, "all_vs_DS1",
						false);
		File origFile = run.getFile();

		ParameterOptimizationRunParser p = new ParameterOptimizationRunParser();
		p.parseFromFile(origFile);

		List<String> expectedMethodNames = Arrays
				.asList(new String[]{"DivisiveParameterOptimizationMethod",
						"DivisiveParameterOptimizationMethod",
						"APDivisiveParameterOptimizationMethod"});

		assertEquals(expectedMethodNames, p.paramOptMethods);

		List<IParameterOptimizationMethod> methods = p.optimizationMethods;

		assertEquals(p.dataConfigs.size() * p.programConfigs.size(),
				methods.size());

		expectedMethodNames = Arrays
				.asList(new String[]{"DivisiveParameterOptimizationMethod",
						"DivisiveParameterOptimizationMethod",
						"DivisiveParameterOptimizationMethod",
						"DivisiveParameterOptimizationMethod",
						"APDivisiveParameterOptimizationMethod",
						"APDivisiveParameterOptimizationMethod"});

		assertEquals(expectedMethodNames, methods.stream().map(m -> m.getName())
				.collect(Collectors.toList()));
	}

}
