/**
 * 
 */
package de.clusteval.framework.repository;

import org.junit.Test;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;

import de.clusteval.utils.AbstractClustEvalTest;
import dk.sdu.imada.compbio.format.Formatter;

/**
 * @author Christian Wiwie
 *
 */
public class TestMyRengine extends AbstractClustEvalTest {

	/**
	 * 
	 */
	public TestMyRengine() {
		super(false, true);
	}

	@Test
	public void testSpeedTransfer1DArray()
			throws REngineException, InterruptedException {
		MyRengine rEngine = new MyRengine("");
		double[] m = new double[10000*10000];
		long start = System.currentTimeMillis();
		System.out.println("Starting transfer");
		rEngine.assign("x", m);
		long end = System.currentTimeMillis();
		System.out.println(Formatter.formatMsToDuration(end - start));
		
//
//		rEngine.connection.assign("x", REXP.createDoubleMatrix(m));
//		long end = System.currentTimeMillis();
//		System.out.println(Formatter.formatMsToDuration(end - start));
	}

	@Test
	public void testSpeedTransfer2DArray()
			throws REngineException, InterruptedException, REXPMismatchException {
		MyRengine rEngine = new MyRengine("");
		double[][] m = new double[10000][10000];
		long start = System.currentTimeMillis();
		System.out.println("Starting transfer");
		rEngine.assign("x", m);
		long end = System.currentTimeMillis();
		System.out.println(Formatter.formatMsToDuration(end - start));
	}
}
