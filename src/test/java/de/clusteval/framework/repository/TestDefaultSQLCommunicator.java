/**
 * 
 */
package de.clusteval.framework.repository;

import org.junit.Test;

import de.clusteval.framework.repository.db.DatabaseConnectException;
import de.clusteval.framework.repository.db.DatabaseInitializationException;
import de.clusteval.framework.repository.db.DefaultSQLCommunicator;
import de.clusteval.framework.repository.db.ISQLConfig;
import de.clusteval.framework.repository.db.ISQLConfig.DB_TYPE;
import de.clusteval.framework.repository.db.SQLConfig;
import de.clusteval.utils.AbstractClustEvalTest;

/**
 * @author Christian Wiwie
 *
 */
public class TestDefaultSQLCommunicator extends AbstractClustEvalTest {

	// TODO: removing this test case for now, as it breaks CI
//	@Test
	public void test() throws DatabaseConnectException, DatabaseInitializationException {
		ISQLConfig sqlConfig = new SQLConfig(true, DB_TYPE.POSTGRESQL, "clustevalwrite", "clusteval", "localhost",
				"clustevalwrite");
		DefaultSQLCommunicator comm = new DefaultSQLCommunicator(getRepository(), sqlConfig);
	}

}
