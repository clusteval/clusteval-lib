/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.cluster.quality;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import org.apache.commons.configuration.ConfigurationException;
import org.junit.Assert;
import org.junit.Test;

import ch.qos.logback.classic.Level;
import de.clusteval.cluster.Cluster;
import de.clusteval.cluster.ClusterItem;
import de.clusteval.cluster.Clustering;
import de.clusteval.cluster.ICluster;
import de.clusteval.cluster.IClustering;
import de.clusteval.cluster.paramOptimization.IncompatibleParameterOptimizationMethodException;
import de.clusteval.cluster.paramOptimization.InvalidOptimizationParameterException;
import de.clusteval.cluster.paramOptimization.UnknownParameterOptimizationMethodException;
import de.clusteval.context.IncompatibleContextException;
import de.clusteval.context.UnknownContextException;
import de.clusteval.data.DataConfigNotFoundException;
import de.clusteval.data.DataConfigurationException;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.DataSetConfigNotFoundException;
import de.clusteval.data.dataset.DataSetConfigurationException;
import de.clusteval.data.dataset.DataSetNotFoundException;
import de.clusteval.data.dataset.IDataSet;
import de.clusteval.data.dataset.IDataSetConfig;
import de.clusteval.data.dataset.IncompatibleDataSetConfigPreprocessorException;
import de.clusteval.data.dataset.NoDataSetException;
import de.clusteval.data.dataset.format.ConversionInputToStandardConfiguration;
import de.clusteval.data.dataset.format.ConversionStandardToInputConfiguration;
import de.clusteval.data.dataset.format.DataSetConversionException;
import de.clusteval.data.dataset.format.DataSetFormat;
import de.clusteval.data.dataset.format.InvalidDataSetFormatVersionException;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.dataset.type.UnknownDataSetTypeException;
import de.clusteval.data.distance.DistanceMeasure;
import de.clusteval.data.distance.UnknownDistanceMeasureException;
import de.clusteval.data.goldstandard.GoldStandardConfigNotFoundException;
import de.clusteval.data.goldstandard.GoldStandardConfigurationException;
import de.clusteval.data.goldstandard.GoldStandardNotFoundException;
import de.clusteval.data.goldstandard.InvalidGoldStandardFormatException;
import de.clusteval.data.preprocessing.IDataPreprocessor;
import de.clusteval.data.preprocessing.InvalidDataPreprocessorOptionsException;
import de.clusteval.data.preprocessing.UnknownDataPreprocessorException;
import de.clusteval.data.randomizer.UnknownDataRandomizerException;
import de.clusteval.data.statistics.UnknownDataStatisticException;
import de.clusteval.framework.AbstractClustevalServer;
import de.clusteval.framework.repository.InvalidRepositoryException;
import de.clusteval.framework.repository.NoRepositoryFoundException;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryAlreadyExistsException;
import de.clusteval.framework.repository.RepositoryCouldNotBeMigratedException;
import de.clusteval.framework.repository.RepositoryVersionTooNewException;
import de.clusteval.framework.repository.RepositoryVersionTooOldException;
import de.clusteval.framework.repository.config.RepositoryConfigNotFoundException;
import de.clusteval.framework.repository.config.RepositoryConfigurationException;
import de.clusteval.framework.repository.db.DatabaseConnectException;
import de.clusteval.framework.repository.maven.MavenRepositoryDuplicateException;
import de.clusteval.framework.threading.IRunSchedulerThread;
import de.clusteval.program.NoOptimizableProgramParameterException;
import de.clusteval.program.UnknownParameterType;
import de.clusteval.program.UnknownProgramParameterException;
import de.clusteval.program.UnknownProgramTypeException;
import de.clusteval.program.r.UnknownRProgramException;
import de.clusteval.run.IRun;
import de.clusteval.run.RUN_STATUS;
import de.clusteval.run.Run;
import de.clusteval.run.RunException;
import de.clusteval.run.runresult.format.UnknownRunResultFormatException;
import de.clusteval.run.runresult.postprocessing.UnknownRunResultPostprocessorException;
import de.clusteval.run.statistics.UnknownRunDataStatisticException;
import de.clusteval.run.statistics.UnknownRunStatisticException;
import de.clusteval.utils.AbstractClustEvalTest;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.FormatConversionException;
import de.clusteval.utils.RCalculationException;
import de.clusteval.utils.RNotAvailableException;
import dk.sdu.imada.compbio.utils.SimilarityMatrix.NUMBER_PRECISION;

/**
 * @author Christian Wiwie
 * 
 */
public class TestSilhouetteValueRClusteringQualityMeasure
		extends
			AbstractClustEvalTest {

	static {
		AbstractClustevalServer.logLevel(Level.WARN);
	}

	/**
	 * 
	 */
	public TestSilhouetteValueRClusteringQualityMeasure() {
		super(false, true);
		AbstractClustevalServer.logLevel(Level.WARN);
//		this.useMaven = true;
	}

//	/*
//	 * (non-Javadoc)
//	 * 
//	 * @see de.clusteval.utils.AbstractClustEvalTest#setUpRepository()
//	 */
//	@Override
//	protected void setUpRepository()
//			throws DatabaseConnectException, FileNotFoundException,
//			RepositoryAlreadyExistsException, InvalidRepositoryException,
//			RepositoryConfigNotFoundException, RepositoryConfigurationException,
//			RepositoryVersionTooOldException, RepositoryVersionTooNewException,
//			RepositoryCouldNotBeMigratedException,
//			MavenRepositoryDuplicateException {
//		super.setUpRepository();
//		this.repository.getRepositoryConfig().getMavenConfig()
//				.setResolveSnapshotVersions(true);
//	}

	@Test
	public void testLargeVector() throws InstantiationException,
			IllegalAccessException, RepositoryAlreadyExistsException,
			InvalidRepositoryException, RepositoryConfigNotFoundException,
			RepositoryConfigurationException, NoRepositoryFoundException,
			RegisterException, NoSuchAlgorithmException, RNotAvailableException,
			RCalculationException, UnknownClusteringQualityMeasureException,
			InterruptedException, GoldStandardNotFoundException,
			GoldStandardConfigurationException, DataSetConfigurationException,
			DataSetNotFoundException, DataSetConfigNotFoundException,
			GoldStandardConfigNotFoundException, NoDataSetException,
			DataConfigurationException, DataConfigNotFoundException,
			ConfigurationException, UnknownContextException,
			UnknownParameterType, RunException, IncompatibleContextException,
			UnknownRunResultFormatException,
			InvalidOptimizationParameterException,
			UnknownProgramParameterException, UnknownProgramTypeException,
			UnknownRProgramException, UnknownDistanceMeasureException,
			UnknownDataSetTypeException, UnknownDataPreprocessorException,
			IncompatibleDataSetConfigPreprocessorException,
			IncompatibleParameterOptimizationMethodException,
			UnknownParameterOptimizationMethodException,
			NoOptimizableProgramParameterException,
			UnknownDataStatisticException, UnknownRunStatisticException,
			UnknownRunDataStatisticException,
			UnknownRunResultPostprocessorException,
			UnknownDataRandomizerException,
			InvalidDataPreprocessorOptionsException, FormatConversionException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException,
			DataSetConversionException,
			DynamicComponentInitializationException {
		try {
			IClustering clustering = new Clustering(this.getRepository(),
					System.currentTimeMillis(), new File(""));
			ICluster cluster1 = null;
			for (int i = 1; i <= 8000; i++) {
				if (i % 1000 == 1) {
					cluster1 = new Cluster("" + i / 1000);
					clustering.addCluster(cluster1);
				}
				cluster1.add(new ClusterItem("" + i), 1.0f);
			}

			IClusteringQualityMeasure measure = ClusteringQualityMeasure
					.parseFromString(getRepository(),
							"SilhouetteValueRClusteringQualityMeasure",
							new ClusteringQualityMeasureParameters());

			IDataConfig dataConfig = (IDataConfig) this.getRepository()
					.getStaticObjectWithNameAndVersion(IDataConfig.class,
							"EEGEyeState");
			IDataSetConfig dsc = dataConfig.getDatasetConfig();
			IDataSet ds = dsc.getDataSet();
			try {
				ds.preprocessAndConvertTo(context,
						DataSetFormat.parseFromString(this.getRepository(),
								"SimMatrixDataSetFormat"),
						new ConversionInputToStandardConfiguration(
								DistanceMeasure.parseFromString(getRepository(),
										"EuclidianDistanceMeasure"),
								NUMBER_PRECISION.DOUBLE,
								new ArrayList<IDataPreprocessor>(),
								new ArrayList<IDataPreprocessor>()),
						new ConversionStandardToInputConfiguration());
				ds.getInStandardFormat().loadIntoMemory(this);

				double quality = measure
						.getQualityOfClustering(clustering, null, dataConfig)
						.getValue();
				Assert.assertEquals(Double.NaN, quality, 0.0);
				dataConfig.getDatasetConfig().getDataSet()
						.unloadFromMemory(this);
			} finally {
				new File(ds.getInStandardFormat().getAbsolutePath()).delete();
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (InvalidGoldStandardFormatException e) {
			e.printStackTrace();
		} catch (UnknownDataSetFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvalidDataSetFormatVersionException e) {
			e.printStackTrace();
		}
	}

//	@Test
	public void testSimilaritiesInMemory() throws Exception {
		IRunSchedulerThread scheduler = this.getRepository()
				.getSupervisorThread().getRunScheduler();

		Run<?> run = (Run) repository.getStaticObjectWithNameAndVersion(IRun.class,
				"all_vs_DS1");
		// dbscan.setRunIdentificationString(
		// "01_20_2017-21_47_00_dbscan_bonemarrow");
		scheduler.schedule("1", "all_vs_DS1");
		// run.perform(scheduler);
		// while (!run.getStatus().equals(RUN_STATUS.FINISHED)) {
		// Thread.sleep(1000);
		// }
		Thread.sleep(2000);
		while (!scheduler.getFinishedRuns("1").contains("all_vs_DS1")) {
			Thread.sleep(1000);
		}
		if (!run.getExceptions().isEmpty()
				&& !run.getExceptions().values().iterator().next().isEmpty())
			throw run.getExceptions().values().iterator().next().iterator()
					.next();
	}
}
