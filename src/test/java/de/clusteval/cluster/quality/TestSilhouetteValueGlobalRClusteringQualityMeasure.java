/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.cluster.quality;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

import ch.qos.logback.classic.Level;
import de.clusteval.cluster.Cluster;
import de.clusteval.cluster.ClusterItem;
import de.clusteval.cluster.Clustering;
import de.clusteval.cluster.ICluster;
import de.clusteval.cluster.IClustering;
import de.clusteval.context.Context;
import de.clusteval.context.IContext;
import de.clusteval.context.UnknownContextException;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.IDataSet;
import de.clusteval.data.dataset.IDataSetConfig;
import de.clusteval.data.dataset.format.ConversionInputToStandardConfiguration;
import de.clusteval.data.dataset.format.ConversionStandardToInputConfiguration;
import de.clusteval.data.dataset.format.DataSetConversionException;
import de.clusteval.data.dataset.format.DataSetFormat;
import de.clusteval.data.dataset.format.InvalidDataSetFormatVersionException;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.distance.DistanceMeasure;
import de.clusteval.data.distance.UnknownDistanceMeasureException;
import de.clusteval.data.goldstandard.InvalidGoldStandardFormatException;
import de.clusteval.data.preprocessing.IDataPreprocessor;
import de.clusteval.framework.AbstractClustevalServer;
import de.clusteval.framework.repository.InvalidRepositoryException;
import de.clusteval.framework.repository.NoRepositoryFoundException;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryAlreadyExistsException;
import de.clusteval.framework.repository.config.RepositoryConfigNotFoundException;
import de.clusteval.framework.repository.config.RepositoryConfigurationException;
import de.clusteval.utils.AbstractClustEvalTest;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.FormatConversionException;
import de.clusteval.utils.RCalculationException;
import de.clusteval.utils.RNotAvailableException;
import dk.sdu.imada.compbio.utils.SimilarityMatrix.NUMBER_PRECISION;

/**
 * @author Christian Wiwie
 * 
 */
public class TestSilhouetteValueGlobalRClusteringQualityMeasure extends AbstractClustEvalTest {

	static {
		AbstractClustevalServer.logLevel(Level.WARN);
	}

	/**
	 * 
	 */
	public TestSilhouetteValueGlobalRClusteringQualityMeasure() {
		super(false, true);
	}

	@Test
	public void testSingleCluster() throws InstantiationException, IllegalAccessException,
			RepositoryAlreadyExistsException, InvalidRepositoryException, RepositoryConfigNotFoundException,
			RepositoryConfigurationException, NoRepositoryFoundException, RegisterException, NoSuchAlgorithmException,
			RNotAvailableException, RCalculationException, UnknownClusteringQualityMeasureException,
			InterruptedException, DataSetConversionException, DynamicComponentInitializationException {
		try {
			IClustering clustering = new Clustering(this.getRepository(), System.currentTimeMillis(), new File(""));
			ICluster cluster1 = new Cluster("1");
			cluster1.add(new ClusterItem("square1"), 1.0f);
			cluster1.add(new ClusterItem("star1"), 1.0f);
			cluster1.add(new ClusterItem("star2"), 1.0f);
			cluster1.add(new ClusterItem("star3"), 1.0f);
			cluster1.add(new ClusterItem("star4"), 1.0f);
			cluster1.add(new ClusterItem("star5"), 1.0f);
			cluster1.add(new ClusterItem("star6"), 1.0f);
			clustering.addCluster(cluster1);

			IClusteringQualityMeasure measure = ClusteringQualityMeasure.parseFromString(getRepository(),
					"SilhouetteValueGlobalRClusteringQualityMeasure", new ClusteringQualityMeasureParameters());
			double quality = measure.getQualityOfClustering(clustering, null, null).getValue();
			Assert.assertEquals(-1.0, quality, 0.0);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (InvalidGoldStandardFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnknownDataSetFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidDataSetFormatVersionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testTwoClusters() throws InstantiationException, IllegalAccessException,
			RepositoryAlreadyExistsException, InvalidRepositoryException, RepositoryConfigNotFoundException,
			RepositoryConfigurationException, NoRepositoryFoundException, RegisterException, NoSuchAlgorithmException,
			RNotAvailableException, RCalculationException, UnknownClusteringQualityMeasureException,
			FormatConversionException, UnknownDistanceMeasureException, UnknownContextException, InterruptedException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException, DataSetConversionException, DynamicComponentInitializationException {
		try {

			IContext context = Context.parseFromString(getRepository(), "ClusteringContext");
			IClustering clustering = new Clustering(this.getRepository(), System.currentTimeMillis(), new File(""));
			ICluster cluster1 = new Cluster("1");
			cluster1.add(new ClusterItem("id1"), 1.0f);
			cluster1.add(new ClusterItem("id2"), 1.0f);
			clustering.addCluster(cluster1);

			ICluster cluster2 = new Cluster("2");
			cluster2.add(new ClusterItem("id3"), 1.0f);
			clustering.addCluster(cluster2);

			IDataConfig dc = (IDataConfig) this.getRepository().getStaticObjectWithNameAndVersion(IDataConfig.class,
					"dunnIndexMatrixTest");
			IDataSetConfig dsc = dc.getDatasetConfig();
			IDataSet ds = dsc.getDataSet();
			ds.preprocessAndConvertTo(context,
					DataSetFormat.parseFromString(this.getRepository(), "SimMatrixDataSetFormat"),
					new ConversionInputToStandardConfiguration(
							DistanceMeasure.parseFromString(getRepository(), "EuclidianDistanceMeasure"),
							NUMBER_PRECISION.DOUBLE, new ArrayList<IDataPreprocessor>(),
							new ArrayList<IDataPreprocessor>()),
					new ConversionStandardToInputConfiguration());
			ds.getInStandardFormat().loadIntoMemory(this);
			IClusteringQualityMeasure measure = ClusteringQualityMeasure.parseFromString(getRepository(),
					"SilhouetteValueGlobalRClusteringQualityMeasure", new ClusteringQualityMeasureParameters());
			double quality = measure.getQualityOfClustering(clustering, null, dc).getValue();
			ds.getInStandardFormat().unloadFromMemory(this);
			Assert.assertEquals(0.3346755237978247, quality, 0.0);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (InvalidGoldStandardFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnknownDataSetFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidDataSetFormatVersionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
