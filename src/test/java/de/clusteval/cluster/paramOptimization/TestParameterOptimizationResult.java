/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.cluster.paramOptimization;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import de.clusteval.run.IRun;
import de.clusteval.run.runresult.IRunResult;
import de.clusteval.run.runresult.ParameterOptimizationResult;
import de.clusteval.utils.AbstractClustEvalTest;
import de.clusteval.utils.plot.Plotter;

/**
 * @author Christian Wiwie
 * 
 */
public class TestParameterOptimizationResult extends AbstractClustEvalTest {

	/**
	 * 
	 */
	public TestParameterOptimizationResult() {
		super(false, true);
	}

	/**
	 * @throws Exception
	 */
	@Test
	public void test() throws Exception {
		List<IRunResult> result = new ArrayList<IRunResult>();
		final IRun run = ParameterOptimizationResult.parseFromRunResultFolder(getRepository(),
				new File(repository.getBasePath() + "/results/11_20_2012-12_45_04_all_vs_DS1").getAbsoluteFile(), result, false,
				false, false);
		for (IRunResult r : result)
			Plotter.plotParameterOptimizationResult((ParameterOptimizationResult) r);
		System.out.println(result);
	}
}
