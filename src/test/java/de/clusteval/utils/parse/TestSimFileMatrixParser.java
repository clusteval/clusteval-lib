/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.utils.parse;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;

import junitx.framework.FileAssert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import de.clusteval.data.dataset.DataSetAttributeFilterer;
import de.clusteval.utils.AbstractClustEvalTest;
import dk.sdu.imada.compbio.utils.SimilarityMatrix;
import dk.sdu.imada.compbio.utils.parse.SimFileMatrixParser;
import dk.sdu.imada.compbio.utils.parse.SimFileParser.SIM_FILE_FORMAT;
import dk.sdu.imada.compbio.utils.parse.TextFileParser.OUTPUT_MODE;

/**
 * @author Christian Wiwie
 * 
 */
public class TestSimFileMatrixParser extends AbstractClustEvalTest {

	protected SimFileMatrixParser parser;

	@Rule
	public TestName name = new TestName();

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testMatrixParsing() {
		try {
			/*
			 * "test/utils/parse/rowSimTestFile.sim"
			 */
			DataSetAttributeFilterer filterer = new DataSetAttributeFilterer(
					repository.getBasePath() + "/data/datasets/rowSimTest/rowSimTestFile.sim.v1");
			filterer.process();

			parser = new SimFileMatrixParser(
					repository.getBasePath() + "/data/datasets/rowSimTest/rowSimTestFile.sim.v1.strip",
					SIM_FILE_FORMAT.ID_ID_SIM);
			parser.process();
			SimilarityMatrix matrix = parser.getSimilarities();

			double[][] expected = new double[][]{new double[]{1.0, 0.6, 0.5},
					new double[]{0.6, 0.5, 0.1}, new double[]{0.5, 0.1, 0.8}};
			SimilarityMatrix expectedMatrix = new SimilarityMatrix(expected);
			expectedMatrix.setIds(new String[]{"1", "2", "3"});
			assertEquals(matrix, expectedMatrix);

			filterer = new DataSetAttributeFilterer(
					repository.getBasePath() + "/data/datasets/rowSimTest/rowSimTestFile2.sim.v1");
			filterer.process();

			/*
			 * "test/utils/parse/rowSimTestFile2.sim"
			 */
			parser = new SimFileMatrixParser(
					repository.getBasePath() + "/data/datasets/rowSimTest/rowSimTestFile2.sim.v1.strip",
					SIM_FILE_FORMAT.ID_ID_SIM);
			parser.process();
			matrix = parser.getSimilarities();

			expected = new double[][]{new double[]{2354.12, 394.2, 2214.0},
					new double[]{394.2, 2354.12, -123.0},
					new double[]{2214.0, -123.0, 2354.12}};
			expectedMatrix = new SimilarityMatrix(expected);
			expectedMatrix.setIds(new String[]{"1", "2", "3"});
			assertEquals(matrix, expectedMatrix);
		} catch (IOException e) {
			e.printStackTrace();
			fail("Parsing did not succeed");
		}
	}

	@Test
	/**
	 * 
	 */
	public void testOutput() {
		try {
			DataSetAttributeFilterer filterer = new DataSetAttributeFilterer(
					repository.getBasePath() + "/data/datasets/rowSimTest/rowSimTestFile.sim.v1");
			filterer.process();
			/*
			 * "test/utils/parse/rowSimTestFile.sim" BURST output
			 */
			parser = new SimFileMatrixParser(
					repository.getBasePath() + "/data/datasets/rowSimTest/rowSimTestFile.sim.v1.strip",
					SIM_FILE_FORMAT.ID_ID_SIM,
					null,
					null,
					repository.getBasePath() + "/data/datasets/rowSimTest/rowSimTestFile.sim.v1.strip.out",
					OUTPUT_MODE.BURST, SIM_FILE_FORMAT.ID_ID_SIM);
			parser.process();

			/*
			 * Testing the similarity matrix
			 */
			SimilarityMatrix matrix = parser.getSimilarities();

			double[][] expected = new double[][]{new double[]{1.0, 0.6, 0.5},
					new double[]{0.6, 0.5, 0.1}, new double[]{0.5, 0.1, 0.8}};
			SimilarityMatrix expectedMatrix = new SimilarityMatrix(expected);
			expectedMatrix.setIds(new String[]{"1", "2", "3"});
			assertEquals(matrix, expectedMatrix);

			/*
			 * Testing the output file
			 */
			FileAssert
					.assertEquals(
							new File(
									repository.getBasePath() + "/data/datasets/rowSimTest/rowSimTestFile.sim.v1.strip"),
							new File(
									repository.getBasePath() + "/data/datasets/rowSimTest/rowSimTestFile.sim.v1.strip.out"));

			/*
			 * STREAM output
			 */
			parser = new SimFileMatrixParser(
					repository.getBasePath() + "/data/datasets/rowSimTest/rowSimTestFile.sim.v1.strip",
					SIM_FILE_FORMAT.ID_ID_SIM,
					null,
					null,
					repository.getBasePath() + "/data/datasets/rowSimTest/rowSimTestFile.sim.v1.strip.out2",
					OUTPUT_MODE.STREAM, SIM_FILE_FORMAT.ID_ID_SIM);
			parser.process();

			FileAssert
					.assertEquals(
							new File(
									repository.getBasePath() + "/data/datasets/rowSimTest/rowSimTestFile.sim.v1.strip.out"),
							new File(
									repository.getBasePath() + "/data/datasets/rowSimTest/rowSimTestFile.sim.v1.strip.out2"));

			/*
			 * Convert ID_ID_SIM -> MATRIX_HEADER, STREAM
			 */
			parser = new SimFileMatrixParser(
					repository.getBasePath() + "/data/datasets/rowSimTest/rowSimTestFile.sim.v1.strip",
					SIM_FILE_FORMAT.ID_ID_SIM,
					null,
					null,
					repository.getBasePath() + "/data/datasets/rowSimTest/rowSimTestFile.sim.v1.strip.out.matriHeader",
					OUTPUT_MODE.STREAM, SIM_FILE_FORMAT.MATRIX_HEADER);
			parser.process();

			/*
			 * Convert ID_ID_SIM -> MATRIX_HEADER, BURST
			 */
			parser = new SimFileMatrixParser(
					repository.getBasePath() + "/data/datasets/rowSimTest/rowSimTestFile.sim.v1.strip",
					SIM_FILE_FORMAT.ID_ID_SIM,
					null,
					null,
					repository.getBasePath() + "/data/datasets/rowSimTest/rowSimTestFile.sim.v1.strip.out.matriHeader2",
					OUTPUT_MODE.BURST, SIM_FILE_FORMAT.MATRIX_HEADER);
			parser.process();

			FileAssert
					.assertEquals(
							new File(
									repository.getBasePath() + "/data/datasets/rowSimTest/rowSimTestFile.sim.v1.strip.out.matriHeader"),
							new File(
									repository.getBasePath() + "/data/datasets/rowSimTest/rowSimTestFile.sim.v1.strip.out.matriHeader2"));

			/*
			 * Convert MATRIX_HEADER -> ID_ID_SIM, BURST
			 */
			parser = new SimFileMatrixParser(
					repository.getBasePath() + "/data/datasets/rowSimTest/rowSimTestFile.sim.v1.strip.out.matriHeader",
					SIM_FILE_FORMAT.MATRIX_HEADER,
					null,
					null,
					repository.getBasePath() + "/data/datasets/rowSimTest/rowSimTestFile.sim.v1.strip.out.row",
					OUTPUT_MODE.BURST, SIM_FILE_FORMAT.ID_ID_SIM);
			parser.process();
			assertEquals(expectedMatrix, parser.getSimilarities());

			FileAssert
					.assertEquals(
							new File(
									repository.getBasePath() + "/data/datasets/rowSimTest/rowSimTestFile.sim.v1.strip"),
							new File(
									repository.getBasePath() + "/data/datasets/rowSimTest/rowSimTestFile.sim.v1.strip.out.row"));

		} catch (IOException e) {
			fail("Parsing did not succeed");
		}
	}
}
