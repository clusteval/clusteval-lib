/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.utils;

import java.io.File;
import java.io.FileNotFoundException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.TestName;

import ch.qos.logback.classic.Level;
import de.clusteval.context.Context;
import de.clusteval.context.IContext;
import de.clusteval.context.UnknownContextException;
import de.clusteval.framework.AbstractClustevalServer;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.InvalidRepositoryException;
import de.clusteval.framework.repository.MyRengine;
import de.clusteval.framework.repository.Repository;
import de.clusteval.framework.repository.RepositoryAlreadyExistsException;
import de.clusteval.framework.repository.RepositoryCouldNotBeMigratedException;
import de.clusteval.framework.repository.RepositoryVersionTooNewException;
import de.clusteval.framework.repository.RepositoryVersionTooOldException;
import de.clusteval.framework.repository.config.RepositoryConfigNotFoundException;
import de.clusteval.framework.repository.config.RepositoryConfigurationException;
import de.clusteval.framework.repository.db.DatabaseConnectException;
import de.clusteval.framework.repository.db.SQLConfig;
import de.clusteval.framework.repository.maven.IMavenConfig;
import de.clusteval.framework.repository.maven.MavenRepositoryDuplicateException;
import de.clusteval.framework.threading.RepositorySupervisorThread;
import de.clusteval.run.runresult.IRunResult;

/**
 * @author Christian Wiwie
 * 
 */
public abstract class AbstractClustEvalTest implements IInMemoryListener {

	@Rule
	public TestName name = new TestName();
	protected IRepository repository;
	protected IRepositoryObject repositoryObject;
	protected IContext context;
	protected boolean useDatabase, useR, useMaven;

	/**
	 * 
	 */
	public AbstractClustEvalTest() {
		this(false, false);
//		this.useMaven = true;
	}

	/**
	 * 
	 */
	public AbstractClustEvalTest(final boolean useDatabase,
			final boolean useR) {
		super();
		this.useDatabase = useDatabase;
		this.useR = useR;
		this.useMaven = false;
//		this.useMaven = true;
		RepositorySupervisorThread.CHECK_FOR_RUN_RESULTS = false;
	}

	/**
	 * @param useMaven
	 *            the useMaven to set
	 */
	public void setUseMaven(boolean useMaven) {
		this.useMaven = useMaven;
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		AbstractClustevalServer.logLevel(Level.WARN);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		System.out.println("################## Testcase: "
				+ this.getClass().getSimpleName() + "." + name.getMethodName());
		AbstractClustevalServer.getBackendServerConfiguration()
				.setNoR(!this.useR);
		MyRengine.initializeRConnection();
		setUpRepository();
		getRepository().initialize();

		if (RepositorySupervisorThread.CHECK_FOR_RUN_RESULTS) {
			while (!getRepository().isInitialized(IRunResult.class)) {
				Thread.sleep(100);
			}
		}

		repositoryObject = new StubRepositoryObject(this.getRepository(),
				System.currentTimeMillis(), new File("test"));
		try {
			context = Context.parseFromString(getRepository(),
					"ClusteringContext");
		} catch (UnknownContextException e) {
		}
	}

	protected void initRepositoryInstance()
			throws DatabaseConnectException, FileNotFoundException,
			RepositoryAlreadyExistsException, InvalidRepositoryException,
			RepositoryConfigNotFoundException, RepositoryConfigurationException,
			RepositoryVersionTooOldException, RepositoryVersionTooNewException,
			RepositoryCouldNotBeMigratedException {
		this.repository = new Repository(
				new File(String.format("testCaseRepositories/%s/%s/%s",
						this.getClass().getPackage().getName().replaceAll("\\.",
								"/"),
						this.getClass().getSimpleName(), name.getMethodName()))
								.getAbsolutePath(),
				null);
	}

	protected void setUpRepository()
			throws DatabaseConnectException, FileNotFoundException,
			RepositoryAlreadyExistsException, InvalidRepositoryException,
			RepositoryConfigNotFoundException, RepositoryConfigurationException,
			RepositoryVersionTooOldException, RepositoryVersionTooNewException,
			RepositoryCouldNotBeMigratedException,
			MavenRepositoryDuplicateException {
		this.initRepositoryInstance();

		this.repository.getRepositoryConfig().getMavenConfig()
				.setUseMaven(this.useMaven);

		IMavenConfig mavenConfig = this.repository.getRepositoryConfig()
				.getMavenConfig();
		mavenConfig.setResolveSnapshotVersions(false);
		mavenConfig.setResolveSnapshotVersions(true);
		if (this.useMaven) {
			try {
				mavenConfig.addMavenRepository("compbio_snapshots",
						"http://maven.compbio.sdu.dk/repository/snapshots/");
				mavenConfig.addMavenRepository("compbio_release",
						"http://maven.compbio.sdu.dk/repository/internal/");
			} catch (MavenRepositoryDuplicateException e) {
				e.printStackTrace();
			}
		}

		if (!this.useDatabase) {
			System.out.println("Not using DB");
			this.repository.getRepositoryConfig()
					.setMysqlConfig(SQLConfig.DUMMY_CONFIG);
		}
		AbstractClustevalServer.getBackendServerConfiguration()
				.setNoR(!this.useR);
		// ClustevalBackendServer.getBackendServerConfiguration()
		// .setCheckForRunResults(false);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		this.repositoryObject = null;
		getRepository().terminateSupervisorThread(true);
		while (getRepository().getSupervisorThread().isAlive()) {
			Thread.sleep(100);
		}
		Repository.unregister(getRepository());
	}

	protected IRepository getRepository() {
		return repository;
	}
}
