/**
 * 
 */
package de.clusteval.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

import org.junit.Test;

import de.clusteval.data.IDataConfig;

/**
 * @author Christian Wiwie
 *
 */
public class TestFileFinder extends AbstractClustEvalTest {

//	@Test
	public void testJavaWatcherAPI() throws IOException, InterruptedException {
		WatchService watcher = FileSystems.getDefault().newWatchService();

		Path dir = new File(this.repository.getBasePath(IDataConfig.class))
				.toPath();
		try {
			WatchKey key = dir.register(watcher,
					StandardWatchEventKinds.ENTRY_CREATE,
					StandardWatchEventKinds.ENTRY_DELETE,
					StandardWatchEventKinds.ENTRY_MODIFY);
		} catch (IOException x) {
			System.err.println(x);
		}

		for (;;) {
			Thread.sleep(500);

			// wait for key to be signaled
			WatchKey key;
			try {
				key = watcher.take();
			} catch (InterruptedException x) {
				return;
			}

			for (WatchEvent<?> event : key.pollEvents()) {
				WatchEvent.Kind<?> kind = event.kind();

				// This key is registered only
				// for ENTRY_CREATE events,
				// but an OVERFLOW event can
				// occur regardless if events
				// are lost or discarded.
				if (kind == StandardWatchEventKinds.OVERFLOW) {
					continue;
				}

				// The filename is the
				// context of the event.
				WatchEvent<Path> ev = (WatchEvent<Path>) event;
				Path filename = ev.context();

				Path child = dir.resolve(filename);

				// Verify that the new
				// file is a text file.
				try {
					// Resolve the filename against the directory.
					// If the filename is "test" and the directory is "foo",
					// the resolved name is "test/foo".
					if (!Files.probeContentType(child).equals("text/plain")) {
						System.err.format(
								"New file '%s'"
										+ " is not a plain text file.%n",
								filename);
						continue;
					}
				} catch (IOException x) {
					System.err.println(x);
					continue;
				}

				// Email the file to the
				// specified email alias.
				System.out.format("Emailing file %s%n", child.toFile().getAbsolutePath());
				// Details left to reader....
			}

			// Reset the key -- this step is critical if you want to
			// receive further watch events. If the key is no longer valid,
			// the directory is inaccessible so exit the loop.
			boolean valid = key.reset();
			if (!valid) {
				break;
			}
		}
	}
}
