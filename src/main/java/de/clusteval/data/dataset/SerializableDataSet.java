/**
 * 
 */
package de.clusteval.data.dataset;

import java.io.File;

import de.clusteval.data.dataset.IDataSet.WEBSITE_VISIBILITY;
import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.data.dataset.type.IDataSetType;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.ISerializableWrapper;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.SerializableWrapperRepositoryObject;

/**
 * @author Christian Wiwie
 *
 * @param <D>
 */
public abstract class SerializableDataSet<D extends IDataSet>
		extends
			SerializableWrapperRepositoryObject<D>
		implements
			ISerializableDataSet<D> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9018799148262844950L;
	protected ISerializableWrapperRepositoryObject<IDataSetFormat> datasetFormat;
	protected ISerializableWrapperRepositoryObject<IDataSetType> datasetType;
	protected String alias;
	protected WEBSITE_VISIBILITY websiteVisibility;
	protected byte[] fileContents;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 * @param alias
	 * @param datasetFormat
	 * @param datasetType
	 * @param websiteVisibility
	 */
	public SerializableDataSet(final File absPath, final String name,
			final String version, final String alias,
			final ISerializableWrapperRepositoryObject<IDataSetFormat> datasetFormat,
			final ISerializableWrapperRepositoryObject<IDataSetType> datasetType,
			final WEBSITE_VISIBILITY websiteVisibility) {
		super(absPath, name, version);
		this.alias = alias;
		this.datasetFormat = datasetFormat;
		this.datasetType = datasetType;
		this.websiteVisibility = websiteVisibility;
	}

	/**
	 * @param dataSet
	 * @throws RepositoryObjectSerializationException
	 */
	public SerializableDataSet(final D dataSet)
			throws RepositoryObjectSerializationException {
		super(dataSet);
		this.alias = dataSet.getAlias();
		if (dataSet.getDataSetFormat() != null)
			this.datasetFormat = dataSet.getDataSetFormat().asSerializable();
		if (dataSet.getDataSetType() != null)
			this.datasetType = dataSet.getDataSetType().asSerializable();
		this.websiteVisibility = dataSet.getWebsiteVisibility();
	}

	/**
	 * @return the datasetFormat
	 */
	@Override
	public ISerializableWrapperRepositoryObject<IDataSetFormat> getDatasetFormat() {
		return datasetFormat;
	}
	
	
	/**
	 * @return the datasetType
	 */
	@Override
	public ISerializableWrapperRepositoryObject<IDataSetType> getDatasetType() {
		return datasetType;
	}
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.ISerializableDataSet#getAlias()
	 */
	@Override
	public String getAlias() {
		return this.alias;
	}
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.ISerializableDataSet#getChecksum()
	 */
	@Override
	public long getChecksum() {
		return this.wrappedComponent.getChecksum();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.dataset.ISerializableDataSet#getWebsiteVisibility()
	 */
	@Override
	public WEBSITE_VISIBILITY getWebsiteVisibility() {
		return this.wrappedComponent.getWebsiteVisibility();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.ISerializableDataSet#isRelative()
	 */
	@Override
	public boolean isRelative() {
		return this.wrappedComponent instanceof IRelativeDataSet;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.ISerializableDataSet#getFullName()
	 */
	@Override
	public String getFullName() {
		return getMajorName() + "/" + getMinorName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.ISerializableDataSet#getMajorName()
	 */
	@Override
	public String getMajorName() {
		return this.wrappedComponent.getMajorName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.ISerializableDataSet#getMinorName()
	 */
	@Override
	public String getMinorName() {
		return this.wrappedComponent.getMinorName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.ISerializableDataSet#getAbsolutePath()
	 */
	@Override
	public String getAbsolutePath() {
		return this.wrappedComponent.getAbsolutePath();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.SerializableWrapper#deserialize(de.
	 * clusteval.framework.repository.IRepository)
	 */
	@Override
	public D deserialize(IRepository repository)
			throws DeserializationException {
		// some attributes of the wrapped component are transient, thus we
		// always need to retrieve the wrapped object freshly
		return deserialize(repository, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.SerializableWrapper#deserializeInternal
	 * (de.clusteval.framework.repository.IRepository)
	 */
	@Override
	protected D deserializeInternal(IRepository repository)
			throws DeserializationException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.SerializableWrapper#compareTo(de.
	 * clusteval.framework.repository.ISerializableWrapper)
	 */
	@Override
	public int compareTo(ISerializableWrapper<D> o) {
		return super.compareTo(o);
	}
}