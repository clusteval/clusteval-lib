/**
 * 
 */
package de.clusteval.data.dataset;

import java.io.File;

import de.clusteval.data.dataset.IDataSet.WEBSITE_VISIBILITY;
import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.data.dataset.format.IRelativeDataSetFormat;
import de.clusteval.data.dataset.type.IDataSetType;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * @author Christian Wiwie
 *
 */
public class SerializableRelativeDataSet
		extends
			SerializableDataSet<IRelativeDataSet> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4589591813757880L;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 * @param alias
	 * @param datasetFormat
	 * @param datasetType
	 * @param websiteVisibility
	 */
	public SerializableRelativeDataSet(File absPath, String name,
			String version, String alias,
			ISerializableWrapperRepositoryObject<IDataSetFormat> datasetFormat,
			ISerializableWrapperRepositoryObject<IDataSetType> datasetType,
			WEBSITE_VISIBILITY websiteVisibility) {
		super(absPath, name, version, alias, datasetFormat, datasetType,
				websiteVisibility);
	}

	/**
	 * @param dataSet
	 * @throws RepositoryObjectSerializationException
	 */
	public SerializableRelativeDataSet(IRelativeDataSet dataSet)
			throws RepositoryObjectSerializationException {
		super(dataSet);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.SerializableWrapper#deserializeInternal
	 * (de.clusteval.framework.repository.IRepository)
	 */
	@Override
	protected IRelativeDataSet deserializeInternal(final IRepository repository)
			throws DeserializationException {
		try {
			return (IRelativeDataSet) repository
					.getStaticObjectWithNameAndVersion(IDataSet.class,
							name + ":" + version, true);
		} catch (ObjectNotRegisteredException
				| ObjectVersionNotRegisteredException e) {
		}
		return new RelativeDataSet(repository, System.currentTimeMillis(),
				new File(FileUtils.buildPath(
						repository.getBasePath(IDataSet.class),
						this.name + ".v" + version)),
				alias,
				(IRelativeDataSetFormat) this.datasetFormat
						.deserialize(repository),
				this.datasetType.deserialize(repository), websiteVisibility);
	}
}
