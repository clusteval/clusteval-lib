/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.data.dataset;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import de.clusteval.data.dataset.format.AbsoluteDataSetFormat;
import de.clusteval.data.dataset.format.IAbsoluteDataSetFormat;
import de.clusteval.data.dataset.format.InvalidDataSetFormatVersionException;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.dataset.type.IDataSetType;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.utils.IInMemoryListener;
import de.clusteval.utils.WeakMemoryListener;
import dk.sdu.imada.compbio.utils.SimilarityMatrix.NUMBER_PRECISION;

/**
 * 
 * An absolute dataset contains data in terms of absolute coordinates, that
 * means similarities between object pairs can be calculated by looking at the
 * absolute coordinates of two objects.
 * 
 * @author Christian Wiwie
 * 
 */
public class AbsoluteDataSet extends DataSet implements IAbsoluteDataSet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7495024506336795348L;
	/**
	 * This variable holds the contents of the dataset, after
	 * {@link #loadIntoMemory()} and before {@link #unloadFromMemory()} was
	 * invoked.
	 * <p>
	 * For absolute datasets the stored data are absolute coordinates. These are
	 * stored in a matrix form (see {@link DataMatrix}).
	 */
	private transient DataMatrix dataMatrix;

	private int numberOfSamples = -1, dimensionality = -1;

	/**
	 * 
	 * @param repository
	 *            the repository this dataset should be registered at.
	 * @param register
	 *            Whether this dataset should be registered in the repository.
	 * @param changeDate
	 *            The change date of this dataset is used for equality checks.
	 * @param alias
	 *            A short alias name for this data set.
	 * @param absPath
	 *            The absolute path of this dataset.
	 * @param dsFormat
	 *            The format of this dataset.
	 * @param dsType
	 *            The type of this dataset
	 * @param websiteVisibility
	 */
	public AbsoluteDataSet(IRepository repository, long changeDate,
			File absPath, final String alias, IAbsoluteDataSetFormat dsFormat,
			IDataSetType dsType, final WEBSITE_VISIBILITY websiteVisibility) {
		super(repository, changeDate, absPath, alias, dsFormat, dsType,
				websiteVisibility);
	}

	/**
	 * @param dataset
	 */
	public AbsoluteDataSet(AbsoluteDataSet dataset) {
		super(dataset);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see data.dataset.DataSet#clone()
	 */
	@Override
	public AbsoluteDataSet clone() {
		return new AbsoluteDataSet(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see data.dataset.DataSet#loadIntoMemory()
	 */
	@Override
	public synchronized boolean loadIntoMemory(IInMemoryListener listener,
			NUMBER_PRECISION precision) throws IllegalArgumentException,
			IOException, InvalidDataSetFormatVersionException {
		this.memoryListener.add(new WeakMemoryListener(listener));
		if (!isInMemory())
			this.dataMatrix = this.getDataSetFormat().parse(this, precision);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see data.dataset.DataSet#getDataSetContent()
	 */
	@Override
	public DataMatrix getDataSetContent() {
		return this.dataMatrix;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.dataset.DataSet#setDataSetContent(java.lang.Object)
	 */
	@Override
	public boolean setDataSetContent(Object newContent) {
		if (!(newContent instanceof DataMatrix))
			return false;

		this.dataMatrix = (DataMatrix) newContent;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see data.dataset.DataSet#isInMemory()
	 */
	@Override
	public synchronized boolean isInMemory() {
		return this.dataMatrix != null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see data.dataset.DataSet#unloadFromMemory()
	 */
	@Override
	public synchronized boolean unloadFromMemory(IInMemoryListener listener) {
		this.memoryListener.remove(new WeakMemoryListener(listener));
		if (this.memoryListener.isEmpty()) {
			this.dataMatrix = null;
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see data.dataset.DataSet#getDataSetFormat()
	 */
	@Override
	public AbsoluteDataSetFormat getDataSetFormat() {
		return (AbsoluteDataSetFormat) super.getDataSetFormat();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see data.dataset.DataSet#getIds()
	 */
	@Override
	public List<String> getIds() {
		return Arrays.asList(this.dataMatrix.getIds());
	}

	@Override
	public int getNumberOfSamples() {
		if (this.numberOfSamples == -1) {
			parseNumberOfSamplesAndDimensionality();
		}

		return this.numberOfSamples;
	}

	public int getDimensionality() {
		if (this.dimensionality > -1)
			return this.dimensionality;

		parseNumberOfSamplesAndDimensionality();

		return this.numberOfSamples;
	}

	private void parseNumberOfSamplesAndDimensionality() {
		// Read dataset file to determine number of samples and dimensionality
		boolean wasInMemory = isInMemory();
		try {
			if (!wasInMemory) {
				try {
					loadIntoMemory(this);
				} catch (InvalidDataSetFormatVersionException
						| UnknownDataSetFormatException
						| IllegalArgumentException | IOException e) {
					e.printStackTrace();
				}
			}
			this.numberOfSamples = this.getIds().size();
			this.dimensionality = this.dataMatrix.data.length > 0
					? this.dataMatrix.data[0].length
					: 0;
		} finally {
			if (!wasInMemory)
				unloadFromMemory(this);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.DataSet#asSerializable()
	 */
	@Override
	public SerializableDataSet asSerializableInternal()
			throws RepositoryObjectSerializationException {
		return new SerializableAbsoluteDataSet(this);
	}
}
