/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
package de.clusteval.data.dataset;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.context.IContext;
import de.clusteval.data.dataset.format.AbsoluteDataSetFormat;
import de.clusteval.data.dataset.format.ConversionStandardToInputConfiguration;
import de.clusteval.data.dataset.format.DataSetConversionException;
import de.clusteval.data.dataset.format.IConversionInputToStandardConfiguration;
import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.data.dataset.format.InvalidDataSetFormatVersionException;
import de.clusteval.data.dataset.format.RelativeDataSetFormat;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.dataset.type.IDataSetType;
import de.clusteval.data.preprocessing.IDataPreprocessor;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.Repository;
import de.clusteval.framework.repository.RepositoryEvent;
import de.clusteval.framework.repository.RepositoryMoveEvent;
import de.clusteval.framework.repository.RepositoryObject;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.RepositoryRemoveEvent;
import de.clusteval.framework.repository.RepositoryReplaceEvent;
import de.clusteval.utils.FormatConversionException;
import de.clusteval.utils.IInMemoryListener;
import de.clusteval.utils.NamedDoubleAttribute;
import de.clusteval.utils.NamedIntegerAttribute;
import de.clusteval.utils.WeakMemoryListener;
import dk.sdu.imada.compbio.file.FileUtils;
import dk.sdu.imada.compbio.utils.SimilarityMatrix.NUMBER_PRECISION;

/**
 * A wrapper class for a dataset on the filesystem.
 * 
 * <p>
 * A dataset corresponds to and is parsed from a file on the filesystem in the
 * corresponding folder of the repository (see {@link Repository#basePath} and
 * {@link DataSetFinder}).
 * 
 * <p>
 * When a program should be applied to a certain dataset during a run, the
 * dataset is not directly used by the run but instead a {@link DataSetConfig}
 * is referenced, which encapsulates a dataset together with some settings.
 * 
 * @author Christian Wiwie
 * 
 */
public abstract class DataSet extends RepositoryObject
		implements
			IDataSet,
			IInMemoryListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7982632076585839013L;

	/**
	 * Every data set needs an alias, that is used to represent the data set as
	 * a short string, for example on the website.
	 */
	protected String alias;

	/**
	 * The format of this dataset. The format for a dataset is required by the
	 * framework in order for it to be able to convert it to the internal
	 * standard format.
	 * 
	 * <p>
	 * When a dataset is used during a run, it is first converted from its
	 * original dataset format to the internal standard format. Then it is
	 * converted to the input format required by the clustering method.
	 */
	protected transient IDataSetFormat datasetFormat;

	/**
	 * The type of the dataset is used to categorize the datasets.
	 */
	protected transient IDataSetType datasetType;

	/**
	 * When a dataset is used during a run, it first is converted to the
	 * internal standard format and afterwards into the format required by the
	 * clustering method. This attribute holds the version of this dataset in
	 * the standard format.
	 */
	protected IDataSet thisInStandardFormat;

	/**
	 * When a dataset is used during a run, it first is converted to the
	 * internal standard format and afterwards into the format required by the
	 * clustering method. This attribute holds the original unconverted dataset.
	 */
	protected IDataSet originalDataSet;

	/**
	 * The checksum of a dataset is used to check a dataset for changes and to
	 * check two datasets for equality.
	 */
	protected long checksum;

	protected WEBSITE_VISIBILITY websiteVisibility;

	protected transient Set<WeakMemoryListener> memoryListener;

	/**
	 * Instantiates a new dataset object.
	 * 
	 * @param repository
	 *            The repository this dataset should be registered at.
	 * @param changeDate
	 *            The change date of this dataset is used for equality checks.
	 * @param absPath
	 *            The absolute path of this dataset.
	 * @param alias
	 *            A short alias name for this data set.
	 * @param dsFormat
	 *            The format of this dataset.
	 * @param dsType
	 *            The type of this dataset
	 * @param websiteVisibility
	 */
	public DataSet(final IRepository repository, final long changeDate,
			final File absPath, final String alias,
			final IDataSetFormat dsFormat, final IDataSetType dsType,
			final WEBSITE_VISIBILITY websiteVisibility) {
		super(repository, changeDate, absPath);

		this.alias = alias;
		this.datasetFormat = dsFormat;
		this.datasetType = dsType;
		this.originalDataSet = this;
		this.checksum = absPath.length();
		this.websiteVisibility = websiteVisibility;
		this.memoryListener = new HashSet<>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#register()
	 */
	@Override
	public boolean register() throws RegisterException {
		if (super.register()) {
			createAndRegisterInternalAttributes();

			// added 21.03.2013: register dataset format here: only if the
			// dataset has been registered
			this.datasetFormat.register();
			this.datasetFormat.addListener(this);
			// added 21.03.2013: register dataset type here: only if the
			// dataset has been registered
			this.datasetType.register();
			this.datasetType.addListener(this);
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.RepositoryObject#setAbsolutePath(java
	 * .io.File)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.IDataSet#setAbsolutePath(java.io.File)
	 */
	@Override
	public void setAbsolutePath(File absFilePath) {
		super.setAbsolutePath(absFilePath);

		try {
			createAndRegisterInternalAttributes();
		} catch (RegisterException e) {
		}
	}

	@SuppressWarnings("unused")
	private void createAndRegisterInternalAttributes()
			throws RegisterException {
		new NamedDoubleAttribute(this.repository,
				this.getAbsolutePath() + ":meanSimilarity",
				new Double(Float.NEGATIVE_INFINITY));
		new NamedDoubleAttribute(this.repository,
				this.getAbsolutePath() + ":minSimilarity",
				new Double(Float.NEGATIVE_INFINITY));
		new NamedDoubleAttribute(this.repository,
				this.getAbsolutePath() + ":maxSimilarity",
				new Double(Float.NEGATIVE_INFINITY));
		new NamedIntegerAttribute(this.repository,
				this.getAbsolutePath() + ":numberOfElements",
				new Integer(Integer.MIN_VALUE));
	}

	/**
	 * Copy constructor for the DataSet class.
	 * 
	 * @param dataset
	 *            the dataset to be cloned
	 */
	public DataSet(DataSet dataset) {
		super(dataset);

		this.datasetFormat = dataset.datasetFormat.clone();
		this.datasetType = dataset.datasetType.clone();
		this.alias = dataset.alias;

		this.checksum = absPath.length();

		this.websiteVisibility = dataset.websiteVisibility;
		this.memoryListener = new HashSet<>();

		if (dataset.originalDataSet != null) {
			if (dataset.originalDataSet == dataset)
				this.originalDataSet = this;
			else
				this.originalDataSet = dataset.originalDataSet.clone();
		}

		if (dataset.thisInStandardFormat != null) {
			if (dataset.thisInStandardFormat == dataset)
				this.thisInStandardFormat = this;
			else if (dataset.thisInStandardFormat == dataset.originalDataSet)
				this.thisInStandardFormat = this.originalDataSet;
			else
				this.thisInStandardFormat = dataset.thisInStandardFormat;
						//.clone();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.IDataSet#clone()
	 */
	@Override
	public abstract DataSet clone();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.IDataSet#getDataSetFormat()
	 */
	@Override
	public IDataSetFormat getDataSetFormat() {
		return datasetFormat;
	}
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.IDataSet#getDataSetType()
	 */
	@Override
	public IDataSetType getDataSetType() {
		return datasetType;
	}
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.IDataSet#getMajorName()
	 */
	@Override
	public String getMajorName() {
		String folderName = absPath.getParentFile().getName();
		return folderName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.IDataSet#getMinorName()
	 */
	@Override
	public String getMinorName() {
		return this.absPath.getName()
				.replaceAll("(" + getAbsPathVersionMatchString() + "?)$", "");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.IDataSet#getFullName()
	 */
	@Override
	public String getFullName() {
		return getMajorName() + "/" + getMinorName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IHasVersion#getVersion()
	 */
	@Override
	public ComparableVersion getVersion() {
		String name = absPath.getName();
		int colonInd = name.lastIndexOf(".");
		return new ComparableVersion(name.substring(colonInd + 2));
	}

	// /*
	// * (non-Javadoc)
	// *
	// * @see java.lang.Object#toString()
	// */
	// /*
	// * (non-Javadoc)
	// *
	// * @see de.clusteval.data.dataset.IDataSet#toString()
	// */
	// @Override
	// public String toString() {
	// return String.format("%s:%s", this.getFullName(), this.getVersion());
	// }

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#getName()
	 */
	@Override
	public String getName() {
		return this.getFullName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.RepositoryObject#copyTo(java.io.File,
	 * boolean)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.IDataSet#copyTo(java.io.File, boolean)
	 */
	@Override
	public boolean copyTo(File copyDestination, final boolean overwrite) {
		return copyTo(copyDestination, overwrite, false, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.IDataSet#copyTo(java.io.File, boolean,
	 * boolean, boolean)
	 */
	@Override
	public boolean copyTo(File copyDestination, final boolean overwrite,
			final boolean updateAbsolutePath,
			final boolean ensureVersionsForAllComponents) {
		boolean result = false;
		if (!copyDestination.exists() || overwrite)
			result = this.datasetFormat.copyDataSetTo(this, copyDestination,
					overwrite);
		if (copyDestination.exists() && updateAbsolutePath)
			this.setAbsolutePath(copyDestination);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.RepositoryObject#copyToFolder(java.io.File,
	 * boolean)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.IDataSet#copyToFolder(java.io.File,
	 * boolean)
	 */
	@Override
	public boolean copyToFolder(File copyFolderDestination,
			final boolean overwrite) {
		return this.datasetFormat.copyDataSetToFolder(this,
				copyFolderDestination, overwrite);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.IDataSet#isInMemory()
	 */
	@Override
	public abstract boolean isInMemory();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.IDataSet#loadIntoMemory()
	 */
	@Override
	public boolean loadIntoMemory(IInMemoryListener listener)
			throws IllegalArgumentException, IOException,
			InvalidDataSetFormatVersionException,
			UnknownDataSetFormatException {
		return this.loadIntoMemory(listener, NUMBER_PRECISION.DOUBLE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.dataset.IDataSet#loadIntoMemory(de.wiwie.wiutils.utils.
	 * SimilarityMatrix.NUMBER_PRECISION)
	 */
	@Override
	public abstract boolean loadIntoMemory(IInMemoryListener listener,
			NUMBER_PRECISION precision)
			throws UnknownDataSetFormatException, IllegalArgumentException,
			IOException, InvalidDataSetFormatVersionException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.IDataSet#writeToFile(boolean)
	 */
	@Override
	public boolean writeToFile(final boolean withHeader) {
		if (!isInMemory())
			return false;
		return this.getDataSetFormat().writeToFile(this, withHeader);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.IDataSet#getDataSetContent()
	 */
	@Override
	public abstract Object getDataSetContent();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.dataset.IDataSet#setDataSetContent(java.lang.Object)
	 */
	@Override
	public abstract boolean setDataSetContent(Object newContent);

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.IDataSet#unloadFromMemory()
	 */
	@Override
	public abstract boolean unloadFromMemory(IInMemoryListener listener);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.dataset.IDataSet#preprocessAndConvertTo(de.clusteval.
	 * context.Context, de.clusteval.data.dataset.format.DataSetFormat,
	 * de.clusteval.data.dataset.format.ConversionInputToStandardConfiguration,
	 * de.clusteval.data.dataset.format.ConversionStandardToInputConfiguration)
	 */
	@Override
	public DataSet preprocessAndConvertTo(final IContext context,
			final IDataSetFormat targetFormat,
			final IConversionInputToStandardConfiguration configInputToStandard,
			final ConversionStandardToInputConfiguration configStandardToInput)
			throws FormatConversionException, IOException, InterruptedException,
			DataSetConversionException {

		// only one conversion process at a time
		File sourceFile = FileUtils
				.getCommonFile(new File(this.getAbsolutePath()));
		synchronized (sourceFile) {
			IDataSet result = null;
			IDataSetFormat sourceFormat = this.getDataSetFormat();

			// check if we can convert from source to target format
			// right now no conversion from relative to absolute possible
			if (sourceFormat instanceof RelativeDataSetFormat
					&& targetFormat instanceof AbsoluteDataSetFormat) {
				throw new FormatConversionException(
						"No conversion from relative to absolute dataset format possible.");
			}

			// check, whether dataset format parsers are registered.
			if ((!sourceFormat.equals(context.getStandardInputFormat())
					&& !this.getRepository().isRegisteredForDataSetFormat(
							sourceFormat.getClass()))
					|| (!targetFormat.equals(context.getStandardInputFormat())
							&& !this.getRepository()
									.isRegisteredForDataSetFormat(
											targetFormat.getClass())))
				throw new FormatConversionException("No conversion from "
						+ sourceFormat + " to " + targetFormat
						+ " via internal standard format "
						+ context.getStandardInputFormat()
						+ " possible, because of missing dataset format parsers.");

			// 13.04.2013: update the original dataset of the dataset to itself
			this.originalDataSet = this;
			this.log.debug("Apply preprocessors");
			// 13.04.2013: apply all data preprocessors before distance
			// conversion
			IDataSet preprocessed = this;
			List<IDataPreprocessor> preprocessors = configInputToStandard
					.getPreprocessorsBeforeDistance();
			for (IDataPreprocessor proc : preprocessors) {
				preprocessed = proc.preprocess(preprocessed);
			}

			// convert the input format to the standard format
			IDataSetFormat standardFormat = context.getStandardInputFormat();
			// DataSetFormat.parseFromString(
			// repository, context.getStandardInputFormat().getClass()
			// .getSimpleName());
			standardFormat.setNormalized(targetFormat.getNormalized());

			this.log.debug(String.format("Convert input to standard format %s",
					standardFormat.toString()));

			// Remove dataset attributes from file and write the result to
			// dataSet.getAbsolutePath() + ".strip"
			final File strippedFilePath = new File(
					this.getAbsolutePath() + ".strip");
			if (!strippedFilePath.exists()) {
				DataSetAttributeFilterer filterer = new DataSetAttributeFilterer(
						this.getAbsolutePath());
				filterer.process();
			}
			this.setAbsolutePath(strippedFilePath);

			result = preprocessed.convertToStandardDirectly(context,
					configInputToStandard);

			// 13.04.2013: apply all data preprocessors after distance
			// conversion
			this.log.debug("Apply preprocessors");
			preprocessors = configInputToStandard
					.getPreprocessorsAfterDistance();
			for (IDataPreprocessor proc : preprocessors) {
				result = proc.preprocess(result);
			}

			this.log.debug(String.format("Convert to input format of method %s",
					targetFormat.toString()));
			/*
			 * This temporary dataset is now in our standard format. Store it
			 * and convert it to the target format.
			 */
			result = result.convertStandardToDirectly(context, targetFormat,
					configStandardToInput);

			return (DataSet) result;
		}
	}

	/**
	 * This method is a helper method to convert a dataset in a internal
	 * standard format to a target format directly, that means using one
	 * conversion step.
	 * 
	 * @param targetFormat
	 *            This is the format, the dataset is expected to be in after the
	 *            conversion process. After the dataset is converted to the
	 *            internal format, it is converted to the target format.
	 * @param configStandardToInput
	 *            This is the configuration that is used during the conversion
	 *            from the internal standard format to the target format.
	 * @return The dataset in the target format.
	 * @throws DataSetConversionException
	 */
	@Override
	public IDataSet convertStandardToDirectly(final IContext context,
			final IDataSetFormat targetFormat,
			final ConversionStandardToInputConfiguration configStandardToInput)
			throws FormatConversionException, DataSetConversionException {

		IDataSet result = null;

		// if the target format is equal to the standard format, we simply
		// return the old dataset
		IDataSetFormat standardFormat = context.getStandardInputFormat();
		if (targetFormat.equals(standardFormat)) {
			result = this;
		}
		// if the target format is an absolute format, then the conversion is
		// only possible, if the original data set was an absolute data set.
		// Then we return the original data set.
		else if (this.getDataSetFormat() instanceof RelativeDataSetFormat
				&& targetFormat instanceof AbsoluteDataSetFormat) {
			if (this.getOriginalDataSet()
					.getDataSetFormat() instanceof AbsoluteDataSetFormat) {
				// changed 18.03.2014: returning a new dataset object instead of
				// the originalDataSet with changed path here
				this.originalDataSet.copyTo(new File(
						this.originalDataSet.getAbsolutePath() + ".conv"),
						false, false, false);
				IDataSet tmp = this.originalDataSet.clone();
				tmp.setAbsolutePath(new File(
						this.originalDataSet.getAbsolutePath() + ".conv"));
				((DataSet) tmp).originalDataSet = this.originalDataSet;
				result = tmp;
			} else
				throw new FormatConversionException(
						"No conversion from relative to absolute dataset format possible.");
		} else
			result = (DataSet) targetFormat.convertToThisFormat(this,
					targetFormat, configStandardToInput);
		((DataSet) result).thisInStandardFormat = this;
		((DataSet) result).originalDataSet = this.originalDataSet;
		return result;
	}

	/**
	 * This method is a helper method to convert a dataset in its original
	 * format to a internal standard format directly, that means using one
	 * conversion step.
	 * 
	 * @param dsFormat
	 *            This is the format, the dataset is expected to be in after the
	 *            conversion process. After the dataset is converted to the
	 *            internal format, it is converted to the target format.
	 * @param configInputToStandard
	 *            This is the configuration that is used during the conversion
	 *            from the original format to the internal standard format.
	 * @return The dataset in the target format.
	 * @throws DataSetConversionException
	 */
	@Override
	public IDataSet convertToStandardDirectly(final IContext context,
			final IConversionInputToStandardConfiguration configInputToStandard)
			throws DataSetConversionException {
		IDataSet result = null;

		// if the source format is equal to the standard format, we simply
		// return the old dataset
		IDataSetFormat standardFormat = context.getStandardInputFormat();
		if (this.getDataSetFormat().equals(standardFormat)) {
			result = this;
		} else
			result = this.getDataSetFormat().convertToStandardFormat(this,
					configInputToStandard);
		((DataSet) result).thisInStandardFormat = result;
		this.thisInStandardFormat = result;
		((DataSet) result).originalDataSet = this.originalDataSet;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.wiwie.wiutils.utils.RepositoryObject#notify(utils.RepositoryEvent)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.IDataSet#notify(de.clusteval.framework.
	 * repository.RepositoryEvent)
	 */
	@Override
	public void notify(RepositoryEvent e) throws RegisterException {
		if (e instanceof RepositoryReplaceEvent) {
			RepositoryReplaceEvent event = (RepositoryReplaceEvent) e;
			if (event.getOld().equals(this))
				super.notify(event);
		} else if (e instanceof RepositoryRemoveEvent) {
			RepositoryRemoveEvent event = (RepositoryRemoveEvent) e;
			if (event.getRemovedObject().equals(this))
				super.notify(event);
			else if (event.getRemovedObject().equals(this.datasetFormat)) {
				event.getRemovedObject().removeListener(this);
				this.log.info(
						"DataSet " + this + ": Removed, because DataSetFormat "
								+ datasetFormat + " has changed.");
				RepositoryRemoveEvent newEvent = new RepositoryRemoveEvent(
						this);
				this.unregister();
				this.notify(newEvent);
			} else if (event.getRemovedObject().equals(this.datasetType)) {
				event.getRemovedObject().removeListener(this);
				this.log.info(
						"DataSet " + this + ": Removed, because DataSetType "
								+ datasetType + " has changed.");
				RepositoryRemoveEvent newEvent = new RepositoryRemoveEvent(
						this);
				this.unregister();
				this.notify(newEvent);
			}
		} else if (e instanceof RepositoryMoveEvent) {
			RepositoryMoveEvent event = (RepositoryMoveEvent) e;
			if (event.getObject().equals(this))
				super.notify(event);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.IDataSet#moveTo(java.io.File, boolean)
	 */
	@Override
	public boolean moveTo(final File targetFile, final boolean overwrite) {
		if (!targetFile.exists() || overwrite) {
			boolean result = this.datasetFormat.moveDataSetTo(this, targetFile,
					overwrite);
			if (targetFile.exists()) {
				this.absPath = targetFile;
				try {
					this.notify(new RepositoryMoveEvent(this));
				} catch (RegisterException e) {
					e.printStackTrace();
				}
			}
			return result;
		}
		if (targetFile.exists())
			this.absPath = targetFile;
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.IDataSet#getAlias()
	 */
	@Override
	public String getAlias() {
		return this.alias;
	}
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.IDataSet#getInStandardFormat()
	 */
	@Override
	public IDataSet getInStandardFormat() {
		return this.thisInStandardFormat;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.IDataSet#getOriginalDataSet()
	 */
	@Override
	public IDataSet getOriginalDataSet() {
		return this.originalDataSet;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.IDataSet#getChecksum()
	 */
	@Override
	public long getChecksum() {
		return this.checksum;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.IDataSet#getIds()
	 */
	@Override
	public abstract List<String> getIds();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.IDataSet#getWebsiteVisibility()
	 */
	@Override
	public WEBSITE_VISIBILITY getWebsiteVisibility() {
		return this.websiteVisibility;
	}

	/**
	 * This method parses the header of a dataset file. A header is required for
	 * a dataset file to be recognized by the framework as a valid dataset file.
	 * If the file does not contain any header lines, it is ignored by the
	 * framework. A header line is of the form '// attribute = value'. The
	 * header should contain several lines:
	 * 
	 * <p>
	 * The type of the dataset, e.g. '// dataSetType =
	 * GeneExpressionDataSetType'
	 * <p>
	 * The format of the dataset, e.g. '// dataSetFormat =
	 * RowSimDataSetFormat:1'
	 * 
	 * @param absPath
	 * @return
	 * @throws IOException
	 */
	@Deprecated
	protected static Map<String, String> extractDataSetAttributes(
			final File absPath) throws IOException {
		DataSetAttributeParser attributeParser = new DataSetAttributeParser(
				absPath.getAbsolutePath());
		attributeParser.process();
		Map<String, String> attributeValues = attributeParser
				.getAttributeValues();
		return attributeValues;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#
	 * ensureVersionsForAllComponents(java.lang.String)
	 */
	@Override
	protected String ensureVersionsForAllComponents(String fileContents) {
		String newfileContents = super.ensureVersionsForAllComponents(
				fileContents);

		newfileContents = ensureVersionForComponent(newfileContents,
				datasetFormat);
		newfileContents = ensureVersionForComponent(newfileContents,
				datasetType);

		return newfileContents;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.IDataSet#getNumberOfSamples()
	 */
	public int getNumberOfSamples() {
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#asSerializable()
	 */
	@Override
	public SerializableDataSet asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableDataSet) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#asSerializable()
	 */
	@Override
	public SerializableDataSet asSerializableInternal()
			throws RepositoryObjectSerializationException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#isSerializable()
	 */
	@Override
	public boolean isSerializable() {
		return true;
	}

	/**
	 * @return the memoryListener
	 */
	public Set<WeakMemoryListener> getMemoryListener() {
		return memoryListener;
	}
}
