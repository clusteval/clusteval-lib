/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
package de.clusteval.data.dataset.format;

import java.io.File;
import java.io.IOException;

import de.clusteval.data.dataset.DataMatrix;
import de.clusteval.data.dataset.IDataSet;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import dk.sdu.imada.compbio.utils.SimilarityMatrix.NUMBER_PRECISION;

/**
 * @author Christian Wiwie
 * 
 */
public abstract class AbsoluteDataSetFormat extends DataSetFormat implements IAbsoluteDataSetFormat {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4307367061292046133L;

	/**
	 * Instantiates a new absolute data set format.
	 * 
	 * @param repo
	 * @param changeDate
	 * @param absPath
	 * 
	 * @throws RegisterException
	 * 
	 */
	public AbsoluteDataSetFormat(final IRepository repo, final long changeDate, final File absPath)
			throws RegisterException {
		super(repo, changeDate, absPath);
	}

	/**
	 * The copy constructor for this format.
	 * 
	 * @param other
	 *            The object to clone.
	 * @throws RegisterException
	 */
	public AbsoluteDataSetFormat(final AbsoluteDataSetFormat other) throws RegisterException {
		super(other);
	}

	public final DataMatrix parse(final IDataSet dataSet, NUMBER_PRECISION precision)
			throws IllegalArgumentException, IOException, InvalidDataSetFormatVersionException {
		return (DataMatrix) super.parse(dataSet, precision);
	}
}
