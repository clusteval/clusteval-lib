/**
 * 
 */
package de.clusteval.data.dataset.format;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import de.clusteval.data.distance.IDistanceMeasure;
import de.clusteval.data.preprocessing.IDataPreprocessor;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import dk.sdu.imada.compbio.utils.SimilarityMatrix.NUMBER_PRECISION;

public class SerializableConversionInputToStandardConfiguration
		implements
			Serializable,
			ISerializableConversionInputToStandardConfiguration {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7942555015050795910L;

	protected IConversionInputToStandardConfiguration config;

	protected List<ISerializableWrapperRepositoryObject<IDataPreprocessor>> preprocessorsBeforeDistance;

	protected ISerializableWrapperRepositoryObject<IDistanceMeasure> distanceMeasureAbsoluteToRelative;

	protected List<ISerializableWrapperRepositoryObject<IDataPreprocessor>> preprocessorsAfterDistance;

	protected NUMBER_PRECISION similarityPrecision;

	/**
	 * @param preprocessorsBeforeDistance
	 * @param distanceMeasureAbsoluteToRelative
	 * @param similarityPrecision
	 * @param preprocessorsAfterDistance
	 */
	public SerializableConversionInputToStandardConfiguration(
			final ISerializableWrapperRepositoryObject<IDistanceMeasure> distanceMeasureAbsoluteToRelative,
			final NUMBER_PRECISION similarityPrecision,
			final List<ISerializableWrapperRepositoryObject<IDataPreprocessor>> preprocessorsBeforeDistance,
			final List<ISerializableWrapperRepositoryObject<IDataPreprocessor>> preprocessorsAfterDistance) {
		super();
		this.preprocessorsBeforeDistance = preprocessorsBeforeDistance;
		this.distanceMeasureAbsoluteToRelative = distanceMeasureAbsoluteToRelative;
		this.preprocessorsAfterDistance = preprocessorsAfterDistance;
		this.similarityPrecision = similarityPrecision;
	}

	public SerializableConversionInputToStandardConfiguration(
			ConversionInputToStandardConfiguration config)
			throws RepositoryObjectSerializationException {
		super();
		this.config = config;
		this.preprocessorsBeforeDistance = new ArrayList<>();
		for (IDataPreprocessor p : config.preprocessorsBeforeDistance)
			this.preprocessorsBeforeDistance.add(p.asSerializable());
		this.distanceMeasureAbsoluteToRelative = config.distanceMeasureAbsoluteToRelative.asSerializable();
		this.preprocessorsAfterDistance = new ArrayList<>();
		for (IDataPreprocessor p : config.preprocessorsAfterDistance)
			this.preprocessorsAfterDistance.add(p.asSerializable());
		this.similarityPrecision = config.similarityPrecision;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.format.
	 * ISerializableConversionInputToStandardConfiguration#getWrappedComponent()
	 */
	@Override
	public IConversionInputToStandardConfiguration getWrappedComponent() {
		return config;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.SerializableWrapper#deserializeInternal
	 * (de.clusteval.framework.repository.IRepository)
	 */
	@Override
	public IConversionInputToStandardConfiguration deserialize(IRepository repository) throws DeserializationException {
		try {
			IDistanceMeasure distanceMeasure = distanceMeasureAbsoluteToRelative.deserialize(repository);
			List<IDataPreprocessor> preprocessorsBeforeDistance = new ArrayList<>();
			for (ISerializableWrapperRepositoryObject<IDataPreprocessor> w : this.preprocessorsBeforeDistance)
				preprocessorsBeforeDistance.add(w.deserialize(repository));
			List<IDataPreprocessor> preprocessorsAfterDistance = new ArrayList<>();
			for (ISerializableWrapperRepositoryObject<IDataPreprocessor> w : this.preprocessorsAfterDistance)
				preprocessorsAfterDistance.add(w.deserialize(repository));
			ConversionInputToStandardConfiguration result = new ConversionInputToStandardConfiguration(distanceMeasure,
					similarityPrecision, preprocessorsBeforeDistance, preprocessorsAfterDistance);
			return result;
		} catch (DeserializationException e) {
			throw e;
		} catch (Exception e) {
			throw new DeserializationException(this.toString(), e);
		}
	}
}