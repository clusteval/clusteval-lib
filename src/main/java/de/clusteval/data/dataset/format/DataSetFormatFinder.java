/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.data.dataset.format;

import java.io.File;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.DynamicComponentMissingVersionException;
import de.clusteval.utils.IncompatibleClustEvalVersionException;
import de.clusteval.utils.InvalidClustEvalVersionException;
import de.clusteval.utils.InvalidDependencyTargetException;
import de.clusteval.utils.JARFinder;
import de.clusteval.utils.JARFinderClassLoader;
import de.clusteval.utils.MissingClustEvalVersionException;

/**
 * @author Christian Wiwie
 */
public class DataSetFormatFinder extends JARFinder<IDataSetFormat>
		implements
			IDataSetFormatFinder {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8696481469993314272L;

	/**
	 * Instantiates a new data set format finder.
	 * 
	 * @param repository
	 *            the repository
	 * @throws RegisterException
	 */
	public DataSetFormatFinder(final IRepository repository)
			throws RegisterException {
		super(repository, IDataSetFormat.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.JARFinder#classNameForJARFile(java.io.File)
	 */
	@Override
	protected String[] classNamesForJARFile(File f) {
		return new String[]{
				String.format("%s%s", this.getClassNamePrefix(),
						f.getName().substring(0, f.getName().indexOf("-"))),
				String.format("%s%sParser", this.getClassNamePrefix(),
						f.getName().substring(0, f.getName().indexOf("-")))};
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.JARFinder#getURLClassLoader(java.io.File)
	 */
	@Override
	protected URLClassLoader getURLClassLoader0(File f,
			final ClassLoader parent) throws MalformedURLException {
		URL url = f.toURI().toURL();
		return new DataSetFormatURLClassLoader(this, new URL[]{url}, parent);
	}
}

class DataSetFormatURLClassLoader extends JARFinderClassLoader<IDataSetFormat> {

	/**
	 * @param urls
	 * @param parent
	 * @param loaderParent
	 */
	public DataSetFormatURLClassLoader(DataSetFormatFinder parent, URL[] urls,
			ClassLoader loaderParent) {
		super(parent, urls, loaderParent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.ClassLoader#loadClass(java.lang.String)
	 */
	@Override
	public Class<?> loadClass(String name) throws ClassNotFoundException {
		Class<?> result = super.loadClass(name, true);

		if (Modifier.isAbstract(result.getModifiers()) || result.isInterface())
			return result;

		if (name.startsWith("de.clusteval.data.dataset.format")) {
			if (name.endsWith("DataSetFormat")) {
				@SuppressWarnings("unchecked")
				Class<? extends DataSetFormat> dataSetFormat = (Class<? extends DataSetFormat>) result;

				// the class needs to have a version annotation
				try {
					// loadClass may have been called not for the newly
					// discovered class of this JAR, but for a parent class
					// which had already been registered before;
					// thus, we only register this class, if it hasn't been
					// registered before
					if (!this.parent.getRepository()
							.isClassRegistered(dataSetFormat)) {
						this.parent.getRepository().registerClass(
								this.parent.getClassToFind(), dataSetFormat);
					}
				} catch (DynamicComponentMissingVersionException
						| DynamicComponentInitializationException
						| IncompatibleClustEvalVersionException
						| MissingClustEvalVersionException
						| InvalidClustEvalVersionException
						| InvalidDependencyTargetException e) {
					handleException(e);
				}
			} else if (name.endsWith("DataSetFormatParser")) {
				@SuppressWarnings("unchecked")
				Class<? extends DataSetFormatParser> dataSetFormatParser = (Class<? extends DataSetFormatParser>) result;

				// the class needs to have a version annotation
				try {
					this.parent.getRepository()
							.registerDataSetFormatParser(dataSetFormatParser);
				} catch (DynamicComponentMissingVersionException
						| IncompatibleClustEvalVersionException
						| MissingClustEvalVersionException
						| InvalidClustEvalVersionException
						| InvalidDependencyTargetException e) {
					handleException(e);
				}

			}
		}
		return result;
	}
}
