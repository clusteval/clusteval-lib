/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.data.dataset.format;

import java.util.ArrayList;
import java.util.List;

import de.clusteval.data.distance.IDistanceMeasure;
import de.clusteval.data.preprocessing.IDataPreprocessor;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import dk.sdu.imada.compbio.utils.SimilarityMatrix.NUMBER_PRECISION;

/**
 * @author Christian Wiwie
 * 
 */
public class ConversionInputToStandardConfiguration extends ConversionConfiguration
		implements
			IConversionInputToStandardConfiguration {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1175992690012913930L;

	protected static List<IDataPreprocessor> clonePreprocessors(List<IDataPreprocessor> preprocessors) {
		List<IDataPreprocessor> result = new ArrayList<IDataPreprocessor>();

		for (IDataPreprocessor proc : preprocessors)
			result.add(proc.clone());

		return result;
	}

	protected transient List<IDataPreprocessor> preprocessorsBeforeDistance;

	protected transient IDistanceMeasure distanceMeasureAbsoluteToRelative;

	protected transient List<IDataPreprocessor> preprocessorsAfterDistance;

	protected NUMBER_PRECISION similarityPrecision;

	/**
	 * @param distanceMeasure
	 * @param preprocessorsBeforeDistance
	 * @param preprocessorsAfterDistance
	 * 
	 */
	public ConversionInputToStandardConfiguration(final IDistanceMeasure distanceMeasure,
			final NUMBER_PRECISION similarityPrecision, final List<IDataPreprocessor> preprocessorsBeforeDistance,
			final List<IDataPreprocessor> preprocessorsAfterDistance) {
		super();

		this.similarityPrecision = similarityPrecision;
		this.distanceMeasureAbsoluteToRelative = distanceMeasure;
		this.preprocessorsBeforeDistance = preprocessorsBeforeDistance;
		this.preprocessorsAfterDistance = preprocessorsAfterDistance;
	}

	/**
	 * The copy constructor for this class.
	 * 
	 * @param other
	 *            The object to clone.
	 */
	public ConversionInputToStandardConfiguration(final ConversionInputToStandardConfiguration other) {
		super();

		this.similarityPrecision = other.similarityPrecision;
		this.distanceMeasureAbsoluteToRelative = other.distanceMeasureAbsoluteToRelative.clone();
		this.preprocessorsBeforeDistance = clonePreprocessors(other.preprocessorsBeforeDistance);
		this.preprocessorsAfterDistance = clonePreprocessors(other.preprocessorsAfterDistance);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.dataset.format.IConversionInputToStandardConfiguration#
	 * clone()
	 */
	@Override
	public IConversionInputToStandardConfiguration clone() {
		return new ConversionInputToStandardConfiguration(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.dataset.format.IConversionInputToStandardConfiguration#
	 * getSimilarityPrecision()
	 */
	@Override
	public NUMBER_PRECISION getSimilarityPrecision() {
		return this.similarityPrecision;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.dataset.format.IConversionInputToStandardConfiguration#
	 * getDistanceMeasureAbsoluteToRelative()
	 */
	@Override
	public IDistanceMeasure getDistanceMeasureAbsoluteToRelative() {
		return this.distanceMeasureAbsoluteToRelative;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.dataset.format.IConversionInputToStandardConfiguration#
	 * getPreprocessorsBeforeDistance()
	 */
	@Override
	public List<IDataPreprocessor> getPreprocessorsBeforeDistance() {
		return this.preprocessorsBeforeDistance;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.dataset.format.IConversionInputToStandardConfiguration#
	 * getPreprocessorsAfterDistance()
	 */
	@Override
	public List<IDataPreprocessor> getPreprocessorsAfterDistance() {
		return this.preprocessorsAfterDistance;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.dataset.format.IConversionInputToStandardConfiguration#
	 * equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof ConversionInputToStandardConfiguration))
			return false;

		ConversionInputToStandardConfiguration other = (ConversionInputToStandardConfiguration) obj;

		return this.distanceMeasureAbsoluteToRelative.equals(other.distanceMeasureAbsoluteToRelative)
				&& this.similarityPrecision.equals(other.similarityPrecision);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.dataset.format.IConversionInputToStandardConfiguration#
	 * hashCode()
	 */
	@Override
	public int hashCode() {
		return (this.distanceMeasureAbsoluteToRelative.toString() + this.similarityPrecision.toString()).hashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.dataset.format.IConversionInputToStandardConfiguration#
	 * asSerializable()
	 */
	@Override
	public ISerializableConversionInputToStandardConfiguration asSerializable() throws RepositoryObjectSerializationException {
		return new SerializableConversionInputToStandardConfiguration(this);
	}
}
