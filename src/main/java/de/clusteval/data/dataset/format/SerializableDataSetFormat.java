/**
 * 
 */
package de.clusteval.data.dataset.format;

import java.io.File;

import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RepositoryObjectDeserializationException;
import de.clusteval.framework.repository.SerializableWrapperDynamicComponent;
import de.clusteval.utils.DynamicComponentInitializationException;

public class SerializableDataSetFormat
		extends
			SerializableWrapperDynamicComponent<IDataSetFormat>
		implements
			ISerializableDataSetFormat {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4254480025166338086L;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 */
	public SerializableDataSetFormat(File absPath, String name, String version,
			final String alias) {
		super(IDataSetFormat.class, absPath, name, version, alias);
	}

	/**
	 * @param wrappedComponent
	 */
	public SerializableDataSetFormat(IDataSetFormat wrappedComponent) {
		super(IDataSetFormat.class, wrappedComponent);
	}

	@Override
	protected IDataSetFormat deserializeInternal(final IRepository repository)
			throws DeserializationException {
		try {
			return DataSetFormat.parseFromString(repository, this.name);
		} catch (UnknownDataSetFormatException
				| DynamicComponentInitializationException e) {
			throw new RepositoryObjectDeserializationException(
					IDataSetFormat.class, this.name, e);
		}
	}
}