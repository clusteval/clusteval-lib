/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
package de.clusteval.data.dataset.format;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.maven.artifact.versioning.ComparableVersion;
import org.slf4j.LoggerFactory;

import de.clusteval.data.dataset.DataSet;
import de.clusteval.data.dataset.IDataSet;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.Repository;
import de.clusteval.framework.repository.RepositoryObjectDynamicComponent;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.utils.ClustEvalAlias;
import de.clusteval.utils.ClassVersion;
import de.clusteval.utils.ClassVersionRequirement;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.DynamicComponentVersion;
import dk.sdu.imada.compbio.file.FileUtils;
import dk.sdu.imada.compbio.utils.Pair;
import dk.sdu.imada.compbio.utils.SimilarityMatrix.NUMBER_PRECISION;

//@formatter:off
/**
 * Datasets can have different formats. For all kinds of operations the
 * framework needs to know which format a dataset has and how it can be
 * converted to an understandable (standard) format.
 * 
 * <p>
 * Every dataset format comes together with a parser class (see
 * {@link DataSetFormatParser}).
 * 
 * A data set format `MyDataSetFormat` can be added to ClustEval by
 * 
 * 1. extending the class {@link DataSetFormat} with your own class `MyDataSetFormat`.
 *     - Your class has to be decorated with the following annotations:
 *         - {@link DynamicComponentVersion}: Any component you add to ClustEval needs to have a version. This is needed to ensure, that results are fully reproducible by running runs using exactly the same versions as previously
 *         - {@link ClassVersionRequirement}: Any component you add to ClustEval needs to specify, which dependencies it has on the ClustEval interfaces. This information is used to ensure compatibility between the version of your component and the used ClustEval version. Whenever core interfaces in the ClustEval API change, their interface version is incremented. By using this annotation it can detect, which components are still compatible.
 *         - {@link ClustEvalAlias}: You need to provide a readable alias for your component. This alias is used when results and components should be presented in a accessible way.
 *     - You have to provide your own implementations for the following methods, otherwise the framework will not be able to load your dataset format.
 *         * {@link #DataSetFormat(IRepository, long, File)}: The constructor of your dataset format class. This constructor has to be implemented and public, otherwise the framework will not be able to load your dataset format.
 *         * {@link #DataSetFormat(DataSetFormat)}: The copy constructor of your class taking another instance of your class. This constructor has to be implemented and public.
 *         * {@link #getDataSetFormatParser()}: This method has to return an instance of the parser corresponding to this format.
 *         * [Optional] {@link #getNumberAdditionalFiles(IDataSet)}: If your format describes data sets consisting of multiple files, this method has to return the number of additional files. For instance, if your format corresponds to two files, a primary and one additional file, this method has to return 1.
 * 2. extending the class {@link DataSetFormatParser} with your own class `MyDataSetFormatParser`.
 *     - Your class has to be decorated with the following annotations:
 *         - {@link DynamicComponentVersion}: Any component you add to ClustEval needs to have a version. This is needed to ensure, that results are fully reproducible by running runs using exactly the same versions as previously
 *         - {@link ClassVersionRequirement}: Any component you add to ClustEval needs to specify, which dependencies it has on the ClustEval interfaces. This information is used to ensure compatibility between the version of your component and the used ClustEval version. Whenever core interfaces in the ClustEval API change, their interface version is incremented. By using this annotation it can detect, which components are still compatible.
 *     - You have to provide your own implementations for the following methods, otherwise the framework will not be able to load your class.
 *         * {@link #convertToStandardFormat(IDataSet, IConversionInputToStandardConfiguration)}: This method converts the given dataset to the standard input format of the framework using the given conversion configuration. This assumes, that the passed dataset has this format.
 *         * {@link #convertToThisFormat(IDataSet, IDataSetFormat, ConversionConfiguration)}: This method converts the given dataset to the given input format using the conversion configuration.
 *         * {@link #parse(IDataSet, NUMBER_PRECISION)}: This method parses the given dataset and returns an object, wrapping the contents of the dataset (e.g. an instance of SimilarityMatrix or DataMatrix ).
 *         * {@link #writeToFile(IDataSet, boolean)}: This method is invoked to write the passed data set object to a file.
 * 3. Creating a jar file named `MyDataSetFormat.jar` containing the `MyDataSetFormat.class` and `MyDataSetFormatParser.class` compiled on your machine in the correct folder structure corresponding to the packages:
 *     - `de/clusteval/data/dataset/format/MyDataSetFormat.class`
 *     * `de/clusteval/data/dataset/format/MyDataSetFormatParser.class`
 * 4. Putting the `MyDataSetFormat.jar` into the dataset formats folder of the repository: `<REPOSITORY ROOT>/supp/formats/dataset`
 * 
 * 
 * The backend server will recognize and try to load the new dataset format automatically the next time, the {@link DataSetFormatFinderThread} checks the filesystem.
 * 
 * 
 * @author Christian Wiwie
 * 
 * <dl>
 * 	<dt>2.1</dt><dd>Using compbio utils instead of Wiutils</dd>
 * 	<dt>&#60; 2.1</dt><dd>...</dd>
 * </dl>
 */
//@formatter:on
@ClassVersion(version = "2.1")
public abstract class DataSetFormat extends RepositoryObjectDynamicComponent
		implements
			IDataSetFormat {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8842487000210767860L;

	/**
	 * This method returns a deep copy of the given list of dataset formats,
	 * i.e. the objects of the list are also cloned.
	 * 
	 * @param dataSetFormats
	 *            The list of dataset formats to clone.
	 * @return The cloned list of dataset formats.
	 */
	public static List<IDataSetFormat> cloneDataSetFormats(
			final List<IDataSetFormat> dataSetFormats) {
		List<IDataSetFormat> result = new ArrayList<IDataSetFormat>();

		for (IDataSetFormat dataSetFormat : dataSetFormats)
			result.add(dataSetFormat.clone());

		return result;
	}

	/**
	 * A boolean indicating, whether the dataset format is normalized.
	 */
	private boolean normalized;

	/**
	 * This method parses a dataset format from the given string, containing a
	 * dataset format class name and a given dataset format version.
	 * 
	 * @param repository
	 *            The repository where to look up the dataset format class.
	 * @param datasetFormat
	 *            The dataset format class name as string.
	 * @return The parsed dataset format.
	 * @throws UnknownDataSetFormatException
	 * @throws DynamicComponentInitializationException
	 */
	public static IDataSetFormat parseFromString(final IRepository repository,
			String datasetFormat) throws UnknownDataSetFormatException,
			DynamicComponentInitializationException {
		Class<? extends IDataSetFormat> c = repository
				.resolveAndGetRegisteredClass(IDataSetFormat.class,
						datasetFormat, true);
		if (c == null) {
			Pair<String, ComparableVersion> fullClassNameAndVersion = repository
					.getFullClassNameAndVersion(IDataSetFormat.class,
							datasetFormat, true);
			if (fullClassNameAndVersion.getSecond() != null
					&& repository.hasError(IDataSetFormat.class,
							fullClassNameAndVersion.getFirst(),
							fullClassNameAndVersion.getSecond().toString()))
				throw new UnknownDataSetFormatException(datasetFormat,
						repository.getErrors(IDataSetFormat.class,
								fullClassNameAndVersion.getFirst(),
								fullClassNameAndVersion.getSecond().toString())
								.iterator().next());
			else
				throw new UnknownDataSetFormatException(datasetFormat,
						"Unknown dataset format");
		}
		return getInstance(repository, datasetFormat, c);
	}

	protected static IDataSetFormat getInstance(final IRepository repository,
			String datasetFormat, Class<? extends IDataSetFormat> c)
			throws UnknownDataSetFormatException,
			DynamicComponentInitializationException {
		try {
			Constructor<? extends IDataSetFormat> constr = c
					.getConstructor(IRepository.class, long.class, File.class);
			// changed 21.03.2013: do not register dataset formats here
			return constr.newInstance(repository, System.currentTimeMillis(),
					new File(datasetFormat));
		} catch (NoSuchMethodException e) {
			throw new DynamicComponentInitializationException(
					IDataSetFormat.class, datasetFormat,
					"The class is missing the correct constructor");
		} catch (Exception e) {
			// this is likely because we passed faked parameters
			return null;
		}
	}

	@Override
	public int getNumberAdditionalFiles(final IDataSet dataSet) {
		return 0;
	}

	/**
	 * This method parses several dataset formats from a string array.
	 * 
	 * <p>
	 * This is a convenience method for
	 * {@link #parseFromString(Repository, String)}.
	 * 
	 * @param repo
	 *            the repo
	 * @param datasetFormats
	 *            the dataset formats
	 * @return the list
	 * @throws UnknownDataSetFormatException
	 *             the unknown data set format exception
	 * @throws DynamicComponentInitializationException
	 */
	public static List<IDataSetFormat> parseFromString(final IRepository repo,
			String[] datasetFormats) throws UnknownDataSetFormatException,
			DynamicComponentInitializationException {
		List<IDataSetFormat> result = new LinkedList<IDataSetFormat>();
		for (String dsFormat : datasetFormats) {
			result.add(parseFromString(repo, dsFormat));
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.dataset.format.IDataSetFormat#parse(de.clusteval.data.
	 * dataset.DataSet,
	 * de.wiwie.wiutils.utils.SimilarityMatrix.NUMBER_PRECISION)
	 */
	@Override
	public Object parse(final IDataSet dataSet, NUMBER_PRECISION precision)
			throws IllegalArgumentException, IOException,
			InvalidDataSetFormatVersionException {
		final DataSetFormatParser parser = getDataSetFormatParser();
		if (parser == null)
			throw new IllegalArgumentException(
					"Operation only supported for the standard dataset format");
		return parser.parse(dataSet, precision);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.dataset.format.IDataSetFormat#writeToFile(de.clusteval.
	 * data.dataset.DataSet, boolean)
	 */
	@Override
	public boolean writeToFile(final IDataSet dataSet,
			final boolean withHeader) {
		final DataSetFormatParser parser = getDataSetFormatParser();
		if (parser == null)
			throw new IllegalArgumentException(
					"Operation only supported for the standard dataset format");
		return parser.writeToFile(dataSet, withHeader);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.dataset.format.IDataSetFormat#convertToStandardFormat(
	 * de.clusteval.data.dataset.DataSet,
	 * de.clusteval.data.dataset.format.ConversionInputToStandardConfiguration)
	 */
	@Override
	public final DataSet convertToStandardFormat(IDataSet dataSet,
			IConversionInputToStandardConfiguration config)
			throws DataSetConversionException {
		DataSet res = null;
		try {
			final DataSetFormatParser parser = getDataSetFormatParser();
			if (parser == null)
				throw new IllegalArgumentException(
						"Operation only supported for the standard dataset format");
			res = parser.convertToStandardFormat(dataSet, config);
		} catch (Exception e) {
			throw new DataSetConversionException(e);
		}
		if (res == null)
			throw new DataSetConversionException(String.format(
					"The conversion of data set '%s' from format '%s' to the standard format failed.",
					dataSet.toString(),
					dataSet.getDataSetFormat().getClass().getSimpleName()));
		return res;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.dataset.format.IDataSetFormat#convertToThisFormat(de.
	 * clusteval.data.dataset.DataSet,
	 * de.clusteval.data.dataset.format.IDataSetFormat,
	 * de.clusteval.data.dataset.format.ConversionConfiguration)
	 */
	@Override
	public final IDataSet convertToThisFormat(IDataSet dataSet,
			IDataSetFormat dataSetFormat, ConversionConfiguration config)
			throws DataSetConversionException {
		DataSet res = null;
		try {
			final DataSetFormatParser parser = getDataSetFormatParser();
			if (parser == null)
				throw new IllegalArgumentException(
						"Operation only supported for the standard dataset format");
			res = parser.convertToThisFormat(dataSet, dataSetFormat, config);
		} catch (Exception e) {
			throw new DataSetConversionException(e);
		}
		if (res == null)
			throw new DataSetConversionException(String.format(
					"The conversion of data set '%s' from format '%s' to the standard format failed.",
					dataSet.getDataSetFormat().getName(), dataSet.toString()));
		return res;
	}

	/**
	 * 
	 * @return An instance of the dataset format parser corresponding to this
	 *         dataset format class.
	 */
	protected abstract DataSetFormatParser getDataSetFormatParser();

	/**
	 * Instantiates a new dataset format with the given version.
	 * 
	 * @param repo
	 * @param changeDate
	 * @param absPath
	 */
	public DataSetFormat(final IRepository repo, final long changeDate,
			final File absPath) {
		super(repo, changeDate, absPath);
		this.log = LoggerFactory.getLogger(this.getClass());
	}

	/**
	 * The copy constructor of dataset formats.
	 * 
	 * @param other
	 *            The object to clone.
	 */
	public DataSetFormat(final DataSetFormat other) {
		super(other);

		this.normalized = other.normalized;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.dataset.format.IDataSetFormat#setNormalized(boolean)
	 */
	@Override
	public void setNormalized(final boolean normalized) {
		this.normalized = normalized;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.format.IDataSetFormat#getNormalized()
	 */
	@Override
	public boolean getNormalized() {
		return this.normalized;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.format.IDataSetFormat#getVersion()
	 */
	@Override
	public ComparableVersion getVersion() {
		try {
			return Repository.getVersionOfDynamicClass(this.getClass());
		} catch (Exception e) {
			// cannot happen because we loaded the class before;
			e.printStackTrace();
			return new ComparableVersion("1");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#getName()
	 */
	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.dataset.format.IDataSetFormat#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof DataSetFormat))
			return false;

		DataSetFormat other = (DataSetFormat) o;
		return (other.getClass().getSimpleName()
				.equals(this.getClass().getSimpleName())
				&& this.normalized == other.normalized
				&& this.getVersion().equals(other.getVersion()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.format.IDataSetFormat#clone()
	 */
	@Override
	public final DataSetFormat clone() {
		try {
			return this.getClass().getConstructor(this.getClass())
					.newInstance(this);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		this.log.warn("Cloning instance of class "
				+ this.getClass().getSimpleName() + " failed");
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.format.IDataSetFormat#hashCode()
	 */
	@Override
	public int hashCode() {
		return (this.getClass().toString() + this.normalized
				+ this.getVersion()).hashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.format.IDataSetFormat#copyDataSetTo(de.
	 * clusteval.data.dataset.DataSet, java.io.File, boolean)
	 */
	@Override
	public boolean copyDataSetTo(final IDataSet dataSet,
			final File copyDestination, final boolean overwrite) {
		try {
			if (!copyDestination.exists() || overwrite)
				org.apache.commons.io.FileUtils.copyFile(
						new File(dataSet.getAbsolutePath()), copyDestination);

			try {
				for (int i = 0; i < this
						.getNumberAdditionalFiles(dataSet); i++) {
					org.apache.commons.io.FileUtils.copyFile(
							new File(dataSet.getAbsolutePath() + ".supp"
									+ (i + 1)),
							new File(copyDestination.getAbsolutePath() + ".supp"
									+ (i + 1)));
				}
			} catch (IOException e) {
				return false;
			}

		} catch (IOException e) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.format.IDataSetFormat#moveDataSetTo(de.
	 * clusteval.data.dataset.DataSet, java.io.File, boolean)
	 */
	@Override
	public boolean moveDataSetTo(final IDataSet dataSet,
			final File moveDestination, final boolean overwrite) {
		try {
			if (!moveDestination.exists() || overwrite)
				org.apache.commons.io.FileUtils.moveFile(
						new File(dataSet.getAbsolutePath()), moveDestination);
		} catch (IOException e) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.dataset.format.IDataSetFormat#copyDataSetToFolder(de.
	 * clusteval.data.dataset.DataSet, java.io.File, boolean)
	 */
	@Override
	public boolean copyDataSetToFolder(final IDataSet dataSet,
			final File copyFolderDestination, final boolean overwrite) {
		try {
			File targetFile = new File(
					FileUtils.buildPath(copyFolderDestination.getAbsolutePath(),
							new File(dataSet.getAbsolutePath()).getName()));
			if (!targetFile.exists() || overwrite)
				org.apache.commons.io.FileUtils.copyFile(
						new File(dataSet.getAbsolutePath()), targetFile);

			try {
				for (int i = 0; i < this
						.getNumberAdditionalFiles(dataSet); i++) {

					targetFile = new File(FileUtils.buildPath(
							copyFolderDestination.getAbsolutePath(),
							new File(dataSet.getAbsolutePath()).getName()
									+ ".supp" + (i + 1)));

					org.apache.commons.io.FileUtils.copyFile(new File(
							dataSet.getAbsolutePath() + ".supp" + (i + 1)),
							targetFile);
				}
			} catch (IOException e) {
				return false;
			}

		} catch (

		IOException e) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObjectDynamicComponent#
	 * asSerializable()
	 */
	@Override
	public SerializableDataSetFormat asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableDataSetFormat) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#asSerializable()
	 */
	@Override
	public SerializableDataSetFormat asSerializableInternal() {
		return new SerializableDataSetFormat(this);
	}
}
