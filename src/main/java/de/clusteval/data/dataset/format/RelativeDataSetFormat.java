/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
package de.clusteval.data.dataset.format;

import java.io.File;
import java.io.IOException;

import de.clusteval.data.dataset.IDataSet;
import de.clusteval.framework.repository.IRepository;
import dk.sdu.imada.compbio.utils.SimilarityMatrix;
import dk.sdu.imada.compbio.utils.SimilarityMatrix.NUMBER_PRECISION;

/**
 * @author Christian Wiwie
 * 
 */
public abstract class RelativeDataSetFormat extends DataSetFormat
		implements
			IRelativeDataSetFormat {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8078695808414055010L;

	/**
	 * Instantiates a new relative data set format.
	 * 
	 * @param repo
	 * @param changeDate
	 * @param absPath
	 * 
	 */
	public RelativeDataSetFormat(final IRepository repo, final long changeDate,
			final File absPath) {
		super(repo, changeDate, absPath);
	}

	/**
	 * The copy constructor for this format.
	 * 
	 * @param other
	 *            The object to clone.
	 */
	public RelativeDataSetFormat(final RelativeDataSetFormat other) {
		super(other);
	}

	@Override
	public final SimilarityMatrix parse(final IDataSet dataSet,
			NUMBER_PRECISION precision) throws IllegalArgumentException,
			IOException, InvalidDataSetFormatVersionException {
		return (SimilarityMatrix) super.parse(dataSet, precision);
	}
}
