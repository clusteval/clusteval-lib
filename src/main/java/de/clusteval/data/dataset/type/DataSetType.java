/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.data.dataset.type;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.List;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.data.dataset.format.DataSetFormat;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.Repository;
import de.clusteval.framework.repository.RepositoryObjectDynamicComponent;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.SerializableWrapperDynamicComponent;
import de.clusteval.utils.ClustEvalAlias;
import de.clusteval.utils.ClassVersion;
import de.clusteval.utils.ClassVersionRequirement;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.DynamicComponentVersion;
import dk.sdu.imada.compbio.utils.Pair;

/**
 * Dataset types are used to classify datasets into different thematic groups.
 * 
 * 
 * ## Writing Data Set Types
 * 
 * Data set types can be added to ClustEval by
 * 
 * 1. extending the {@link DataSetFormat} class with your own class
 * `MyDataSetType`. - Your class has to be decorated with the following
 * annotations: - {@link DynamicComponentVersion}: Any component you add to
 * ClustEval needs to have a version. This is needed to ensure, that results are
 * fully reproducible by running runs using exactly the same versions as
 * previously - {@link ClassVersionRequirement}: Any component you add to
 * ClustEval needs to specify, which dependencies it has on the ClustEval
 * interfaces. This information is used to ensure compatibility between the
 * version of your component and the used ClustEval version. Whenever core
 * interfaces in the ClustEval API change, their interface version is
 * incremented. By using this annotation it can detect, which components are
 * still compatible. - {@link ClustEvalAlias}: You need to provide a readable
 * alias for your component. This alias is used when results and components
 * should be presented in a accessible way. - You have to provide your own
 * implementations for the following methods, otherwise the framework will not
 * be able to load your class. * {@link #DataSetType(IRepository, long, File)}:
 * The constructor of your class. This constructor has to be implemented and
 * public, otherwise the framework will not be able to load your class. *
 * {@link #DataSetType(DataSetType)}: The copy constructor of your class taking
 * another instance of your class. This constructor has to be im- plemented and
 * public. 2. Creating a jar file named MyDataSetType.jar containing the
 * MyDataSetType.class compiled on your machine in the correct folder structure
 * corresponding to the packages: *
 * de/clusteval/data/dataset/type/MyDataSetType.class 3. Putting the
 * `MyDataSetType.jar` into the dataset types folder of the repository:
 * `<REPOSITORY ROOT>/supp/types/dataset`
 * 
 * 
 * The backend server will recognize and try to load the new dataset type
 * automatically the next time, the {@link DataSetTypeFinderThread} checks the
 * filesystem.
 * 
 * @author Christian Wiwie
 * 
 */
@ClassVersion(version = "1")
public abstract class DataSetType extends RepositoryObjectDynamicComponent
		implements
			IDataSetType {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5796594101611015664L;

	/**
	 * @param repository
	 * @param changeDate
	 * @param absPath
	 * @throws RegisterException
	 * 
	 */
	public DataSetType(final IRepository repository, final long changeDate,
			final File absPath) throws RegisterException {
		super(repository, changeDate, absPath);
	}

	/**
	 * The copy constructor for dataset types.
	 * 
	 * @param other
	 *            The object to clone.
	 * @throws RegisterException
	 */
	public DataSetType(final DataSetType other) throws RegisterException {
		super(other);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.repository.RepositoryObject#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.type.IDataSetType#clone()
	 */
	@Override
	public final DataSetType clone() {
		try {
			return this.getClass().getConstructor(this.getClass())
					.newInstance(this);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		this.log.warn("Cloning instance of class "
				+ this.getClass().getSimpleName() + " failed");
		return null;
	}

	/**
	 * Parses the from string.
	 * 
	 * @param repository
	 *            the repository
	 * @param datasetType
	 *            the dataset type
	 * @return the data set format
	 * @throws UnknownDataSetTypeException
	 *             the unknown data set type exception
	 * @throws DynamicComponentInitializationException
	 */
	public static IDataSetType parseFromString(final IRepository repository,
			String datasetType) throws UnknownDataSetTypeException,
			DynamicComponentInitializationException {
		Class<? extends IDataSetType> c = repository
				.resolveAndGetRegisteredClass(IDataSetType.class, datasetType,
						true);
		if (c == null) {
			Pair<String, ComparableVersion> fullClassNameAndVersion = repository
					.getFullClassNameAndVersion(IDataSetType.class, datasetType,
							true);
			if (fullClassNameAndVersion.getSecond() != null
					&& repository.hasError(IDataSetType.class,
							fullClassNameAndVersion.getFirst(),
							fullClassNameAndVersion.getSecond().toString()))
				throw new UnknownDataSetTypeException(datasetType,
						repository.getErrors(IDataSetType.class,
								fullClassNameAndVersion.getFirst(),
								fullClassNameAndVersion.getSecond().toString())
								.iterator().next());
			else
				throw new UnknownDataSetTypeException(datasetType,
						"Unknown dataset type");
		}
		return getInstance(repository, datasetType, c);
	}

	protected static IDataSetType getInstance(final IRepository repository,
			String datasetType, Class<? extends IDataSetType> c)
			throws UnknownDataSetTypeException,
			DynamicComponentInitializationException {
		try {
			Constructor<? extends IDataSetType> constr = c
					.getConstructor(IRepository.class, long.class, File.class);
			// changed 21.03.2013: do not register dataset types here
			return constr.newInstance(repository, System.currentTimeMillis(),
					new File(datasetType));
		} catch (NoSuchMethodException e) {
			throw new DynamicComponentInitializationException(
					IDataSetType.class, datasetType,
					"The class is missing the correct constructor");
		} catch (Exception e) {
			// this is likely because we passed faked parameters
			return null;
		}
	}

	/**
	 * Parses the from string.
	 * 
	 * @param repo
	 *            the repo
	 * @param datasetTypes
	 *            the dataset Types
	 * @return the list
	 * @throws UnknownDataSetTypeException
	 *             the unknown data set type exception
	 * @throws DynamicComponentInitializationException
	 */
	public static List<IDataSetType> parseFromString(final IRepository repo,
			String[] datasetTypes) throws UnknownDataSetTypeException,
			DynamicComponentInitializationException {
		List<IDataSetType> result = new LinkedList<IDataSetType>();
		for (String dsType : datasetTypes) {
			result.add(parseFromString(repo, dsType));
		}
		return result;
	}

	@Override
	public ComparableVersion getVersion() {
		try {
			return Repository.getVersionOfObjectDynamicClass(this);
		} catch (Exception e) {
			// cannot happen because we loaded the class before;
			e.printStackTrace();
			return new ComparableVersion("1");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#getName()
	 */
	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObjectDynamicComponent#
	 * asSerializable()
	 */
	@Override
	public SerializableWrapperDynamicComponent<IDataSetType> asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableWrapperDynamicComponent<IDataSetType>) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObjectDynamicComponent#
	 * asSerializable()
	 */
	@Override
	public SerializableWrapperDynamicComponent<IDataSetType> asSerializableInternal() {
		return new SerializableDataSetType(this);
	}
}
