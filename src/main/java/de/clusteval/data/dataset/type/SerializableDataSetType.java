/**
 * 
 */
package de.clusteval.data.dataset.type;

import java.io.File;

import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RepositoryObjectDeserializationException;
import de.clusteval.framework.repository.SerializableWrapperDynamicComponent;
import de.clusteval.utils.DynamicComponentInitializationException;

public class SerializableDataSetType
		extends
			SerializableWrapperDynamicComponent<IDataSetType> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6585459374318350351L;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 */
	public SerializableDataSetType(File absPath, String name, String version,
			final String alias) {
		super(IDataSetType.class, absPath, name, version, alias);
	}

	/**
	 * @param wrappedComponent
	 */
	public SerializableDataSetType(IDataSetType wrappedComponent) {
		super(IDataSetType.class, wrappedComponent);
	}

	@Override
	protected IDataSetType deserializeInternal(final IRepository repository)
			throws DeserializationException {
		try {
			return DataSetType.parseFromString(repository, this.name);
		} catch (UnknownDataSetTypeException
				| DynamicComponentInitializationException e) {
			throw new RepositoryObjectDeserializationException(
					IDataSetType.class, this.name, e);
		}
	}
}