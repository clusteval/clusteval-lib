/**
 * 
 */
package de.clusteval.data.dataset;

import java.io.File;

import de.clusteval.data.dataset.format.ConversionStandardToInputConfiguration;
import de.clusteval.data.dataset.format.ISerializableConversionInputToStandardConfiguration;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.SerializableWrapperRepositoryObject;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * @author Christian Wiwie
 *
 */
public class SerializableDataSetConfig
		extends
			SerializableWrapperRepositoryObject<IDataSetConfig>
		implements
			ISerializableDataSetConfig {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5035853482710767972L;
	protected ISerializableConversionInputToStandardConfiguration configInputToStandard;
	protected ISerializableDataSet<IDataSet> dataSet;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 * @param configInputToStandard
	 * @param dataSet
	 */
	public SerializableDataSetConfig(File absPath, String name, String version,
			ISerializableConversionInputToStandardConfiguration configInputToStandard,
			ISerializableDataSet dataSet) {
		super(absPath, name, version);
		this.configInputToStandard = configInputToStandard;
		this.dataSet = dataSet;
	}

	public SerializableDataSetConfig(final IDataSetConfig dataSetConfig)
			throws RepositoryObjectSerializationException {
		super(dataSetConfig);
		if (dataSetConfig.getConversionInputToStandardConfiguration() != null)
			this.configInputToStandard = dataSetConfig
					.getConversionInputToStandardConfiguration()
					.asSerializable();
		if (dataSetConfig.getDataSet() != null)
			this.dataSet = (ISerializableDataSet) dataSetConfig.getDataSet()
					.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.ISerializableDataSetConfig#
	 * getConfigInputToStandard()
	 */
	@Override
	public ISerializableConversionInputToStandardConfiguration getConfigInputToStandard() {
		return configInputToStandard;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.ISerializableDataSetConfig#getDataSet()
	 */
	@Override
	public ISerializableDataSet getDataSet() {
		return dataSet;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.ISerializableDataSetConfig#
	 * getConversionStandardToInputConfiguration()
	 */
	@Override
	public ConversionStandardToInputConfiguration getConversionStandardToInputConfiguration() {
		return this.wrappedComponent
				.getConversionStandardToInputConfiguration();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.SerializableWrapper#deserializeInternal
	 * (de.clusteval.framework.repository.IRepository)
	 */
	@Override
	protected IDataSetConfig deserializeInternal(IRepository repository)
			throws DeserializationException {
		try {
			return repository.getStaticObjectWithNameAndVersion(
					IDataSetConfig.class, name + ":" + version, true);
		} catch (ObjectNotRegisteredException
				| ObjectVersionNotRegisteredException e) {
		}
		return new DataSetConfig(repository, System.currentTimeMillis(),
				new File(FileUtils.buildPath(
						repository.getBasePath(IDataSetConfig.class),
						this.name + ".v" + version + ".dsconfig")),
				dataSet.deserialize(repository),
				configInputToStandard.deserialize(repository),
				new ConversionStandardToInputConfiguration());
	}
}