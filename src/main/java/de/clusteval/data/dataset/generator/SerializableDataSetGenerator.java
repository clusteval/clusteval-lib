/**
 * 
 */
package de.clusteval.data.dataset.generator;

import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RepositoryObjectDeserializationException;
import de.clusteval.framework.repository.SerializableWrapperDynamicComponent;
import de.clusteval.utils.DynamicComponentInitializationException;

public class SerializableDataSetGenerator extends SerializableWrapperDynamicComponent<IDataSetGenerator> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7518060445393642490L;

	/**
	 * @param wrappedComponent
	 */
	public SerializableDataSetGenerator(IDataSetGenerator wrappedComponent) {
		super(IDataSetGenerator.class, wrappedComponent);
	}

	@Override
	protected IDataSetGenerator deserializeInternal(final IRepository repository) throws DeserializationException {
		try {
			return DataSetGenerator.parseFromString(repository, this.name);
		} catch (UnknownDataSetGeneratorException | DynamicComponentInitializationException e) {
			throw new RepositoryObjectDeserializationException(IDataSetGenerator.class, this.name, e);
		}
	}
}