/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.data.dataset.generator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.AbsoluteDataSet;
import de.clusteval.data.dataset.AbstractDataSetProvider;
import de.clusteval.data.dataset.DataSet;
import de.clusteval.data.dataset.IDataSet;
import de.clusteval.data.dataset.IDataSet.WEBSITE_VISIBILITY;
import de.clusteval.data.dataset.format.AbsoluteDataSetFormat;
import de.clusteval.data.dataset.format.DataSetFormat;
import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.dataset.type.DataSetType;
import de.clusteval.data.dataset.type.UnknownDataSetTypeException;
import de.clusteval.data.distance.UnknownDistanceMeasureException;
import de.clusteval.data.goldstandard.GoldStandard;
import de.clusteval.data.goldstandard.IGoldStandard;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.IRepositoryObjectDynamicComponent;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.Repository;
import de.clusteval.framework.repository.RepositoryObjectDumpException;
import de.clusteval.framework.repository.SerializableWrapperDynamicComponent;
import de.clusteval.utils.ClustEvalAlias;
import de.clusteval.utils.ClassVersion;
import de.clusteval.utils.ClassVersionRequirement;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.DynamicComponentVersion;
import dk.sdu.imada.compbio.file.FileUtils;

//@formatter:off
/**
 * 
 * A data set generator `MyDataSetGenerator` can be added to ClustEval by
 * 
 * 1. extending the class {@link DataSetGenerator} with your own class `MyDataSetGenerator`.
 *     - Your class has to be decorated with the following annotations:
 *         - {@link DynamicComponentVersion}: Any component you add to ClustEval needs to have a version. This is needed to ensure, that results are fully reproducible by running runs using exactly the same versions as previously
 *         - {@link ClassVersionRequirement}: Any component you add to ClustEval needs to specify, which dependencies it has on the ClustEval interfaces. This information is used to ensure compatibility between the version of your component and the used ClustEval version. Whenever core interfaces in the ClustEval API change, their interface version is incremented. By using this annotation it can detect, which components are still compatible.
 *         - {@link ClustEvalAlias}: You need to provide a readable alias for your component. This alias is used when results and components should be presented in a accessible way.
 *     - You have to provide your own implementations for the following methods, otherwise the framework will not be able to load your runresult format.
 *         * {@link #DataSetGenerator(IRepository, long, File)}: The constructor of your class. This constructor has to be implemented and public, otherwise the framework will not be able to load your runresult format.
 *         * {@link #DataSetGenerator(DataSetGenerator)}: The copy constructor of your class taking another instance of your class. This constructor has to be implemented and public.
 *         * {@link #generateDataSet()}: This method generates the data set, writes it to the file system and returns a DataSet wrapper object.
 *         * {@link #generatesGoldStandard()}: Returns true, if this generator generates a gold standard together with each generated data set.
 *         * {@link #generateGoldStandard()}: If generatesGoldStandard()` returns true, this method generates a gold standard for the generated data set, writes it to the file system and returns a GoldStandard wrapper object.
 *         * {@link #getOptions()}: This method returns an Options` object that encapsulates all parameters that this generator has. These can be set by the user in the client.
 *         * {@link #handleOptions(CommandLine)}: This method handles the values that the user set for the parameters specified in getOptions()`.
 * 2. Creating a jar file named `MyDataSetGenerator.jar` containing the `MyDataSetGenerator.class` compiled on your machine in the correct folder structure corresponding to the packages: `de/clusteval/data/dataset/generator/MyDataSetGenerator.class`
 * 3. Putting the `MyDataSetGenerator.jar` into the corresponding folder of the repository: `<REPOSITORY ROOT>/supp/generators`
 * 
 * 
 * The backend server will recognize and try to load the new class automatically the next time, the {@link DataSetGeneratorFinderThread} checks the filesystem.
 * 
 * @author Christian Wiwie
 * 
 */
//@formatter:on
@ClassVersion(version = "1")
public abstract class DataSetGenerator extends AbstractDataSetProvider
		implements
			IDataSetGenerator {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4426033657614200901L;

	/**
	 * This attribute corresponds to the name of the folder located in
	 * {@link Repository#getDataSetBasePath()}, in which the dataset (and
	 * goldstandard) will be stored.
	 */
	private String folderName;

	/**
	 * This attribute corresponds to the name of the dataset file, in which the
	 * generated dataset (and optionally goldstandard) will be stored within the
	 * {@link #folderName}.
	 */
	private String fileName;

	/**
	 * The alias of the data set that is to be generated.
	 */
	private String alias;

	/**
	 * Temp variable to hold the generated data set.
	 */
	protected double[][] coords;

	/**
	 * Temp variable for the goldstandard classes.
	 */
	protected int[] classes;

	/**
	 * @param repository
	 * @param changeDate
	 * @param absPath
	 * @throws RegisterException
	 */
	public DataSetGenerator(IRepository repository, long changeDate,
			File absPath) throws RegisterException {
		super(repository, changeDate, absPath);
	}

	/**
	 * The copy constructor of dataset generators.
	 * 
	 * @param other
	 *            The object to clone.
	 * @throws RegisterException
	 */
	public DataSetGenerator(DataSetGenerator other) throws RegisterException {
		super(other);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.repository.RepositoryObject#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.generator.IDataSetGenerator#clone()
	 */
	@Override
	public IRepositoryObject clone() {
		try {
			return this.getClass().getConstructor(this.getClass())
					.newInstance(this);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		this.log.warn("Cloning instance of class "
				+ this.getClass().getSimpleName() + " failed");
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.dataset.generator.IDataSetGenerator#getAllOptions()
	 */
	@Override
	public Options getAllOptions() {
		// options of actual generator implementation
		Options options = this.getOptions();

		// default options of all generators
		this.addDefaultOptions(options);
		return options;
	}

	/**
	 * @return A wrapper object keeping the options of your dataset generator.
	 *         The options returned by this method are going to be used and
	 *         interpreted in your subclass implementation in
	 *         {@link #generateDataSet()} .
	 */
	protected abstract Options getOptions();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.generator.IDataSetGenerator#
	 * generatesGoldStandard()
	 */
	@Override
	public abstract boolean generatesGoldStandard();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.dataset.generator.IDataSetGenerator#generate(java.lang.
	 * String[])
	 */
	@Override
	public IDataSet generate(final String[] cliArguments) throws ParseException,
			DataSetGenerationException, GoldStandardGenerationException,
			InterruptedException, RepositoryObjectDumpException,
			RegisterException, UnknownDistanceMeasureException,
			DynamicComponentInitializationException {
		CommandLineParser parser = new PosixParser();

		Options options = this.getAllOptions();

		CommandLine cmd = parser.parse(options, cliArguments);

		this.folderName = cmd.getOptionValue("folderName");
		this.fileName = cmd.getOptionValue("fileName") + ".v1";
		this.alias = cmd.getOptionValue("alias");

		this.handleOptions(cmd);

		// Ensure, that the dataset target file does not exist yet
		File targetFile = new File(
				FileUtils.buildPath(this.repository.getBasePath(IDataSet.class),
						this.folderName, this.fileName));

		if (targetFile.exists())
			throw new ParseException(
					"A dataset with the given name does already exist!");
		targetFile.getParentFile().mkdirs();

		generateDataSet();

		DataSet dataSet = null;

		try {
			// create the target file
			File dataSetFile = new File(FileUtils.buildPath(
					this.repository.getBasePath(IDataSet.class),
					this.getFolderName(), this.getFileName()));

			dataSet = writeCoordsToFile(dataSetFile);
		} catch (Exception e) {
			throw new DataSetGenerationException(
					"The dataset could not be generated!");
		}

		GoldStandard gs = null;

		if (this.generatesGoldStandard()) {
			// Ensure, that the goldstandard target file does not exist yet
			targetFile = new File(FileUtils.buildPath(
					this.repository.getBasePath(IGoldStandard.class),
					this.folderName, this.fileName));

			if (targetFile.exists())
				throw new ParseException(
						"A goldstandard with the given name does already exist!");
			targetFile.getParentFile().mkdirs();

			gs = generateGoldStandard();
		}

		IDataConfig dataConfig = this.writeConfigFiles(dataSet, gs, fileName);

		return dataSet;
	}

	/**
	 * Adds the default options of dataset generators to the given Options
	 * attribute
	 * 
	 * @param options
	 *            The existing Options attribute, holding already the options of
	 *            the actual generator implementation.
	 */
	private void addDefaultOptions(final Options options) {
		OptionBuilder.withArgName("folderName");
		OptionBuilder.isRequired();
		OptionBuilder.hasArg();
		OptionBuilder.withDescription(
				"The name of the folder to store this dataset in.");
		Option option = OptionBuilder.create("folderName");
		options.addOption(option);

		OptionBuilder.withArgName("fileName");
		OptionBuilder.isRequired();
		OptionBuilder.hasArg();
		OptionBuilder
				.withDescription("The name of the dataset file to generate.");
		option = OptionBuilder.create("fileName");
		options.addOption(option);

		OptionBuilder.withArgName("alias");
		OptionBuilder.isRequired();
		OptionBuilder.hasArg();
		OptionBuilder.withDescription("The alias of the data set.");
		option = OptionBuilder.create("alias");
		options.addOption(option);
	}

	/**
	 * This method is reponsible for interpreting the arguments passed to this
	 * generator call and to initialize possibly needed member variables.
	 * 
	 * <p>
	 * If you want to react to certain options in your implementation of
	 * {@link #generateDataSet()}, initialize member variables in this method.
	 * 
	 * @param cmd
	 *            A wrapper object for the arguments passed to this generator.
	 * @throws ParseException
	 */
	protected abstract void handleOptions(final CommandLine cmd)
			throws ParseException;

	protected String getFileName() {
		return this.fileName;
	}

	protected String getFolderName() {
		return this.folderName;
	}

	public String getDatasetAlias() {
		return this.alias;
	}

	/**
	 * This method needs to be implemented in subclasses and is a helper method
	 * for {@link #generate(String[])}. It provides the core of a dataset
	 * generator by generating the dataset and storing it in the coords
	 * attribute.
	 * 
	 * @throws DataSetGenerationException
	 *             If something goes wrong during the generation process, this
	 *             exception is thrown.
	 * @throws InterruptedException
	 */
	protected abstract void generateDataSet()
			throws DataSetGenerationException, InterruptedException;

	/**
	 * This method needs to be implemented in subclasses and is a helper method
	 * for {@link #generate(String[])}. It provides the functionality to
	 * generate the goldstandard file and creating a {@link GoldStandard}
	 * wrapper object for it.
	 * 
	 * @return A {@link GoldStandard} wrapper object for the generated
	 *         goldstandard file.
	 * @throws GoldStandardGenerationException
	 *             If something goes wrong during the generation process, this
	 *             exception is thrown.
	 */
	protected abstract GoldStandard generateGoldStandard()
			throws GoldStandardGenerationException;

	/**
	 * Parses a dataset generator from string.
	 * 
	 * @param repository
	 *            the repository
	 * @param dataSetGenerator
	 *            The simple name of the dataset generator class.
	 * @return the clustering quality measure
	 * @throws UnknownDataSetGeneratorException
	 * @throws DynamicComponentInitializationException
	 */
	public static IDataSetGenerator parseFromString(
			final IRepository repository, String dataSetGenerator)
			throws UnknownDataSetGeneratorException,
			DynamicComponentInitializationException {

		Class<? extends IDataSetGenerator> c = repository
				.resolveAndGetRegisteredClass(IDataSetGenerator.class,
						dataSetGenerator, true);
		if (c == null)
			throw new UnknownDataSetGeneratorException(dataSetGenerator,
					"Unknown dataset generator");
		return getInstance(repository, dataSetGenerator, c);
	}

	protected static IDataSetGenerator getInstance(final IRepository repository,
			String dataSetGenerator, Class<? extends IDataSetGenerator> c)
			throws UnknownDataSetGeneratorException,
			DynamicComponentInitializationException {
		try {
			IDataSetGenerator generator = c
					.getConstructor(IRepository.class, long.class, File.class)
					.newInstance(repository, System.currentTimeMillis(),
							new File(dataSetGenerator));
			return generator;
		} catch (NoSuchMethodException e) {
			throw new DynamicComponentInitializationException(
					IDataSetGenerator.class, dataSetGenerator,
					"The class is missing the correct constructor");
		} catch (Exception e) {
			// this is likely because we passed faked parameters
			return null;
		}
	}

	/**
	 * @param dataSetFile
	 * @return
	 * @throws IOException
	 * @throws UnknownDataSetTypeException
	 * @throws RegisterException
	 * @throws UnknownDataSetFormatException
	 * @throws DynamicComponentInitializationException
	 * 
	 */
	protected DataSet writeCoordsToFile(final File dataSetFile)
			throws IOException, UnknownDataSetFormatException,
			RegisterException, UnknownDataSetTypeException,
			DynamicComponentInitializationException {
		IDataSetFormat format = DataSetFormat.parseFromString(repository,
				"MatrixDataSetFormat");
		// dataset file
		BufferedWriter writer = new BufferedWriter(new FileWriter(dataSetFile));
		// writer header
		writer.append("// version = 1");
		writer.newLine();
		writer.append("// alias = " + getDatasetAlias());
		writer.newLine();
		writer.append("// dataSetFormat = MatrixDataSetFormat");
		writer.append(":" + format.getVersion());
		writer.newLine();
		writer.append("// dataSetType = SyntheticDataSetType");
		writer.newLine();
		for (int row = 0; row < coords.length; row++) {
			StringBuilder sb = new StringBuilder();
			sb.append((row + 1));
			sb.append("\t");
			for (int i = 0; i < coords[row].length; i++) {
				sb.append(coords[row][i] + "\t");
			}
			sb.deleteCharAt(sb.length() - 1);
			writer.append(sb.toString());
			writer.newLine();
		}
		writer.close();

		AbsoluteDataSet absoluteDataSet = new AbsoluteDataSet(this.repository,
				dataSetFile.lastModified(), dataSetFile, getDatasetAlias(),
				(AbsoluteDataSetFormat) format,
				DataSetType.parseFromString(repository, "SyntheticDataSetType"),
				WEBSITE_VISIBILITY.HIDE);
		absoluteDataSet.register();
		return absoluteDataSet;

	}

	@Override
	public ComparableVersion getVersion() {
		try {
			return Repository.getVersionOfObjectDynamicClass(this);
		} catch (Exception e) {
			// cannot happen because we loaded the class before;
			e.printStackTrace();
			return new ComparableVersion("1");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#getName()
	 */
	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObjectDynamicComponent#
	 * asSerializable()
	 */
	@Override
	public SerializableWrapperDynamicComponent<? extends IRepositoryObjectDynamicComponent> asSerializableInternal() {
		return new SerializableDataSetGenerator(this);
	}
}
