/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.data.dataset;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import de.clusteval.data.dataset.format.IRelativeDataSetFormat;
import de.clusteval.data.dataset.format.InvalidDataSetFormatVersionException;
import de.clusteval.data.dataset.format.RelativeDataSetFormat;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.dataset.type.IDataSetType;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.utils.IInMemoryListener;
import de.clusteval.utils.WeakMemoryListener;
import dk.sdu.imada.compbio.utils.SimilarityMatrix;
import dk.sdu.imada.compbio.utils.SimilarityMatrix.NUMBER_PRECISION;

/**
 * A relative dataset contains data in terms of pairwise similarities or
 * distances between object pairs. From these no absolute coordinates of the
 * objects can be deduced. Thus a relative dataset can never be converted to an
 * absolute dataset (lossfree).
 * 
 * @author Christian Wiwie
 * 
 */
public class RelativeDataSet extends DataSet implements IRelativeDataSet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8847472599758064721L;
	private transient SimilarityMatrix similarities;

	private int numberOfSamples = -1;

	/**
	 * 
	 * @param repository
	 *            the repository this dataset should be registered at.
	 * @param changeDate
	 *            The change date of this dataset is used for equality checks.
	 * @param absPath
	 *            The absolute path of this dataset.
	 * @param alias
	 *            A short alias name for this data set.
	 * @param dsFormat
	 *            The format of this dataset.
	 * @param dsType
	 *            The type of this dataset
	 * @param websiteVisibility
	 */
	public RelativeDataSet(IRepository repository, long changeDate,
			File absPath, final String alias, IRelativeDataSetFormat dsFormat,
			IDataSetType dsType, final WEBSITE_VISIBILITY websiteVisibility) {
		super(repository, changeDate, absPath, alias, dsFormat, dsType,
				websiteVisibility);
	}

	/**
	 * @param dataset
	 */
	public RelativeDataSet(RelativeDataSet dataset) {
		super(dataset);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see data.dataset.DataSet#clone()
	 */
	@Override
	public RelativeDataSet clone() {
		return new RelativeDataSet(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see data.dataset.DataSet#getDataSetFormat()
	 */
	@Override
	public RelativeDataSetFormat getDataSetFormat() {
		return (RelativeDataSetFormat) super.getDataSetFormat();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see data.dataset.DataSet#loadIntoMemory()
	 */
	@Override
	public synchronized boolean loadIntoMemory(final IInMemoryListener listener,
			NUMBER_PRECISION precision) throws IllegalArgumentException,
			IOException, InvalidDataSetFormatVersionException {
		this.memoryListener.add(new WeakMemoryListener(listener));
		if (!isInMemory())
			this.similarities = this.getDataSetFormat().parse(this, precision);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.dataset.DataSet#setDataSetContent(java.lang.Object)
	 */
	@Override
	public boolean setDataSetContent(Object newContent) {
		if (!(newContent instanceof SimilarityMatrix))
			return false;

		this.similarities = (SimilarityMatrix) newContent;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see data.dataset.DataSet#isInMemory()
	 */
	@Override
	public synchronized boolean isInMemory() {
		return this.similarities != null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see data.dataset.DataSet#getDataSetContent()
	 */
	@Override
	public SimilarityMatrix getDataSetContent() {
		return this.similarities;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see data.dataset.DataSet#unloadFromMemory()
	 */
	@Override
	public synchronized boolean unloadFromMemory(
			final IInMemoryListener listener) {
		this.memoryListener.remove(new WeakMemoryListener(listener));
		if (this.memoryListener.isEmpty()) {
			this.similarities = null;
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see data.dataset.DataSet#getIds()
	 */
	@Override
	public List<String> getIds() {
		String[] result = new String[this.similarities.getIds().size()];
		for (String id : this.similarities.getIds().keySet())
			result[this.similarities.getIds().get(id)] = id;
		return Arrays.asList(result);
	}

	@Override
	public int getNumberOfSamples() {
		if (this.numberOfSamples == -1) {
			parseNumberOfSamples();
		}

		return this.numberOfSamples;
	}

	private void parseNumberOfSamples() {
		// Read dataset file to determine number of samples and dimensionality
		boolean wasInMemory = isInMemory();
		try {
			if (!wasInMemory) {
				try {
					loadIntoMemory(this);
				} catch (InvalidDataSetFormatVersionException
						| UnknownDataSetFormatException
						| IllegalArgumentException | IOException e) {
					e.printStackTrace();
				}
			}
			this.numberOfSamples = this.getIds().size();
		} finally {
			if (!wasInMemory)
				unloadFromMemory(this);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.DataSet#asSerializable()
	 */
	@Override
	public SerializableDataSet asSerializableInternal()
			throws RepositoryObjectSerializationException {
		return new SerializableRelativeDataSet(this);
	}
}
