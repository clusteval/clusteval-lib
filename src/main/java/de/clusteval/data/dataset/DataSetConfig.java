/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
package de.clusteval.data.dataset;

import java.io.File;

import org.apache.commons.configuration.HierarchicalINIConfiguration;
import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.data.dataset.format.ConversionStandardToInputConfiguration;
import de.clusteval.data.dataset.format.IConversionInputToStandardConfiguration;
import de.clusteval.data.preprocessing.IDataPreprocessor;
import de.clusteval.framework.repository.DumpableRepositoryObject;
import de.clusteval.framework.repository.IDumpableRepositoryObject;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.Repository;
import de.clusteval.framework.repository.RepositoryEvent;
import de.clusteval.framework.repository.RepositoryMoveEvent;
import de.clusteval.framework.repository.RepositoryObjectDumpException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.RepositoryRemoveEvent;
import de.clusteval.framework.repository.RepositoryReplaceEvent;
import dk.sdu.imada.compbio.utils.SimilarityMatrix.NUMBER_PRECISION;

/**
 * 
 * A dataset configuration encapsulates options and settings for a dataset.
 * During the execution of a run, when programs are applied to datasets,
 * settings are required that control the behaviour of how the dataset has to be
 * handled.
 * 
 * <p>
 * A dataset configuration corresponds to and is parsed from a file on the
 * filesystem in the corresponding folder of the repository (see
 * {@link Repository#dataSetConfigBasePath} and {@link DataSetConfigFinder}).
 * 
 * <p>
 * There are several options, that can be specified in the dataset configuration
 * file (see {@link #parseFromFile(File)}).
 * 
 * @author Christian Wiwie
 * 
 */
public class DataSetConfig extends DumpableRepositoryObject
		implements
			IDataSetConfig,
			IDumpableRepositoryObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3269640033592156062L;

	/**
	 * A dataset configuration encapsulates a dataset. This attribute stores a
	 * reference to the dataset wrapper object.
	 */
	protected IDataSet dataset;

	/**
	 * This variable holds the configuration needed, when {@link #dataset} is
	 * converted from its original input format to the internal standard format
	 * of the framework.
	 */
	protected IConversionInputToStandardConfiguration configInputToStandard;

	/**
	 * This variable holds the configuration needed, when {@link #dataset} is
	 * converted from the internal standard format of the framework to the input
	 * format of a program.
	 */
	protected ConversionStandardToInputConfiguration configStandardToInput;

	protected String name;

	/**
	 * Instantiates a new dataset configuration.
	 * 
	 * @param repository
	 *            The repository this dataset configuration should be registered
	 *            at.
	 * @param changeDate
	 *            The change date of this dataset configuration is used for
	 *            equality checks.
	 * @param absPath
	 *            The absolute path of this dataset configuration.
	 * @param ds
	 *            The encapsulated dataset.
	 * @param configInputToStandard
	 *            The configuration needed, when {@link #dataset} is converted
	 *            from its original input format to the internal standard format
	 *            of the framework.
	 * @param configStandardToInput
	 *            The configuration needed, when {@link #dataset} is converted
	 *            from the internal standard format of the framework to the
	 *            input format of a program.
	 */
	public DataSetConfig(final IRepository repository, final long changeDate,
			final File absPath, final IDataSet ds,
			final IConversionInputToStandardConfiguration configInputToStandard,
			final ConversionStandardToInputConfiguration configStandardToInput) {
		super(repository, changeDate, absPath);

		this.name = this.absPath.getName().replaceAll(
				"(" + getAbsPathVersionMatchString() + "?).dsconfig$", "");

		this.dataset = ds;
		this.configInputToStandard = configInputToStandard;
		this.configStandardToInput = configStandardToInput;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#register()
	 */
	@Override
	public boolean register() throws RegisterException {
		if (super.register()) {
			this.dataset.addListener(this);

			// added 21.03.2013: register only, if this dataset config has been
			// registered before
			this.configInputToStandard.getDistanceMeasureAbsoluteToRelative()
					.register();
			this.configInputToStandard.getDistanceMeasureAbsoluteToRelative()
					.addListener(this);
			return true;
		}
		return false;
	}

	/**
	 * The copy constructor for dataset configurations.
	 * 
	 * @param datasetConfig
	 *            The dataset configuration to be cloned.
	 */
	public DataSetConfig(DataSetConfig datasetConfig) {
		super(datasetConfig);

		this.name = this.absPath.getName().replaceAll(
				"(" + getAbsPathVersionMatchString() + "?).dsconfig$", "");

		this.dataset = datasetConfig.dataset.clone();
		this.configInputToStandard = datasetConfig.configInputToStandard
				.clone();
		this.configStandardToInput = datasetConfig.configStandardToInput
				.clone();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.repository.RepositoryObject#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.IDataSetConfig#clone()
	 */
	@Override
	public DataSetConfig clone() {
		return new DataSetConfig(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.IDataSetConfig#getDataSet()
	 */
	@Override
	public IDataSet getDataSet() {
		return dataset;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.wiwie.wiutils.utils.RepositoryObject#notify(utils.RepositoryEvent)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.dataset.IDataSetConfig#notify(de.clusteval.framework.
	 * repository.RepositoryEvent)
	 */
	@Override
	public void notify(RepositoryEvent e) throws RegisterException {
		if (e instanceof RepositoryReplaceEvent) {
			RepositoryReplaceEvent event = (RepositoryReplaceEvent) e;
			if (event.getOld().equals(this)) {
				super.notify(event);
			} else {
				if (event.getOld().equals(dataset)) {
					event.getOld().removeListener(this);
					this.log.info("DataSetConfig " + this.absPath.getName()
							+ ": Dataset reloaded due to modifications in filesystem");
					event.getReplacement().addListener(this);
					// added 06.07.2012
					this.dataset = (DataSet) event.getReplacement();
				}
			}
		} else if (e instanceof RepositoryRemoveEvent) {
			RepositoryRemoveEvent event = (RepositoryRemoveEvent) e;
			if (event.getRemovedObject().equals(this))
				super.notify(event);
			else {
				if (event.getRemovedObject().equals(dataset)) {
					event.getRemovedObject().removeListener(this);
					this.log.info("DataSetConfig " + this
							+ ": Removed, because DataSet " + dataset
							+ " was removed.");
					RepositoryRemoveEvent newEvent = new RepositoryRemoveEvent(
							this);
					this.unregister();
					this.notify(newEvent);
				} else if (this.configInputToStandard
						.getDistanceMeasureAbsoluteToRelative()
						.equals(event.getRemovedObject())) {
					event.getRemovedObject().removeListener(this);
					this.log.info("DataSetConfig " + this
							+ ": Removed, because DistanceMeasure "
							+ event.getRemovedObject() + " was removed.");
					RepositoryRemoveEvent newEvent = new RepositoryRemoveEvent(
							this);
					this.unregister();
					this.notify(newEvent);
				}
			}
		} else if (e instanceof RepositoryMoveEvent) {
			RepositoryMoveEvent event = (RepositoryMoveEvent) e;
			if (event.getObject().equals(this)) {
				super.notify(event);
			} else {
				if (event.getObject().equals(dataset)) {
					this.log.info("DataSetConfig " + this.absPath.getName()
							+ ": updated with new dataset path.");
					// dump this dataset config to file with the updated dataset
					// file path.
					try {
						this.dumpToFile();
						// TODO: replace with new exception type
					} catch (RepositoryObjectDumpException e1) {
						e1.printStackTrace();
					}
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.IDataSetConfig#
	 * getConversionInputToStandardConfiguration()
	 */
	@Override
	public IConversionInputToStandardConfiguration getConversionInputToStandardConfiguration() {
		return this.configInputToStandard;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.IDataSetConfig#
	 * getConversionStandardToInputConfiguration()
	 */
	@Override
	public ConversionStandardToInputConfiguration getConversionStandardToInputConfiguration() {
		return this.configStandardToInput;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.dataset.IDataSetConfig#setDataSet(de.clusteval.data.
	 * dataset.DataSet)
	 */
	@Override
	public void setDataSet(IDataSet dataset) {
		this.dataset = dataset;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.DumpableRepositoryObject#dumpToFile()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.dataset.IDataSetConfig#dumpToFileHelper()
	 */
	@Override
	public void dumpToFileHelper(final HierarchicalINIConfiguration conf)
			throws RepositoryObjectDumpException {
		conf.addProperty("version", this.getVersion());
		conf.addProperty("datasetName", this.dataset.getMajorName());
		conf.addProperty("datasetFile",
				this.dataset.getMinorName() + ":" + this.dataset.getVersion());
		conf.addProperty("distanceMeasureAbsoluteToRelative",
				configInputToStandard.getDistanceMeasureAbsoluteToRelative()
						.getClass().getSimpleName());
		conf.addProperty("similarityPrecision",
				NUMBER_PRECISION.DOUBLE.toString());

		StringBuilder sb = new StringBuilder();
		if (!configInputToStandard.getPreprocessorsBeforeDistance().isEmpty()) {
			for (IDataPreprocessor pre : configInputToStandard
					.getPreprocessorsBeforeDistance()) {
				sb.append(pre.getClass().getSimpleName());
				sb.append(",");
			}
			sb.deleteCharAt(sb.length() - 1);

			conf.addProperty("preprocessorBeforeDistance", sb.toString());
		}

		if (!configInputToStandard.getPreprocessorsAfterDistance().isEmpty()) {
			sb = new StringBuilder();
			for (IDataPreprocessor pre : configInputToStandard
					.getPreprocessorsAfterDistance()) {
				sb.append(pre.getClass().getSimpleName());
				sb.append(",");
			}
			sb.deleteCharAt(sb.length() - 1);
			conf.addProperty("preprocessorAfterDistance", sb.toString());
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#
	 * ensureVersionsForAllComponents(java.lang.String)
	 */
	@Override
	protected String ensureVersionsForAllComponents(String fileContents) {
		String newfileContents = super.ensureVersionsForAllComponents(
				fileContents);

		String old = dataset.getMinorName();
		String withVersion = dataset.getMinorName() + ":"
				+ dataset.getVersion();
		newfileContents = newfileContents.replaceAll(
				String.format("datasetFile\\s*=\\s*%s(?!\\:)", old),
				"datasetFile = " + withVersion);

		newfileContents = ensureVersionForComponent(newfileContents,
				configInputToStandard.getDistanceMeasureAbsoluteToRelative());

		for (IRepositoryObject r : configInputToStandard
				.getPreprocessorsBeforeDistance()) {
			newfileContents = ensureVersionForComponent(newfileContents, r);
		}
		for (IRepositoryObject r : configInputToStandard
				.getPreprocessorsAfterDistance()) {
			newfileContents = ensureVersionForComponent(newfileContents, r);
		}

		return newfileContents;
	}

	/**
	 * @return the version
	 */
	@Override
	public ComparableVersion getVersion() {
		String name = absPath.getName().replaceAll(".dsconfig", "");
		int colonInd = name.lastIndexOf(".");
		return new ComparableVersion(name.substring(colonInd + 2));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#getName()
	 */
	@Override
	public String getName() {
		return this.name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#asSerializable()
	 */
	@Override
	public SerializableDataSetConfig asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableDataSetConfig) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#asSerializable()
	 */
	@Override
	public SerializableDataSetConfig asSerializableInternal()
			throws RepositoryObjectSerializationException {
		return new SerializableDataSetConfig(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#isSerializable()
	 */
	@Override
	public boolean isSerializable() {
		return true;
	}
}
