/**
 * 
 */
package de.clusteval.data.dataset;

import java.io.File;
import java.util.ArrayList;

import de.clusteval.data.DataConfig;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.format.ConversionInputToStandardConfiguration;
import de.clusteval.data.dataset.format.ConversionStandardToInputConfiguration;
import de.clusteval.data.distance.DistanceMeasure;
import de.clusteval.data.distance.UnknownDistanceMeasureException;
import de.clusteval.data.goldstandard.GoldStandard;
import de.clusteval.data.goldstandard.GoldStandardConfig;
import de.clusteval.data.goldstandard.IGoldStandardConfig;
import de.clusteval.data.preprocessing.IDataPreprocessor;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryObjectDumpException;
import de.clusteval.framework.repository.RepositoryObjectDynamicComponent;
import de.clusteval.utils.DynamicComponentInitializationException;
import dk.sdu.imada.compbio.file.FileUtils;
import dk.sdu.imada.compbio.utils.SimilarityMatrix.NUMBER_PRECISION;

/**
 * Subclasses of this abstract class correspond to objects, that provide in some
 * way data sets to the framework. For example by generating new ones
 * (generators) or by deriving them from existing ones (e.g. randomizer).
 * 
 * This class encapsulate the logic that is common to all these data set
 * providers, e.g. the logic to write configuration files for the newly
 * generated data set.
 * 
 * @author Christian Wiwie
 *
 */
public abstract class AbstractDataSetProvider extends RepositoryObjectDynamicComponent
		implements
			IAbstractDataSetProvider {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3915657812240107773L;

	/**
	 * @param repository
	 * @param changeDate
	 * @param absPath
	 * @throws RegisterException
	 */
	public AbstractDataSetProvider(IRepository repository, long changeDate, File absPath) throws RegisterException {
		super(repository, changeDate, absPath);
	}

	/**
	 * The copy constructor of dataset generators.
	 * 
	 * @param other
	 *            The object to clone.
	 * @throws RegisterException
	 */
	public AbstractDataSetProvider(AbstractDataSetProvider other) throws RegisterException {
		super(other);
	}

	protected DataConfig writeConfigFiles(final DataSet newDataSet, final GoldStandard newGoldStandard,
			final String configFileName) throws RepositoryObjectDumpException, RegisterException,
			UnknownDistanceMeasureException, DynamicComponentInitializationException {
		// write dataset config file
		File dsConfigFile = new File(
				FileUtils.buildPath(repository.getBasePath(IDataSetConfig.class), configFileName + ".v1.dsconfig"));
		DataSetConfig dsConfig = new DataSetConfig(this.repository, System.currentTimeMillis(), dsConfigFile,
				newDataSet,
				new ConversionInputToStandardConfiguration(
						DistanceMeasure.parseFromString(repository, "EuclidianDistanceMeasure"),
						NUMBER_PRECISION.DOUBLE, new ArrayList<IDataPreprocessor>(),
						new ArrayList<IDataPreprocessor>()),
				new ConversionStandardToInputConfiguration());

		dsConfig.dumpToFile();

		GoldStandardConfig gsConfig = null;

		if (newGoldStandard != null) {

			File gsConfigFile = new File(FileUtils.buildPath(repository.getBasePath(IGoldStandardConfig.class),
					configFileName + ".v1.gsconfig"));

			// write goldstandard config file
			gsConfig = new GoldStandardConfig(this.repository, System.currentTimeMillis(), gsConfigFile,
					newGoldStandard);

			gsConfig.dumpToFile();
		}

		// write data config file
		File dataConfigFile = new File(
				FileUtils.buildPath(repository.getBasePath(IDataConfig.class), configFileName + ".v1.dataconfig"));
		DataConfig dataConfig = new DataConfig(this.repository, System.currentTimeMillis(), dataConfigFile, dsConfig,
				gsConfig);

		dataConfig.dumpToFile();

		return dataConfig;
	}
}
