/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.data;

import de.clusteval.data.dataset.DataSetConfigFinderThread;
import de.clusteval.data.dataset.IDataSetConfig;
import de.clusteval.data.goldstandard.GoldStandardConfigFinderThread;
import de.clusteval.data.goldstandard.IGoldStandardConfig;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.threading.ISupervisorThread;
import de.clusteval.utils.FinderThread;
import de.clusteval.utils.IFinder;

/**
 * @author Christian Wiwie
 * 
 */
public class DataConfigFinderThread extends FinderThread<IDataConfig> {

	/**
	 * @param supervisorThread
	 * @param repository
	 *            The repository to check for new data configurations.
	 * @param checkOnce
	 *            If true, this thread only checks once for new data
	 *            configurations.
	 * 
	 */
	public DataConfigFinderThread(final ISupervisorThread supervisorThread, final IRepository repository,
			final boolean checkOnce) {
		super(supervisorThread, repository, IDataConfig.class, 30000, checkOnce);
	}

	/**
	 * @param supervisorThread
	 * @param repository
	 *            The repository to check for new runs.
	 * @param sleepTime
	 *            The time between two checks.
	 * @param checkOnce
	 *            If true, this thread only checks once for new runs.
	 * 
	 */
	public DataConfigFinderThread(final ISupervisorThread supervisorThread, final IRepository repository,
			final long sleepTime, final boolean checkOnce) {
		super(supervisorThread, repository, IDataConfig.class, sleepTime, checkOnce);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.FinderThread#beforeFind()
	 */
	@Override
	protected void beforeFind() {
		if (!this.repository.isInitialized(IDataSetConfig.class))
			this.supervisorThread.getThread(DataSetConfigFinderThread.class).waitFor();

		if (!this.repository.isInitialized(IGoldStandardConfig.class))
			this.supervisorThread.getThread(GoldStandardConfigFinderThread.class).waitFor();
		super.beforeFind();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.FinderThread#getFinder()
	 */
	@Override
	public IFinder<IDataConfig> getFinder() throws RegisterException {
		return new DataConfigFinder(repository);
	}
}
