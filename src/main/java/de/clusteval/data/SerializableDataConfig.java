/**
 * 
 */
package de.clusteval.data;

import java.io.File;

import de.clusteval.data.dataset.ISerializableDataSetConfig;
import de.clusteval.data.goldstandard.ISerializableGoldStandardConfig;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.SerializableWrapperRepositoryObject;
import dk.sdu.imada.compbio.file.FileUtils;

public class SerializableDataConfig extends SerializableWrapperRepositoryObject<IDataConfig>
		implements
			ISerializableDataConfig {

	/**
	 * 
	 */
	private static final long serialVersionUID = 606709592031999130L;

	protected ISerializableDataSetConfig dataSetConfig, standardDataSetConfig;

	protected ISerializableGoldStandardConfig goldstandardConfig;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 * @param dataSetConfig
	 * @param standardDataSetConfig
	 * @param goldstandardConfig
	 */
	public SerializableDataConfig(File absPath, String name, String version,
			ISerializableDataSetConfig dataSetConfig,
			ISerializableDataSetConfig standardDataSetConfig,
			ISerializableGoldStandardConfig goldstandardConfig) {
		super(absPath, name, version);
		this.dataSetConfig = dataSetConfig;
		this.standardDataSetConfig = standardDataSetConfig;
		this.goldstandardConfig = goldstandardConfig;
	}

	/**
	 * @param dataConfig
	 */
	public SerializableDataConfig(final IDataConfig dataConfig)
			throws RepositoryObjectSerializationException {
		super(dataConfig);
		if (dataConfig.getDatasetConfig() != null)
			this.dataSetConfig = (ISerializableDataSetConfig) dataConfig
					.getDatasetConfig().asSerializable();
		if (dataConfig.getStandardDataSetConfig() != null)
			this.standardDataSetConfig = (ISerializableDataSetConfig) dataConfig
					.getStandardDataSetConfig().asSerializable();
		if (dataConfig.getGoldstandardConfig() != null)
			this.goldstandardConfig = (ISerializableGoldStandardConfig) dataConfig
					.getGoldstandardConfig().asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.ISerializableDataConfig#getDatasetConfig()
	 */
	@Override
	public ISerializableDataSetConfig getDatasetConfig() {
		return dataSetConfig;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.ISerializableDataConfig#getStandardDataSetConfig()
	 */
	@Override
	public ISerializableDataSetConfig getStandardDataSetConfig() {
		return standardDataSetConfig;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.ISerializableDataConfig#getGoldstandardConfig()
	 */
	@Override
	public ISerializableGoldStandardConfig getGoldstandardConfig() {
		return goldstandardConfig;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.ISerializableDataConfig#hasGoldStandardConfig()
	 */
	@Override
	public boolean hasGoldStandardConfig() {
		return this.goldstandardConfig != null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.SerializableWrapper#deserializeInternal
	 * (de.clusteval.framework.repository.IRepository)
	 */
	@Override
	protected IDataConfig deserializeInternal(IRepository repository)
			throws DeserializationException {
		try {
			return repository.getStaticObjectWithNameAndVersion(
					IDataConfig.class, name + ":" + version);
		} catch (ObjectNotRegisteredException
				| ObjectVersionNotRegisteredException e) {
		}
		return new DataConfig(repository, System.currentTimeMillis(),
				new File(FileUtils.buildPath(
						repository.getBasePath(IDataConfig.class),
						this.name + ".v" + version + ".dataconfig")),
				dataSetConfig.deserialize(repository),
				goldstandardConfig != null
						? goldstandardConfig.deserialize(repository)
						: null);
	}
}