/**
 * 
 */
package de.clusteval.data.randomizer;

import java.io.File;

import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RepositoryObjectDeserializationException;
import de.clusteval.framework.repository.SerializableWrapperDynamicComponent;
import de.clusteval.utils.DynamicComponentInitializationException;

public class SerializableDataRandomizer
		extends
			SerializableWrapperDynamicComponent<IDataRandomizer>
		implements
			ISerializableDataRandomizer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2016390008104871693L;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 */
	public SerializableDataRandomizer(File absPath, String name, String version,
			final String alias) {
		super(IDataRandomizer.class, absPath, name, version, alias);
	}

	/**
	 * @param wrappedComponent
	 */
	public SerializableDataRandomizer(IDataRandomizer wrappedComponent) {
		super(IDataRandomizer.class, wrappedComponent);
	}

	@Override
	protected IDataRandomizer deserializeInternal(final IRepository repository)
			throws DeserializationException {
		try {
			return DataRandomizer.parseFromString(repository, this.name);
		} catch (UnknownDataRandomizerException
				| DynamicComponentInitializationException e) {
			throw new RepositoryObjectDeserializationException(
					IDataRandomizer.class, this.name, e);
		}
	}
}