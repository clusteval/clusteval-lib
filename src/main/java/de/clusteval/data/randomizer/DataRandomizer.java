/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.data.randomizer;

import java.io.File;
import java.lang.reflect.InvocationTargetException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.data.DataConfig;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.AbstractDataSetProvider;
import de.clusteval.data.dataset.DataSet;
import de.clusteval.data.dataset.generator.DataSetGenerationException;
import de.clusteval.data.goldstandard.GoldStandard;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.Repository;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.utils.ClustEvalAlias;
import de.clusteval.utils.ClassVersion;
import de.clusteval.utils.ClassVersionRequirement;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.DynamicComponentVersion;
import dk.sdu.imada.compbio.file.FileUtils;
import dk.sdu.imada.compbio.utils.Pair;

/**
 * A data randomizer `MyDataRandomizer` can be added to ClustEval by
 * 
 * 1. extending the class {@link DataRandomizer} with your own class `MyDataRandomizer`.
 *     - Your class has to be decorated with the following annotations:
 *         - {@link DynamicComponentVersion}: Any component you add to ClustEval needs to have a version. This is needed to ensure, that results are fully reproducible by running runs using exactly the same versions as previously
 *         - {@link ClassVersionRequirement}: Any component you add to ClustEval needs to specify, which dependencies it has on the ClustEval interfaces. This information is used to ensure compatibility between the version of your component and the used ClustEval version. Whenever core interfaces in the ClustEval API change, their interface version is incremented. By using this annotation it can detect, which components are still compatible.
 *         - {@link ClustEvalAlias}: You need to provide a readable alias for your component. This alias is used when results and components should be presented in a accessible way.
 *     - You have to provide your own implementations for the following methods, otherwise the framework will not be able to load your runresult format.
 *         * {@link #DataRandomizer(IRepository, long, File)}: The constructor of your class. This constructor has to be implemented and public, otherwise the framework will not be able to load your runresult format.
 *         * {@link #DataRandomizer(DataRandomizer)}: The copy constructor of your class taking another instance of your class. This constructor has to be implemented and public.
 *         * {@link #getCustomOptions()}: This method returns an :java:ref:`Options` object that encapsulates all parameters that this randomizer has. These can be set by the user in the client.
 *         * {@link #handleOptions(CommandLine)}: This method handles the values that the user set for the parameters specified in :java:ref:`getOptions()`.
 *         * {@link #getDataSetFileNamePostFix()}: This method makes sure, that randomized data sets of the same data configuration do not end up with the same file name and overwrite each other. A good advice is to integrate the randomizer parameter values or a timestamp.
 *         * {@link #randomizeDataConfig()}: This is the core of your randomizer; In this method the #dataConfig attribute is randomized and a distorted data set and gold standard is returned.
 * 2. Creating a jar file named `MyDataRandomizer.jar` containing the `MyDataRandomizer.class` compiled on your machine in the correct folder structure corresponding to the packages:
 *     * `de/clusteval/data/randomizer/MyDataRandomizer.class`
 * 3. Putting the `MyDataRandomizer.jar` into the corresponding folder of the repository: `<REPOSITORY ROOT>/supp/randomizers`
 * 
 * 
 * The backend server will recognize and try to load the new class automatically the next time, the {@link DataRandomizerFinderThread} checks the filesystem.
 * 
 * @author Christian Wiwie
 * 
 */
@ClassVersion(version = "3")
public abstract class DataRandomizer extends AbstractDataSetProvider
		implements
			IDataRandomizer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3193233264563126858L;

	/**
	 * This attribute holds the name of the data configuration to randomize.
	 */
	protected IDataConfig dataConfig;

	protected String uniqueId;

	protected boolean onlySimulate;

	/**
	 * @param repository
	 * @param changeDate
	 * @param absPath
	 * @throws RegisterException
	 */
	public DataRandomizer(IRepository repository, long changeDate, File absPath)
			throws RegisterException {
		super(repository, changeDate, absPath);
	}

	/**
	 * The copy constructor of dataset randomizer.
	 * 
	 * @param other
	 *            The object to clone.
	 * @throws RegisterException
	 */
	public DataRandomizer(DataRandomizer other) throws RegisterException {
		super(other);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.repository.RepositoryObject#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.randomizer.IDataRandomizer#clone()
	 */
	@Override
	public IDataRandomizer clone() {
		try {
			return this.getClass().getConstructor(this.getClass())
					.newInstance(this);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		this.log.warn("Cloning instance of class "
				+ this.getClass().getSimpleName() + " failed");
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.randomizer.IDataRandomizer#getAllOptions()
	 */
	@Override
	public Options getAllOptions() {
		// options of actual generator implementation
		Options options = this.getCustomOptions();

		// default options of all generators
		this.addDefaultOptions(options);
		return options;
	}

	/**
	 * @return A wrapper object keeping the options of your dataset generator.
	 *         The options returned by this method are going to be used and
	 *         interpreted in your subclass implementation in
	 *         {@link #generateDataSet()} .
	 */
	@Override
	public abstract Options getCustomOptions();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.randomizer.IDataRandomizer#randomize(java.lang.String[]
	 * )
	 */
	@Override
	public DataConfig randomize(final String[] cliArguments)
			throws DataRandomizeException {
		return randomize(cliArguments, false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.randomizer.IDataRandomizer#randomize(java.lang.String[]
	 * , boolean)
	 */
	// TODO: remove onlySimulate attribute
	@Override
	public DataConfig randomize(final String[] cliArguments,
			final boolean onlySimulate) throws DataRandomizeException {
		try {
			this.onlySimulate = onlySimulate;
			CommandLineParser parser = new PosixParser();

			Options options = this.getAllOptions();

			CommandLine cmd = parser.parse(options, cliArguments);

			// get data config with the specified name
			String absFilePath = FileUtils.buildPath(
					this.repository.getBasePath(IDataConfig.class),
					cmd.getOptionValue("dataConfig") + ".dataconfig");
			this.dataConfig = (IDataConfig) this.repository
					.getRegisteredObject(new File(absFilePath));

			this.uniqueId = cmd.getOptionValue("uniqueId");

			this.handleOptions(cmd);

			Pair<DataSet, GoldStandard> newObjects = randomizeDataConfig();

			DataConfig dataConfig = this.writeConfigFiles(newObjects.getFirst(),
					newObjects.getSecond(),
					this.uniqueId + "_"
							+ this.dataConfig.getGoldstandardConfig().toString()
							+ getDataSetFileNamePostFix());

			return dataConfig;
		} catch (Exception e) {
			throw new DataRandomizeException(e);
		}
	}

	protected abstract String getDataSetFileNamePostFix();

	/**
	 * Adds the default options of dataset generators to the given Options
	 * attribute
	 * 
	 * @param options
	 *            The existing Options attribute, holding already the options of
	 *            the actual generator implementation.
	 */
	private void addDefaultOptions(final Options options) {
		OptionBuilder.withArgName("dataConfig");
		OptionBuilder.isRequired();
		OptionBuilder.hasArg();
		OptionBuilder.withDescription(
				"The name of the data configuration to randomize");
		Option option = OptionBuilder.create("dataConfig");
		options.addOption(option);

		OptionBuilder.withArgName("uniqueId");
		OptionBuilder.isRequired();
		OptionBuilder.hasArg();
		OptionBuilder.withDescription(
				"A unique id (infix) for the generated files.");
		option = OptionBuilder.create("uniqueId");
		options.addOption(option);
	}

	/**
	 * This method is reponsible for interpreting the arguments passed to this
	 * generator call and to initialize possibly needed member variables.
	 * 
	 * <p>
	 * If you want to react to certain options in your implementation of
	 * {@link #generateDataSet()}, initialize member variables in this method.
	 * 
	 * @param cmd
	 *            A wrapper object for the arguments passed to this generator.
	 * @throws ParseException
	 */
	protected abstract void handleOptions(final CommandLine cmd)
			throws ParseException;

	/**
	 * This method needs to be implemented in subclasses and is a helper method
	 * for {@link #randomize(String[])}. It provides the core of a dataset
	 * generator by generating the dataset file and creating a {@link DataSet}
	 * wrapper object for it.
	 * 
	 * @throws InterruptedException
	 * 
	 * @throws DataSetGenerationException
	 *             If something goes wrong during the generation process, this
	 *             exception is thrown.
	 */
	protected abstract Pair<DataSet, GoldStandard> randomizeDataConfig()
			throws InterruptedException;

	/**
	 * Parses a dataconfig randomizer from string.
	 * 
	 * @param repository
	 *            the repository
	 * @param dataRandomizer
	 *            The simple name of the dataset randomizer class.
	 * @return the clustering quality measure
	 * @throws UnknownDataRandomizerException
	 * @throws DynamicComponentInitializationException
	 */
	public static IDataRandomizer parseFromString(final IRepository repository,
			String dataRandomizer) throws UnknownDataRandomizerException,
			DynamicComponentInitializationException {

		Class<? extends IDataRandomizer> c = repository
				.resolveAndGetRegisteredClass(IDataRandomizer.class,
						dataRandomizer, true);
		if (c == null)
			throw new UnknownDataRandomizerException(dataRandomizer,
					"Unknown data randomizer.");
		return getInstance(repository, dataRandomizer, c);
	}

	protected static IDataRandomizer getInstance(final IRepository repository,
			String dataRandomizer, Class<? extends IDataRandomizer> c)
			throws UnknownDataRandomizerException,
			DynamicComponentInitializationException {
		try {
			IDataRandomizer generator = c
					.getConstructor(IRepository.class, long.class, File.class)
					.newInstance(repository, System.currentTimeMillis(),
							new File(dataRandomizer));
			return generator;
		} catch (NoSuchMethodException e) {
			throw new DynamicComponentInitializationException(
					IDataRandomizer.class, dataRandomizer,
					"The class is missing the correct constructor");
		} catch (Exception e) {
			// this is likely because we passed faked parameters
			return null;
		}
	}

	@Override
	public ComparableVersion getVersion() {
		try {
			return Repository.getVersionOfObjectDynamicClass(this);
		} catch (Exception e) {
			// cannot happen because we loaded the class before;
			e.printStackTrace();
			return new ComparableVersion("1");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#getName()
	 */
	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObjectDynamicComponent#
	 * asSerializable()
	 */
	@Override
	public ISerializableDataRandomizer asSerializable()
			throws RepositoryObjectSerializationException {
		return (ISerializableDataRandomizer) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObjectDynamicComponent#
	 * asSerializable()
	 */
	@Override
	public ISerializableDataRandomizer asSerializableInternal() {
		return new SerializableDataRandomizer(this);
	}
}
