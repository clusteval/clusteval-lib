/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
package de.clusteval.data.goldstandard;

import java.io.File;

import org.apache.commons.configuration.HierarchicalINIConfiguration;
import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.framework.repository.DumpableRepositoryObject;
import de.clusteval.framework.repository.IDumpableRepositoryObject;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.Repository;
import de.clusteval.framework.repository.RepositoryEvent;
import de.clusteval.framework.repository.RepositoryMoveEvent;
import de.clusteval.framework.repository.RepositoryObjectDumpException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.RepositoryRemoveEvent;
import de.clusteval.framework.repository.RepositoryReplaceEvent;

/**
 * A goldstandard configuration encapsulates options and settings for a
 * goldstandard. During the execution of a run, when goldstandards are used,
 * settings are required that control the behaviour of how the goldstandard has
 * to be handled.
 * 
 * <p>
 * A goldstandard configuration corresponds to and is parsed from a file on the
 * filesystem in the corresponding folder of the repository (see
 * {@link Repository#goldStandardConfigBasePath} and
 * {@link GoldStandardConfigFinder}).
 * 
 * <p>
 * There are several options, that can be specified in the goldstandard
 * configuration file (see {@link #parseFromFile(File)}).
 * 
 * @author Christian Wiwie
 * 
 */
public class GoldStandardConfig extends DumpableRepositoryObject
		implements
			IGoldStandardConfig,
			IDumpableRepositoryObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2470316819068374021L;

	/**
	 * A goldstandard configuration encapsulates a goldstandard. This attribute
	 * stores a reference to the goldstandard wrapper object.
	 */
	private IGoldStandard goldStandard;

	protected String name;

	/**
	 * Instantiates a new goldstandard configuration.
	 * 
	 * @param repository
	 *            The repository this goldstandard configuration should be
	 *            registered at.
	 * @param changeDate
	 *            The change date of this goldstandard configuration is used for
	 *            equality checks.
	 * @param absPath
	 *            The absolute path of this goldstandard configuration.
	 * @param goldstandard
	 *            The encapsulated goldstandard.
	 */
	public GoldStandardConfig(final IRepository repository,
			final long changeDate, final File absPath,
			final IGoldStandard goldstandard) {
		super(repository, changeDate, absPath);
		this.name = this.absPath.getName().replaceAll(
				"(" + getAbsPathVersionMatchString() + "?).gsconfig$", "");
		this.goldStandard = goldstandard;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#register()
	 */
	@Override
	public boolean register() throws RegisterException {
		if (super.register()) {
			this.goldStandard.addListener(this);
			return true;
		}
		return false;
	}

	/**
	 * The copy constructor for goldstandard configurations.
	 * 
	 * @param goldstandardConfig
	 *            The goldstandard configuration to be cloned.
	 */
	public GoldStandardConfig(GoldStandardConfig goldstandardConfig) {
		super(goldstandardConfig);
		this.goldStandard = goldstandardConfig.goldStandard.clone();
		this.name = this.absPath.getName().replaceAll(
				"(" + getAbsPathVersionMatchString() + "?).gsconfig$", "");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.repository.RepositoryObject#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.goldstandard.IGoldStandardConfig#clone()
	 */
	@Override
	public GoldStandardConfig clone() {
		return new GoldStandardConfig(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.goldstandard.IGoldStandardConfig#getGoldstandard()
	 */
	@Override
	public IGoldStandard getGoldstandard() {
		return goldStandard;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.goldstandard.IGoldStandardConfig#setGoldStandard(de.
	 * clusteval.data.goldstandard.GoldStandard)
	 */
	@Override
	public void setGoldStandard(final IGoldStandard goldStandard) {
		this.goldStandard = goldStandard;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.wiwie.wiutils.utils.RepositoryObject#notify(utils.RepositoryEvent)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.goldstandard.IGoldStandardConfig#notify(de.clusteval.
	 * framework.repository.RepositoryEvent)
	 */
	@Override
	public void notify(RepositoryEvent e) throws RegisterException {
		if (e instanceof RepositoryReplaceEvent) {
			RepositoryReplaceEvent event = (RepositoryReplaceEvent) e;
			if (event.getOld().equals(this))
				super.notify(event);
			else {
				if (event.getOld().equals(goldStandard)) {
					event.getOld().removeListener(this);
					this.log.info("GoldStandardConfig " + this.absPath.getName()
							+ ": GoldStandard reloaded due to modifications in filesystem");
					event.getReplacement().addListener(this);
					// added 06.07.2012
					this.goldStandard = (GoldStandard) event.getReplacement();
				}
			}
		} else if (e instanceof RepositoryRemoveEvent) {
			RepositoryRemoveEvent event = (RepositoryRemoveEvent) e;
			if (event.getRemovedObject().equals(this))
				super.notify(event);
			else {
				if (event.getRemovedObject().equals(goldStandard)) {
					event.getRemovedObject().removeListener(this);
					this.log.info("GoldStandardConfig " + this
							+ ": Removed, because GoldStandard " + goldStandard
							+ " was removed.");
					RepositoryRemoveEvent newEvent = new RepositoryRemoveEvent(
							this);
					this.unregister();
					this.notify(newEvent);
				}
			}
		} else if (e instanceof RepositoryMoveEvent) {
			RepositoryMoveEvent event = (RepositoryMoveEvent) e;
			if (event.getObject().equals(this))
				super.notify(event);
			else {
				if (event.getObject().equals(goldStandard)) {
					this.log.info("GoldStandardConfig " + this
							+ " updated with new goldstandard path.");
					try {
						this.dumpToFile();
					} catch (RepositoryObjectDumpException e1) {
						e1.printStackTrace();
					}
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#getName()
	 */
	@Override
	public String getName() {
		return this.name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.DumpableRepositoryObject#
	 * dumpToFileHelper ()
	 */
	@Override
	protected void dumpToFileHelper(final HierarchicalINIConfiguration conf)
			throws RepositoryObjectDumpException {
		conf.addProperty("version", this.getVersion());
		conf.addProperty("goldstandardName", this.goldStandard.getMajorName());
		conf.addProperty("goldstandardFile", this.goldStandard.getMinorName()
				+ ":" + this.goldStandard.getVersion());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#
	 * ensureVersionsForAllComponents(java.lang.String)
	 */
	@Override
	protected String ensureVersionsForAllComponents(String fileContents) {
		String newfileContents = super.ensureVersionsForAllComponents(
				fileContents);

		String old = goldStandard.getMinorName();
		String withVersion = goldStandard.getMinorName() + ":"
				+ goldStandard.getVersion();
		newfileContents = newfileContents.replaceAll(
				String.format("goldstandardFile\\s*=\\s*%s(?!\\:)", old),
				"goldstandardFile = " + withVersion);

		return newfileContents;
	}

	/**
	 * @return the version
	 */
	@Override
	public ComparableVersion getVersion() {
		String name = absPath.getName().replaceAll(".gsconfig", "");
		int colonInd = name.lastIndexOf(".");
		return new ComparableVersion(name.substring(colonInd + 2));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#isSerializable()
	 */
	@Override
	public boolean isSerializable() {
		return true;
	}
	
	/* (non-Javadoc)
	 * @see de.clusteval.framework.repository.RepositoryObject#asSerializable()
	 */
	@Override
	public SerializableGoldStandardConfig asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableGoldStandardConfig) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#asSerializable()
	 */
	@Override
	public SerializableGoldStandardConfig asSerializableInternal()
			throws RepositoryObjectSerializationException {
		return new SerializableGoldStandardConfig(this);
	}
}
