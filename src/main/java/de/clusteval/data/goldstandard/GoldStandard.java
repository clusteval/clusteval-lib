/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
package de.clusteval.data.goldstandard;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.cluster.Cluster;
import de.clusteval.cluster.ClusterItem;
import de.clusteval.cluster.FileBackedClustering;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RepositoryObject;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.run.runresult.format.RunResultFormat;
import dk.sdu.imada.compbio.utils.text.TextFileMapParser;

/**
 * A wrapper class for a goldstandard on the filesystem.
 * 
 * @author Christian Wiwie
 * 
 */
public class GoldStandard extends RepositoryObject implements IGoldStandard {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1418542963161543230L;

	/**
	 * This attribute holds the clustering that corresponds to the goldstandard.
	 * Every goldstandard can be interpreted as a clustering: A partition of the
	 * data objects into several groups.
	 * 
	 * @see {@link FileBackedClustering}
	 */
	protected transient FileBackedClustering clustering;

	protected String minorName, majorName;

	/**
	 * Instantiates a new goldstandard object.
	 * 
	 * @param repository
	 *            the repository this goldstandard should be registered at.
	 * @param changeDate
	 *            The change date of this goldstandard is used for equality
	 *            checks.
	 * @param absGoldStandardPath
	 *            The absolute path of this goldstandard.
	 */
	public GoldStandard(final IRepository repository, final long changeDate,
			final File absGoldStandardPath) {
		super(repository, changeDate, absGoldStandardPath);

		this.majorName = this.absPath.getParentFile().getName();
		this.minorName = this.absPath.getName()
				.replaceAll("(" + getAbsPathVersionMatchString() + "?)$", "");

		this.log.trace("Goldstandard file: \"" + this.getAbsolutePath() + "\"");
	}

	/**
	 * Copy constructor for the GoldStandard class.
	 * 
	 * @param goldStandard
	 *            The goldstandard to be cloned.
	 */
	public GoldStandard(final GoldStandard goldStandard) {
		super(goldStandard);
		this.absPath = new File(goldStandard.absPath.getAbsolutePath());

		this.majorName = this.absPath.getParentFile().getName();
		this.minorName = this.absPath.getName()
				.replaceAll("(" + getAbsPathVersionMatchString() + "?)$", "");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.repository.RepositoryObject#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.goldstandard.IGoldStandard#clone()
	 */
	@Override
	public GoldStandard clone() {
		return new GoldStandard(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.goldstandard.IGoldStandard#isInMemory()
	 */
	@Override
	public boolean isInMemory() {
		return this.clustering != null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.goldstandard.IGoldStandard#loadIntoMemory()
	 */
	@Override
	public boolean loadIntoMemory() throws InvalidGoldStandardFormatException {
		try {
			TextFileMapParser parser = new TextFileMapParser(
					this.absPath.getAbsolutePath(), 0, 1);
			parser.process();

			Map<String, Cluster> clusterMap = new HashMap<String, Cluster>();

			Map<String, String> mapping = parser.getResult();
			for (String itemId : mapping.keySet()) {
				ClusterItem item = new ClusterItem(itemId);

				String fuzzyClusterIds = mapping.get(itemId);
				String[] clusterIds = fuzzyClusterIds.split(";");

				for (String fuzzyClusterId : clusterIds) {
					try {
						String[] split = fuzzyClusterId.split(":");
						String clusterId = split[0];
						String fuzzy;
						// with fuzzy coefficient
						if (split.length == 2) {
							fuzzy = split[1];
						}
						// without fuzzy coefficient
						else if (split.length == 1) {
							fuzzy = "1.0";
						}
						// unsupported
						else {
							throw new ArrayIndexOutOfBoundsException();
						}

						if (!clusterMap.containsKey(clusterId)) {
							Cluster newCluster = new Cluster(clusterId);
							clusterMap.put(clusterId, newCluster);
						}
						clusterMap.get(clusterId).add(item,
								Float.parseFloat(fuzzy));
					} catch (ArrayIndexOutOfBoundsException e) {
						throw new InvalidGoldStandardFormatException(
								"The gold standard format is not valid near '"
										+ itemId
										+ "': Please ensure that it follows the per-row format ID{tab}CLASS_1:FUZZY_COEFF_1;CLASS_2:FUZZY_COEFF_2;...;CLASS_K:FUZZY_COEFF_K");
					}
				}
			}

			this.clustering = new FileBackedClustering(this.repository,
					this.absPath.lastModified(), this.absPath,
					RunResultFormat.parseFromString(getRepository(),
							"TabSeparatedRunResultFormat"));
			for (String clusterId : clusterMap.keySet())
				this.clustering.addCluster(clusterMap.get(clusterId));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			throw new InvalidGoldStandardFormatException(e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.goldstandard.IGoldStandard#unloadFromMemory()
	 */
	@Override
	public boolean unloadFromMemory() {
		this.clustering = null;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.goldstandard.IGoldStandard#size()
	 */
	@Override
	public int size() {
		return this.clustering.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.goldstandard.IGoldStandard#fuzzySize()
	 */
	@Override
	public float fuzzySize() {
		return this.clustering.fuzzySize();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.goldstandard.IGoldStandard#getClustering()
	 */
	@Override
	public FileBackedClustering getClustering()
			throws InvalidGoldStandardFormatException {
		if (!isInMemory()) {
			loadIntoMemory();
		}
		return this.clustering;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#getName()
	 */
	@Override
	public String getName() {
		return this.getFullName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.goldstandard.IGoldStandard#getFullName()
	 */
	@Override
	public String getFullName() {
		return getMajorName() + "/" + getMinorName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.goldstandard.IGoldStandard#getMajorName()
	 */
	@Override
	public String getMajorName() {
		return this.majorName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.goldstandard.IGoldStandard#getMinorName()
	 */
	@Override
	public String getMinorName() {
		return this.minorName;
	}

	/**
	 * @return the version
	 */
	@Override
	public ComparableVersion getVersion() {
		String name = absPath.getName();
		int colonInd = name.lastIndexOf(".");
		return new ComparableVersion(name.substring(colonInd + 2));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#isSerializable()
	 */
	@Override
	public boolean isSerializable() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#asSerializable()
	 */
	@Override
	public SerializableGoldStandard asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableGoldStandard) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#asSerializable()
	 */
	@Override
	public SerializableGoldStandard asSerializableInternal()
			throws RepositoryObjectSerializationException {
		return new SerializableGoldStandard(this);
	}
}
