/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.data.goldstandard.format;

import java.io.File;

import org.apache.maven.artifact.versioning.ComparableVersion;
import org.slf4j.LoggerFactory;

import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryObject;
import de.clusteval.framework.repository.SerializableWrapperRepositoryObject;

/**
 * The Class GoldStandardFormat.
 * 
 * @author Christian Wiwie
 */
public class GoldStandardFormat extends RepositoryObject
		implements
			IGoldStandardFormat {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5281108500402084725L;

	/**
	 * Instantiates a new gold standard format.
	 * 
	 * @param repo
	 * @param changeDate
	 * @param absPath
	 * @throws RegisterException
	 * 
	 */
	public GoldStandardFormat(final IRepository repo, final long changeDate,
			final File absPath) throws RegisterException {
		super(repo, changeDate, absPath);
		this.log = LoggerFactory.getLogger(this.getClass());
	}

	/**
	 * The copy constructor of goldstandard formats.
	 * 
	 * @param other
	 *            The object to clone.
	 * @throws RegisterException
	 */
	public GoldStandardFormat(final GoldStandardFormat other)
			throws RegisterException {
		super(other);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#clone()
	 */
	@Override
	public IRepositoryObject clone() {
		try {
			return new GoldStandardFormat(this);
		} catch (RegisterException e) {
			this.log.warn("Cloning instance of class "
					+ this.getClass().getSimpleName() + " failed");
			// should not happen
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#getVersion()
	 */
	@Override
	public ComparableVersion getVersion() {
		return new ComparableVersion("1");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#getName()
	 */
	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#asSerializable()
	 */
	@Override
	public SerializableWrapperRepositoryObject<? extends IRepositoryObject> asSerializableInternal() {
		// TODO: do we need this?
		return null;
	}
}
