/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
package de.clusteval.data.goldstandard;

import java.io.File;
import java.util.Iterator;

import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.utils.SubDirectoryIterator;

/**
 * @author Christian Wiwie
 * 
 * 
 */
public class RunResultGoldstandardFinder extends GoldStandardFinder {

	/**
	 * 
	 */
	private static final long serialVersionUID = 926325476656736733L;

	/**
	 * Instantiates a new run finder.
	 * 
	 * @param repository
	 *            the repository
	 * @throws RegisterException
	 */
	public RunResultGoldstandardFinder(final IRepository repository) throws RegisterException {
		super(repository);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.Finder#getIterator()
	 */
	@Override
	protected Iterator<File> getIterator() {
		return new SubDirectoryIterator(getBaseDir());
	}
}
