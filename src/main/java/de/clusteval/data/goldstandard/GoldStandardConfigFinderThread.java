/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.data.goldstandard;

import de.clusteval.data.distance.DistanceMeasureFinderThread;
import de.clusteval.data.distance.IDistanceMeasure;
import de.clusteval.data.preprocessing.DataPreprocessorFinderThread;
import de.clusteval.data.preprocessing.IDataPreprocessor;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.threading.ISupervisorThread;
import de.clusteval.utils.FinderThread;
import de.clusteval.utils.IFinder;

/**
 * @author Christian Wiwie
 * 
 */
public class GoldStandardConfigFinderThread extends FinderThread<IGoldStandardConfig> {

	/**
	 * @param supervisorThread
	 * @param repository
	 *            The repository to check for new goldstandard configurations.
	 * @param checkOnce
	 *            If true, this thread only checks once for new goldstandard
	 *            configurations.
	 * 
	 */
	public GoldStandardConfigFinderThread(final ISupervisorThread supervisorThread, final IRepository repository,
			final boolean checkOnce) {
		super(supervisorThread, repository, IGoldStandardConfig.class, 30000, checkOnce);
	}

	/**
	 * @param supervisorThread
	 * @param repository
	 *            The repository to check for new goldstandard configurations.
	 * @param sleepTime
	 *            The time between two checks.
	 * @param checkOnce
	 *            If true, this thread only checks once for new goldstandard
	 *            configurations.
	 * 
	 */
	public GoldStandardConfigFinderThread(final ISupervisorThread supervisorThread, final IRepository repository,
			final long sleepTime, final boolean checkOnce) {
		super(supervisorThread, repository, IGoldStandardConfig.class, sleepTime, checkOnce);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.FinderThread#getFinder()
	 */
	@Override
	public IFinder<IGoldStandardConfig> getFinder() throws RegisterException {
		return new GoldStandardConfigFinder(repository);
	}
	
	/* (non-Javadoc)
	 * @see de.clusteval.utils.FinderThread#beforeFind()
	 */
	@Override
	protected void beforeFind() {
		if (!this.repository.isInitialized(IGoldStandard.class))
			this.supervisorThread.getThread(GoldStandardFinderThread.class).waitFor();

		if (!this.repository.isInitialized(IDistanceMeasure.class))
			this.supervisorThread.getThread(DistanceMeasureFinderThread.class).waitFor();

		if (!this.repository.isInitialized(IDataPreprocessor.class))
			this.supervisorThread.getThread(DataPreprocessorFinderThread.class).waitFor();
		super.beforeFind();
	}
}
