/**
 * 
 */
package de.clusteval.data.goldstandard;

import java.io.File;

import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.framework.repository.SerializableWrapperRepositoryObject;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * @author Christian Wiwie
 *
 */
public class SerializableGoldStandard extends SerializableWrapperRepositoryObject<IGoldStandard> implements ISerializableGoldStandard {

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 */
	public SerializableGoldStandard(File absPath, String name, String version) {
		super(absPath, name, version);
	}

	public SerializableGoldStandard(final IGoldStandard goldStandard) {
		super(goldStandard);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.goldstandard.ISerializableGoldStandard#fuzzySize()
	 */
	@Override
	public float fuzzySize() {
		return this.wrappedComponent.fuzzySize();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.goldstandard.ISerializableGoldStandard#getFullName()
	 */
	@Override
	public String getFullName() {
		return this.wrappedComponent.getFullName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.goldstandard.ISerializableGoldStandard#getMajorName()
	 */
	@Override
	public String getMajorName() {
		return this.wrappedComponent.getMajorName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.goldstandard.ISerializableGoldStandard#getMinorName()
	 */
	@Override
	public String getMinorName() {
		return this.wrappedComponent.getMinorName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.goldstandard.ISerializableGoldStandard#size()
	 */
	@Override
	public int size() {
		return this.wrappedComponent.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.SerializableWrapper#deserializeInternal
	 * (de.clusteval.framework.repository.IRepository)
	 */
	@Override
	protected IGoldStandard deserializeInternal(IRepository repository) throws DeserializationException {
		try {
			return (IGoldStandard) repository.getStaticObjectWithNameAndVersion(IGoldStandard.class,
					name + ":" + version);
		} catch (ObjectNotRegisteredException | ObjectVersionNotRegisteredException e) {
		}
		return new GoldStandard(repository, System.currentTimeMillis(),
				new File(FileUtils.buildPath(repository.getBasePath(IGoldStandard.class), this.name + ".v" + version)));
	}
}
