/**
 * 
 */
package de.clusteval.data.goldstandard;

import java.io.File;

import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.SerializableWrapperRepositoryObject;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * @author Christian Wiwie
 *
 */
public class SerializableGoldStandardConfig
		extends
			SerializableWrapperRepositoryObject<IGoldStandardConfig>
		implements
			ISerializableGoldStandardConfig {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1418845640200117612L;

	private ISerializableWrapperRepositoryObject<IGoldStandard> goldStandard;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 * @param goldStandard
	 */
	public SerializableGoldStandardConfig(File absPath, String name,
			String version, ISerializableWrapperRepositoryObject<IGoldStandard> goldStandard) {
		super(absPath, name, version);
		this.goldStandard = goldStandard;
	}

	public SerializableGoldStandardConfig(final IGoldStandardConfig gsConfig)
			throws RepositoryObjectSerializationException {
		super(gsConfig);

		if (gsConfig.getGoldstandard() != null)
			this.goldStandard = (ISerializableWrapperRepositoryObject) gsConfig
					.getGoldstandard().asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.goldstandard.ISerializableGoldStandardConfig#
	 * getGoldstandard()
	 */
	@Override
	public ISerializableWrapperRepositoryObject<IGoldStandard> getGoldstandard() {
		return goldStandard;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.SerializableWrapper#deserializeInternal
	 * (de.clusteval.framework.repository.IRepository)
	 */
	@Override
	protected IGoldStandardConfig deserializeInternal(IRepository repository)
			throws DeserializationException {
		try {
			return repository.getStaticObjectWithNameAndVersion(
					IGoldStandardConfig.class, name + ":" + version);
		} catch (ObjectNotRegisteredException
				| ObjectVersionNotRegisteredException e) {
		}
		return new GoldStandardConfig(repository, System.currentTimeMillis(),
				new File(FileUtils.buildPath(
						repository.getBasePath(IGoldStandardConfig.class),
						this.name + ".v" + version + ".gsconfig")),
				goldStandard.deserialize(repository));
	}
}
