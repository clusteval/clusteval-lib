/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.data.distance;

import java.io.File;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.DynamicComponentMissingVersionException;
import de.clusteval.utils.IncompatibleClustEvalVersionException;
import de.clusteval.utils.InvalidClustEvalVersionException;
import de.clusteval.utils.InvalidDependencyTargetException;
import de.clusteval.utils.JARFinder;
import de.clusteval.utils.JARFinderClassLoader;
import de.clusteval.utils.MissingClustEvalVersionException;

/**
 * @author Christian Wiwie
 * 
 */
public class DistanceMeasureFinder extends JARFinder<IDistanceMeasure> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7724639960415315913L;

	/**
	 * @param repository
	 * @throws RegisterException
	 */
	public DistanceMeasureFinder(IRepository repository)
			throws RegisterException {
		super(repository, IDistanceMeasure.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.JARFinder#getURLClassLoader0(java.io.File)
	 */
	@Override
	protected URLClassLoader getURLClassLoader0(File f,
			final ClassLoader parent) throws MalformedURLException {
		URL url = f.toURI().toURL();
		return new DistanceMeasureURLClassLoader(this, new URL[]{url}, parent);
	}

}

class DistanceMeasureURLClassLoader
		extends
			JARFinderClassLoader<IDistanceMeasure> {

	/**
	 * @param urls
	 * @param parent
	 * @param loaderParent
	 */
	public DistanceMeasureURLClassLoader(DistanceMeasureFinder parent,
			URL[] urls, ClassLoader loaderParent) {
		super(parent, urls, loaderParent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.ClassLoader#loadClass(java.lang.String)
	 */
	@Override
	public Class<?> loadClass(String name) throws ClassNotFoundException {
		Class<?> result = super.loadClass(name);

		if (Modifier.isAbstract(result.getModifiers()) || result.isInterface())
			return result;

		if (name.startsWith("de.clusteval.data.distance")) {
			if (name.endsWith("DistanceMeasure")) {
				@SuppressWarnings("unchecked")
				Class<? extends DistanceMeasure> distanceMeasure = (Class<? extends DistanceMeasure>) result;

				try {

					// loadClass may have been called not for the newly
					// discovered class of this JAR, but for a parent class
					// which had already been registered before;
					// thus, we only register this class, if it hasn't been
					// registered before
					if (!this.parent.getRepository()
							.isClassRegistered(distanceMeasure)) {
						this.parent.getRepository().registerClass(
								IDistanceMeasure.class,
								(Class<? extends DistanceMeasure>) distanceMeasure);
					}
				} catch (DynamicComponentMissingVersionException
						| DynamicComponentInitializationException
						| IncompatibleClustEvalVersionException
						| MissingClustEvalVersionException
						| InvalidClustEvalVersionException
						| InvalidDependencyTargetException e) {
					handleException(e);
				}
			}
		}
		return result;
	}
}
