/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.data.distance;

import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.threading.ISupervisorThread;
import de.clusteval.utils.FinderThread;

/**
 * @author Christian Wiwie
 * 
 */
public class DistanceMeasureFinderThread extends FinderThread<IDistanceMeasure> {

	/**
	 * @param supervisorThread
	 * @param framework
	 * @param checkOnce
	 */
	public DistanceMeasureFinderThread(final ISupervisorThread supervisorThread, final IRepository framework,
			final boolean checkOnce) {
		super(supervisorThread, framework, IDistanceMeasure.class, 30000, checkOnce);
	}

	/**
	 * @param supervisorThread
	 * @param framework
	 * @param sleepTime
	 * @param checkOnce
	 */
	public DistanceMeasureFinderThread(final ISupervisorThread supervisorThread, final IRepository framework,
			final long sleepTime, final boolean checkOnce) {
		super(supervisorThread, framework, IDistanceMeasure.class, sleepTime, checkOnce);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.FinderThread#getFinder()
	 */
	@Override
	public DistanceMeasureFinder getFinder() throws RegisterException {
		return new DistanceMeasureFinder(repository);
	}

}
