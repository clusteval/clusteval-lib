/**
 * 
 */
package de.clusteval.data.distance;

import java.io.File;

import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RepositoryObjectDeserializationException;
import de.clusteval.framework.repository.SerializableWrapperDynamicComponent;
import de.clusteval.utils.DynamicComponentInitializationException;

public class SerializableDistanceMeasure
		extends
			SerializableWrapperDynamicComponent<IDistanceMeasure> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8410611015712391810L;

	/**
	 * @param type
	 * @param absPath
	 * @param name
	 * @param version
	 */
	public SerializableDistanceMeasure(File absPath, String name,
			String version, final String alias) {
		super(IDistanceMeasure.class, absPath, name, version, alias);
	}

	/**
	 * @param wrappedComponent
	 */
	public SerializableDistanceMeasure(IDistanceMeasure wrappedComponent) {
		super(IDistanceMeasure.class, wrappedComponent);
	}

	@Override
	protected IDistanceMeasure deserializeInternal(final IRepository repository)
			throws DeserializationException {
		try {
			return DistanceMeasure.parseFromString(repository, this.name);
		} catch (UnknownDistanceMeasureException
				| DynamicComponentInitializationException e) {
			throw new RepositoryObjectDeserializationException(
					IDistanceMeasure.class, this.name, e);
		}
	}
}