/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.data.distance;

import java.io.File;
import java.lang.reflect.InvocationTargetException;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.data.dataset.format.IConversionInputToStandardConfiguration;
import de.clusteval.framework.RLibraryRequirement;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.Repository;
import de.clusteval.framework.repository.RepositoryObjectDynamicComponent;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.SerializableWrapperDynamicComponent;
import de.clusteval.program.r.RLibraryInferior;
import de.clusteval.utils.ClustEvalAlias;
import de.clusteval.utils.ClassVersion;
import de.clusteval.utils.ClassVersionRequirement;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.DynamicComponentVersion;
import de.clusteval.utils.RNotAvailableException;
import dk.sdu.imada.compbio.utils.Pair;
import dk.sdu.imada.compbio.utils.SimilarityMatrix;

//@formatter:off
/**
 * 
 * 
 * ## Writing Distance Measures
 * 
 * 
 * A distance measure MyDistanceMeasure can be added to ClustEval by
 * 
 * 1. extending the {@link DistanceMeasure} class with your own class `MyDistanceMeasure`.
 *     - Your class has to be decorated with the following annotations:
 *         - {@link DynamicComponentVersion}: Any component you add to ClustEval needs to have a version. This is needed to ensure, that results are fully reproducible by running runs using exactly the same versions as previously
 *         - {@link ClassVersionRequirement}: Any component you add to ClustEval needs to specify, which dependencies it has on the ClustEval interfaces. This information is used to ensure compatibility between the version of your component and the used ClustEval version. Whenever core interfaces in the ClustEval API change, their interface version is incremented. By using this annotation it can detect, which components are still compatible.
 *         - {@link ClustEvalAlias}: You need to provide a readable alias for your component. This alias is used when results and components should be presented in a accessible way.
 *         - [Optional] {@link RLibraryRequirement}: If your component depends on R libraries being installed and loaded when your program is being executed, it needs to be decorated with this annotation.
 *     - You have to provide your own implementations for the following methods, otherwise the framework will not be able to load your component: 
 *         * {@link #DistanceMeasure(IRepository, long, File)}: The constructor for your distance measure. This constructor has to be implemented and public, otherwise the framework will not be able to load your distance measure.
 *         * {@link #DistanceMeasure(DistanceMeasure)}: The copy constructor for your distance measure. This constructor has to be implemented and public, otherwise the framework will not be able to load your distance measure.
 *         * {@link #isSymmetric()}: Whether your distance measure is symmetric. If it is, ClustEval will invoke your {@link #getDistance(double[], double[])} function only ones for each pair of objects. 
 *         * {@link #supportsMatrix()}: This method indicates, whether your distance measure can calculate distances of a whole set of point-pairs, i.e. your distance measure implements the method getDistances(double[][]).
 *         * {@link #getDistances(IConversionInputToStandardConfiguration, double[][])}: 
 *         * {@link #getDistance(double[], double[])}: The absolute coordinates of the points are stored row-wise in the given matrix and distances are calculated between every pair of rows. Position [i][j] of the returned double[][] matrix contains the distance between the i-th and j-th row of the input matrix.
 * 4. Creating a jar file named `MyDistanceMeasure.jar` containing the `MyDistanceMeasure.class` compiled on your machine in the correct folder structure corresponding to the packages: `de/clusteval/data/distance/MyDistanceMeasure.class`
 * 5. Putting the `MyDistanceMeasure.jar` into the distance measure folder of the repository: `<REPOSITORY ROOT>/supp/distanceMeasures`
 *   
 * The backend server will recognize and try to load the new distance measure automatically the next time, the `DistanceMeasureFinderThread` checks the filesystem.
 * 
 * @author Christian Wiwie
 * 
 *
 *         <dl>
 *         <dt>2.1</dt>
 *         <dd>Using compbio utils instead of Wiutils</dd>
 *         <dt>&#60; 2.1</dt>
 *         <dd>...</dd>
 *         </dl>
 */
//@formatter:on
@ClassVersion(version = "2.1")
public abstract class DistanceMeasure extends RepositoryObjectDynamicComponent
		implements
			RLibraryInferior,
			IDistanceMeasure {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7061681616501949309L;

	/**
	 * @param repository
	 * @param changeDate
	 * @param absPath
	 */
	public DistanceMeasure(IRepository repository, long changeDate,
			File absPath) {
		super(repository, changeDate, absPath);
	}

	/**
	 * The copy constructor of this distance measures.
	 * 
	 * @param other
	 *            The object to clone.
	 */
	public DistanceMeasure(final DistanceMeasure other) {
		super(other);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.distance.IDistanceMeasure#clone()
	 */
	@Override
	public final DistanceMeasure clone() {
		try {
			return this.getClass().getConstructor(this.getClass())
					.newInstance(this);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		this.log.warn("Cloning instance of class "
				+ this.getClass().getSimpleName() + " failed");
		return null;
	}

	/**
	 * Parses the from string.
	 * 
	 * @param repository
	 *            the repository
	 * @param distanceMeasure
	 *            the distance measure
	 * @return the distance measure
	 * @throws UnknownDistanceMeasureException
	 * @throws DynamicComponentInitializationException
	 */
	public static IDistanceMeasure parseFromString(final IRepository repository,
			String distanceMeasure) throws UnknownDistanceMeasureException,
			DynamicComponentInitializationException {
		Class<? extends IDistanceMeasure> c = repository
				.resolveAndGetRegisteredClass(IDistanceMeasure.class,
						distanceMeasure, true);
		if (c == null) {
			Pair<String, ComparableVersion> fullClassNameAndVersion = repository
					.getFullClassNameAndVersion(IDistanceMeasure.class,
							distanceMeasure, true);
			if (fullClassNameAndVersion.getSecond() != null
					&& repository.hasError(IDistanceMeasure.class,
							fullClassNameAndVersion.getFirst(),
							fullClassNameAndVersion.getSecond().toString()))
				throw new UnknownDistanceMeasureException(distanceMeasure,
						repository.getErrors(IDistanceMeasure.class,
								fullClassNameAndVersion.getFirst(),
								fullClassNameAndVersion.getSecond().toString())
								.iterator().next());
			else
				throw new UnknownDistanceMeasureException(distanceMeasure,
						"Unknown distance measure");
		}
		return getInstance(repository, distanceMeasure, c);
	}

	protected static IDistanceMeasure getInstance(final IRepository repository,
			String distanceMeasure, Class<? extends IDistanceMeasure> c)
			throws DynamicComponentInitializationException {
		try {
			IDistanceMeasure measure = c
					.getConstructor(IRepository.class, long.class, File.class)
					.newInstance(repository, System.currentTimeMillis(),
							new File(distanceMeasure));

			return measure;
		} catch (NoSuchMethodException e) {
			throw new DynamicComponentInitializationException(
					IDistanceMeasure.class, distanceMeasure,
					"The class is missing the correct constructor");
		} catch (Exception e) {
			// this is likely because we passed faked parameters
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.distance.IDistanceMeasure#getDistance(double[],
	 * double[])
	 */
	@Override
	public abstract double getDistance(double[] point1, double[] point2)
			throws RNotAvailableException, InterruptedException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.distance.IDistanceMeasure#supportsMatrix()
	 */
	@Override
	public abstract boolean supportsMatrix();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.distance.IDistanceMeasure#isSymmetric()
	 */
	@Override
	public abstract boolean isSymmetric();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.distance.IDistanceMeasure#getDistances(de.clusteval.
	 * data.dataset.format.ConversionInputToStandardConfiguration, double[][])
	 */
	@Override
	public abstract SimilarityMatrix getDistances(
			IConversionInputToStandardConfiguration config, double[][] matrix)
			throws RNotAvailableException, InterruptedException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.distance.IDistanceMeasure#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		return this.getClass().equals(obj.getClass());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.distance.IDistanceMeasure#hashCode()
	 */
	@Override
	public int hashCode() {
		return this.getClass().hashCode();
	}

	@Override
	public ComparableVersion getVersion() {
		try {
			return Repository.getVersionOfObjectDynamicClass(this);
		} catch (Exception e) {
			// cannot happen because we loaded the class before;
			e.printStackTrace();
			return new ComparableVersion("1");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#getName()
	 */
	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObjectDynamicComponent#
	 * asSerializable()
	 */
	@Override
	public SerializableWrapperDynamicComponent<IDistanceMeasure> asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableWrapperDynamicComponent<IDistanceMeasure>) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#asSerializable()
	 */
	@Override
	public SerializableWrapperDynamicComponent<IDistanceMeasure> asSerializableInternal() {
		return new SerializableDistanceMeasure(this);
	}

	@Override
	public boolean isSerializable() {
		return false;
	};
}
