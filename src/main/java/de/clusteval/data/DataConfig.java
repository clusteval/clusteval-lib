/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
package de.clusteval.data;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration.HierarchicalINIConfiguration;
import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.data.dataset.DataSetConfig;
import de.clusteval.data.dataset.IDataSetConfig;
import de.clusteval.data.goldstandard.GoldStandardConfig;
import de.clusteval.data.goldstandard.IGoldStandardConfig;
import de.clusteval.framework.repository.DumpableRepositoryObject;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.Repository;
import de.clusteval.framework.repository.RepositoryEvent;
import de.clusteval.framework.repository.RepositoryObjectDumpException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.RepositoryRemoveEvent;
import de.clusteval.framework.repository.RepositoryReplaceEvent;

/**
 * A data configuration encapsulates options and settings for all kinds of data
 * (dataset and goldstandard). During the execution of a run, when programs are
 * applied to datasets and goldstandards, settings are required that control the
 * behaviour of how this data has to be handled.
 * 
 * <p>
 * A data configuration corresponds to and is parsed from a file on the
 * filesystem in the corresponding folder of the repository (see
 * {@link Repository#dataConfigBasePath} and {@link DataConfigFinder}).
 * 
 * <p>
 * There are several options, that can be specified in the data configuration
 * file (see {@link #parseFromFile(File)}).
 * 
 * 
 * @author Christian Wiwie
 * 
 */
public class DataConfig extends DumpableRepositoryObject
		implements
			IDataConfig {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8950348636129983014L;

	/**
	 * A helper method for cloning a list of data configurations.
	 * 
	 * @param dataConfigs
	 *            The list of data configurations to clone.
	 * @return The list containing the cloned objects of the input list.
	 */
	public static List<IDataConfig> cloneDataConfigurations(
			final List<IDataConfig> dataConfigs) {
		List<IDataConfig> result = new ArrayList<IDataConfig>();

		for (IDataConfig dataConfig : dataConfigs) {
			result.add(dataConfig.clone());
		}

		return result;
	}

	/**
	 * A data configuration encapsulates a dataset configuration. This dataset
	 * configuration contains options and settings how to handle the
	 * encapsulated dataset.
	 */
	protected IDataSetConfig dataSetConfig;

	/**
	 * The original dataset config. is only != null, if the corresponding
	 * dataset was converted into a different format
	 */
	protected IDataSetConfig standardDataSetConfig;

	/**
	 * A data configuration encapsulates a goldstandard configuration. This
	 * goldstandard configuration contains options and settings how to handle
	 * the encapsulated goldstandard.
	 */
	protected IGoldStandardConfig goldstandardConfig;

	/**
	 * Instantiates a new data configuration.
	 * 
	 * @param repository
	 *            The repository this data configuration should be registered
	 *            at.
	 * @param changeDate
	 *            The change date of this data configuration is used for
	 *            equality checks.
	 * @param absPath
	 *            The absolute path of this data configuration.
	 * @param datasetConfig
	 *            The dataset configuration encapsulated by this data
	 *            configuration.
	 * @param gsConfig
	 *            The goldstandard configuration encapsulated by this data
	 *            configuration.
	 */
	public DataConfig(final IRepository repository, final long changeDate,
			final File absPath, final IDataSetConfig datasetConfig,
			final IGoldStandardConfig gsConfig) {
		super(repository, changeDate, absPath);

		this.dataSetConfig = datasetConfig;
		this.goldstandardConfig = gsConfig;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#register()
	 */
	@Override
	public boolean register() throws RegisterException {
		if (super.register()) {
			this.dataSetConfig.addListener(this);
			if (this.hasGoldStandardConfig())
				this.goldstandardConfig.addListener(this);
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.IDataConfig#hasGoldStandardConfig()
	 */
	@Override
	public boolean hasGoldStandardConfig() {
		return this.goldstandardConfig != null;
	}

	/**
	 * The copy constructor for data configurations. This copy constructor
	 * creates a deep copy by not only copying the references of attribute
	 * variables, but also cloning those.
	 * 
	 * @param dataConfig
	 *            the data config
	 */
	public DataConfig(DataConfig dataConfig) {
		super(dataConfig);

		this.dataSetConfig = dataConfig.dataSetConfig.clone();
		this.goldstandardConfig = dataConfig.hasGoldStandardConfig()
				? dataConfig.goldstandardConfig.clone()
				: null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.repository.RepositoryObject#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.IDataConfig#clone()
	 */
	@Override
	public IDataConfig clone() {
		return new DataConfig(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.IDataConfig#getDatasetConfig()
	 */
	@Override
	public IDataSetConfig getDatasetConfig() {
		return dataSetConfig;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.IDataConfig#getGoldstandardConfig()
	 */
	@Override
	public IGoldStandardConfig getGoldstandardConfig() {
		return goldstandardConfig;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.IDataConfig#getName()
	 */
	@Override
	public String getName() {
		return this.absPath.getName().replaceAll(
				"(" + getAbsPathVersionMatchString() + "?).dataconfig$", "");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.wiwie.wiutils.utils.RepositoryObject#notify(utils.RepositoryEvent)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.IDataConfig#notify(de.clusteval.framework.repository.
	 * RepositoryEvent)
	 */
	@Override
	public void notify(RepositoryEvent e) throws RegisterException {
		if (e instanceof RepositoryReplaceEvent) {
			RepositoryReplaceEvent event = (RepositoryReplaceEvent) e;
			if (event.getOld().equals(this))
				super.notify(event);
			else {
				if (event.getOld().equals(dataSetConfig)) {
					event.getOld().removeListener(this);
					this.log.info("DataConfig " + this
							+ ": DataSetConfig reloaded due to modifications in filesystem");
					event.getReplacement().addListener(this);
					this.dataSetConfig = (DataSetConfig) event.getReplacement();
				} else if (event.getOld().equals(goldstandardConfig)) {
					event.getOld().removeListener(this);
					this.log.info("DataConfig " + this
							+ ": GoldStandardConfig reloaded due to modifications in filesystem");
					event.getReplacement().addListener(this);
					this.goldstandardConfig = (GoldStandardConfig) event
							.getReplacement();
				} else if (event.getOld().equals(standardDataSetConfig)) {
					event.getOld().removeListener(this);
					this.log.info("DataConfig " + this
							+ ": Original DataSet reloaded due to modifications in filesystem");
					event.getReplacement().addListener(this);
					this.standardDataSetConfig = (IDataSetConfig) event
							.getReplacement();
				}
			}
		} else if (e instanceof RepositoryRemoveEvent) {
			RepositoryRemoveEvent event = (RepositoryRemoveEvent) e;
			if (event.getRemovedObject().equals(this))
				super.notify(event);
			else {
				if (event.getRemovedObject().equals(dataSetConfig)) {
					event.getRemovedObject().removeListener(this);
					this.log.info("DataConfig " + this
							+ ": Removed, because DataSetConfig "
							+ dataSetConfig + " was removed.");
					RepositoryRemoveEvent newEvent = new RepositoryRemoveEvent(
							this);
					this.unregister();
					this.notify(newEvent);
				} else if (event.getRemovedObject()
						.equals(goldstandardConfig)) {
					event.getRemovedObject().removeListener(this);
					this.log.info("DataConfig " + this
							+ ": Removed, because GoldStandardConfig "
							+ goldstandardConfig + " was removed.");
					RepositoryRemoveEvent newEvent = new RepositoryRemoveEvent(
							this);
					this.unregister();
					this.notify(newEvent);
				} else if (event.getRemovedObject()
						.equals(standardDataSetConfig)) {
					event.getRemovedObject().removeListener(this);
					this.log.info("DataConfig " + this
							+ ": Removed, because Standard-DataSetConfig "
							+ standardDataSetConfig + " was removed.");
					RepositoryRemoveEvent newEvent = new RepositoryRemoveEvent(
							this);
					this.unregister();
					this.notify(newEvent);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.DumpableRepositoryObject#
	 * dumpToFileHelper ()
	 */
	@Override
	protected void dumpToFileHelper(final HierarchicalINIConfiguration conf)
			throws RepositoryObjectDumpException {
		conf.addProperty("version", this.getVersion());
		conf.addProperty("datasetConfig", this.dataSetConfig.toString());
		if (this.hasGoldStandardConfig())
			conf.addProperty("goldstandardConfig",
					this.goldstandardConfig.toString());
	}

	/**
	 * @return the version
	 */
	@Override
	public ComparableVersion getVersion() {
		String name = absPath.getName().replaceAll(".dataconfig", "");
		int colonInd = name.lastIndexOf(".");
		return new ComparableVersion(name.substring(colonInd + 2));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#asSerializable()
	 */
	@Override
	public SerializableDataConfig asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableDataConfig) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#asSerializable()
	 */
	@Override
	public SerializableDataConfig asSerializableInternal()
			throws RepositoryObjectSerializationException {
		return new SerializableDataConfig(this);
	}

	/**
	 * @return the standardDataSetConfig
	 */
	@Override
	public IDataSetConfig getStandardDataSetConfig() {
		return standardDataSetConfig;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#isSerializable()
	 */
	@Override
	public boolean isSerializable() {
		return true;
	}
}
