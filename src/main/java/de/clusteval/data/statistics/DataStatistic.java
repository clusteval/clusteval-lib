/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.data.statistics;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.List;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.Repository;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.run.DataAnalysisRun;
import de.clusteval.utils.ClassVersion;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.Statistic;
import dk.sdu.imada.compbio.utils.Pair;

/**
 * A data statistic is a {@link Statistic}, which summarizes properties of data
 * sets. Data statistics are assessed by a {@link DataAnalysisRun}.
 * <p/>
 * 
 * 
 * {@code
 * 
 * 
 * A data statistic MyDataStatistic can be added to ClustEval by
 * 
 * 1. extending the class :java:ref:`DataStatistic` with your own class MyDataStatistic. You have to provide your own implementations for the following methods, otherwise the framework will not be able to load your class.
 * 
 *   * :java:ref:`DataStatistic(Repository, boolean, long, File)` : The constructor for your data statistic. This constructor has to be implemented and public.
 *   * :java:ref:`DataStatistic(MyDataStatistic)` : The copy constructor for your data statistic. This constructor has to be implemented and public.
 *   * :java:ref:`Statistic.getAlias()` : See :java:ref:`Statistic.getAlias()`.
 *   * :java:ref:`Statistic.parseFromString(String)` : See :java:ref:`Statistic.parseFromString(String)`.
 *   
 * 2. extending the class :java:ref:`DataStatisticCalculator` with your own class MyDataStatisticCalculator . You have to provide your own implementations for the following methods.
 * 
 *   * :java:ref:`DataStatisticCalculator(Repository, long, File, DataConfig)` : The constructor for your data statistic calculator. This constructor has to be implemented and public.
 *   * :java:ref:`DataStatisticCalculator(MyDataStatisticCalculator)` : The copy constructor for your data statistic calculator. This constructor has to be implemented and public.
 *   * :java:ref:`DataStatisticCalculator.calculateResult()`: See :java:ref:`StatisticCalculator.calculateResult()`.
 *   * :java:ref:`StatisticCalculator.writeOutputTo(File)`: See :java:ref:`StatisticCalculator.writeOutputTo(File)`.
 *   
 * 3. Creating a jar file named MyDataStatisticCalculator.jar containing the MyDataStatistic.class and MyDataStatisticCalculator.class compiled on your machine in the correct folder structure corresponding to the packages:
 * 
 *   * de/clusteval/run/statistics/MyDataStatistic.class
 *   * de/clusteval/run/statistics/MyDataStatisticCalculator.class
 *   
 * 4. Putting the MyDataStatistic.jar into the data statistics folder of the repository:
 * 
 *   * <REPOSITORY ROOT>/supp/statistics/data
 *   * The backend server will recognize and try to load the new data statistics automatically the next time, the DataStatisticFinderThread checks the filesystem.
 * 
 * }
 * 
 * @author Christian Wiwie
 * 
 */
@ClassVersion(version = "1")
public abstract class DataStatistic extends Statistic
		implements
			IDataStatistic {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3242418090715282277L;

	/**
	 * @param repository
	 * @param changeDate
	 * @param absPath
	 * @throws RegisterException
	 */
	public DataStatistic(IRepository repository, long changeDate, File absPath)
			throws RegisterException {
		super(repository, changeDate, absPath);
	}

	/**
	 * The copy constructor of data statistics.
	 * 
	 * @param other
	 *            The object to clone.
	 * @throws RegisterException
	 */
	public DataStatistic(final DataStatistic other) throws RegisterException {
		super(other);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.statistics.IDataStatistic#clone()
	 */
	@Override
	public final DataStatistic clone() {
		try {
			return this.getClass().getConstructor(this.getClass())
					.newInstance(this);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		this.log.warn("Cloning instance of class "
				+ this.getClass().getSimpleName() + " failed");
		return null;
	}

	/**
	 * This method parses a string and maps it to an instance of a
	 * {@link DataStatistic} looking its class up in the given repository.
	 * 
	 * @param repository
	 *            The repository to look for the classes.
	 * @param dataStatistic
	 *            The string representation of a data statistic subclass.
	 * @return A subclass of {@link DataStatistic}.
	 * @throws UnknownDataStatisticException
	 * @throws DynamicComponentInitializationException
	 */
	public static IDataStatistic parseFromString(final IRepository repository,
			String dataStatistic) throws UnknownDataStatisticException,
			DynamicComponentInitializationException {
		Class<? extends IDataStatistic> c = repository
				.resolveAndGetRegisteredClass(IDataStatistic.class,
						dataStatistic, true);
		if (c == null) {
			Pair<String, ComparableVersion> fullClassNameAndVersion = repository
					.getFullClassNameAndVersion(IDataStatistic.class,
							dataStatistic, true);
			if (fullClassNameAndVersion.getSecond() != null
					&& repository.hasError(IDataStatistic.class,
							fullClassNameAndVersion.getFirst(),
							fullClassNameAndVersion.getSecond().toString()))
				throw new UnknownDataStatisticException(dataStatistic,
						repository.getErrors(IDataStatistic.class,
								fullClassNameAndVersion.getFirst(),
								fullClassNameAndVersion.getSecond().toString())
								.iterator().next());
			else
				throw new UnknownDataStatisticException(dataStatistic,
						"Unknown data statistic.");
		}
		return getInstance(repository, dataStatistic, c);
	}

	protected static IDataStatistic getInstance(final IRepository repository,
			String dataStatistic, Class<? extends IDataStatistic> c)
			throws UnknownDataStatisticException,
			DynamicComponentInitializationException {
		try {
			IDataStatistic statistic = c
					.getConstructor(IRepository.class, long.class, File.class)
					.newInstance(repository, System.currentTimeMillis(),
							new File(dataStatistic));

			return statistic;
		} catch (Exception e2) {
			// fallback
			try {
				IDataStatistic statistic = c.getConstructor(Repository.class,
						long.class, File.class).newInstance(repository,
								System.currentTimeMillis(),
								new File(dataStatistic));

				return statistic;
			} catch (NoSuchMethodException e) {
				throw new DynamicComponentInitializationException(
						IDataStatistic.class, dataStatistic,
						"The class is missing the correct constructor");
			} catch (Exception e) {
				// this is likely because we passed faked parameters
				return null;
			}
		}
	}

	/**
	 * This method parses several strings and maps them to instances of
	 * {@link DataStatistic} looking their classes up in the given repository.
	 * 
	 * @param repo
	 *            The repository to look for the classes.
	 * @param dataStatistics
	 *            The string representation of a data statistic subclass.
	 * @return A subclass of {@link DataStatistic}.
	 * @throws UnknownDataStatisticException
	 * @throws DynamicComponentInitializationException
	 */
	public static List<IDataStatistic> parseFromString(final IRepository repo,
			String[] dataStatistics) throws UnknownDataStatisticException,
			DynamicComponentInitializationException {
		List<IDataStatistic> result = new LinkedList<IDataStatistic>();
		for (String dataStatistic : dataStatistics) {
			result.add(parseFromString(repo, dataStatistic));
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.statistics.IDataStatistic#requiresGoldStandard()
	 */
	@Override
	public abstract boolean requiresGoldStandard();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.Statistic#asSerializable()
	 */
	@Override
	public SerializableDataStatistic asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableDataStatistic) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.Statistic#asSerializable()
	 */
	@Override
	public SerializableDataStatistic asSerializableInternal() {
		return new SerializableDataStatistic(this);
	}
}
