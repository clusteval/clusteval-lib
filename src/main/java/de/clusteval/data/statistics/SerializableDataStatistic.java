/**
 * 
 */
package de.clusteval.data.statistics;

import java.io.File;

import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RepositoryObjectDeserializationException;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.SerializableStatistic;

public class SerializableDataStatistic
		extends
			SerializableStatistic<IDataStatistic> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8936645729232827025L;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 */
	public SerializableDataStatistic(File absPath, String name, String version,
			final String alias) {
		super(IDataStatistic.class, absPath, name, version, alias);
	}

	/**
	 * @param wrappedComponent
	 */
	public SerializableDataStatistic(IDataStatistic wrappedComponent) {
		super(IDataStatistic.class, wrappedComponent);
	}

	@Override
	protected IDataStatistic deserializeInternal(final IRepository repository)
			throws DeserializationException {
		try {
			return DataStatistic.parseFromString(repository, this.name);
		} catch (UnknownDataStatisticException
				| DynamicComponentInitializationException e) {
			throw new RepositoryObjectDeserializationException(
					IDataStatistic.class, this.name, e);
		}
	}
}