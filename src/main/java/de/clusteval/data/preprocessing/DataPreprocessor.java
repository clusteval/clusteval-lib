/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.data.preprocessing;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.data.dataset.DataSet;
import de.clusteval.data.dataset.IDataSet;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.Repository;
import de.clusteval.framework.repository.RepositoryObjectDynamicComponent;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.SerializableWrapperDynamicComponent;
import de.clusteval.program.r.RLibraryInferior;
import de.clusteval.utils.ClustEvalAlias;
import de.clusteval.utils.ClassVersion;
import de.clusteval.utils.ClassVersionRequirement;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.DynamicComponentVersion;
import dk.sdu.imada.compbio.utils.Pair;

//@formatter:off
/**
 * 
 * A data preprocessor `MyDataPreprocessor` can be added to ClustEval by
 * 
 * 1. extending the {@link DataPreprocessor} class with your own class `MyDataPreprocessor`.
 *     - Your class has to be decorated with the following annotations:
 *         - {@link DynamicComponentVersion}: Any component you add to ClustEval needs to have a version. This is needed to ensure, that results are fully reproducible by running runs using exactly the same versions as previously
 *         - {@link ClassVersionRequirement}: Any component you add to ClustEval needs to specify, which dependencies it has on the ClustEval interfaces. This information is used to ensure compatibility between the version of your component and the used ClustEval version. Whenever core interfaces in the ClustEval API change, their interface version is incremented. By using this annotation it can detect, which components are still compatible.
 *         - {@link ClustEvalAlias}: You need to provide a readable alias for your component. This alias is used when results and components should be presented in a accessible way.
 *     - You have to provide your own implementations for the following methods, otherwise the framework will not be able to load your component:
 *         * {@link #DataPreprocessor(IRepository, long, File)}: The constructor of your class. This constructor has to be implemented and public, otherwise the framework will not be able to load your runresult format.
 *         * {@link #DataPreprocessor(DataPreprocessor)}: The copy constructor of your class taking another instance of your class. This constructor has to be implemented and public.
 *         * {@link #getCompatibleDataSetFormats()}: Returns a set of data formats that this preprocessor can be applied to.
 *         * {@link #preprocess(IDataSet)}: This is the core of your preprocessor; it takes the input data set and returns a wrapper object of the preprocessed one.
 * 2. Creating a jar file named `MyDataPreprocessor.jar` containing the `MyDataPreprocessor.class` compiled on your machine in the correct folder structure corresponding to the packages: `de/clusteval/data/preprocessing/MyDataPreprocessor.class`
 * 3. Putting the `MyRunResultFormat.jar` into the corresponding folder of the repository: `<REPOSITORY ROOT>/supp/preprocessing`
 *   
 *   
 * The backend server will recognize and try to load the new runresult format automatically the next time, the {@link DataPreprocessorFinderThread} checks the filesystem.
 * 
 * @author Christian Wiwie
 * 
 */
//@formatter:off
@ClassVersion(version = "2")
public abstract class DataPreprocessor extends RepositoryObjectDynamicComponent
		implements
			RLibraryInferior,
			IDataPreprocessor {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6992278273092585333L;

	/**
	 * @param repository
	 * @param changeDate
	 * @param absPath
	 * @throws RegisterException
	 */
	public DataPreprocessor(IRepository repository, long changeDate,
			File absPath) throws RegisterException {
		super(repository, changeDate, absPath);
	}

	/**
	 * The copy constructor of data preprocessors.
	 * 
	 * @param other
	 *            The object to clone.
	 * @throws RegisterException
	 */
	public DataPreprocessor(DataPreprocessor other) throws RegisterException {
		super(other);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.repository.RepositoryObject#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.preprocessing.IDataPreprocessor#clone()
	 */
	@Override
	public IDataPreprocessor clone() {
		try {
			return this.getClass().getConstructor(this.getClass())
					.newInstance(this);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		this.log.warn("Cloning instance of class "
				+ this.getClass().getSimpleName() + " failed");
		return null;
	}

	/**
	 * Parses a list of data preprocessors from a string array.
	 * 
	 * @param repository
	 *            the repository
	 * @param dataPreprocessors
	 *            The array containing simple names of the data preprocessor
	 *            class.
	 * @return A list containing data preprocessors.
	 * @throws UnknownDataPreprocessorException
	 * @throws InvalidDataPreprocessorOptionsException
	 * @throws DynamicComponentInitializationException
	 */
	public static List<IDataPreprocessor> parseFromString(
			final IRepository repository, String[] dataPreprocessors,
			List<String[]> cliArguments)
			throws UnknownDataPreprocessorException,
			InvalidDataPreprocessorOptionsException,
			DynamicComponentInitializationException {
		List<IDataPreprocessor> result = new ArrayList<IDataPreprocessor>();

		for (int i = 0; i < dataPreprocessors.length; i++) {
			String s = dataPreprocessors[i];
			String[] cli = cliArguments.get(i);
			result.add(parseFromString(repository, s, cli));
		}

		return result;
	}

	/**
	 * Parses a data preprocessor from string.
	 * 
	 * @param repository
	 *            the repository
	 * @param dataPreprocessor
	 *            The simple name of the data preprocessor class.
	 * @return the data preprocessor
	 * @throws UnknownDataPreprocessorException
	 * @throws InvalidDataPreprocessorOptionsException
	 * @throws DynamicComponentInitializationException
	 */
	public static IDataPreprocessor parseFromString(
			final IRepository repository, String dataPreprocessor,
			final String[] cliArguments)
			throws UnknownDataPreprocessorException,
			InvalidDataPreprocessorOptionsException,
			DynamicComponentInitializationException {

		Class<? extends IDataPreprocessor> c = repository
				.resolveAndGetRegisteredClass(IDataPreprocessor.class,
						dataPreprocessor, true);
		if (c == null) {
			Pair<String, ComparableVersion> fullClassNameAndVersion = repository
					.getFullClassNameAndVersion(IDataPreprocessor.class,
							dataPreprocessor, true);
			if (fullClassNameAndVersion.getSecond() != null
					&& repository.hasError(IDataPreprocessor.class,
					fullClassNameAndVersion.getFirst(),
					fullClassNameAndVersion.getSecond().toString()))
				throw new UnknownDataPreprocessorException(
						dataPreprocessor,
						repository.getErrors(IDataPreprocessor.class,
								fullClassNameAndVersion.getFirst(),
								fullClassNameAndVersion.getSecond().toString())
								.iterator().next());
			else
				throw new UnknownDataPreprocessorException(dataPreprocessor,
					"Unknown data preprocessor.");
		}
		return getInstance(repository, dataPreprocessor, cliArguments, c);
	}

	protected static IDataPreprocessor getInstance(final IRepository repository,
			String dataPreprocessor, final String[] cliArguments,
			Class<? extends IDataPreprocessor> c)
			throws UnknownDataPreprocessorException,
			InvalidDataPreprocessorOptionsException,
			DynamicComponentInitializationException {
		try {
			IDataPreprocessor preprocessor = c
					.getConstructor(IRepository.class, long.class, File.class)
					.newInstance(repository, System.currentTimeMillis(),
							new File(dataPreprocessor));
			preprocessor.register();
			// TODO: init cli arguments
			CommandLineParser parser = new PosixParser();
			Options options = preprocessor.getAllOptions();
			CommandLine cmd = parser.parse(options, cliArguments);

			preprocessor.handleOptions(cmd);
			return preprocessor;
		} catch (NoSuchMethodException e) {
			throw new DynamicComponentInitializationException(
					IDataPreprocessor.class, dataPreprocessor,
					"The class is missing the correct constructor");
		} catch (Exception e) {
			// this is likely because we passed faked parameters
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.preprocessing.IDataPreprocessor#
	 * getCompatibleDataSetFormats()
	 */
	@Override
	public abstract Set<String> getCompatibleDataSetFormats();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.data.preprocessing.IDataPreprocessor#preprocess(de.clusteval
	 * .data.dataset.IDataSet)
	 */
	@Override
	public abstract DataSet preprocess(final IDataSet dataSet)
			throws InterruptedException;

	/**
	 * @return A wrapper object keeping the options of your dataset generator.
	 *         The options returned by this method are going to be used and
	 *         interpreted in your subclass implementation in
	 *         {@link #preprocess()} .
	 */
	protected abstract Options getOptions();

	/**
	 * This method is reponsible for interpreting the arguments passed to this
	 * generator call and to initialize possibly needed member variables.
	 * 
	 * <p>
	 * If you want to react to certain options in your implementation of
	 * {@link #preprocess()}, initialize member variables in this method.
	 * 
	 * @param cmd
	 *            A wrapper object for the arguments passed to this generator.
	 * @throws ParseException
	 */
	@Override
	public abstract void handleOptions(final CommandLine cmd)
			throws ParseException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.data.preprocessing.IDataPreprocessor#getAllOptions()
	 */
	@Override
	public Options getAllOptions() {
		// options of actual generator implementation
		Options options = this.getOptions();

		// default options of all generators
		this.addDefaultOptions(options);
		return options;
	}

	/**
	 * Adds the default options of dataset generators to the given Options
	 * attribute
	 * 
	 * @param options
	 *            The existing Options attribute, holding already the options of
	 *            the actual generator implementation.
	 */
	private void addDefaultOptions(final Options options) {
	}

	@Override
	public ComparableVersion getVersion() {
		try {
			return Repository.getVersionOfObjectDynamicClass(this);
		} catch (Exception e) {
			// cannot happen because we loaded the class before;
			e.printStackTrace();
			return new ComparableVersion("1");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#getName()
	 */
	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObjectDynamicComponent#
	 * asSerializable()
	 */
	@Override
	public SerializableWrapperDynamicComponent<IDataPreprocessor> asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableWrapperDynamicComponent<IDataPreprocessor>) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#asSerializable()
	 */
	@Override
	public SerializableWrapperDynamicComponent<IDataPreprocessor> asSerializableInternal() {
		return new SerializableDataPreprocessor(this);
	}
}
