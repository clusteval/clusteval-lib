/**
 * 
 */
package de.clusteval.data.preprocessing;

import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RepositoryObjectDeserializationException;
import de.clusteval.framework.repository.SerializableWrapperDynamicComponent;
import de.clusteval.utils.DynamicComponentInitializationException;

public class SerializableDataPreprocessor extends SerializableWrapperDynamicComponent<IDataPreprocessor> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5377814177315569553L;

	/**
	 * @param wrappedComponent
	 */
	public SerializableDataPreprocessor(IDataPreprocessor wrappedComponent) {
		super(IDataPreprocessor.class, wrappedComponent);
	}

	@Override
	protected IDataPreprocessor deserializeInternal(final IRepository repository) throws DeserializationException {
		// TODO: no arguments?
		try {
			return DataPreprocessor.parseFromString(repository, this.name, new String[0]);
		} catch (InvalidDataPreprocessorOptionsException e) {
			throw new RepositoryObjectDeserializationException(IDataPreprocessor.class, this.name,
					new DynamicComponentInitializationException(IDataPreprocessor.class, this.name + ":" + this.version,
							e));
		} catch (UnknownDataPreprocessorException | DynamicComponentInitializationException e) {
			throw new RepositoryObjectDeserializationException(IDataPreprocessor.class, this.name, e);
		}
	}
}