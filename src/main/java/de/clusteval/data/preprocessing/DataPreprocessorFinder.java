/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.data.preprocessing;

import java.io.File;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.DynamicComponentMissingVersionException;
import de.clusteval.utils.IExceptionWithDependent;
import de.clusteval.utils.IncompatibleClustEvalVersionException;
import de.clusteval.utils.InvalidClustEvalVersionException;
import de.clusteval.utils.InvalidDependencyTargetException;
import de.clusteval.utils.JARFinder;
import de.clusteval.utils.JARFinderClassLoader;
import de.clusteval.utils.MissingClustEvalVersionException;

/**
 * @author Christian Wiwie
 */
public class DataPreprocessorFinder extends JARFinder<IDataPreprocessor> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4303433002568216856L;

	/**
	 * Instantiates a new data set generator finder.
	 * 
	 * @param repository
	 *            the repository
	 * @throws RegisterException
	 */
	public DataPreprocessorFinder(final IRepository repository)
			throws RegisterException {
		super(repository, IDataPreprocessor.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.JARFinder#getURLClassLoader(java.io.File)
	 */
	@Override
	protected URLClassLoader getURLClassLoader0(File f,
			final ClassLoader parent) throws MalformedURLException {
		URL url = f.toURI().toURL();
		return new DataPreprocessorURLClassLoader(this, new URL[]{url}, parent);
	}
}

class DataPreprocessorURLClassLoader
		extends
			JARFinderClassLoader<IDataPreprocessor> {

	/**
	 * @param urls
	 * @param parent
	 * @param loaderParent
	 */
	public DataPreprocessorURLClassLoader(DataPreprocessorFinder parent,
			URL[] urls, ClassLoader loaderParent) {
		super(parent, urls, loaderParent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.ClassLoader#loadClass(java.lang.String)
	 */
	@Override
	public Class<?> loadClass(String name) throws ClassNotFoundException {
		Class<?> result = super.loadClass(name, true);

		if (Modifier.isAbstract(result.getModifiers()) || result.isInterface())
			return result;

		if (name.startsWith("de.clusteval.data.preprocessing")) {
			if (name.endsWith("DataPreprocessor")) {
				@SuppressWarnings("unchecked")
				Class<? extends DataPreprocessor> dataSetGenerator = (Class<? extends DataPreprocessor>) result;

				try {

					// loadClass may have been called not for the newly
					// discovered class of this JAR, but for a parent class
					// which had already been registered before;
					// thus, we only register this class, if it hasn't been
					// registered before
					try {
						if (!this.parent.getRepository()
								.isClassRegistered(dataSetGenerator)) {
							this.parent.getRepository().registerClass(
									this.parent.getClassToFind(),
									dataSetGenerator);
						}
					} catch (NullPointerException e) {
						e.printStackTrace();
					}
				} catch (DynamicComponentMissingVersionException
						| DynamicComponentInitializationException
						| IncompatibleClustEvalVersionException
						| MissingClustEvalVersionException
						| InvalidClustEvalVersionException
						| InvalidDependencyTargetException e) {
					handleException(e);
				}
			}
		}
		return result;
	}
}
