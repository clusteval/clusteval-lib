/**
 * 
 */
package de.clusteval.framework.repository.maven;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.aether.repository.ArtifactRepository;
import org.eclipse.aether.repository.RemoteRepository;

/**
 * @author Christian Wiwie
 *
 */
public class MavenConfig implements IMavenConfig {

	/**
	 * A map from repository name to repository URL
	 */
	protected Map<String, ArtifactRepository> mavenRepositories;

	protected boolean useMaven;

	protected boolean resolveSnapshotVersions;

	/**
	 * 
	 */
	public MavenConfig() {
		super();
		this.mavenRepositories = new HashMap<String, ArtifactRepository>();
		this.useMaven = true;
		this.resolveSnapshotVersions = false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.maven.IMavenConfig#addMavenRepository(
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public void addMavenRepository(final String name, final String url)
			throws MavenRepositoryDuplicateException {
		if (this.mavenRepositories.containsValue(url)) {
			throw new MavenRepositoryDuplicateException(url);
		}

		// if (MavenRepositoryResolver.isURL(url))
		this.mavenRepositories.put(name,
				new RemoteRepository.Builder(name, "default", url).build());
		// else
		// this.mavenRepositories.put(name, new LocalRepository(url));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.maven.IMavenConfig#getMavenRepositories
	 * ()
	 */
	@Override
	public Map<String, ArtifactRepository> getMavenRepositories() {
		return mavenRepositories;
	}

	/**
	 * @param useMaven
	 *            the useMaven to set
	 */
	public void setUseMaven(boolean useMaven) {
		this.useMaven = useMaven;
	}

	/**
	 * @return the useMaven
	 */
	public boolean useMaven() {
		return useMaven;
	}

	/**
	 * @return the resolveSnapshotVersions
	 */
	@Override
	public boolean resolveSnapshotVersions() {
		return resolveSnapshotVersions;
	}

	/**
	 * @param resolveSnapshotVersions
	 *            the resolveSnapshotVersions to set
	 */
	@Override
	public void setResolveSnapshotVersions(boolean resolveSnapshotVersions) {
		this.resolveSnapshotVersions = resolveSnapshotVersions;
	}
}
