/**
 * 
 */
package de.clusteval.framework.repository.maven;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.maven.artifact.versioning.ComparableVersion;
import org.eclipse.aether.resolution.ArtifactResolutionException;
import org.eclipse.aether.resolution.VersionRangeRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.IRepositoryObject;

/**
 * @author Christian Wiwie
 *
 */
public class StubRepositoryResolver implements IMavenRepositoryResolver {

	protected IRepository repository;
	protected Logger log;

	/**
	 * @param repository
	 * 
	 */
	public StubRepositoryResolver(final IRepository repository) {
		super();
		this.repository = repository;
		this.log = LoggerFactory.getLogger(this.getClass());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.maven.IMavenRepositoryResolver#
	 * isAvailable(java.lang.String)
	 */
	@Override
	public synchronized boolean isDynamicComponentAvailable(
			final String fullClassNameWithVersion)
			throws InvalidDynamicComponentNameException {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.maven.IMavenRepositoryResolver#
	 * isAvailable(java.lang.String,
	 * org.apache.maven.artifact.versioning.ComparableVersion)
	 */
	@Override
	public synchronized boolean isDynamicComponentAvailable(
			final String simpleClassName, final ComparableVersion version)
			throws InvalidDynamicComponentNameException {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.maven.IMavenRepositoryResolver#
	 * getAvailableVersions(java.lang.String)
	 */
	@Override
	public synchronized Set<ComparableVersion> getAvailableVersionsForDynamicComponent(
			final String simpleClassName)
			throws InvalidDynamicComponentNameException {
		return new HashSet<>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.maven.IMavenRepositoryResolver#
	 * installIntoRepository(java.lang.String)
	 */
	@Override
	public synchronized boolean installDynamicComponentIntoRepository(
			final String fullClassNameWithVersion)
			throws InvalidDynamicComponentNameException {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.maven.IMavenRepositoryResolver#
	 * installIntoRepository(java.lang.String,
	 * org.apache.maven.artifact.versioning.ComparableVersion)
	 */
	@Override
	public synchronized boolean installDynamicComponentIntoRepository(
			final String simpleClassName, ComparableVersion version)
			throws InvalidDynamicComponentNameException {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.maven.IMavenRepositoryResolver#
	 * informOnClassRemoved(java.lang.String,
	 * org.apache.maven.artifact.versioning.ComparableVersion)
	 */
	@Override
	public synchronized void informOnClassRemoved(final String simpleClassName,
			final ComparableVersion version) {
	}

	@Override
	public synchronized void informOnRepositoryObjectRemoved(
			final Class<? extends IRepositoryObject> clazz, final String name,
			final ComparableVersion version) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.maven.IMavenRepositoryResolver#
	 * getAvailableVersionsForRepositoryObject(java.lang.Class,
	 * java.lang.String)
	 */
	@Override
	public Set<ComparableVersion> getAvailableVersionsForRepositoryObject(
			Class<? extends IRepositoryObject> type, String objectName) {
		return new HashSet<>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.maven.IMavenRepositoryResolver#
	 * installRepositoryObjectIntoRepository(java.lang.Class, java.lang.String,
	 * org.apache.maven.artifact.versioning.ComparableVersion)
	 */
	@Override
	public boolean installRepositoryObjectIntoRepository(
			Class<? extends IRepositoryObject> type, String objectName,
			ComparableVersion version) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.maven.IMavenRepositoryResolver#
	 * installRepositoryObjectIntoRepository(java.lang.Class, java.lang.String)
	 */
	@Override
	public boolean installRepositoryObjectIntoRepository(
			Class<? extends IRepositoryObject> type,
			String objectNameWithVersion) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.maven.IMavenRepositoryResolver#
	 * isRepositoryObjectAvailable(java.lang.Class, java.lang.String,
	 * org.apache.maven.artifact.versioning.ComparableVersion)
	 */
	@Override
	public boolean isRepositoryObjectAvailable(
			Class<? extends IRepositoryObject> type, String objectName,
			ComparableVersion version) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.maven.IMavenRepositoryResolver#
	 * isRepositoryObjectAvailable(java.lang.Class, java.lang.String)
	 */
	@Override
	public boolean isRepositoryObjectAvailable(
			Class<? extends IRepositoryObject> type,
			String objectNameWithVersion) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.maven.IMavenRepositoryResolver#
	 * checkForNewerVersionClustEvalClient(org.apache.maven.artifact.versioning.
	 * ComparableVersion, boolean)
	 */
	@Override
	public synchronized ComparableVersion checkForNewerVersionClustEvalClient(
			final ComparableVersion clientVersion,
			final boolean includeSnapshotVersions) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.maven.IMavenRepositoryResolver#
	 * checkForNewerVersionClustEvalServer(boolean)
	 */
	@Override
	public synchronized ComparableVersion checkForNewerVersionClustEvalServer(
			final boolean includeSnapshotVersions) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.maven.IMavenRepositoryResolver#
	 * getAvailableVersions(org.eclipse.aether.resolution.VersionRangeRequest,
	 * boolean)
	 */
	@Override
	public synchronized Set<ComparableVersion> getAvailableVersions(
			final VersionRangeRequest rangeRequest,
			final boolean includeSnapshotVersions) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.maven.IMavenRepositoryResolver#
	 * checkForNewerVersionDynamicComponent(java.lang.String,
	 * org.apache.maven.artifact.versioning.ComparableVersion)
	 */
	@Override
	public synchronized ComparableVersion checkForNewerVersionDynamicComponent(
			final String simpleClassName, final ComparableVersion version)
			throws InvalidDynamicComponentNameException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.maven.IMavenRepositoryResolver#
	 * checkForNewerVersionsComponents(java.lang.String, java.util.Map, boolean)
	 */
	@Override
	public Map<String, Map<String, String>> checkForNewerVersionsComponents(
			final String clientId,
			final Map<String, Set<ComparableVersion>> registeredClassesNamesAndVersions,
			boolean includeSnapshotVersions)
			throws RemoteException, InvalidDynamicComponentNameException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.maven.IMavenRepositoryResolver#
	 * getReleaseNotes(java.lang.String, java.lang.String)
	 */
	@Override
	public synchronized String getReleaseNotes(final String groupName,
			final String artifactName, final String artifactVersion) {
		return null;
	}

	/* (non-Javadoc)
	 * @see de.clusteval.framework.repository.maven.IMavenRepositoryResolver#getReleaseTitle(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public synchronized String getReleaseTitle(final String groupName, final String artifactName,
			final String artifactVersion) throws ArtifactResolutionException {
		return null;
	}
}
