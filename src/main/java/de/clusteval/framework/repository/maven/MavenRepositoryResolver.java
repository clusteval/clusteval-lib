/**
 * 
 */
package de.clusteval.framework.repository.maven;

import java.io.File;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.maven.artifact.versioning.ComparableVersion;
import org.apache.maven.repository.internal.MavenRepositorySystemUtils;
import org.eclipse.aether.DefaultRepositorySystemSession;
import org.eclipse.aether.RepositorySystem;
import org.eclipse.aether.RepositorySystemSession;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.connector.basic.BasicRepositoryConnectorFactory;
import org.eclipse.aether.impl.DefaultServiceLocator;
import org.eclipse.aether.repository.ArtifactRepository;
import org.eclipse.aether.repository.LocalRepository;
import org.eclipse.aether.repository.RemoteRepository;
import org.eclipse.aether.resolution.ArtifactRequest;
import org.eclipse.aether.resolution.ArtifactResolutionException;
import org.eclipse.aether.resolution.ArtifactResult;
import org.eclipse.aether.resolution.VersionRangeRequest;
import org.eclipse.aether.resolution.VersionRangeResolutionException;
import org.eclipse.aether.resolution.VersionRangeResult;
import org.eclipse.aether.spi.connector.RepositoryConnectorFactory;
import org.eclipse.aether.spi.connector.transport.TransporterFactory;
import org.eclipse.aether.transport.file.FileTransporterFactory;
import org.eclipse.aether.transport.http.HttpTransporterFactory;
import org.eclipse.aether.version.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod;
import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.context.IContext;
import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.data.dataset.generator.IDataSetGenerator;
import de.clusteval.data.dataset.type.IDataSetType;
import de.clusteval.data.distance.IDistanceMeasure;
import de.clusteval.data.preprocessing.IDataPreprocessor;
import de.clusteval.data.randomizer.IDataRandomizer;
import de.clusteval.data.statistics.IDataStatistic;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.RepositoryObject;
import de.clusteval.program.r.IRProgram;
import de.clusteval.run.runresult.format.IRunResultFormat;
import de.clusteval.run.runresult.postprocessing.IRunResultPostprocessor;
import de.clusteval.run.statistics.IRunDataStatistic;
import de.clusteval.run.statistics.IRunStatistic;
import de.clusteval.serverclient.IBackendServer;
import de.clusteval.utils.ClassVersion;
import de.clusteval.utils.IFinder;
import de.clusteval.utils.IJARFinder;
import dk.sdu.imada.compbio.file.FileUtils;
import dk.sdu.imada.compbio.utils.Pair;
import dk.sdu.imada.compbio.utils.Triple;

/**
 * @author Christian Wiwie
 *
 */
public class MavenRepositoryResolver implements IMavenRepositoryResolver {

	protected IRepository repository;
	protected Logger log;
	protected RepositorySystem system;
	protected RepositorySystemSession session;

	protected Set<Pair<String, ComparableVersion>> alreadyTriedToInstallDynamicComponents;
	// protected Map<String, Set<ComparableVersion>>
	// availableVersionsDynamicComponents;
	protected Set<Triple<Class, String, ComparableVersion>> alreadyTriedToInstallRepositoryObject;
	// protected Map<Class, Map<String, Set<ComparableVersion>>>
	// availableVersionsRepositoryObject;

	/**
	 * @param repository
	 * 
	 */
	public MavenRepositoryResolver(final IRepository repository) {
		super();
		this.repository = repository;
		this.log = LoggerFactory.getLogger(this.getClass());
		// this.availableVersionsDynamicComponents = new HashMap<String,
		// Set<ComparableVersion>>();
		this.alreadyTriedToInstallDynamicComponents = new HashSet<Pair<String, ComparableVersion>>();
		// this.availableVersionsRepositoryObject = new HashMap<Class,
		// Map<String, Set<ComparableVersion>>>();
		this.alreadyTriedToInstallRepositoryObject = new HashSet<Triple<Class, String, ComparableVersion>>();

		this.system = createRepositorySystem();
		this.session = newRepositorySystemSession(system);
	}

	protected DefaultRepositorySystemSession newRepositorySystemSession(
			RepositorySystem system) {
		DefaultRepositorySystemSession session = MavenRepositorySystemUtils
				.newSession();

		// create a local maven repository
		String localPath = FileUtils
				.buildPath(repository.getSupplementaryBasePath(), ".mvntmp");
		new File(localPath).mkdirs();
		LocalRepository localRepo = new LocalRepository(localPath);
		session.setLocalRepositoryManager(
				system.newLocalRepositoryManager(session, localRepo));

		// session.setTransferListener(new ConsoleTransferListener());
		// session.setRepositoryListener(new ConsoleRepositoryListener());

		return session;
	}

	protected RepositorySystem createRepositorySystem() {
		DefaultServiceLocator locator = MavenRepositorySystemUtils
				.newServiceLocator();
		locator.addService(RepositoryConnectorFactory.class,
				BasicRepositoryConnectorFactory.class);
		locator.addService(TransporterFactory.class,
				FileTransporterFactory.class);
		locator.addService(TransporterFactory.class,
				HttpTransporterFactory.class);

		locator.setErrorHandler(new DefaultServiceLocator.ErrorHandler() {

			@Override
			public void serviceCreationFailed(Class<?> type, Class<?> impl,
					Throwable exception) {
				exception.printStackTrace();
			}
		});

		return locator.getService(RepositorySystem.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.maven.IMavenRepositoryResolver#
	 * isAvailable(java.lang.String)
	 */
	@Override
	public synchronized boolean isDynamicComponentAvailable(
			final String fullClassNameWithVersion)
			throws InvalidDynamicComponentNameException {
		if (fullClassNameWithVersion.contains(":")) {
			String[] split = fullClassNameWithVersion.split(":");
			return isDynamicComponentAvailable(split[0],
					new ComparableVersion(split[1]));
		}
		return isDynamicComponentAvailable(fullClassNameWithVersion, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.maven.IMavenRepositoryResolver#
	 * isAvailable(java.lang.String,
	 * org.apache.maven.artifact.versioning.ComparableVersion)
	 */
	@Override
	public synchronized boolean isDynamicComponentAvailable(
			final String simpleClassName, final ComparableVersion version)
			throws InvalidDynamicComponentNameException {
		Set<ComparableVersion> availableVersions = this
				.getAvailableVersionsForDynamicComponent(simpleClassName);
		if (version != null)
			return availableVersions.contains(version);
		return availableVersions.size() > 0;
	}

	@Override
	public synchronized Set<ComparableVersion> getAvailableVersions(
			final VersionRangeRequest rangeRequest,
			final boolean includeSnapshotVersions) {
		Set<ComparableVersion> result = new HashSet<ComparableVersion>();

		VersionRangeResult rangeResult;
		try {
			rangeResult = system.resolveVersionRange(session, rangeRequest);
			List<Version> versions = rangeResult.getVersions();
			for (Version v : versions) {
				// we do not resolve SNAPSHOT versions unless specified
				if (v.toString().endsWith("SNAPSHOT")
						&& !includeSnapshotVersions)
					continue;

				result.add(new ComparableVersion(v.toString()));
			}
		} catch (VersionRangeResolutionException e) {
			// should never happen
			e.printStackTrace();
			result = new HashSet<ComparableVersion>();
		}
		return result;
	}

	@Override
	public synchronized String getReleaseTitle(final String groupName,
			final String artifactName, final String artifactVersion)
			throws ArtifactResolutionException {
		try {
			Artifact artifact = new DefaultArtifact(
					String.format("%s:%s:md:release-title:%s", groupName,
							artifactName, artifactVersion));

			List<RemoteRepository> repos = new ArrayList<RemoteRepository>();
			for (ArtifactRepository r : this.repository.getRepositoryConfig()
					.getMavenConfig().getMavenRepositories().values())
				repos.add((RemoteRepository) r);

			ArtifactRequest request = new ArtifactRequest(artifact, repos, "");

			ArtifactResult resolveArtifact = system.resolveArtifact(session,
					request);

			artifact = resolveArtifact.getArtifact();
			File artifactFile = artifact.getFile();
			return org.apache.commons.io.FileUtils
					.readFileToString(artifactFile);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	/**
	 * 
	 * @param includeSnapshotVersions
	 *            Whether also to include snapshot versions.
	 * @return The newer version if available, null otherwise.
	 */
	@Override
	public synchronized ComparableVersion checkForNewerVersionClustEvalServer(
			final boolean includeSnapshotVersions) {
		Artifact artifact = new DefaultArtifact(
				String.format("de.clusteval:clusteval-server:(%s,)",
						IBackendServer.class
								.getAnnotation(ClassVersion.class)
								.version()));

		VersionRangeRequest rangeRequest = new VersionRangeRequest();
		rangeRequest.setArtifact(artifact);

		List<RemoteRepository> repos = new ArrayList<RemoteRepository>();
		repos.add(new RemoteRepository.Builder("clusteval-release", "default",
				CLUSTEVAL_MAVEN_REPOSITORY.CLUSTEVAL_RELEASE.getUrl()).build());
		if (includeSnapshotVersions)
			repos.add(new RemoteRepository.Builder("clusteval-snapshot",
					"default",
					CLUSTEVAL_MAVEN_REPOSITORY.CLUSTEVAL_SNAPSHOT.getUrl())
							.build());

		rangeRequest.setRepositories(repos);

		VersionRangeResult rangeResult;
		try {
			rangeResult = system.resolveVersionRange(session, rangeRequest);
			if (!rangeResult.getVersions().isEmpty()) {
				ComparableVersion newestVersion = new ComparableVersion(
						rangeResult.getHighestVersion().toString());
				return newestVersion;
			}
		} catch (VersionRangeResolutionException e) {
			// should never happen
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * @param clientVersion
	 *            The current version of the client.
	 * @param includeSnapshotVersions
	 *            Whether also to include snapshot versions.
	 * @return The newer version if available, null otherwise.
	 */
	@Override
	public synchronized ComparableVersion checkForNewerVersionClustEvalClient(
			final ComparableVersion clientVersion,
			final boolean includeSnapshotVersions) {
		Artifact artifact = new DefaultArtifact(
				String.format("de.clusteval:clusteval-client:(%s,)",
						clientVersion.toString()));

		VersionRangeRequest rangeRequest = new VersionRangeRequest();
		rangeRequest.setArtifact(artifact);

		List<RemoteRepository> repos = new ArrayList<RemoteRepository>();
		repos.add(new RemoteRepository.Builder("clusteval-release", "default",
				CLUSTEVAL_MAVEN_REPOSITORY.CLUSTEVAL_RELEASE.getUrl()).build());
		if (includeSnapshotVersions)
			repos.add(new RemoteRepository.Builder("clusteval-snapshot",
					"default",
					CLUSTEVAL_MAVEN_REPOSITORY.CLUSTEVAL_SNAPSHOT.getUrl())
							.build());

		rangeRequest.setRepositories(repos);

		VersionRangeResult rangeResult;
		try {
			rangeResult = system.resolveVersionRange(session, rangeRequest);
			if (!rangeResult.getVersions().isEmpty()) {
				ComparableVersion newestVersion = new ComparableVersion(
						rangeResult.getHighestVersion().toString());
				return newestVersion;
			}
		} catch (VersionRangeResolutionException e) {
			// should never happen
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.maven.IMavenRepositoryResolver#
	 * getAvailableVersions(java.lang.String)
	 */
	@Override
	public synchronized Set<ComparableVersion> getAvailableVersionsForDynamicComponent(
			final String simpleClassName)
			throws InvalidDynamicComponentNameException {
		// if (availableVersionsDynamicComponents.containsKey(simpleClassName))
		// {
		// return availableVersionsDynamicComponents.get(simpleClassName);
		// }
		Set<ComparableVersion> result = new HashSet<ComparableVersion>();
		try {
			Artifact artifact;
			String versionString = "[0,)";
			artifact = new DefaultArtifact(String.format("%s:%s:%s",
					getGroupIDFromSimpleClassName(simpleClassName),
					simpleClassName, versionString));

			VersionRangeRequest rangeRequest = new VersionRangeRequest();
			rangeRequest.setArtifact(artifact);

			List<RemoteRepository> repos = new ArrayList<RemoteRepository>();
			for (ArtifactRepository r : this.repository.getRepositoryConfig()
					.getMavenConfig().getMavenRepositories().values())
				repos.add((RemoteRepository) r);
			rangeRequest.setRepositories(repos);

			result = getAvailableVersions(rangeRequest,
					repository.getRepositoryConfig().getMavenConfig()
							.resolveSnapshotVersions());

			return result;
		} finally {
			// availableVersionsDynamicComponents.put(simpleClassName, result);
		}
	}

	protected static Class<? extends IRepositoryObject> getBaseClassFromSimpleClassName(
			final String simpleClassName)
			throws InvalidDynamicComponentNameException {
		Class<? extends IRepositoryObject> c = null;
		if (simpleClassName.endsWith("ClusteringQualityMeasure"))
			c = IClusteringQualityMeasure.class;
		else if (simpleClassName.endsWith("Context"))
			c = IContext.class;
		else if (simpleClassName.endsWith("DataPreprocessor"))
			c = IDataPreprocessor.class;
		else if (simpleClassName.endsWith("DataRandomizer"))
			c = IDataRandomizer.class;
		else if (simpleClassName.endsWith("DataSetFormat"))
			c = IDataSetFormat.class;
		else if (simpleClassName.endsWith("DataSetGenerator"))
			c = IDataSetGenerator.class;
		else if (simpleClassName.endsWith("DataSetType"))
			c = IDataSetType.class;
		else if (simpleClassName.endsWith("RunDataStatistic"))
			c = IRunDataStatistic.class;
		else if (simpleClassName.endsWith("DataStatistic"))
			c = IDataStatistic.class;
		else if (simpleClassName.endsWith("DistanceMeasure"))
			c = IDistanceMeasure.class;
		else if (simpleClassName.endsWith("ParameterOptimizationMethod"))
			c = IParameterOptimizationMethod.class;
		else if (simpleClassName.endsWith("RProgram"))
			c = IRProgram.class;
		else if (simpleClassName.endsWith("RunResultFormat"))
			c = IRunResultFormat.class;
		else if (simpleClassName.endsWith("RunResultPostprocessor"))
			c = IRunResultPostprocessor.class;
		else if (simpleClassName.endsWith("RunStatistic"))
			c = IRunStatistic.class;
		if (c != null)
			return c;
		throw new InvalidDynamicComponentNameException(simpleClassName);

	}

	protected static String getGroupIDFromSimpleClassName(
			final String simpleClassName)
			throws InvalidDynamicComponentNameException {
		Class<? extends IRepositoryObject> c = getBaseClassFromSimpleClassName(
				simpleClassName);
		return getGroupIDFromClass(c);
	}

	protected static String getGroupIDFromClass(
			final Class<? extends IRepositoryObject> c) {
		return c.getPackage().getName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.maven.IMavenRepositoryResolver#
	 * installIntoRepository(java.lang.String)
	 */
	@Override
	public synchronized boolean installDynamicComponentIntoRepository(
			final String fullClassNameWithVersion)
			throws InvalidDynamicComponentNameException {
		if (fullClassNameWithVersion.contains(":")) {
			String[] split = fullClassNameWithVersion.split(":");
			return installDynamicComponentIntoRepository(split[0],
					new ComparableVersion(split[1]));
		}
		return installDynamicComponentIntoRepository(fullClassNameWithVersion,
				null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.maven.IMavenRepositoryResolver#
	 * installIntoRepository(java.lang.String,
	 * org.apache.maven.artifact.versioning.ComparableVersion)
	 */
	@Override
	public synchronized boolean installDynamicComponentIntoRepository(
			final String simpleClassName, ComparableVersion version)
			throws InvalidDynamicComponentNameException {
		try {
			String informString;
			if (version != null)
				informString = String.format(
						"Trying to resolve missing class '%s (%s)' using maven repositories.",
						simpleClassName, version);
			else
				informString = String.format(
						"Trying to resolve missing class '%s' using maven repositories.",
						simpleClassName);
			Artifact artifact;
			String versionString;
			if (version != null)
				versionString = String.format("%s", version);
			else {
				Set<ComparableVersion> availableVersions = getAvailableVersionsForDynamicComponent(
						simpleClassName);
				version = Collections.max(availableVersions);
				versionString = String.format("%s", version);
			}

			if (alreadyTriedToInstallDynamicComponents
					.contains(Pair.getPair(simpleClassName, version)))
				return false;

			this.log.info(informString);

			artifact = new DefaultArtifact(String.format("%s:%s:%s",
					getGroupIDFromSimpleClassName(simpleClassName),
					simpleClassName, versionString));

			ArtifactRequest artifactRequest = new ArtifactRequest();
			artifactRequest.setArtifact(artifact);

			List<RemoteRepository> repos = new ArrayList<RemoteRepository>();
			for (ArtifactRepository r : this.repository.getRepositoryConfig()
					.getMavenConfig().getMavenRepositories().values())
				repos.add((RemoteRepository) r);
			artifactRequest.setRepositories(repos);

			try {
				ArtifactResult artifactResult = system.resolveArtifact(session,
						artifactRequest);
				artifact = artifactResult.getArtifact();
				File artifactFile = artifact.getFile();
				String targetDir = repository.getBasePath(
						getBaseClassFromSimpleClassName(simpleClassName));
				new File(targetDir).mkdirs();
				File targetFile = new File(
						FileUtils.buildPath(targetDir, artifactFile.getName()));
				artifactFile.renameTo(targetFile);

				// invoke the corresponding finder class to the result jar file
				IJARFinder<? extends RepositoryObject> finder = (IJARFinder<? extends RepositoryObject>) repository
						.getStaticObjectWithNameAndVersion(IFinder.class,
								"Finder: " + getBaseClassFromSimpleClassName(
										simpleClassName).getSimpleName());
				finder.doOnFileFound(targetFile);

				if (version != null)
					this.log.info(String.format(
							"Successfully resolved class '%s (%s)' from maven repositories.",
							simpleClassName, version));
				else
					this.log.info(String.format(
							"Successfully resolved class '%s' from maven repositories.",
							simpleClassName));
				return true;
			} catch (Exception e) {
				if (version != null)
					this.log.info(String.format(
							"Failed to resolve class '%s (%s)' from maven repositories.",
							simpleClassName, version));
				else
					this.log.info(String.format(
							"Failed to resolve class '%s' from maven repositories.",
							simpleClassName));
				return false;
			}
		} finally {
			alreadyTriedToInstallDynamicComponents
					.add(Pair.getPair(simpleClassName, version));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.maven.IMavenRepositoryResolver#
	 * informOnClassRemoved(java.lang.String,
	 * org.apache.maven.artifact.versioning.ComparableVersion)
	 */
	@Override
	public synchronized void informOnClassRemoved(final String simpleClassName,
			final ComparableVersion version) {
		alreadyTriedToInstallDynamicComponents
				.remove(Pair.getPair(simpleClassName, version));
	}

	@Override
	public synchronized void informOnRepositoryObjectRemoved(
			final Class<? extends IRepositoryObject> clazz, final String name,
			final ComparableVersion version) {
		alreadyTriedToInstallRepositoryObject
				.remove(Triple.getTriple(clazz, name, version));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.maven.IMavenRepositoryResolver#
	 * getAvailableVersionsForRepositoryObject(java.lang.Class,
	 * java.lang.String)
	 */
	@Override
	public Set<ComparableVersion> getAvailableVersionsForRepositoryObject(
			Class<? extends IRepositoryObject> type, String objectName) {
		// if (availableVersionsRepositoryObject.containsKey(type)
		// && availableVersionsRepositoryObject.get(type)
		// .containsKey(objectName)) {
		// return availableVersionsRepositoryObject.get(type).get(objectName);
		// }
		Set<ComparableVersion> result = new HashSet<ComparableVersion>();
		try {
			Artifact artifact;
			String versionString = "[0,)";
			// TODO: do we need to ensure that version numbers are replaced?
			artifact = new DefaultArtifact(String.format("%s:%s:%s",
					this.repository.getMavenGroupIDForRepositoryObject(type,
							objectName),
					this.repository.getMavenArtifactIDForRepositoryObject(type,
							objectName),
					versionString));

			VersionRangeRequest rangeRequest = new VersionRangeRequest();
			rangeRequest.setArtifact(artifact);

			List<RemoteRepository> repos = new ArrayList<RemoteRepository>();
			for (ArtifactRepository r : this.repository.getRepositoryConfig()
					.getMavenConfig().getMavenRepositories().values())
				repos.add((RemoteRepository) r);
			rangeRequest.setRepositories(repos);

			result = getAvailableVersions(rangeRequest,
					repository.getRepositoryConfig().getMavenConfig()
							.resolveSnapshotVersions());

			return result;
		} finally {
			// if (!availableVersionsRepositoryObject.containsKey(type))
			// availableVersionsRepositoryObject.put(type,
			// new HashMap<String, Set<ComparableVersion>>());
			// availableVersionsRepositoryObject.get(type).put(objectName,
			// result);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.maven.IMavenRepositoryResolver#
	 * installRepositoryObjectIntoRepository(java.lang.Class, java.lang.String,
	 * org.apache.maven.artifact.versioning.ComparableVersion)
	 */
	@Override
	public boolean installRepositoryObjectIntoRepository(
			Class<? extends IRepositoryObject> type, String objectName,
			ComparableVersion version) {
		try {
			Artifact artifact;
			String versionString;
			if (version != null)
				versionString = String.format("%s", version);
			else {
				Set<ComparableVersion> availableVersions = getAvailableVersionsForRepositoryObject(
						type, objectName);
				version = Collections.max(availableVersions);
				versionString = String.format("%s", version);
			}

			if (alreadyTriedToInstallRepositoryObject
					.contains(Triple.getTriple(type, objectName, version))) {
				String informString;
				if (version != null)
					informString = String.format(
							"We already tried resolving missing '%s' '%s (%s)' from maven repositories -> Not trying again",
							type.getSimpleName(), objectName, version);
				else
					informString = String.format(
							"We already tried resolving missing '%s' '%s (%s)' from maven repositories -> Not trying again",
							type.getSimpleName(), objectName);

				this.log.debug(informString);

				return false;
			}

			String informString;
			if (version != null)
				informString = String.format(
						"Trying to resolve missing '%s' '%s (%s)' using maven repositories.",
						type.getSimpleName(), objectName, version);
			else
				informString = String.format(
						"Trying to resolve missing '%s' '%s' using maven repositories.",
						type.getSimpleName(), objectName);

			this.log.info(informString);

			String groupId = this.repository
					.getMavenGroupIDForRepositoryObject(type, objectName);
			String artifactId = this.repository
					.getMavenArtifactIDForRepositoryObject(type, objectName);

			artifact = new DefaultArtifact(String.format("%s:%s:%s", groupId,
					artifactId, versionString));

			ArtifactRequest artifactRequest = new ArtifactRequest();
			artifactRequest.setArtifact(artifact);

			List<RemoteRepository> repos = new ArrayList<RemoteRepository>();
			for (ArtifactRepository r : this.repository.getRepositoryConfig()
					.getMavenConfig().getMavenRepositories().values())
				repos.add((RemoteRepository) r);
			artifactRequest.setRepositories(repos);

			try {
				ArtifactResult artifactResult = system.resolveArtifact(session,
						artifactRequest);
				artifact = artifactResult.getArtifact();
				File artifactFile = artifact.getFile();

				this.repository.installRepositoryObjectFromJarFromMaven(type,
						objectName, artifactFile, version);

				if (version != null)
					this.log.info(String.format(
							"Successfully resolved '%s' '%s (%s)' from maven repositories.",
							type.getSimpleName(), objectName, version));
				else
					this.log.info(String.format(
							"Successfully resolved '%s' '%s' from maven repositories.",
							type.getSimpleName(), objectName));
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				if (version != null)
					this.log.info(String.format(
							"Failed to resolve '%s' '%s (%s)' from maven repositories.",
							type.getSimpleName(), objectName, version));
				else
					this.log.info(String.format(
							"Failed to resolve '%s' '%s' from maven repositories.",
							type.getSimpleName(), objectName));
				return false;
			}
		} finally {
			alreadyTriedToInstallRepositoryObject
					.add(Triple.getTriple((Class) type, objectName, version));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.maven.IMavenRepositoryResolver#
	 * installRepositoryObjectIntoRepository(java.lang.Class, java.lang.String)
	 */
	@Override
	public boolean installRepositoryObjectIntoRepository(
			Class<? extends IRepositoryObject> type,
			String objectNameWithVersion) {
		if (objectNameWithVersion.contains(":")) {
			String[] split = objectNameWithVersion.split(":");
			return installRepositoryObjectIntoRepository(type, split[0],
					new ComparableVersion(split[1]));
		}
		return installRepositoryObjectIntoRepository(type,
				objectNameWithVersion, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.maven.IMavenRepositoryResolver#
	 * isRepositoryObjectAvailable(java.lang.Class, java.lang.String,
	 * org.apache.maven.artifact.versioning.ComparableVersion)
	 */
	@Override
	public boolean isRepositoryObjectAvailable(
			Class<? extends IRepositoryObject> type, String objectName,
			ComparableVersion version) {
		Set<ComparableVersion> availableVersions = this
				.getAvailableVersionsForRepositoryObject(type, objectName);
		if (version != null)
			return availableVersions.contains(version);
		return availableVersions.contains(new ComparableVersion("1"));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.maven.IMavenRepositoryResolver#
	 * isRepositoryObjectAvailable(java.lang.Class, java.lang.String)
	 */
	@Override
	public boolean isRepositoryObjectAvailable(
			Class<? extends IRepositoryObject> type,
			String objectNameWithVersion) {
		if (objectNameWithVersion.contains(":")) {
			String[] split = objectNameWithVersion.split(":");
			return isRepositoryObjectAvailable(type, split[0],
					new ComparableVersion(split[1]));
		}
		return isRepositoryObjectAvailable(type, objectNameWithVersion, null);
	}

	@Override
	public Map<String, Map<String, String>> checkForNewerVersionsComponents(
			final String clientId,
			final Map<String, Set<ComparableVersion>> registeredClassesNamesAndVersions,
			boolean includeSnapshotVersions)
			throws RemoteException, InvalidDynamicComponentNameException {
		Map<String, Map<String, String>> availableNewerVersions = new HashMap<>();
		for (String className : registeredClassesNamesAndVersions.keySet()) {
			Map<String, String> availableNewerVersionsClass = new HashMap<>();
			ComparableVersion classMax = Collections
					.max(registeredClassesNamesAndVersions.get(className));
			ComparableVersion classNewestVersionRepository = checkForNewerVersionDynamicComponent(
					className.substring(className.lastIndexOf(".") + 1),
					classMax);
			if (classNewestVersionRepository != null
					&& classNewestVersionRepository.compareTo(classMax) > 0)
				classMax = classNewestVersionRepository;
			for (ComparableVersion classVersion : registeredClassesNamesAndVersions
					.get(className)) {
				if (classMax.compareTo(classVersion) > 0)
					availableNewerVersionsClass.put(classVersion.toString(),
							classMax.toString());
			}
			if (!availableNewerVersionsClass.isEmpty())
				availableNewerVersions.put(className,
						availableNewerVersionsClass);
		}

		return availableNewerVersions;
	}

	@Override
	public synchronized ComparableVersion checkForNewerVersionDynamicComponent(
			final String simpleClassName, final ComparableVersion version)
			throws InvalidDynamicComponentNameException {
		Artifact artifact;
		String versionString = String.format("(%s,)", version.toString());
		artifact = new DefaultArtifact(String.format("%s:%s:%s",
				getGroupIDFromSimpleClassName(simpleClassName), simpleClassName,
				versionString));

		VersionRangeRequest rangeRequest = new VersionRangeRequest();
		rangeRequest.setArtifact(artifact);

		List<RemoteRepository> repos = new ArrayList<RemoteRepository>();
		for (ArtifactRepository r : this.repository.getRepositoryConfig()
				.getMavenConfig().getMavenRepositories().values())
			repos.add((RemoteRepository) r);
		rangeRequest.setRepositories(repos);

		ComparableVersion newestVersion = null;
		for (ComparableVersion v : getAvailableVersions(rangeRequest,
				repository.getRepositoryConfig().getMavenConfig()
						.resolveSnapshotVersions())) {
			if (newestVersion == null || v.compareTo(newestVersion) > 0)
				newestVersion = v;
		}
		return newestVersion;
	}

	@Override
	public synchronized String getReleaseNotes(final String groupName,
			final String artifactName, final String artifactVersion)
			throws ArtifactResolutionException {
		try {
			Artifact artifact = new DefaultArtifact(
					String.format("%s:%s:md:release-notes:%s", groupName,
							artifactName, artifactVersion));

			List<RemoteRepository> repos = new ArrayList<RemoteRepository>();
			for (ArtifactRepository r : this.repository.getRepositoryConfig()
					.getMavenConfig().getMavenRepositories().values())
				repos.add((RemoteRepository) r);

			ArtifactRequest request = new ArtifactRequest(artifact, repos, "");

			ArtifactResult resolveArtifact = system.resolveArtifact(session,
					request);

			artifact = resolveArtifact.getArtifact();
			File artifactFile = artifact.getFile();
			return org.apache.commons.io.FileUtils
					.readFileToString(artifactFile);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
	//
	// public static boolean isURL(final String url) {
	// try {
	// new URL(url);
	// return true;
	// } catch (Exception e) {
	// return false;
	// }
	// }
}
