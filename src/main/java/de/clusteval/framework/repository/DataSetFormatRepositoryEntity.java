/**
 * 
 */
package de.clusteval.framework.repository;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.data.dataset.format.DataSetFormat;
import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.data.dataset.format.IDataSetFormatParser;
import de.clusteval.utils.DynamicComponentMissingVersionException;

/**
 * @author Christian Wiwie
 * 
 */
public class DataSetFormatRepositoryEntity extends DynamicRepositoryObjectCollection {

	/**
	 * A map containing all classes of dataset format parsers registered in this
	 * repository.
	 */
	protected Map<String, Map<ComparableVersion, Class<? extends IDataSetFormatParser>>> dataSetFormatParser;

	/**
	 * @param repository
	 * @param parent
	 * @param basePath
	 */
	public DataSetFormatRepositoryEntity(IRepository repository, DynamicRepositoryObjectCollection parent, String basePath) {
		super(repository, DataSetFormat.class, parent, basePath);
		this.dataSetFormatParser = new ConcurrentHashMap<String, Map<ComparableVersion, Class<? extends IDataSetFormatParser>>>();
	}

	/**
	 * This method looks up and returns (if it exists) the class of the parser
	 * corresponding to the dataset format with the given name.
	 * 
	 * @param dataSetFormatName
	 *            The name of the class of the dataset format.
	 * @return The class of the dataset format parser with the given name or
	 *         null, if it does not exist.
	 */
	public Class<? extends IDataSetFormatParser> getDataSetFormatParser(final String dataSetFormatName,
			final int version) {
		Class<? extends IDataSetFormatParser> result = this.dataSetFormatParser.containsKey(dataSetFormatName)
				? this.dataSetFormatParser.get(dataSetFormatName).get(version)
				: null;
		if (result == null && parent != null)
			return ((DataSetFormatRepositoryEntity) this.parent).getDataSetFormatParser(dataSetFormatName, version);
		return result;
	}

	/**
	 * This method registers a dataset format parser.
	 * 
	 * @param dsFormatParser
	 *            The dataset format parser to register.
	 * @return True, if the dataset format parser replaced an old object.
	 * @throws DynamicComponentMissingVersionException
	 */
	public boolean registerDataSetFormatParser(final Class<? extends IDataSetFormatParser> dsFormatParser)
			throws DynamicComponentMissingVersionException {
		String className = dsFormatParser.getName().replace("Parser", "");
		if (!this.dataSetFormatParser.containsKey(className))
			this.dataSetFormatParser.put(className,
					new HashMap<ComparableVersion, Class<? extends IDataSetFormatParser>>());
		this.dataSetFormatParser.get(className).put(Repository.getVersionOfDynamicClass(dsFormatParser),
				dsFormatParser);
		return true;
	}

	/**
	 * This method checks whether a parser has been registered for the given
	 * dataset format class.
	 * 
	 * @param dsFormat
	 *            The class for which we want to know whether a parser has been
	 *            registered.
	 * @return True, if the parser has been registered.
	 */
	public boolean isRegisteredForDataSetFormat(final Class<? extends IDataSetFormat> dsFormat) {
		try {
			return this.dataSetFormatParser.containsKey(dsFormat.getName())
					&& this.dataSetFormatParser.get(dsFormat.getName())
							.containsKey(Repository.getVersionOfDynamicClass(dsFormat))
					|| (this.parent != null
							&& ((DataSetFormatRepositoryEntity) this.parent).isRegisteredForDataSetFormat(dsFormat));
		} catch (DynamicComponentMissingVersionException e) {
			return false;
		}
	}
}
