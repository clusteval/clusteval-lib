/**
 * 
 */
package de.clusteval.framework.repository;

import de.clusteval.cluster.FileBackedClustering;

/**
 * @author Christian Wiwie
 *
 */
public class ClusteringCollection extends StaticRepositoryObjectCollection {

	/**
	 * @param repository
	 * @param parent
	 * @param basePath
	 */
	public ClusteringCollection(IRepository repository, StaticRepositoryObjectCollection parent, String basePath) {
		super(repository, FileBackedClustering.class, parent, basePath);
		this.printOnRegister = false;
	}

	/* (non-Javadoc)
	 * @see de.clusteval.framework.repository.StaticRepositoryEntity#isVersioned()
	 */
	@Override
	public boolean isVersioned() {
		return false;
	}
}
