/**
 * 
 */
package de.clusteval.framework.repository;

import java.util.HashMap;
import java.util.Map;

//TODO: rename
public class StaticRepositoryEntityMap {

	protected Map<Class<? extends IRepositoryObject>, StaticRepositoryObjectCollection> map;

	/**
	 * 
	 */
	public StaticRepositoryEntityMap() {
		super();
		this.map = new HashMap<Class<? extends IRepositoryObject>, StaticRepositoryObjectCollection>();
	}

	@SuppressWarnings("unchecked")
	public <T extends IRepositoryObject> StaticRepositoryObjectCollection put(final Class<? extends T> c,
			final StaticRepositoryObjectCollection o) {
		return (StaticRepositoryObjectCollection) this.map.put(c, o);
	}

	@SuppressWarnings("unchecked")
	public <T extends IRepositoryObject> StaticRepositoryObjectCollection get(final Class<T> c) {
		Object o = this.map.get(c);
		if (o != null)
			return (StaticRepositoryObjectCollection) o;
		return null;
	}

	public boolean containsKey(final Class<? extends IRepositoryObject> c) {
		return this.map.containsKey(c);
	}
}