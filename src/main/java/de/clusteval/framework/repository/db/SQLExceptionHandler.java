/**
 * 
 */
package de.clusteval.framework.repository.db;

import java.sql.SQLException;

/**
 * @author Christian Wiwie
 *
 */
public abstract class SQLExceptionHandler {

	protected ISQLCommunicator sqlCommunicator;

	public SQLExceptionHandler(final ISQLCommunicator sqlCommunicator) {
		super();

		this.sqlCommunicator = sqlCommunicator;
	}

	public abstract void handleException(final SQLException e);
}
