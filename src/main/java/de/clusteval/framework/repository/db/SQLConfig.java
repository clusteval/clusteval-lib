/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.framework.repository.db;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Wrapper class to store a sql connection configuration.
 * 
 * @author Christian Wiwie
 * 
 */
public class SQLConfig implements ISQLConfig {

	public static ISQLConfig DUMMY_CONFIG = new SQLConfig(false, DB_TYPE.NONE, null, null, null, false);

	protected boolean usesSql;
	protected DB_TYPE dbType;
	protected String username;
	protected String database;
	protected String host;
	// optional
	protected String password;
	protected boolean usesPassword;

	public SQLConfig(final boolean usesSql, final DB_TYPE dbType, final String username, final String database,
			final String host, final String password) {
		super();
		this.usesSql = usesSql;
		this.dbType = dbType;
		this.username = username;
		this.database = database;
		this.host = host;
		this.password = password;
		this.usesPassword = password != null;
	}

	/**
	 * @param usesSql
	 * @param dbType
	 * @param username
	 * @param database
	 * @param host
	 * @param usesPassword
	 *            Whether the sql connection uses a password to connect and thus
	 *            prompt for it when connecting.
	 */
	public SQLConfig(final boolean usesSql, final DB_TYPE dbType, final String username, final String database,
			final String host, final boolean usesPassword) {
		super();
		this.usesSql = usesSql;
		this.dbType = dbType;
		this.username = username;
		this.database = database;
		this.host = host;
		this.usesPassword = usesPassword;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.db.ISQLConfig#usesSql()
	 */
	@Override
	public boolean usesSql() {
		return this.usesSql;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.db.ISQLConfig#getDatabaseType()
	 */
	@Override
	public DB_TYPE getDatabaseType() {
		return this.dbType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.db.ISQLConfig#getUsername()
	 */
	@Override
	public String getUsername() {
		return this.username;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.db.ISQLConfig#usesPassword()
	 */
	@Override
	public boolean usesPassword() {
		return this.usesPassword;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.db.ISQLConfig#getPassword()
	 */
	@Override
	public String getPassword() {
		if (this.password != null)
			return this.password;

		Logger log = LoggerFactory.getLogger(this.getClass());
		log.info("No database password specified in repository configuration. Asking for it interactively...");
		
		String password;
		Console c = System.console();
		if (c != null) {
			password = new String(System.console().readPassword("SQL password for '%s'@%s: ", username, host));
		}
		// handling for eclipse launching
		else {
			password = "";
			System.out.printf("SQL password for '%s'@%s: ", username, host);
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
			try {
				password = bufferedReader.readLine();
			} catch (IOException e) {
				// Ignore
			}
		}
		return password;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.db.ISQLConfig#getDatabase()
	 */
	@Override
	public String getDatabase() {
		return this.database;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.db.ISQLConfig#getHost()
	 */
	@Override
	public String getHost() {
		return this.host;
	}
}
