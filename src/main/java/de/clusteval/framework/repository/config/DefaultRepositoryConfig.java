/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.framework.repository.config;

import java.io.File;
import java.util.HashMap;

import de.clusteval.framework.repository.Repository;
import de.clusteval.framework.repository.db.ISQLConfig.DB_TYPE;
import de.clusteval.framework.repository.db.SQLConfig;
import de.clusteval.framework.repository.maven.MavenConfig;

/**
 * @author Christian Wiwie
 * 
 */
public class DefaultRepositoryConfig extends RepositoryConfig {

	/**
	 * @param absPath
	 */
	public DefaultRepositoryConfig(final File absPath) {
		super(absPath,
				new SQLConfig(false, DB_TYPE.NONE, null, null, null, false),
				new MavenConfig(), new HashMap<String, Long>(),
				Repository.SUPPORTED_REPOSITORY_VERSION);
	}
}
