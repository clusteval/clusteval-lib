/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.framework.repository.config;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalINIConfiguration;
import org.apache.commons.configuration.SubnodeConfiguration;
import org.apache.maven.artifact.versioning.ComparableVersion;
import org.eclipse.aether.repository.ArtifactRepository;
import org.eclipse.aether.repository.LocalRepository;
import org.eclipse.aether.repository.RemoteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.clusteval.framework.repository.Repository;
import de.clusteval.framework.repository.db.ISQLConfig;
import de.clusteval.framework.repository.db.ISQLConfig.DB_TYPE;
import de.clusteval.framework.repository.db.SQLConfig;
import de.clusteval.framework.repository.maven.IMavenConfig;
import de.clusteval.framework.repository.maven.MavenConfig;
import de.clusteval.framework.repository.maven.MavenRepositoryDuplicateException;
import de.clusteval.utils.DumpException;

/**
 * A repository configuration determines certain settings and options for a
 * {@link Repository} and also for the complete backend. This includes for
 * example whether an sql database should be used or how often the supervising
 * threads of the repository should scan for changes.
 * 
 * @author Christian Wiwie
 * 
 */
public class RepositoryConfig implements IRepositoryConfig {

	public static boolean NO_DATABASE;

	protected File absPath;

	/**
	 * This map holds the sleeping times for all threads that check the
	 * repository for changes.
	 */
	protected Map<String, Long> threadingSleepingTimes;

	/**
	 * This method parses a repository configuration from the file at the given
	 * absolute path.
	 * 
	 * <p>
	 * A repository configuration contains several sections and possible
	 * options:
	 * <ul>
	 * <li><b>[mysql]</b></li>
	 * <ul>
	 * <li><b>host</b>: The ip address of the mysql host server.</li>
	 * <li><b>database</b>: The mysql database name.</li>
	 * <li><b>user</b>: The username used to connect to the database.</li>
	 * <li><b>password</b>: The mysql password used to connect to the database.
	 * The password is prompted from the console and not parsed from the
	 * file.</li>
	 * </ul>
	 * <li><b>[threading]</b></li>
	 * <li><b>NameOfTheThreadSleepTime</b>: Sleeping time of the thread
	 * 'NameOfTheThread'. This option can be used to control the frequency, with
	 * which the threads check for changes on the filesystem.</li>
	 * </ul>
	 * 
	 * @param absConfigPath
	 *            The absolute path of the repository configuration file.
	 * @return The parsed repository configuration.
	 * @throws RepositoryConfigNotFoundException
	 * @throws RepositoryConfigurationException
	 */
	public static IRepositoryConfig parseFromFile(final File absConfigPath)
			throws RepositoryConfigNotFoundException,
			RepositoryConfigurationException {
		if (!absConfigPath.exists())
			throw new RepositoryConfigNotFoundException("Repository config \""
					+ absConfigPath + "\" does not exist!");

		Logger log = LoggerFactory.getLogger(RepositoryConfig.class);

		log.debug("Parsing repository config \"" + absConfigPath + "\"");

		try {

			HierarchicalINIConfiguration props = new HierarchicalINIConfiguration(
					absConfigPath);
			props.setThrowExceptionOnMissing(true);

			boolean usesMysql = false;
			ISQLConfig mysqlConfig = null;

			if (props.getSections().contains("mysql") && !NO_DATABASE) {
				usesMysql = true;
				String mysqlUsername, mysqlDatabase, mysqlHost;
				SubnodeConfiguration mysql = props.getSection("mysql");
				mysqlUsername = mysql.getString("user");

				mysqlDatabase = mysql.getString("database");
				mysqlHost = mysql.getString("host");
				if (!mysql.containsKey("password"))
					mysqlConfig = new SQLConfig(usesMysql, DB_TYPE.MYSQL,
							mysqlUsername, mysqlDatabase, mysqlHost, false);
				else if (mysql.getString("password").isEmpty())
					mysqlConfig = new SQLConfig(usesMysql, DB_TYPE.MYSQL,
							mysqlUsername, mysqlDatabase, mysqlHost, true);
				else {
					mysqlConfig = new SQLConfig(usesMysql, DB_TYPE.MYSQL,
							mysqlUsername, mysqlDatabase, mysqlHost,
							mysql.getString("password"));
					log.info(
							"Using database password specified in repository configuration");
				}
			} else if (props.getSections().contains("postgresql")
					&& !NO_DATABASE) {
				usesMysql = true;
				String username, db, host;
				SubnodeConfiguration mysql = props.getSection("postgresql");
				username = mysql.getString("user");

				db = mysql.getString("database");
				host = mysql.getString("host");
				if (!mysql.containsKey("password"))
					mysqlConfig = new SQLConfig(usesMysql, DB_TYPE.POSTGRESQL,
							username, db, host, false);
				else if (mysql.getString("password").isEmpty())
					mysqlConfig = new SQLConfig(usesMysql, DB_TYPE.POSTGRESQL,
							username, db, host, true);
				else {
					mysqlConfig = new SQLConfig(usesMysql, DB_TYPE.POSTGRESQL,
							username, db, host, mysql.getString("password"));
					log.info(
							"Using database password specified in repository configuration");
				}
			} else
				mysqlConfig = new SQLConfig(false, DB_TYPE.NONE, "", "", "",
						false);

			IMavenConfig mavenConfig = new MavenConfig();
			if (props.getSections().contains("maven")) {
				// add maven repositories to config
				SubnodeConfiguration mavenSection = props.getSection("maven");
				Iterator<String> it = mavenSection.getKeys();
				while (it.hasNext()) {
					String repoName = it.next();
					if (repoName.equals("resolveSnapshotVersions"))
						continue;
					String repoURL = mavenSection.getString(repoName);
					try {
						mavenConfig.addMavenRepository(repoName, repoURL);
					} catch (MavenRepositoryDuplicateException e) {
						log.warn(String.format("%s: Not adding again",
								e.getMessage()));
					}
				}

				if (mavenSection.containsKey("resolveSnapshotVersions"))
					mavenConfig.setResolveSnapshotVersions(
							mavenSection.getBoolean("resolveSnapshotVersions"));
			} else {
				mavenConfig.setUseMaven(false);
			}

			Map<String, Long> threadingSleepTimes = new HashMap<String, Long>();

			if (props.getSections().contains("threading")) {
				SubnodeConfiguration threading = props.getSection("threading");
				Iterator<String> it = threading.getKeys();
				while (it.hasNext()) {
					String key = it.next();
					if (key.endsWith("SleepTime")) {
						String subKey = key.substring(0,
								key.indexOf("SleepTime"));
						try {
							threadingSleepTimes.put(subKey,
									threading.getLong(key));
						} catch (Exception e) {
							// in case anything goes wrong, we just ignore this
							// option
							e.printStackTrace();
						}
					}
				}
			}

			ComparableVersion repositoryVersion = null;

			if (props.getSections().contains("repository")) {
				SubnodeConfiguration repository = props
						.getSection("repository");
				if (repository.containsKey("version"))
					repositoryVersion = new ComparableVersion(
							repository.getString("version"));
			}
			if (repositoryVersion == null)
				repositoryVersion = new ComparableVersion("1");

			return new RepositoryConfig(absConfigPath, mysqlConfig, mavenConfig,
					threadingSleepTimes, repositoryVersion);
		} catch (ConfigurationException e) {
			throw new RepositoryConfigurationException(e.getMessage());
		} catch (NoSuchElementException e) {
			throw new RepositoryConfigurationException(e.getMessage());
		}
	}

	/**
	 * The configuration of the mysql connection of the repository.
	 */
	protected ISQLConfig mysqlConfig;

	/**
	 * The configuration of maven dependency resolution of the repository.
	 */
	protected IMavenConfig mavenConfig;

	protected ComparableVersion version;

	/**
	 * Creates a new repository configuration.
	 * 
	 * @param absPath
	 *            The absolute path to this repository config.
	 * @param mysqlConfig
	 *            The mysql configuration for the repository.
	 * @param mavenConfig
	 *            The maven configuration for the repository.
	 * @param threadingSleepTimes
	 *            The sleep times of the threads created for the repository.
	 */
	public RepositoryConfig(final File absPath, final ISQLConfig mysqlConfig,
			final IMavenConfig mavenConfig,
			final Map<String, Long> threadingSleepTimes,
			final ComparableVersion version) {
		super();
		this.absPath = absPath;
		this.mysqlConfig = mysqlConfig;
		this.mavenConfig = mavenConfig;
		this.threadingSleepingTimes = threadingSleepTimes;
		this.version = version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.config.IRepositoryConfig#getSQLConfig()
	 */
	@Override
	public ISQLConfig getSQLConfig() {
		return this.mysqlConfig;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.config.IRepositoryConfig#setMysqlConfig
	 * (de.clusteval.framework.repository.db.SQLConfig)
	 */
	@Override
	public void setMysqlConfig(final ISQLConfig mysqlConfig) {
		this.mysqlConfig = mysqlConfig;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.config.IRepositoryConfig#getMavenConfig
	 * ()
	 */
	@Override
	public IMavenConfig getMavenConfig() {
		return mavenConfig;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.config.IRepositoryConfig#setMavenConfig
	 * (de.clusteval.framework.repository.maven.MavenConfig)
	 */
	@Override
	public void setMavenConfig(IMavenConfig mavenConfig) {
		this.mavenConfig = mavenConfig;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.config.IRepositoryConfig#
	 * getThreadSleepTimes()
	 */
	@Override
	public Map<String, Long> getThreadSleepTimes() {
		return this.threadingSleepingTimes;
	}

	/**
	 * @return the version
	 */
	@Override
	public ComparableVersion getVersion() {
		return version;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	@Override
	public void setVersion(ComparableVersion version) {
		this.version = version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.IDumpable#dumpToFile()
	 */
	@Override
	public void dumpToFile() throws DumpException {
		try {
			if (this.absPath.exists())
				this.absPath.delete();
			HierarchicalINIConfiguration props = new HierarchicalINIConfiguration(
					this.absPath);
			SubnodeConfiguration repoSection = props.getSection("repository");
			repoSection.setProperty("version",
					Repository.SUPPORTED_REPOSITORY_VERSION);
			SubnodeConfiguration mavenSection = props.getSection("maven");
			mavenSection.setProperty("resolveSnapshotVersions",
					this.mavenConfig.resolveSnapshotVersions());
			for (String name : mavenConfig.getMavenRepositories().keySet()) {
				ArtifactRepository r = mavenConfig.getMavenRepositories()
						.get(name);
				if (r instanceof RemoteRepository)
					mavenSection.setProperty(name,
							((RemoteRepository) r).getUrl());
				else if (r instanceof LocalRepository)
					mavenSection.setProperty(name,
							((LocalRepository) r).getBasedir());
			}
			props.save();
		} catch (Exception e) {
			throw new DumpException(e);
		}
	}
}
