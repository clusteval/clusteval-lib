/**
 * 
 */
package de.clusteval.framework.repository;

import org.apache.maven.artifact.versioning.ComparableVersion;
import org.rosuda.REngine.Rserve.RserveException;

import de.clusteval.program.r.RProgram;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.DynamicComponentMissingVersionException;
import de.clusteval.utils.DynamicComponentVersion;

/**
 * @author Christian Wiwie
 * 
 */
public class RProgramRepositoryEntity extends DynamicRepositoryObjectCollection {

	protected StaticRepositoryObjectCollection programEntity;

	/**
	 * @param repository
	 * @param programEntity
	 * @param parent
	 * @param basePath
	 */
	public RProgramRepositoryEntity(IRepository repository, StaticRepositoryObjectCollection programEntity,
			DynamicRepositoryObjectCollection parent, String basePath) {
		super(repository, RProgram.class, parent, basePath);
		this.programEntity = programEntity;
	}

	@Override
	public boolean register(final IRepositoryObject rProgram) throws RegisterException {
		programEntity.register(rProgram);
		return super.register(rProgram);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.DynamicRepositoryEntity#registerClass
	 * (java.lang.Class)
	 */
	@Override
	public boolean registerClass(Class<? extends IRepositoryObjectDynamicComponent> object)
			throws DynamicComponentMissingVersionException, DynamicComponentInitializationException {
		if (!super.registerClass(object))
			return false;

		try {
			// registers the program
			object.getConstructor(IRepository.class).newInstance(this.repository);
		} catch (NoSuchMethodException e1) {
			throw new DynamicComponentInitializationException(RProgram.class, object.getSimpleName(),
					"The class is missing the correct constructor");
		} catch (Exception e) {
			// this is likely because we passed faked parameters
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.DynamicRepositoryEntity#ensureLibraries
	 * (java.lang.Class)
	 */
	@Override
	protected boolean ensureLibraries(Class<? extends IRepositoryObjectDynamicComponent> classObject) throws InterruptedException {
		boolean result;
		try {
			// check whether we have R available
			this.repository.getRengineForCurrentThread();

			result = super.ensureLibraries(classObject);
		} catch (RserveException e) {
			this.repository.warn("\"" + classObject.getSimpleName()
					+ "\" could not be loaded since it requires R and no connection could be established.");
			result = false;
		}
		if (!result) {
			String className = classObject.getName();
			ComparableVersion classVersion = new ComparableVersion(
					classObject.getAnnotation(DynamicComponentVersion.class).version());

			this.classes.get(className).remove(classVersion);
		}
		return result;
	}
}
