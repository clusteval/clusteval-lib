/**
 * 
 */
package de.clusteval.framework.repository;

import java.io.File;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalINIConfiguration;

/**
 * @author Christian Wiwie
 *
 */
public abstract class DumpableRepositoryObject extends RepositoryObject
		implements
			IDumpableRepositoryObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7600888849634507132L;

	/**
	 * Instantiates a new dumpable repository object.
	 * 
	 * @param repository
	 *            The repository this object is registered in.
	 * @param changeDate
	 *            The changedate of this object can be used for identification
	 *            and equality checks of objects.
	 * @param absPath
	 *            The absolute path of this object is used for identification
	 *            and equality checks of objects.
	 */
	public DumpableRepositoryObject(final IRepository repository,
			final long changeDate, final File absPath) {
		super(repository, changeDate, absPath);
	}

	/**
	 * The copy constructor for dumpable repository objects.
	 * 
	 * <p>
	 * Cloned repository objects are never registered at the repository.
	 * 
	 * @param other
	 *            The object to clone.
	 */
	public DumpableRepositoryObject(final RepositoryObject other) {
		this(other.repository, other.changeDate,
				new File(other.absPath.getAbsolutePath()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IDumpableRepositoryObject#dumpToFile()
	 */
	@Override
	public final void dumpToFile() throws RepositoryObjectDumpException {
		HierarchicalINIConfiguration config = new HierarchicalINIConfiguration();
		config.setDelimiterParsingDisabled(true);
		this.dumpToFileHelper(config);
		try {
			config.save(this.absPath);
		} catch (ConfigurationException e) {
			throw new RepositoryObjectDumpException(e);
		}
		this.changeDate = this.absPath.lastModified();
	}

	protected abstract void dumpToFileHelper(
			final HierarchicalINIConfiguration config)
			throws RepositoryObjectDumpException;

}
