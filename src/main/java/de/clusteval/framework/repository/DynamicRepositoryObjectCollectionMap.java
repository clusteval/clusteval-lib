/**
 * 
 */
package de.clusteval.framework.repository;

import java.util.HashMap;
import java.util.Map;

public class DynamicRepositoryObjectCollectionMap {

	protected Map<Class<? extends IRepositoryObjectDynamicComponent>, DynamicRepositoryObjectCollection> map;

	/**
	 * 
	 */
	public DynamicRepositoryObjectCollectionMap() {
		super();
		this.map = new HashMap<Class<? extends IRepositoryObjectDynamicComponent>, DynamicRepositoryObjectCollection>();
	}

	@SuppressWarnings("unchecked")
	public <T extends IRepositoryObject> DynamicRepositoryObjectCollection put(final Class<? extends T> c,
			final DynamicRepositoryObjectCollection o) {
		return (DynamicRepositoryObjectCollection) this.map.put((Class<? extends IRepositoryObjectDynamicComponent>) c,
				o);
	}

	@SuppressWarnings("unchecked")
	public <T extends IRepositoryObject> DynamicRepositoryObjectCollection get(final Class<T> c) {
		Object o = this.map.get(c);
		if (o != null)
			return (DynamicRepositoryObjectCollection) o;
		return null;
	}

	public <T extends IRepositoryObject> boolean containsKey(final Class<T> c) {
		return this.map.containsKey(c);
	}
}