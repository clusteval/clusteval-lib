/**
 * 
 */
package de.clusteval.framework.repository;

import de.clusteval.data.dataset.DataSetFinderThread;

public abstract class RepositoryObjectCollection
		implements
			IRepositoryObjectCollection {

	protected IRepository repository;

	/**
	 * A boolean attribute indicating whether the datasets have been initialized
	 * by the {@link DataSetFinderThread}.
	 */
	private boolean initialized;

	private Class<? extends IRepositoryObject> entityClass;

	/**
	 * The absolute path to the directory within this repository, where all
	 * datasets are stored.
	 */
	protected String basePath;

	protected boolean printOnRegister = true;

	public RepositoryObjectCollection(final IRepository repository,
			Class<? extends IRepositoryObject> entityClass,
			final String basePath) {
		super();
		this.repository = repository;
		this.entityClass = entityClass;
		this.initialized = false;
		this.basePath = basePath;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepositoryEntity#setInitialized()
	 */
	@Override
	public synchronized void setInitialized() {
		this.initialized = true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepositoryEntity#isInitialized()
	 */
	@Override
	public synchronized boolean isInitialized() {
		return this.initialized;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepositoryEntity#getBasePath()
	 */
	@Override
	public String getBasePath() {
		return this.basePath;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepositoryEntity#register(de.clusteval
	 * .framework.repository.RepositoryObject)
	 */
	@Override
	public abstract boolean register(final IRepositoryObject object)
			throws RegisterException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepositoryEntity#unregister(de.
	 * clusteval.framework.repository.RepositoryObject)
	 */
	@Override
	public abstract boolean unregister(final IRepositoryObject object);

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepositoryEntity#getEntityClass()
	 */
	@Override
	public Class<? extends IRepositoryObject> getEntityClass() {
		return entityClass;
	}
}