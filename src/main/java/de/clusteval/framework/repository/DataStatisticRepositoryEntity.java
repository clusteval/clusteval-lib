/**
 * 
 */
package de.clusteval.framework.repository;

import java.util.HashMap;
import java.util.Map;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.data.statistics.DataStatistic;
import de.clusteval.data.statistics.IDataStatistic;
import de.clusteval.data.statistics.IDataStatisticCalculator;
import de.clusteval.utils.DynamicComponentMissingVersionException;

/**
 * @author Christian Wiwie
 * 
 */
public class DataStatisticRepositoryEntity extends DynamicRepositoryObjectCollection {

	/**
	 * A map containing all classes of data statistic calculators registered in
	 * this repository.
	 */
	protected Map<String, Map<ComparableVersion, Class<? extends IDataStatisticCalculator<? extends IDataStatistic>>>> dataStatisticCalculatorClasses;

	/**
	 * @param repository
	 * @param parent
	 * @param basePath
	 */
	public DataStatisticRepositoryEntity(IRepository repository, DataStatisticRepositoryEntity parent, String basePath) {
		super(repository, DataStatistic.class, parent, basePath);

		this.dataStatisticCalculatorClasses = new HashMap<String, Map<ComparableVersion, Class<? extends IDataStatisticCalculator<? extends IDataStatistic>>>>();
	}

	/**
	 * This method looks up and returns (if it exists) the class of the data
	 * statistic calculator for the datastatistic class with the given name.
	 * 
	 * @param dataStatisticClassName
	 *            The name of the datastatistic class.
	 * 
	 * @return The class of the data statistic calculator with the given name or
	 *         null, if it does not exist.
	 */
	public Class<? extends IDataStatisticCalculator<? extends IDataStatistic>> getDataStatisticCalculator(
			final String dataStatisticClassName, final ComparableVersion version) {
		Class<? extends IDataStatisticCalculator<? extends IDataStatistic>> result = this.dataStatisticCalculatorClasses
				.containsKey(dataStatisticClassName)
						? this.dataStatisticCalculatorClasses.get(dataStatisticClassName).get(version)
						: null;
		if (result == null && parent != null)
			result = ((DataStatisticRepositoryEntity) this.parent).getDataStatisticCalculator(dataStatisticClassName,
					version);
		return result;
	}

	public boolean registerDataStatisticCalculator(
			Class<? extends IDataStatisticCalculator<? extends IDataStatistic>> dataStatisticCalculator)
			throws DynamicComponentMissingVersionException {
		String className = dataStatisticCalculator.getName().replace("Calculator", "");
		if (!this.dataStatisticCalculatorClasses.containsKey(className))
			this.dataStatisticCalculatorClasses.put(className,
					new HashMap<ComparableVersion, Class<? extends IDataStatisticCalculator<? extends IDataStatistic>>>());
		return this.dataStatisticCalculatorClasses.get(className)
				.put(Repository.getVersionOfDynamicClass(dataStatisticCalculator), dataStatisticCalculator) != null;
	}
}
