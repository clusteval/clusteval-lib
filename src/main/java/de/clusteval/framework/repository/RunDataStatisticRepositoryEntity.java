/**
 * 
 */
package de.clusteval.framework.repository;

import java.util.HashMap;
import java.util.Map;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.run.statistics.IRunDataStatistic;
import de.clusteval.run.statistics.IRunDataStatisticCalculator;
import de.clusteval.run.statistics.RunDataStatistic;
import de.clusteval.utils.DynamicComponentMissingVersionException;

/**
 * @author Christian Wiwie
 * 
 */
public class RunDataStatisticRepositoryEntity extends DynamicRepositoryObjectCollection {

	/**
	 * A map containing all classes of run data statistic calculators registered
	 * in this repository.
	 */
	protected Map<String, Map<ComparableVersion, Class<? extends IRunDataStatisticCalculator<? extends IRunDataStatistic>>>> runDataStatisticCalculatorClasses;

	/**
	 * @param repository
	 * @param parent
	 * @param basePath
	 */
	public RunDataStatisticRepositoryEntity(IRepository repository, RunDataStatisticRepositoryEntity parent,
			String basePath) {
		super(repository, RunDataStatistic.class, parent, basePath);
		this.runDataStatisticCalculatorClasses = new HashMap<String, Map<ComparableVersion, Class<? extends IRunDataStatisticCalculator<? extends IRunDataStatistic>>>>();
	}

	/**
	 * This method looks up and returns (if it exists) the class of the run-data
	 * statistic calculator corresponding to the run-data-statistic with the
	 * given name.
	 * 
	 * @param runDataStatisticClassName
	 *            The name of the class of the run-data statistic.
	 * @return The class of the run-data statistic calculator for the given
	 *         name, or null if it does not exist.
	 */
	public Class<? extends IRunDataStatisticCalculator<? extends IRunDataStatistic>> getRunDataStatisticCalculator(
			final String runDataStatisticClassName, final ComparableVersion version) {
		Class<? extends IRunDataStatisticCalculator<? extends IRunDataStatistic>> result = this.runDataStatisticCalculatorClasses
				.containsKey(runDataStatisticClassName)
						? this.runDataStatisticCalculatorClasses.get(runDataStatisticClassName).get(version)
						: null;
		if (result == null && parent != null)
			result = ((RunDataStatisticRepositoryEntity) this.parent)
					.getRunDataStatisticCalculator(runDataStatisticClassName, version);
		return result;
	}

	/**
	 * This method registers a new run-data statistic calculator class.
	 * 
	 * @param object
	 *            The new class to register.
	 * @return True, if the new class replaced an old one.
	 * @throws DynamicComponentMissingVersionException
	 */
	public boolean registerRunDataStatisticCalculator(
			final Class<? extends IRunDataStatisticCalculator<? extends IRunDataStatistic>> object)
			throws DynamicComponentMissingVersionException {
		String className = object.getName().replace("Calculator", "");
		if (!this.runDataStatisticCalculatorClasses.containsKey(className))
			this.runDataStatisticCalculatorClasses.put(className,
					new HashMap<ComparableVersion, Class<? extends IRunDataStatisticCalculator<? extends IRunDataStatistic>>>());
		return this.runDataStatisticCalculatorClasses.get(className).put(Repository.getVersionOfDynamicClass(object),
				object) != null;
	}
}
