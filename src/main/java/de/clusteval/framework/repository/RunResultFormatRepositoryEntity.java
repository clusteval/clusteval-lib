/**
 * 
 */
package de.clusteval.framework.repository;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.run.runresult.format.IRunResultFormat;
import de.clusteval.run.runresult.format.IRunResultFormatParser;
import de.clusteval.run.runresult.format.RunResultFormat;
import de.clusteval.utils.DynamicComponentMissingVersionException;

/**
 * @author Christian Wiwie
 * 
 */
public class RunResultFormatRepositoryEntity extends DynamicRepositoryObjectCollection {

	/**
	 * A map containing all runresult format parsers registered in this
	 * repository.
	 */
	protected Map<String, Map<ComparableVersion, Class<? extends IRunResultFormatParser>>> runResultFormatParser;

	/**
	 * @param repository
	 * @param parent
	 * @param basePath
	 */
	public RunResultFormatRepositoryEntity(IRepository repository, DynamicRepositoryObjectCollection parent,
			String basePath) {
		super(repository, RunResultFormat.class, parent, basePath);
		this.runResultFormatParser = new ConcurrentHashMap<String, Map<ComparableVersion, Class<? extends IRunResultFormatParser>>>();
	}

	/**
	 * This method looks up and returns (if it exists) the class of the
	 * runresult format parser corresponding to the runresult format with the
	 * given name.
	 * 
	 * @param runResultFormatName
	 *            The runresult format name.
	 * @param version
	 *            The version of the format
	 * @return The runresult format parser for the given runresult format name,
	 *         or null if it does not exist.
	 */
	public Class<? extends IRunResultFormatParser> getRunResultFormatParser(final String runResultFormatName,
			final int version) {
		Class<? extends IRunResultFormatParser> result = this.runResultFormatParser.containsKey(runResultFormatName)
				? this.runResultFormatParser.get(runResultFormatName).get(version)
				: null;
		if (result == null && parent != null)
			return ((RunResultFormatRepositoryEntity) this.parent).getRunResultFormatParser(runResultFormatName,
					version);
		return result;
	}

	/**
	 * This method checks whether a parser has been registered for the given
	 * runresult format class.
	 * 
	 * @param runResultFormat
	 *            The class for which we want to know whether a parser has been
	 *            registered.
	 * @return True, if the parser has been registered.
	 */
	public boolean isRegisteredForRunResultFormat(final Class<? extends IRunResultFormat> runResultFormat) {
		try {
			return this.runResultFormatParser.containsKey(runResultFormat.getName())
					&& this.runResultFormatParser.get(runResultFormat.getName())
							.containsKey(Repository.getVersionOfDynamicClass(runResultFormat))
					|| (this.parent != null && ((RunResultFormatRepositoryEntity) this.parent)
							.isRegisteredForRunResultFormat(runResultFormat));
		} catch (DynamicComponentMissingVersionException e) {
			return false;
		}
	}

	/**
	 * This method registers a new runresult format parser class.
	 * 
	 * @param runResultFormatParser
	 *            The new class to register.
	 * @return True, if the new class replaced an old one.
	 * @throws DynamicComponentMissingVersionException
	 */
	public boolean registerRunResultFormatParser(final Class<? extends IRunResultFormatParser> runResultFormatParser)
			throws DynamicComponentMissingVersionException {
		String className = runResultFormatParser.getName().replace("Parser", "");
		if (!this.runResultFormatParser.containsKey(className))
			this.runResultFormatParser.put(className,
					new HashMap<ComparableVersion, Class<? extends IRunResultFormatParser>>());
		this.runResultFormatParser.get(className).put(Repository.getVersionOfDynamicClass(runResultFormatParser),
				runResultFormatParser);
		return true;
	}

	/**
	 * This method unregisters the passed object.
	 * 
	 * @param object
	 *            The object to be removed.
	 * @return True, if the object was remved successfully
	 */
	public boolean unregisterRunResultFormatParser(final Class<? extends IRunResultFormatParser> object) {
		return this.runResultFormatParser.remove(object.getName().replace("Parser", "")) != null;
	}
}
