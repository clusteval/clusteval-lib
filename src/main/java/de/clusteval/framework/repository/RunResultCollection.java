/**
 * 
 */
package de.clusteval.framework.repository;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import de.clusteval.run.runresult.IRunResult;
import de.clusteval.run.runresult.ParameterOptimizationResult;
import de.clusteval.run.runresult.RunResult;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * @author Christian Wiwie
 * 
 */
public class RunResultCollection extends StaticRepositoryObjectCollection {

	protected Map<String, RunResult> runResultIdentifier;

	/**
	 * @param repository
	 * @param parent
	 * @param basePath
	 */
	public RunResultCollection(IRepository repository,
			StaticRepositoryObjectCollection parent, String basePath) {
		super(repository, RunResult.class, parent, basePath);
		this.runResultIdentifier = new HashMap<String, RunResult>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObjectEntity#
	 * registerWhenExisting (de.clusteval.framework.repository.RepositoryObject,
	 * de.clusteval.framework.repository.RepositoryObject)
	 */
	@Override
	protected boolean registerWhenExisting(IRepositoryObject old,
			IRepositoryObject object) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObjectEntity#
	 * registerWithoutExisting
	 * (de.clusteval.framework.repository.RepositoryObject)
	 */
	@Override
	protected boolean registerWithoutExisting(IRepositoryObject object)
			throws RegisterException {
		if (!(object instanceof RunResult))
			return false;
		this.runResultIdentifier.put(((IRunResult) object).getIdentifier(),
				(RunResult) object);
		return super.registerWithoutExisting(object);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObjectEntity#
	 * unregisterAfterRemove(de.clusteval.framework.repository.RepositoryObject)
	 */
	@Override
	protected void unregisterAfterRemove(IRepositoryObject object) {
		if (!(object instanceof RunResult))
			return;
		this.runResultIdentifier.remove(((IRunResult) object).getIdentifier());

		// added 07.01.2013: add unregister of sqlcommunicator
		if (object instanceof ParameterOptimizationResult)
			// if the runresult folder exists, only delete the parameter
			// optimization run result from the database
			if (new File(object.getAbsolutePath()).getParentFile().exists())
				this.repository.getSqlCommunicator()
						.unregister((ParameterOptimizationResult) object);
			else if (Repository.getRepositoryForExactPath(
					object.getRepository().getBasePath()) != null)
				Repository.unregister(Repository.getRepositoryForExactPath(
						object.getRepository().getBasePath()));

		super.unregisterAfterRemove(object);
	}

	/**
	 * The absolute path to the directory, where for a certain runresult
	 * (identified by its unique run identifier) all clustering results are
	 * stored.
	 */
	public String getClusterResultsBasePath() {
		return FileUtils.buildPath(this.getBasePath(), "%RUNIDENTSTRING",
				"clusters");
	}

	/**
	 * The absolute path to the directory, where for a certain runresult
	 * (identified by its unique run identifier) all qualities of clustering
	 * results are stored.
	 */
	public String getClusterResultsQualityBasePath() {
		return FileUtils.buildPath(this.getBasePath(), "%RUNIDENTSTRING",
				"clusters");
	}

	/**
	 * @return The absolute path to the directory, where for a certain runresult
	 *         (identified by its unique run identifier) all analysis results
	 *         are stored.
	 */
	public String getAnalysisResultsBasePath() {
		return FileUtils.buildPath(this.getBasePath(), "%RUNIDENTSTRING",
				"analyses");
	}

	public String getResultLogBasePath() {
		return FileUtils.buildPath(this.getBasePath(), "%RUNIDENTSTRING",
				"logs");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.StaticRepositoryEntity#isVersioned()
	 */
	@Override
	public boolean isVersioned() {
		return false;
	}
	//
	// /* (non-Javadoc)
	// * @see
	// de.clusteval.framework.repository.StaticRepositoryEntity#hasVersionInAbsPath(java.lang.String)
	// */
	// @Override
	// public boolean hasVersionInName(String absPath) {
	// return false;
	// }
	//
	// /* (non-Javadoc)
	// * @see
	// de.clusteval.framework.repository.StaticRepositoryEntity#getVersionFromAbsPath(java.lang.String)
	// */
	// @Override
	// public ComparableVersion getVersionFromName(String absPath) {
	// return null;
	// }
	//
	// /* (non-Javadoc)
	// * @see
	// de.clusteval.framework.repository.StaticRepositoryEntity#insertVersionIntoAbsPath(java.lang.String,
	// org.apache.maven.artifact.versioning.ComparableVersion)
	// */
	// @Override
	// public String insertVersionIntoAbsPath(String absPath, ComparableVersion
	// version) {
	// return null;
	// }
	//
	// /* (non-Javadoc)
	// * @see
	// de.clusteval.framework.repository.StaticRepositoryEntity#getWithoutVersionInAbsPath(java.lang.String)
	// */
	// @Override
	// public String getWithoutVersionInName(String absPath) {
	// return null;
	// }
}