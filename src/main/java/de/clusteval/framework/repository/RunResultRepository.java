/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.framework.repository;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.cluster.IClustering;
import de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod;
import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.context.IContext;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.DataSetRegisterException;
import de.clusteval.data.dataset.IDataSet;
import de.clusteval.data.dataset.IDataSetConfig;
import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.data.dataset.generator.IDataSetGenerator;
import de.clusteval.data.dataset.type.IDataSetType;
import de.clusteval.data.distance.IDistanceMeasure;
import de.clusteval.data.goldstandard.IGoldStandard;
import de.clusteval.data.goldstandard.IGoldStandardConfig;
import de.clusteval.data.goldstandard.format.GoldStandardFormat;
import de.clusteval.data.preprocessing.IDataPreprocessor;
import de.clusteval.data.randomizer.IDataRandomizer;
import de.clusteval.data.statistics.IDataStatistic;
import de.clusteval.framework.repository.config.RepositoryConfigNotFoundException;
import de.clusteval.framework.repository.config.RepositoryConfigurationException;
import de.clusteval.framework.repository.db.DatabaseConnectException;
import de.clusteval.framework.repository.db.DatabaseInitializationException;
import de.clusteval.framework.repository.db.ISQLCommunicator;
import de.clusteval.framework.repository.db.RunResultSQLCommunicator;
import de.clusteval.framework.repository.db.StubSQLCommunicator;
import de.clusteval.framework.repository.maven.IMavenRepositoryResolver;
import de.clusteval.framework.threading.RunResultRepositorySupervisorThread;
import de.clusteval.framework.threading.SupervisorThread;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IStandaloneProgram;
import de.clusteval.program.r.IRProgram;
import de.clusteval.run.IRun;
import de.clusteval.run.runresult.IRunResult;
import de.clusteval.run.runresult.format.IRunResultFormat;
import de.clusteval.run.runresult.postprocessing.IRunResultPostprocessor;
import de.clusteval.run.statistics.IRunDataStatistic;
import de.clusteval.run.statistics.IRunStatistic;
import de.clusteval.utils.IFinder;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * A runresult repository corresponds to a runresult directory in the results
 * directory of its parent repository.
 * 
 * <p>
 * The runresult directories contain copies of the inputs and configurations at
 * the time, the corresponding runs were started. Therefore every runresult
 * directory can be treated as an individual smaller repository which contains a
 * subset of the files as a regular {@link Repository}.
 * 
 * @author Christian Wiwie
 * 
 */
public class RunResultRepository extends Repository {

	// TODO: check, whether all those are needed for a RunResultRepository
	/**
	 * @param basePath
	 *            The absolute path of the root directory of this repository.
	 * @param parent
	 *            The parent repository.
	 * @throws FileNotFoundException
	 * @throws RepositoryAlreadyExistsException
	 * @throws InvalidRepositoryException
	 * @throws RepositoryConfigurationException
	 * @throws RepositoryConfigNotFoundException
	 * @throws DatabaseConnectException
	 * @throws RepositoryVersionTooNewException
	 * @throws RepositoryVersionTooOldException
	 * @throws RepositoryCouldNotBeMigratedException
	 */
	public RunResultRepository(String basePath, IRepository parent)
			throws FileNotFoundException, RepositoryAlreadyExistsException,
			InvalidRepositoryException, RepositoryConfigNotFoundException,
			RepositoryConfigurationException, DatabaseConnectException,
			RepositoryVersionTooOldException, RepositoryVersionTooNewException,
			RepositoryCouldNotBeMigratedException {
		super(basePath, parent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.Repository#createSQLCommunicator()
	 */
	@Override
	protected ISQLCommunicator createSQLCommunicator()
			throws DatabaseConnectException, DatabaseInitializationException {
		if (this.parent.getRepositoryConfig().getSQLConfig().usesSql())
			return new RunResultSQLCommunicator(this,
					this.parent.getRepositoryConfig().getSQLConfig());
		return new StubSQLCommunicator(this);
	}

	// /*
	// * (non-Javadoc)
	// *
	// * @see
	// * de.clusteval.framework.repository.Repository#createAndAddStaticEntity(
	// * java.lang.Class, java.lang.String)
	// */
	// @Override
	// protected <T extends IRepositoryObject> void
	// createAndAddStaticEntity(Class<T> c, String basePath) {
	// this.staticRepositoryEntities.put(c, new
	// RunResultStaticRepositoryEntity(this, c,
	// this.parent != null ? this.parent.staticRepositoryEntities.get(c) : null,
	// basePath));
	// }

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.Repository#initAttributes()
	 */
	@Override
	protected void initAttributes() {

		this.staticRepositoryEntities = new StaticRepositoryEntityMap();

		this.dynamicRepositoryEntities = new DynamicRepositoryObjectCollectionMap();

		this.staticRepositoryEntities.put(IDataConfig.class,
				new RepositoryObjectCollectionRunResultRepository(this,
						IDataConfig.class,
						this.parent != null
								? this.parent.staticRepositoryEntities
										.get(IDataConfig.class)
								: null,
						FileUtils.buildPath(this.basePath, "configs")));
		this.staticRepositoryEntities.put(IDataSetConfig.class,
				new RepositoryObjectCollectionRunResultRepository(this,
						IDataSetConfig.class,
						this.parent != null
								? this.parent.staticRepositoryEntities
										.get(IDataSetConfig.class)
								: null,
						FileUtils.buildPath(this.basePath, "configs")));
		this.staticRepositoryEntities.put(IGoldStandardConfig.class,
				new RepositoryObjectCollectionRunResultRepository(this,
						IGoldStandardConfig.class,
						this.parent != null
								? this.parent.staticRepositoryEntities
										.get(IGoldStandardConfig.class)
								: null,
						FileUtils.buildPath(this.basePath, "configs")));
		this.staticRepositoryEntities.put(IProgramConfig.class,
				new RepositoryObjectCollectionRunResultRepository(this,
						IProgramConfig.class,
						this.parent != null
								? this.parent.staticRepositoryEntities
										.get(IProgramConfig.class)
								: null,
						FileUtils.buildPath(this.basePath, "configs")));
		this.staticRepositoryEntities.put(IRun.class,
				new RepositoryObjectCollectionRunResultRepository(this,
						IRun.class,
						this.parent != null
								? this.parent.staticRepositoryEntities
										.get(IRun.class)
								: null,
						FileUtils.buildPath(this.basePath, "configs")));

		this.staticRepositoryEntities.put(IDataSet.class,
				new DataSetCollectionRunResultRepository(this,
						this.parent != null
								? this.parent.staticRepositoryEntities
										.get(IDataSet.class)
								: null,
						FileUtils.buildPath(this.basePath, "inputs")));

		this.staticRepositoryEntities.put(IGoldStandard.class,
				new GoldStandardCollectionRunResultRepository(this,
						this.parent != null
								? this.parent.staticRepositoryEntities
										.get(IGoldStandard.class)
								: null,
						FileUtils.buildPath(this.basePath, "goldstandards")));

		this.staticRepositoryEntities.put(IStandaloneProgram.class,
				this.parent.staticRepositoryEntities
						.get(IStandaloneProgram.class));
		this.staticRepositoryEntities.put(IClustering.class,
				this.parent.staticRepositoryEntities.get(IClustering.class));

		this.staticRepositoryEntities.put(IRunResult.class,
				this.parent.staticRepositoryEntities.get(IRunResult.class));

		this.staticRepositoryEntities.put(IFinder.class,
				this.parent.staticRepositoryEntities.get(IFinder.class));

		this.dynamicRepositoryEntities.put(IDistanceMeasure.class,
				this.parent.dynamicRepositoryEntities
						.get(IDistanceMeasure.class));

		this.dynamicRepositoryEntities.put(IDataSetGenerator.class,
				this.parent.dynamicRepositoryEntities
						.get(IDataSetGenerator.class));

		this.dynamicRepositoryEntities.put(IDataRandomizer.class,
				this.parent.dynamicRepositoryEntities
						.get(IDataRandomizer.class));

		this.dynamicRepositoryEntities.put(IDataPreprocessor.class,
				this.parent.dynamicRepositoryEntities
						.get(IDataPreprocessor.class));

		this.dynamicRepositoryEntities.put(IRunResultPostprocessor.class,
				this.parent.dynamicRepositoryEntities
						.get(IRunResultPostprocessor.class));

		this.dynamicRepositoryEntities.put(IDataStatistic.class,
				this.parent.dynamicRepositoryEntities
						.get(IDataStatistic.class));

		this.dynamicRepositoryEntities.put(IRunStatistic.class,
				this.parent.dynamicRepositoryEntities.get(IRunStatistic.class));

		this.dynamicRepositoryEntities.put(IRunDataStatistic.class,
				this.parent.dynamicRepositoryEntities
						.get(IRunDataStatistic.class));

		this.dynamicRepositoryEntities.put(IClusteringQualityMeasure.class,
				this.parent.dynamicRepositoryEntities
						.get(IClusteringQualityMeasure.class));

		this.dynamicRepositoryEntities.put(IRProgram.class,
				this.parent.dynamicRepositoryEntities.get(IRProgram.class));

		this.dynamicRepositoryEntities.put(IContext.class,
				this.parent.dynamicRepositoryEntities.get(IContext.class));

		this.dynamicRepositoryEntities.put(IParameterOptimizationMethod.class,
				this.parent.dynamicRepositoryEntities
						.get(IParameterOptimizationMethod.class));

		this.dynamicRepositoryEntities.put(IDataSetType.class,
				this.parent.dynamicRepositoryEntities.get(IDataSetType.class));

		this.dynamicRepositoryEntities.put(IDataSetFormat.class,
				this.parent.dynamicRepositoryEntities
						.get(IDataSetFormat.class));

		this.dynamicRepositoryEntities.put(IRunResultFormat.class,
				this.parent.dynamicRepositoryEntities
						.get(IRunResultFormat.class));

		this.goldStandardFormats = new ConcurrentHashMap<GoldStandardFormat, GoldStandardFormat>();

		this.internalDoubleAttributes = this.parent.internalDoubleAttributes;
		this.internalStringAttributes = this.parent.internalStringAttributes;
		this.internalIntegerAttributes = this.parent.internalIntegerAttributes;

		// added 14.04.2013
		// changed 2017/07/31 to be independent of the parent repository
		// exceptions
		this.knownFinderExceptions = new ConcurrentHashMap<Class<? extends IRepositoryObject>, Map<ISerializableWrapperRepositoryObject<? extends IRepositoryObject>, List<Throwable>>>();

		this.finderClassLoaders = this.parent.finderClassLoaders;
		this.finderWaitingFiles = this.parent.finderWaitingFiles;
		this.finderLoadedJarFileChangeDates = this.parent.finderLoadedJarFileChangeDates;

		this.finderErrorObjects = new ConcurrentHashMap<Class<? extends IRepositoryObject>, Map<ISerializableWrapperRepositoryObject<IRepositoryObject>, IRepositoryObject>>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.Repository#initializePaths()
	 */
	@Override
	protected void initializePaths() throws InvalidRepositoryException {
		if (this.parent == null)
			throw new InvalidRepositoryException(
					"A RunResultRepository needs a valid parent repository");

		this.supplementaryBasePath = this.parent.supplementaryBasePath;
		this.suppClusteringBasePath = this.parent.suppClusteringBasePath;
		this.formatsBasePath = this.parent.formatsBasePath;
		this.generatorBasePath = this.parent.generatorBasePath;
		this.typesBasePath = this.parent.typesBasePath;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.Repository#log(java.lang.String)
	 */
	@Override
	public void info(String message) {
		// reduce visibility of log messages
		this.log.debug(message);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.repository.Repository#warn(java.lang.String)
	 */
	@Override
	public void warn(String message) {
		// reduce visibility of log messages
		this.log.debug(message);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.Repository#createSupervisorThread()
	 */
	@Override
	protected SupervisorThread createSupervisorThread() {
		return new RunResultRepositorySupervisorThread(this,
				this.getParent().repositoryConfig.getThreadSleepTimes());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.Repository#getVersion()
	 */
	@Override
	public ComparableVersion getVersion() {
		// TODO: will this work?
		return this.parent.getVersion();
	}

	@Override
	public String getRelativeFilePathToRepository(final File file) {
		return new File(this.parent.getBasePath()).toPath()
				.relativize(file.toPath()).toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#getKnownFinderExceptions()
	 */
	@Override
	public Map<Class<? extends IRepositoryObject>, Map<ISerializableWrapperRepositoryObject<? extends IRepositoryObject>, List<Throwable>>> getKnownFinderExceptions() {
		return this.parent.knownFinderExceptions;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#getJARFinderClassLoaders()
	 */
	@Override
	public Map<URL, URLClassLoader> getJARFinderClassLoaders() {
		return this.parent.finderClassLoaders;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#getJARFinderWaitingFiles()
	 */
	@Override
	public Map<String, List<File>> getJARFinderWaitingFiles() {
		return this.parent.finderWaitingFiles;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#
	 * getFinderLoadedJarFileChangeDates()
	 */
	@Override
	public Map<String, Long> getFinderLoadedJarFileChangeDates() {
		return this.parent.finderLoadedJarFileChangeDates;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#
	 * getFullClassNamesAndVersionsForPresentFiles()
	 */
	@Override
	public Map<Class<? extends IRepositoryObject>, Map<String, Set<ComparableVersion>>> getFullClassNamesAndVersionsForPresentFiles() {
		return this.parent.fullClassNamesAndVersionsForPresentFiles;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.Repository#getMavenResolver()
	 */
	@Override
	public IMavenRepositoryResolver getMavenResolver() {
		return this.parent.getMavenResolver();
	}
}

class RepositoryObjectCollectionRunResultRepository<T extends IRepositoryObject>
		extends
			StaticRepositoryObjectCollection {

	public RepositoryObjectCollectionRunResultRepository(IRepository repository,
			Class<? extends RepositoryObject> entityClass,
			StaticRepositoryObjectCollection parent, String basePath) {
		super(repository, entityClass, parent, basePath);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.RepositoryObjectEntity#register(de.
	 * clusteval.framework.repository.RepositoryObject)
	 */
	@Override
	public boolean register(IRepositoryObject object) throws RegisterException {
		IRepositoryObject objectInParentRepository;
		try {
			objectInParentRepository = object.getRepository().getParent()
					.getStaticObjectWithNameAndVersion(this.getEntityClass(),
							object.toString());
			return super.register(object);
		} catch (ObjectNotRegisteredException e) {
			throw new RegisterException(String.format(
					"The %s '%s' of a runresult is missing in its parent repository.",
					object.getClass().getSimpleName(),
					object.getAbsolutePath()));
		} catch (ObjectVersionNotRegisteredException e) {
			throw new RegisterException(String.format(
					"The version 'v%s' of %s '%s' of a runresult is missing in its parent repository.",
					e.getVersion(), object.getClass().getSimpleName(),
					object.getAbsolutePath()));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.StaticRepositoryEntity#isVersioned()
	 */
	@Override
	public boolean isVersioned() {
		return this.parent.isVersioned();
	}
	//
	// /* (non-Javadoc)
	// * @see
	// de.clusteval.framework.repository.StaticRepositoryEntity#hasVersionInAbsPath(java.lang.String)
	// */
	// @Override
	// public boolean hasVersionInName(String absPath) {
	// return this.parent.hasVersionInName(absPath);
	// }
	//
	// /* (non-Javadoc)
	// * @see
	// de.clusteval.framework.repository.StaticRepositoryEntity#getVersionFromAbsPath(java.lang.String)
	// */
	// @Override
	// public ComparableVersion getVersionFromName(String absPath) {
	// return this.parent.getVersionFromName(absPath);
	// }
	//
	// /* (non-Javadoc)
	// * @see
	// de.clusteval.framework.repository.StaticRepositoryEntity#insertVersionIntoAbsPath(java.lang.String,
	// org.apache.maven.artifact.versioning.ComparableVersion)
	// */
	// @Override
	// public String insertVersionIntoAbsPath(String absPath, ComparableVersion
	// version) {
	// return this.parent.insertVersionIntoAbsPath(absPath, version);
	// }
	//
	// /* (non-Javadoc)
	// * @see
	// de.clusteval.framework.repository.StaticRepositoryEntity#getWithoutVersionInAbsPath(java.lang.String)
	// */
	// @Override
	// public String getWithoutVersionInName(String absPath) {
	// return this.parent.getWithoutVersionInName(absPath);
	// }
}

class DataSetCollectionRunResultRepository
		extends
			RepositoryObjectCollectionRunResultRepository {

	/**
	 * @param repository
	 * @param parent
	 * @param basePath
	 */
	public DataSetCollectionRunResultRepository(IRepository repository,
			StaticRepositoryObjectCollection parent, String basePath) {
		super(repository, IDataSet.class, parent, basePath);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.RepositoryObjectEntity#register(de.
	 * clusteval.framework.repository.RepositoryObject)
	 */
	@Override
	public boolean register(IRepositoryObject object) throws RegisterException {
		IRepositoryObject dataSetInParentRepository;
		try {
			dataSetInParentRepository = object.getRepository().getParent()
					.getStaticObjectWithNameAndVersion(IDataSet.class,
							object.toString());
			return super.register(object);
		} catch (ObjectNotRegisteredException e) {
			throw new DataSetRegisterException("The dataset '"
					+ object.getAbsolutePath()
					+ "' of a runresult is missing in its parent repository.");
		} catch (ObjectVersionNotRegisteredException e) {
			throw new DataSetRegisterException("The version 'v" + e.getVersion()
					+ "' of dataset '" + object.getAbsolutePath()
					+ "' of a runresult is missing in its parent repository.");
		}
	}
}

class GoldStandardCollectionRunResultRepository
		extends
			RepositoryObjectCollectionRunResultRepository {

	/**
	 * @param repository
	 * @param parent
	 * @param basePath
	 */
	public GoldStandardCollectionRunResultRepository(IRepository repository,
			StaticRepositoryObjectCollection parent, String basePath) {
		super(repository, IGoldStandard.class, parent, basePath);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.RepositoryObjectEntity#register(de.
	 * clusteval.framework.repository.RepositoryObject)
	 */
	@Override
	public boolean register(IRepositoryObject object) throws RegisterException {
		IRepositoryObject gsInParentRepository;
		try {
			gsInParentRepository = object.getRepository().getParent()
					.getStaticObjectWithNameAndVersion(IGoldStandard.class,
							object.toString());
			return super.register(object);
		} catch (ObjectNotRegisteredException e) {
			throw new DataSetRegisterException("The goldstandard '"
					+ object.getAbsolutePath()
					+ "' of a runresult is missing in its parent repository.");
		} catch (ObjectVersionNotRegisteredException e) {
			throw new DataSetRegisterException("The version 'v" + e.getVersion()
					+ "' of goldstandard '" + object.getAbsolutePath()
					+ "' of a runresult is missing in its parent repository.");
		}
	}
}