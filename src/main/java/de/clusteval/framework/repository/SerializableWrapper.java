/**
 * 
 */
package de.clusteval.framework.repository;

import java.io.File;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Christian Wiwie
 *
 * @param <R>
 */
public abstract class SerializableWrapper<R extends Object>
		implements
			Serializable,
			ISerializableWrapper<R> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1743628886969550432L;
	public R wrappedComponent;
	protected File absPath;
	protected String name;
	protected String version;

	/**
	 * 
	 */
	public SerializableWrapper() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.ISerializableWrapper#deserialize(de.
	 * clusteval.framework.repository.IRepository)
	 */
	@Override
	public R deserialize(final IRepository repository)
			throws DeserializationException {
		return this.deserialize(repository, false);
	}

	@Override
	public R deserialize(final IRepository repository, final boolean force)
			throws DeserializationException {
		if (!force && wrappedComponent != null)
			return wrappedComponent;
		R result = deserializeInternal(repository);
		this.wrappedComponent = result;
		return result;
	}

	protected abstract R deserializeInternal(final IRepository repository)
			throws DeserializationException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.ISerializableWrapper#
	 * setWrappedComponent(R)
	 */
	@Override
	public void setWrappedComponent(R wrappedComponent) {
		this.wrappedComponent = wrappedComponent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.ISerializableWrapper#getFile()
	 */
	@Override
	public File getFile() {
		return absPath;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.ISerializableWrapper#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.ISerializableWrapper#getVersion()
	 */
	@Override
	public String getVersion() {
		return version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.ISerializableWrapper#equals(java.lang.
	 * Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof SerializableWrapper))
			return false;

		SerializableWrapper<R> other = (SerializableWrapper) obj;
		return Objects.equals(this.absPath, other.absPath)
				&& this.name.equals(other.name)
				&& this.version.equals(other.version);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.ISerializableWrapper#hashCode()
	 */
	@Override
	public int hashCode() {
		return (Objects.toString(this.absPath) + " " + this.name + ":"
				+ this.version).hashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.ISerializableWrapper#toString()
	 */
	@Override
	public String toString() {
		if (this.wrappedComponent != null)
			return this.wrappedComponent.toString();
		if (this.version.isEmpty())
			return this.name;
		return String.format("%s:%s", this.name, this.version);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.ISerializableWrapper#compareTo(de.
	 * clusteval.framework.repository.ISerializableWrapperRepositoryObject)
	 */
	@Override
	public int compareTo(ISerializableWrapper<R> o) {
		if (this.absPath != null)
			return this.absPath.compareTo(o.getFile());
		return this.toString().toLowerCase()
				.compareTo(o.toString().toLowerCase());
	}

}