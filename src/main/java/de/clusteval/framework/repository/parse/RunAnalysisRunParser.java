/**
 * 
 */
package de.clusteval.framework.repository.parse;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.configuration.ConfigurationException;

import de.clusteval.run.IRunAnalysisRun;
import de.clusteval.run.RunAnalysisRun;
import de.clusteval.run.statistics.IRunStatistic;
import de.clusteval.run.statistics.RunStatistic;
import de.clusteval.run.statistics.UnknownRunStatisticException;
import de.clusteval.utils.DynamicComponentInitializationException;

/**
 * @author Christian Wiwie
 *
 */
public class RunAnalysisRunParser extends AnalysisRunParser<RunAnalysisRun> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.parse.Parser#getClassToParse()
	 */
	@Override
	protected Class<IRunAnalysisRun> getClassToParse() {
		return IRunAnalysisRun.class;
	}

	@Override
	public void parseFromFile(File absPath)
			throws RepositoryObjectParseException {
		super.parseFromFile(absPath);

		try {

			/*
			 * An analysis run consists of a set of dataconfigs
			 */
			List<String> uniqueRunIdentifiers = parseUniqueRunIds();

			List<IRunStatistic> runStatistics = parseRunStatistics();

			result = new RunAnalysisRun(repo, context, changeDate, absPath,
					uniqueRunIdentifiers, runStatistics);
			if (!recoverFromExceptions) {
				result.register();
				result = (RunAnalysisRun) repo.getRegisteredObject(result,
						false);
			}
		} catch (Exception e) {
			if (!handleException(e))
				throw new RepositoryObjectParseException(this.getClassToParse(),
						this.name, this.versionString, e);
		}
	}

	protected List<IRunStatistic> parseRunStatistics()
			throws ConfigurationException, UnknownRunStatisticException,
			DynamicComponentInitializationException {
		List<IRunStatistic> runStatistics = new LinkedList<IRunStatistic>();

		try {
			/**
			 * We catch the exceptions such that all statistics are tried to be
			 * loaded once so that they are ALL registered as missing in the
			 * repository.
			 */
			List<UnknownRunStatisticException> thrownExceptions = new ArrayList<UnknownRunStatisticException>();
			for (String runStatistic : getProps()
					.getStringArray("runStatistics")) {
				try {
					runStatistics.add(parseRunStatistic(runStatistic));
				} catch (UnknownRunStatisticException e) {
					if (!handleException(e))
						thrownExceptions.add(e);
				}
			}
			if (thrownExceptions.size() > 0) {
				// just throw the first exception
				throw thrownExceptions.get(0);
			}
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return runStatistics;
	}

	protected IRunStatistic parseRunStatistic(String runStatistic)
			throws UnknownRunStatisticException,
			DynamicComponentInitializationException {
		try {
			return RunStatistic.parseFromString(repo, runStatistic);
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return null;
	}

	protected List<String> parseUniqueRunIds() throws ConfigurationException {
		List<String> uniqueRunIdentifiers = new LinkedList<String>();
		try {
			uniqueRunIdentifiers.addAll(Arrays
					.asList(getProps().getStringArray("uniqueRunIdentifiers")));
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return uniqueRunIdentifiers;
	};
}