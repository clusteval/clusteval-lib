/**
 * 
 */
package de.clusteval.framework.repository.parse;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.slf4j.LoggerFactory;

import de.clusteval.context.Context;
import de.clusteval.data.dataset.NoDataSetException;
import de.clusteval.program.IStandaloneProgram;
import de.clusteval.program.StandaloneProgram;

/**
 * @author Christian Wiwie
 *
 */
public class StandaloneProgramParser
		extends
			RepositoryObjectParser<StandaloneProgram> {

	protected String alias;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.parse.Parser#getFileEnding()
	 */
	@Override
	protected String getFileEnding() {
		return "";
	}

	@Override
	protected final String parseVersion() {
		String name = absPath.getParentFile().getName()
				.replaceAll(getFileEnding() + "$", "");
		if (!hasVersionInFileName(name))
			return "";
		int colonInd = name.lastIndexOf(".");
		return name.substring(colonInd + 2);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.parse.Parser#ensureVersionInFileName(
	 * java.io.File)
	 */
	@Override
	protected void ensureVersionInFileName(File absPath)
			throws NoDataSetException {
		super.ensureVersionInFileName(absPath.getParentFile());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.parse.Parser#getClassToParse()
	 */
	@Override
	protected Class<IStandaloneProgram> getClassToParse() {
		return IStandaloneProgram.class;
	}

	protected String parseAlias() {
		File optionalAliasFile = new File(
				absPath.getAbsolutePath() + ".clusteval.alias");
		// read first line of file and use it as alias
		if (optionalAliasFile.exists()) {
			try {
				byte[] encoded = Files.readAllBytes(optionalAliasFile.toPath());
				return new String(encoded);

			} catch (IOException e) {
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.RepositoryObjectParser#parseFromFile
	 * (java.io.File)
	 */
	@Override
	public void parseFromFile(File absPath)
			throws RepositoryObjectParseException {
		super.parseFromFile(absPath);
		try {
			ensureVersionInFileName(absPath);

			final long changeDate = absPath.lastModified();

			alias = parseAlias();

			LoggerFactory.getLogger(StandaloneProgram.class)
					.debug("Parsing standalone program \"" + absPath + "\"");

			result = new StandaloneProgram(repo,
					Context.parseFromString(repo, "ClusteringContext"),
					changeDate, absPath, alias);
			if (!recoverFromExceptions) {
				result.register();
				result = (StandaloneProgram) repo.getRegisteredObject(result);
			}
			LoggerFactory.getLogger(StandaloneProgram.class)
					.debug("Standalone program parsed");
		} catch (Exception e) {
			if (!handleException(e))
				throw new RepositoryObjectParseException(this.getClassToParse(),
						this.name, this.versionString, e);
		}
	}
}