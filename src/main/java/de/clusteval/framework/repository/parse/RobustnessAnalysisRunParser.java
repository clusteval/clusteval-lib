/**
 * 
 */
package de.clusteval.framework.repository.parse;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.configuration.ConfigurationException;

import de.clusteval.data.DataConfig;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.randomizer.DataRandomizer;
import de.clusteval.data.randomizer.IDataRandomizer;
import de.clusteval.framework.repository.RunResultRepository;
import de.clusteval.program.ParameterSet;
import de.clusteval.run.IRobustnessAnalysisRun;
import de.clusteval.run.RobustnessAnalysisRun;
import de.clusteval.run.RunException;

/**
 * @author Christian Wiwie
 *
 */
public class RobustnessAnalysisRunParser
		extends
			ExecutionRunParser<RobustnessAnalysisRun> {

	protected List<String> uniqueRunIdentifiers;
	protected List<IDataConfig> originalDataConfigs;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.parse.Parser#getClassToParse()
	 */
	@Override
	protected Class<IRobustnessAnalysisRun> getClassToParse() {
		return IRobustnessAnalysisRun.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.ExecutionRunParser#parseFromFile(java
	 * .io.File)
	 */
	@Override
	public void parseFromFile(File absPath)
			throws RepositoryObjectParseException {
		super.parseFromFile(absPath);

		try {
			String[] list = parseUniqueRunIds();
			// 10.07.2014: remove duplicates.
			list = new ArrayList<String>(
					new HashSet<String>(Arrays.asList(list)))
							.toArray(new String[0]);
			this.uniqueRunIdentifiers = Arrays.asList(list);

			String randomizerS = parseRandomizerName();
			IDataRandomizer randomizer = parseRandomizer(randomizerS);
			int numberOfRandomizedDataSets = parseNumberRandomizedDataSets();

			// get randomizer parameter sets
			List<ParameterSet> paramSets = parseRandomizersParameterSets(
					randomizerS);

			result = new RobustnessAnalysisRun(repo, context, changeDate,
					absPath, uniqueRunIdentifiers, programConfigs, dataConfigs,
					originalDataConfigs, qualityMeasures, postprocessor,
					randomizer, paramSets, numberOfRandomizedDataSets,
					maxExecutionTimes);
			if (!recoverFromExceptions) {
				result.register();
				result = (RobustnessAnalysisRun) repo
						.getRegisteredObject(result, false);
			}
		} catch (Exception e) {
			if (!handleException(e))
				throw new RepositoryObjectParseException(this.getClassToParse(),
						this.name, this.versionString, e);
		}
	}

	protected List<ParameterSet> parseRandomizersParameterSets(
			String randomizerS) throws ConfigurationException {
		List<ParameterSet> paramSets = new ArrayList<ParameterSet>();
		try {
			int c = 1;
			while (getProps().getSections().contains(randomizerS + "_" + c)) {
				c = parseRandomizerParameterSet(randomizerS, paramSets, c);
			}
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return paramSets;
	}

	protected int parseRandomizerParameterSet(String randomizerS,
			List<ParameterSet> paramSets, int c) throws ConfigurationException {
		ParameterSet paramSet = new ParameterSet();
		try {
			Iterator<String> parameters = getProps()
					.getKeys(randomizerS + "_" + c);
			while (parameters.hasNext()) {
				String param = parameters.next();
				String value = getProps().getString(param);

				paramSet.put(param.replace(randomizerS + "_" + c + ".", ""),
						value);
			}
			c++;
			paramSets.add(paramSet);
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return c;
	}

	protected int parseNumberRandomizedDataSets()
			throws ConfigurationException {
		try {
			int numberOfRandomizedDataSets = getProps()
					.getInt("numberOfRandomizedDataSets");
			return numberOfRandomizedDataSets;
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return 0;
	}

	protected IDataRandomizer parseRandomizer(String randomizerS)
			throws Exception {
		IDataRandomizer randomizer = null;
		try {
			if (this.repo instanceof RunResultRepository)
				randomizer = DataRandomizer
						.parseFromString(this.repo.getParent(), randomizerS);
			else
				randomizer = DataRandomizer.parseFromString(this.repo,
						randomizerS);
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return randomizer;
	}

	protected String parseRandomizerName() throws ConfigurationException {
		try {
			String randomizerS = getProps().getString("randomizer");
			return randomizerS;
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return null;
	}

	protected String[] parseUniqueRunIds() throws Exception {
		try {
			String[] list = getProps().getStringArray("uniqueRunIdentifiers");
			if (list.length == 0)
				throw new RunException(
						"At least one run result identifier must be specified");
			return list;
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return new String[0];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.parse.ExecutionRunParser#
	 * parseDataConfigurations()
	 */
	@Override
	protected void parseDataConfigurations()
			throws RepositoryObjectParseException {

		try {

			if (this.repo instanceof RunResultRepository) {
				this.originalDataConfigs = new ArrayList<IDataConfig>();
				// get the original data configs
				String[] list = getProps().getStringArray("dataConfig");
				if (list.length == 0)
					throw new RunException(
							"At least one data config must be specified");
				// 10.07.2014: remove duplicates.
				list = new ArrayList<String>(
						new HashSet<String>(Arrays.asList(list)))
								.toArray(new String[0]);
				for (String dataConfig : list) {
					this.originalDataConfigs.add((DataConfig) repo.getParent()
							.getStaticObjectWithNameAndVersion(
									IDataConfig.class, dataConfig));
				}
				this.dataConfigs = new ArrayList<IDataConfig>(
						this.originalDataConfigs);
			} else {
				super.parseDataConfigurations();
				this.originalDataConfigs = new ArrayList<IDataConfig>(
						this.dataConfigs);
			}
		} catch (Exception e) {
			throw new RepositoryObjectParseException(this.getClassToParse(),
					this.name, this.versionString, e);
		}
	}
}