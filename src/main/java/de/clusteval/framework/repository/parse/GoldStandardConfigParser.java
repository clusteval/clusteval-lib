/**
 * 
 */
package de.clusteval.framework.repository.parse;

import java.io.File;
import java.util.NoSuchElementException;

import org.apache.commons.configuration.ConfigurationException;

import de.clusteval.data.goldstandard.GoldStandardConfig;
import de.clusteval.data.goldstandard.GoldStandardConfigurationException;
import de.clusteval.data.goldstandard.GoldStandardNotFoundException;
import de.clusteval.data.goldstandard.IGoldStandard;
import de.clusteval.data.goldstandard.IGoldStandardConfig;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;

/**
 * @author Christian Wiwie
 *
 */
public class GoldStandardConfigParser
		extends
			RepositoryObjectParser<GoldStandardConfig> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.parse.Parser#getFileEnding()
	 */
	@Override
	protected String getFileEnding() {
		return ".gsconfig";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.parse.Parser#getClassToParse()
	 */
	@Override
	protected Class<IGoldStandardConfig> getClassToParse() {
		return IGoldStandardConfig.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.RepositoryObjectParser#parseFromFile
	 * (java.io.File)
	 */
	@Override
	public void parseFromFile(File absPath)
			throws RepositoryObjectParseException {
		super.parseFromFile(absPath);

		log.debug("Parsing goldstandard config \"" + absPath + "\"");

		try {

			ensureVersionInFileName(absPath);

			getProps().setThrowExceptionOnMissing(true);

			String gsName = parseGoldStandardName();
			String gsFile = parseGoldStandardFile();

			IGoldStandard gs = parseGoldStandard(gsName, gsFile);

			result = new GoldStandardConfig(repo, changeDate, absPath, gs);
			if (!recoverFromExceptions) {
				result.register();
				result = (GoldStandardConfig) repo.getRegisteredObject(result);
			}
			log.debug("Goldstandard config parsed");
		} catch (ObjectNotRegisteredException e) {
			if (!handleException(e))
				throw new RepositoryObjectParseException(this.getClassToParse(),
						this.name, this.versionString,
						new GoldStandardNotFoundException(e.getObjectName()));
		} catch (NoSuchElementException e) {
			if (!handleException(e))
				throw new RepositoryObjectParseException(this.getClassToParse(),
						this.name, this.versionString,
						new GoldStandardConfigurationException(e));
		} catch (Exception e) {
			if (!handleException(e))
				throw new RepositoryObjectParseException(this.getClassToParse(),
						this.name, this.versionString, e);
		}
	}

	protected IGoldStandard parseGoldStandard(String gsName, String gsFile)
			throws ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException {
		try {
			IGoldStandard gs = (IGoldStandard) repo
					.getStaticObjectWithNameAndVersion(IGoldStandard.class,
							gsName + "/" + gsFile);
			return gs;
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return null;
	}

	protected String parseGoldStandardFile() throws ConfigurationException {
		try {
			String gsFile = getProps().getString("goldstandardFile");
			return gsFile;
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return null;
	}

	protected String parseGoldStandardName() throws ConfigurationException {
		try {
			String gsName = getProps().getString("goldstandardName");
			return gsName;
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return null;
	}
}