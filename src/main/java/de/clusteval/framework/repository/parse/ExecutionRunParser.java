/**
 * 
 */
package de.clusteval.framework.repository.parse;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.configuration.ConfigurationException;

import de.clusteval.cluster.quality.ClusteringQualityMeasure;
import de.clusteval.cluster.quality.ClusteringQualityMeasureParameters;
import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.cluster.quality.UnknownClusteringQualityMeasureException;
import de.clusteval.context.IncompatibleContextException;
import de.clusteval.context.UnknownContextException;
import de.clusteval.data.DataConfig;
import de.clusteval.data.DataConfigNotFoundException;
import de.clusteval.data.IDataConfig;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.program.NoOptimizableProgramParameterException;
import de.clusteval.program.ProgramConfig;
import de.clusteval.program.ProgramConfigNotFoundException;
import de.clusteval.program.UnknownProgramParameterException;
import de.clusteval.run.ExecutionRun;
import de.clusteval.run.IExecutionRun;
import de.clusteval.run.RunException;
import de.clusteval.run.runresult.postprocessing.IRunResultPostprocessor;
import de.clusteval.run.runresult.postprocessing.RunResultPostprocessor;
import de.clusteval.run.runresult.postprocessing.RunResultPostprocessorParameters;
import de.clusteval.run.runresult.postprocessing.UnknownRunResultPostprocessorException;
import de.clusteval.utils.DynamicComponentInitializationException;

/**
 * @author Christian Wiwie
 *
 * @param <T>
 */
public abstract class ExecutionRunParser<T extends IExecutionRun>
		extends
			RunParser<T> {

	protected List<IProgramConfig> programConfigs;
	protected List<IDataConfig> dataConfigs;
	protected List<IClusteringQualityMeasure> qualityMeasures;
	protected Map<IProgramConfig, Map<IProgramParameter<?>, String>> fixedParameterValues;
	protected List<IRunResultPostprocessor> postprocessor;
	protected Map<String, Integer> maxExecutionTimes;

	@Override
	public void parseFromFile(final File absPath)
			throws RepositoryObjectParseException {
		super.parseFromFile(absPath);

		try {

			/*
			 * A run consists of a set of programconfigs and a set of
			 * dataconfigs, that are pairwise combined.
			 */
			programConfigs = new LinkedList<IProgramConfig>();
			dataConfigs = new LinkedList<IDataConfig>();
			/*
			 * The quality measures that should be calculated for every pair of
			 * programconfig+dataconfig.
			 */
			qualityMeasures = new LinkedList<IClusteringQualityMeasure>();
			/*
			 * A list with parameter values that are set in the run config. They
			 * will overwrite the default values of the program config.
			 */
			fixedParameterValues = new HashMap<IProgramConfig, Map<IProgramParameter<?>, String>>();

			maxExecutionTimes = new HashMap<String, Integer>();

			parseProgramConfigurations();

			parseQualityMeasures();

			parseDataConfigurations();

			parsePostprocessors();

			ExecutionRun.checkCompatibilityQualityMeasuresDataConfigs(
					dataConfigs, qualityMeasures);
		} catch (RepositoryObjectParseException e) {
			if (!handleException(e))
				throw e;
		} catch (ParseException e) {
			if (!handleException(e))
				throw new RepositoryObjectParseException(this.getClassToParse(),
						this.name, this.versionString, e);
		} catch (Exception e) {
			if (!handleException(e))
				throw new RepositoryObjectParseException(this.getClassToParse(),
						this.name, this.versionString, e);
		}
	}

	protected void parseProgramConfigurations()
			throws RepositoryObjectParseException {
		try {
			String[] list = parseProgramConfigurationNames();
			for (String programConfig : list) {
				IProgramConfig newProgramConfig = parseProgramConfiguration(
						programConfig);
				programConfigs.add(newProgramConfig);

				/*
				 * parse the overriding parameter-values for this program config
				 */
				parseProgramConfigParams(newProgramConfig);
			}
		} catch (ObjectNotRegisteredException e) {
			if (!handleException(e))
				throw new RepositoryObjectParseException(this.getClassToParse(),
						this.name, this.versionString,
						new ProgramConfigNotFoundException(e.getObjectName()));
		} catch (Exception e) {
			if (!handleException(e))
				throw new RepositoryObjectParseException(this.getClassToParse(),
						this.name, this.versionString, e);
		}
	}

	protected IProgramConfig parseProgramConfiguration(String programConfig)
			throws ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException, UnknownContextException,
			IncompatibleContextException,
			DynamicComponentInitializationException {
		try {
			IProgramConfig newProgramConfig = (IProgramConfig) this.repo
					.getStaticObjectWithNameAndVersion(IProgramConfig.class,
							programConfig);

			if (!newProgramConfig.getProgram().getContext().equals(context))
				throw new IncompatibleContextException(
						"Incompatible run context (" + context
								+ ") and program context ("
								+ newProgramConfig.getProgram().getContext()
								+ ")");

			newProgramConfig = (ProgramConfig) repo
					.getRegisteredObject(newProgramConfig);
			return newProgramConfig;
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return null;
	}

	protected String[] parseProgramConfigurationNames()
			throws ConfigurationException, RunException {
		// 10.07.2014: remove duplicates.
		Set<String> list = new HashSet<>();
		try {
			list = new HashSet<>(
					Arrays.asList(getProps().getStringArray("programConfig")));
			if (list.isEmpty())
				throw new RunException(
						"At least one program config must be specified");
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return new ArrayList<String>(list).toArray(new String[0]);
	}

	protected boolean isParamConfigurationEntry(final String name) {
		return name != null;
	}

	protected boolean checkParamValueToMap(final String param) {
		return true;
	}

	protected void parseQualityMeasures()
			throws RepositoryObjectParseException {
		try {
			String[] measures = getProps().getStringArray("qualityMeasures");
			if (measures.length == 0)
				throw new RunException(
						"At least one quality measure must be specified");
			/**
			 * We catch the exceptions such that all quality measures are tried
			 * to be loaded once so that they are ALL registered as missing in
			 * the repository.
			 */
			List<UnknownClusteringQualityMeasureException> thrownExceptions = new ArrayList<UnknownClusteringQualityMeasureException>();
			for (String qualityMeasure : measures) {
				try {
					ClusteringQualityMeasureParameters p = parseQualityMeasureParameters(
							qualityMeasure);
					IClusteringQualityMeasure measure = parseQualityMeasure(
							qualityMeasure, p);

					qualityMeasures.add(measure);

				} catch (UnknownClusteringQualityMeasureException e) {
					if (!handleException(e))
						thrownExceptions.add(e);
				}
			}
			if (thrownExceptions.size() > 0) {
				// just throw the first exception
				throw thrownExceptions.get(0);
			}
		} catch (Exception e) {
			if (!handleException(e))
				throw new RepositoryObjectParseException(this.getClassToParse(),
						this.name, this.versionString, e);
		}
	}

	protected IClusteringQualityMeasure parseQualityMeasure(
			String qualityMeasure, ClusteringQualityMeasureParameters p)
			throws UnknownClusteringQualityMeasureException,
			DynamicComponentInitializationException {
		try {
			IClusteringQualityMeasure measure = ClusteringQualityMeasure
					.parseFromString(repo, qualityMeasure, p);
			return measure;
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return null;
	}

	protected ClusteringQualityMeasureParameters parseQualityMeasureParameters(
			String qualityMeasure) throws ConfigurationException {
		// parse parameters for this quality measure
		ClusteringQualityMeasureParameters p = new ClusteringQualityMeasureParameters();
		try {
			if (getProps().getSections().contains(qualityMeasure)) {
				Iterator<String> parameters = getProps()
						.getKeys(qualityMeasure);
				while (parameters.hasNext()) {
					String param = parameters.next();
					String value = getProps().getString(param);

					p.put(param, value);
				}
			}
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return p;
	}

	protected void parseDataConfigurations()
			throws RepositoryObjectParseException {
		try {
			String[] list = parseDataConfigurationNames();
			for (String dataConfig : list) {
				dataConfigs.add(parseDataConfiguration(dataConfig));
			}
		} catch (ObjectNotRegisteredException e) {
			if (!handleException(e))
				throw new RepositoryObjectParseException(this.getClassToParse(),
						this.name, this.versionString,
						new DataConfigNotFoundException(e.getObjectName()));
		} catch (Exception e) {
			if (!handleException(e))
				throw new RepositoryObjectParseException(this.getClassToParse(),
						this.name, this.versionString, e);
		}
	}

	protected DataConfig parseDataConfiguration(String dataConfig)
			throws ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException {
		try {
			return (DataConfig) repo.getStaticObjectWithNameAndVersion(
					IDataConfig.class, dataConfig);
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return null;
	}

	protected String[] parseDataConfigurationNames()
			throws ConfigurationException, RunException {
		// 10.07.2014: remove duplicates.
		Set<String> list = new HashSet<>();
		try {
			list = new HashSet<>(
					Arrays.asList(getProps().getStringArray("dataConfig")));
			if (list.isEmpty())
				throw new RunException(
						"At least one data config must be specified");
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return new ArrayList<>(list).toArray(new String[0]);
	}

	protected void parseProgramConfigParams(final IProgramConfig programConfig)
			throws ConfigurationException,
			NoOptimizableProgramParameterException,
			UnknownProgramParameterException, RunException {
		Map<IProgramParameter<?>, String> paramMap = new HashMap<IProgramParameter<?>, String>();

		try {
			if (getProps().getSections().contains(programConfig.getName())) {
				/*
				 * General parameters, not only for optimization.
				 */
				Iterator<String> itParams = getProps()
						.getSection(programConfig.getName()).getKeys();
				while (itParams.hasNext()) {
					String param = itParams.next();
					if (param.equals("maxExecutionTimeMinutes")) {
						this.maxExecutionTimes.put(programConfig.getName(),
								Integer.parseInt(getProps()
										.getSection(programConfig.getName())
										.getString(param)));
					} else if (isParamConfigurationEntry(param))
						try {
							IProgramParameter<?> p = programConfig
									.getParamWithId(param);

							if (checkParamValueToMap(param))
								paramMap.put(p,
										getProps()
												.getSection(
														programConfig.getName())
												.getString(param));
						} catch (UnknownProgramParameterException e) {
							log.error("The run " + absPath.getName()
									+ " contained invalid parameter values: "
									+ programConfig.getProgram()
									+ " does not have a parameter " + param);
						}
				}
			}
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}

		fixedParameterValues.put(programConfig, paramMap);
	}

	protected void parsePostprocessors() throws RepositoryObjectParseException {
		try {
			postprocessor = new ArrayList<IRunResultPostprocessor>();

			if (!getProps().containsKey("postprocessor"))
				return;

			String[] list = parsePostprocessorNames();
			for (String postprocessor : list) {

				RunResultPostprocessorParameters params = parsePostprocessorParams(
						postprocessor);

				IRunResultPostprocessor newPostprocessor = parsePostprocessor(
						postprocessor, params);
				this.postprocessor.add(newPostprocessor);
			}
		} catch (Exception e) {
			if (!handleException(e))
				throw new RepositoryObjectParseException(this.getClassToParse(),
						this.name, this.versionString, e);
		}
	}

	protected String[] parsePostprocessorNames() throws ConfigurationException {
		try {
			String[] list = getProps().getStringArray("postprocessor");
			// 10.07.2014: remove duplicates.
			list = new ArrayList<String>(
					new HashSet<String>(Arrays.asList(list)))
							.toArray(new String[0]);
			return list;
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return new String[0];
	}

	protected RunResultPostprocessorParameters parsePostprocessorParams(
			String postprocessor) throws ConfigurationException {
		// parse parameters
		RunResultPostprocessorParameters params = new RunResultPostprocessorParameters();

		try {
			if (getProps().getSections().contains(postprocessor)) {
				Iterator<String> it = getProps().getSection(postprocessor)
						.getKeys();
				while (it.hasNext()) {
					String param = it.next();

					params.put(param, getProps().getSection(postprocessor)
							.getString(param));
				}
			}
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return params;
	}

	protected IRunResultPostprocessor parsePostprocessor(String postprocessor,
			RunResultPostprocessorParameters params)
			throws UnknownRunResultPostprocessorException,
			DynamicComponentInitializationException {
		try {
			IRunResultPostprocessor newPostprocessor = RunResultPostprocessor
					.parseFromString(this.repo, postprocessor, params);
			return newPostprocessor;
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return null;
	}
}