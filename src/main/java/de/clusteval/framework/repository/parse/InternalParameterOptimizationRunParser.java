/**
 * 
 */
package de.clusteval.framework.repository.parse;

import java.io.File;

import de.clusteval.run.IInternalParameterOptimizationRun;
import de.clusteval.run.InternalParameterOptimizationRun;

/**
 * @author Christian Wiwie
 *
 */
public class InternalParameterOptimizationRunParser
		extends
			ExecutionRunParser<InternalParameterOptimizationRun> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.parse.Parser#getClassToParse()
	 */
	@Override
	protected Class<IInternalParameterOptimizationRun> getClassToParse() {
		return IInternalParameterOptimizationRun.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.ExecutionRunParser#parseFromFile(java
	 * .io.File)
	 */
	@Override
	public void parseFromFile(File absPath)
			throws RepositoryObjectParseException {
		super.parseFromFile(absPath);

		try {

			result = new InternalParameterOptimizationRun(repo, context,
					changeDate, absPath, programConfigs, dataConfigs,
					qualityMeasures, fixedParameterValues, postprocessor,
					maxExecutionTimes);
			if (!recoverFromExceptions) {
				result.register();
				result = (InternalParameterOptimizationRun) repo
						.getRegisteredObject(result, false);
			}
		} catch (Exception e) {
			if (!handleException(e))
				throw new RepositoryObjectParseException(this.getClassToParse(),
						this.name, this.versionString, e);
		}

	}
}