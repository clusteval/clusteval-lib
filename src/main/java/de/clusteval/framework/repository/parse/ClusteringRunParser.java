/**
 * 
 */
package de.clusteval.framework.repository.parse;

import java.io.File;

import de.clusteval.run.ClusteringRun;
import de.clusteval.run.IClusteringRun;

/**
 * @author Christian Wiwie
 *
 */
public class ClusteringRunParser extends ExecutionRunParser<ClusteringRun> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.ExecutionRunParser#parseFromFile(java
	 * .io.File)
	 */
	@Override
	public void parseFromFile(File absPath)
			throws RepositoryObjectParseException {
		super.parseFromFile(absPath);

		try {

			result = new ClusteringRun(repo, context, changeDate, absPath,
					programConfigs, dataConfigs, qualityMeasures,
					fixedParameterValues, postprocessor, maxExecutionTimes);
			if (!recoverFromExceptions) {
				result.register();
				result = (ClusteringRun) repo.getRegisteredObject(result,
						false);
			}
		} catch (Exception e) {
			if (!handleException(e))
				throw new RepositoryObjectParseException(this.getClassToParse(),
						this.name, this.versionString, e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.parse.Parser#getClassToParse()
	 */
	@Override
	protected Class<IClusteringRun> getClassToParse() {
		return IClusteringRun.class;
	}
}