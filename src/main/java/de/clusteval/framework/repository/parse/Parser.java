/**
 * 
 */
package de.clusteval.framework.repository.parse;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.IDataSet;
import de.clusteval.data.dataset.IDataSetConfig;
import de.clusteval.data.dataset.NoDataSetException;
import de.clusteval.data.goldstandard.IGoldStandard;
import de.clusteval.data.goldstandard.IGoldStandardConfig;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IStandaloneProgram;
import de.clusteval.run.IClusteringRun;
import de.clusteval.run.IDataAnalysisRun;
import de.clusteval.run.IInternalParameterOptimizationRun;
import de.clusteval.run.IParameterOptimizationRun;
import de.clusteval.run.IRobustnessAnalysisRun;
import de.clusteval.run.IRun;
import de.clusteval.run.IRunAnalysisRun;
import de.clusteval.run.IRunDataAnalysisRun;
import de.clusteval.run.IToolRuntimeEvaluationRun;
import de.clusteval.run.RUN_TYPE;

/**
 * @author Christian Wiwie
 * 
 */
public abstract class Parser<P extends IRepositoryObject> {

	protected Logger log = LoggerFactory.getLogger(this.getClass());

	protected boolean recoverFromExceptions;

	/**
	 * @param recoverFromExceptions
	 *            the recoverFromExceptions to set
	 */
	public void setRecoverFromExceptions(boolean recoverFromExceptions) {
		this.recoverFromExceptions = recoverFromExceptions;
	}

	/**
	 * @return the recoverFromExceptions
	 */
	public boolean isRecoverFromExceptions() {
		return recoverFromExceptions;
	}

	/**
	 * 
	 * @param e
	 * @return A boolean indicating whether the exception has been handled.
	 */
	public <T extends Throwable> boolean handleException(final T e) {
		if (recoverFromExceptions)
			this.log.debug("Recovering from exception: " + e.getMessage());
		return recoverFromExceptions;
	}

	protected static <T extends IRepositoryObject> Parser<T> getParserForClass(
			final Class<T> c) {
		return getParserForClass(c, false);
	}

	@SuppressWarnings("unchecked")
	protected static <T extends IRepositoryObject> Parser<T> getParserForClass(
			final Class<T> c, final boolean recoverFromExceptions) {
		Parser<T> p = null;
		if (c.equals(IClusteringRun.class))
			p = (Parser<T>) new ClusteringRunParser();
		else if (c.equals(IParameterOptimizationRun.class))
			p = (Parser<T>) new ParameterOptimizationRunParser();
		else if (c.equals(IInternalParameterOptimizationRun.class))
			p = (Parser<T>) new InternalParameterOptimizationRunParser();
		else if (c.equals(IDataAnalysisRun.class))
			p = (Parser<T>) new DataAnalysisRunParser();
		else if (c.equals(IRunAnalysisRun.class))
			p = (Parser<T>) new RunAnalysisRunParser();
		else if (c.equals(IRunDataAnalysisRun.class))
			p = (Parser<T>) new RunDataAnalysisRunParser();
		else if (c.equals(IRobustnessAnalysisRun.class))
			p = (Parser<T>) new RobustnessAnalysisRunParser();
		else if (c.equals(IToolRuntimeEvaluationRun.class))
			p = (Parser<T>) new ToolRuntimeEvaluationRunParser();
		else if (c.equals(IDataSetConfig.class))
			p = (Parser<T>) new DataSetConfigParser();
		else if (c.equals(IDataSet.class))
			p = (Parser<T>) new DataSetParser();
		else if (c.equals(IRun.class))
			p = new RunParser();
		else if (c.equals(IGoldStandard.class))
			p = (Parser<T>) new GoldStandardParser();
		else if (c.equals(IGoldStandardConfig.class))
			p = (Parser<T>) new GoldStandardConfigParser();
		else if (c.equals(IProgramConfig.class))
			p = (Parser<T>) new ProgramConfigParser();
		else if (c.equals(IDataConfig.class))
			p = (Parser<T>) new DataConfigParser();
		else if (c.equals(IStandaloneProgram.class))
			p = (Parser<T>) new StandaloneProgramParser();
		else
			return null;
		p.setRecoverFromExceptions(recoverFromExceptions);
		return p;
	}

	public static <T extends IRepositoryObject> T parseFromFile(
			final Class<T> c, final File absPath)
			throws RepositoryObjectParseException {
		return parseFromFile(c, absPath, false);
	}

	/**
	 * @param c
	 * @param absPath
	 * @return
	 * @throws RepositoryObjectParseException
	 */
	public static <T extends IRepositoryObject> T parseFromFile(
			final Class<T> c, final File absPath,
			final boolean recoverFromExceptions)
			throws RepositoryObjectParseException {
		Parser<T> parser = getParserForClass(c, recoverFromExceptions);
		parser.parseFromFile(absPath);
		return parser.getResult();
	}

	public static IRun parseRunFromFile(final File file)
			throws RepositoryObjectParseException {
		return parseRunFromFile(file, false);
	}

	public static IRun parseRunFromFile(final File file,
			final boolean recoverFromExceptions)
			throws RepositoryObjectParseException {
		try {
			String runMode = Parser.getModeOfRun(file, recoverFromExceptions);
			if (runMode.equals(RUN_TYPE.CLUSTERING.toString())) {
				return Parser.parseFromFile(IClusteringRun.class, file,
						recoverFromExceptions);
			} else if (runMode
					.equals(RUN_TYPE.PARAMETER_OPTIMIZATION.toString())) {
				return Parser.parseFromFile(IParameterOptimizationRun.class,
						file, recoverFromExceptions);
			} else if (runMode.equals(
					RUN_TYPE.INTERNAL_PARAMETER_OPTIMIZATION.toString())) {
				return Parser.parseFromFile(
						IInternalParameterOptimizationRun.class, file,
						recoverFromExceptions);
			} else if (runMode.equals(RUN_TYPE.DATA_ANALYSIS.toString())) {
				return Parser.parseFromFile(IDataAnalysisRun.class, file,
						recoverFromExceptions);
			} else if (runMode.equals(RUN_TYPE.RUN_ANALYSIS.toString())) {
				return Parser.parseFromFile(IRunAnalysisRun.class, file,
						recoverFromExceptions);
			} else if (runMode.equals(RUN_TYPE.RUN_DATA_ANALYSIS.toString())) {
				return Parser.parseFromFile(IRunDataAnalysisRun.class, file,
						recoverFromExceptions);
			} else if (runMode
					.equals(RUN_TYPE.ROBUSTNESS_ANALYSIS.toString())) {
				return Parser.parseFromFile(IRobustnessAnalysisRun.class, file,
						recoverFromExceptions);
			} else if (runMode
					.equals(RUN_TYPE.TOOL_RUNTIME_EVALUATION.toString())) {
				return Parser.parseFromFile(IToolRuntimeEvaluationRun.class,
						file, recoverFromExceptions);
			}
		} catch (RepositoryObjectParseException e) {
			throw e;
		}
		return null;
	}

	protected static String getModeOfRun(final File absPath,
			final boolean recoverFromExceptions)
			throws RepositoryObjectParseException {
		RunParser<? extends IRun> p = (RunParser<IRun>) getParserForClass(
				IRun.class, recoverFromExceptions);
		p.parseFromFile(absPath);
		return p.mode;
	}

	/**
	 * The file ending of the file to be parsed including file ending separator,
	 * such as the period
	 */
	protected abstract String getFileEnding();

	protected void ensureVersionInFileName(File absPath)
			throws NoDataSetException {
		try {
			String name = absPath.getName().replaceAll(getFileEnding() + "$",
					"");
			int colonInd = name.lastIndexOf(".");
			if (colonInd < 0 || !name.substring(colonInd)
					.matches("\\.v\\d+(-SNAPSHOT)?"))
				throw new NoDataSetException(
						"The file name is missing a version.");
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
	}

	protected P result;

	public abstract void parseFromFile(final File absPath)
			throws RepositoryObjectParseException;

	public P getResult() {
		return this.result;
	}

	protected abstract Class<? extends IRepositoryObject> getClassToParse();
}