/**
 * 
 */
package de.clusteval.framework.repository.parse;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.configuration.ConfigurationException;

import de.clusteval.run.IRunDataAnalysisRun;
import de.clusteval.run.RunDataAnalysisRun;
import de.clusteval.run.statistics.IRunDataStatistic;
import de.clusteval.run.statistics.RunDataStatistic;
import de.clusteval.run.statistics.UnknownRunDataStatisticException;
import de.clusteval.utils.DynamicComponentInitializationException;

/**
 * @author Christian Wiwie
 *
 */
public class RunDataAnalysisRunParser extends AnalysisRunParser<RunDataAnalysisRun> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.parse.Parser#getClassToParse()
	 */
	@Override
	protected Class<IRunDataAnalysisRun> getClassToParse() {
		return IRunDataAnalysisRun.class;
	}

	@Override
	public void parseFromFile(File absPath)
			throws RepositoryObjectParseException {
		super.parseFromFile(absPath);

		try {

			List<String> uniqueRunAnalysisRunIdentifiers = new LinkedList<String>();
			List<String> uniqueDataAnalysisRunIdentifiers = new LinkedList<String>();

			parseUniqueRunIds(uniqueRunAnalysisRunIdentifiers);
			parseUniqueDataIds(uniqueDataAnalysisRunIdentifiers);

			List<IRunDataStatistic> runDataStatistics = parseRunDataStatistics();

			result = new RunDataAnalysisRun(repo, context, changeDate, absPath,
					uniqueRunAnalysisRunIdentifiers,
					uniqueDataAnalysisRunIdentifiers, runDataStatistics);
			if (!recoverFromExceptions) {
				result.register();
				result = (RunDataAnalysisRun) repo.getRegisteredObject(result,
						false);
			}
		} catch (Exception e) {
			if (!handleException(e))
				throw new RepositoryObjectParseException(this.getClassToParse(),
						this.name, this.versionString, e);
		}

	}

	protected List<IRunDataStatistic> parseRunDataStatistics()
			throws ConfigurationException, UnknownRunDataStatisticException,
			DynamicComponentInitializationException {
		/**
		 * We catch the exceptions such that all statistics are tried to be
		 * loaded once so that they are ALL registered as missing in the
		 * repository.
		 */
		List<IRunDataStatistic> runDataStatistics = new LinkedList<IRunDataStatistic>();
		try {
			List<UnknownRunDataStatisticException> thrownExceptions = new ArrayList<UnknownRunDataStatisticException>();
			for (String runStatistic : getProps()
					.getStringArray("runDataStatistics")) {
				try {
					runDataStatistics.add(parseRunDataStatistic(runStatistic));
				} catch (UnknownRunDataStatisticException e) {
					if (!handleException(e))
						thrownExceptions.add(e);
				}
			}
			if (thrownExceptions.size() > 0) {
				// just throw the first exception
				throw thrownExceptions.get(0);
			}
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return runDataStatistics;
	}

	protected IRunDataStatistic parseRunDataStatistic(String runStatistic)
			throws UnknownRunDataStatisticException,
			DynamicComponentInitializationException {
		try {
			return RunDataStatistic.parseFromString(repo, runStatistic);
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return null;
	}

	protected void parseUniqueDataIds(
			List<String> uniqueDataAnalysisRunIdentifiers)
			throws ConfigurationException {
		try {
			uniqueDataAnalysisRunIdentifiers.addAll(Arrays.asList(
					getProps().getStringArray("uniqueDataIdentifiers")));
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
	}

	protected void parseUniqueRunIds(
			List<String> uniqueRunAnalysisRunIdentifiers)
			throws ConfigurationException {
		try {
			uniqueRunAnalysisRunIdentifiers.addAll(Arrays
					.asList(getProps().getStringArray("uniqueRunIdentifiers")));
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
	};
}