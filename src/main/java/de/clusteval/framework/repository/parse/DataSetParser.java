/**
 * 
 */
package de.clusteval.framework.repository.parse;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.LoggerFactory;

import de.clusteval.data.dataset.AbsoluteDataSet;
import de.clusteval.data.dataset.DataSet;
import de.clusteval.data.dataset.DataSetAttributeParser;
import de.clusteval.data.dataset.DataSetConfigurationException;
import de.clusteval.data.dataset.IDataSet;
import de.clusteval.data.dataset.NoDataSetException;
import de.clusteval.data.dataset.RelativeDataSet;
import de.clusteval.data.dataset.format.AbsoluteDataSetFormat;
import de.clusteval.data.dataset.format.DataSetFormat;
import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.data.dataset.format.RelativeDataSetFormat;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.dataset.type.DataSetType;
import de.clusteval.data.dataset.type.IDataSetType;
import de.clusteval.data.dataset.type.UnknownDataSetTypeException;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.RunResultRepository;
import de.clusteval.utils.DynamicComponentInitializationException;

/**
 * @author Christian Wiwie
 *
 */
public class DataSetParser extends RepositoryObjectParser<DataSet> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.parse.Parser#getFileEnding()
	 */
	@Override
	protected String getFileEnding() {
		return "";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.parse.Parser#getClassToParse()
	 */
	@Override
	protected Class<IDataSet> getClassToParse() {
		return IDataSet.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.RepositoryObjectParser#parseFromFile
	 * (java.io.File)
	 */
	@Override
	public void parseFromFile(File absPath)
			throws RepositoryObjectParseException {
		super.parseFromFile(absPath);
		try {
			Map<String, String> attributeValues = extractDataSetAttributes(
					absPath);

			ensureVersionInFileName(absPath);

			String alias = parseAlias(absPath, attributeValues);
			// check whether the alias is already taken by another dataset
			// ->
			// throw exception
			validateAlias(absPath, alias);

			IDataSetFormat dsFormat = parseDataSetFormat(absPath,
					attributeValues);

			IDataSetType dsType = parseDataSetType(absPath, attributeValues);

			DataSet.WEBSITE_VISIBILITY websiteVisibility = parseVisibility(
					attributeValues);

			final long changeDate = absPath.lastModified();

			LoggerFactory.getLogger(DataSet.class)
					.debug("Parsing dataset \"" + absPath + "\"");

			/*
			 * Either the format is absolute or relative
			 */
			if (RelativeDataSetFormat.class
					.isAssignableFrom(dsFormat.getClass()))
				result = new RelativeDataSet(repo, changeDate, absPath, alias,
						(RelativeDataSetFormat) dsFormat, dsType,
						websiteVisibility);
			else
				result = new AbsoluteDataSet(repo, changeDate, absPath, alias,
						(AbsoluteDataSetFormat) dsFormat, dsType,
						websiteVisibility);
			if (!recoverFromExceptions) {
				result.register();
				result = (DataSet) repo.getRegisteredObject(result);
			}
			LoggerFactory.getLogger(DataSet.class).debug("Dataset parsed");
		} catch (Exception e) {
			if (!handleException(e))
				throw new RepositoryObjectParseException(this.getClassToParse(),
						this.name, this.versionString, e);
		}
	}

	protected DataSet.WEBSITE_VISIBILITY parseVisibility(
			Map<String, String> attributeValues) {
		DataSet.WEBSITE_VISIBILITY websiteVisibility = IDataSet.WEBSITE_VISIBILITY.HIDE;
		String vis = attributeValues.containsKey("websiteVisibility")
				? attributeValues.get("websiteVisibility")
				: "hide";
		if (vis.equals("hide"))
			websiteVisibility = IDataSet.WEBSITE_VISIBILITY.HIDE;
		else if (vis.equals("show_always"))
			websiteVisibility = IDataSet.WEBSITE_VISIBILITY.SHOW_ALWAYS;
		else if (vis.equals("show_optional"))
			websiteVisibility = IDataSet.WEBSITE_VISIBILITY.SHOW_OPTIONAL;
		return websiteVisibility;
	}

	protected IDataSetType parseDataSetType(File absPath,
			Map<String, String> attributeValues)
			throws UnknownDataSetTypeException, DataSetConfigurationException,
			DynamicComponentInitializationException {
		IDataSetType dsType = null;
		try {
			if (attributeValues.containsKey("dataSetType")) {
				dsType = DataSetType.parseFromString(repo,
						attributeValues.get("dataSetType"));
			} else {
				throw new DataSetConfigurationException(
						"No type specified for dataset "
								+ absPath.getAbsolutePath());
			}
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return dsType;
	}

	protected IDataSetFormat parseDataSetFormat(File absPath,
			Map<String, String> attributeValues)
			throws UnknownDataSetFormatException, DataSetConfigurationException,
			DynamicComponentInitializationException {
		IDataSetFormat dsFormat = null;
		try {
			if (attributeValues.containsKey("dataSetFormat")) {
				dsFormat = DataSetFormat.parseFromString(repo,
						attributeValues.get("dataSetFormat"));
			} else {
				throw new DataSetConfigurationException(
						"No format specified for dataset "
								+ absPath.getAbsolutePath());
			}
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return dsFormat;
	}

	protected void validateAlias(File absPath, String alias)
			throws DataSetConfigurationException {
		try {
			Collection<IDataSet> dataSets;
			if (repo instanceof RunResultRepository)
				dataSets = repo.getParent()
						.getCollectionStaticEntities(IDataSet.class);
			else
				dataSets = repo.getCollectionStaticEntities(IDataSet.class);

			for (IRepositoryObject o : dataSets) {
				DataSet ds = (DataSet) o;
				if (!(repo instanceof RunResultRepository)
						&& !(ds.getAbsolutePath()
								.replaceAll("(\\.v\\d+(-SNAPSHOT)??)$", "")
								.equals(absPath.getAbsolutePath().replaceAll(
										"(\\.v\\d+(-SNAPSHOT)??)$", "")))
						&& ds.getAlias().equals(alias))
					throw new DataSetConfigurationException(
							"The alias (" + alias + ") of the data set "
									+ absPath.getAbsolutePath()
									+ " is already taken by the data set "
									+ ds.getAbsolutePath());
			}
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
	}

	protected String parseAlias(File absPath,
			Map<String, String> attributeValues)
			throws DataSetConfigurationException {
		String alias = null;
		try {
			if (attributeValues.containsKey("alias"))
				alias = attributeValues.get("alias");
			else
				throw new DataSetConfigurationException(
						"No alias specified for data set "
								+ absPath.getAbsolutePath());
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return alias;
	}

	/**
	 * This method parses the header of a dataset file. A header is required for
	 * a dataset file to be recognized by the framework as a valid dataset file.
	 * If the file does not contain any header lines, it is ignored by the
	 * framework. A header line is of the form '// attribute = value'. The
	 * header should contain several lines:
	 * 
	 * <p>
	 * The type of the dataset, e.g. '// dataSetType =
	 * GeneExpressionDataSetType'
	 * <p>
	 * The format of the dataset, e.g. '// dataSetFormat =
	 * RowSimDataSetFormat:1'
	 * 
	 * @param absPath
	 * @return
	 * @throws IOException
	 */
	protected Map<String, String> extractDataSetAttributes(final File absPath)
			throws IOException, NoDataSetException {
		Map<String, String> attributeValues = new HashMap<>();
		try {
			DataSetAttributeParser attributeParser = new DataSetAttributeParser(
					absPath.getAbsolutePath());
			attributeParser.process();
			attributeValues.putAll(attributeParser.getAttributeValues());

			if (attributeValues.size() == 0)
				throw new NoDataSetException("The file " + absPath
						+ " does not contain a dataset header.");
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return attributeValues;
	}
}