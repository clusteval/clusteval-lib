/**
 * 
 */
package de.clusteval.framework.repository.parse;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.SubnodeConfiguration;

import de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod;
import de.clusteval.cluster.paramOptimization.ParameterOptimizationMethod;
import de.clusteval.cluster.paramOptimization.UnknownParameterOptimizationMethodException;
import de.clusteval.cluster.quality.ClusteringQualityMeasure;
import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.cluster.quality.UnknownClusteringQualityMeasureException;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.IDataSet;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RunResultRepository;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.program.NoOptimizableProgramParameterException;
import de.clusteval.program.UnknownProgramParameterException;
import de.clusteval.run.IParameterOptimizationRun;
import de.clusteval.run.ParameterOptimizationRun;
import de.clusteval.run.RunException;
import de.clusteval.utils.DynamicComponentInitializationException;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * @author Christian Wiwie
 *
 */
public class ParameterOptimizationRunParser
		extends
			ExecutionRunParser<ParameterOptimizationRun> {

	protected Map<IProgramConfig, List<IProgramParameter<?>>> optimizationParameters;
	protected List<IParameterOptimizationMethod> optimizationMethods;
	protected String[] optimizationParas;
	protected List<IProgramParameter<?>> optParaList;
	protected String paramOptMethod;
	protected List<String> paramOptMethods;
	protected Map<IProgramConfig, Map<IProgramParameter<?>, String>> parameterMinValueOverrides,
			parameterMaxValueOverrides;
	protected Map<IProgramConfig, Map<IProgramParameter<?>, String[]>> parameterOptionsOverrides;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.parse.Parser#getClassToParse()
	 */
	@Override
	protected Class<IParameterOptimizationRun> getClassToParse() {
		return IParameterOptimizationRun.class;
	}

	@Override
	public void parseFromFile(final File absPath)
			throws RepositoryObjectParseException {
		try {
			this.optimizationParameters = new HashMap<IProgramConfig, List<IProgramParameter<?>>>();
			/*
			 * The optimization methods, for every program one method.
			 */
			this.optimizationMethods = new ArrayList<IParameterOptimizationMethod>();

			this.parameterMinValueOverrides = new HashMap<IProgramConfig, Map<IProgramParameter<?>, String>>();
			this.parameterMaxValueOverrides = new HashMap<IProgramConfig, Map<IProgramParameter<?>, String>>();
			this.parameterOptionsOverrides = new HashMap<IProgramConfig, Map<IProgramParameter<?>, String[]>>();

			super.parseFromFile(absPath);

			IClusteringQualityMeasure optimizationCriterion = parseOptimizationCriterion();
			int paramOptIterations = parseOptimizationIterations();

			parseOptimizationMethods(paramOptIterations);

			result = new ParameterOptimizationRun(repo, context, changeDate,
					absPath, programConfigs, dataConfigs, qualityMeasures,
					fixedParameterValues, paramOptIterations,
					optimizationCriterion, optimizationParameters,
					this.parameterMinValueOverrides,
					this.parameterMaxValueOverrides,
					this.parameterOptionsOverrides, paramOptMethod,
					optimizationMethods, postprocessor, maxExecutionTimes);
			ParameterOptimizationRun registeredResult = null;
			if (!recoverFromExceptions) {
				result.register();
				registeredResult = (ParameterOptimizationRun) repo
						.getRegisteredObject(result, false);

				if (registeredResult != null) {
					result = registeredResult;
				}
			}

			// now we set the run reference of the methods
			setOptimizationMethodsRun();

			// if we have the run already registered, we take that run and do
			// not register the parameter optimization methods.
			if (!recoverFromExceptions) {
				if (registeredResult == null) {
					registerOptimizationMethods();
				}
			}

			ParameterOptimizationRun
					.checkCompatibilityParameterOptimizationMethod(
							optimizationMethods, programConfigs, dataConfigs);
		} catch (RepositoryObjectParseException e) {
			if (!handleException(e))
				throw e;
		} catch (Exception e) {
			if (!handleException(e))
				throw new RepositoryObjectParseException(this.getClassToParse(),
						this.name, this.versionString, e);
		}
	}

	protected void parseOptimizationMethods(int paramOptIterations)
			throws UnknownParameterOptimizationMethodException,
			DynamicComponentInitializationException {
		try {
			for (int i = 0; i < programConfigs.size(); i++) {
				for (int j = 0; j < dataConfigs.size(); j++) {
					optimizationMethods.add(
							parseOptimizationMethod(paramOptIterations, i, j));
				}
			}
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
	}

	protected void setOptimizationMethodsRun() throws Exception {
		try {
			for (int i = 0; i < optimizationMethods.size(); i++) {
				setOptimizationMethodRun(i);
			}
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
	}

	protected void setOptimizationMethodRun(int i) throws Exception {
		try {
			IParameterOptimizationMethod method = optimizationMethods.get(i);
			method.setRun(result);
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
	}

	protected void registerOptimizationMethods() throws RegisterException {
		try {
			// added 21.03.2013: handle registering of the methods
			for (int i = 0; i < optimizationMethods.size(); i++) {
				registerOptimizationMethod(i);
			}
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
	}

	protected void registerOptimizationMethod(int i) throws RegisterException {
		try {
			IParameterOptimizationMethod method = optimizationMethods.get(i);

			method.register();
			optimizationMethods.set(i, (IParameterOptimizationMethod) repo
					.getRegisteredObject(method));
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
	}

	protected IParameterOptimizationMethod parseOptimizationMethod(
			int paramOptIterations, int i, int j)
			throws UnknownParameterOptimizationMethodException,
			DynamicComponentInitializationException {
		try {
			IProgramConfig pc = programConfigs.get(i).clone();
			IDataConfig dc = dataConfigs.get(j).clone();

			if (repo instanceof RunResultRepository) {
				// fix the path to the data set
				String dsPath = FileUtils.buildPath(
						repo.getBasePath(IDataSet.class),
						pc.toString(".v") + "_" + dc.toString(".v"),
						dc.getDatasetConfig().getDataSet().toString(".v"));
				dc.getDatasetConfig().getDataSet()
						.setAbsolutePath(new File(dsPath));
			}

			IParameterOptimizationMethod result = ParameterOptimizationMethod
					.parseFromString(repo, paramOptMethods.get(i),
							// first we initialize the object with a null
							// reference instead of the run
							null, pc, dc, optimizationParameters.get(pc),
							paramOptIterations, false);
			return result;
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
			return null;
		}
	}

	protected int parseOptimizationIterations()
			throws ConfigurationException, RunException {
		try {
			if (!getProps().containsKey("optimizationIterations"))
				throw new RunException(
						"The number of optimization iterations has to be specified as attribute 'optimizationIterations'");
			int paramOptIterations = Integer
					.valueOf(getProps().getString("optimizationIterations"));
			return paramOptIterations;
		} catch (NumberFormatException e) {
			if (!handleException(e))
				throw e;
		}
		return 0;
	}

	protected IClusteringQualityMeasure parseOptimizationCriterion()
			throws ConfigurationException,
			UnknownClusteringQualityMeasureException,
			DynamicComponentInitializationException {
		IClusteringQualityMeasure optimizationCriterion = null;

		try {
			IClusteringQualityMeasure paramOptCriterion = ClusteringQualityMeasure
					.parseFromString(repo,
							getProps().getString("optimizationCriterion"),
							null);
			for (IClusteringQualityMeasure m : qualityMeasures)
				if (m.equals(paramOptCriterion))
					optimizationCriterion = m;
			if (optimizationCriterion == null)
				throw new UnknownClusteringQualityMeasureException(
						paramOptCriterion.toString(),
						"The optimization criterion is not contained in the list of quality measures.");
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return optimizationCriterion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.ExecutionRunParser#
	 * isParamConfigurationEntry(java.lang.String)
	 */
	@Override
	protected boolean isParamConfigurationEntry(String name) {
		return super.isParamConfigurationEntry(name)
				&& !name.equals("optimizationParameters")
				&& !name.equals("optimizationMethod");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.ExecutionRunParser#addParamValueToMap()
	 */
	@Override
	protected boolean checkParamValueToMap(final String param) {
		for (String optPa : optimizationParas)
			if (optPa.equals(param))
				return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.ExecutionRunParser#
	 * parseProgramConfigurations()
	 */
	@Override
	protected void parseProgramConfigurations()
			throws RepositoryObjectParseException {

		try {
			/*
			 * Default optimization method for all programs, where no specific
			 * method is defined
			 */
			paramOptMethod = getProps().getString("optimizationMethod");
			paramOptMethods = new ArrayList<String>();
		} catch (Exception e) {
			throw new RepositoryObjectParseException(this.getClassToParse(),
					this.name, this.versionString, e);
		}

		super.parseProgramConfigurations();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.ExecutionRunParser#
	 * parseProgramConfiguration(de.clusteval.program.ProgramConfig,
	 * java.util.Map)
	 */
	@Override
	protected void parseProgramConfigParams(IProgramConfig programConfig)
			throws NoOptimizableProgramParameterException,
			UnknownProgramParameterException, RunException,
			ConfigurationException {

		optParaList = new ArrayList<IProgramParameter<?>>();

		String[] possibleSectionNames = new String[]{programConfig.getName(),
				programConfig.toString()};

		// we assume that settings are stored within one section and take
		// the first one that matches
		boolean sectionFound = false;
		for (String possibleSectionName : possibleSectionNames) {
			if (getProps().getSections().contains(possibleSectionName)) {
				sectionFound = true;
				SubnodeConfiguration programConfigSection = getProps()
						.getSection(possibleSectionName);

				/*
				 * These parameters are used for parameter optimization. If we
				 * are in parameter optimization mode and there are concrete
				 * values for this parameters in this section, they will be
				 * ignored.
				 */
				optimizationParas = programConfigSection
						.getStringArray("optimizationParameters");

				/*
				 * Check whether the given optimization parameter are indeed
				 * defined as optimizable parameters in the program config.
				 */
				for (String optPa : optimizationParas) {
					try {
						IProgramParameter<?> p = programConfig
								.getParamWithId(optPa);
						if (!programConfig.getOptimizableParameters()
								.contains(p))
							throw new NoOptimizableProgramParameterException(
									"The run config " + absPath.getName()
											+ " contained invalid optimization parameters: "
											+ optPa
											+ " is not an optimizable program parameter of program "
											+ programConfig.getProgram());
						optParaList.add(p);

						/*
						 * Check whether we have overriden the properties of the
						 * parameter TODO: right now we are not checking,
						 * whether these values agree with the values set in the
						 * program configuration
						 */

						// we assume, that parameter sections are stored using
						// the same section name for the program config
						if (getProps().getSections()
								.contains(possibleSectionName + ":" + optPa)) {

							SubnodeConfiguration optParamSection = getProps()
									.getSection(
											possibleSectionName + ":" + optPa);

							if (optParamSection.containsKey("minValue")) {
								String minValue = optParamSection
										.getString("minValue");
								if (!this.parameterMinValueOverrides
										.containsKey(programConfig))
									this.parameterMinValueOverrides.put(
											programConfig,
											new HashMap<IProgramParameter<?>, String>());
								this.parameterMinValueOverrides
										.get(programConfig).put(p, minValue);
							}
							if (optParamSection.containsKey("maxValue")) {
								String maxValue = optParamSection
										.getString("maxValue");
								if (!this.parameterMaxValueOverrides
										.containsKey(programConfig))
									this.parameterMaxValueOverrides.put(
											programConfig,
											new HashMap<IProgramParameter<?>, String>());
								this.parameterMaxValueOverrides
										.get(programConfig).put(p, maxValue);
							}
							if (optParamSection.containsKey("options")) {
								String[] options = optParamSection
										.getStringArray("options");
								if (!this.parameterOptionsOverrides
										.containsKey(programConfig))
									this.parameterOptionsOverrides.put(
											programConfig,
											new HashMap<IProgramParameter<?>, String[]>());
								this.parameterOptionsOverrides
										.get(programConfig).put(p, options);
							}
						}
					} catch (UnknownProgramParameterException e) {
						/*
						 * Modify the message
						 */
						throw new UnknownProgramParameterException("The run "
								+ absPath.getName()
								+ " contained invalid parameter values: "
								+ programConfig.getProgram()
								+ " does not have a parameter " + optPa);
					}
				}

				if (programConfigSection.containsKey("optimizationMethod")) {
					paramOptMethods.add(programConfigSection
							.getString("optimizationMethod"));
				}
				/*
				 * Default optimization method of this run config
				 */
				else
					paramOptMethods.add(paramOptMethod);
			}

			if (sectionFound)
				break;
		}
		if (!sectionFound) {
			/*
			 * If there are no explicit optimization parameters set in the run
			 * config, use all optimizable parameters of program config.
			 */
			optParaList.addAll(programConfig.getOptimizableParameters());
			paramOptMethods.add(paramOptMethod);
		}

		if (optParaList.isEmpty())
			throw new RunException(
					"At least one optimization parameter must be specified for program configuration "
							+ programConfig);

		optimizationParameters.put(programConfig, optParaList);

		super.parseProgramConfigParams(programConfig);
	}
}