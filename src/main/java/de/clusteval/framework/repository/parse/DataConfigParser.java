/**
 * 
 */
package de.clusteval.framework.repository.parse;

import java.io.File;
import java.util.NoSuchElementException;

import de.clusteval.data.DataConfig;
import de.clusteval.data.DataConfigurationException;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.DataSetConfigNotFoundException;
import de.clusteval.data.dataset.IDataSetConfig;
import de.clusteval.data.goldstandard.GoldStandardConfigNotFoundException;
import de.clusteval.data.goldstandard.IGoldStandardConfig;
import de.clusteval.framework.repository.ObjectNotRegisteredException;

/**
 * @author Christian Wiwie
 *
 */
public class DataConfigParser extends RepositoryObjectParser<DataConfig> {

	private IGoldStandardConfig goldStandardConfig;
	private IDataSetConfig dataSetConfig;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.parse.Parser#getFileEnding()
	 */
	@Override
	protected String getFileEnding() {
		return ".dataconfig";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.RepositoryObjectParser#parseFromFile
	 * (java.io.File)
	 */
	@Override
	public void parseFromFile(File absPath)
			throws RepositoryObjectParseException {
		super.parseFromFile(absPath);

		try {
			log.debug("Parsing data config \"" + absPath + "\"");

			ensureVersionInFileName(absPath);

			try {
				getProps().setThrowExceptionOnMissing(true);

				parseDataSetConfig();
				parseGoldStandardConfig();

				result = new DataConfig(repo, changeDate, absPath,
						dataSetConfig, goldStandardConfig);
				if (!recoverFromExceptions) {
					result.register();
					result = (DataConfig) repo.getRegisteredObject(result);
				}
			} catch (NoSuchElementException e) {
				if (!handleException(e))
					throw new DataConfigurationException(e);
			}
		} catch (ObjectNotRegisteredException e) {
			if (!handleException(e)) {
				if (e.getClazz().equals(IDataSetConfig.class)) {
					throw new RepositoryObjectParseException(
							this.getClassToParse(), this.name,
							this.versionString,
							new DataSetConfigNotFoundException(
									e.getObjectName()));
				} else if (e.getClazz().equals(IGoldStandardConfig.class))
					throw new RepositoryObjectParseException(
							this.getClassToParse(), this.name,
							this.versionString,
							new GoldStandardConfigNotFoundException(
									e.getObjectName()));
			}
		} catch (Exception e) {
			if (!handleException(e))
				throw new RepositoryObjectParseException(this.getClassToParse(),
						this.name, this.versionString, e);
		}
	}

	/**
	 * @throws Exception
	 */
	protected void parseDataSetConfig() throws Exception {
		try {
			String datasetConfigName = getProps().getString("datasetConfig");
			dataSetConfig = (IDataSetConfig) this.repo
					.getStaticObjectWithNameAndVersion(IDataSetConfig.class,
							datasetConfigName);
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
	}

	/**
	 * @throws Exception
	 */
	protected void parseGoldStandardConfig() throws Exception {
		try {
			goldStandardConfig = null;
			if (getProps().containsKey("goldstandardConfig")) {
				String gsConfigName = getProps()
						.getString("goldstandardConfig");
				goldStandardConfig = (IGoldStandardConfig) this.repo
						.getStaticObjectWithNameAndVersion(
								IGoldStandardConfig.class, gsConfigName);
			}
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.parse.Parser#getClassToParse()
	 */
	@Override
	protected Class<IDataConfig> getClassToParse() {
		return IDataConfig.class;
	}
}