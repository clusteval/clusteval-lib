/**
 * 
 */
package de.clusteval.framework.repository.parse;

import de.clusteval.run.IAnalysisRun;

/**
 * @author Christian Wiwie
 *
 * @param <T>
 */
public abstract class AnalysisRunParser<T extends IAnalysisRun<?, ?>>
		extends
			RunParser<T> {
}