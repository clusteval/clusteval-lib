/**
 * 
 */
package de.clusteval.framework.repository.parse;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.data.dataset.DataSet;
import de.clusteval.data.dataset.DataSetConfig;
import de.clusteval.data.dataset.DataSetConfigurationException;
import de.clusteval.data.dataset.DataSetNotFoundException;
import de.clusteval.data.dataset.IDataSet;
import de.clusteval.data.dataset.IDataSetConfig;
import de.clusteval.data.dataset.IncompatibleDataSetConfigPreprocessorException;
import de.clusteval.data.dataset.format.ConversionInputToStandardConfiguration;
import de.clusteval.data.dataset.format.ConversionStandardToInputConfiguration;
import de.clusteval.data.dataset.format.IConversionInputToStandardConfiguration;
import de.clusteval.data.distance.DistanceMeasure;
import de.clusteval.data.distance.IDistanceMeasure;
import de.clusteval.data.distance.UnknownDistanceMeasureException;
import de.clusteval.data.preprocessing.DataPreprocessor;
import de.clusteval.data.preprocessing.IDataPreprocessor;
import de.clusteval.data.preprocessing.InvalidDataPreprocessorOptionsException;
import de.clusteval.data.preprocessing.UnknownDataPreprocessorException;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.utils.DynamicComponentInitializationException;
import dk.sdu.imada.compbio.utils.SimilarityMatrix.NUMBER_PRECISION;

/**
 * @author Christian Wiwie
 *
 */
public class DataSetConfigParser extends RepositoryObjectParser<DataSetConfig> {

	protected String datasetName;
	protected String datasetFile;

	protected IDataSet dataSet;
	protected IConversionInputToStandardConfiguration configInputToStandard;
	protected ConversionStandardToInputConfiguration configStandardToInput;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.parse.Parser#getFileEnding()
	 */
	@Override
	protected String getFileEnding() {
		return ".dsconfig";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.RepositoryObjectParser#parseFromFile
	 * (java.io.File)
	 */
	@Override
	public void parseFromFile(File absPath)
			throws RepositoryObjectParseException {
		super.parseFromFile(absPath);

		try {
			log.debug("Parsing dataset config \"" + absPath + "\"");

			ensureVersionInFileName(absPath);

			try {
				getProps().setThrowExceptionOnMissing(true);

				parseDataSetName();
				parseDataSetFile();
				dataSet = parseDataSet();

				IDistanceMeasure distanceMeasure = parseDistanceMeasure();

				NUMBER_PRECISION similarityPrecision = parsePrecision();

				// added 12.04.2013
				List<IDataPreprocessor> preprocessorBeforeDistance = parsePreprocessorsBeforeDistance();

				List<IDataPreprocessor> preprocessorAfterDistance = parsePreprocessorsAfterDistance();

				configInputToStandard = new ConversionInputToStandardConfiguration(
						distanceMeasure, similarityPrecision,
						preprocessorBeforeDistance, preprocessorAfterDistance);
				configStandardToInput = new ConversionStandardToInputConfiguration();

				result = new DataSetConfig(repo, changeDate, absPath, dataSet,
						configInputToStandard, configStandardToInput);
				if (!recoverFromExceptions) {
					result.register();
					result = (DataSetConfig) repo.getRegisteredObject(result);
				}
			} catch (NoSuchElementException e) {
				if (!handleException(e))
					throw new DataSetConfigurationException(e);
			}
		} catch (RepositoryObjectParseException e) {
			if (!handleException(e))
				throw e;
		} catch (ObjectNotRegisteredException e) {
			if (!handleException(e))
				throw new RepositoryObjectParseException(this.getClassToParse(),
						this.name, this.versionString,
						new DataSetNotFoundException(e.getObjectName()));
		} catch (Exception e) {
			if (!handleException(e))
				throw new RepositoryObjectParseException(this.getClassToParse(),
						this.name, this.versionString, e);
		}
	}

	protected void parseDataSetFile() throws ConfigurationException {
		try {
			datasetFile = getProps().getString("datasetFile");
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
	}

	protected void parseDataSetName() throws ConfigurationException {
		try {
			datasetName = getProps().getString("datasetName");
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
	}

	/**
	 * @return
	 * @throws ConfigurationException
	 * @throws UnknownDataPreprocessorException
	 * @throws InvalidDataPreprocessorOptionsException
	 */
	protected List<IDataPreprocessor> parsePreprocessorsAfterDistance()
			throws ConfigurationException, UnknownDataPreprocessorException,
			InvalidDataPreprocessorOptionsException,
			DynamicComponentInitializationException {
		List<IDataPreprocessor> preprocessorAfterDistance = new ArrayList<>();
		try {
			if (getProps().containsKey("preprocessorAfterDistance")) {
				String[] preprocessors = parsePreprocessorAfterDistanceNames();
				// find parameters for each of the preprocessors
				List<String[]> cliArguments = new ArrayList<String[]>();
				for (String preprocessor : preprocessors) {
					List<String> cli = parseArgumentsPreprocessorAfterDistance(
							preprocessor);
					cliArguments.add(cli.toArray(new String[0]));

					preprocessorAfterDistance
							.add(parseDataPreprocessor(preprocessor, cli));
				}
			} else
				preprocessorAfterDistance = new ArrayList<IDataPreprocessor>();
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return preprocessorAfterDistance;
	}

	protected String[] parsePreprocessorAfterDistanceNames()
			throws ConfigurationException {
		try {
			return getProps().getStringArray("preprocessorAfterDistance");
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return new String[0];
	}

	protected List<String> parseArgumentsPreprocessorAfterDistance(
			String preprocessor) throws ConfigurationException {
		List<String> cli = new ArrayList<String>();
		try {
			if (getProps().getSections().contains(preprocessor)) {
				Iterator<String> itParams = getProps().getSection(preprocessor)
						.getKeys();
				while (itParams.hasNext()) {
					String param = itParams.next();
					cli.add("-" + param);
					cli.add(getProps().getSection(preprocessor)
							.getString(param));
				}
			}
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return cli;
	}

	/**
	 * @return
	 * @throws ConfigurationException
	 * @throws UnknownDataPreprocessorException
	 * @throws InvalidDataPreprocessorOptionsException
	 * @throws IncompatibleDataSetConfigPreprocessorException
	 */
	protected List<IDataPreprocessor> parsePreprocessorsBeforeDistance()
			throws ConfigurationException, UnknownDataPreprocessorException,
			InvalidDataPreprocessorOptionsException,
			IncompatibleDataSetConfigPreprocessorException,
			DynamicComponentInitializationException {
		List<IDataPreprocessor> preprocessorBeforeDistance = new ArrayList<>();
		try {
			if (getProps().containsKey("preprocessorBeforeDistance")) {
				String[] preprocessors = parsePreprocessorBeforeDistanceNames();
				// find parameters for each of the preprocessors
				List<String[]> cliArguments = new ArrayList<String[]>();
				for (String preprocessor : preprocessors) {
					List<String> cli = parseArgumentsPreprocessorBeforeDistance(
							preprocessor);
					cliArguments.add(cli.toArray(new String[0]));

					preprocessorBeforeDistance
							.add(parseDataPreprocessor(preprocessor, cli));
				}

				for (IDataPreprocessor proc : preprocessorBeforeDistance) {
					if (!proc.getCompatibleDataSetFormats().contains(dataSet
							.getDataSetFormat().getClass().getSimpleName())) {
						throw new IncompatibleDataSetConfigPreprocessorException(
								"The data preprocessor "
										+ proc.getClass().getSimpleName()
										+ " cannot be applied to a dataset with format "
										+ dataSet.getDataSetFormat().getClass()
												.getSimpleName());
					}
				}
			} else
				preprocessorBeforeDistance = new ArrayList<IDataPreprocessor>();
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return preprocessorBeforeDistance;
	}

	protected IDataPreprocessor parseDataPreprocessor(String preprocessor,
			List<String> cli) throws UnknownDataPreprocessorException,
			InvalidDataPreprocessorOptionsException,
			DynamicComponentInitializationException {
		try {
			return DataPreprocessor.parseFromString(repo, preprocessor,
					cli.toArray(new String[0]));
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
			return null;
		}
	}

	protected List<String> parseArgumentsPreprocessorBeforeDistance(
			String preprocessor) throws ConfigurationException {
		List<String> cli = new ArrayList<String>();
		try {
			if (getProps().getSections().contains(preprocessor)) {
				Iterator<String> itParams = getProps().getSection(preprocessor)
						.getKeys();
				while (itParams.hasNext()) {
					String param = itParams.next();
					cli.add(param);
					cli.add(getProps().getSection(preprocessor)
							.getString(param));
				}
			}
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return cli;
	}

	protected String[] parsePreprocessorBeforeDistanceNames()
			throws ConfigurationException {
		try {
			String[] preprocessors = getProps()
					.getStringArray("preprocessorBeforeDistance");
			return preprocessors;
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return new String[0];
	}

	/**
	 * @return
	 * @throws ConfigurationException
	 */
	private NUMBER_PRECISION parsePrecision() throws ConfigurationException {
		NUMBER_PRECISION similarityPrecision = NUMBER_PRECISION.DOUBLE;
		try {
			if (getProps().containsKey("similarityPrecision")) {
				String val = getProps().getString("similarityPrecision");
				if (val.equals("double"))
					similarityPrecision = NUMBER_PRECISION.DOUBLE;
				else if (val.equals("float"))
					similarityPrecision = NUMBER_PRECISION.FLOAT;
				else if (val.equals("short"))
					similarityPrecision = NUMBER_PRECISION.SHORT;
			}
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return similarityPrecision;
	}

	/**
	 * @return
	 * @throws ConfigurationException
	 * @throws UnknownDistanceMeasureException
	 */
	private IDistanceMeasure parseDistanceMeasure()
			throws ConfigurationException, UnknownDistanceMeasureException,
			DynamicComponentInitializationException {
		IDistanceMeasure distanceMeasure = null;
		try {
			if (getProps().containsKey("distanceMeasureAbsoluteToRelative")) {
				distanceMeasure = DistanceMeasure.parseFromString(repo,
						getProps().getString(
								"distanceMeasureAbsoluteToRelative"));
			} else
				distanceMeasure = DistanceMeasure.parseFromString(repo,
						"EuclidianDistanceMeasure");
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return distanceMeasure;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.parse.Parser#getClassToParse()
	 */
	@Override
	protected Class<IDataSetConfig> getClassToParse() {
		return IDataSetConfig.class;
	}

	protected IDataSet parseDataSet() throws RepositoryObjectParseException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException {
		try {
			return (DataSet) repo.getStaticObjectWithNameAndVersion(
					IDataSet.class, datasetName + "/" + datasetFile);
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return null;
	}

	protected boolean hasVersionInFileName(String datasetName) {
		int lastIndex = datasetName.lastIndexOf(".");
		if (lastIndex < 0)
			return false;
		return datasetName.substring(lastIndex).matches("\\.v\\d+(-SNAPSHOT)?");
	}

	protected String insertVersionIntoDatasetName(String datasetName,
			ComparableVersion version) {
		return datasetName + ".v" + version.toString();
	}
}