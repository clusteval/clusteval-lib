/**
 * 
 */
package de.clusteval.framework.repository.parse;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.cluster.paramOptimization.InvalidOptimizationParameterException;
import de.clusteval.context.Context;
import de.clusteval.context.IContext;
import de.clusteval.context.UnknownContextException;
import de.clusteval.data.dataset.format.DataSetFormat;
import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.program.IProgram;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.program.IStandaloneProgram;
import de.clusteval.program.ProgramConfig;
import de.clusteval.program.ProgramParameter;
import de.clusteval.program.StandaloneProgramConfig;
import de.clusteval.program.UnknownParameterType;
import de.clusteval.program.UnknownProgramParameterException;
import de.clusteval.program.UnknownProgramTypeException;
import de.clusteval.program.r.IRProgram;
import de.clusteval.program.r.RProgram;
import de.clusteval.program.r.RProgramConfig;
import de.clusteval.program.r.UnknownRProgramException;
import de.clusteval.run.runresult.format.IRunResultFormat;
import de.clusteval.run.runresult.format.RunResultFormat;
import de.clusteval.run.runresult.format.UnknownRunResultFormatException;
import de.clusteval.utils.DynamicComponentInitializationException;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * @author Christian Wiwie
 *
 */
public class ProgramConfigParser extends RepositoryObjectParser<ProgramConfig> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.parse.Parser#getFileEnding()
	 */
	@Override
	protected String getFileEnding() {
		return ".config";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.parse.Parser#getClassToParse()
	 */
	@Override
	protected Class<IProgramConfig> getClassToParse() {
		return IProgramConfig.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.RepositoryObjectParser#parseFromFile
	 * (java.io.File)
	 */
	@Override
	public void parseFromFile(File absPath)
			throws RepositoryObjectParseException {
		super.parseFromFile(absPath);

		try {
			ensureVersionInFileName(absPath);

			log.debug("Parsing program config \"" + absPath + "\"");
			getProps().setThrowExceptionOnMissing(true);

			IContext context = parseContext();

			/*
			 * Added 07.08.2012 Type of programconfig is either standalone or R
			 */
			String type = parseType();

			IProgram programP = null;

			IRunResultFormat runresultFormat = null;
			List<IDataSetFormat> compatibleDataSetFormats = null;
			boolean expectsNormalizedDataSet = false;

			List<String> parameterNames = null;
			List<IProgramParameter<?>> params = null;

			if (type.equals("standalone")) {
				String programPath = parseStandaloneProgramPath();
				File programFile = parseStandaloneProgramFile(programPath);

				changeDate = programFile.lastModified();

				String outputFormat = parseOutputFormatName();

				// initialize compatible dataset formats
				String[] compatibleDataSetFormatsStr = parseCompatibleFormatNames();
				compatibleDataSetFormats = parseCompatibleFormats(
						compatibleDataSetFormatsStr);

				expectsNormalizedDataSet = parseExpectsNormalizedDataSet();

				setNormalizedToFormats(compatibleDataSetFormats,
						expectsNormalizedDataSet);

				runresultFormat = parseOutputFormat(outputFormat);

				// programP = new StandaloneProgram(repo, context, changeDate,
				// programFile);
				// if (!recoverFromExceptions)
				// programP.register();
				programP = (IProgram) this.repo
						.getStaticObjectWithNameAndVersion(
								IStandaloneProgram.class, programPath, false);

				// we parse the parameters from the program configuration
				parameterNames = parseParameterNames();
				params = new ArrayList<IProgramParameter<?>>();
				for (String pa : parameterNames)
					params.add(ProgramParameter.parseFromConfiguration(result,
							pa, getProps().getSection(pa)));

			} else {
				// try loading it as r-program
				try {
					programP = parseRProgram(type);
					IRProgram rProgram = (IRProgram) programP;

					compatibleDataSetFormats = parseRProgramCompatibleFormats(
							rProgram);

					runresultFormat = parseRProgramOutputFormat(rProgram);

					// we retrieve the parameters from the RProgram class
					params = rProgram.getParameters();
					parameterNames = new ArrayList<>();
					for (IProgramParameter<?> p : params)
						parameterNames.add(p.getName());
				} catch (UnknownRProgramException e) {
					if (!handleException(e))
						throw new UnknownProgramTypeException(
								"The type " + type + " is unknown.");
				} catch (Exception e) {
					if (!handleException(e))
						throw e;
				}
			}

			List<IProgramParameter<?>> optimizableParameters = new ArrayList<IProgramParameter<?>>();

			changeDate = absPath.lastModified();

			// check whether there are parameter-sections for parameters, that
			// are not listed in the parameters-list
			validateSections(absPath, parameterNames);

			int maxExecutionTimeMinutes = parseMaxExecutionTimeMinutes();

			if (type.equals("standalone")) {
				String invocationFormat = parseInvocationFormat();
				String invocationFormatWithoutGoldStandard = null;
				String invocationFormatParameterOptimization = null;
				String invocationFormatParameterOptimizationWithoutGoldStandard = null;

				invocationFormatWithoutGoldStandard = parseInvocationFormatWithoutGoldStandard(
						invocationFormat);

				invocationFormatParameterOptimization = parseInvocationFormatParamOpt(
						invocationFormatParameterOptimization);

				invocationFormatParameterOptimizationWithoutGoldStandard = parseInvocationFormatParamOptWithoutGoldStandard(
						invocationFormatParameterOptimization);

				String alias = parseAlias();
				Map<String, String> envVars = parseEnvVars();

				result = new StandaloneProgramConfig(repo, changeDate, absPath,
						programP, runresultFormat, compatibleDataSetFormats,
						invocationFormat, invocationFormatWithoutGoldStandard,
						invocationFormatParameterOptimization,
						invocationFormatParameterOptimizationWithoutGoldStandard,
						params, optimizableParameters, expectsNormalizedDataSet,
						maxExecutionTimeMinutes, alias, envVars);
			}
			// RProgram
			else {
				result = new RProgramConfig(repo, changeDate, absPath, programP,
						optimizableParameters, expectsNormalizedDataSet,
						maxExecutionTimeMinutes);
			}
			if (!recoverFromExceptions) {
				// boolean programConfigRegistered = result.register();
				// only parse the parameters if this program configuration is
				// new
				// if (programConfigRegistered) {
				/*
				 * Get the optimization parameters (parameters, that can be
				 * optimized for this program in parameter_optimization runmode
				 */
				String[] optimizableParamNames = parseOptimizableParamNames();

				// iterate over all parameters
				parseParameters(parameterNames, params, optimizableParameters,
						optimizableParamNames);
				// }
				result.register();
				result = (ProgramConfig) repo.getRegisteredObject(result);
			}

		} catch (Exception e) {
			if (!handleException(e))
				throw new RepositoryObjectParseException(this.getClassToParse(),
						this.name, this.versionString, e);
		}
	}

	protected void parseParameters(List<String> parameterNames,
			List<IProgramParameter<?>> params,
			List<IProgramParameter<?>> optimizableParameters,
			String[] optimizableParamNames)
			throws RegisterException, UnknownParameterType,
			ConfigurationException, InvalidOptimizationParameterException {
		try {
			for (int p = 0; p < params.size(); p++) {
				parseParameter(params, optimizableParameters,
						optimizableParamNames, p);
			}
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
	}

	protected void parseParameter(List<IProgramParameter<?>> params,
			List<IProgramParameter<?>> optimizableParameters,
			String[] optimizableParamNames, int p)
			throws RegisterException, UnknownParameterType,
			ConfigurationException, InvalidOptimizationParameterException {
		try {
			IProgramParameter<?> param = params.get(p);
			String pa = param.getName();
			// skip the empty string
			if (pa.equals(""))
				return;

			final Map<String, String> paramValues = new HashMap<String, String>();
			paramValues.put("name", pa);

			/*
			 * Check if this parameter is declared as an optimizable parameter
			 */
			boolean optimizable = false;
			for (String optPa : optimizableParamNames)
				if (optPa.equals(pa)) {
					optimizable = true;
					break;
				}

			if (optimizable) {
				/*
				 * Check if min and max values are given for this parameter,
				 * which is necessary for optimizing it
				 */
				if (!(param.isMinValueSet() || !param.isMaxValueSet())
						&& !param.isOptionsSet())
					throw new InvalidOptimizationParameterException(
							"The parameter " + param
									+ " cannot be used as an optimization parameter, because its min and max values are not set.");
				optimizableParameters.add(param);
			}
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
	}

	protected String[] parseOptimizableParamNames()
			throws ConfigurationException {
		try {
			String[] optimizableParams = getProps()
					.getStringArray("optimizationParameters");
			return optimizableParams;
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return new String[0];
	}

	protected String parseInvocationFormatParamOptWithoutGoldStandard(
			String invocationFormatParameterOptimization)
			throws ConfigurationException {
		String invocationFormatParameterOptimizationWithoutGoldStandard = null;
		try {
			if (getProps().getSection("invocationFormat").containsKey(
					"invocationFormatParameterOptimizationWithoutGoldStandard"))
				invocationFormatParameterOptimizationWithoutGoldStandard = getProps()
						.getSection("invocationFormat").getString(
								"invocationFormatParameterOptimizationWithoutGoldStandard");
			else
				invocationFormatParameterOptimizationWithoutGoldStandard = invocationFormatParameterOptimization;
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return invocationFormatParameterOptimizationWithoutGoldStandard;
	}

	protected String parseInvocationFormatParamOpt(
			String invocationFormatParameterOptimization)
			throws ConfigurationException {
		try {
			if (getProps().getSection("invocationFormat")
					.containsKey("invocationFormatParameterOptimization"))
				invocationFormatParameterOptimization = getProps()
						.getSection("invocationFormat")
						.getString("invocationFormatParameterOptimization");
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return invocationFormatParameterOptimization;
	}

	protected String parseInvocationFormatWithoutGoldStandard(
			String invocationFormat) throws ConfigurationException {
		String invocationFormatWithoutGoldStandard = null;
		try {
			if (getProps().getSection("invocationFormat")
					.containsKey("invocationFormatWithoutGoldStandard"))
				invocationFormatWithoutGoldStandard = getProps()
						.getSection("invocationFormat")
						.getString("invocationFormatWithoutGoldStandard");
			else
				invocationFormatWithoutGoldStandard = invocationFormat;
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return invocationFormatWithoutGoldStandard;
	}

	protected String parseInvocationFormat() throws ConfigurationException {
		try {
			String invocationFormat = getProps().getSection("invocationFormat")
					.getString("invocationFormat");
			return invocationFormat;
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return null;
	}

	protected int parseMaxExecutionTimeMinutes() throws ConfigurationException {
		int maxExecutionTimeMinutes = -1;
		try {
			if (getProps().containsKey("maxExecutionTimeMinutes"))
				maxExecutionTimeMinutes = getProps()
						.getInt("maxExecutionTimeMinutes");
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return maxExecutionTimeMinutes;
	}

	protected void validateSections(File absPath, List<String> parameterNames)
			throws ConfigurationException, UnknownProgramParameterException {
		try {
			Set<String> sections = getProps().getSections();
			sections.removeAll(parameterNames);
			sections.remove("envVars");
			sections.remove(null);
			sections.remove("invocationFormat");

			if (sections.size() > 0) {
				throw new UnknownProgramParameterException(
						"There are parameter-sections " + sections
								+ " in ProgramConfig " + absPath.getName()
								+ " for undefined parameters. Please add them to the parameter-list.");
			}
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
	}

	protected List<String> parseParameterNames() throws ConfigurationException {
		try {
			List<String> parameterNames = Arrays
					.asList(getProps().getStringArray("parameters"));
			return parameterNames;
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return new ArrayList<>();
	}

	protected IRunResultFormat parseRProgramOutputFormat(IRProgram rProgram)
			throws UnknownRunResultFormatException,
			DynamicComponentInitializationException {
		IRunResultFormat runresultFormat = null;
		try {
			runresultFormat = rProgram.getRunResultFormat();
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return runresultFormat;
	}

	protected List<IDataSetFormat> parseRProgramCompatibleFormats(
			IRProgram rProgram) throws UnknownDataSetFormatException,
			DynamicComponentInitializationException {
		List<IDataSetFormat> compatibleDataSetFormats = new ArrayList<IDataSetFormat>();
		try {
			compatibleDataSetFormats
					.addAll(rProgram.getCompatibleDataSetFormats());
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return compatibleDataSetFormats;
	}

	protected IProgram parseRProgram(String type)
			throws UnknownRProgramException,
			DynamicComponentInitializationException,
			UnknownRunResultFormatException, UnknownDataSetFormatException {
		IProgram programP = null;
		try {
			programP = RProgram.parseFromString(repo, type, true);
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return programP;
	}

	protected Map<String, String> parseEnvVars() throws ConfigurationException {
		Map<String, String> envVars = new HashMap<String, String>();
		try {
			Iterator<String> vars = getProps().getSection("envVars").getKeys();
			while (vars.hasNext()) {
				String var = vars.next();
				envVars.put(var,
						getProps().getSection("envVars").getString(var));
			}
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return envVars;
	}

	protected String parseAlias() throws ConfigurationException {
		try {
			String alias = getProps().getString("alias");
			return alias;
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return null;
	}

	protected IRunResultFormat parseOutputFormat(String outputFormat)
			throws UnknownRunResultFormatException,
			DynamicComponentInitializationException {
		IRunResultFormat runresultFormat = null;
		try {
			runresultFormat = RunResultFormat.parseFromString(repo,
					outputFormat);
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return runresultFormat;
	}

	protected void setNormalizedToFormats(
			List<IDataSetFormat> compatibleDataSetFormats,
			boolean expectsNormalizedDataSet) {
		try {
			for (IDataSetFormat format : compatibleDataSetFormats)
				setNormalizedFormat(expectsNormalizedDataSet, format);
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
	}

	protected void setNormalizedFormat(boolean expectsNormalizedDataSet,
			IDataSetFormat format) {
		try {
			format.setNormalized(expectsNormalizedDataSet);
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
	}

	protected boolean parseExpectsNormalizedDataSet()
			throws ConfigurationException {
		try {
			boolean expectsNormalizedDataSet;
			if (getProps().containsKey("expectsNormalizedDataSet"))
				expectsNormalizedDataSet = getProps()
						.getBoolean("expectsNormalizedDataSet");
			else
				expectsNormalizedDataSet = false;
			return expectsNormalizedDataSet;
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return false;
	}

	protected List<IDataSetFormat> parseCompatibleFormats(
			String[] compatibleDataSetFormatsStr)
			throws UnknownDataSetFormatException,
			DynamicComponentInitializationException {
		List<IDataSetFormat> compatibleDataSetFormats = new ArrayList<>();
		try {
			for (String f : compatibleDataSetFormatsStr)
				compatibleDataSetFormats.add(parseCompatibleFormat(f));
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return compatibleDataSetFormats;
	}

	protected IDataSetFormat parseCompatibleFormat(String f)
			throws UnknownDataSetFormatException,
			DynamicComponentInitializationException {
		try {
			return DataSetFormat.parseFromString(repo, f);
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return null;
	}

	protected String[] parseCompatibleFormatNames()
			throws ConfigurationException {
		try {
			String[] compatibleDataSetFormatsStr;
			compatibleDataSetFormatsStr = getProps()
					.getStringArray("compatibleDataSetFormats");
			return compatibleDataSetFormatsStr;
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return new String[0];
	}

	protected String parseOutputFormatName() throws ConfigurationException {
		try {
			String outputFormat = getProps().getString("outputFormat");
			return outputFormat;
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return null;
	}

	protected File parseStandaloneProgramFile(String programPath)
			throws Exception {
		File programFile = null;
		try {
			ComparableVersion version;
			String programWithoutVersion;
			if (programPath.lastIndexOf(":") < 0
					|| !programPath.substring(programPath.lastIndexOf(":"))
							.matches("\\:\\d+(-SNAPSHOT)?")) {
				version = new ComparableVersion("1");
				programWithoutVersion = programPath;
			} else {
				programWithoutVersion = programPath.substring(0,
						programPath.lastIndexOf(":"));
				version = new ComparableVersion(programPath
						.substring(programPath.lastIndexOf(":") + 1));
			}

			String program = FileUtils.buildPath(
					repo.getBasePath(IStandaloneProgram.class),
					String.format("%s.v%s%s",
							programWithoutVersion.substring(0,
									programWithoutVersion.lastIndexOf("/")),
							version.toString(), programWithoutVersion.substring(
									programWithoutVersion.lastIndexOf("/"))));

			programFile = new File(program);

			if (!(programFile).exists()) {
				try {
					IStandaloneProgram p = this.repo
							.getStaticObjectWithNameAndVersion(
									IStandaloneProgram.class,
									getProps().getString("program"));
				} catch (ObjectNotRegisteredException e) {
				}
				if (!(programFile).exists())
					throw new FileNotFoundException(String.format(
							"The given program executable does not exist: %s:%s",
							programWithoutVersion, version));
			}
			return programFile;
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return programFile;
	}

	protected String parseStandaloneProgramPath()
			throws ConfigurationException {
		try {
			String programPath = getProps().getString("program");
			return programPath;
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return null;
	}

	protected String parseType() throws ConfigurationException {
		try {
			String type;
			if (getProps().containsKey("type")) {
				type = getProps().getString("type");
			} else
				// Default
				type = "standalone";
			return type;
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return null;
	}

	protected IContext parseContext() throws ConfigurationException,
			UnknownContextException, DynamicComponentInitializationException {
		IContext context = null;
		try {
			// by default we are in a clustering context
			if (getProps().containsKey("context"))
				context = Context.parseFromString(repo,
						getProps().getString("context"));
			else
				context = Context.parseFromString(repo, "ClusteringContext");
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return context;
	}

}