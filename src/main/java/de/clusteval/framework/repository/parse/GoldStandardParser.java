/**
 * 
 */
package de.clusteval.framework.repository.parse;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import de.clusteval.data.dataset.DataSetAttributeParser;
import de.clusteval.data.goldstandard.GoldStandard;
import de.clusteval.data.goldstandard.IGoldStandard;
import de.clusteval.framework.repository.Repository;

/**
 * @author Christian Wiwie
 *
 */
public class GoldStandardParser extends RepositoryObjectParser<GoldStandard> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.parse.Parser#getFileEnding()
	 */
	@Override
	protected String getFileEnding() {
		return "";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.parse.Parser#getClassToParse()
	 */
	@Override
	protected Class<IGoldStandard> getClassToParse() {
		return IGoldStandard.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.RepositoryObjectParser#parseFromFile
	 * (java.io.File)
	 */
	@Override
	public void parseFromFile(File absPath)
			throws RepositoryObjectParseException {
		super.parseFromFile(absPath);

		try {

			ensureVersionInFileName(absPath);

			final long changeDate = absPath.lastModified();

			this.result = new GoldStandard(
					Repository.getRepositoryForPath(absPath.getAbsolutePath()),
					changeDate, absPath);
			if (!recoverFromExceptions)
				result.register();
		} catch (Exception e) {
			if (!handleException(e))
				throw new RepositoryObjectParseException(this.getClassToParse(),
						this.name, this.versionString, e);
		}
	}

	/**
	 * This method parses the header of a dataset file. A header is required for
	 * a dataset file to be recognized by the framework as a valid dataset file.
	 * If the file does not contain any header lines, it is ignored by the
	 * framework. A header line is of the form '// attribute = value'. The
	 * header should contain several lines:
	 * 
	 * <p>
	 * The type of the dataset, e.g. '// dataSetType =
	 * GeneExpressionDataSetType'
	 * <p>
	 * The format of the dataset, e.g. '// dataSetFormat =
	 * RowSimDataSetFormat:1'
	 * 
	 * @param absPath
	 * @return
	 * @throws IOException
	 */
	protected static Map<String, String> extractDataSetAttributes(
			final File absPath) throws IOException {
		DataSetAttributeParser attributeParser = new DataSetAttributeParser(
				absPath.getAbsolutePath());
		attributeParser.process();
		Map<String, String> attributeValues = attributeParser
				.getAttributeValues();
		return attributeValues;
	}
}