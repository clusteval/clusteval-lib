/**
 * 
 */
package de.clusteval.framework.repository.parse;

import java.io.File;

import org.apache.commons.configuration.ConfigurationException;

import de.clusteval.context.Context;
import de.clusteval.context.IContext;
import de.clusteval.context.UnknownContextException;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.run.IRun;
import de.clusteval.utils.DynamicComponentInitializationException;

// TODO: This parser should actually be abstract, but we need to make it
// concrete because we are using it to parse the mode of a run file
/**
 * @author Christian Wiwie
 *
 * @param <T>
 */
public class RunParser<T extends IRun> extends RepositoryObjectParser<T> {

	// the members of the Run class
	protected IContext context;

	protected String mode;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.parse.Parser#getFileEnding()
	 */
	@Override
	protected String getFileEnding() {
		return ".run";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.parse.Parser#getClassToParse()
	 */
	@Override
	protected Class<? extends IRepositoryObject> getClassToParse() {
		// TODO: we need this to be implemented because we use this parser
		// (only) to parse the mode of a run file
		return IRun.class;
	}

	@Override
	public void parseFromFile(final File absPath)
			throws RepositoryObjectParseException {
		super.parseFromFile(absPath);

		try {
			ensureVersionInFileName(absPath);

			parseContext();

			parseMode();
		} catch (Exception e) {
			if (!handleException(e))
				throw new RepositoryObjectParseException(this.getClassToParse(),
						this.name, this.versionString, e);
		}
	}

	protected void parseContext() throws ConfigurationException,
			UnknownContextException, DynamicComponentInitializationException {
		try {
			// by default we are in a clustering context
			if (getProps().containsKey("context"))
				context = Context.parseFromString(repo,
						getProps().getString("context"));
			else
				context = Context.parseFromString(repo, "ClusteringContext");
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
	}

	protected void parseMode() throws ConfigurationException {
		try {
			mode = getProps().getString("mode", "clustering");
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
	}
}