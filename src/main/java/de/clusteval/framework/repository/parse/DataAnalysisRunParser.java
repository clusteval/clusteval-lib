/**
 * 
 */
package de.clusteval.framework.repository.parse;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.configuration.ConfigurationException;

import de.clusteval.data.DataConfig;
import de.clusteval.data.DataConfigNotFoundException;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.statistics.DataStatistic;
import de.clusteval.data.statistics.IDataStatistic;
import de.clusteval.data.statistics.UnknownDataStatisticException;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.run.DataAnalysisRun;
import de.clusteval.run.IDataAnalysisRun;
import de.clusteval.run.RunException;
import de.clusteval.utils.DynamicComponentInitializationException;

/**
 * @author Christian Wiwie
 *
 */
public class DataAnalysisRunParser extends AnalysisRunParser<DataAnalysisRun> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.parse.Parser#getClassToParse()
	 */
	@Override
	protected Class<IDataAnalysisRun> getClassToParse() {
		return IDataAnalysisRun.class;
	}

	@Override
	public void parseFromFile(File absPath)
			throws RepositoryObjectParseException {
		super.parseFromFile(absPath);

		try {

			String[] list = getProps().getStringArray("dataConfig");
			if (list.length == 0)
				throw new RunException(
						"At least one data config must be specified");
			// 2016/05/05: remove duplicates.
			list = new ArrayList<String>(
					new HashSet<String>(Arrays.asList(list)))
							.toArray(new String[0]);

			List<IDataConfig> dataConfigs = parseDataConfigs(list);

			List<IDataStatistic> dataStatistics = parseDataStatistics();

			result = new DataAnalysisRun(repo, context, changeDate, absPath,
					dataConfigs, dataStatistics);
			if (!recoverFromExceptions) {
				result.register();
				result = (DataAnalysisRun) repo.getRegisteredObject(result,
						false);
			}
		} catch (ObjectNotRegisteredException e) {
			if (!handleException(e))
				throw new RepositoryObjectParseException(this.getClassToParse(),
						this.name, this.versionString,
						new DataConfigNotFoundException(e.getObjectName()));
		} catch (Exception e) {
			if (!handleException(e))
				throw new RepositoryObjectParseException(this.getClassToParse(),
						this.name, this.versionString, e);
		}
	}

	protected List<IDataStatistic> parseDataStatistics()
			throws ConfigurationException, UnknownDataStatisticException,
			DynamicComponentInitializationException {
		List<IDataStatistic> dataStatistics = new LinkedList<IDataStatistic>();
		try {
			/*
			 * We catch the exceptions such that all statistics are tried to be
			 * loaded once so that they are ALL registered as missing in the
			 * repository.
			 */
			List<UnknownDataStatisticException> thrownExceptions = new ArrayList<UnknownDataStatisticException>();
			for (String dataStatistic : getProps()
					.getStringArray("dataStatistics")) {
				try {
					dataStatistics.add(parseDataStatistic(dataStatistic));
				} catch (UnknownDataStatisticException e) {
					if (!handleException(e))
						thrownExceptions.add(e);
				}
			}
			if (thrownExceptions.size() > 0) {
				// just throw the first exception
				throw thrownExceptions.get(0);
			}
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
		return dataStatistics;
	}

	protected IDataStatistic parseDataStatistic(String dataStatistic)
			throws UnknownDataStatisticException,
			DynamicComponentInitializationException {
		try {
			return DataStatistic.parseFromString(repo, dataStatistic);
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
			return null;
		}
	}

	/*
	 * An analysis run consists of a set of dataconfigs
	 */
	protected List<IDataConfig> parseDataConfigs(String[] list)
			throws Exception {
		List<IDataConfig> dataConfigs = new LinkedList<IDataConfig>();
		try {
			for (String dataConfig : list) {
				parseDataConfig(dataConfigs, dataConfig);
			}
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
			dataConfigs.add(null);
		}
		return dataConfigs;
	}

	protected void parseDataConfig(List<IDataConfig> dataConfigs,
			String dataConfig) throws Exception {
		try {
			dataConfigs.add((DataConfig) repo.getStaticObjectWithNameAndVersion(
					IDataConfig.class, dataConfig));
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
	};
}