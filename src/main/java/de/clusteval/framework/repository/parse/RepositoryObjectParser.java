/**
 * 
 */
package de.clusteval.framework.repository.parse;

import java.io.File;
import java.io.FileNotFoundException;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalINIConfiguration;

import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.Repository;

/**
 * @author Christian Wiwie
 *
 * @param <T>
 */
public abstract class RepositoryObjectParser<T extends IRepositoryObject>
		extends
			Parser<T> {

	// the members of the RepositoryObject class

	private HierarchicalINIConfiguration props;
	protected IRepository repo;
	protected long changeDate;
	protected File absPath;
	protected String fileEnding;
	protected String name, versionString;

	public void parseFromFile(final File absPath)
			throws RepositoryObjectParseException {

		this.absPath = absPath;
		this.name = parseName();
		this.versionString = parseVersion();

		try {
			validateAbsPath();

			this.repo = Repository
					.getRepositoryForPath(this.absPath.getAbsolutePath());
			this.changeDate = this.absPath.lastModified();
		} catch (Exception e) {
			if (!handleException(e))
				throw new RepositoryObjectParseException(this.getClassToParse(),
						this.name, this.versionString, e);
		}
	}

	protected void validateAbsPath() throws FileNotFoundException {
		try {
			if (!this.absPath.exists())
				throw new FileNotFoundException(
						"File \"" + this.absPath + "\" does not exist!");
		} catch (Exception e) {
			if (!handleException(e))
				throw e;
		}
	}

	protected HierarchicalINIConfiguration getProps()
			throws ConfigurationException {
		if (props == null)
			props = new HierarchicalINIConfiguration(absPath.getAbsolutePath());
		return props;
	}

	protected final String parseName() {
		return this.absPath.getName().replaceAll("("
				+ getAbsPathVersionMatchString() + ")?" + getFileEnding() + "$",
				"");
	}

	protected final String getAbsPathVersionMatchString() {
		return "\\.v\\d+(-SNAPSHOT)?";
	}

	protected boolean hasVersionInFileName(String fileName) {
		int lastIndex = fileName.lastIndexOf(".");
		if (lastIndex < 0)
			return false;
		return fileName.substring(lastIndex).matches("\\.v\\d+(-SNAPSHOT)?");
	}

	protected String parseVersion() {
		if (!hasVersionInFileName(
				absPath.getName().replaceAll(getFileEnding() + "$", "")))
			return "";
		String name = absPath.getName().replaceAll(getFileEnding(), "");
		int colonInd = name.lastIndexOf(".");
		return name.substring(colonInd + 2);
	}
}