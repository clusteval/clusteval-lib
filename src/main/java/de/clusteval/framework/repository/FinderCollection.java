/**
 * 
 */
package de.clusteval.framework.repository;

import de.clusteval.utils.IFinder;

/**
 * @author Christian Wiwie
 * 
 */
public class FinderCollection extends StaticRepositoryObjectCollection {

	/**
	 * @param repository
	 * @param parent
	 * @param basePath
	 */
	public FinderCollection(IRepository repository, StaticRepositoryObjectCollection parent, String basePath) {
		super(repository, IFinder.class, parent, basePath);
		this.printOnRegister = false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.StaticRepositoryEntity#
	 * unregisterAfterRemove(de.clusteval.framework.repository.RepositoryObject)
	 */
	@Override
	protected void unregisterAfterRemove(IRepositoryObject object) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.StaticRepositoryEntity#register(de.
	 * clusteval.framework.repository.IRepositoryObject)
	 */
	@Override
	public boolean register(IRepositoryObject object) throws RegisterException {
		return super.register(object);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.StaticRepositoryEntity#
	 * registerWhenExisting (de.clusteval.framework.repository.RepositoryObject,
	 * de.clusteval.framework.repository.RepositoryObject)
	 */
	@Override
	protected boolean registerWhenExisting(IRepositoryObject old, IRepositoryObject object) throws RegisterException {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.StaticRepositoryEntity#isVersioned()
	 */
	@Override
	public boolean isVersioned() {
		return false;
	}

}
