/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
package de.clusteval.framework.repository;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.annotation.AnnotationTypeMismatchException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.commons.math3.fitting.PolynomialCurveFitter;
import org.apache.commons.math3.fitting.WeightedObservedPoints;
import org.apache.maven.artifact.versioning.ArtifactVersion;
import org.apache.maven.artifact.versioning.ComparableVersion;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;
import org.apache.maven.artifact.versioning.InvalidVersionSpecificationException;
import org.apache.maven.artifact.versioning.VersionRange;
import org.rosuda.REngine.Rserve.RserveException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.clusteval.cluster.IClustering;
import de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod;
import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.cluster.quality.UnknownClusteringQualityMeasureException;
import de.clusteval.context.IContext;
import de.clusteval.context.UnknownContextException;
import de.clusteval.data.DataConfig;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.IDataSet;
import de.clusteval.data.dataset.IDataSetConfig;
import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.data.dataset.format.IDataSetFormatParser;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.dataset.generator.IDataSetGenerator;
import de.clusteval.data.dataset.type.IDataSetType;
import de.clusteval.data.dataset.type.UnknownDataSetTypeException;
import de.clusteval.data.distance.IDistanceMeasure;
import de.clusteval.data.distance.UnknownDistanceMeasureException;
import de.clusteval.data.goldstandard.IGoldStandard;
import de.clusteval.data.goldstandard.IGoldStandardConfig;
import de.clusteval.data.goldstandard.format.GoldStandardFormat;
import de.clusteval.data.preprocessing.IDataPreprocessor;
import de.clusteval.data.randomizer.IDataRandomizer;
import de.clusteval.data.statistics.IDataStatistic;
import de.clusteval.data.statistics.IDataStatisticCalculator;
import de.clusteval.framework.RLibraryNotLoadedException;
import de.clusteval.framework.repository.config.DefaultRepositoryConfig;
import de.clusteval.framework.repository.config.IRepositoryConfig;
import de.clusteval.framework.repository.config.RepositoryConfig;
import de.clusteval.framework.repository.config.RepositoryConfigNotFoundException;
import de.clusteval.framework.repository.config.RepositoryConfigurationException;
import de.clusteval.framework.repository.db.DatabaseConnectException;
import de.clusteval.framework.repository.db.DatabaseException;
import de.clusteval.framework.repository.db.DatabaseInitializationException;
import de.clusteval.framework.repository.db.DefaultSQLCommunicator;
import de.clusteval.framework.repository.db.ISQLCommunicator;
import de.clusteval.framework.repository.db.RunResultSQLCommunicator;
import de.clusteval.framework.repository.db.StubSQLCommunicator;
import de.clusteval.framework.repository.maven.IMavenRepositoryResolver;
import de.clusteval.framework.repository.maven.InvalidDynamicComponentNameException;
import de.clusteval.framework.repository.maven.MavenRepositoryResolver;
import de.clusteval.framework.repository.maven.StubRepositoryResolver;
import de.clusteval.framework.repository.migrate.RepositoryMigration1_2;
import de.clusteval.framework.repository.migrate.RepositoryMigration2_3;
import de.clusteval.framework.threading.IRunSchedulerThread;
import de.clusteval.framework.threading.RepositorySupervisorThread;
import de.clusteval.framework.threading.RunResultRepositorySupervisorThread;
import de.clusteval.framework.threading.SupervisorThread;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IStandaloneProgram;
import de.clusteval.program.ProgramConfig;
import de.clusteval.program.r.IRProgram;
import de.clusteval.run.IRun;
import de.clusteval.run.IToolRuntimeEvaluationRun;
import de.clusteval.run.RUN_STATUS;
import de.clusteval.run.ToolRuntimeEvaluationRun;
import de.clusteval.run.runresult.ClusteringRunResult;
import de.clusteval.run.runresult.DataAnalysisRunResult;
import de.clusteval.run.runresult.IRunResult;
import de.clusteval.run.runresult.ParameterOptimizationResult;
import de.clusteval.run.runresult.RunAnalysisRunResult;
import de.clusteval.run.runresult.RunDataAnalysisRunResult;
import de.clusteval.run.runresult.RunResult;
import de.clusteval.run.runresult.ToolRuntimeEvaluationRunResult;
import de.clusteval.run.runresult.format.IRunResultFormat;
import de.clusteval.run.runresult.format.IRunResultFormatParser;
import de.clusteval.run.runresult.postprocessing.IRunResultPostprocessor;
import de.clusteval.run.statistics.IRunDataStatistic;
import de.clusteval.run.statistics.IRunDataStatisticCalculator;
import de.clusteval.run.statistics.IRunStatistic;
import de.clusteval.run.statistics.IRunStatisticCalculator;
import de.clusteval.utils.ClassVersion;
import de.clusteval.utils.ClassVersionRequirement;
import de.clusteval.utils.ClustEvalAlias;
import de.clusteval.utils.DumpException;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.DynamicComponentMissingVersionException;
import de.clusteval.utils.DynamicComponentVersion;
import de.clusteval.utils.ErrorSerializableWrapper;
import de.clusteval.utils.IErrorSerializableWrapper;
import de.clusteval.utils.IFinder;
import de.clusteval.utils.INamedDoubleAttribute;
import de.clusteval.utils.INamedIntegerAttribute;
import de.clusteval.utils.INamedStringAttribute;
import de.clusteval.utils.IncompatibleClustEvalVersionException;
import de.clusteval.utils.InternalAttributeException;
import de.clusteval.utils.InvalidClustEvalVersionException;
import de.clusteval.utils.InvalidDependencyTargetException;
import de.clusteval.utils.MissingClustEvalVersionException;
import dk.sdu.imada.compbio.file.FileUtils;
import dk.sdu.imada.compbio.utils.Pair;

/**
 * The repository is the central object of the backend, where objects are
 * registered and centrally controlled. Objects can be registered and
 * unregistered and get certain functions for free. For example duplication
 * recognition, automatic detection of changes of objects and informing other
 * objects (as listeners) about changes of other objects.
 * 
 * <p>
 * General hint: This class contains a lot of hashmaps for performance reasons.
 * All the hashmaps of this class are updated with current objects. The maps
 * then contain old key objects and current value objects. Therefore you should
 * never iterate over the result of keySet() of the maps, but instead use
 * values().
 * 
 * @author Christian Wiwie
 * 
 */
public class Repository implements IRepository {

	public static final ComparableVersion SUPPORTED_REPOSITORY_VERSION = new ComparableVersion(
			"3");
	public static boolean MIGRATE_REPOSITORY = false;
	public static boolean AWAIT_INITIALIZED_DATABASE_SCHEMA = false;
	public static boolean AWAIT_STARTED_DATABASE_SERVER = false;
	public static boolean DEBUG_NO_NEW_CLUSTERINGS = false;

	/**
	 * A map containing all repository objects. This includes this repository
	 * but also all run result repositories or other child repositories, that
	 * are contained within this repository.
	 */
	protected static Map<String, IRepository> repositories = new HashMap<String, IRepository>();

	/**
	 * This method returns a repository (if available) with the given root path.
	 * 
	 * @param absFilePath
	 *            The absolute root path of the repository.
	 * @return The repository with the given root path.
	 */
	public static IRepository getRepositoryForExactPath(
			final String absFilePath) {
		return Repository.repositories.get(absFilePath);
	}

	/**
	 * This method returns the lowest repository in repository-hierarchy, that
	 * contains the given path. That means, if there are several nested
	 * repositories for the given path, this method will return the lowest one
	 * of the hierarchy.
	 * 
	 * @param absFilePath
	 *            The absolute file path we want to find the repository for.
	 * @return The repository for the given path, which is lowest in the
	 *         repository-hierarchy.
	 * @throws NoRepositoryFoundException
	 */
	public static IRepository getRepositoryForPath(final String absFilePath)
			throws NoRepositoryFoundException {
		String resultPath = null;
		for (String repoPath : new HashSet<String>(
				Repository.repositories.keySet()))
			if (absFilePath.startsWith(
					repoPath + System.getProperty("file.separator")))
				if (resultPath == null
						|| repoPath.length() > resultPath.length())
					resultPath = repoPath;
		if (resultPath == null)
			throw new NoRepositoryFoundException(absFilePath);
		return Repository.repositories.get(resultPath);
	}

	/**
	 * This method checks, whether the given string represents an internal
	 * attribute placeholder, that means it follows the format of
	 * {@value #internalAttributePattern}.
	 * 
	 * @param value
	 *            The string to check whether it is a internal attribute.
	 * @return True, if the given string is an internal attribute, false
	 *         otherwise.
	 */
	public static boolean isInternalAttribute(final String value) {
		Pattern p = internalAttributePattern;
		return p.matcher(value).matches();
	}

	/**
	 * Register a new repository.
	 * 
	 * @param repository
	 *            The new repository to register.
	 * @return The old repository, if the new repository replaced an old one
	 *         with equal root path. Null otherwise.
	 * @throws RepositoryAlreadyExistsException
	 * @throws InvalidRepositoryException
	 */
	public static IRepository register(IRepository repository)
			throws RepositoryAlreadyExistsException,
			InvalidRepositoryException {
		IRepository other = null;
		try {
			other = Repository.getRepositoryForPath(repository.getBasePath());
		} catch (NoRepositoryFoundException e) {
		}
		if (other == null)
			return Repository.repositories.put(repository.getBasePath(),
					repository);
		if (other.getBasePath().equals(repository.getBasePath()))
			throw new RepositoryAlreadyExistsException(other.getBasePath());
		if (repository.getParent() == null
				|| !repository.getParent().equals(other))
			throw new InvalidRepositoryException(
					"Repositories must not be nested without parental relationship");
		return Repository.repositories.put(repository.getBasePath(),
				repository);
	}

	/**
	 * Unregister the given repository.
	 * 
	 * @param repository
	 *            The repository to remove.
	 * @return The removed repository. If null, the given repository was not
	 *         registered.
	 */
	public static IRepository unregister(IRepository repository) {
		return Repository.repositories.remove(repository.getBasePath());
	}

	/**
	 * In case the backend is connected to a mysql database in the frontend,
	 * this attribute is set to a sql communicator, which updates the database
	 * after changes of repository objects (removal, addition).
	 */
	protected ISQLCommunicator sqlCommunicator;

	/**
	 * The supervisor thread is responsible for starting and keeping alive all
	 * threads that check the repository on the filesystem for changes.
	 */
	protected SupervisorThread supervisorThread;

	/**
	 * A repository can have a parent repository, which means, that the root
	 * folder of this repository is located inside the parent repository.
	 * 
	 * <p>
	 * As a consequence if a child repository cannot complete a lookup operation
	 * sucessfully, that means cannot find a certain object, it will also look
	 * for this object in the parent repository.
	 * 
	 * <p>
	 * This relationship is only allowed (located inside a subfolder), if the
	 * parental relationship is indicated by setting this parent repository
	 * attribute.
	 */
	protected Repository parent;

	/**
	 * The absolute path of the root of this repository.
	 */
	protected String basePath;

	/**
	 * The absolute path to the directory within this repository, where all
	 * generators are stored.
	 */
	protected String generatorBasePath;

	/**
	 * The absolute path to the directory within this repository, where all
	 * randomizers are stored.
	 */
	protected String randomizerBasePath;

	/**
	 * The absolute path to the directory, where for a certain runresult
	 * (identified by its unique run identifier) all analysis results are
	 * stored.
	 */
	protected String analysisResultsBasePath;

	/**
	 * The absolute path to the directory within this repository, where all
	 * supplementary materials are stored.
	 * 
	 * <p>
	 * Supplementary materials contain e.g. jar files of parameter optimization
	 * methods or clustering quality measures.
	 */
	protected String supplementaryBasePath;

	/**
	 * The absolute path to the directory within this repository, where all
	 * supplementary materials related to clustering are stored.
	 */
	protected String suppClusteringBasePath;

	/**
	 * The absolute path to the directory within this repository, where all type
	 * jars are stored.
	 */
	protected String typesBasePath;

	/**
	 * The absolute path to the directory within this repository, where all
	 * format jars are stored, e.g. dataset formats.
	 */
	protected String formatsBasePath;

	/**
	 * This map contains the absolute path of every repository object registered
	 * in this repository and maps it to the object itself.
	 */
	protected Map<File, IRepositoryObject> pathToRepositoryObject;

	protected StaticRepositoryEntityMap staticRepositoryEntities;

	protected DynamicRepositoryObjectCollectionMap dynamicRepositoryEntities;

	/**
	 * A map containing all goldstandard formats registered in this repository.
	 */
	protected Map<GoldStandardFormat, GoldStandardFormat> goldStandardFormats;

	/**
	 * The pattern that is used to scan a string ofr internal attribute
	 * placeholders in {@link #isInternalAttribute(String)}.
	 */
	protected static Pattern internalAttributePattern = Pattern
			.compile("\\$\\([^\\$)]*\\)");

	/**
	 * This map holds all available internal float attributes, which can be used
	 * by any kind of configuration file as a option value, which is not
	 * available before starting of a run.
	 */
	protected Map<String, INamedDoubleAttribute> internalDoubleAttributes;

	/**
	 * This map holds all available internal string attributes, which can be
	 * used by any kind of configuration file as a option value, which is not
	 * available before starting of a run.
	 */
	protected Map<String, INamedStringAttribute> internalStringAttributes;

	/**
	 * This map holds all available internal integer attributes, which can be
	 * used by any kind of configuration file as a option value, which is not
	 * available before starting of a run.
	 */
	protected Map<String, INamedIntegerAttribute> internalIntegerAttributes;

	protected Logger log;

	/**
	 * The configuration of this repository holds options that can specify the
	 * behaviour of this repository. For example it can be specified, whether
	 * the repository should communicate and insert its information into a sql
	 * database.
	 */
	protected IRepositoryConfig repositoryConfig;

	/**
	 * This attribute maps the names of a class to all exceptions of required R
	 * libraries that could not be loaded.
	 */
	protected Map<String, Set<RLibraryNotLoadedException>> missingRLibraries;

	/**
	 * All exceptions thrown during parsing of finder instances are being
	 * inserted into this map. New exceptions with messages equal to messages of
	 * exceptions in this list will not be thrown again.
	 */
	protected Map<Class<? extends IRepositoryObject>, Map<ISerializableWrapperRepositoryObject<? extends IRepositoryObject>, List<Throwable>>> knownFinderExceptions;

	/**
	 * The class loaders used by the finders to load classes dynamically.
	 */
	protected Map<URL, URLClassLoader> finderClassLoaders;

	protected Map<Class<? extends IRepositoryObject>, Map<ISerializableWrapperRepositoryObject<IRepositoryObject>, IRepositoryObject>> finderErrorObjects;

	/**
	 * A map containing dependencies between jar files that are loaded
	 * dynamically.
	 */
	protected Map<String, List<File>> finderWaitingFiles;

	/**
	 * The change dates of the jar files that were loaded dynamically by jar
	 * finder instances.
	 */
	protected Map<String, Long> finderLoadedJarFileChangeDates;

	protected Map<String, Map<ComparableVersion, Class<? extends IRepositoryObjectDynamicComponent>>> loadedClasses;

	protected Map<Class<? extends IRepositoryObject>, Map<String, Set<ComparableVersion>>> fullClassNamesAndVersionsForPresentFiles;

	private Map<Thread, MyRengine> rEngines;

	protected IMavenRepositoryResolver resolver;
	protected Map<IProgramConfig, double[]> toolRuntimePolynomialFits;

	/**
	 * Instantiates a new repository.
	 * 
	 * @param parent
	 *            Can be null, if this repository has no parent repository.
	 * @param basePath
	 *            The absolute path of the root of this repository.
	 * @throws FileNotFoundException
	 * @throws InvalidRepositoryException
	 * @throws RepositoryAlreadyExistsException
	 * @throws RepositoryConfigurationException
	 * @throws RepositoryConfigNotFoundException
	 * @throws DatabaseConnectException
	 * @throws RepositoryVersionTooNewException
	 * @throws RepositoryVersionTooOldException
	 * @throws RepositoryCouldNotBeMigratedException
	 */
	public Repository(final String basePath, final IRepository parent)
			throws FileNotFoundException, RepositoryAlreadyExistsException,
			InvalidRepositoryException, RepositoryConfigNotFoundException,
			RepositoryConfigurationException, DatabaseConnectException,
			RepositoryVersionTooOldException, RepositoryVersionTooNewException,
			RepositoryCouldNotBeMigratedException {
		this(basePath, parent, null);
	}

	/**
	 * Instantiates a new repository.
	 * 
	 * @param parent
	 *            Can be null, if this repository has no parent repository.
	 * @param basePath
	 *            The absolute path of the root of this repository.
	 * @param overrideConfig
	 *            Set this parameter != null, if you want to override the
	 *            repository.config file.
	 * @throws FileNotFoundException
	 * @throws InvalidRepositoryException
	 * @throws RepositoryAlreadyExistsException
	 * @throws RepositoryConfigurationException
	 * @throws RepositoryConfigNotFoundException
	 * @throws RepositoryVersionTooOldException
	 * @throws RepositoryVersionTooNewException
	 * @throws RepositoryCouldNotBeMigratedException
	 */
	public Repository(final String basePath, final IRepository parent,
			final IRepositoryConfig overrideConfig)
			throws FileNotFoundException, RepositoryAlreadyExistsException,
			InvalidRepositoryException, RepositoryConfigNotFoundException,
			RepositoryConfigurationException, RepositoryVersionTooOldException,
			RepositoryVersionTooNewException,
			RepositoryCouldNotBeMigratedException {
		super();

		this.log = LoggerFactory.getLogger(this.getClass());

		this.basePath = basePath;
		// remove trailing file separator
		if (this.basePath.length() > 1
				&& this.basePath.endsWith(System.getProperty("file.separator")))
			this.basePath = this.basePath.substring(0, this.basePath.length()
					- System.getProperty("file.separator").length());
		this.parent = (Repository) parent;
		this.missingRLibraries = new ConcurrentHashMap<String, Set<RLibraryNotLoadedException>>();

		this.initializePaths();

		this.initAttributes();

		this.ensureFolderStructure();

		this.pathToRepositoryObject = new ConcurrentHashMap<File, IRepositoryObject>();

		File repositoryConfigFile = new File(
				FileUtils.buildPath(this.basePath, "repository.config"));

		Repository.register(this);

		if (overrideConfig != null)
			this.repositoryConfig = overrideConfig;
		else if (repositoryConfigFile.exists()) {
			/*
			 * Parsing the configuration file (if it exists)
			 */
			this.repositoryConfig = RepositoryConfig
					.parseFromFile(repositoryConfigFile);
			this.log.debug(
					"Using repository configuration: " + repositoryConfigFile);
		} else {
			this.repositoryConfig = new DefaultRepositoryConfig(
					repositoryConfigFile);
			this.log.debug("Using default repository configuration");
		}

		if (this.repositoryConfig.getMavenConfig().useMaven())
			this.resolver = new MavenRepositoryResolver(this);
		else
			this.resolver = new StubRepositoryResolver(this);

		// try {
		// this.rEngineForLibraryInstalledChecks = new REnginePool(5);
		// } catch (RserveException e) {
		// // if there is no R, we will not be using this field in the future
		// this.rEngineForLibraryInstalledChecks = null;
		// this.rEngineException = e;
		// }
		this.rEngines = new HashMap<Thread, MyRengine>();

		if (!(this instanceof RunResultRepository)) {
			// check compatibility of the repository with this server version
			int versionComparison = this.getVersion()
					.compareTo(SUPPORTED_REPOSITORY_VERSION);
			if (versionComparison < 0) {
				if (Repository.MIGRATE_REPOSITORY) {
					this.log.info(String.format(
							"The repository version (%s) is too old for this version of ClustEval and needs to be migrated to version %s.",
							this.getVersion(), SUPPORTED_REPOSITORY_VERSION));
					if (!Repository.migrateToTargetVersion(this,
							this.getVersion(), SUPPORTED_REPOSITORY_VERSION))
						throw new RepositoryCouldNotBeMigratedException(
								this.getVersion(),
								SUPPORTED_REPOSITORY_VERSION);
				} else
					throw new RepositoryVersionTooOldException(
							this.getVersion(), SUPPORTED_REPOSITORY_VERSION);
			}
			if (versionComparison > 0)
				throw new RepositoryVersionTooNewException(this.getVersion(),
						SUPPORTED_REPOSITORY_VERSION);
		}
	}

	protected static boolean migrateToTargetVersion(
			final IRepository repository, ComparableVersion version,
			ComparableVersion targetVersion) {
		int currentRepositoryVersion = Integer.valueOf(version.toString());
		int targetRepositoryVersion = Integer.valueOf(targetVersion.toString());

		for (int i = currentRepositoryVersion; i < targetRepositoryVersion; i++) {
			if (!migrateToNextVersion(repository, i))
				return false;
		}
		return true;
	}

	protected static boolean migrateToNextVersion(final IRepository repository,
			int currentVersion) {
		Logger log = LoggerFactory.getLogger(Repository.class);
		log.info(String.format("Migrating repository '%s': %d -> %d",
				repository, currentVersion, currentVersion + 1));

		switch (currentVersion) {
			case 1 :
				return new RepositoryMigration1_2().apply(repository);
			case 2 :
				return new RepositoryMigration2_3().apply(repository);
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#getMavenResolver()
	 */
	@Override
	public IMavenRepositoryResolver getMavenResolver() {
		return resolver;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#addMissingRLibraryException
	 * (de.clusteval.framework.RLibraryNotLoadedException)
	 */
	@Override
	public boolean addMissingRLibraryException(RLibraryNotLoadedException e) {
		if (!(this.missingRLibraries.containsKey(e.getClassName())))
			this.missingRLibraries.put(e.getClassName(),
					Collections.synchronizedSet(
							new HashSet<RLibraryNotLoadedException>()));
		return this.missingRLibraries.get(e.getClassName()).add(e);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#clearMissingRLibraries(java
	 * .lang.String)
	 */
	@Override
	public Set<RLibraryNotLoadedException> clearMissingRLibraries(
			String className) {
		return this.missingRLibraries.remove(className);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#commitDB()
	 */
	@Override
	public void commitDB() {
		synchronized (this.sqlCommunicator) {
			this.sqlCommunicator.commitDB();
		}
	}

	/**
	 * This method creates a sql communicator for this repository depending on
	 * the fact, whether mysql should be used by this repository.
	 * 
	 * <p>
	 * Override this method in subclasses, if you want to change the type of sql
	 * communicator for your subtype. You can see an example in
	 * {@link RunResultRepository#createSQLCommunicator()}, where instead of
	 * {@link DefaultSQLCommunicator} a {@link RunResultSQLCommunicator} is
	 * created.
	 * 
	 * @return A new instance of sql communicator.
	 * @throws DatabaseConnectException
	 * @throws DatabaseInitializationException
	 */
	protected ISQLCommunicator createSQLCommunicator()
			throws DatabaseConnectException, DatabaseInitializationException {
		if (this.repositoryConfig.getSQLConfig().usesSql())
			return new DefaultSQLCommunicator(this,
					this.repositoryConfig.getSQLConfig());

		return new StubSQLCommunicator(this);
	}

	/**
	 * This method creates the supervisor thread object for this repository.
	 * 
	 * <p>
	 * Override this method in subclasses, if you want to change the type of
	 * supervisor thread for your subtype. You can see an example in
	 * {@link RunResultRepository#createSupervisorThread()}, where instead of a
	 * {@link RepositorySupervisorThread} a
	 * {@link RunResultRepositorySupervisorThread} is created.
	 * 
	 * @return
	 */
	protected SupervisorThread createSupervisorThread() {
		return new RepositorySupervisorThread(this,
				this.repositoryConfig.getThreadSleepTimes(), false);
	}

	/**
	 * Helper method of {@link #ensureFolderStructure()}, which ensures that a
	 * single folder exists.
	 * 
	 * @param absFolderPath
	 *            The absolute path of the folder to ensure.
	 * @return true, if successful
	 * @throws FileNotFoundException
	 */
	private boolean ensureFolder(final String absFolderPath)
			throws FileNotFoundException {
		final File folder = new File(absFolderPath);
		if (!(folder.exists())) {
			folder.mkdirs();
			this.info("Recreating repository folder " + folder);
		}
		if (!(folder.exists()))
			throw new FileNotFoundException(
					"Could not create folder " + folder.getAbsolutePath());
		return true;
	}

	/**
	 * This method ensures the complete folder structure of this repository. If
	 * a folder does not exist, it is recreated. In case a folder creation is
	 * not successful an exception is thrown.
	 * 
	 * <p>
	 * A helper method of
	 * {@link #Repository(String, Repository, long, long, long, long, long, long, long)}
	 * .
	 * 
	 * @return true, if successful
	 * @throws FileNotFoundException
	 */
	private boolean ensureFolderStructure() throws FileNotFoundException {
		boolean newRepository = !new File(this.basePath).exists();
		this.ensureFolder(this.basePath);
		if (newRepository) {
			// create new repository config with compatible version
			try {
				String configFile = FileUtils.buildPath(this.basePath,
						"repository.config");
				RepositoryConfig config = new DefaultRepositoryConfig(
						new File(configFile));
				config.dumpToFile();
			} catch (DumpException e) {
				e.printStackTrace();
			}

		}

		// TODO: replace by for loop over entries of #repositoryObjectEntities
		this.ensureFolder(this.getBasePath(IDataConfig.class));
		this.ensureFolder(this.getBasePath(IDataSet.class));
		this.ensureFolder(this.getBasePath(IDataSetFormat.class));
		this.ensureFolder(this.getBasePath(IDataSetType.class));
		this.ensureFolder(this.getBasePath(IDataSetConfig.class));
		this.ensureFolder(this.getBasePath(IGoldStandard.class));
		this.ensureFolder(this.getBasePath(IGoldStandardConfig.class));
		this.ensureFolder(this.getBasePath(IStandaloneProgram.class));
		this.ensureFolder(this.getBasePath(IProgramConfig.class));
		this.ensureFolder(this.getBasePath(IRun.class));
		this.ensureFolder(this.getBasePath(IRunResult.class));
		this.ensureFolder(this.getBasePath(IRunResultFormat.class));
		this.ensureFolder(this.supplementaryBasePath);
		this.ensureFolder(this.suppClusteringBasePath);
		this.ensureFolder(this.getBasePath(IContext.class));
		this.ensureFolder(this.getBasePath(IParameterOptimizationMethod.class));
		this.ensureFolder(this.getBasePath(IDataStatistic.class));
		this.ensureFolder(this.getBasePath(IRunStatistic.class));
		this.ensureFolder(this.getBasePath(IRunDataStatistic.class));
		this.ensureFolder(this.getBasePath(IDistanceMeasure.class));
		this.ensureFolder(this.generatorBasePath);
		this.ensureFolder(this.getBasePath(IDataSetGenerator.class));
		this.ensureFolder(this.getBasePath(IDataRandomizer.class));
		this.ensureFolder(this.getBasePath(IDataPreprocessor.class));

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Repository))
			return false;

		Repository repo = (Repository) obj;

		return this.basePath.equals(repo.basePath);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#evaluateInternalAttributes(
	 * java.lang.String, de.clusteval.data.DataConfig,
	 * de.clusteval.program.ProgramConfig)
	 */
	@Override
	public String evaluateInternalAttributes(final String old,
			final IDataConfig dataConfig, final IProgramConfig programConfig)
			throws InternalAttributeException {

		final String extended = extendInternalAttributes(old, dataConfig,
				programConfig);

		StringBuilder result = new StringBuilder(extended);
		int pos = -1;
		while ((pos = result.indexOf("$(")) != -1) {
			int endPos = result.indexOf(")", pos + 1);

			String attributeName = result.substring(pos + 2, endPos);
			String replaceValue;
			if (this.internalDoubleAttributes.containsKey(attributeName)) {
				replaceValue = this.internalDoubleAttributes.get(attributeName)
						+ "";
			} else if (this.internalIntegerAttributes
					.containsKey(attributeName)) {
				replaceValue = this.internalIntegerAttributes.get(attributeName)
						+ "";
			} else if (this.internalStringAttributes
					.containsKey(attributeName)) {
				replaceValue = this.internalStringAttributes.get(attributeName)
						+ "";
			} else {
				throw new InternalAttributeException("The internal attribute "
						+ attributeName + " does not exist.");
			}
			result.replace(pos, endPos + 1, replaceValue);
		}

		return result.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#evaluateJavaScript(java.
	 * lang.String)
	 */
	@Override
	public String evaluateJavaScript(final String script)
			throws ScriptException {
		ScriptEngineManager mgr = new ScriptEngineManager();
		ScriptEngine engine = mgr.getEngineByName("JavaScript");
		// define min function
		engine.eval("function min(n1,n2) {" + "if(n1 <= n2) return n1; "
				+ "  else return n2; " + "};");
		// define max function
		engine.eval("function max(n1,n2) {" + "if(n1 >= n2) return n1; "
				+ "  else return n2; " + "};");
		return engine.eval(script) + "";
	}

	/**
	 * This method prefixes placeholder of internal attributes with their data
	 * configuration such that they can be replaced in an unambigious way later
	 * in {@link Repository#evaluateInternalAttributes(String)}.
	 * 
	 * <p>
	 * A helper method for
	 * {@link #evaluateInternalAttributes(String, DataConfig, ProgramConfig)}.
	 * 
	 * @param old
	 *            The parameter value that might contain placeholders which need
	 *            to be extended.
	 * @param dataConfig
	 *            The data configuration which might be needed to extend the
	 *            placeholders.
	 * @param programConfig
	 *            The program configuration which might be needed to extend the
	 *            placeholders.
	 * @return The parameter value with extended placeholders.
	 */
	@SuppressWarnings("unused")
	private String extendInternalAttributes(final String old,
			final de.clusteval.data.IDataConfig dataConfig,
			final IProgramConfig programConfig) {
		String result = old.replaceAll("\\$\\(minSimilarity\\)",
				"\\$\\(" + dataConfig.getDatasetConfig().getDataSet()
						.getOriginalDataSet().getAbsolutePath()
						+ ":minSimilarity\\)");
		result = result.replaceAll("\\$\\(maxSimilarity\\)",
				"\\$\\(" + dataConfig.getDatasetConfig().getDataSet()
						.getOriginalDataSet().getAbsolutePath()
						+ ":maxSimilarity\\)");
		result = result.replaceAll("\\$\\(meanSimilarity\\)",
				"\\$\\(" + dataConfig.getDatasetConfig().getDataSet()
						.getOriginalDataSet().getAbsolutePath()
						+ ":meanSimilarity\\)");
		result = result.replaceAll("\\$\\(numberOfElements\\)",
				"\\$\\(" + dataConfig.getDatasetConfig().getDataSet()
						.getOriginalDataSet().getAbsolutePath()
						+ ":numberOfElements\\)");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#terminateSupervisorThread()
	 */
	@Override
	public void terminateSupervisorThread() throws InterruptedException {
		this.terminateSupervisorThread(false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#terminateSupervisorThread(
	 * boolean)
	 */
	@Override
	public void terminateSupervisorThread(final boolean closeRengines)
			throws InterruptedException {
		if (closeRengines) {
			// close Rengine pool
			// this.rEngineForLibraryInstalledChecks.close();
			for (MyRengine rEngine : this.rEngines.values()) {
				rEngine.close();
				// rEngine.shutdown();
			}
			this.rEngines.clear();
		}

		// terminate supervisor thread
		if (this.supervisorThread == null)
			return;
		this.supervisorThread.interrupt();
		this.supervisorThread.join();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#getBasePath()
	 */
	@Override
	public String getBasePath() {
		return this.basePath;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#getBasePath(java.lang.
	 * Class)
	 */
	@Override
	public String getBasePath(final Class<? extends IRepositoryObject> c) {
		if (this.staticRepositoryEntities.containsKey(c))
			return this.staticRepositoryEntities.get(c).getBasePath();
		return this.dynamicRepositoryEntities.get(c).getBasePath();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#getCollectionStaticEntities
	 * (java.lang.Class)
	 */
	@Override
	public <R extends IRepositoryObject> Collection<R> getCollectionStaticEntities(
			final Class<R> c) {
		return (Collection) this.staticRepositoryEntities.get(c).asCollection();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#
	 * getStaticEntitiesWithVersions(java.lang.Class)
	 */
	@Override
	public <R extends IRepositoryObject> Map<String, Map<ComparableVersion, R>> getStaticEntitiesWithVersions(
			Class<R> c) {
		return (Map) this.staticRepositoryEntities.get(c).asMapWithVersions();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#getStaticObjectWithName(
	 * java.lang.Class, java.lang.String)
	 */
	@Override
	public <R extends IRepositoryObject> R getStaticObjectWithNameAndVersion(
			final Class<R> c, final String name)
			throws ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException {
		return this.getStaticObjectWithNameAndVersion(c, name, true);
	}

	@Override
	public <R extends IRepositoryObject> R getStaticObjectWithNameAndVersion(
			final Class<R> c, final String name, final boolean tryToResolve)
			throws ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException {
		try {
			return (R) this.staticRepositoryEntities.get(c).findByString(name,
					tryToResolve);
		} catch (ObjectNotFoundException e) {
			throw new ObjectNotRegisteredException(c, name);
		} catch (ObjectVersionNotFoundException e) {
			throw new ObjectVersionNotRegisteredException(c, name,
					e.getVersion());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#isInitialized(java.lang.
	 * Class)
	 */
	@Override
	public boolean isInitialized(final Class<? extends IRepositoryObject> c) {
		if (this.staticRepositoryEntities.containsKey(c))
			return this.staticRepositoryEntities.get(c).isInitialized();
		return this.dynamicRepositoryEntities.get(c).isInitialized();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#getRegisteredObject(de.
	 * clusteval.framework.repository.IRepositoryObject)
	 */
	@Override
	public IRepositoryObject getRegisteredObject(
			final IRepositoryObject object) {
		return this.getRegisteredObject(object, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#getRegisteredObject(de.
	 * clusteval.framework.repository.IRepositoryObject, boolean)
	 */
	@Override
	public IRepositoryObject getRegisteredObject(final IRepositoryObject object,
			final boolean ignoreChangeDate) {
		@SuppressWarnings("unchecked")
		Class<? extends IRepositoryObject> c = object.getClass();
		return this.getRegisteredObject(c, object, ignoreChangeDate);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#getRegisteredObject(java.
	 * lang.Class, de.clusteval.framework.repository.IRepositoryObject, boolean)
	 */
	@Override
	public IRepositoryObject getRegisteredObject(
			final Class<? extends IRepositoryObject> c,
			final IRepositoryObject object, final boolean ignoreChangeDate) {
		boolean staticEntityFound = false;
		boolean dynamicEntityFound = false;
		if (!((staticEntityFound = this.staticRepositoryEntities.containsKey(c))
				|| (dynamicEntityFound = this.dynamicRepositoryEntities
						.containsKey(c)))) {
			for (Class<?> i : c.getInterfaces())
				if (IRepositoryObject.class.isAssignableFrom(i))
					return this.getRegisteredObject(
							(Class<? extends IRepositoryObject>) i, object,
							ignoreChangeDate);
			if (c.getSuperclass() != null)
				if (IRepositoryObject.class.isAssignableFrom(c.getSuperclass()))
					return this
							.getRegisteredObject(
									(Class<? extends IRepositoryObject>) c
											.getSuperclass(),
									object, ignoreChangeDate);
		}
		if (staticEntityFound)
			return this.staticRepositoryEntities.get(c)
					.getRegisteredObject(object, ignoreChangeDate);
		else if (dynamicEntityFound)
			return this.dynamicRepositoryEntities.get(c).getRegisteredObject(
					(IRepositoryObjectDynamicComponent) object,
					ignoreChangeDate);
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#unregister(S)
	 */
	@Override
	public <T extends IRepositoryObject, S extends T> boolean unregister(
			final S object) {
		@SuppressWarnings("unchecked")
		Class<S> c = (Class<S>) object.getClass();
		return this.unregister(c, object);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#unregister(java.lang.Class,
	 * O)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public <S extends IRepositoryObject, T extends S, O extends T> boolean unregister(
			final Class<T> c, final O object) {
		boolean staticEntityFound = false;
		boolean dynamicEntityFound = false;
		if (!((staticEntityFound = this.staticRepositoryEntities.containsKey(c))
				|| (dynamicEntityFound = this.dynamicRepositoryEntities
						.containsKey(c)))) {
			for (Class<?> i : c.getInterfaces())
				if (IRepositoryObject.class.isAssignableFrom(i))
					return this.unregister((Class<S>) i, object);
			if (c.getSuperclass() != null)
				if (IRepositoryObject.class.isAssignableFrom(c.getSuperclass()))
					return this.unregister((Class<S>) c.getSuperclass(),
							object);
		}

		if (staticEntityFound)
			return this.staticRepositoryEntities.get(c).unregister(object);
		else if (dynamicEntityFound)
			return this.dynamicRepositoryEntities.get(c).unregister(object);
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#register(S)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public <T extends IRepositoryObject, S extends T> boolean register(
			final S object) throws RegisterException {
		Class<S> c = (Class<S>) object.getClass();
		return this.register(c, object);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#register(java.lang.Class,
	 * O)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public <S extends IRepositoryObject, T extends S, O extends T> boolean register(
			final Class<T> c, final O object) throws RegisterException {
		boolean staticEntityFound = false;
		boolean dynamicEntityFound = false;
		if (!((staticEntityFound = this.staticRepositoryEntities.containsKey(c))
				|| (dynamicEntityFound = this.dynamicRepositoryEntities
						.containsKey(c)))) {
			for (Class<?> i : c.getInterfaces())
				if (IRepositoryObject.class.isAssignableFrom(i))
					// we only return, if we found the right class
					return this.register((Class<S>) i, object);
			if (c.getSuperclass() != null)
				if (IRepositoryObject.class.isAssignableFrom(c.getSuperclass()))
					return this.register((Class<S>) c.getSuperclass(), object);
		}

		if (staticEntityFound)
			return this.staticRepositoryEntities.get(c).register(object);
		else if (dynamicEntityFound)
			return this.dynamicRepositoryEntities.get(c).register(object);
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#setInitialized(java.lang.
	 * Class)
	 */
	@Override
	public <T extends IRepositoryObject> void setInitialized(final Class<T> c) {
		if (this.staticRepositoryEntities.containsKey(c))
			this.staticRepositoryEntities.get(c).setInitialized();
		else
			this.dynamicRepositoryEntities.get(c).setInitialized();
	}

	/**
	 * @param classToCheck
	 * @throws IncompatibleClustEvalVersionException
	 * @throws MissingClustEvalVersionException
	 * @throws InvalidClustEvalVersionException
	 * @throws InvalidDependencyTargetException
	 */
	public void ensureClassCompatibleWithClustEvalInterfaceVersion(
			final Class<?> classToCheck)
			throws IncompatibleClustEvalVersionException,
			MissingClustEvalVersionException, InvalidClustEvalVersionException,
			InvalidDependencyTargetException {
		ClassVersionRequirement anno = classToCheck
				.getAnnotation(ClassVersionRequirement.class);
		if (anno == null)
			throw new MissingClustEvalVersionException(
					classToCheck.getSimpleName());

		String classVersion = anno.versionSpecification();
		if (!(classVersion.startsWith("[")) && !(classVersion.startsWith("(")))
			classVersion = "[" + classVersion;
		if (!(classVersion.endsWith("]")) && !(classVersion.endsWith(")")))
			classVersion = classVersion + "]";
		final Class<?> interfaceClass = anno.target();
		try {
			VersionRange versionSpec = VersionRange
					.createFromVersionSpec(classVersion);
			ClassVersion annotation = interfaceClass
					.getAnnotation(ClassVersion.class);
			if (annotation == null)
				throw new InvalidDependencyTargetException(
						classToCheck.getSimpleName(), interfaceClass);
			ArtifactVersion serverVersion = new DefaultArtifactVersion(
					annotation.version());

			boolean containsVersion = versionSpec
					.containsVersion(serverVersion);
			if (!containsVersion)
				throw new IncompatibleClustEvalVersionException(
						classToCheck.getSimpleName(), versionSpec,
						interfaceClass, serverVersion.toString());
		} catch (IncompatibleClustEvalVersionException e) {
			throw e;
		} catch (InvalidVersionSpecificationException e) {
			throw new InvalidClustEvalVersionException(
					classToCheck.getSimpleName(), interfaceClass, classVersion);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#isClassRegistered(java.lang
	 * .Class)
	 */
	@Override
	public <T extends IRepositoryObject> boolean isClassRegistered(
			final Class<T> c) throws DynamicComponentMissingVersionException {
		return this.isClassRegistered(c.getName(), getVersionOfDynamicClass(c));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#isClassRegistered(java.lang
	 * .String)
	 */
	@Override
	public <T extends IRepositoryObject> boolean isClassRegistered(
			final String classFullName) {
		if (classFullName.contains(":")) {
			String[] split = classFullName.split(":");
			return this.isClassRegistered(split[0],
					new ComparableVersion(split[1]));
		}
		return this.isClassAvailable(classFullName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#isClassRegistered(java.lang
	 * .String, org.apache.maven.artifact.versioning.ComparableVersion)
	 */
	@Override
	public <T extends IRepositoryObject> boolean isClassRegistered(
			final String classFullName, final ComparableVersion version) {
		return this.isClassAvailable(classFullName, version);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#isClassRegistered(java.lang
	 * .Class, java.lang.String)
	 */
	@Override
	public <T extends IRepositoryObject> boolean isClassRegistered(
			final Class<T> base, final String classSimpleName) {
		if (classSimpleName.contains(":")) {
			String[] split = classSimpleName.split(":");
			return this.isClassRegistered(base, split[0],
					new ComparableVersion(split[1]));
		}
		if (!this.dynamicRepositoryEntities.containsKey(base)) {
			for (Class<?> i : base.getInterfaces())
				if (IRepositoryObject.class.isAssignableFrom(i))
					return this.isClassRegistered(
							(Class<? extends IRepositoryObject>) i,
							classSimpleName);
			if (base.getSuperclass() != null)
				if (IRepositoryObject.class
						.isAssignableFrom(base.getSuperclass()))
					return this.isClassRegistered(
							(Class<? extends IRepositoryObject>) base
									.getSuperclass(),
							classSimpleName);
		}
		return this.dynamicRepositoryEntities.get(base)
				.isClassRegistered(classSimpleName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#isClassRegistered(java.lang
	 * .Class, java.lang.String,
	 * org.apache.maven.artifact.versioning.ComparableVersion)
	 */
	@Override
	public <T extends IRepositoryObject> boolean isClassRegistered(
			final Class<T> base, final String classSimpleName,
			final ComparableVersion version) {
		if (!this.dynamicRepositoryEntities.containsKey(base)) {
			for (Class<?> i : base.getInterfaces())
				if (IRepositoryObject.class.isAssignableFrom(i))
					return this.isClassRegistered(
							(Class<? extends IRepositoryObject>) i,
							classSimpleName, version);
			if (base.getSuperclass() != null)
				if (IRepositoryObject.class
						.isAssignableFrom(base.getSuperclass()))
					return this
							.isClassRegistered(
									(Class<? extends IRepositoryObject>) base
											.getSuperclass(),
									classSimpleName, version);
		}
		return this.dynamicRepositoryEntities.get(base)
				.isClassRegistered(classSimpleName, version);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#registerClass(java.lang.
	 * Class, java.lang.Class)
	 */
	@Override
	public <S extends IRepositoryObject, T extends S, SUB extends T> boolean registerClass(
			final Class<T> base, final Class<SUB> c)
			throws DynamicComponentMissingVersionException,
			DynamicComponentInitializationException,
			IncompatibleClustEvalVersionException,
			MissingClustEvalVersionException, InvalidClustEvalVersionException,
			InvalidDependencyTargetException {
		ensureClassCompatibleWithClustEvalInterfaceVersion(c);
		if (!this.dynamicRepositoryEntities.containsKey(base)) {
			for (Class<?> i : base.getInterfaces())
				if (IRepositoryObject.class.isAssignableFrom(i))
					return this.registerClass((Class<S>) i, c);
			if (base.getSuperclass() != null)
				if (IRepositoryObject.class
						.isAssignableFrom(base.getSuperclass()))
					return this.registerClass((Class<S>) base.getSuperclass(),
							c);
		}
		return this.dynamicRepositoryEntities.get(base).registerClass(
				(Class<? extends IRepositoryObjectDynamicComponent>) c);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#unregisterClass(java.lang.
	 * Class)
	 */
	@Override
	public <T extends IRepositoryObject> boolean unregisterClass(
			final Class<T> c) {
		return this.unregisterClass(c, c);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#unregisterClass(java.lang.
	 * Class, java.lang.Class)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public <S extends IRepositoryObject, T extends S, SUB extends T> boolean unregisterClass(
			final Class<T> base, final Class<SUB> c) {
		if (!this.dynamicRepositoryEntities.containsKey(base)) {
			for (Class<?> i : base.getInterfaces())
				if (IRepositoryObject.class.isAssignableFrom(i))
					return this.unregisterClass((Class<S>) i, c);
			if (base.getSuperclass() != null)
				if (IRepositoryObject.class
						.isAssignableFrom(base.getSuperclass()))
					return this.unregisterClass((Class<S>) c.getSuperclass(),
							c);
		}
		return this.dynamicRepositoryEntities.get(base).unregisterClass(
				(Class<? extends IRepositoryObjectDynamicComponent>) c);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#getRegisteredClass(java.
	 * lang.String)
	 */
	@Override
	public Class<? extends IRepositoryObject> getRegisteredClass(
			final String fullClassName) {
		return this.getRegisteredClassWithFullName(fullClassName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#getRegisteredClass(java.
	 * lang.Class, java.lang.String)
	 */
	@Override
	public <T extends IRepositoryObject> Class<? extends T> getRegisteredClass(
			final Class<T> c, final String className) {
		return (Class<? extends T>) this.dynamicRepositoryEntities.get(c)
				.getRegisteredClass(className);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#getRegisteredClass(java.
	 * lang.Class, java.lang.String,
	 * org.apache.maven.artifact.versioning.ComparableVersion)
	 */
	@Override
	public <T extends IRepositoryObject> Class<? extends T> getRegisteredClass(
			final Class<T> c, final String className,
			final ComparableVersion version) {
		return (Class<? extends T>) this.dynamicRepositoryEntities.get(c)
				.getRegisteredClass(className, version);
	}

	@Override
	public <T extends IRepositoryObject> Pair<String, ComparableVersion> getFullClassNameAndVersion(
			final Class<T> baseClass, final String simpleClassName,
			final boolean resolveToNewestIfVersionMissing) {
		String fullClassName;
		ComparableVersion version;
		if (simpleClassName.contains(":")) {
			String[] split = simpleClassName.split(":");
			fullClassName = String.format("%s.%s",
					baseClass.getPackage().getName(), split[0]);
			version = new ComparableVersion(split[1]);
		} else {
			fullClassName = String.format("%s.%s",
					baseClass.getPackage().getName(), simpleClassName);
			version = null;
			if (fullClassName != null && resolveToNewestIfVersionMissing) {
				Map<String, Map<ComparableVersion, Class<? extends IRepositoryObjectDynamicComponent>>> classesWithNameAndVersion = getClassesWithNameAndVersion(
						baseClass);
				if (classesWithNameAndVersion.containsKey(fullClassName)
						&& !classesWithNameAndVersion.get(fullClassName)
								.isEmpty())
					version = Collections.max(classesWithNameAndVersion
							.get(fullClassName).keySet());
			}
		}
		return Pair.getPair(fullClassName, version);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#
	 * resolveAndGetRegisteredClass(java.lang.Class, java.lang.String, boolean)
	 */
	@Override
	public <T extends IRepositoryObject> Class<? extends T> resolveAndGetRegisteredClass(
			final Class<T> baseClass, final String simpleClassName,
			boolean tryMaven) {
		Pair<String, ComparableVersion> fullClassNameAndVersion = getFullClassNameAndVersion(
				baseClass, simpleClassName, false);
		String fullClassName = fullClassNameAndVersion.getFirst();
		ComparableVersion version = fullClassNameAndVersion.getSecond();

		Class<? extends T> c;
		if (version != null) {
			c = this.getRegisteredClass(baseClass, fullClassName, version);
		} else {
			c = this.getRegisteredClass(baseClass, fullClassName);
		}

		if (c == null && tryMaven
				&& this.getFullClassNamesAndVersionsForPresentFiles() != null
				&& this.getFullClassNamesAndVersionsForPresentFiles()
						.get(baseClass) != null) {
			if (version != null)
				tryMaven &= !this.getFullClassNamesAndVersionsForPresentFiles()
						.get(baseClass).containsKey(fullClassName)
						|| !this.getFullClassNamesAndVersionsForPresentFiles()
								.get(baseClass).get(fullClassName)
								.contains(version);
			else
				tryMaven &= !(this.getFullClassNamesAndVersionsForPresentFiles()
						.get(baseClass).containsKey(fullClassName)
						&& this.getFullClassNamesAndVersionsForPresentFiles()
								.get(baseClass).get(fullClassName).size() > 0);

			if (tryMaven) {
				// try to resolve class using maven
				IMavenRepositoryResolver resolver = this.getMavenResolver();
				try {
					if (resolver.isDynamicComponentAvailable(simpleClassName)) {
						resolver.installDynamicComponentIntoRepository(
								simpleClassName);
					}

					// try to load it again after we have successfully loaded
					// the
					// class from maven
					if (simpleClassName.contains(":")) {
						String[] split = simpleClassName.split(":");
						c = this.getRegisteredClass(baseClass,
								String.format("%s.%s",
										baseClass.getPackage().getName(),
										split[0]),
								new ComparableVersion(split[1]));
					} else
						c = this.getRegisteredClass(baseClass,
								String.format("%s.%s",
										baseClass.getPackage().getName(),
										simpleClassName));
				} catch (InvalidDynamicComponentNameException e) {
				}
			}
		}
		return c;
	}

	@Override
	public Map<String, Map<ComparableVersion, Class<? extends IRepositoryObjectDynamicComponent>>> getClassesWithNameAndVersion(
			Class<? extends IRepositoryObject> c) {
		return this.dynamicRepositoryEntities.get(c)
				.getClassesWithNameAndVersion();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#getClasses(java.lang.Class)
	 */
	@Override
	public Collection<Class<? extends IRepositoryObjectDynamicComponent>> getClasses(
			Class<? extends IRepositoryObject> c) {
		return this.dynamicRepositoryEntities.get(c).getClasses();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#getClassesWithVersions(java
	 * .lang.Class)
	 */
	@Override
	public Map<Class<? extends IRepositoryObjectDynamicComponent>, ComparableVersion> getClassesWithVersions(
			Class<? extends IRepositoryObject> c) {
		return this.dynamicRepositoryEntities.get(c).getClassesWithVersions();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#isClassAvailable(java.lang.
	 * String, org.apache.maven.artifact.versioning.ComparableVersion)
	 */
	@Override
	public boolean isClassAvailable(final String fullClassName,
			final ComparableVersion version) {
		return loadedClasses.containsKey(fullClassName)
				&& loadedClasses.get(fullClassName).containsKey(version);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#isClassAvailable(java.lang.
	 * String)
	 */
	@Override
	public boolean isClassAvailable(final String fullClassName) {
		return loadedClasses.containsKey(fullClassName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#
	 * getRegisteredClassWithFullName(java.lang.String, int)
	 */
	@Override
	public Class<? extends IRepositoryObject> getRegisteredClassWithFullName(
			final String fullClassName, final int version) {
		return loadedClasses.containsKey(fullClassName)
				? loadedClasses.get(fullClassName).get(version)
				: null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#
	 * getRegisteredClassWithFullName(java.lang.String)
	 */
	@Override
	public Class<? extends IRepositoryObject> getRegisteredClassWithFullName(
			final String fullClassName) {
		// find the newest one
		Class<? extends IRepositoryObject> result = null;
		if (loadedClasses.containsKey(fullClassName)) {
			ComparableVersion maxVersion = Collections
					.max(loadedClasses.get(fullClassName).keySet());
			result = loadedClasses.get(fullClassName).get(maxVersion);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#getAnalysisResultsBasePath(
	 * )
	 */
	@Override
	public String getAnalysisResultsBasePath() {
		return ((RunResultCollection) this.staticRepositoryEntities
				.get(IRunResult.class)).getAnalysisResultsBasePath();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#getClusterResultsBasePath()
	 */
	@Override
	public String getClusterResultsBasePath() {
		return ((RunResultCollection) this.staticRepositoryEntities
				.get(IRunResult.class)).getClusterResultsBasePath();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#
	 * getClusterResultsQualityBasePath()
	 */
	@Override
	public String getClusterResultsQualityBasePath() {
		return ((RunResultCollection) this.staticRepositoryEntities
				.get(IRunResult.class)).getClusterResultsQualityBasePath();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#
	 * registerDataStatisticCalculator(java.lang.Class)
	 */
	@Override
	public boolean registerDataStatisticCalculator(
			Class<? extends IDataStatisticCalculator<? extends IDataStatistic>> dataStatisticCalculator)
			throws DynamicComponentMissingVersionException,
			IncompatibleClustEvalVersionException,
			MissingClustEvalVersionException, InvalidClustEvalVersionException,
			InvalidDependencyTargetException {
		ensureClassCompatibleWithClustEvalInterfaceVersion(
				dataStatisticCalculator);
		return ((DataStatisticRepositoryEntity) this.dynamicRepositoryEntities
				.get(IDataStatistic.class)).registerDataStatisticCalculator(
						dataStatisticCalculator);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#
	 * registerRunDataStatisticCalculator(java.lang.Class)
	 */
	@Override
	public boolean registerRunDataStatisticCalculator(
			Class<? extends IRunDataStatisticCalculator<? extends IRunDataStatistic>> runDataStatisticCalculator)
			throws DynamicComponentMissingVersionException,
			IncompatibleClustEvalVersionException,
			MissingClustEvalVersionException, InvalidClustEvalVersionException,
			InvalidDependencyTargetException {
		ensureClassCompatibleWithClustEvalInterfaceVersion(
				runDataStatisticCalculator);
		return ((RunDataStatisticRepositoryEntity) this.dynamicRepositoryEntities
				.get(IRunDataStatistic.class))
						.registerRunDataStatisticCalculator(
								runDataStatisticCalculator);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#
	 * registerRunStatisticCalculator(java.lang.Class)
	 */
	@Override
	public boolean registerRunStatisticCalculator(
			Class<? extends IRunStatisticCalculator<? extends IRunStatistic>> runStatisticCalculator)
			throws DynamicComponentMissingVersionException,
			IncompatibleClustEvalVersionException,
			MissingClustEvalVersionException, InvalidClustEvalVersionException,
			InvalidDependencyTargetException {
		ensureClassCompatibleWithClustEvalInterfaceVersion(
				runStatisticCalculator);
		return ((RunStatisticRepositoryEntity) this.dynamicRepositoryEntities
				.get(IRunStatistic.class))
						.registerRunStatisticCalculator(runStatisticCalculator);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#getDataStatisticCalculator(
	 * java.lang.String, org.apache.maven.artifact.versioning.ComparableVersion)
	 */
	@Override
	public Class<? extends IDataStatisticCalculator<? extends IDataStatistic>> getDataStatisticCalculator(
			final String dataStatisticClassName,
			final ComparableVersion version) {
		return ((DataStatisticRepositoryEntity) this.dynamicRepositoryEntities
				.get(IDataStatistic.class)).getDataStatisticCalculator(
						dataStatisticClassName, version);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#
	 * getRunDataStatisticCalculator(java.lang.String,
	 * org.apache.maven.artifact.versioning.ComparableVersion)
	 */
	@Override
	public Class<? extends IRunDataStatisticCalculator<? extends IRunDataStatistic>> getRunDataStatisticCalculator(
			final String runDataStatisticClassName,
			final ComparableVersion version) {
		return ((RunDataStatisticRepositoryEntity) this.dynamicRepositoryEntities
				.get(IRunDataStatistic.class)).getRunDataStatisticCalculator(
						runDataStatisticClassName, version);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#getRunStatisticCalculator(
	 * java.lang.String, org.apache.maven.artifact.versioning.ComparableVersion)
	 */
	@Override
	public Class<? extends IRunStatisticCalculator<? extends IRunStatistic>> getRunStatisticCalculator(
			final String runStatisticClassName,
			final ComparableVersion version) {
		return ((RunStatisticRepositoryEntity) this.dynamicRepositoryEntities
				.get(IRunStatistic.class)).getRunStatisticCalculator(
						runStatisticClassName, version);
	}

	public static ComparableVersion getVersionOfDynamicClass(
			final Class<? extends Object> c)
			throws DynamicComponentMissingVersionException {
		if (c == null)
			throw new IllegalArgumentException("The class cannot be null");
		try {
			DynamicComponentVersion anno = c
					.getAnnotation(DynamicComponentVersion.class);
			if (anno == null)
				throw new DynamicComponentMissingVersionException(
						"The class does not have the version annotation.");
			return new ComparableVersion(anno.version());
		} catch (DynamicComponentMissingVersionException e) {
			throw e;
		} catch (AnnotationTypeMismatchException e) {
			throw new DynamicComponentMissingVersionException(
					"The class uses an outdated format for version annotation and is not supported.");
		} catch (Exception e) {
			throw new DynamicComponentMissingVersionException(e);
		}
	}

	/**
	 * 
	 * @param c
	 *            The class for which fto retrieve the alias.
	 * @return The alias of the class as string. Null, if the class does not
	 *         have an alias.
	 */
	public static String getAliasOfDynamicClass(
			final Class<? extends Object> c) {
		if (c == null)
			throw new IllegalArgumentException("The class cannot be null");
		try {
			ClustEvalAlias anno = c.getAnnotation(ClustEvalAlias.class);
			if (anno == null)
				return null;
			return anno.alias();
		} catch (Exception e) {
			return null;
		}
	}

	public static ComparableVersion getVersionOfObjectDynamicClass(
			final Object o) {
		try {
			return new ComparableVersion(o.getClass()
					.getAnnotation(DynamicComponentVersion.class).version());
		} catch (Exception e) {
			// should not happen because we only allow classes, which have
			// version info, and thus only objects of such classes
			e.printStackTrace();
			return new ComparableVersion("1");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#getInternalDoubleAttribute(
	 * java.lang.String)
	 */
	@Override
	public INamedDoubleAttribute getInternalDoubleAttribute(
			final String value) {
		if (!isInternalAttribute(value)) {
			return null;
		}
		INamedDoubleAttribute result = this.internalDoubleAttributes
				.get(value.substring(2, value.length() - 1));
		if (result == null && parent != null)
			result = parent.getInternalDoubleAttribute(value);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#getInternalIntegerAttribute
	 * (java.lang.String)
	 */
	@Override
	public INamedIntegerAttribute getInternalIntegerAttribute(
			final String value) {
		if (!isInternalAttribute(value)) {
			return null;
		}
		INamedIntegerAttribute result = this.internalIntegerAttributes
				.get(value.substring(2, value.length() - 1));
		if (result == null && parent != null)
			result = parent.getInternalIntegerAttribute(value);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#getInternalStringAttribute(
	 * java.lang.String)
	 */
	@Override
	public INamedStringAttribute getInternalStringAttribute(
			final String value) {
		if (!isInternalAttribute(value)) {
			return null;
		}
		INamedStringAttribute result = this.internalStringAttributes
				.get(value.substring(2, value.length() - 1));
		if (result == null && parent != null)
			result = parent.getInternalStringAttribute(value);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#getLogBasePath()
	 */
	@Override
	public String getLogBasePath() {
		return ((RunResultCollection) this.staticRepositoryEntities
				.get(IRunResult.class)).getResultLogBasePath();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#getParent()
	 */
	@Override
	public Repository getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#getRegisteredObject(java.io
	 * .File)
	 */
	@Override
	public IRepositoryObject getRegisteredObject(final File absFilePath) {
		return this.pathToRepositoryObject.get(absFilePath);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#getRegisteredObject(de.
	 * clusteval.utils.NamedDoubleAttribute)
	 */
	@Override
	public INamedDoubleAttribute getRegisteredObject(
			final INamedDoubleAttribute object) {
		INamedDoubleAttribute other = this.internalDoubleAttributes
				.get(object.getName());
		if (other == null && parent != null)
			return parent.getRegisteredObject(object);
		return other;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#getRegisteredObject(de.
	 * clusteval.utils.NamedIntegerAttribute)
	 */
	@Override
	public INamedIntegerAttribute getRegisteredObject(
			final INamedIntegerAttribute object) {
		INamedIntegerAttribute other = this.internalIntegerAttributes
				.get(object.getName());
		if (other == null && parent != null)
			return parent.getRegisteredObject(object);
		return other;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#getRegisteredObject(de.
	 * clusteval.utils.NamedStringAttribute)
	 */
	@Override
	public INamedStringAttribute getRegisteredObject(
			final INamedStringAttribute object) {
		INamedStringAttribute other = this.internalStringAttributes
				.get(object.getName());
		if (other == null && parent != null)
			return parent.getRegisteredObject(object);
		return other;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#getRegisteredRunResult(java
	 * .lang.String)
	 */
	@Override
	public RunResult getRegisteredRunResult(final String runIdentifier) {
		return ((RunResultCollection) this.staticRepositoryEntities
				.get(IRunResult.class)).runResultIdentifier.get(runIdentifier);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#getRepositoryConfig()
	 */
	@Override
	public IRepositoryConfig getRepositoryConfig() {
		return this.repositoryConfig;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#
	 * getParameterOptimizationRunResultIdentifiers()
	 */
	@Override
	public Collection<String> getParameterOptimizationRunResultIdentifiers() {
		Collection<String> result = new HashSet<String>();
		for (IRepositoryObject runResult : getCollectionStaticEntities(
				IRunResult.class)) {
			if (runResult instanceof ParameterOptimizationResult) {
				result.add(((ParameterOptimizationResult) runResult)
						.getIdentifier());
			}
		}

		// for (File resultDir : new
		// File(this.getBasePath(RunResult.class)).listFiles()) {
		// if (resultDir.isDirectory()) {
		// File clustersDir = new
		// File(FileUtils.buildPath(resultDir.getAbsolutePath(), "clusters"));
		// if (clustersDir.exists() && clustersDir.isDirectory()) {
		// /*
		// * Take only those, that contain at least one *.complete
		// * file
		// */
		// for (File resultsFile : clustersDir.listFiles()) {
		// if (resultsFile.getName().endsWith(".complete")) {
		// result.add(resultDir.getName());
		// break;
		// }
		// }
		// }
		// }
		// }

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#
	 * getClusteringRunResultIdentifiers()
	 */
	@Override
	public Collection<String> getClusteringRunResultIdentifiers() {
		Collection<String> result = new HashSet<String>();
		for (IRepositoryObject runResult : getCollectionStaticEntities(
				IRunResult.class)) {
			if (runResult.getClass().equals(ClusteringRunResult.class)) {
				result.add(((ClusteringRunResult) runResult).getIdentifier());
			}
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#
	 * getDataAnalysisRunResultIdentifiers()
	 */
	@Override
	public Collection<String> getDataAnalysisRunResultIdentifiers() {
		Collection<String> result = new HashSet<String>();
		for (IRepositoryObject runResult : getCollectionStaticEntities(
				IRunResult.class)) {
			if (runResult instanceof DataAnalysisRunResult) {
				result.add(((DataAnalysisRunResult) runResult).getIdentifier());
			}
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#
	 * getRunAnalysisRunResultIdentifiers()
	 */
	@Override
	public Collection<String> getRunAnalysisRunResultIdentifiers() {
		Collection<String> result = new HashSet<String>();
		for (IRepositoryObject runResult : getCollectionStaticEntities(
				IRunResult.class)) {
			if (runResult instanceof RunAnalysisRunResult) {
				result.add(((RunAnalysisRunResult) runResult).getIdentifier());
			}
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#
	 * getRunDataAnalysisRunResultIdentifiers()
	 */
	@Override
	public Collection<String> getRunDataAnalysisRunResultIdentifiers() {
		Collection<String> result = new HashSet<String>();
		for (IRepositoryObject runResult : getCollectionStaticEntities(
				IRunResult.class)) {
			if (runResult instanceof RunDataAnalysisRunResult) {
				result.add(
						((RunDataAnalysisRunResult) runResult).getIdentifier());
			}
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#getRunResumes()
	 */
	@Override
	public Collection<String> getRunResumes() {
		Collection<String> result = new HashSet<String>();

		for (File resultDir : new File(this.getBasePath(IRunResult.class))
				.listFiles()) {
			if (resultDir.isDirectory()) {
				result.add(resultDir.getName());
			}
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#getSqlCommunicator()
	 */
	@Override
	public ISQLCommunicator getSqlCommunicator() {
		return sqlCommunicator;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#getSupervisorThread()
	 */
	@Override
	public SupervisorThread getSupervisorThread() {
		return supervisorThread;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#getSupplementaryBasePath()
	 */
	@Override
	public String getSupplementaryBasePath() {
		return this.supplementaryBasePath;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#
	 * getSupplementaryClusteringBasePath()
	 */
	@Override
	public String getSupplementaryClusteringBasePath() {
		return this.suppClusteringBasePath;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return this.basePath.hashCode();
	}

	/**
	 * A helper method for logging, which can overwritten to change the
	 * logger-level in subclasses of this class. For example in
	 * RunResultRepostories we do not want to log everything, therefore we
	 * change the log level to debug.
	 * 
	 * @param The
	 *            message to log.
	 */
	@Override
	public void info(final String message) {
		this.log.info(message);
	}

	// protected <T extends IRepositoryObject> void
	// createAndAddStaticEntity(final Class<T> c, final String basePath) {
	// this.staticRepositoryEntities.put(c, new StaticRepositoryEntity(this, c,
	// this.parent != null ? this.parent.staticRepositoryEntities.get(c) : null,
	// basePath));
	// }

	protected <T extends IRepositoryObject> void createAndAddDynamicEntity(
			final Class<T> c, final String basePath) {
		this.dynamicRepositoryEntities.put(c,
				new DynamicRepositoryObjectCollection(this, c,
						this.parent != null
								? this.parent.dynamicRepositoryEntities.get(c)
								: null,
						basePath));
	}

	/**
	 * This method initializes all attribute maps and all variables, that keep
	 * registered repository objects.
	 * 
	 * <p>
	 * A helper method for and invoked by
	 * {@link #Repository(String, Repository, long, long, long, long, long, long, long)}
	 * .
	 */
	protected void initAttributes() {
		this.loadedClasses = new ConcurrentHashMap<String, Map<ComparableVersion, Class<? extends IRepositoryObjectDynamicComponent>>>();
		this.staticRepositoryEntities = new StaticRepositoryEntityMap();

		this.dynamicRepositoryEntities = new DynamicRepositoryObjectCollectionMap();

		this.staticRepositoryEntities.put(IDataSet.class,
				new DataSetCollection(this, IDataSet.class, this.parent != null
						? this.parent.staticRepositoryEntities
								.get(IDataSet.class)
						: null,
						FileUtils.buildPath(this.basePath, "data",
								"datasets")));
		this.staticRepositoryEntities.put(IDataSetConfig.class,
				new DataSetConfigCollection(this, IDataSetConfig.class,
						this.parent != null
								? this.parent.staticRepositoryEntities
										.get(IDataSetConfig.class)
								: null,
						FileUtils.buildPath(this.basePath, "data", "datasets",
								"configs")));
		this.staticRepositoryEntities.put(IGoldStandard.class,
				new GoldStandardCollection(this, IGoldStandard.class,
						this.parent != null
								? this.parent.staticRepositoryEntities
										.get(IGoldStandard.class)
								: null,
						FileUtils.buildPath(this.basePath, "data",
								"goldstandards")));
		this.staticRepositoryEntities.put(IGoldStandardConfig.class,
				new GoldStandardConfigCollection(this,
						IGoldStandardConfig.class,
						this.parent != null
								? this.parent.staticRepositoryEntities
										.get(IGoldStandardConfig.class)
								: null,
						FileUtils.buildPath(this.basePath, "data",
								"goldstandards", "configs")));
		this.staticRepositoryEntities.put(IDataConfig.class,
				new DataConfigCollection(this, IDataConfig.class,
						this.parent != null
								? this.parent.staticRepositoryEntities
										.get(IDataConfig.class)
								: null,
						FileUtils.buildPath(this.basePath, "data", "configs")));
		this.staticRepositoryEntities.put(IRun.class,
				new RunCollection(this, IRun.class, this.parent != null
						? this.parent.staticRepositoryEntities.get(IRun.class)
						: null, FileUtils.buildPath(this.basePath, "runs")));
		this.staticRepositoryEntities.put(IProgramConfig.class,
				new ProgramConfigCollection(this, IProgramConfig.class,
						this.parent != null
								? this.parent.staticRepositoryEntities
										.get(IProgramConfig.class)
								: null,
						FileUtils.buildPath(this.basePath, "programs",
								"configs")));

		this.staticRepositoryEntities.put(IClustering.class,
				new ClusteringCollection(this,
						this.parent != null
								? this.parent.staticRepositoryEntities
										.get(IClustering.class)
								: null,
						FileUtils.buildPath(this.basePath, "results")));

		this.staticRepositoryEntities.put(IRunResult.class,
				new RunResultCollection(this,
						this.parent != null
								? this.parent.staticRepositoryEntities
										.get(IRunResult.class)
								: null,
						FileUtils.buildPath(this.basePath, "results")));

		this.staticRepositoryEntities.put(IFinder.class,
				new FinderCollection(this,
						this.parent != null
								? this.parent.staticRepositoryEntities
										.get(IFinder.class)
								: null,
						null));

		this.createAndAddDynamicEntity(IDistanceMeasure.class, FileUtils
				.buildPath(this.supplementaryBasePath, "distanceMeasures"));

		this.dynamicRepositoryEntities.put(IDataStatistic.class,
				new DataStatisticRepositoryEntity(this, this.parent != null
						? (DataStatisticRepositoryEntity) this.parent.dynamicRepositoryEntities
								.get(IDataStatistic.class)
						: null,
						FileUtils.buildPath(this.supplementaryBasePath,
								"statistics", "data")));

		this.dynamicRepositoryEntities.put(IRunStatistic.class,
				new RunStatisticRepositoryEntity(this, this.parent != null
						? (RunStatisticRepositoryEntity) this.parent.dynamicRepositoryEntities
								.get(IRunStatistic.class)
						: null,
						FileUtils.buildPath(this.supplementaryBasePath,
								"statistics", "run")));

		this.dynamicRepositoryEntities.put(IRunDataStatistic.class,
				new RunDataStatisticRepositoryEntity(this, this.parent != null
						? (RunDataStatisticRepositoryEntity) this.parent.dynamicRepositoryEntities
								.get(IRunDataStatistic.class)
						: null,
						FileUtils.buildPath(this.supplementaryBasePath,
								"statistics", "rundata")));

		this.createAndAddDynamicEntity(IDataSetGenerator.class,
				FileUtils.buildPath(this.generatorBasePath, "dataset"));
		this.createAndAddDynamicEntity(IDataRandomizer.class,
				FileUtils.buildPath(this.randomizerBasePath, "data"));
		this.createAndAddDynamicEntity(IDataPreprocessor.class, FileUtils
				.buildPath(this.supplementaryBasePath, "preprocessing"));
		this.createAndAddDynamicEntity(IRunResultPostprocessor.class, FileUtils
				.buildPath(this.supplementaryBasePath, "postprocessing"));

		this.staticRepositoryEntities.put(IStandaloneProgram.class,
				new StandaloneProgramCollection(this, IStandaloneProgram.class,
						this.parent != null
								? this.parent.staticRepositoryEntities
										.get(IStandaloneProgram.class)
								: null,
						FileUtils.buildPath(this.basePath, "programs")));
		this.dynamicRepositoryEntities.put(IRProgram.class,
				new RProgramRepositoryEntity(this,
						this.staticRepositoryEntities
								.get(IStandaloneProgram.class),
						this.parent != null
								? (RProgramRepositoryEntity) this.parent.dynamicRepositoryEntities
										.get(IRProgram.class)
								: null,
						this.getBasePath(IStandaloneProgram.class)));

		this.createAndAddDynamicEntity(IClusteringQualityMeasure.class,
				FileUtils.buildPath(this.suppClusteringBasePath,
						"qualityMeasures"));

		this.createAndAddDynamicEntity(IContext.class,
				FileUtils.buildPath(this.supplementaryBasePath, "contexts"));

		this.createAndAddDynamicEntity(IParameterOptimizationMethod.class,
				FileUtils.buildPath(this.suppClusteringBasePath,
						"paramOptimization"));

		this.createAndAddDynamicEntity(IDataSetType.class,
				FileUtils.buildPath(this.typesBasePath, "dataset"));

		this.dynamicRepositoryEntities.put(IDataSetFormat.class,
				new DataSetFormatRepositoryEntity(this, this.parent != null
						? (DataSetFormatRepositoryEntity) this.parent.dynamicRepositoryEntities
								.get(IDataSetFormat.class)
						: null,
						FileUtils.buildPath(this.formatsBasePath, "dataset")));

		this.dynamicRepositoryEntities.put(IRunResultFormat.class,
				new RunResultFormatRepositoryEntity(this, this.parent != null
						? (RunResultFormatRepositoryEntity) this.parent.dynamicRepositoryEntities
								.get(IRunResultFormat.class)
						: null,
						FileUtils.buildPath(this.formatsBasePath,
								"runresult")));

		this.goldStandardFormats = new ConcurrentHashMap<GoldStandardFormat, GoldStandardFormat>();

		this.internalDoubleAttributes = new ConcurrentHashMap<String, INamedDoubleAttribute>();
		this.internalStringAttributes = new ConcurrentHashMap<String, INamedStringAttribute>();
		this.internalIntegerAttributes = new ConcurrentHashMap<String, INamedIntegerAttribute>();

		// added 14.04.2013
		this.knownFinderExceptions = new ConcurrentHashMap<Class<? extends IRepositoryObject>, Map<ISerializableWrapperRepositoryObject<? extends IRepositoryObject>, List<Throwable>>>();
		this.finderClassLoaders = new ConcurrentHashMap<URL, URLClassLoader>();
		this.finderWaitingFiles = new ConcurrentHashMap<String, List<File>>();
		this.finderLoadedJarFileChangeDates = new ConcurrentHashMap<String, Long>();
		this.fullClassNamesAndVersionsForPresentFiles = new ConcurrentHashMap<Class<? extends IRepositoryObject>, Map<String, Set<ComparableVersion>>>();
		this.finderErrorObjects = new ConcurrentHashMap<Class<? extends IRepositoryObject>, Map<ISerializableWrapperRepositoryObject<IRepositoryObject>, IRepositoryObject>>();

		this.toolRuntimePolynomialFits = new HashMap<>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#initialize()
	 */
	@Override
	public void initialize() throws InterruptedException, DatabaseException {
		if (isInitialized() || this.supervisorThread != null)
			return;

		boolean databaseAvailable = true;
		boolean dbSchemaInitialized = true;
		do {
			try {
				this.sqlCommunicator = createSQLCommunicator();
				databaseAvailable = true;
				dbSchemaInitialized = true;
			} catch (DatabaseConnectException e) {
				databaseAvailable = false;
				if (!Repository.AWAIT_STARTED_DATABASE_SERVER)
					throw e;
				this.log.info(String.format(
						"Connecting to the database server failed with the following error: %s",
						e.getMessage()));
				Thread.sleep(5000);
			} catch (DatabaseInitializationException e) {
				databaseAvailable = true;
				dbSchemaInitialized = false;
				if (!Repository.AWAIT_INITIALIZED_DATABASE_SCHEMA)
					throw e;
				this.log.info(
						"The database schema is not (completely) initialized yet. Retrying later ...");
				Thread.sleep(5000);
			}
		} while ((!databaseAvailable
				&& Repository.AWAIT_STARTED_DATABASE_SERVER)
				|| (!dbSchemaInitialized
						&& Repository.AWAIT_INITIALIZED_DATABASE_SCHEMA));

		this.supervisorThread = createSupervisorThread();

		// wait until repository initialized
		try {
			while (!this.isInitialized())
				Thread.sleep(100);
		} catch (InterruptedException e) {
			this.terminateSupervisorThread();
			throw e;
		}

		if (!RepositorySupervisorThread.CHECK_FOR_RUN_RESULTS)
			this.sqlCommunicator.setRepositoryParsingFinished();

		this.info("Repository initialization finished");

		/**
		 * Print warnings for all required R libraries, that could not be loaded
		 */
		if (MyRengine.IS_R_AVAILABLE) {
			if (this.missingRLibraries.size() > 0) {
				this.warn(
						"The following R library dependencies are not satisified (the corresponding class has not been loaded):");
				this.warn(
						"Please ensure that those libraries are installed in your R installation:");

				StringBuilder sb = new StringBuilder();
				sb.append("install.packages(c(");

				for (String className : this.missingRLibraries.keySet())
					for (RLibraryNotLoadedException e : this.missingRLibraries
							.get(className)) {
						this.warn("Class '" + e.getClassName()
								+ "' requires the unavailable R library '"
								+ e.getRLibrary() + "'");
						sb.append(String.format("\"%s\",", e.getRLibrary()));
					}
				sb.deleteCharAt(sb.length() - 1);

				sb.append("))");

				this.warn(
						"You can use the following command to install them in R:");
				this.warn(sb.toString());
			}
		}
	}

	/**
	 * This method sets all the absolute paths used by the repository to store
	 * any kinds of files and data on the filesystem.
	 * 
	 * <p>
	 * This method only initializes the attributes itself to valid paths, but
	 * does not create or ensure any folder structure.
	 * <p>
	 * A helper method of
	 * {@link #Repository(String, Repository, long, long, long, long, long, long, long)}
	 * .
	 * 
	 * @throws InvalidRepositoryException
	 * 
	 */
	@SuppressWarnings("unused")
	protected void initializePaths() throws InvalidRepositoryException {
		this.supplementaryBasePath = FileUtils.buildPath(this.basePath, "supp");
		this.suppClusteringBasePath = FileUtils
				.buildPath(this.supplementaryBasePath, "clustering");
		this.formatsBasePath = FileUtils.buildPath(this.supplementaryBasePath,
				"formats");
		this.generatorBasePath = FileUtils.buildPath(this.supplementaryBasePath,
				"generators");
		this.randomizerBasePath = FileUtils
				.buildPath(this.supplementaryBasePath, "randomizers");
		this.typesBasePath = FileUtils.buildPath(this.supplementaryBasePath,
				"types");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#isInitialized()
	 */
	@Override
	public boolean isInitialized() {
		// TODO: for loop?
		return isInitialized(IDataSetFormat.class)
				&& isInitialized(IDataSetType.class)
				&& isInitialized(IDataStatistic.class)
				&& isInitialized(IRunStatistic.class)
				&& isInitialized(IRunDataStatistic.class)
				&& isInitialized(IRunResultFormat.class)
				&& isInitialized(IClusteringQualityMeasure.class)
				&& isInitialized(IParameterOptimizationMethod.class)
				&& isInitialized(IRun.class) && isInitialized(IRProgram.class)
				&& isInitialized(IDataSetConfig.class)
				&& isInitialized(IDataSet.class)
				&& isInitialized(IGoldStandardConfig.class)
				&& isInitialized(IDataConfig.class)
				&& isInitialized(IProgramConfig.class)
				&& isInitialized(IDataSetGenerator.class)
				&& isInitialized(IContext.class)
				&& isInitialized(IDataPreprocessor.class)
				&& isInitialized(IDistanceMeasure.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#
	 * isRegisteredForRunResultFormat(java.lang.Class)
	 */
	@Override
	public boolean isRegisteredForRunResultFormat(
			final Class<? extends IRunResultFormat> runResultFormat) {
		return ((RunResultFormatRepositoryEntity) this.dynamicRepositoryEntities
				.get(IRunResultFormat.class))
						.isRegisteredForRunResultFormat(runResultFormat);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#register(de.clusteval.utils
	 * .NamedDoubleAttribute)
	 */
	@Override
	public boolean register(final INamedDoubleAttribute object) {
		if (this.getRegisteredObject(object) != null)
			return false;
		this.internalDoubleAttributes.put(object.getName(), object);
		this.pathToRepositoryObject.put(object.getFile(), object);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#register(de.clusteval.utils
	 * .NamedIntegerAttribute)
	 */
	@Override
	public boolean register(final INamedIntegerAttribute object) {
		if (this.getRegisteredObject(object) != null)
			return false;
		this.internalIntegerAttributes.put(object.getName(), object);
		this.pathToRepositoryObject.put(object.getFile(), object);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#register(de.clusteval.utils
	 * .NamedStringAttribute)
	 */
	@Override
	public boolean register(final INamedStringAttribute object) {
		if (this.getRegisteredObject(object) != null)
			return false;
		this.internalStringAttributes.put(object.getName(), object);
		this.pathToRepositoryObject.put(object.getFile(), object);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#getRengineForCurrentThread(
	 * )
	 */
	@Override
	public IMyRengine getRengineForCurrentThread() throws RserveException {
		Thread currentThread = Thread.currentThread();
		synchronized (this.rEngines) {
			if (!this.rEngines.containsKey(currentThread))
				this.rEngines.put(currentThread, new MyRengine(""));
			return this.rEngines.get(currentThread);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#getRengine(java.lang.
	 * Thread)
	 */
	@Override
	public IMyRengine getRengine(final Thread thread) throws RserveException {
		synchronized (this.rEngines) {
			if (!this.rEngines.containsKey(thread))
				this.rEngines.put(thread, new MyRengine(""));
			return this.rEngines.get(thread);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#
	 * clearRengineForCurrentThread()
	 */
	@Override
	public void clearRengineForCurrentThread() {
		Thread currentThread = Thread.currentThread();
		synchronized (this.rEngines) {
			if (this.rEngines.containsKey(currentThread))
				this.rEngines.remove(currentThread);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#clearRengine(java.lang.
	 * Thread)
	 */
	@Override
	public void clearRengine(final Thread thread) {
		synchronized (this.rEngines) {
			if (this.rEngines.containsKey(thread))
				this.rEngines.remove(thread);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#registerDataSetFormatParser
	 * (java.lang.Class)
	 */
	@Override
	public boolean registerDataSetFormatParser(
			final Class<? extends IDataSetFormatParser> dsFormatParser)
			throws DynamicComponentMissingVersionException,
			IncompatibleClustEvalVersionException,
			MissingClustEvalVersionException, InvalidClustEvalVersionException,
			InvalidDependencyTargetException {
		ensureClassCompatibleWithClustEvalInterfaceVersion(dsFormatParser);
		return ((DataSetFormatRepositoryEntity) this.dynamicRepositoryEntities
				.get(IDataSetFormat.class))
						.registerDataSetFormatParser(dsFormatParser);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#
	 * isRegisteredForDataSetFormat(java.lang.Class)
	 */
	@Override
	public boolean isRegisteredForDataSetFormat(
			final Class<? extends IDataSetFormat> dsFormat) {
		return ((DataSetFormatRepositoryEntity) this.dynamicRepositoryEntities
				.get(IDataSetFormat.class))
						.isRegisteredForDataSetFormat(dsFormat);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#
	 * registerRunResultFormatParser(java.lang.Class)
	 */
	@Override
	public boolean registerRunResultFormatParser(
			final Class<? extends IRunResultFormatParser> runResultFormatParser)
			throws IncompatibleClustEvalVersionException,
			DynamicComponentMissingVersionException,
			MissingClustEvalVersionException, InvalidClustEvalVersionException,
			InvalidDependencyTargetException {
		ensureClassCompatibleWithClustEvalInterfaceVersion(
				runResultFormatParser);
		return ((RunResultFormatRepositoryEntity) this.dynamicRepositoryEntities
				.get(IRunResultFormat.class))
						.registerRunResultFormatParser(runResultFormatParser);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#setSQLCommunicator(de.
	 * clusteval.framework.repository.db.SQLCommunicator)
	 */
	@Override
	public void setSQLCommunicator(final ISQLCommunicator comm) {
		this.sqlCommunicator = comm;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.basePath;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#unregister(de.clusteval.
	 * utils.NamedDoubleAttribute)
	 */
	@Override
	public boolean unregister(INamedDoubleAttribute object) {
		return this.internalDoubleAttributes.remove(object) != null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#unregister(de.clusteval.
	 * utils.NamedIntegerAttribute)
	 */
	@Override
	public boolean unregister(INamedIntegerAttribute object) {
		return this.internalIntegerAttributes.remove(object) != null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#unregister(de.clusteval.
	 * utils.NamedStringAttribute)
	 */
	@Override
	public boolean unregister(INamedStringAttribute object) {
		return this.internalStringAttributes.remove(object) != null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#
	 * unregisterRunResultFormatParser(java.lang.Class)
	 */
	@Override
	public boolean unregisterRunResultFormatParser(
			final Class<? extends IRunResultFormatParser> object) {
		return ((RunResultFormatRepositoryEntity) this.dynamicRepositoryEntities
				.get(IRunResultFormat.class))
						.unregisterRunResultFormatParser(object);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#updateStatusOfRun(de.
	 * clusteval.run.Run, java.lang.String)
	 */
	@Override
	public boolean updateStatusOfRun(final IRun run, final String newStatus) {
		return this.sqlCommunicator.updateStatusOfRun(run, newStatus);
	}

	/**
	 * A helper method for logging, which can overwritten to change the
	 * logger-level in subclasses of this class. For example in
	 * RunResultRepostories we do not want to log everything, therefore we
	 * change the log level to debug.
	 * 
	 * @param The
	 *            message to log.
	 */
	@Override
	public void warn(final String message) {
		this.log.warn(message);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#getKnownFinderExceptions()
	 */
	@Override
	public Map<Class<? extends IRepositoryObject>, Map<ISerializableWrapperRepositoryObject<? extends IRepositoryObject>, List<Throwable>>> getKnownFinderExceptions() {
		return this.knownFinderExceptions;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#getJARFinderClassLoaders()
	 */
	@Override
	public Map<URL, URLClassLoader> getJARFinderClassLoaders() {
		return this.finderClassLoaders;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#getJARFinderWaitingFiles()
	 */
	@Override
	public Map<String, List<File>> getJARFinderWaitingFiles() {
		return this.finderWaitingFiles;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#
	 * getFinderLoadedJarFileChangeDates()
	 */
	@Override
	public Map<String, Long> getFinderLoadedJarFileChangeDates() {
		return this.finderLoadedJarFileChangeDates;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepository#
	 * getFullClassNamesAndVersionsForPresentFiles()
	 */
	@Override
	public Map<Class<? extends IRepositoryObject>, Map<String, Set<ComparableVersion>>> getFullClassNamesAndVersionsForPresentFiles() {
		return fullClassNamesAndVersionsForPresentFiles;
	}

	/**
	 * @return the loadedClasses
	 */
	@Override
	public Map<String, Map<ComparableVersion, Class<? extends IRepositoryObjectDynamicComponent>>> getLoadedClasses() {
		return loadedClasses;
	}

	/**
	 * @return the pathToRepositoryObject
	 */
	@Override
	public Map<File, IRepositoryObject> getPathToRepositoryObject() {
		return pathToRepositoryObject;
	}

	/**
	 * Every repository has a version. This version corresponds to the structure
	 * and format the repository uses to stores information.
	 * 
	 * @return The version of this repository.
	 */
	public ComparableVersion getVersion() {
		return this.repositoryConfig.getVersion();
	}

	@Override
	public boolean installRepositoryObjectFromJarFromMaven(
			final Class<? extends IRepositoryObject> type,
			final String objectName, final File jarFile,
			final ComparableVersion version) {
		return this.staticRepositoryEntities.get(type)
				.installFromJarFromMaven(objectName, jarFile, version);
	}

	@Override
	public String getMavenGroupIDForRepositoryObject(
			final Class<? extends IRepositoryObject> c,
			final String objectName) {
		return this.staticRepositoryEntities.get(c)
				.getMavenGroupIDForRepositoryObject(objectName);
	}

	@Override
	public String getMavenArtifactIDForRepositoryObject(
			final Class<? extends IRepositoryObject> c,
			final String objectName) {
		return this.staticRepositoryEntities.get(c)
				.getMavenArtifactIDForRepositoryObject(objectName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepository#getErrorObjects(java.lang.
	 * Class)
	 */
	@Override
	public Map<Class<? extends IRepositoryObject>, Map<ISerializableWrapperRepositoryObject<IRepositoryObject>, IRepositoryObject>> getErrorObjects()
			throws ObjectNotFoundException, ObjectVersionNotFoundException {
		return this.finderErrorObjects;
	}

	@Override
	public boolean hasError(final Class<? extends IRepositoryObject> clazz,
			final String name, final String version) {
		return this.knownFinderExceptions.containsKey(clazz)
				&& this.knownFinderExceptions.get(clazz).containsKey(
						new ErrorSerializableWrapper<>(null, name, version));
	}

	@Override
	public Collection<Throwable> getErrors(
			final Class<? extends IRepositoryObject> clazz, final String name,
			final String version) {
		Collection<Throwable> result = new HashSet<>();
		IErrorSerializableWrapper<IRepositoryObject> wrapper = new ErrorSerializableWrapper<>(
				null, name, version);
		if (this.knownFinderExceptions.containsKey(clazz)
				&& this.knownFinderExceptions.get(clazz).containsKey(wrapper))
			result.addAll(this.knownFinderExceptions.get(clazz).get(wrapper));
		return result;
	}

	@Override
	public String getRelativeFilePathToRepository(final File file) {
		return new File(this.getBasePath()).toPath().relativize(file.toPath())
				.toString();
	}

	@Override
	public Map<String, Map<ComparableVersion, Class<? extends IRepositoryObjectDynamicComponent>>> getAllRegisteredClasses() {
		Map<String, Map<ComparableVersion, Class<? extends IRepositoryObjectDynamicComponent>>> result = new HashMap<>();

		for (Class<? extends IRepositoryObjectDynamicComponent> c : dynamicRepositoryEntities.map
				.keySet()) {
			result.putAll(dynamicRepositoryEntities.map.get(c).classes);
		}
		return result;
	}

	@Override
	public long getExpectedRuntimePerClustering(
			IProgramConfig queryProgramConfig, int numberSamplesInDataset) {
		double[] coefficients = null;
		if (toolRuntimePolynomialFits.containsKey(queryProgramConfig)) {
			coefficients = toolRuntimePolynomialFits.get(queryProgramConfig);
		} else {
			Map<Integer, List<Long>> numberSamplesToWalltimes = new HashMap<>();
			Map<String, Map<ComparableVersion, IRunResult>> staticEntitiesWithVersions = this
					.getStaticEntitiesWithVersions(IRunResult.class);
			for (String resultId : staticEntitiesWithVersions.keySet()) {
				// run results are not versioned
				IRunResult result = staticEntitiesWithVersions.get(resultId)
						.values().iterator().next();
				if (!(result instanceof ToolRuntimeEvaluationRunResult))
					continue;

				IProgramConfig resultProgramConfig = ((ToolRuntimeEvaluationRunResult) result)
						.getProgramConfig();

				if (!resultProgramConfig.toString()
						.equals(queryProgramConfig.toString()))
					continue;

				IDataConfig resultDataConfig = ((ToolRuntimeEvaluationRunResult) result)
						.getDataConfig();
				Map<String, String> metaDataOfIteration = ((ToolRuntimeEvaluationRunResult) result)
						.getMetaDataOfIteration(1);
				if (!metaDataOfIteration.containsKey("totalwalltime"))
					continue;

				long walltime = Long
						.valueOf(metaDataOfIteration.get("totalwalltime"));
				int datasetSize = resultDataConfig.getDatasetConfig()
						.getDataSet().getNumberOfSamples();
				if (!numberSamplesToWalltimes.containsKey(datasetSize))
					numberSamplesToWalltimes.put(datasetSize,
							new ArrayList<>());
				numberSamplesToWalltimes.get(datasetSize).add(walltime);
			}

			if (numberSamplesToWalltimes.isEmpty())
				return -1l;

			System.out.println(queryProgramConfig);
			// use library to fit a polynom
			WeightedObservedPoints points = new WeightedObservedPoints();
			for (Map.Entry<Integer, List<Long>> e : numberSamplesToWalltimes
					.entrySet()) {
				for (long walltime : e.getValue()) {
					// long walltime = (long) e.getValue().stream().mapToLong(l
					// -> l)
					// .summaryStatistics().getAverage();
					System.out.println(e.getKey() + "\t" + walltime);
					points.add(e.getKey(), walltime);
				}
			}

			PolynomialCurveFitter fitter = PolynomialCurveFitter.create(2);
			double[] fit = fitter.fit(points.toList());
			int p = fit.length - 1;
			while (p >= 0 && fit[p] < 0)
				fit[p--] = 0.0;
			coefficients = fit;
			toolRuntimePolynomialFits.put(queryProgramConfig, coefficients);
		}

		double result = 0.0;
		for (int i = 0; i < coefficients.length; i++) {
			result += coefficients[i] * Math.pow(numberSamplesInDataset, i);
		}
		return (long) result;
	}

	@Override
	public void ensureRuntimeInformationForAllProgramConfigs()
			throws UnknownContextException,
			UnknownClusteringQualityMeasureException,
			UnknownDataSetFormatException, UnknownDataSetTypeException,
			UnknownDistanceMeasureException, RepositoryObjectDumpException,
			DynamicComponentInitializationException, RegisterException,
			IOException, URISyntaxException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException, InterruptedException {
		Set<IProgramConfig> pcs = ToolRuntimeEvaluationRun
				.getProgramConfigsWithoutRuntimeInformation(this);
		if (pcs.isEmpty())
			return;
		for (int i = 0; i < 3; i++) {
			Set<String> runningRunNames = new HashSet<String>();
			Set<String> runningRunIDs = new HashSet<String>();
			for (IProgramConfig pc : pcs) {
				Map<IDataConfig, IToolRuntimeEvaluationRun<?>> runs = ToolRuntimeEvaluationRun
						.getOrCreateRuntimeEvaluationRuns(this, pc);
				for (IDataConfig dc : runs.keySet()) {
					IToolRuntimeEvaluationRun<?> run = runs.get(dc);
					String runId = this.getSupervisorThread().getRunScheduler()
							.schedule("1", run.getName());
					runningRunIDs.add(runId);
					runningRunNames.add(run.getName());
				}
			}

			IRunSchedulerThread runScheduler = this.getSupervisorThread()
					.getRunScheduler();

			// wait for runs to finish
			boolean runExecuting = false;
			do {
				runExecuting = false;
				Set<Entry<String, Pair<RUN_STATUS, Float>>> entrySet = runScheduler
						.getRunStatusForClientId("1").entrySet();
				for (Entry<String, Pair<RUN_STATUS, Float>> e : entrySet) {
					if ((runningRunIDs.contains(e.getKey()) && e.getValue()
							.getFirst().equals(RUN_STATUS.RUNNING))
							|| (runningRunNames.contains(e.getKey())
									&& e.getValue().getFirst()
											.equals(RUN_STATUS.SCHEDULED))) {
						runExecuting = true;
						break;
					}
				}
				if (runExecuting)
					Thread.sleep(1000);
			} while (runExecuting);
		}
	}
}