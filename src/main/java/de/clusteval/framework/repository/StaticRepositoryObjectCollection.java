/**
 * 
 */
package de.clusteval.framework.repository;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.framework.repository.maven.IMavenRepositoryResolver;
import de.clusteval.utils.IFinder;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * @author Christian Wiwie
 *
 *
 * @since Nov 7, 2017
 */
public abstract class StaticRepositoryObjectCollection
		extends
			RepositoryObjectCollection {

	protected StaticRepositoryObjectCollection parent;

	/**
	 * A map containing all datasets registered in this repository.
	 */
	protected Map<IRepositoryObject, IRepositoryObject> objects;
	protected Map<String, Map<ComparableVersion, IRepositoryObject>> nameToObject;

	public StaticRepositoryObjectCollection(final IRepository repository,
			final Class<? extends IRepositoryObject> entityClass,
			final StaticRepositoryObjectCollection parent,
			final String basePath) {
		super(repository, entityClass, basePath);
		this.parent = parent;
		this.objects = new HashMap<IRepositoryObject, IRepositoryObject>();
		this.nameToObject = new HashMap<String, Map<ComparableVersion, IRepositoryObject>>();
	}

	public synchronized Collection<IRepositoryObject> asCollection() {
		Collection<IRepositoryObject> result = new HashSet<IRepositoryObject>();

		for (Map<ComparableVersion, IRepositoryObject> m : nameToObject
				.values())
			result.addAll(m.values());

		if (parent != null)
			result.addAll(parent.asCollection());

		return result;
	}

	public synchronized Map<String, Map<ComparableVersion, IRepositoryObject>> asMapWithVersions() {
		Map<String, Map<ComparableVersion, IRepositoryObject>> result = new HashMap<String, Map<ComparableVersion, IRepositoryObject>>();

		for (String name : nameToObject.keySet())
			result.put(name, nameToObject.get(name));

		if (parent != null) {
			Map<String, Map<ComparableVersion, IRepositoryObject>> pMap = parent
					.asMapWithVersions();

			for (String name : pMap.keySet())
				if (result.containsKey(name)) {
					result.get(name).putAll(pMap.get(name));
				} else
					result.put(name, pMap.get(name));

		}

		return result;
	}

	public synchronized IRepositoryObject findByString(String search)
			throws ObjectNotFoundException, ObjectVersionNotFoundException {
		return this.findByString(search, true);
	}

	/**
	 * 
	 * This method looks up and returns (if it exists) the object with the given
	 * name.
	 * 
	 * @param search
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws ObjectVersionNotFoundException
	 */
	public synchronized IRepositoryObject findByString(String search,
			boolean tryToResolve)
			throws ObjectNotFoundException, ObjectVersionNotFoundException {
		String withoutVersion = null;
		ComparableVersion version = null;
		try {
			if (hasVersionInName(search)) {
				withoutVersion = getWithoutVersionInName(search);
				version = getVersionFromName(search);
			} else {
				withoutVersion = search;
				version = new ComparableVersion("1");
			}
			if (!nameToObject.containsKey(withoutVersion))
				throw new ObjectNotFoundException(search);
			if (!nameToObject.get(withoutVersion).containsKey(version))
				throw new ObjectVersionNotFoundException(search, version);
		} catch (ObjectResolutionException e) {
			if (tryToResolve && canBeResolvedWithMaven()) {
				// try to resolve class using maven
				IMavenRepositoryResolver resolver = this.repository
						.getMavenResolver();
				if (resolver.isRepositoryObjectAvailable(this.getEntityClass(),
						search)) {
					if (resolver.installRepositoryObjectIntoRepository(
							this.getEntityClass(), search)) {

					}
				}

				// try to load it again after we have successfully loaded
				// the class from maven
				if (hasVersionInName(search)) {
					withoutVersion = getWithoutVersionInName(search);
					version = getVersionFromName(search);
				} else {
					withoutVersion = search;
					version = new ComparableVersion("1");
				}
			}
		}
		if (!nameToObject.containsKey(withoutVersion))
			throw new ObjectNotFoundException(search);
		if (!nameToObject.get(withoutVersion).containsKey(version))
			throw new ObjectVersionNotFoundException(search, version);
		IRepositoryObject result = nameToObject.get(withoutVersion)
				.get(version);
		if (result != null)
			return result;
		if (parent != null)
			return parent.findByString(search);
		return null;
	}

	protected boolean canBeResolvedWithMaven() {
		return false;
	}

	protected String getVersionMatchString() {
		return ":\\d+(-SNAPSHOT)?";
	}

	/**
	 * This method checks, whether there is an object registered, that is equal
	 * to the passed object and returns it.
	 * 
	 * <p>
	 * Equality is checked in terms of
	 * <ul>
	 * <li><b>object.hashCode == other.hashCode</b></li>
	 * <li><b>object.equals(other)</b></li>
	 * </ul>
	 * since internally the repository uses hash datastructures.
	 * 
	 * <p>
	 * By default the {@link RepositoryObject#equals(Object)} method is only
	 * based on the absolute path of the repository object and the repositories
	 * of the two objects, this means two repository objects are considered the
	 * same if they are stored in the same repository and they have the same
	 * absolute path.
	 * 
	 * @param obj
	 * @return
	 * 
	 */
	public synchronized IRepositoryObject getRegisteredObject(
			final IRepositoryObject obj) {
		return this.getRegisteredObject(obj, true);
	}

	public synchronized IRepositoryObject getRegisteredObject(
			final IRepositoryObject object, final boolean ignoreChangeDate) {
		// get object without changedate

		RepositoryObject other = (RepositoryObject) this.objects.get(object);
		// inserted parent, 02.06.2012
		if (other == null && parent != null)
			return parent.getRegisteredObject(object, ignoreChangeDate);
		else if (ignoreChangeDate || other == null)
			return other;
		else if (other.changeDate == object.getChangeDate()) {
			return other;
		}
		return object;
	}

	/**
	 * 
	 * This method registers a new object.
	 * 
	 * <p>
	 * First by invoking {@link #getRegisteredObject(RepositoryObject)} the
	 * method checks, whether another object equalling the new object has been
	 * registered before.
	 * 
	 * <p>
	 * If there is no old equalling object, the new object is simply registered
	 * at the repository.
	 * 
	 * <p>
	 * If there is an old equalling object, their <b>changedates</b> are
	 * compared. The new object is only registered, if the changedate of the new
	 * object is newer than the changedate of the old object. If the changedate
	 * is newer, the new object is registered at the repository and a
	 * {@link RepositoryReplaceEvent} is being thrown. This event tells the old
	 * object and all its listeners in {@link RepositoryObject#listener}, that
	 * it has been replaced by the new object. This allows all objects to update
	 * their references to the old object to the new object.
	 * 
	 * <p>
	 * The method also tells the {@link #repository.sqlCommunicator} of the
	 * repository, that a new object has been registered and causes him, to
	 * handle the new object.
	 * 
	 * @param object
	 * @return
	 * @throws RegisterException
	 */
	@Override
	public synchronized boolean register(final IRepositoryObject object)
			throws RegisterException {
		IRepositoryObject old = this.getRegisteredObject(object);
		if (old != null) {
			return this.registerWhenExisting(old, object);
		}
		return this.registerWithoutExisting(object);
	}

	protected synchronized boolean hasChanged(final IRepositoryObject old,
			final IRepositoryObject object) {
		return old.getChangeDate() < object.getChangeDate();
	}

	protected synchronized boolean registerWhenExisting(
			final IRepositoryObject old, final IRepositoryObject object)
			throws RegisterException {
		// check, whether the changeDate is equal
		if (!hasChanged(old, object))
			return false;

		if (this.printOnRegister)
			this.repository.info(object.getClass().getSimpleName() + " "
					+ object.toString() + " reloaded");

		/*
		 * replace old object by new object
		 */
		RepositoryReplaceEvent event = new RepositoryReplaceEvent(old, object);
		this.objects.put(object, object);
		String withoutVersion = getWithoutVersionInName(object.toString());
		if (!this.nameToObject.containsKey(withoutVersion))
			this.nameToObject.put(withoutVersion,
					new HashMap<ComparableVersion, IRepositoryObject>());
		this.nameToObject.get(withoutVersion).put(object.getVersion(), object);
		this.repository.getPathToRepositoryObject().put(object.getFile(),
				object);
		old.notify(event);

		this.repository.getSqlCommunicator().register(object, true);

		return true;
	}

	protected synchronized boolean registerWithoutExisting(
			final IRepositoryObject object) throws RegisterException {
		this.objects.put(object, object);
		String withoutVersion = getWithoutVersionInName(object.toString());
		if (!this.nameToObject.containsKey(withoutVersion))
			this.nameToObject.put(withoutVersion,
					new HashMap<ComparableVersion, IRepositoryObject>());
		this.nameToObject.get(withoutVersion).put(object.getVersion(), object);
		this.repository.getPathToRepositoryObject().put(object.getFile(),
				object);
		if (this.printOnRegister)
			this.repository.info("New " + object.getClass().getSimpleName()
					+ ": " + object.toString());

		this.repository.getSqlCommunicator().register(object, false);

		return true;
	}

	/**
	 * 
	 * This method unregisters the passed object.
	 * 
	 * <p>
	 * If the object has been registered before and was unregistered now, this
	 * method tells the sql communicator such that he can also handle the
	 * removal of the object.
	 * 
	 * @param object
	 * @return
	 */
	@Override
	public synchronized boolean unregister(final IRepositoryObject object) {
		boolean result = this.objects.remove(object) != null;
		String withoutVersion = getWithoutVersionInName(object.toString());
		result &= this.nameToObject.containsKey(withoutVersion)
				&& this.nameToObject.get(withoutVersion)
						.remove(object.getVersion()) != null;
		if (result && this.nameToObject.get(withoutVersion).isEmpty())
			this.nameToObject.remove(withoutVersion);
		result &= this.repository.getPathToRepositoryObject()
				.remove(object.getFile()) != null;
		if (result) {
			this.unregisterAfterRemove(object);
		}
		return result;
	}

	protected synchronized void unregisterAfterRemove(
			final IRepositoryObject object) {
		if (this.printOnRegister)
			this.repository.info(
					object.getClass().getSimpleName() + " removed: " + object);

		this.repository.getSqlCommunicator().unregister(object);
	}

	public abstract boolean isVersioned();

	public synchronized boolean installFromJarFromMaven(final String objectName,
			final File artifactFile, final ComparableVersion version) {
		try {
			String destPath = artifactFile + "_extracted";
			File mainEntryFile = null;
			List<File> supplementaryEntryFiles = new ArrayList<File>();
			String expectedEntryName = getMainEntryNameForMavenJar(objectName,
					artifactFile, version);
			// If folder exist, delete it.
			FileUtils.delete(new File(destPath));

			JarFile jarFile = new JarFile(artifactFile);

			try {
				Enumeration<JarEntry> enums = jarFile.entries();
				while (enums.hasMoreElements()) {
					JarEntry entry = enums.nextElement();
					// if specified, we include all entries that match a given
					// pattern for that repository object type; otherwise, we
					// only match the main entry (e.g. the exact dataconfig
					// entry name)
					String entryMatchPattern;
					entryMatchPattern = getIncludedEntryNamePatternForMavenJar(
							objectName, artifactFile, version);
					if (entryMatchPattern == null)
						entryMatchPattern = expectedEntryName;
					if (entry.getName().matches(entryMatchPattern)) {
						new File(FileUtils.buildPath(destPath,
								expectedEntryName)).getParentFile().mkdirs();
						File toWrite = new File(
								FileUtils.buildPath(destPath, entry.getName()));
						if (entry.getName().matches(expectedEntryName))
							mainEntryFile = toWrite;
						else if (toWrite.isDirectory())
							continue;
						else
							supplementaryEntryFiles.add(toWrite);
						InputStream in = new BufferedInputStream(
								jarFile.getInputStream(entry));
						OutputStream out = new BufferedOutputStream(
								new FileOutputStream(toWrite));
						byte[] buffer = new byte[2048];
						for (;;) {
							int nBytes = in.read(buffer);
							if (nBytes <= 0) {
								break;
							}
							out.write(buffer, 0, nBytes);
						}
						out.flush();
						out.close();
						in.close();
					}
				}
			} finally {
				jarFile.close();
			}
			if (mainEntryFile == null)
				return false;

			String targetFilePath = getExtractedPathForMainEntryFromMavenJar(
					objectName, artifactFile, version);

			File targetFile = moveResolvedFilesIntoRepository(targetFilePath,
					mainEntryFile, supplementaryEntryFiles, version);

			registerInstalledMavenJar(targetFile);

			return true;
		} catch (IOException ex) {
			return false;
		}
	}

	protected synchronized File moveResolvedFilesIntoRepository(
			String targetFilePath, File mainEntryFile,
			List<File> supplementaryEntryFiles, ComparableVersion version)
			throws IOException {
		// rename the main entry file (e.g. the dataconfig file)
		targetFilePath = setCorrectVersionInformationToObjectFromJarFromMaven(
				targetFilePath, version);
		File targetFile = new File(targetFilePath);
		targetFile.getParentFile().mkdirs();
		mainEntryFile.renameTo(targetFile);

		// move the supplementary files into the same target directory
		for (File f : supplementaryEntryFiles)
			org.apache.commons.io.FileUtils.moveFileToDirectory(f,
					new File(targetFilePath).getParentFile(), false);
		return targetFile;
	}

	protected synchronized boolean registerInstalledMavenJar(final File file) {
		try {
			// invoke the corresponding finder class to the result
			// jar file
			IFinder<? extends RepositoryObject> finder = (IFinder<? extends RepositoryObject>) repository
					.getStaticObjectWithNameAndVersion(IFinder.class,
							"Finder: " + this.getEntityClass().getSimpleName());
			finder.doOnFileFound(file);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public final boolean hasVersionInName(final String name) {
		int lastIndex = name.lastIndexOf(":");
		if (lastIndex < 0)
			return false;
		return name.substring(lastIndex).matches(getVersionMatchString());
	}

	public final ComparableVersion getVersionFromName(String name) {
		// name = new File(name).getParentFile().getName();
		int lastIndex = name.lastIndexOf(":");
		if (lastIndex < 0)
			return null;
		return new ComparableVersion(name.substring(lastIndex + 1));
	}

	public final String insertVersionIntoName(String name,
			ComparableVersion version) {
		return name + ":" + version.toString();
	}

	public final String getWithoutVersionInName(String name) {
		return name.replaceAll(getVersionMatchString() + "$", "");
	}

	protected String setCorrectVersionInformationToObjectFromJarFromMaven(
			final String targetPath, final ComparableVersion version) {
		return targetPath;
	}

	protected String getMainEntryNameForMavenJar(final String objectName,
			final File artifactFile, final ComparableVersion version) {
		return null;
	}

	protected String getIncludedEntryNamePatternForMavenJar(
			final String objectName, final File artifactFile,
			final ComparableVersion version) {
		return null;
	}

	protected String getExtractedPathForMainEntryFromMavenJar(
			final String objectName, final File artifactFile,
			final ComparableVersion version) {
		return null;
	}

	protected String getMavenGroupIDForRepositoryObject(
			final String objectName) {
		return null;
	}

	protected String getMavenArtifactIDForRepositoryObject(
			final String objectName) {
		return null;
	}
}