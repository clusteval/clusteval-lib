/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.framework.repository;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.apache.maven.artifact.versioning.ComparableVersion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.clusteval.utils.IInMemoryListener;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * A {@link RepositoryObject} provides integrated functionalities in terms of
 * automatic handling by the {@link Repository} it is registered in.
 * 
 * <p>
 * Functionality of this repository registration includes
 * <ul>
 * <li>automatic detection of changes of repository objects</li>
 * <li>automatic notification of changes about other repository objects this
 * object listens to</li>
 * <li>notifications of other objects about changes of this object</li>
 * <li>central access to all objects of the framework in the repository</li>
 * <li>copy handling</li>
 * </ul>
 * 
 * @author Christian Wiwie
 */
public abstract class RepositoryObject
		implements
			IRepositoryObject,
			IInMemoryListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7144193500324028559L;

	/**
	 * The repository this object is registered in.
	 */
	protected transient IRepository repository;

	/**
	 * The changedate of this object can be used for identification and equality
	 * checks of objects.
	 */
	protected long changeDate;

	/**
	 * A set with all the listeners, that want to be informed about changes of
	 * this object.
	 */
	protected transient Set<IRepositoryListener> listener;

	/**
	 * The absolute path of this object is used for identification and equality
	 * checks of objects.
	 */
	protected File absPath;

	protected transient Logger log;

	/**
	 * If this object has been serialized it is stored in this attribute.
	 */
	protected ISerializableWrapperRepositoryObject<? extends IRepositoryObject> serialized;

	/**
	 * Instantiates a new repository object.
	 * 
	 * @param repository
	 *            The repository this object is registered in.
	 * @param changeDate
	 *            The changedate of this object can be used for identification
	 *            and equality checks of objects.
	 * @param absPath
	 *            The absolute path of this object is used for identification
	 *            and equality checks of objects.
	 */
	public RepositoryObject(final IRepository repository, final long changeDate,
			final File absPath) {
		super();
		this.repository = repository;
		this.changeDate = changeDate;
		this.listener = Collections
				.synchronizedSet(new HashSet<IRepositoryListener>());
		this.absPath = absPath;
		this.log = LoggerFactory.getLogger(this.getClass());
	}

	/**
	 * The copy constructor for repository objects.
	 * 
	 * @param other
	 *            The object to clone.
	 */
	public RepositoryObject(final RepositoryObject other) {
		this(other.repository, other.changeDate,
				new File(other.absPath.getAbsolutePath()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepositoryObject#clone()
	 */
	@Override
	public abstract IRepositoryObject clone();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepositoryObject#getRepository()
	 */
	@Override
	public IRepository getRepository() {
		return this.repository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepositoryObject#getAbsolutePath()
	 */
	@Override
	public String getAbsolutePath() {
		return absPath.getAbsolutePath();
	}

	protected final String getAbsPathVersionMatchString() {
		return "\\.v\\d+(-SNAPSHOT)?";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepositoryObject#getFile()
	 */
	@Override
	public File getFile() {
		return absPath;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepositoryObject#setAbsolutePath(java.
	 * io.File)
	 */
	@Override
	public void setAbsolutePath(final File absFilePath) {
		this.absPath = absFilePath;
		invalidateSerializedVersion();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepositoryObject#register()
	 */
	@Override
	public boolean register() throws RegisterException {
		return this.repository.register(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepositoryObject#unregister()
	 */
	@Override
	public boolean unregister() {
		return this.repository.unregister(this);
	}

	/**
	 * Do on register.
	 */
	protected void doOnRegister() {
		// by default nothing
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepositoryObject#getChangeDate()
	 */
	@Override
	public long getChangeDate() {
		return this.changeDate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepositoryObject#equals(java.lang.
	 * Object)
	 */
	@Override
	public boolean equals(Object obj) {
		RepositoryObject other = (RepositoryObject) obj;

		if (this instanceof IHasVersion) {
			if (other instanceof IHasVersion) {
				if (!((IHasVersion) this).getVersion()
						.equals(((IHasVersion) other).getVersion()))
					return false;
			} else
				return false;
		}

		return this.repository.equals(other.repository)
				&& ((this.absPath == null && other.absPath == null)
						|| this.absPath.equals(other.absPath));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepositoryObject#hashCode()
	 */
	@Override
	public int hashCode() {
		String s = Objects.toString(this.repository) + "_"
				+ this.absPath.toString();

		if (this instanceof IHasVersion) {
			s += "_" + ((IHasVersion) this).getVersion();
		}

		return s.hashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepositoryObject#copyTo(java.io.File)
	 */
	@Override
	public boolean copyTo(final File copyDestination) {
		return copyTo(copyDestination, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepositoryObject#copyTo(java.io.File,
	 * boolean)
	 */
	@Override
	public boolean copyTo(final File copyDestination, final boolean overwrite) {
		// by default we wait until the copied file is equal
		return copyTo(copyDestination, overwrite, true, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepositoryObject#copyTo(java.io.File,
	 * boolean, boolean, boolean)
	 */
	@Override
	public boolean copyTo(final File copyDestination, final boolean overwrite,
			final boolean wait, final boolean ensureVersionsForAllComponents) {
		try {
			if (!copyDestination.exists() || overwrite)
				org.apache.commons.io.FileUtils.copyFile(this.absPath,
						copyDestination);
			while (wait && !org.apache.commons.io.FileUtils
					.contentEquals(this.absPath, copyDestination)) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			if (ensureVersionsForAllComponents) {
				String fileContents = FileUtils
						.readStringFromFile(copyDestination.getAbsolutePath());
				String newFileContents = ensureVersionsForAllComponents(
						fileContents);
				FileUtils.writeStringToFile(copyDestination.getAbsolutePath(),
						newFileContents);
			}
		} catch (IOException e) {
			return false;
		}
		return true;
	}

	protected String ensureVersionsForAllComponents(final String fileContents) {
		// by default we dont do anything
		return fileContents;
	}

	protected String ensureVersionForComponent(final String fileContents,
			final IRepositoryObject component) {
		ComparableVersion version = Repository
				.getVersionOfObjectDynamicClass(component);
		return fileContents.replaceAll(
				String.format("%s(?!\\:)",
						component.getClass().getSimpleName()),
				String.format("%s:%s", component.getClass().getSimpleName(),
						version));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepositoryObject#moveTo(java.io.File)
	 */
	@Override
	public boolean moveTo(final File moveDestination) {
		return moveTo(moveDestination, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepositoryObject#moveTo(java.io.File,
	 * boolean)
	 */
	@Override
	public boolean moveTo(final File moveDest, final boolean overwrite) {
		try {
			if (!moveDest.exists() || overwrite) {
				org.apache.commons.io.FileUtils.moveFile(this.absPath,
						moveDest);
				setAbsolutePath(moveDest);
				this.notify(new RepositoryMoveEvent(this));
			}
		} catch (IOException e) {
			return false;
		} catch (RegisterException e) {
			e.printStackTrace();
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepositoryObject#moveToFolder(java.io.
	 * File)
	 */
	@Override
	public boolean moveToFolder(final File moveFolderDestination) {
		return moveToFolder(moveFolderDestination, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepositoryObject#moveToFolder(java.io.
	 * File, boolean)
	 */
	@Override
	public boolean moveToFolder(final File moveFolderDestination,
			final boolean overwrite) {
		File targetFile = new File(
				FileUtils.buildPath(moveFolderDestination.getAbsolutePath(),
						this.absPath.getName()));
		return moveTo(targetFile, overwrite);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepositoryObject#copyToFolder(java.io.
	 * File)
	 */
	@Override
	public boolean copyToFolder(final File copyFolderDestination) {
		return copyToFolder(copyFolderDestination, true, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepositoryObject#copyToFolder(java.io.
	 * File, boolean)
	 */
	@Override
	public boolean copyToFolder(final File copyFolderDestination,
			final boolean overwrite) {
		return copyToFolder(copyFolderDestination, overwrite, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepositoryObject#copyToFolder(java.io.
	 * File, boolean, boolean)
	 */
	@Override
	public boolean copyToFolder(final File copyFolderDestination,
			final boolean overwrite,
			final boolean ensureVersionsForAllComponents) {
		File targetFile = new File(
				FileUtils.buildPath(copyFolderDestination.getAbsolutePath(),
						this.absPath.getName()));
		return copyTo(targetFile, overwrite, true,
				ensureVersionsForAllComponents);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepositoryObject#addListener(de.
	 * clusteval.framework.repository.RepositoryListener)
	 */
	@Override
	public boolean addListener(final IRepositoryListener listener) {
		if (listener.equals(this))
			return false;
		return this.listener.add(listener);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepositoryObject#removeListener(de.
	 * clusteval.framework.repository.RepositoryListener)
	 */
	@Override
	public boolean removeListener(final IRepositoryListener listener) {
		return this.listener.remove(listener);
	}

	/**
	 * A helper method of {@link #notify(RepositoryEvent)}, in case this object
	 * needs to inform its listeners about the passed event.
	 * 
	 * @param event
	 *            The event related to this object which is going to be
	 *            propagated to the listeners of this object.
	 * @throws RegisterException
	 */
	private void notifyListener(final RepositoryEvent event)
			throws RegisterException {
		List<IRepositoryListener> toNotify = new ArrayList<IRepositoryListener>(
				this.listener);
		for (IRepositoryListener listener : toNotify) {
			listener.notify(event);
		}
		this.invalidateSerializedVersion();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.wiwie.wiutils.utils.RepositoryListener#notify(utils.RepositoryEvent)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IRepositoryObject#notify(de.clusteval.
	 * framework.repository.RepositoryEvent)
	 */
	@Override
	public void notify(RepositoryEvent e) throws RegisterException {
		if (e instanceof RepositoryReplaceEvent) {
			RepositoryReplaceEvent event = (RepositoryReplaceEvent) e;
			if (event.old.equals(this))
				// this object is going to be removed and replaced from the
				// repository
				this.notifyListener(event);
			else {
				// do something in subclasses
			}
		} else if (e instanceof RepositoryRemoveEvent) {
			RepositoryRemoveEvent event = (RepositoryRemoveEvent) e;
			if (event.old.equals(this)) {
				// this object is going to be removed and replaced from the
				// repository
				this.notifyListener(event);
			} else {
				// do something in subclasses
			}
		} else if (e instanceof RepositoryMoveEvent) {
			RepositoryMoveEvent event = (RepositoryMoveEvent) e;
			if (event.object.equals(this)) {
				// this object has been moved
				this.notifyListener(event);
			} else {
				// do something in subclasses
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepositoryObject#getLog()
	 */
	@Override
	public Logger getLog() {
		return log;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.toString(":");
	}

	@Override
	public String toString(final String versionSeparator) {
		return String.format("%s%s%s", this.getName(), versionSeparator,
				this.getVersion());
	}

	@Override
	public abstract ComparableVersion getVersion();

	@Override
	public abstract String getName();

	@Override
	public synchronized ISerializableWrapperRepositoryObject<? extends IRepositoryObject> asSerializable()
			throws RepositoryObjectSerializationException {
		if (this.serialized != null)
			return (SerializableWrapperRepositoryObject<IRepositoryObject>) this.serialized;
		ISerializableWrapperRepositoryObject<? extends IRepositoryObject> serializedResult = asSerializableInternal();
		this.serialized = serializedResult;
		return serializedResult;
	}

	protected abstract ISerializableWrapperRepositoryObject<? extends IRepositoryObject> asSerializableInternal()
			throws RepositoryObjectSerializationException;

	protected void invalidateSerializedVersion() {
		this.serialized = null;
	}

	@Override
	public boolean isSerializable() {
		return false;
	}
}
