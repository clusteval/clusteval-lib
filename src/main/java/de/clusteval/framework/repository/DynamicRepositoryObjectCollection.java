/**
 * 
 */
package de.clusteval.framework.repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.maven.artifact.versioning.ComparableVersion;
import org.rosuda.REngine.Rserve.RserveException;

import de.clusteval.framework.RLibraryNotLoadedException;
import de.clusteval.framework.RLibraryRequirement;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.DynamicComponentMissingVersionException;
import de.clusteval.utils.DynamicComponentVersion;
import de.clusteval.utils.UnsatisfiedRLibraryException;

/**
 * @author Christian Wiwie
 */
public class DynamicRepositoryObjectCollection
		extends
			RepositoryObjectCollection {

	protected DynamicRepositoryObjectCollection parent;

	/**
	 * A map containing all objects registered in this entity.
	 */
	protected Map<String, Map<ComparableVersion, List<IRepositoryObjectDynamicComponent>>> objects;
	protected Map<String, Map<ComparableVersion, Class<? extends IRepositoryObjectDynamicComponent>>> classes;

	public DynamicRepositoryObjectCollection(final IRepository repository,
			final Class<? extends IRepositoryObject> entityType,
			final DynamicRepositoryObjectCollection parent,
			final String basePath) {
		super(repository, entityType, basePath);
		this.parent = parent;
		this.objects = new HashMap<String, Map<ComparableVersion, List<IRepositoryObjectDynamicComponent>>>();
		this.classes = new HashMap<String, Map<ComparableVersion, Class<? extends IRepositoryObjectDynamicComponent>>>();
	}

	public synchronized Collection<Class<? extends IRepositoryObjectDynamicComponent>> getClasses() {
		Collection<Class<? extends IRepositoryObjectDynamicComponent>> result = new HashSet<Class<? extends IRepositoryObjectDynamicComponent>>();
		for (Map<ComparableVersion, Class<? extends IRepositoryObjectDynamicComponent>> map : this.classes
				.values())
			result.addAll(map.values());

		if (parent != null)
			result.addAll(parent.getClasses());

		return result;
	}

	public synchronized Map<String, Map<ComparableVersion, Class<? extends IRepositoryObjectDynamicComponent>>> getClassesWithNameAndVersion() {
		return this.classes;
	}

	public synchronized Map<Class<? extends IRepositoryObjectDynamicComponent>, ComparableVersion> getClassesWithVersions() {
		Map<Class<? extends IRepositoryObjectDynamicComponent>, ComparableVersion> result = new HashMap<Class<? extends IRepositoryObjectDynamicComponent>, ComparableVersion>();
		for (Map<ComparableVersion, Class<? extends IRepositoryObjectDynamicComponent>> map : this.classes
				.values())
			for (ComparableVersion version : map.keySet()) {
				result.put(map.get(version), version);
			}

		if (parent != null)
			result.putAll(parent.getClassesWithVersions());

		return result;
	}

	/**
	 * This method checks, whether there is an object registered, that is equal
	 * to the passed object and returns it.
	 * 
	 * <p>
	 * Equality is checked in terms of
	 * <ul>
	 * <li><b>object.hashCode == other.hashCode</b></li>
	 * <li><b>object.equals(other)</b></li>
	 * </ul>
	 * since internally the repository uses hash datastructures.
	 * 
	 * <p>
	 * By default the {@link IRepositoryObject#equals(Object)} method is only
	 * based on the absolute path of the repository object and the repositories
	 * of the two objects, this means two repository objects are considered the
	 * same if they are stored in the same repository and they have the same
	 * absolute path.
	 * 
	 * @param obj
	 * @return
	 * 
	 */
	public synchronized IRepositoryObjectDynamicComponent getRegisteredObject(
			final IRepositoryObjectDynamicComponent obj) {
		return this.getRegisteredObject(obj, true);
	}

	public synchronized IRepositoryObjectDynamicComponent getRegisteredObject(
			final IRepositoryObjectDynamicComponent object,
			final boolean ignoreChangeDate) {
		// get object without changedate
		IRepositoryObjectDynamicComponent other = null;

		for (IRepositoryObjectDynamicComponent elem : this.objects
				.get(object.getClass().getName())
				.get(Repository.getVersionOfObjectDynamicClass(object)))
			if (elem.equals(object)) {
				other = (IRepositoryObjectDynamicComponent) elem;
				break;
			}
		// inserted parent, 02.06.2012
		if (other == null && parent != null)
			return parent.getRegisteredObject(object, ignoreChangeDate);
		else if (ignoreChangeDate || other == null)
			return other;
		else if (other.getChangeDate() == object.getChangeDate()) {
			return other;
		}
		return object;
	}

	/**
	 * This method registers a new class. It is only registered, if it was not
	 * before.
	 * 
	 * @param object
	 *            The new object to register.
	 * @return True, if the new object has been registered.
	 * @throws DynamicComponentMissingVersionException
	 * @throws DynamicComponentInitializationException
	 */
	public synchronized boolean registerClass(
			final Class<? extends IRepositoryObjectDynamicComponent> object)
			throws DynamicComponentMissingVersionException,
			DynamicComponentInitializationException {
		String className = object.getName();
		ComparableVersion classVersion = Repository
				.getVersionOfDynamicClass(object);
		if (isClassRegistered(object)) {
			// first remove the old class
			unregisterClass(this.classes.get(className).get(classVersion));
		}
		if (!this.classes.containsKey(className))
			this.classes.put(className,
					new HashMap<ComparableVersion, Class<? extends IRepositoryObjectDynamicComponent>>());
		this.classes.get(className).put(classVersion, object);

		// is this right, to always put an empty list even though there was
		// an old class before?
		if (!this.objects.containsKey(className))
			this.objects.put(className,
					new HashMap<ComparableVersion, List<IRepositoryObjectDynamicComponent>>());
		this.objects.get(className).put(classVersion,
				Collections.synchronizedList(
						new ArrayList<IRepositoryObjectDynamicComponent>()));

		try {
			if (!ensureLibraries(object))
				return false;
		} catch (InterruptedException e) {
			return false;
		}

		if (!this.repository.getLoadedClasses().containsKey(className))
			this.repository.getLoadedClasses().put(className,
					new HashMap<ComparableVersion, Class<? extends IRepositoryObjectDynamicComponent>>());
		this.repository.getLoadedClasses().get(className).put(classVersion,
				object);

		this.repository.info("Dynamic class registered: "
				+ object.getSimpleName() + " (" + classVersion + ")");

		this.repository.getSqlCommunicator().register(object);

		return true;
	}

	/**
	 * This method checks whether a class is registered in this repository.
	 * 
	 * @param classToRegister
	 *            The class to look up.
	 * @return True, if the class was registered.
	 * @throws DynamicComponentMissingVersionException
	 */
	public synchronized boolean isClassRegistered(
			final Class<? extends IRepositoryObjectDynamicComponent> classToRegister)
			throws DynamicComponentMissingVersionException {
		return this.isClassRegistered(classToRegister.getName(),
				Repository.getVersionOfDynamicClass(classToRegister));
	}

	public synchronized boolean isClassRegistered(final String className) {
		return (this.classes.containsKey(className)) || (this.parent != null
				&& this.parent.isClassRegistered(className));
	}

	/**
	 * This method checks whether a class with the given name is registered in
	 * this repository.
	 * 
	 * @param className
	 *            The name of the class to look up.
	 * @return True, if the class was registered.
	 */
	public synchronized boolean isClassRegistered(final String className,
			final ComparableVersion version) {
		return (this.classes.containsKey(className)
				&& this.classes.get(className).containsKey(version))
				|| (this.parent != null
						&& this.parent.isClassRegistered(className, version));
	}

	/**
	 * 
	 * This method registers a new object.
	 * 
	 * <p>
	 * First by invoking {@link #getRegisteredObject(IRepositoryObject)} the
	 * method checks, whether another object equalling the new object has been
	 * registered before.
	 * 
	 * <p>
	 * If there is no old equalling object, the new object is simply registered
	 * at the repository.
	 * 
	 * <p>
	 * If there is an old equalling object, their <b>changedates</b> are
	 * compared. The new object is only registered, if the changedate of the new
	 * object is newer than the changedate of the old object. If the changedate
	 * is newer, the new object is registered at the repository and a
	 * {@link RepositoryReplaceEvent} is being thrown. This event tells the old
	 * object and all its listeners in {@link IRepositoryObject#listener}, that
	 * it has been replaced by the new object. This allows all objects to update
	 * their references to the old object to the new object.
	 * 
	 * <p>
	 * The method also tells the {@link #repository.sqlCommunicator} of the
	 * repository, that a new object has been registered and causes him, to
	 * handle the new object.
	 * 
	 * @param object
	 * @return
	 * @throws RegisterException
	 */
	@Override
	public synchronized boolean register(final IRepositoryObject object)
			throws RegisterException {
		if (!(object instanceof IRepositoryObjectDynamicComponent))
			return false;
		this.objects.get(object.getClass().getName())
				.get(new ComparableVersion(object.getClass()
						.getAnnotation(DynamicComponentVersion.class)
						.version()))
				.add((IRepositoryObjectDynamicComponent) object);
		// TODO: check duplicates in list?
		return true;
	}

	/**
	 * 
	 * This method unregisters the passed object.
	 * 
	 * <p>
	 * If the object has been registered before and was unregistered now, this
	 * method tells the sql communicator such that he can also handle the
	 * removal of the object.
	 * 
	 * @param object
	 * @return
	 */
	@Override
	public synchronized boolean unregister(final IRepositoryObject object) {
		if (!(object instanceof IRepositoryObjectDynamicComponent))
			return false;
		boolean result = this.objects.get(object.getClass().getName())
				.get(new ComparableVersion(object.getClass()
						.getAnnotation(DynamicComponentVersion.class)
						.version()))
				.remove(object);
		if (result) {
			try {
				object.notify(new RepositoryRemoveEvent(object));
			} catch (RegisterException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * This method unregisters the passed object.
	 * 
	 * <p>
	 * If the object has been registered before and was unregistered now, this
	 * method tells the sql communicator such that he can also handle the
	 * removal of the object.
	 * 
	 * @param c
	 *            The object to be removed.
	 * @return True, if the object was remved successfully
	 */
	public synchronized boolean unregisterClass(
			final Class<? extends IRepositoryObjectDynamicComponent> c) {
		try {
			String className = c.getName();
			ComparableVersion classVersion = Repository
					.getVersionOfDynamicClass(c);
			boolean result = this.classes.containsKey(className)
					? this.classes.get(className).remove(classVersion) != null
					: false;
			if (result) {
				if (this.printOnRegister)
					this.repository.info(
							String.format("Dynamic class removed: %s (%s)",
									c.getSimpleName(), classVersion));
				// we inform all listeners about the new class. that
				// means those objects are deleted such that new instances
				// instances
				// can be created using the new class.

				for (IRepositoryObject object : Collections
						.synchronizedList(new ArrayList<IRepositoryObject>(
								(List<IRepositoryObjectDynamicComponent>) objects
										.get(className).get(classVersion)))) {
					object.unregister();
				}

				this.repository.getLoadedClasses().get(className)
						.remove(classVersion);

				this.repository.getSqlCommunicator().unregister(c);
			}
			return result;
		} catch (DynamicComponentMissingVersionException e) {
			// this class can't have been loaded because it does not have a
			// version
			return false;
		}
	}

	/**
	 * This method assumes, that the class that is passed is currently
	 * registered in this repository.
	 * 
	 * <p>
	 * If the R libraries are not satisfied, the class is removed from the
	 * repository.
	 * 
	 * @param classObject
	 *            The class for which we want to ensure R library dependencies.
	 * @return True, if all R library dependencies are fulfilled.
	 * @throws InterruptedException
	 * @throws UnsatisfiedRLibraryException
	 */
	protected synchronized boolean ensureLibraries(
			final Class<? extends IRepositoryObjectDynamicComponent> classObject)
			throws InterruptedException {
		if (classObject.isAnnotationPresent(RLibraryRequirement.class)) {
			String[] requiredLibraries = classObject
					.getAnnotation(RLibraryRequirement.class)
					.requiredRLibraries();

			String className = classObject.getName();
			ComparableVersion classVersion = new ComparableVersion(classObject
					.getAnnotation(DynamicComponentVersion.class).version());

			// ensure that all R libraries are available
			IMyRengine rEngine;
			try {
				if (MyRengine.IS_R_AVAILABLE) {
					rEngine = this.repository.getRengineForCurrentThread();

					// ensure that all R libraries are available
					for (String libName : requiredLibraries)
						try {
							rEngine.loadLibrary(libName,
									classObject.getSimpleName());
							// first we clear the old exceptions for this
							// class
							this.repository.clearMissingRLibraries(
									classObject.getName());
						} catch (RLibraryNotLoadedException e) {
							if (this.repository.addMissingRLibraryException(e))
								this.repository.warn(String.format(
										"\"%s (%s)\" could not be loaded due to an unsatisfied R library dependency: %s",
										classObject.getSimpleName(),
										classVersion, libName));

							this.classes.get(className).remove(classVersion);
							return false;
						}
					return true;
				} else {
					throw new RserveException(null, className);
				}
			} catch (RserveException e) {
				if (this.repository.addMissingRLibraryException(
						new RLibraryNotLoadedException(classObject.getName(),
								"R")))
					this.repository.warn(String.format(
							"\"%s (%s)\" could not be loaded since it requires R and no connection could be established.",
							classObject.getSimpleName(), classVersion));
				this.classes.get(className).remove(classVersion);
				return false;
			}
		}
		return true;
	}

	/**
	 * This method looks up and returns (if it exists) the newest version of the
	 * class with the given name.
	 * 
	 * @param className
	 *            The name of the class.
	 * @return The class with the given name or null, if it does not exist.
	 */
	public synchronized Class<? extends IRepositoryObjectDynamicComponent> getRegisteredClass(
			final String className) {
		Map<ComparableVersion, Class<? extends IRepositoryObjectDynamicComponent>> map = this.classes
				.get(className);
		ComparableVersion version = null;
		Class<? extends IRepositoryObjectDynamicComponent> result = null;
		// find the newest version of this class
		if (map != null && map.size() > 0) {
			version = Collections.max(map.keySet());
			result = map.get(version);
		}
		if (result == null && parent != null)
			result = this.parent.getRegisteredClass(className, version);
		return result;
	}

	/**
	 * This method looks up and returns (if it exists) the class with the given
	 * name.
	 * 
	 * @param className
	 *            The name of the class.
	 * @return The class with the given name or null, if it does not exist.
	 */
	public synchronized Class<? extends IRepositoryObjectDynamicComponent> getRegisteredClass(
			final String className, final ComparableVersion version) {
		Class<? extends IRepositoryObjectDynamicComponent> result = this.classes
				.containsKey(className)
						? this.classes.get(className).get(version)
						: null;
		if (result == null && parent != null)
			result = this.parent.getRegisteredClass(className, version);
		return result;
	}
}