/**
 * 
 */
package de.clusteval.framework.repository.migrate;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import de.clusteval.framework.repository.IRepository;
import de.clusteval.run.runresult.IRunResult;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * @author Christian Wiwie
 *
 */
public class RepositoryMigration2_3 extends RepositoryMigration {

	protected IRepository repository;

	/**
	 * 
	 */
	public RepositoryMigration2_3() {
		super(2, 3);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.migrate.RepositoryMigration#doPerform()
	 */
	@Override
	public boolean doApply(final IRepository repository) {
		boolean result = true;
		this.repository = repository;
		if (!changeDateFormatInRunResultFolders())
			return false;

		return result;
	}

	/**
	 * @return
	 */
	private boolean changeDateFormatInRunResultFolders() {
		final String oldDateFormatString = "MM_dd_yyyy-HH_mm_ss";
		final String newDateFormatString = "yyyy_MM_dd-HH_mm_ss";

		String basePath = repository.getBasePath(IRunResult.class);
		for (File resultFolder : new File(basePath).listFiles()) {
			if (!resultFolder.isDirectory())
				continue;

			// find the right portion of the folder name
			int dashCount = 0;
			int index = -1;
			while (dashCount < 5) {
				index = index + resultFolder.getName().substring(index + 1)
						.indexOf("_") + 1;
				dashCount++;
			}

			String dateString = resultFolder.getName().substring(0, index);
			String nonDateName = resultFolder.getName().substring(index);

			try {
				Date parse = new SimpleDateFormat(oldDateFormatString)
						.parse(dateString);

				String newDateString = new SimpleDateFormat(newDateFormatString)
						.format(parse);

				String newFolderName = newDateString + nonDateName;

				log.info(String.format("Rename result folder %s to %s",
						resultFolder.getName(), newFolderName));

				File target = new File(FileUtils.buildPath(
						resultFolder.getParentFile().getAbsolutePath(),
						newFolderName));

				resultFolder.renameTo(target);
			} catch (ParseException e) {
				// check if that folder had already been renamed to the new
				// format
				try {
					Date parse = new SimpleDateFormat(newDateFormatString)
							.parse(dateString);
					log.info(String.format(
							"Folder already has the right format -> not renaming"));

					return true;
				} catch (ParseException e1) {
					log.error(String.format(
							"Could not parse date format of result folder %s",
							resultFolder.getName()));
					return false;
				}

			}
		}

		return true;
	}

	protected Set<String> originalProgramConfigNames;

	protected Set<String> originalDataConfigNames;

}
