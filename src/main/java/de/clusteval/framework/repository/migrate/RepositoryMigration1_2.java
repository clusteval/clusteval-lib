/**
 * 
 */
package de.clusteval.framework.repository.migrate;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.DataSetAttributeParser;
import de.clusteval.data.dataset.IDataSet;
import de.clusteval.data.dataset.IDataSetConfig;
import de.clusteval.data.goldstandard.IGoldStandard;
import de.clusteval.data.goldstandard.IGoldStandardConfig;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IStandaloneProgram;
import de.clusteval.run.IRun;
import de.clusteval.run.runresult.IRunResult;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * @author Christian Wiwie
 *
 */
public class RepositoryMigration1_2 extends RepositoryMigration {

	protected IRepository repository;

	/**
	 * 
	 */
	public RepositoryMigration1_2() {
		super(1, 2);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.migrate.RepositoryMigration#doPerform()
	 */
	@Override
	public boolean doApply(final IRepository repository) {
		boolean result = true;
		this.repository = repository;
		if (!appendVersionToDatasetFileNames())
			return false;
		if (!appendVersionToGoldStandardFileNames())
			return false;
		if (!appendVersionToRunFileNames())
			return false;
		if (!appendVersionToStandaloneProgramsParentFolderNames())
			return false;
		if (!appendVersionToProgramConfigFileNames())
			return false;
		if (!appendVersionToDataConfigFileNames())
			return false;
		if (!appendVersionToDataSetConfigFileNames())
			return false;
		if (!appendVersionToGoldStandardConfigFileNames())
			return false;

		String basePath = this.repository.getBasePath(IRunResult.class);
		for (File f : new File(basePath).listFiles()) {
			if (!appendVersionToRunResultGoldStandardFileNames(f))
				return false;
			if (!appendVersionToRunResultRunFileNames(f))
				return false;
			if (!appendVersionToRunResultProgramConfigFileNames(f))
				return false;
			if (!appendVersionToRunResultDataConfigFileNames(f))
				return false;
			if (!appendVersionToRunResultDataSetConfigFileNames(f))
				return false;
			if (!appendVersionToRunResultGoldStandardConfigFileNames(f))
				return false;
			if (!appendVersionToRunResultDatasetFileNames(f))
				return false;
		}

		return result;
	}

	/**
	 * 
	 */
	protected boolean appendVersionToRunResultGoldStandardConfigFileNames(final File runResultDir) {
		this.log.info("appendVersionToRunResultGoldStandardConfigFileNames");

		String basePath = FileUtils.buildPath(runResultDir.getAbsolutePath(), "configs");

		this.log.info("Checking for goldstandard configs in " + basePath);

		for (File f : new File(basePath).listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".gsconfig");
			}
		})) {
			String targetName = f.getAbsolutePath().replaceAll(".gsconfig$", ".v1.gsconfig");
			this.log.info("Renaming '{}' -> '{}'", f, targetName);
			if (!onlySimulate) {
				f.renameTo(new File(targetName));
			}
		}

		return true;
	}

	/**
	 * 
	 */
	protected boolean appendVersionToRunResultDataSetConfigFileNames(final File runResultDir) {
		this.log.info("appendVersionToRunResultDataSetConfigFileNames");

		String basePath = FileUtils.buildPath(runResultDir.getAbsolutePath(), "configs");

		this.log.info("Checking for data set configs in " + basePath);

		for (File f : new File(basePath).listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".dsconfig");
			}
		})) {
			String targetName = f.getAbsolutePath().replaceAll(".dsconfig$", ".v1.dsconfig");
			this.log.info("Renaming '{}' -> '{}'", f, targetName);
			if (!onlySimulate) {
				f.renameTo(new File(targetName));
			}
		}

		return true;
	}

	/**
	 * 
	 */
	protected boolean appendVersionToRunResultDataConfigFileNames(final File runResultDir) {
		this.log.info("appendVersionToRunResultDataConfigFileNames");

		String basePath = FileUtils.buildPath(runResultDir.getAbsolutePath(), "configs");

		this.log.info("Checking for data configs in " + basePath);

		for (File f : new File(basePath).listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".dataconfig");
			}
		})) {
			String targetName = f.getAbsolutePath().replaceAll(".dataconfig$", ".v1.dataconfig");
			this.log.info("Renaming '{}' -> '{}'", f, targetName);
			if (!onlySimulate) {
				f.renameTo(new File(targetName));
			}
		}

		return true;
	}

	protected Set<String> originalProgramConfigNames;

	/**
	 * 
	 */
	protected boolean appendVersionToRunResultProgramConfigFileNames(final File runResultDir) {
		this.log.info("appendVersionToRunResultProgramConfigFileNames");

		originalProgramConfigNames = new HashSet<String>();

		String basePath = FileUtils.buildPath(runResultDir.getAbsolutePath(), "configs");

		this.log.info("Checking for program configs in " + basePath);

		for (File f : new File(basePath).listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".config");
			}
		})) {
			String targetName = f.getAbsolutePath().replaceAll(".config$", ".v1.config");
			this.log.info("Renaming '{}' -> '{}'", f, targetName);
			originalProgramConfigNames.add(f.getName().replaceAll(".config$", ""));
			if (!onlySimulate) {
				f.renameTo(new File(targetName));
			}
		}

		return true;
	}

	/**
	 * 
	 */
	protected boolean appendVersionToRunResultRunFileNames(final File runResultDir) {
		this.log.info("appendVersionToRunResultRunFileNames");

		String basePath = FileUtils.buildPath(runResultDir.getAbsolutePath(), "configs");

		this.log.info("Checking for runs in " + basePath);

		for (File f : new File(basePath).listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".run");
			}
		})) {
			String targetName = f.getAbsolutePath().replaceAll(".run$", ".v1.run");
			this.log.info("Renaming '{}' -> '{}'", f, targetName);
			if (!onlySimulate) {
				f.renameTo(new File(targetName));
			}
		}

		return true;
	}

	/**
	 * 
	 */
	protected boolean appendVersionToRunResultGoldStandardFileNames(final File runResultDir) {
		this.log.info("appendVersionToRunResultGoldStandardParentFolderNames");

		String basePath = FileUtils.buildPath(runResultDir.getAbsolutePath(), "goldstandards");

		this.log.info("Checking for gold standard folders in " + basePath);

		for (File subfolder : new File(basePath).listFiles()) {
			if (!subfolder.isDirectory())
				continue;
			for (File f : subfolder.listFiles()) {
				if (!f.isFile())
					continue;
				String targetName = f.getAbsolutePath() + ".v1";
				this.log.info("Renaming '{}' -> '{}'", f, targetName);
				if (!onlySimulate) {
					f.renameTo(new File(targetName));
				}
			}
		}

		return true;
	}

	/**
	 * 
	 */
	protected boolean appendVersionToRunResultDatasetFileNames(final File runResultDir) {
		this.log.info("appendVersionToRunResultDatasetParentFolderNames");

		String basePath = FileUtils.buildPath(runResultDir.getAbsolutePath(), "inputs");

		this.log.info("Checking for gold standard folders in " + basePath);

		for (File f : new File(basePath).listFiles()) {
			if (!(f.isDirectory()))
				continue;

			// rename data set file
			String targetName;
			for (File subfolder : f.listFiles()) {
				if (!subfolder.isDirectory())
					continue;
				for (File f2 : subfolder.listFiles()) {
					if (!f2.isFile())
						continue;

					DataSetAttributeParser p;
					try {
						p = new DataSetAttributeParser(f2.getAbsolutePath());
						p.process();
					} catch (IOException e) {
						continue;
					}
					if (p.getAttributeValues().size() == 0)
						continue;

					targetName = f2.getAbsolutePath() + ".v1";
					this.log.info("Renaming '{}' -> '{}'", f2, targetName);
					if (!onlySimulate) {
						f2.renameTo(new File(targetName));
					}
				}
			}

			// find out which program config and data config this corresponds to
			String pc = null;
			String dc = null;
			// if we have no program configs, its an analysis run
			if (originalProgramConfigNames.isEmpty()) {
				targetName = f.getAbsolutePath() + ".v1";
			} else {
				for (String origPC : this.originalProgramConfigNames) {
					for (String origDC : this.originalDataConfigNames) {
						if (f.getName().equals(origPC + "_" + origDC)) {
							dc = origDC;
							pc = origPC;
							break;
						}
					}
					if (pc != null && dc != null)
						break;
				}
				if (pc == null || dc == null) {
					this.log.warn(
							"Cannot find corresponding data config ({}) or program config ({}) for data set parent folder name '{}' -> Skipping renaming",
							new Object[]{Objects.toString(dc), Objects.toString(pc), f.getAbsolutePath()});
					continue;
				}
				targetName = FileUtils.buildPath(f.getParentFile().getAbsolutePath(),
						f.getName().replaceFirst(pc, pc + ".v1").replaceFirst(dc, dc + ".v1"));
			}
			this.log.info("Renaming '{}' -> '{}'", f, targetName);
			if (!onlySimulate) {
				f.renameTo(new File(targetName));
			}
		}
		return true;
	}

	/**
	 * 
	 */
	protected boolean appendVersionToGoldStandardConfigFileNames() {
		this.log.info("appendVersionToGoldStandardConfigFileNames");

		String basePath = this.repository.getBasePath(IGoldStandardConfig.class);

		this.log.info("Checking for goldstandard configs in " + basePath);

		for (File f : new File(basePath).listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".gsconfig");
			}
		})) {
			String targetName = f.getAbsolutePath().replaceAll(".gsconfig$", ".v1.gsconfig");
			this.log.info("Renaming '{}' -> '{}'", f, targetName);
			if (!onlySimulate) {
				f.renameTo(new File(targetName));
			}
		}

		return true;
	}

	/**
	 * 
	 */
	protected boolean appendVersionToDataSetConfigFileNames() {
		this.log.info("appendVersionToDataSetConfigFileNames");

		String basePath = this.repository.getBasePath(IDataSetConfig.class);

		this.log.info("Checking for data set configs in " + basePath);

		for (File f : new File(basePath).listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".dsconfig");
			}
		})) {
			String targetName = f.getAbsolutePath().replaceAll(".dsconfig$", ".v1.dsconfig");
			this.log.info("Renaming '{}' -> '{}'", f, targetName);
			if (!onlySimulate) {
				f.renameTo(new File(targetName));
			}
		}

		return true;
	}

	protected Set<String> originalDataConfigNames;

	/**
	 * 
	 */
	protected boolean appendVersionToDataConfigFileNames() {
		this.log.info("appendVersionToDataConfigFileNames");

		originalDataConfigNames = new HashSet<String>();

		String basePath = this.repository.getBasePath(IDataConfig.class);

		this.log.info("Checking for data configs in " + basePath);

		for (File f : new File(basePath).listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".dataconfig");
			}
		})) {
			String targetName = f.getAbsolutePath().replaceAll(".dataconfig$", ".v1.dataconfig");
			this.log.info("Renaming '{}' -> '{}'", f, targetName);
			originalDataConfigNames.add(f.getName().replaceAll(".dataconfig$", ""));
			if (!onlySimulate) {
				f.renameTo(new File(targetName));
			}
		}

		return true;
	}

	/**
	 * 
	 */
	protected boolean appendVersionToProgramConfigFileNames() {
		this.log.info("appendVersionToProgramConfigFileNames");

		String basePath = this.repository.getBasePath(IProgramConfig.class);

		this.log.info("Checking for program configs in " + basePath);

		for (File f : new File(basePath).listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".config");
			}
		})) {
			String targetName = f.getAbsolutePath().replaceAll(".config$", ".v1.config");
			this.log.info("Renaming '{}' -> '{}'", f, targetName);
			if (!onlySimulate) {
				f.renameTo(new File(targetName));
			}
		}

		return true;
	}

	/**
	 * 
	 */
	protected boolean appendVersionToStandaloneProgramsParentFolderNames() {
		this.log.info("appendVersionToStandaloneProgramsParentFolderNames");

		this.log.info("Checking for standalone programs in " + this.repository.getBasePath(IStandaloneProgram.class));

		for (File subfolder : new File(this.repository.getBasePath(IStandaloneProgram.class)).listFiles()) {
			if (!subfolder.isDirectory() || subfolder.getName().equals("configs"))
				continue;
			String targetName = subfolder.getAbsolutePath() + ".v1";
			this.log.info("Renaming '{}' -> '{}'", subfolder, targetName);
			if (!onlySimulate) {
				subfolder.renameTo(new File(targetName));
			}
		}

		return true;
	}

	/**
	 * 
	 */
	protected boolean appendVersionToRunFileNames() {
		this.log.info("appendVersionToRunFileNames");

		String basePath = this.repository.getBasePath(IRun.class);

		this.log.info("Checking for runs in " + basePath);

		for (File f : new File(basePath).listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".run");
			}
		})) {
			String targetName = f.getAbsolutePath().replaceAll(".run$", ".v1.run");
			this.log.info("Renaming '{}' -> '{}'", f, targetName);
			if (!onlySimulate) {
				f.renameTo(new File(targetName));
			}
		}

		return true;
	}

	/**
	 * 
	 */
	protected boolean appendVersionToGoldStandardFileNames() {
		this.log.info("appendVersionToGoldStandardFileNames");

		this.log.info("Checking for gold standards in " + this.repository.getBasePath(IGoldStandard.class));

		for (File subfolder : new File(this.repository.getBasePath(IGoldStandard.class)).listFiles()) {
			if (!subfolder.isDirectory() || subfolder.getName().equals("configs"))
				continue;

			for (File f : subfolder.listFiles()) {
				if (!f.isFile())
					continue;
				String targetName = f.getAbsolutePath() + ".v1";
				this.log.info("Renaming '{}' -> '{}'", f, targetName);
				if (!onlySimulate) {
					f.renameTo(new File(targetName));
				}
			}
		}

		return true;
	}

	protected boolean appendVersionToDatasetFileNames() {
		this.log.info("appendVersionToDatasetFileNames");

		this.log.info("Checking for data sets in " + this.repository.getBasePath(IDataSet.class));

		for (File subfolder : new File(this.repository.getBasePath(IDataSet.class)).listFiles()) {
			if (!subfolder.isDirectory() || subfolder.getName().equals("configs"))
				continue;

			for (File f : subfolder.listFiles()) {
				if (!f.isFile())
					continue;

				DataSetAttributeParser p;
				try {
					p = new DataSetAttributeParser(f.getAbsolutePath());
					p.process();
				} catch (IOException e) {
					continue;
				}
				if (p.getAttributeValues().size() == 0)
					continue;

				String targetName = f.getAbsolutePath() + ".v1";
				this.log.info("Renaming '{}' -> '{}'", f, targetName);
				if (!onlySimulate) {
					f.renameTo(new File(targetName));
				}
			}
		}

		return true;
	}

}
