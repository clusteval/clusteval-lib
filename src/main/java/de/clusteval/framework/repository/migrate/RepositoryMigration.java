/**
 * 
 */
package de.clusteval.framework.repository.migrate;

import java.io.File;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalINIConfiguration;
import org.apache.commons.configuration.SubnodeConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.clusteval.framework.repository.IRepository;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * @author Christian Wiwie
 *
 */
public abstract class RepositoryMigration {

	protected Logger log;
	protected int sourceVersion;
	protected int targetVersion;
	protected boolean onlySimulate;

	/**
	 * 
	 */
	public RepositoryMigration(final int sourceVersion, final int targetVersion) {
		super();
		this.log = LoggerFactory.getLogger(this.getClass());
		this.sourceVersion = sourceVersion;
		this.targetVersion = targetVersion;
		// TODO: for now
		this.onlySimulate = false;
	}
	
	
	/**
	 * @param onlySimulate the onlySimulate to set
	 */
	public void setOnlySimulate(boolean onlySimulate) {
		this.onlySimulate = onlySimulate;
	}

	/**
	 * @return the sourceVersion
	 */
	public int getSourceVersion() {
		return sourceVersion;
	}

	/**
	 * @return the targetVersion
	 */
	public int getTargetVersion() {
		return targetVersion;
	}

	/**
	 * @return
	 */
	public final boolean apply(final IRepository repository) {
		log.info(String.format("Applying repository migration %s (version %d -> %d)", this.getClass().getSimpleName(),
				sourceVersion, targetVersion));

		boolean result = doApply(repository);
		if (!result) {
			log.warn(String.format("Migration %s unsuccessful", this.getClass().getSimpleName()));
			return result;
		}
		log.info(String.format("Migration %s successful", this.getClass().getSimpleName()));
		result &= afterApply(repository);
		return result;
	}

	public abstract boolean doApply(final IRepository repository);

	protected boolean afterApply(final IRepository repository) {
		log.info("Updating repository version in repository.config");
		try {
			File f = new File(FileUtils.buildPath(repository.getBasePath(), "repository.config"));
			HierarchicalINIConfiguration props;
			if (f.exists())
				props = new HierarchicalINIConfiguration(f);
			else
				props = new HierarchicalINIConfiguration();
			SubnodeConfiguration repoSection = props.getSection("repository");
			repoSection.setProperty("version", targetVersion);
			if (f.exists())
				props.save();
			else
				props.save(f);
		} catch (ConfigurationException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
