/**
 * 
 */
package de.clusteval.framework.repository;

import java.io.File;
import java.io.IOException;
import java.nio.file.attribute.PosixFilePermission;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

import org.apache.maven.artifact.versioning.ComparableVersion;

import com.google.common.io.Files;

import de.clusteval.program.IStandaloneProgram;
import de.clusteval.program.StandaloneProgram;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * @author Christian Wiwie
 *
 */
public class StandaloneProgramCollection
		extends
			StaticRepositoryObjectCollection {

	/**
	 * 
	 */
	public StandaloneProgramCollection(final IRepository repository,
			final Class<? extends IRepositoryObject> entityClass,
			final StaticRepositoryObjectCollection parent,
			final String basePath) {
		super(repository, entityClass, parent, basePath);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.StaticRepositoryEntity#isVersioned()
	 */
	@Override
	public boolean isVersioned() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.StaticRepositoryEntity#
	 * canBeResolvedWithMaven()
	 */
	@Override
	protected boolean canBeResolvedWithMaven() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.StaticRepositoryObjectCollection#
	 * getPathOfExtractedObjectFromJarFromMaven(java.lang.String, java.io.File,
	 * org.apache.maven.artifact.versioning.ComparableVersion)
	 */
	@Override
	protected String getExtractedPathForMainEntryFromMavenJar(String objectName,
			File artifactFile, ComparableVersion version) {
		String targetDir = repository.getBasePath(this.getEntityClass());
		String targetFilePath = FileUtils.buildPath(targetDir, objectName);
		return targetFilePath;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.StaticRepositoryObjectCollection#
	 * getPathOfObjectInJarFromMaven(java.lang.String, java.io.File,
	 * org.apache.maven.artifact.versioning.ComparableVersion)
	 */
	@Override
	protected String getMainEntryNameForMavenJar(String objectName,
			File artifactFile, ComparableVersion version) {
		return "programs/" + objectName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.StaticRepositoryObjectCollection#
	 * getIncludedEntryNamePatternForMavenJar(java.lang.String, java.io.File,
	 * org.apache.maven.artifact.versioning.ComparableVersion)
	 */
	@Override
	protected String getIncludedEntryNamePatternForMavenJar(String objectName,
			File artifactFile, ComparableVersion version) {
		return String.format("programs/%s/.*",
				objectName.substring(0, objectName.indexOf("/")));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.StaticRepositoryObjectCollection#
	 * setCorrectVersionInformationToObjectFromJarFromMaven(java.lang.String,
	 * org.apache.maven.artifact.versioning.ComparableVersion)
	 */
	@Override
	protected String setCorrectVersionInformationToObjectFromJarFromMaven(
			String targetPath, ComparableVersion version) {
		// remove version information if it is present
		targetPath = targetPath.replaceAll("\\.v\\d+(-SNAPSHOT)?$", "");
		// append version of the artifact to the filename
		targetPath += String.format(".v%s", version.toString());
		return targetPath;
	}

	@Override
	protected String getMavenGroupIDForRepositoryObject(
			final String objectName) {
		return String.format("%s.%s",
				this.getEntityClass().getPackage().getName(),
				objectName.substring(0, objectName.lastIndexOf("/")));
	}

	@Override
	protected String getMavenArtifactIDForRepositoryObject(
			final String objectName) {
		return objectName.substring(objectName.lastIndexOf("/") + 1);
	}

	protected File moveResolvedFilesIntoRepository(String targetFilePath,
			File mainEntryFile, List<File> supplementaryEntryFiles,
			ComparableVersion version) throws IOException {
		// make sure that the parent directory of the executable contains
		// the version information
		File targetDirectory = new File(
				setCorrectVersionInformationToObjectFromJarFromMaven(
						new File(targetFilePath).getParentFile()
								.getAbsolutePath(),
						version));
		targetDirectory.mkdirs();
		// and backpropagate it to the executable path
		targetFilePath = FileUtils.buildPath(targetDirectory.getAbsolutePath(),
				new File(targetFilePath).getName());

		// rename the main entry file
		File targetFile = new File(targetFilePath);
		Files.copy(mainEntryFile, targetFile);

		java.nio.file.Files.setPosixFilePermissions(targetFile.toPath(),
				new HashSet<>(Arrays.asList(PosixFilePermission.OWNER_EXECUTE,
						PosixFilePermission.OWNER_READ,
						PosixFilePermission.OWNER_WRITE)));

		// move the supplementary files into the same target directory
		for (File f : supplementaryEntryFiles)
			org.apache.commons.io.FileUtils.copyFileToDirectory(f,
					targetDirectory, false);
		return targetFile;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.StaticRepositoryObjectCollection#
	 * hasChanged(de.clusteval.framework.repository.IRepositoryObject,
	 * de.clusteval.framework.repository.IRepositoryObject)
	 */
	@Override
	protected boolean hasChanged(IRepositoryObject old,
			IRepositoryObject object) {
		boolean result = super.hasChanged(old, object);
		if (old instanceof IStandaloneProgram
				&& object instanceof IStandaloneProgram)
			result |= !Objects.equals(((StandaloneProgram) old).getAlias(),
					(((StandaloneProgram) object).getAlias()));
		return result;
	}
}
