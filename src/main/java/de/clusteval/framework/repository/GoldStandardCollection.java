/**
 * 
 */
package de.clusteval.framework.repository;

import java.io.File;

import org.apache.maven.artifact.versioning.ComparableVersion;

import dk.sdu.imada.compbio.file.FileUtils;

/**
 * @author Christian Wiwie
 *
 */
public class GoldStandardCollection extends StaticRepositoryObjectCollection {

	/**
	 * 
	 */
	public GoldStandardCollection(final IRepository repository, final Class<? extends IRepositoryObject> entityClass,
			final StaticRepositoryObjectCollection parent, final String basePath) {
		super(repository, entityClass, parent, basePath);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.StaticRepositoryEntity#isVersioned()
	 */
	@Override
	public boolean isVersioned() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.StaticRepositoryEntity#
	 * canBeResolvedWithMaven()
	 */
	@Override
	protected boolean canBeResolvedWithMaven() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.StaticRepositoryObjectCollection#
	 * getPathOfExtractedObjectFromJarFromMaven(java.lang.String, java.io.File,
	 * org.apache.maven.artifact.versioning.ComparableVersion)
	 */
	@Override
	protected String getExtractedPathForMainEntryFromMavenJar(String objectName, File artifactFile,
			ComparableVersion version) {
		String targetDir = repository.getBasePath(this.getEntityClass());
		String targetFilePath = FileUtils.buildPath(targetDir, objectName);
		return targetFilePath;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.StaticRepositoryObjectCollection#
	 * getPathOfObjectInJarFromMaven(java.lang.String, java.io.File,
	 * org.apache.maven.artifact.versioning.ComparableVersion)
	 */
	@Override
	protected String getMainEntryNameForMavenJar(String objectName, File artifactFile, ComparableVersion version) {
		return "data/goldstandards/" + objectName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.StaticRepositoryObjectCollection#
	 * setCorrectVersionInformationToObjectFromJarFromMaven(java.lang.String,
	 * org.apache.maven.artifact.versioning.ComparableVersion)
	 */
	@Override
	protected String setCorrectVersionInformationToObjectFromJarFromMaven(String targetPath,
			ComparableVersion version) {
		// remove version information if it is present
		targetPath = targetPath.replaceAll("\\.v\\d+(-SNAPSHOT)?$", "");
		// append version of the artifact to the filename
		targetPath += String.format(".v%s", version.toString());
		return targetPath;
	}

	@Override
	protected String getMavenGroupIDForRepositoryObject(final String objectName) {
		return String.format("%s.%s", this.getEntityClass().getPackage().getName(),
				objectName.substring(0, objectName.lastIndexOf("/")));
	}

	@Override
	protected String getMavenArtifactIDForRepositoryObject(final String objectName) {
		return objectName.substring(objectName.lastIndexOf("/") + 1);
	}
}
