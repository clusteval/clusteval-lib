/**
 * 
 */
package de.clusteval.framework.repository;

import java.io.File;

/**
 * @author Christian Wiwie
 * @param <R>
 *
 */
public class SerializableWrapperDynamicComponent<R extends IRepositoryObjectDynamicComponent>
		extends
			SerializableWrapperRepositoryObject<R>
		implements
			ISerializableWrapperDynamicComponent<R> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8862678811932756705L;

	protected Class<? extends IRepositoryObjectDynamicComponent> type;

	protected String alias;

	/**
	 * @param type
	 * @param absPath
	 * @param name
	 * @param version
	 * @param alias
	 */
	public SerializableWrapperDynamicComponent(
			Class<? extends IRepositoryObjectDynamicComponent> type,
			File absPath, String name, String version, String alias) {
		super(absPath, name, version);
		this.type = type;
		this.alias = alias;
	}

	/**
	 * @param wrappedComponent
	 */
	public SerializableWrapperDynamicComponent(
			Class<? extends IRepositoryObjectDynamicComponent> type,
			R wrappedComponent) {
		super(wrappedComponent);
		this.type = type;
		this.alias = wrappedComponent.getAlias();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.SerializableWrapper#deserializeInternal
	 * (de.clusteval.framework.repository.IRepository)
	 */
	@Override
	protected R deserializeInternal(IRepository repository)
			throws DeserializationException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.ISerializableWrapperDynamicComponent#
	 * hasAlias()
	 */
	@Override
	public boolean hasAlias() {
		return this.alias != null && !this.alias.isEmpty();
	}

	/**
	 * @return the alias
	 */
	@Override
	public String getAlias() {
		return alias;
	}

	@Override
	public String toString(boolean readable) {
		// if we don't have an alias, we use the name instead
		readable &= this.hasAlias();
		if (!readable)
			return toString();

		if (this.wrappedComponent != null)
			return this.wrappedComponent.toString();
		if (this.version.isEmpty())
			return this.alias;
		return String.format("%s:%s", readable ? this.alias : this.name,
				this.version);
	}
}
