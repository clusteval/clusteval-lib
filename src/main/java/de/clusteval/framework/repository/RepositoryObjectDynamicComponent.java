/**
 * 
 */
package de.clusteval.framework.repository;

import java.io.File;

import de.clusteval.utils.ClustEvalAlias;

/**
 * @author Christian Wiwie
 *
 */
public abstract class RepositoryObjectDynamicComponent extends RepositoryObject
		implements
			IRepositoryObjectDynamicComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3734553376335848109L;

	/**
	 * @param repository
	 * @param changeDate
	 * @param absPath
	 */
	public RepositoryObjectDynamicComponent(IRepository repository,
			long changeDate, File absPath) {
		super(repository, changeDate, absPath);
	}

	/**
	 * @param other
	 */
	public RepositoryObjectDynamicComponent(RepositoryObject other) {
		super(other);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#asSerializable()
	 */
	@Override
	public ISerializableWrapperDynamicComponent<? extends IRepositoryObjectDynamicComponent> asSerializable()
			throws RepositoryObjectSerializationException {
		return (ISerializableWrapperDynamicComponent<? extends IRepositoryObjectDynamicComponent>) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.RepositoryObject#asSerializableInternal
	 * ()
	 */
	@Override
	protected abstract ISerializableWrapperDynamicComponent<? extends IRepositoryObjectDynamicComponent> asSerializableInternal()
			throws RepositoryObjectSerializationException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IRepositoryObjectDynamicComponent#
	 * getAlias()
	 */
	@Override
	public String getAlias() {
		return this.getClass().getAnnotation(ClustEvalAlias.class).alias();
	}
}
