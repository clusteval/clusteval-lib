/**
 * 
 */
package de.clusteval.framework.repository;

import java.io.File;

/**
 * @author Christian Wiwie
 * @param <R>
 *
 */
public abstract class SerializableWrapperRepositoryObject<R extends IRepositoryObject>
		extends
			SerializableWrapper<R>
		implements
			ISerializableWrapperRepositoryObject<R> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3243571296767860379L;

	/**
	 * @param absPath
	 * @param name
	 *            The name of the wrapped component.
	 * @param version
	 *            The version of the wrapped component.
	 */
	public SerializableWrapperRepositoryObject(final File absPath,
			final String name, final String version) {
		super();
		this.absPath = absPath;
		this.name = name;
		this.version = version;
	}

	/**
	 * @param wrappedComponent
	 *            This component wrapped by this instance.
	 */
	public SerializableWrapperRepositoryObject(final R wrappedComponent) {
		super();
		this.absPath = wrappedComponent.getFile();
		this.name = wrappedComponent.getName();
		this.version = wrappedComponent.getVersion().toString();
		if (wrappedComponent.isSerializable())
			this.wrappedComponent = wrappedComponent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.SerializableWrapper#setWrappedComponent
	 * (java.lang.Object)
	 */
	@Override
	public void setWrappedComponent(R wrappedComponent) {
		if (wrappedComponent.isSerializable())
			super.setWrappedComponent(wrappedComponent);
	}
}
