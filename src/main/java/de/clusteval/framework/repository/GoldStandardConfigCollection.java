/**
 * 
 */
package de.clusteval.framework.repository;

import java.io.File;

import org.apache.maven.artifact.versioning.ComparableVersion;

import dk.sdu.imada.compbio.file.FileUtils;

/**
 * @author Christian Wiwie
 *
 */
public class GoldStandardConfigCollection extends StaticRepositoryObjectCollection {

	/**
	 * 
	 */
	public GoldStandardConfigCollection(final IRepository repository,
			final Class<? extends IRepositoryObject> entityClass, final StaticRepositoryObjectCollection parent,
			final String basePath) {
		super(repository, entityClass, parent, basePath);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.StaticRepositoryEntity#isVersioned()
	 */
	@Override
	public boolean isVersioned() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.StaticRepositoryEntity#
	 * canBeResolvedWithMaven()
	 */
	@Override
	protected boolean canBeResolvedWithMaven() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.StaticRepositoryObjectCollection#
	 * getPathOfExtractedObjectFromJarFromMaven(java.lang.String, java.io.File,
	 * org.apache.maven.artifact.versioning.ComparableVersion)
	 */
	@Override
	protected String getExtractedPathForMainEntryFromMavenJar(String objectName, File artifactFile,
			ComparableVersion version) {
		String targetDir = repository.getBasePath(this.getEntityClass());
		String targetFilePath = FileUtils.buildPath(targetDir, objectName + ".gsconfig");
		return targetFilePath;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.StaticRepositoryObjectCollection#
	 * getPathOfObjectInJarFromMaven(java.lang.String, java.io.File,
	 * org.apache.maven.artifact.versioning.ComparableVersion)
	 */
	@Override
	protected String getMainEntryNameForMavenJar(String objectName, File artifactFile, ComparableVersion version) {
		return "data/goldstandards/configs/" + objectName + "(\\.v\\d+(-SNAPSHOT)?)?.gsconfig";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.StaticRepositoryObjectCollection#
	 * setCorrectVersionInformationToObjectFromJarFromMaven(java.lang.String,
	 * org.apache.maven.artifact.versioning.ComparableVersion)
	 */
	@Override
	protected String setCorrectVersionInformationToObjectFromJarFromMaven(String targetPath,
			ComparableVersion version) {
		// remove version information if it is present
		targetPath = targetPath.replaceAll("(\\.v\\d+(-SNAPSHOT)?)?.gsconfig$", "");
		// append version of the artifact to the filename
		targetPath += String.format(".v%s.gsconfig", version.toString());
		return targetPath;
	}

	@Override
	protected String getMavenGroupIDForRepositoryObject(final String objectName) {
		return this.getEntityClass().getPackage().getName() + ".config";
	}

	@Override
	protected String getMavenArtifactIDForRepositoryObject(final String objectName) {
		return objectName;
	}
}
