/**
 * 
 */
package de.clusteval.framework.repository;

import java.util.HashMap;
import java.util.Map;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.run.statistics.IRunStatistic;
import de.clusteval.run.statistics.IRunStatisticCalculator;
import de.clusteval.run.statistics.RunStatistic;
import de.clusteval.utils.DynamicComponentMissingVersionException;

/**
 * @author Christian Wiwie
 * 
 */
public class RunStatisticRepositoryEntity extends DynamicRepositoryObjectCollection {

	/**
	 * A map containing all classes of run data statistic calculators registered
	 * in this repository.
	 */
	protected Map<String, Map<ComparableVersion, Class<? extends IRunStatisticCalculator<? extends IRunStatistic>>>> runStatisticCalculatorClasses;

	/**
	 * @param repository
	 * @param parent
	 * @param basePath
	 */
	public RunStatisticRepositoryEntity(IRepository repository, RunStatisticRepositoryEntity parent, String basePath) {
		super(repository, RunStatistic.class, parent, basePath);

		this.runStatisticCalculatorClasses = new HashMap<String, Map<ComparableVersion, Class<? extends IRunStatisticCalculator<? extends IRunStatistic>>>>();
	}

	/**
	 * This method looks up and returns (if it exists) the class of the run-data
	 * statistic calculator corresponding to the run-data-statistic with the
	 * given name.
	 * 
	 * @param runStatisticClassName
	 *            The name of the class of the run-data statistic.
	 * @return The class of the run-data statistic calculator for the given
	 *         name, or null if it does not exist.
	 */
	public Class<? extends IRunStatisticCalculator<? extends IRunStatistic>> getRunStatisticCalculator(
			final String runStatisticClassName, final ComparableVersion version) {
		Class<? extends IRunStatisticCalculator<? extends IRunStatistic>> result = this.runStatisticCalculatorClasses
				.containsKey(runStatisticClassName)
						? this.runStatisticCalculatorClasses.get(runStatisticClassName).get(version)
						: null;
		if (result == null && parent != null)
			result = ((RunStatisticRepositoryEntity) this.parent).getRunStatisticCalculator(runStatisticClassName,
					version);
		return result;
	}

	/**
	 * This method registers a new run-data statistic calculator class.
	 * 
	 * @param object
	 *            The new class to register.
	 * @return True, if the new class replaced an old one.
	 * @throws DynamicComponentMissingVersionException
	 */
	public boolean registerRunStatisticCalculator(
			final Class<? extends IRunStatisticCalculator<? extends IRunStatistic>> object)
			throws DynamicComponentMissingVersionException {
		String className = object.getName().replace("Calculator", "");
		if (!this.runStatisticCalculatorClasses.containsKey(className))
			this.runStatisticCalculatorClasses.put(className,
					new HashMap<ComparableVersion, Class<? extends IRunStatisticCalculator<? extends IRunStatistic>>>());
		return this.runStatisticCalculatorClasses.get(className).put(Repository.getVersionOfDynamicClass(object),
				object) != null;
	}
}
