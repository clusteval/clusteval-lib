/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.framework.repository;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPDouble;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.clusteval.framework.AbstractClustevalServer;
import de.clusteval.framework.RLibraryNotLoadedException;

/**
 * This class is used throughout the framework to provide access to the R
 * framework.
 * 
 * <p>
 * This class is a wrapper class for {@link RConnection} which adds convenience
 * functions.
 * 
 * @author Christian Wiwie
 * 
 */
public class MyRengine implements IMyRengine {

	public static boolean IS_R_AVAILABLE;
	public static String R_SERVE_HOST = "127.0.0.1";
	public static int R_SERVE_PORT = 6311;

	protected RConnection connection;

	protected int pid;

	protected boolean interrupted;

	protected Logger log;

	protected Set<String> loadedLibraries;

	/**
	 * @param string
	 *            The parameter string.
	 * @throws RserveException
	 */
	public MyRengine(String string) throws RserveException {
		super();

		this.connection = new RConnection(R_SERVE_HOST, R_SERVE_PORT);
		try {
			this.pid = this.connection.eval("Sys.getpid()").asInteger();
		} catch (REXPMismatchException e) {
			e.printStackTrace();
			// should not happen
		}
		// set buffer size to 100MB
		// this.connection.setSendBufferSize(1024l * 1024 * 1024 * 100);
		this.log = LoggerFactory.getLogger(this.getClass());
		this.loadedLibraries = new HashSet<String>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IMyRengine#loadLibrary(java.lang.
	 * String, java.lang.String)
	 */
	@Override
	public synchronized boolean loadLibrary(final String name, final String requiredByClass)
			throws RLibraryNotLoadedException, InterruptedException {
		if (this.interrupted)
			throw new InterruptedException();
		try {
			if (this.loadedLibraries.contains(name))
				return true;
			this.log.debug("Loading R library '" + name + "' ...");
			this.voidEval("library(" + name + ")");
			this.loadedLibraries.add(name);
			this.log.debug("R library '" + name + "' loaded successfully");
			return true;
		} catch (RserveException e) {
			this.log.debug("R library '" + name + "' loading failed");
			throw new RLibraryNotLoadedException(requiredByClass, name);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IMyRengine#clear()
	 */
	@Override
	public synchronized void clear() throws RserveException, InterruptedException {
		if (interrupted)
			throw new InterruptedException();
		this.voidEval("rm(list=ls(all=TRUE))");
		this.voidEval("gc()");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IMyRengine#assign(java.lang.String,
	 * double[][])
	 */
	@Override
	public synchronized void assign(String arg0, double[][] arg1)
			throws REngineException, InterruptedException {
		if (interrupted)
			throw new InterruptedException();
		int inputRows = arg1.length;
		int inputColumns = inputRows > 0 ? arg1[0].length : 0;

		long maxNumberElementsPer1DArray = 100000000;

		int numberRowsIn1DArray = Math
				.max((int) (maxNumberElementsPer1DArray / inputColumns), 1);

		int currentRowOfInput = 0;
		this.voidEval(arg0 + " <- c()");
		while (currentRowOfInput < inputRows) {
			int currNumberRows = Math.min(numberRowsIn1DArray,
					inputRows - currentRowOfInput);
			double[] oneDim = new double[currNumberRows * inputColumns];
			for (int i = currentRowOfInput; i < Math.min(
					currentRowOfInput + numberRowsIn1DArray, inputRows); i++) {
				for (int j = 0; j < inputColumns; j++) {
					oneDim[j + (i - currentRowOfInput)
							* inputColumns] = arg1[i][j];
				}
			}
			this.connection.assign("tmp", oneDim);
			this.voidEval(arg0 + " <- c(" + arg0 + ", tmp); 0;");
			currentRowOfInput = currentRowOfInput + numberRowsIn1DArray;
		}

		this.voidEval(arg0 + " <- matrix(" + arg0 + ",nrow=" + inputRows
				+ ",ncol=" + inputColumns + ",byrow=T); 0;");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IMyRengine#assign(java.lang.String,
	 * int[][])
	 */
	@Override
	public synchronized void assign(String arg0, int[][] arg1)
			throws REngineException, InterruptedException {
		if (interrupted)
			throw new InterruptedException();
		int x = arg1.length;
		int y = x > 0 ? arg1[0].length : 0;
		int[] oneDim = new int[x * y];
		for (int i = 0; i < x; i++) {
			for (int j = 0; j < y; j++) {
				oneDim[j + i * y] = arg1[i][j];
			}
		}
		this.voidEval(arg0 + " <- c()");
		this.connection.assign(arg0, oneDim);
		this.voidEval(arg0 + " <- matrix(" + arg0 + ",nrow=" + x + ",ncol=" + y
				+ ",byrow=T)");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.rosuda.REngine.Rserve.RConnection#eval(java.lang.String)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IMyRengine#eval(java.lang.String)
	 */
	@Override
	public synchronized REXP eval(String cmd) throws RserveException, InterruptedException {
		if (interrupted)
			throw new InterruptedException();
		try {
			this.connection.assign(".tmp.", cmd);
			REXP r = this.connection
					.eval("try(eval(parse(text=.tmp.)),silent=TRUE)");
			if (r == null)
				throw new RserveException(this.connection, "Evaluation error");
			else if (r.inherits("try-error"))
				try {
					throw new RserveException(this.connection,
							r.asString().replace("\n", " - "));
				} catch (REXPMismatchException e) {
					throw new RserveException(this.connection,
							"Evaluation error");
				}
			return r;
		} catch (REngineException e) {
			throw new RserveException(this.connection, e.getMessage());
		} catch (NullPointerException e) {
			System.out.format("%s - %s%n", Thread.currentThread(), this);
			throw e;
		}
	}

	@Override
	public synchronized void voidEval(String cmd)
			throws RserveException, InterruptedException {
		if (interrupted)
			throw new InterruptedException();
		try {
			this.connection.assign(".tmp.", cmd);
			this.connection
					.voidEval("try(eval(parse(text=.tmp.)),silent=TRUE)");
		} catch (REngineException e) {
			throw new RserveException(this.connection, e.getMessage());
		} catch (NullPointerException e) {
			System.out.format("%s - %s%n", Thread.currentThread(), this);
			throw e;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IMyRengine#printLastError()
	 */
	@Override
	public synchronized void printLastError() throws InterruptedException {
		if (this.interrupted)
			throw new InterruptedException();
		log.error("R error: " + this.connection.getLastError());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IMyRengine#assign(java.lang.String,
	 * int[])
	 */
	@Override
	public synchronized void assign(String arg0, int[] arg1)
			throws REngineException, InterruptedException {
		if (interrupted)
			throw new InterruptedException();

		this.connection.assign(arg0, arg1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IMyRengine#assign(java.lang.String,
	 * double)
	 */
	@Override
	public synchronized void assign(String arg0, double arg1)
			throws REngineException, InterruptedException {
		if (interrupted)
			throw new InterruptedException();
		this.connection.assign(arg0, new REXPDouble(arg1));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IMyRengine#assign(java.lang.String,
	 * double[])
	 */
	@Override
	public synchronized void assign(String arg0, double[] arg1)
			throws REngineException, InterruptedException {
		if (interrupted)
			throw new InterruptedException();
		this.connection.assign(arg0, arg1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IMyRengine#getLastError()
	 */
	@Override
	public synchronized String getLastError() throws InterruptedException {
		if (this.interrupted)
			throw new InterruptedException();
		return this.connection.getLastError();
	}

	/*
	 * TODO: Put javadoc of {@link RConnection#close()}
	 */
	protected synchronized boolean close() {
		return this.connection.close();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IMyRengine#interrupt()
	 */
	@Override
	public boolean interrupt() {
		try {
			interrupted = true;
			this.connection.close();
			Runtime.getRuntime().exec(("kill -9 " + this.pid).split(" "));
		} catch (IOException e) {
			return false;
		}
		return true;
	}

	protected synchronized boolean shutdown() {
		try {
			this.connection.shutdown();
			return true;
		} catch (RserveException e) {
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.IMyRengine#assign(java.lang.String,
	 * java.lang.String[])
	 */
	@Override
	public synchronized void assign(String arg0, String[] arg1)
			throws REngineException, InterruptedException {
		if (interrupted)
			throw new InterruptedException();
		this.connection.assign(arg0, arg1);
	}

	/**
	 * @return the connection
	 */
	@Override
	public RConnection getConnection() {
		return connection;
	}

	public static void initializeRConnection() {
		Logger log = LoggerFactory.getLogger(MyRengine.class);

		if (AbstractClustevalServer.getBackendServerConfiguration().isNoR()) {
			log.info("Not attempting to connect to R.");
			log.info("Functionality that requires R will not be available.");
			MyRengine.IS_R_AVAILABLE = false;
		} else {
			try {
				// try to establish a connection to R
				log.info("Attempting connection to Rserve on "
						+ MyRengine.R_SERVE_HOST + ":"
						+ MyRengine.R_SERVE_PORT);
				@SuppressWarnings("unused")
				IMyRengine myRengine = new MyRengine("");
				log.info("Success");
				MyRengine.IS_R_AVAILABLE = true;
			} catch (RserveException e) {
				log.error("Connection to Rserve could not be established, "
						+ "please ensure that your Rserve instance is "
						+ "running before starting this framework.");
				log.error("Functionality that requires R will not be available"
						+ " until Rserve has been started.");
				MyRengine.IS_R_AVAILABLE = false;
			}
		}
	}
}
