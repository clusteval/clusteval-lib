/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.framework;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.BindException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.rmi.AccessException;
import java.rmi.AlreadyBoundException;
import java.rmi.NoSuchObjectException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.ExportException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.maven.artifact.versioning.ComparableVersion;
import org.eclipse.aether.resolution.ArtifactResolutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import de.clusteval.auth.BasicClientPermissions;
import de.clusteval.auth.IClientPermissions;
import de.clusteval.auth.OperationNotPermittedException;
import de.clusteval.auth.UnknownClientException;
import de.clusteval.cluster.IClustering;
import de.clusteval.cluster.ISerializableClustering;
import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.cluster.quality.UnknownClusteringQualityMeasureException;
import de.clusteval.context.UnknownContextException;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.ISerializableDataConfig;
import de.clusteval.data.SerializableDataConfig;
import de.clusteval.data.dataset.AbsoluteDataSet;
import de.clusteval.data.dataset.DataMatrix;
import de.clusteval.data.dataset.IDataSet;
import de.clusteval.data.dataset.IDataSetConfig;
import de.clusteval.data.dataset.ISerializableDataSet;
import de.clusteval.data.dataset.ISerializableDataSetConfig;
import de.clusteval.data.dataset.format.DataSetParseException;
import de.clusteval.data.dataset.format.IRelativeDataSetFormat;
import de.clusteval.data.dataset.format.ISerializableDataSetFormat;
import de.clusteval.data.dataset.format.InvalidDataSetFormatVersionException;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.dataset.generator.DataSetGenerationException;
import de.clusteval.data.dataset.generator.DataSetGenerator;
import de.clusteval.data.dataset.generator.GoldStandardGenerationException;
import de.clusteval.data.dataset.generator.IDataSetGenerator;
import de.clusteval.data.dataset.generator.UnknownDataSetGeneratorException;
import de.clusteval.data.dataset.type.UnknownDataSetTypeException;
import de.clusteval.data.distance.UnknownDistanceMeasureException;
import de.clusteval.data.goldstandard.IGoldStandard;
import de.clusteval.data.goldstandard.IGoldStandardConfig;
import de.clusteval.data.goldstandard.ISerializableGoldStandard;
import de.clusteval.data.goldstandard.ISerializableGoldStandardConfig;
import de.clusteval.data.randomizer.DataRandomizeException;
import de.clusteval.data.randomizer.DataRandomizer;
import de.clusteval.data.randomizer.IDataRandomizer;
import de.clusteval.data.randomizer.UnknownDataRandomizerException;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.IRepositoryObjectDynamicComponent;
import de.clusteval.framework.repository.ISerializableWrapperDynamicComponent;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;
import de.clusteval.framework.repository.InvalidRepositoryException;
import de.clusteval.framework.repository.ObjectNotFoundException;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotFoundException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.Repository;
import de.clusteval.framework.repository.RepositoryAlreadyExistsException;
import de.clusteval.framework.repository.RepositoryCouldNotBeMigratedException;
import de.clusteval.framework.repository.RepositoryObject;
import de.clusteval.framework.repository.RepositoryObjectDumpException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.RepositoryVersionTooNewException;
import de.clusteval.framework.repository.RepositoryVersionTooOldException;
import de.clusteval.framework.repository.SerializableWrapperDynamicComponent;
import de.clusteval.framework.repository.UnknownDynamicComponentException;
import de.clusteval.framework.repository.config.RepositoryConfigNotFoundException;
import de.clusteval.framework.repository.config.RepositoryConfigurationException;
import de.clusteval.framework.repository.db.DatabaseException;
import de.clusteval.framework.repository.db.ISQLConfig.DB_TYPE;
import de.clusteval.framework.repository.maven.InvalidDynamicComponentNameException;
import de.clusteval.framework.repository.parse.RepositoryObjectParseException;
import de.clusteval.framework.threading.IRunSchedulerThread;
import de.clusteval.framework.threading.RunSchedulerThread;
import de.clusteval.framework.threading.SupervisorThread;
import de.clusteval.program.IProgram;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.program.ISerializableProgram;
import de.clusteval.program.ISerializableProgramConfig;
import de.clusteval.program.ISerializableStandaloneProgram;
import de.clusteval.program.IStandaloneProgram;
import de.clusteval.program.r.IRProgram;
import de.clusteval.program.r.RProgram;
import de.clusteval.program.r.UnknownRProgramException;
import de.clusteval.run.IRun;
import de.clusteval.run.ISerializableRun;
import de.clusteval.run.IToolRuntimeEvaluationRun;
import de.clusteval.run.RUN_STATUS;
import de.clusteval.run.RUN_TYPE;
import de.clusteval.run.ToolRuntimeEvaluationRun;
import de.clusteval.run.UnknownRunTypeException;
import de.clusteval.run.runnable.AnalysisIterationRunnable;
import de.clusteval.run.runnable.ClusteringIterationRunnable;
import de.clusteval.run.runnable.DataAnalysisIterationRunnable;
import de.clusteval.run.runnable.DataAnalysisRunRunnable;
import de.clusteval.run.runnable.ExecutionRunRunnable;
import de.clusteval.run.runnable.IClusteringIterationRunnable;
import de.clusteval.run.runnable.IDataAnalysisIterationRunnable;
import de.clusteval.run.runnable.IIterationRunnable;
import de.clusteval.run.runnable.IIterationWrapper;
import de.clusteval.run.runnable.IRunAnalysisIterationRunnable;
import de.clusteval.run.runnable.IterationRunnableStatus;
import de.clusteval.run.runnable.RunAnalysisIterationRunnable;
import de.clusteval.run.runnable.RunAnalysisRunRunnable;
import de.clusteval.run.runresult.IClusteringRunResult;
import de.clusteval.run.runresult.IExecutionRunResult;
import de.clusteval.run.runresult.IParameterOptimizationResult;
import de.clusteval.run.runresult.IRunResult;
import de.clusteval.run.runresult.ISerializableRunResult;
import de.clusteval.run.runresult.ParameterOptimizationResult;
import de.clusteval.run.runresult.UnknownRunResultException;
import de.clusteval.run.runresult.format.UnknownRunResultFormatException;
import de.clusteval.serverclient.IBackendServer;
import de.clusteval.utils.ClassVersion;
import de.clusteval.utils.ClustEvalAlias;
import de.clusteval.utils.DeletionException;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.ErrorSerializableWrapper;
import de.clusteval.utils.IFinder;
import de.clusteval.utils.IInMemoryListener;
import de.clusteval.utils.MatrixParser;
import dk.sdu.imada.compbio.file.FileUtils;
import dk.sdu.imada.compbio.utils.Pair;
import dk.sdu.imada.compbio.utils.StringExt;
import dk.sdu.imada.compbio.utils.Triple;

/**
 * This class represents the server of the backend of the framework. The server
 * takes commands from the client like performing, resuming or terminating runs,
 * shutdown the framework or get status information about various objects
 * available in the repository (e.g. datasets, runs, programs,...).
 * 
 * <p>
 * You can start the server by invoking the {@link #main(String[])} method. If
 * you do so, you can pass either a path to an existing repository or a new
 * repository is automatically created in the subfolder 'repository'.
 * 
 * <p>
 * When the server is started it registers itself in the RMI registry (remote
 * method invocation), either with the default port 1099 or if specified with
 * -hostport xxxx under any other port.
 * 
 * <p>
 * The start of the server requires a running Rserve instance. If this cannot be
 * found, the server will not start.
 * 
 * @author Christian Wiwie
 */
public abstract class AbstractClustevalServer
		implements
			IBackendServer,
			IInMemoryListener {

	protected static BackendServerConfig config = new BackendServerConfig();

	/**
	 * @return The configuration of this backend server.
	 */
	public static BackendServerConfig getBackendServerConfiguration() {
		return config;
	}

	/**
	 * This variable holds the port this server will be listening on. It can be
	 * specified by passing -hostport xxx to the {@link #main(String[])} method.
	 */
	protected static int port;

	protected static IBackendServer singletonBackendServer;

	/**
	 * @return the singletonBackendServer
	 */
	public static IBackendServer getSingletonBackendServer() {
		return singletonBackendServer;
	}

	private Logger log;

	/**
	 * Every backend server has exactly one repository, which stores all the
	 * data on the filesystem.
	 */
	protected IRepository repository;

	/**
	 * The number of clients connected to this server so far. This number is
	 * used to give new clients a new number for authentication in case, several
	 * users connect to the server.
	 */
	protected int clientCount;

	protected Map<String, IClientPermissions> userPermissions;

	/**
	 * Instantiates a new backend server.
	 * 
	 * @param absRepositoryPath
	 *            The absolute path to the repository used by this server.
	 * @throws FileNotFoundException
	 * @throws InvalidRepositoryException
	 * @throws RepositoryAlreadyExistsException
	 * @throws RepositoryConfigurationException
	 * @throws RepositoryConfigNotFoundException
	 * @throws InterruptedException
	 * @throws DatabaseException
	 * @throws RepositoryVersionTooNewException
	 * @throws RepositoryVersionTooOldException
	 * @throws RepositoryCouldNotBeMigratedException
	 */
	public AbstractClustevalServer(final String absRepositoryPath)
			throws FileNotFoundException, RepositoryAlreadyExistsException,
			InvalidRepositoryException, RepositoryConfigNotFoundException,
			RepositoryConfigurationException, InterruptedException,
			DatabaseException, RepositoryVersionTooOldException,
			RepositoryVersionTooNewException,
			RepositoryCouldNotBeMigratedException {
		this(new Repository(absRepositoryPath, null));
	}

	/**
	 * Instantiates a new backend server and registers the server at the RMI
	 * registry.
	 * 
	 * @param repository
	 *            The repository used by this server.
	 * @throws InterruptedException
	 * @throws DatabaseException
	 */
	public AbstractClustevalServer(final IRepository repository)
			throws InterruptedException, DatabaseException {
		this(repository, true);
	}

	/**
	 * @param repository
	 * @param registerServer
	 * @throws InterruptedException
	 * @throws DatabaseException
	 */
	public AbstractClustevalServer(final IRepository repository,
			final boolean registerServer)
			throws InterruptedException, DatabaseException {
		super();
		singletonBackendServer = this;

		this.log = LoggerFactory.getLogger(this.getClass());

		this.repository = repository;

		this.userPermissions = new HashMap<String, IClientPermissions>();
		for (String uuid : config.administratorUUIDs) {
			BasicClientPermissions perms = new BasicClientPermissions();
			perms.setCanShutdown(true);
			perms.setCanUpload(true);
			perms.setCanDelete(true);
			this.userPermissions.put(uuid, perms);
		}

		this.log.info(
				"Using repository at '" + this.repository.getBasePath() + "'");

		if (!registerServer || AbstractClustevalServer.registerServer(this)) {
			/*
			 * Check, whether the repository is being initialized
			 */
			if (this.repository.getSupervisorThread() == null)
				this.repository.initialize();
		}
	}

	/**
	 * Gets the repository.
	 * 
	 * @return The repository used by this server.
	 * @see #repository
	 */
	public IRepository getRepository() {
		synchronized (this.repository) {
			return this.repository;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see serverclient.EvalServer#performRun(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public String performRun(final String clientId, final String runId)
			throws OperationNotPermittedException, RemoteException,
			UnknownClientException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException {
		if (!getClientPermissions(clientId).canStartRun())
			throw new OperationNotPermittedException();
		return this.repository.getSupervisorThread().getRunScheduler()
				.schedule(clientId, runId);
	}

	@Override
	public boolean resumeRun(final String clientId,
			final String uniqueRunIdentifier)
			throws OperationNotPermittedException, RemoteException,
			UnknownClientException {
		if (!getClientPermissions(clientId).canStartRun())
			throw new OperationNotPermittedException();
		boolean result = this.repository.getSupervisorThread().getRunScheduler()
				.scheduleResume(clientId, uniqueRunIdentifier);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see serverclient.EvalServer#performRun(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public boolean terminateRun(final String clientId, final String runId)
			throws OperationNotPermittedException, RemoteException,
			UnknownClientException {
		if (!getClientPermissions(clientId).canStopRun())
			throw new OperationNotPermittedException();
		boolean result = this.repository.getSupervisorThread().getRunScheduler()
				.terminate(clientId, runId);
		return result;
	}

	/**
	 * A helper method for {@link #ClusteringEvalFramework(Repository)}, which
	 * registers the new backend server instance in the RMI registry.
	 * 
	 * @param framework
	 *            The backend server to register.
	 * @return True, if the server has been registered successfully
	 */
	protected static boolean registerServer(IBackendServer framework) {
		Logger log = LoggerFactory.getLogger(AbstractClustevalServer.class);

		try {
			LocateRegistry.createRegistry(port);

			IBackendServer stub = (IBackendServer) UnicastRemoteObject
					.exportObject(framework, port);
			Registry registry = LocateRegistry.getRegistry(port);
			registry.bind("EvalServer", stub);
			log.info("Framework up and listening on port " + port);
			log.info("Used number of processors: "
					+ RunSchedulerThread.NUMBER_THREADS);
			return true;
		} catch (ExportException e) {
			if (e.getCause() != null && e.getCause() instanceof BindException) {
				log.error(
						"Could not bind the specified port. Probably another ClustEval instance is already running.");
			}
			return false;
		} catch (AlreadyBoundException e) {
			log.error("Another instance is already running...");
			return false;
		} catch (RemoteException e) {
			log.error(
					"An error occurred when trying to register the backend server in RMI registry:");
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * A helper method for {@link #shutdown(String, long)}, which terminates
	 * this framework after a certain timeout.
	 * 
	 * <p>
	 * This method first interrupts the supervisor thread (see
	 * {@link SupervisorThread}) and waits for its termination until the timeout
	 * was reached.
	 * 
	 * <p>
	 * Then the backend server instance is unregistered from the RMI registry
	 * and the repository of this framework is removed from the set of all
	 * registered repositories.
	 * 
	 * @throws InterruptedException
	 * 
	 */
	private void terminate(final long forceTimeout)
			throws InterruptedException {
		this.repository.terminateSupervisorThread();

		try {
			Registry registry = LocateRegistry.getRegistry(port);
			registry.unbind("EvalServer");
			UnicastRemoteObject.unexportObject(this, true);
		} catch (NoSuchObjectException e) {
			e.printStackTrace();
		} catch (AccessException e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		}
		/*
		 * unregister repository
		 */
		Repository.unregister(this.repository);
		// should work without, just to be sure
		// System.exit(0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see serverclient.IBackendServer#getClientId()
	 */
	@SuppressWarnings("unused")
	@Override
	public String getClientId() throws RemoteException {
		String uuid = UUID.randomUUID().toString();
		IClientPermissions perms = new BasicClientPermissions();

		userPermissions.put(uuid, perms);

		return uuid;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see serverclient.EvalServer#getRunStatusForClientId(java.lang.String)
	 */
	@SuppressWarnings("unused")
	@Override
	public Map<String, Pair<RUN_STATUS, Float>> getRunStatusForClientId(
			String clientId) throws RemoteException {
		return this.repository.getSupervisorThread().getRunScheduler()
				.getRunStatusForClientId(clientId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IBackendServer#
	 * getEstimatedRemainingRuntimeForClientId(java.lang.String)
	 */
	@Override
	public Map<String, Long> getEstimatedRemainingRuntimeForClientId(
			String clientId) throws RemoteException {
		return this.repository.getSupervisorThread().getRunScheduler()
				.getEstimatedRemainingRuntimeForClientId(clientId);
	}

	// TODO
	@Override
	public Map<String, Pair<Pair<RUN_STATUS, Float>, Map<Pair<String, String>, Pair<Double, Map<String, Pair<Map<String, String>, String>>>>>> getOptimizationRunStatusForClientId(
			String clientId) throws RemoteException {
		return this.repository.getSupervisorThread().getRunScheduler()
				.getOptimizationRunStatusForClientId(clientId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see serverclient.EvalServer#getRuns()
	 */
	@Override
	public Collection<ISerializableRun> getRuns(final String clientId)
			throws RepositoryObjectSerializationException {
		Collection<ISerializableRun> result = new HashSet<ISerializableRun>();

		Collection<IRun> res = (Collection) this.repository
				.getCollectionStaticEntities(IRun.class);

		for (IRun run : res) {
			if (!(run instanceof IToolRuntimeEvaluationRun<?>))
				result.add((ISerializableRun) run.asSerializable());
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see serverclient.EvalServer#getDataSets()
	 */
	@Override
	public Collection<ISerializableDataSet> getDataSets(final String clientId)
			throws RepositoryObjectSerializationException {
		Collection<ISerializableDataSet> result = new HashSet<ISerializableDataSet>();

		Collection<IDataSet> res = (Collection) this.repository
				.getCollectionStaticEntities(IDataSet.class);

		for (IDataSet ds : res) {
			if (!(ds.getMajorName().equals(
					ToolRuntimeEvaluationRun.RUNTIME_EVALUATION_DATASET_MAJOR_NAME)))
				result.add((ISerializableDataSet) ds.asSerializable());
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see serverclient.EvalServer#getPrograms()
	 */
	@Override
	public Collection<ISerializableProgram> getPrograms(final String clientId)
			throws UnknownRProgramException,
			DynamicComponentInitializationException,
			RepositoryObjectSerializationException,
			UnknownDataSetFormatException, UnknownRunResultFormatException {
		Collection<ISerializableProgram> result = new HashSet<ISerializableProgram>();

		Collection<IStandaloneProgram> res = (Collection) this.repository
				.getCollectionStaticEntities(IStandaloneProgram.class);

		for (IProgram program : res)
			result.add(program.asSerializable());

		Collection<Class<? extends IRepositoryObjectDynamicComponent>> classes = this.repository
				.getClasses(IRProgram.class);

		for (Class<? extends IRepositoryObjectDynamicComponent> program : classes)
			result.add(RProgram
					.parseFromString(repository, program.getSimpleName(), false)
					.asSerializable());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see serverclient.EvalServer#getPrograms()
	 */
	@Override
	public Collection<String> getProgramAliases(final String clientId) {
		Collection<String> result = new HashSet<String>();
		// TODO
		// for (IRepositoryObject program :
		// this.repository.getCollectionStaticEntities(IProgram.class))
		// result.add(((IProgram) program).getAlias());
		return result;
	}

	@Override
	public void shutdown(final String clientId, final long timeOut)
			throws OperationNotPermittedException, RemoteException,
			UnknownClientException {
		if (!getClientPermissions(clientId).canShutdown())
			throw new OperationNotPermittedException();
		log.info("Shutting down framework...");
		try {
			this.terminate(timeOut);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @return True, if this framework is still running and the corresponding
	 *         supervisor thread hasn't been interrupted.
	 */
	public boolean isRunning() {
		return this.repository.getSupervisorThread().isAlive();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see serverclient.EvalServer#getRunResumes()
	 */
	@SuppressWarnings("unused")
	@Override
	public Collection<String> getRunResumes(final String clientId)
			throws RemoteException {
		Collection<String> result = new HashSet<String>(
				this.repository.getRunResumes());
		return result;
	}

	@SuppressWarnings("unused")
	@Override
	public Collection<String> getClusteringRunResultIdentifiers(
			final String clientId) throws RemoteException {
		Collection<String> result = new HashSet<String>(
				this.repository.getClusteringRunResultIdentifiers());
		return result;
	}

	@SuppressWarnings("unused")
	@Override
	public Collection<String> getParameterOptimizationRunResultIdentifiers(
			final String clientId) throws RemoteException {
		Collection<String> result = new HashSet<String>(
				this.repository.getParameterOptimizationRunResultIdentifiers());
		return result;
	}

	@SuppressWarnings("unused")
	@Override
	public Collection<String> getDataAnalysisRunResultIdentifiers(
			final String clientId) throws RemoteException {
		Collection<String> result = new HashSet<String>(
				this.repository.getDataAnalysisRunResultIdentifiers());
		return result;
	}

	@SuppressWarnings("unused")
	@Override
	public Collection<String> getRunAnalysisRunResultIdentifiers(
			final String clientId) throws RemoteException {
		Collection<String> result = new HashSet<String>(
				this.repository.getRunAnalysisRunResultIdentifiers());
		return result;
	}

	@SuppressWarnings("unused")
	@Override
	public Collection<String> getRunDataAnalysisRunResultIdentifiers(
			final String clientId) throws RemoteException {
		Collection<String> result = new HashSet<String>(
				this.repository.getRunDataAnalysisRunResultIdentifiers());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see serverclient.EvalServer#getRunResults(java.lang.String,
	 * java.lang.String)
	 */
	@SuppressWarnings("unused")
	@Override
	public Map<Pair<String, String>, Map<String, Double>> getRunResults(
			final String clientId, String uniqueRunIdentifier)
			throws RemoteException {
		Map<Pair<String, String>, Map<String, Double>> result = new HashMap<Pair<String, String>, Map<String, Double>>();

		List<IRunResult> list = new ArrayList<IRunResult>();
		try {
			ParameterOptimizationResult.parseFromRunResultFolder(repository,
					new File(FileUtils.buildPath(
							repository.getBasePath(IRunResult.class),
							uniqueRunIdentifier)),
					list, false, false, false);
			for (IRunResult res : list) {
				IParameterOptimizationResult r = (IParameterOptimizationResult) res;
				String dataConfig = r.getMethod().getDataConfig().getName();
				String programConfig = r.getMethod().getProgramConfig()
						.getName();
				Map<String, Double> measureToOptimalQuality = new HashMap<String, Double>();
				for (IClusteringQualityMeasure measure : r
						.getOptimalIterations().keySet()) {
					measureToOptimalQuality.put(
							measure.getClass().getSimpleName(),
							r.get(r.getOptimalIterations().get(measure))
									.get(measure).getValue());
				}
				result.put(Pair.getPair(dataConfig, programConfig),
						measureToOptimalQuality);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (RepositoryObjectParseException e) {
			e.printStackTrace();
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IBackendServer#getRunResult(java.lang.String)
	 */
	@Override
	public Collection<ISerializableRunResult<? extends IRunResult>> getRunResult(
			final String clientId, String uniqueRunIdentifier)
			throws RemoteException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException,
			RepositoryObjectSerializationException {
		Set<ISerializableRunResult<? extends IRunResult>> results = new HashSet<>();
		for (IRepositoryObject r : this.repository
				.getCollectionStaticEntities(IRunResult.class)) {
			if (((IRunResult) r).getIdentifier().equals(uniqueRunIdentifier))
				results.add(((IRunResult) r).asSerializable());
		}
		return results;
	}

	@Override
	public ISerializableClustering getClusteringOfClusteringRun(
			final String clientId, final String resultId,
			final String programConfig, final String dataConfig)
			throws UnknownRunResultException,
			RepositoryObjectSerializationException, RemoteException {
		for (IRepositoryObject r : this.repository
				.getCollectionStaticEntities(IRunResult.class)) {
			if (!(r instanceof IClusteringRunResult))
				continue;
			IClusteringRunResult res = (IClusteringRunResult) r;

			if (res.getIdentifier().equals(resultId)
					&& res.getProgramConfig().toString()
							.equals(programConfig.toString())
					&& res.getDataConfig().toString()
							.equals(dataConfig.toString())) {
				boolean isLoaded = res.isInMemory();
				try {
					res.loadIntoMemory();

					IClustering clustering = res.getClustering();
					ISerializableClustering serClustering = clustering
							.asSerializable();
					if (!isLoaded)
						res.unloadFromMemory();
					return serClustering;
				} catch (Exception e) {
				}
			}

		}
		throw new UnknownRunResultException(
				"The specified run result of type parameter optimization could not be found.");
	}

	@Override
	public ISerializableClustering getClusteringOfParameterOptimizationIteration(
			final String clientId, final String resultId,
			final String programConfig, final String dataConfig,
			final long iteration) throws UnknownRunResultException,
			RepositoryObjectSerializationException, RemoteException {
		for (IRepositoryObject r : this.repository
				.getCollectionStaticEntities(IRunResult.class)) {
			if (!(r instanceof IParameterOptimizationResult))
				continue;
			IParameterOptimizationResult res = (IParameterOptimizationResult) r;

			if (res.getIdentifier().equals(resultId)
					&& res.getProgramConfig().toString()
							.equals(programConfig.toString())
					&& res.getDataConfig().toString()
							.equals(dataConfig.toString())) {
				boolean isLoaded = res.isInMemory(),
						parseClusterings = res.isParseClusterings(),
						storeClusterings = res.isStoreClusterings();
				try {
					res.setParseClusterings(true);
					res.setStoreClusterings(true);
					res.loadIntoMemory(false);

					IClustering clustering = res.getIterationToClustering()
							.get(iteration);
					ISerializableClustering serClustering = clustering
							.asSerializable();
					if (!isLoaded)
						res.unloadFromMemory();
					res.setParseClusterings(parseClusterings);
					res.setStoreClusterings(storeClusterings);
					return serClustering;
				} catch (Exception e) {
				}
			}

		}
		throw new UnknownRunResultException(
				"The specified run result of type parameter optimization could not be found.");
	}

	@Override
	public DataMatrix getPCAOfDataConfig(final String clientId,
			final String resultId, final String programConfig,
			final String dataConfig, final int numberPCs)
			throws UnknownRunResultException,
			RepositoryObjectSerializationException, IOException {
		for (IRepositoryObject r : this.repository
				.getCollectionStaticEntities(IRunResult.class)) {
			if (!(r instanceof IExecutionRunResult))
				continue;
			IExecutionRunResult res = (IExecutionRunResult) r;

			if (res.getIdentifier().equals(resultId)
					&& res.getProgramConfig().toString()
							.equals(programConfig.toString())
					&& res.getDataConfig().toString()
							.equals(dataConfig.toString())) {
				File file = res.getDataConfig().getDatasetConfig().getDataSet()
						.getFile().getParentFile();
				File[] listFiles = file.listFiles(new FilenameFilter() {

					@Override
					public boolean accept(File dir, String name) {
						return name.endsWith(".PCA");
					}
				});

				MatrixParser parser = new MatrixParser(
						listFiles[0].getAbsolutePath());
				parser.process();
				List<Pair<String, double[]>> coords = parser.getCoordinates();
				double[][] coordsMatrix = new double[coords.size()][];
				String[] ids = new String[coords.size()];
				for (int i = 0; i < coordsMatrix.length; i++) {
					coordsMatrix[i] = new double[Math.min(numberPCs,
							coords.get(i).getSecond().length)];
					for (int j = 0; j < coordsMatrix[i].length; j++)
						coordsMatrix[i][j] = coords.get(i).getSecond()[j];
					ids[i] = coords.get(i).getFirst();
				}
				return new DataMatrix(ids, coordsMatrix);
			}

		}
		throw new UnknownRunResultException(
				"The specified run result of type parameter optimization could not be found.");

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * serverclient.IBackendServer#setLogLevel(ch.qos.logback.classic.Level)
	 */
	@SuppressWarnings("unused")
	@Override
	public void setLogLevel(final String clientId, Level logLevel)
			throws RemoteException {
		((ch.qos.logback.classic.Logger) LoggerFactory
				.getLogger(Logger.ROOT_LOGGER_NAME)).setLevel(logLevel);
	}

	/**
	 * Change the log level of this JVM.
	 * 
	 * @param logLevel
	 *            The new log level
	 */
	public static void logLevel(Level logLevel) {
		((ch.qos.logback.classic.Logger) LoggerFactory
				.getLogger(Logger.ROOT_LOGGER_NAME)).setLevel(logLevel);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see serverclient.IBackendServer#getOptionsForDataSetGenerator(java.lang.
	 * String )
	 */
	@Override
	public Options getOptionsForDataSetGenerator(final String clientId,
			String generatorName) throws UnknownDataSetGeneratorException,
			DynamicComponentInitializationException, RemoteException {
		IDataSetGenerator generator = DataSetGenerator
				.parseFromString(repository, generatorName);
		return generator.getAllOptions();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see serverclient.IBackendServer#generateDataSet(java.lang.String,
	 * java.lang.String[])
	 */
	@SuppressWarnings("unused")
	@Override
	public boolean generateDataSet(final String clientId, String generatorName,
			String[] args) throws RemoteException {
		try {
			IDataSetGenerator generator = DataSetGenerator
					.parseFromString(this.repository, generatorName);
			generator.generate(args);
		} catch (UnknownDataSetGeneratorException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (DataSetGenerationException e) {
			e.printStackTrace();
		} catch (GoldStandardGenerationException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (RepositoryObjectDumpException e) {
			e.printStackTrace();
		} catch (RegisterException e) {
			e.printStackTrace();
		} catch (UnknownDistanceMeasureException e) {
			e.printStackTrace();
		} catch (DynamicComponentInitializationException e) {
			e.printStackTrace();
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see serverclient.IBackendServer#getQueue()
	 */
	@SuppressWarnings("unused")
	@Override
	public Collection<String> getQueue(final String clientId)
			throws RemoteException {
		final Collection<String> result = this.repository.getSupervisorThread()
				.getRunScheduler().getQueue(clientId);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IBackendServer#getFinishedRuns()
	 */
	@Override
	public Collection<String> getFinishedRuns(final String clientId) {
		return this.repository.getSupervisorThread().getRunScheduler()
				.getFinishedRuns(clientId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IBackendServer#getRunningRuns()
	 */
	@Override
	public Collection<String> getRunningRuns(final String clientId) {
		return this.repository.getSupervisorThread().getRunScheduler()
				.getRunningRuns(clientId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IBackendServer#getTerminatedRuns()
	 */
	@Override
	public Collection<String> getTerminatedRuns(final String clientId) {
		return this.repository.getSupervisorThread().getRunScheduler()
				.getTerminatedRuns(clientId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IBackendServer#getActiveThreads()
	 */
	@Override
	public Map<String, IterationRunnableStatus> getActiveThreads(
			final String clientId) throws RemoteException,
			OperationNotPermittedException, UnknownClientException {
		Map<String, IterationRunnableStatus> result = new HashMap<String, IterationRunnableStatus>();

		IRunSchedulerThread scheduler = this.getRepository()
				.getSupervisorThread().getRunScheduler();
		Map<Thread, IIterationRunnable<? extends IIterationWrapper, ?>> map = scheduler
				.getActiveIterationRunnables();
		for (Map.Entry<Thread, IIterationRunnable<? extends IIterationWrapper, ?>> e : map
				.entrySet()) {
			long startTime = e.getValue().getStartTime();
			String runnableState = e.getValue().getCurrentState();

			String parentRunnableName = "";
			String status = "";
			if (e.getValue() instanceof ClusteringIterationRunnable) {
				ExecutionRunRunnable r = (ExecutionRunRunnable) (e.getValue()
						.getParentRunnable());
				status = ((ClusteringIterationRunnable) e.getValue())
						.getIterationNumber() + " ("
						+ ((ClusteringIterationRunnable) e.getValue())
								.getParameterSet()
						+ ")";
				parentRunnableName = r.getRun().getName() + ": "
						+ r.getProgramConfig() + "," + r.getDataConfig();
			} else if (e.getValue() instanceof DataAnalysisIterationRunnable) {
				DataAnalysisRunRunnable r = (DataAnalysisRunRunnable) (e
						.getValue().getParentRunnable());

				status = ((AnalysisIterationRunnable) e.getValue())
						.getStatistic().toString();
				parentRunnableName = r.getRun().getName() + ": " + status + ","
						+ r.getDataConfig();
			} else if (e.getValue() instanceof RunAnalysisIterationRunnable) {
				RunAnalysisRunRunnable r = (RunAnalysisRunRunnable) (e
						.getValue().getParentRunnable());

				status = ((AnalysisIterationRunnable) e.getValue())
						.getStatistic().toString();
				parentRunnableName = r.getRun().getName() + ": " + status + ","
						+ r.getRunIdentifier();
			}
			// TODO
			// else if (e.getValue() instanceof
			// RunDataAnalysisIterationRunnable) {
			// RunDataAnalysisRunRunnable r = (RunDataAnalysisRunRunnable) (e
			// .getValue().getParentRunnable());
			//
			// status = ((AnalysisIterationRunnable) e.getValue())
			// .getStatistic().getAlias();
			// name = r.getRun().getName() + ": " + r.get;
			// }

			result.put(e.getKey().getName(), new IterationRunnableStatus(
					startTime, runnableState, parentRunnableName, status));
		}
		return result;
	}

	@Override
	public List<Triple<String, String, Long>> getIterationRunnableQueue(
			final String clientId) throws RemoteException {
		List<Triple<String, String, Long>> result = new ArrayList<Triple<String, String, Long>>();

		IRunSchedulerThread scheduler = this.getRepository()
				.getSupervisorThread().getRunScheduler();
		List<Runnable> runnables = scheduler.getIterationRunnableQueue();
		for (Runnable run : runnables) {
			IIterationRunnable e = (IIterationRunnable) run;
			long priority = e.getRun().getStartTime();

			String name = "";
			String status = "";
			if (e instanceof IClusteringIterationRunnable) {
				ExecutionRunRunnable r = (ExecutionRunRunnable) (e
						.getParentRunnable());
				status = ((ClusteringIterationRunnable) e).getIterationNumber()
						+ " ("
						+ ((ClusteringIterationRunnable) e).getParameterSet()
						+ ")";
				name = r.getRun().getName() + ": " + r.getProgramConfig() + ","
						+ r.getDataConfig();
			} else if (e instanceof IDataAnalysisIterationRunnable) {
				DataAnalysisRunRunnable r = (DataAnalysisRunRunnable) (e
						.getParentRunnable());

				status = ((AnalysisIterationRunnable) e).getStatistic()
						.toString();
				name = r.getRun().getName() + ": " + status + ","
						+ r.getDataConfig();
			} else if (e instanceof IRunAnalysisIterationRunnable) {
				RunAnalysisRunRunnable r = (RunAnalysisRunRunnable) (e
						.getParentRunnable());

				status = ((AnalysisIterationRunnable) e).getStatistic()
						.toString();
				name = r.getRun().getName() + ": " + status + ","
						+ r.getRunIdentifier();
			}
			result.add(Triple.getTriple(name, status, priority));
		}
		Collections.sort(result,
				new Comparator<Triple<String, String, Long>>() {

					public int compare(Triple<String, String, Long> o1,
							Triple<String, String, Long> o2) {
						return o1.getThird().compareTo(o2.getThird());
					};
				});
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IBackendServer#setDebugNoNewClusterings(
	 * boolean)
	 */
	@Override
	public void setDebugNoNewClusterings(final String clientId, boolean b)
			throws RemoteException {
		Repository.DEBUG_NO_NEW_CLUSTERINGS = b;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * serverclient.IBackendServer#getOptionsForDataRandomizer(java.lang.String
	 * )
	 */
	@Override
	public Options getOptionsForDataRandomizer(final String clientId,
			String randomizerName) throws UnknownDataRandomizerException,
			DynamicComponentInitializationException {
		IDataRandomizer randomizer = DataRandomizer.parseFromString(repository,
				randomizerName);
		return randomizer.getCustomOptions();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see serverclient.IBackendServer#randomizeDataConfig(java.lang.String,
	 * java.lang.String[])
	 */
	@SuppressWarnings("unused")
	@Override
	public boolean randomizeDataConfig(final String clientId,
			String randomizerName, String[] args)
			throws RemoteException, UnknownDataRandomizerException,
			DynamicComponentInitializationException, DataRandomizeException {
		IDataRandomizer randomizer = DataRandomizer
				.parseFromString(this.repository, randomizerName);
		randomizer.randomize(args);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IBackendServer#setThreadNumber(int)
	 */
	@Override
	public void setThreadNumber(final String clientId, int threadNumber)
			throws RemoteException {
		this.repository.getSupervisorThread().getRunScheduler()
				.updateThreadPoolSize(threadNumber);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IBackendServer#getAbsoluteRepositoryPath()
	 */
	@Override
	public String getAbsoluteRepositoryPath(final String clientId)
			throws RemoteException {
		return this.repository.getBasePath();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IBackendServer#getDynamicComponents(java.lang.
	 * Class)
	 */
	@Override
	public <R extends IRepositoryObjectDynamicComponent> Collection<ISerializableWrapperDynamicComponent<R>> getDynamicComponents(
			final String clientId, Class<R> type) throws RemoteException {
		Collection<ISerializableWrapperDynamicComponent<R>> result = new HashSet<ISerializableWrapperDynamicComponent<R>>();

		Map<Class<? extends IRepositoryObjectDynamicComponent>, ComparableVersion> classes = this.repository
				.getClassesWithVersions(type);

		for (Class<? extends IRepositoryObjectDynamicComponent> clazz : classes
				.keySet()) {
			ClustEvalAlias aliasAnno = clazz
					.getAnnotation(ClustEvalAlias.class);
			result.add(new SerializableWrapperDynamicComponent<R>(type, null,
					clazz.getSimpleName(), classes.get(clazz).toString(),
					aliasAnno != null ? aliasAnno.alias() : null));
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IBackendServer#deleteDynamicComponent(java.lang
	 * .Class, java.lang.String, java.lang.String)
	 */
	@Override
	public void deleteDynamicComponent(final String clientId,
			Class<? extends IRepositoryObjectDynamicComponent> clazz,
			String name, String version) throws RemoteException,
			OperationNotPermittedException, UnknownDynamicComponentException,
			UnknownClientException, DeletionException {
		if (!getClientPermissions(clientId).canDelete())
			throw new OperationNotPermittedException();

		String qualifiedName = clazz.getPackage().getName() + "." + name;

		if (!this.repository.isClassRegistered(clazz, qualifiedName,
				new ComparableVersion(version))) {
			throw new UnknownDynamicComponentException(clazz,
					name + ":" + version,
					"The specified dynamic component could not be found");
		}

		Class<?> registeredClass = this.repository.getRegisteredClass(clazz,
				qualifiedName, new ComparableVersion(version));

		File f = new File(FileUtils.buildPath(repository.getBasePath(clazz),
				String.format("%s-%s.jar", name, version)));
		if (!f.exists())
			throw new DeletionException(
					"The file corresponding to the specified component could not be found");
		FileUtils.delete(f);

		repository.unregisterClass(clazz, (Class) registeredClass);
	}

	@Override
	public String getNextVersionForRepositoryObject(final String clientId,
			final Class<? extends IRepositoryObject> clazz, final String name,
			final String currentVersion) throws RemoteException {
		Collection<IRepositoryObject> res = (Collection) this.repository
				.getCollectionStaticEntities(clazz);

		ComparableVersion maxVersion = new ComparableVersion(currentVersion);

		for (IRepositoryObject obj : res) {
			if (obj.getName().equals(name)
					&& obj.getVersion().compareTo(maxVersion) > 0)
				maxVersion = obj.getVersion();
		}

		try {
			Map<ISerializableWrapperRepositoryObject<IRepositoryObject>, IRepositoryObject> errorObjects = this.repository
					.getErrorObjects().get(clazz);

			for (IRepositoryObject obj : errorObjects.values()) {
				if (obj.getName().equals(name)
						&& obj.getVersion().compareTo(maxVersion) > 0)
					maxVersion = obj.getVersion();
			}
		} catch (ObjectVersionNotFoundException | ObjectNotFoundException
				| NullPointerException e) {
		}

		String versionString = maxVersion.toString();
		versionString = versionString.replace("-SNAPSHOT", "");
		return "" + (Long.parseLong(versionString) + 1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IBackendServer#getDataConfigurations()
	 */
	@Override
	public Collection<ISerializableDataConfig> getDataConfigurations(
			final String clientId)
			throws RemoteException, RepositoryObjectSerializationException {
		Collection<ISerializableDataConfig> result = new HashSet<ISerializableDataConfig>();

		Collection<IDataConfig> res = this.repository
				.getCollectionStaticEntities(IDataConfig.class);

		for (IDataConfig dc : res) {
			if (!(dc.getName().startsWith(
					ToolRuntimeEvaluationRun.RUNTIME_EVALUATION_DATASET_MINOR_NAME_PREFIX)))
				result.add((SerializableDataConfig) dc.asSerializable());
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IBackendServer#getDataConfiguration(java.lang.
	 * String, java.lang.String)
	 */
	@Override
	public ISerializableDataConfig getDataConfiguration(final String clientId,
			String name, String version) throws RemoteException,
			ObjectNotFoundException, ObjectVersionNotFoundException,
			RepositoryObjectSerializationException {
		IDataConfig res;
		ISerializableDataConfig ser = null;

		Map<String, Map<ComparableVersion, IDataConfig>> objects = this.repository
				.getStaticEntitiesWithVersions(IDataConfig.class);
		Map<ISerializableWrapperRepositoryObject<IRepositoryObject>, IRepositoryObject> errorObjects = this.repository
				.getErrorObjects().get(IDataConfig.class);

		if (objects.containsKey(name) && objects.get(name)
				.containsKey(new ComparableVersion(version))) {
			res = (IDataConfig) objects.get(name)
					.get(new ComparableVersion(version));
			ser = res.asSerializable();
		} else if (errorObjects.containsKey(
				new ErrorSerializableWrapper<IRepositoryObject>(null, name,
						version))) {
			res = (IDataConfig) errorObjects
					.get(new ErrorSerializableWrapper<IRepositoryObject>(null,
							name, version));
			ser = res.asSerializable();
		}

		return ser;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IBackendServer#getProgramConfiguration(java.
	 * lang.String, java.lang.String)
	 */
	@Override
	public ISerializableProgramConfig getProgramConfiguration(
			final String clientId, String name, String version)
			throws RemoteException, ObjectNotFoundException,
			ObjectVersionNotFoundException,
			RepositoryObjectSerializationException {
		IProgramConfig res;
		ISerializableProgramConfig ser = null;

		Map<String, Map<ComparableVersion, IProgramConfig>> objects = this.repository
				.getStaticEntitiesWithVersions(IProgramConfig.class);
		Map<ISerializableWrapperRepositoryObject<IRepositoryObject>, IRepositoryObject> errorObjects = this.repository
				.getErrorObjects().get(IProgramConfig.class);

		if (objects.containsKey(name) && objects.get(name)
				.containsKey(new ComparableVersion(version))) {
			res = (IProgramConfig) objects.get(name)
					.get(new ComparableVersion(version));
			ser = res.asSerializable();
		} else if (errorObjects.containsKey(
				new ErrorSerializableWrapper<IRepositoryObject>(null, name,
						version))) {
			res = (IProgramConfig) errorObjects
					.get(new ErrorSerializableWrapper<IRepositoryObject>(null,
							name, version));
			ser = res.asSerializable();
		}

		return ser;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IBackendServer#getRun(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public ISerializableRun getRun(final String clientId, String name,
			String version) throws RemoteException, ObjectNotFoundException,
			ObjectVersionNotFoundException,
			RepositoryObjectSerializationException {
		IRun res;
		ISerializableRun<? extends IRun> ser = null;

		Map<String, Map<ComparableVersion, IRun>> objects = this.repository
				.getStaticEntitiesWithVersions(IRun.class);
		Map<ISerializableWrapperRepositoryObject<IRepositoryObject>, IRepositoryObject> errorObjects = this.repository
				.getErrorObjects().get(IRun.class);

		if (objects.containsKey(name) && objects.get(name)
				.containsKey(new ComparableVersion(version))) {
			res = (IRun) objects.get(name).get(new ComparableVersion(version));
			ser = res.asSerializable();
		} else if (errorObjects.containsKey(
				new ErrorSerializableWrapper<IRepositoryObject>(null, name,
						version))) {
			res = (IRun) errorObjects
					.get(new ErrorSerializableWrapper<IRepositoryObject>(null,
							name, version));
			ser = res.asSerializable();
		}

		return ser;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IBackendServer#getProgram(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public ISerializableProgram getProgram(final String clientId, String name,
			String version) throws RemoteException, ObjectNotFoundException,
			ObjectVersionNotFoundException,
			RepositoryObjectSerializationException {
		IProgram res;

		Map<String, Map<ComparableVersion, IStandaloneProgram>> objects = this.repository
				.getStaticEntitiesWithVersions(IStandaloneProgram.class);
		Map<ISerializableWrapperRepositoryObject<IRepositoryObject>, IRepositoryObject> errorObjects = this.repository
				.getErrorObjects().get(IStandaloneProgram.class);
		Map<String, Map<ComparableVersion, Class<? extends IRepositoryObjectDynamicComponent>>> rPrograms = this.repository
				.getClassesWithNameAndVersion(IRProgram.class);

		if (objects.containsKey(name) && objects.get(name)
				.containsKey(new ComparableVersion(version))) {
			res = (IProgram) objects.get(name)
					.get(new ComparableVersion(version));
			return res.asSerializable();
		}

		if (rPrograms.containsKey(name) && rPrograms.get(name)
				.containsKey(new ComparableVersion(version))) {
			try {
				return RProgram.parseFromString(repository,
						name + ":" + version, false).asSerializable();
			} catch (UnknownRProgramException
					| DynamicComponentInitializationException
					| UnknownDataSetFormatException
					| UnknownRunResultFormatException e) {
			}
		}

		if (errorObjects.containsKey(
				new ErrorSerializableWrapper<IRepositoryObject>(null, name,
						version))) {
			res = (IProgram) errorObjects
					.get(new ErrorSerializableWrapper<IRepositoryObject>(null,
							name, version));
			return res.asSerializable();
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IBackendServer#getDataSet(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public ISerializableDataSet getDataSet(final String clientId, String name,
			String version) throws RemoteException, ObjectNotFoundException,
			ObjectVersionNotFoundException,
			RepositoryObjectSerializationException {
		IDataSet res;
		ISerializableDataSet ser = null;

		Map<String, Map<ComparableVersion, IDataSet>> objects = this.repository
				.getStaticEntitiesWithVersions(IDataSet.class);
		Map<ISerializableWrapperRepositoryObject<IRepositoryObject>, IRepositoryObject> errorObjects = this.repository
				.getErrorObjects().get(IDataSet.class);

		if (objects.containsKey(name) && objects.get(name)
				.containsKey(new ComparableVersion(version))) {
			res = (IDataSet) objects.get(name)
					.get(new ComparableVersion(version));
			ser = res.asSerializable();
		} else if (errorObjects.containsKey(
				new ErrorSerializableWrapper<IRepositoryObject>(null, name,
						version))) {
			res = (IDataSet) errorObjects
					.get(new ErrorSerializableWrapper<IRepositoryObject>(null,
							name, version));
			ser = res.asSerializable();
		}

		return ser;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IBackendServer#getGoldStandard(java.lang.
	 * String, java.lang.String)
	 */
	@Override
	public ISerializableGoldStandard getGoldStandard(final String clientId,
			String name, String version) throws RemoteException,
			ObjectNotFoundException, ObjectVersionNotFoundException,
			RepositoryObjectSerializationException {
		IGoldStandard res;
		ISerializableGoldStandard ser = null;

		Map<String, Map<ComparableVersion, IGoldStandard>> objects = this.repository
				.getStaticEntitiesWithVersions(IGoldStandard.class);
		Map<ISerializableWrapperRepositoryObject<IRepositoryObject>, IRepositoryObject> errorObjects = this.repository
				.getErrorObjects().get(IGoldStandard.class);

		if (objects.containsKey(name) && objects.get(name)
				.containsKey(new ComparableVersion(version))) {
			res = (IGoldStandard) objects.get(name)
					.get(new ComparableVersion(version));
			ser = res.asSerializable();
		} else if (errorObjects.containsKey(
				new ErrorSerializableWrapper<IRepositoryObject>(null, name,
						version))) {
			res = (IGoldStandard) errorObjects
					.get(new ErrorSerializableWrapper<IRepositoryObject>(null,
							name, version));
			ser = res.asSerializable();
		}

		return ser;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IBackendServer#getDataSetConfiguration(java.
	 * lang.String, java.lang.String)
	 */
	@Override
	public ISerializableDataSetConfig getDataSetConfiguration(
			final String clientId, String name, String version)
			throws RemoteException, ObjectNotFoundException,
			ObjectVersionNotFoundException,
			RepositoryObjectSerializationException {
		IDataSetConfig res;
		ISerializableDataSetConfig ser = null;

		Map<String, Map<ComparableVersion, IDataSetConfig>> objects = this.repository
				.getStaticEntitiesWithVersions(IDataSetConfig.class);
		Map<ISerializableWrapperRepositoryObject<IRepositoryObject>, IRepositoryObject> errorObjects = this.repository
				.getErrorObjects().get(IDataSetConfig.class);

		if (objects.containsKey(name) && objects.get(name)
				.containsKey(new ComparableVersion(version))) {
			res = (IDataSetConfig) objects.get(name)
					.get(new ComparableVersion(version));
			ser = res.asSerializable();
		} else if (errorObjects.containsKey(
				new ErrorSerializableWrapper<IRepositoryObject>(null, name,
						version))) {
			res = (IDataSetConfig) errorObjects
					.get(new ErrorSerializableWrapper<IRepositoryObject>(null,
							name, version));
			ser = res.asSerializable();
		}

		return ser;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IBackendServer#getGoldStandardConfiguration(
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public ISerializableGoldStandardConfig getGoldStandardConfiguration(
			final String clientId, String name, String version)
			throws RemoteException, ObjectNotFoundException,
			ObjectVersionNotFoundException,
			RepositoryObjectSerializationException {
		IGoldStandardConfig res;
		ISerializableGoldStandardConfig ser = null;

		Map<String, Map<ComparableVersion, IGoldStandardConfig>> objects = this.repository
				.getStaticEntitiesWithVersions(IGoldStandardConfig.class);
		Map<ISerializableWrapperRepositoryObject<IRepositoryObject>, IRepositoryObject> errorObjects = this.repository
				.getErrorObjects().get(IGoldStandardConfig.class);

		if (objects.containsKey(name) && objects.get(name)
				.containsKey(new ComparableVersion(version))) {
			res = (IGoldStandardConfig) objects.get(name)
					.get(new ComparableVersion(version));
			ser = res.asSerializable();
		} else if (errorObjects.containsKey(
				new ErrorSerializableWrapper<IRepositoryObject>(null, name,
						version))) {
			res = (IGoldStandardConfig) errorObjects
					.get(new ErrorSerializableWrapper<IRepositoryObject>(null,
							name, version));
			ser = res.asSerializable();
		}

		return ser;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IBackendServer#getDataSetConfigurations()
	 */
	@Override
	public Collection<ISerializableDataSetConfig> getDataSetConfigurations(
			final String clientId)
			throws RemoteException, RepositoryObjectSerializationException {
		Collection<ISerializableDataSetConfig> result = new HashSet<ISerializableDataSetConfig>();
		for (IDataSetConfig obj : this.repository
				.getCollectionStaticEntities(IDataSetConfig.class))
			if (!(obj.getName().startsWith(
					ToolRuntimeEvaluationRun.RUNTIME_EVALUATION_DATASET_MINOR_NAME_PREFIX)))
				result.add((ISerializableDataSetConfig) obj.asSerializable());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IBackendServer#getStatsOfDataConfiguration(java
	 * .lang.String, java.lang.String)
	 */
	@Override
	public Map<String, Object> getStatsOfDataConfiguration(
			final String clientId, String name, String version)
			throws RemoteException {
		IDataConfig res = (IDataConfig) this.repository
				.getStaticEntitiesWithVersions(IDataConfig.class).get(name)
				.get(new ComparableVersion(version));
		IDataSet dataSet = res.getDatasetConfig().getDataSet();
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			dataSet.loadIntoMemory(this);
			result.put("numberOfSamples", dataSet.getNumberOfSamples());
			if (dataSet instanceof AbsoluteDataSet)
				result.put("dimensionality",
						((AbsoluteDataSet) dataSet).getDimensionality());
			dataSet.unloadFromMemory(this);
		} catch (InvalidDataSetFormatVersionException
				| UnknownDataSetFormatException | IllegalArgumentException
				| IOException e) {
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IBackendServer#getGoldStandardConfigurations()
	 */
	@Override
	public Collection<ISerializableGoldStandardConfig> getGoldStandardConfigurations(
			final String clientId)
			throws RemoteException, RepositoryObjectSerializationException {
		Collection<ISerializableGoldStandardConfig> result = new HashSet<ISerializableGoldStandardConfig>();
		for (IRepositoryObject obj : this.repository
				.getCollectionStaticEntities(IGoldStandardConfig.class))
			result.add((ISerializableGoldStandardConfig) obj.asSerializable());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IBackendServer#getGoldStandards()
	 */
	@Override
	public Collection<ISerializableGoldStandard> getGoldStandards(
			final String clientId)
			throws RemoteException, RepositoryObjectSerializationException {
		Collection<ISerializableGoldStandard> result = new HashSet<ISerializableGoldStandard>();
		for (IRepositoryObject obj : this.repository
				.getCollectionStaticEntities(IGoldStandard.class))
			result.add((ISerializableGoldStandard) obj.asSerializable());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IBackendServer#getProgramConfigurations()
	 */
	@Override
	public Collection<ISerializableProgramConfig> getProgramConfigurations(
			final String clientId)
			throws RemoteException, RepositoryObjectSerializationException {
		Collection<ISerializableProgramConfig> result = new HashSet<ISerializableProgramConfig>();
		for (IRepositoryObject obj : this.repository
				.getCollectionStaticEntities(IProgramConfig.class))
			result.add((ISerializableProgramConfig) ((IProgramConfig) obj)
					.asSerializable());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IBackendServer#
	 * getParametersForProgramConfiguration(java.lang.String)
	 */
	@Override
	public Map<String, Map<String, String>> getParametersForProgramConfiguration(
			final String clientId, final String programConfigName)
			throws RemoteException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException {
		IRepositoryObject programConfig = repository
				.getStaticObjectWithNameAndVersion(IProgramConfig.class,
						programConfigName);
		if (programConfig == null) {
			return null;
		}
		Map<String, Map<String, String>> result = new HashMap<String, Map<String, String>>();
		for (IProgramParameter<?> param : ((IProgramConfig) programConfig)
				.getParameters()) {
			Map<String, String> paramMap = new HashMap<String, String>();
			paramMap.put("defaultValue", param.getDefault());
			if (param.isMinValueSet())
				paramMap.put("minValue", param.getMinValue());
			if (param.isMaxValueSet())
				paramMap.put("maxValue", param.getMaxValue());
			if (param.isOptionsSet())
				paramMap.put("options",
						StringExt.paste(",", param.getOptions()));

			result.put(param.getName(), paramMap);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IBackendServer#getParsingExceptionMessages(java
	 * .lang.String)
	 */
	@Override
	public Map<ISerializableWrapperRepositoryObject<? extends IRepositoryObject>, List<String>> getParsingExceptionMessages(
			final String clientId,
			final Class<? extends IRepositoryObject>... classes)
			throws RemoteException, OperationNotPermittedException {
		if (!config.administratorUUIDs.contains(clientId))
			throw new OperationNotPermittedException();
		Map<ISerializableWrapperRepositoryObject<? extends IRepositoryObject>, List<String>> result = new HashMap<ISerializableWrapperRepositoryObject<? extends IRepositoryObject>, List<String>>();

		Map<Class<? extends IRepositoryObject>, Map<ISerializableWrapperRepositoryObject<? extends IRepositoryObject>, List<Throwable>>> knownFinderExceptions = this.repository
				.getKnownFinderExceptions();

		for (Class<? extends IRepositoryObject> clazz : classes) {
			if (!knownFinderExceptions.containsKey(clazz))
				continue;
			for (ISerializableWrapperRepositoryObject<? extends IRepositoryObject> serializable : knownFinderExceptions
					.get(clazz).keySet()) {
				List<String> list = new ArrayList<String>();
				for (Throwable t : knownFinderExceptions.get(clazz)
						.get(serializable))
					if (t instanceof UnknownDynamicComponentException)
						list.add(
								String.format("%s: %s",
										((UnknownDynamicComponentException) t)
												.getObjectName(),
										t.getMessage()));
					else
						list.add(t.getMessage());
				result.put(serializable, list);
			}
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IBackendServer#getRunExceptions(int,
	 * java.lang.String)
	 */
	@Override
	public List<String> getRunExceptionMessages(final String clientId,
			final String uniqueRunId, final boolean getWarningExceptions)
			throws RemoteException {
		return this.repository.getSupervisorThread().getRunScheduler()
				.getRunExceptionMessages(clientId, uniqueRunId,
						getWarningExceptions);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IBackendServer#clearRunExceptions(java.lang.
	 * String, java.lang.String)
	 */
	@Override
	public void clearRunExceptions(String clientId, String uniqueRunId)
			throws RemoteException {
		this.repository.getSupervisorThread().getRunScheduler()
				.clearRunExceptions(clientId, uniqueRunId);
		this.repository.getSupervisorThread().getRunScheduler()
				.clearRunWarningExceptions(clientId, uniqueRunId);
	}

	@Override
	public void forgetRunStatus(String clientId, String uniqueRunId)
			throws RemoteException {
		this.repository.getSupervisorThread().getRunScheduler()
				.forgetRunStatus(clientId, uniqueRunId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IBackendServer#getThreadNumber()
	 */
	@Override
	public int getThreadNumber(final String clientId) throws RemoteException {
		return this.repository.getSupervisorThread().getRunScheduler()
				.getThreadPoolSize();
	}

	// TODO
	// /*
	// * (non-Javadoc)
	// *
	// * @see
	// *
	// de.clusteval.serverclient.IBackendServer#addBackendServerSlave(java.lang.
	// * String)
	// */
	// @Override
	// public boolean addBackendServerSlave(String host, int port) throws
	// RemoteException {
	// try {
	// Registry registry = LocateRegistry.getRegistry(host, port);
	// IBackendServer server = (IBackendServer) registry.lookup("EvalServer");
	// IRunSchedulerThread runScheduler =
	// this.repository.getSupervisorThread().getRunScheduler();
	// if (runScheduler.isBackendServerSlaveRegistered(server)) {
	// this.log.warn(String.format("Slave computation server already added:
	// '%s:%d'", host, port));
	// return false;
	// }
	// runScheduler.addBackendServerSlave(server);
	// this.log.info(String.format("Added slave computation server: '%s:%d' with
	// %d threads", host, port,
	// server.getThreadNumber()));
	// } catch (ConnectException e) {
	// this.log.error("Could not connect to server");
	// throw e;
	// } catch (RemoteException e) {
	// e.printStackTrace();
	// } catch (NotBoundException e) {
	// e.printStackTrace();
	// }
	//
	// return false;
	// }
	//
	// /*
	// * (non-Javadoc)
	// *
	// * @see
	// * de.clusteval.serverclient.IBackendServer#delicateIteration(java.lang.
	// * String, java.lang.String, java.lang.String,
	// * de.clusteval.program.ParameterSet)
	// */
	// @Override
	// public IClustering delegateIteration(String clientId, String
	// repositoryAbsPath, String dataConfigId,
	// String programConfigId, ParameterSet paramSet) throws RemoteException {
	// // Repository repositoryForExactPath =
	// // Repository.getRepositoryForExactPath(repositoryAbsPath);
	// // SupervisorThread supervisorThread =
	// // repositoryForExactPath.getSupervisorThread();
	// // RunSchedulerThread runScheduler = supervisorThread.getRunScheduler();
	// //
	// // ExecutionIterationWrapper executionIterationWrapper = new
	// // ExecutionIterationWrapper();
	// // ClusteringIterationRunnable executionIterationRunnable = new
	// // ClusteringIterationRunnable(executionIterationWrapper, format);
	// //
	// // runScheduler.registerIterationRunnable(executionIterationRunnable)
	// return null;
	// }

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IBackendServer#refreshDatabaseMaterializedViews
	 * ()
	 */
	@Override
	public void refreshDatabaseMaterializedViews(final String clientId)
			throws RemoteException {
		if (!this.repository.getRepositoryConfig().getSQLConfig().usesSql()) {
			this.log.info(
					"Cannot refresh database materialized views: No database is in use.");
			return;
		}
		if (!this.repository.getRepositoryConfig().getSQLConfig()
				.getDatabaseType().equals(DB_TYPE.POSTGRESQL))
			this.log.info(
					"Cannot refresh database materialized views: Only supported by postgresql.");
		this.repository.getSqlCommunicator().refreshMaterializedViews();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IBackendServer#reparseAllRunResultsIntoDatabase
	 * ()
	 */
	@Override
	public void reparseAllRunResultsIntoDatabase(final String clientId)
			throws RemoteException {
		if (!this.repository.getRepositoryConfig().getSQLConfig().usesSql()) {
			this.log.info(
					"Cannot reparse run results into database: No database is in use.");
			return;
		}
		this.log.info("Deleting all run results from the database");
		try {
			this.repository.getSqlCommunicator().deleteAllRunResults();
			Collection<IRunResult> runResults = this.repository
					.getCollectionStaticEntities(IRunResult.class);
			for (IRepositoryObject res : runResults) {
				res.unregister();
			}
			this.log.info("Reparsing run results into the database ...");
			for (IRepositoryObject res : runResults) {
				res.register();
			}
			this.log.info(" ... done reparsing run results into the database");
		} catch (Exception e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IBackendServer#reparseRunResultIntoDatabase(
	 * java.lang.String)
	 */
	@Override
	public void reparseRunResultIntoDatabase(final String clientId,
			String runResultId) throws RemoteException {
		if (!this.repository.getRepositoryConfig().getSQLConfig().usesSql()) {
			this.log.info(
					"Cannot reparse run results into database: No database is in use.");
			return;
		}
		this.log.info(
				"Deleting run result " + runResultId + " from the database");
		try {
			this.repository.getSqlCommunicator().deleteRunResult(runResultId);
			IRunResult runResult = this.repository
					.getRegisteredRunResult(runResultId);
			if (runResult == null) {
				this.log.info(String.format(
						"No run result with name '%s' exists.", runResultId));
				return;
			}
			runResult.unregister();
			this.log.info("Reparsing run result " + runResultId
					+ " into the database ...");
			runResult.register();
			this.log.info(" ... done reparsing run result " + runResultId
					+ " into the database");
		} catch (Exception e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IBackendServer#getUserPermissions()
	 */
	@Override
	public IClientPermissions getClientPermissions(final String UUID)
			throws RemoteException, UnknownClientException {
		if (!this.userPermissions.containsKey(UUID))
			throw new UnknownClientException();
		return this.userPermissions.get(UUID);
	}

	@Override
	public boolean createNewDatasetWithAttributesFromExisting(
			final String clientId, final ISerializableDataSet<IDataSet> dataSet,
			final ISerializableDataSet<IDataSet> existingDataSet)
			throws DeserializationException, UnknownClientException,
			OperationNotPermittedException, IOException, RegisterException,
			DataSetParseException {
		IDataSet existing = existingDataSet.deserialize(repository);
		File file = existing.getFile();
		byte[] bytesDatasetFile = Files.readAllBytes(file.toPath());
		List<byte[]> bytesAdditionalDatasetFiles = new ArrayList<>();
		for (int i = 0; i < existing.getDataSetFormat()
				.getNumberAdditionalFiles(existing); i++) {
			bytesAdditionalDatasetFiles.add(Files.readAllBytes(
					new File(file.getAbsolutePath() + ".supp" + (i + 1))
							.toPath()));
		}

		return this.uploadDataset(clientId, dataSet, bytesDatasetFile,
				bytesAdditionalDatasetFiles);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IBackendServer#uploadDataset(java.lang.String,
	 * java.lang.String, java.lang.String, byte[])
	 */
	@Override
	public boolean uploadDataset(final String clientId,
			final ISerializableDataSet<IDataSet> dataSet,
			final byte[] bytesDatasetFile,
			final List<byte[]> bytesAdditionalDatasetFiles)
			throws DeserializationException, UnknownClientException,
			OperationNotPermittedException, IOException, RegisterException,
			DataSetParseException {
		if (!getClientPermissions(clientId).canUpload())
			throw new OperationNotPermittedException();
		final String filePath = FileUtils.buildPath(
				repository.getBasePath(IDataSet.class), String.format("%s.v%s",
						dataSet.getName(), dataSet.getVersion()));
		new File(filePath).getParentFile().mkdirs();
		Path p = Paths.get(filePath);

		// write header into file
		BufferedWriter writer = new BufferedWriter(new FileWriter(p.toFile()));
		writer.write("// dataSetFormat = " + dataSet.getDatasetFormat() + "\n");
		writer.write("// dataSetType = " + dataSet.getDatasetType() + "\n");
		writer.write("// dataSetFormatVersion = 1\n");
		writer.write("// alias = " + dataSet.getAlias() + "\n");
		writer.write("// websiteVisibility = show_always\n");

		// filter out header
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(
						new ByteArrayInputStream(bytesDatasetFile)));
		while (bufferedReader.ready()) {
			String line = bufferedReader.readLine().trim();
			if (line.startsWith("//"))
				continue;
			writer.write(line);
			writer.newLine();
		}

		writer.close();

		// write additional files (if available)
		for (int i = 0; i < bytesAdditionalDatasetFiles.size(); i++) {
			// String addFileName =
			// dataSet.getAdditionalDataSetFileNames().get(i);
			String additionalFilePath = String.format("%s.supp%d", filePath,
					i + 1);
			new File(additionalFilePath).getParentFile().mkdirs();
			p = Paths.get(additionalFilePath);
			Files.write(p, bytesAdditionalDatasetFiles.get(i));
		}

		IDataSet deserialize = dataSet.deserialize(repository);
		boolean isLoaded = deserialize.isInMemory();
		try {
			deserialize.loadIntoMemory(this);
		} catch (Exception e) {
			new File(filePath).delete();
			for (int i = 0; i < bytesAdditionalDatasetFiles.size(); i++) {
				String additionalFilePath = String.format("%s.supp%d", filePath,
						i + 1);
				new File(additionalFilePath).delete();
			}
			throw new DataSetParseException(String.format(
					"Your dataset is not compatible to the specified format: %s",
					deserialize.getDataSetFormat().toString()));
		} finally {
			if (!isLoaded && deserialize.isInMemory())
				deserialize.unloadFromMemory(this);
		}
		deserialize.register();

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IBackendServer#uploadGoldStandard(java.lang.
	 * String, java.lang.String, java.lang.String, byte[])
	 */
	@Override
	public boolean uploadGoldStandard(final String clientId,
			final ISerializableGoldStandard goldStandard, byte[] bytes)
			throws UnknownClientException, OperationNotPermittedException,
			IOException, RegisterException, DeserializationException {
		if (!getClientPermissions(clientId).canUpload())
			throw new OperationNotPermittedException();
		final String filePath = FileUtils.buildPath(
				repository.getBasePath(IGoldStandard.class),
				String.format("%s.v%s", goldStandard.getName(),
						goldStandard.getVersion()));
		new File(filePath).getParentFile().mkdirs();
		Files.write(Paths.get(filePath), bytes);

		IGoldStandard deserialize = goldStandard.deserialize(repository);
		deserialize.register();

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IBackendServer#uploadDataConfig(java.lang.
	 * String, java.lang.String, byte[])
	 */
	@Override
	public boolean uploadDataConfig(final String clientId,
			final ISerializableDataConfig dataConfig)
			throws UnknownClientException, OperationNotPermittedException,
			IOException, RegisterException, DeserializationException,
			RepositoryObjectDumpException {
		if (!getClientPermissions(clientId).canUpload())
			throw new OperationNotPermittedException();
		final String filePath = FileUtils.buildPath(
				repository.getBasePath(IDataConfig.class),
				String.format("%s.v%s.dataconfig", dataConfig.getName(),
						dataConfig.getVersion()));
		new File(filePath).getParentFile().mkdirs();

		IDataConfig deserialize = dataConfig.deserialize(repository);
		deserialize.dumpToFile();
		deserialize.register();

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IBackendServer#uploadDataSetConfig(java.lang.
	 * String, java.lang.String, byte[])
	 */
	@Override
	public boolean uploadDataSetConfig(final String clientId,
			final ISerializableDataSetConfig dataSetConfig)
			throws UnknownClientException, OperationNotPermittedException,
			IOException, RegisterException, DeserializationException,
			RepositoryObjectDumpException {
		if (!getClientPermissions(clientId).canUpload())
			throw new OperationNotPermittedException();
		final String filePath = FileUtils
				.buildPath(repository.getBasePath(IDataSetConfig.class),
						String.format("%s.v%s.dsconfig",
								dataSetConfig.getName(),
								dataSetConfig.getVersion()));
		new File(filePath).getParentFile().mkdirs();

		IDataSetConfig deserialize = dataSetConfig.deserialize(repository);
		deserialize.dumpToFile();
		deserialize.register();

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IBackendServer#uploadGoldStandardConfig(java.
	 * lang.String, java.lang.String, byte[])
	 */
	@Override
	public boolean uploadGoldStandardConfig(final String clientId,
			final ISerializableGoldStandardConfig gsConfig)
			throws UnknownClientException, OperationNotPermittedException,
			IOException, RegisterException, DeserializationException,
			RepositoryObjectDumpException {
		if (!getClientPermissions(clientId).canUpload())
			throw new OperationNotPermittedException();
		final String filePath = FileUtils.buildPath(
				repository.getBasePath(IGoldStandardConfig.class),
				String.format("%s.v%s.gsconfig", gsConfig.getName(),
						gsConfig.getVersion()));
		new File(filePath).getParentFile().mkdirs();

		IGoldStandardConfig deserialize = gsConfig.deserialize(repository);
		deserialize.dumpToFile();
		deserialize.register();

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IBackendServer#uploadRun(java.lang.String,
	 * java.lang.String, byte[])
	 */
	@Override
	public boolean uploadRun(final String clientId,
			final ISerializableRun<IRun> run) throws UnknownClientException,
			OperationNotPermittedException, IOException, RegisterException,
			DeserializationException, RepositoryObjectDumpException {
		if (!getClientPermissions(clientId).canUpload())
			throw new OperationNotPermittedException();
		final String filePath = FileUtils.buildPath(
				repository.getBasePath(IRun.class),
				String.format("%s.v%s.run", run.getName(), run.getVersion()));
		new File(filePath).getParentFile().mkdirs();
		IRun deserialize = run.deserialize(repository);
		deserialize.dumpToFile();
		deserialize.register();

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IBackendServer#uploadProgram(java.lang.String,
	 * de.clusteval.program.SerializableProgram, byte[])
	 */
	@Override
	public boolean uploadStandaloneProgram(String clientId,
			ISerializableStandaloneProgram program, byte[] bytes)
			throws UnknownClientException, OperationNotPermittedException,
			IOException, DeserializationException, RegisterException {
		if (!getClientPermissions(clientId).canUpload())
			throw new OperationNotPermittedException();
		final String filePath;
		filePath = FileUtils.buildPath(
				repository.getBasePath(IStandaloneProgram.class),
				String.format("%s.v%s", program.getMajorName(),
						program.getVersion()),
				program.getMinorName());
		new File(filePath).getParentFile().mkdirs();
		Files.write(Paths.get(filePath), bytes);

		Files.setPosixFilePermissions(Paths.get(filePath),
				new HashSet<>(Arrays.asList(PosixFilePermission.OWNER_EXECUTE,
						PosixFilePermission.GROUP_EXECUTE)));

		IStandaloneProgram deserialize = program.deserialize(repository);
		deserialize.register();

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IBackendServer#uploadProgramConfig(java.lang.
	 * String, java.lang.String, byte[])
	 */
	@Override
	public boolean uploadProgramConfig(final String clientId,
			final ISerializableProgramConfig programConfig)
			throws UnknownClientException, OperationNotPermittedException,
			IOException, RegisterException, DeserializationException,
			RepositoryObjectDumpException {
		if (!getClientPermissions(clientId).canUpload())
			throw new OperationNotPermittedException();
		final String filePath = FileUtils.buildPath(
				repository.getBasePath(IProgramConfig.class),
				String.format("%s.v%s.config", programConfig.getName(),
						programConfig.getVersion()));
		new File(filePath).getParentFile().mkdirs();

		IProgramConfig deserialize = programConfig.deserialize(repository);
		deserialize.dumpToFile();
		deserialize.register();

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IBackendServer#deleteDataset(java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public boolean deleteDataset(String clientId,
			final ISerializableDataSet dataSet) throws RemoteException,
			IOException, OperationNotPermittedException, UnknownClientException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException {
		if (!getClientPermissions(clientId).canDelete())
			throw new OperationNotPermittedException();
		File f = new File(
				FileUtils.buildPath(repository.getBasePath(IDataSet.class),
						dataSet.getName() + (dataSet.getVersion().isEmpty()
								? ""
								: (".v" + dataSet.getVersion()))));
		if (!f.exists())
			return false;
		FileUtils.delete(f);
		try {
			IRepositoryObject obj = repository
					.getStaticObjectWithNameAndVersion(IDataSet.class,
							dataSet.getName() + ":" + dataSet.getVersion(),
							false);
			if (obj != null)
				obj.unregister();
		} catch (Exception e) {
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IBackendServer#deleteDataConfiguration(java.
	 * lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public boolean deleteDataConfiguration(String clientId,
			final ISerializableDataConfig dataConfig) throws RemoteException,
			IOException, OperationNotPermittedException, UnknownClientException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException {
		if (!getClientPermissions(clientId).canDelete())
			throw new OperationNotPermittedException();
		File f = new File(
				FileUtils
						.buildPath(repository.getBasePath(IDataConfig.class),
								dataConfig.getName()
										+ (dataConfig.getVersion().isEmpty()
												? ""
												: (".v" + dataConfig
														.getVersion()))
										+ ".dataconfig"));
		if (!f.exists())
			return false;
		FileUtils.delete(f);
		try {
			IRepositoryObject obj = repository
					.getStaticObjectWithNameAndVersion(IDataConfig.class,
							dataConfig.getName() + ":"
									+ dataConfig.getVersion(),
							false);
			if (obj != null)
				obj.unregister();
		} catch (Exception e) {
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IBackendServer#deleteDataSetConfiguration(java.
	 * lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public boolean deleteDataSetConfiguration(String clientId,
			final ISerializableDataSetConfig datasetConfig)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException {
		if (!getClientPermissions(clientId).canDelete())
			throw new OperationNotPermittedException();
		File f = new File(
				FileUtils
						.buildPath(repository.getBasePath(IDataSetConfig.class),
								datasetConfig.getName()
										+ (datasetConfig.getVersion().isEmpty()
												? ""
												: (".v" + datasetConfig
														.getVersion()))
										+ ".dsconfig"));
		if (!f.exists())
			return false;
		FileUtils.delete(f);
		try {
			IRepositoryObject obj = repository
					.getStaticObjectWithNameAndVersion(IDataSetConfig.class,
							datasetConfig.getName() + ":"
									+ datasetConfig.getVersion(),
							false);
			if (obj != null)
				obj.unregister();
		} catch (Exception e) {
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IBackendServer#deleteGoldStandardConfiguration(
	 * java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public boolean deleteGoldStandardConfiguration(String clientId,
			final ISerializableGoldStandardConfig goldstandardConfig)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException {
		if (!getClientPermissions(clientId).canDelete())
			throw new OperationNotPermittedException();
		File f = new File(FileUtils
				.buildPath(repository.getBasePath(IGoldStandardConfig.class),
						goldstandardConfig.getName()
								+ (goldstandardConfig.getVersion()
										.isEmpty()
												? ""
												: (".v" + goldstandardConfig
														.getVersion()))
								+ ".dsconfig"));
		if (!f.exists())
			return false;
		FileUtils.delete(f);
		try {
			IRepositoryObject obj = repository
					.getStaticObjectWithNameAndVersion(
							IGoldStandardConfig.class,
							goldstandardConfig.getName() + ":"
									+ goldstandardConfig.getVersion(),
							false);
			if (obj != null)
				obj.unregister();
		} catch (Exception e) {
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IBackendServer#deleteGoldStandard(java.lang.
	 * String, java.lang.String, java.lang.String)
	 */
	@Override
	public boolean deleteGoldStandard(String clientId,
			final ISerializableGoldStandard goldStandard)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException {
		if (!getClientPermissions(clientId).canDelete())
			throw new OperationNotPermittedException();
		File f = new File(FileUtils.buildPath(
				repository.getBasePath(IGoldStandard.class),
				goldStandard.getName() + (goldStandard.getVersion().isEmpty()
						? ""
						: (".v" + goldStandard.getVersion()))));
		if (!f.exists())
			return false;
		FileUtils.delete(f);
		try {
			IRepositoryObject obj = repository
					.getStaticObjectWithNameAndVersion(IGoldStandard.class,
							goldStandard.getName() + ":"
									+ goldStandard.getVersion(),
							false);
			if (obj != null)
				obj.unregister();
		} catch (Exception e) {
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IBackendServer#deleteProgram(java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public boolean deleteProgram(String clientId,
			final ISerializableProgram program) throws RemoteException,
			IOException, OperationNotPermittedException, UnknownClientException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException {
		if (!getClientPermissions(clientId).canDelete())
			throw new OperationNotPermittedException();
		File f;
		boolean isStandalone = program instanceof ISerializableStandaloneProgram;
		if (isStandalone)
			f = repository
					.getStaticObjectWithNameAndVersion(IStandaloneProgram.class,
							program.getName() + ":" + program.getVersion(),
							false)
					.getFile();
		else
			// TODO: does this work?
			f = new File(FileUtils.buildPath(
					repository.getBasePath(IRProgram.class),
					program.getName() + "-" + program.getVersion() + ".jar"));
		if (!f.exists())
			return false;
		FileUtils.delete(f);
		try {
			if (isStandalone) {
				IRepositoryObject obj = repository
						.getStaticObjectWithNameAndVersion(IDataSet.class,
								program.getName() + ":" + program.getVersion(),
								false);
				if (obj != null)
					obj.unregister();
			}
			// TODO: delete RProgram?
		} catch (Exception e) {
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IBackendServer#deleteProgramConfiguration(java.
	 * lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public boolean deleteProgramConfiguration(String clientId,
			final ISerializableProgramConfig programConfig)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException {
		if (!getClientPermissions(clientId).canDelete())
			throw new OperationNotPermittedException();
		File f = new File(
				FileUtils
						.buildPath(repository.getBasePath(IProgramConfig.class),
								programConfig.getName()
										+ (programConfig.getVersion().isEmpty()
												? ""
												: (".v" + programConfig
														.getVersion()))
										+ ".config"));
		if (!f.exists())
			return false;
		FileUtils.delete(f);
		try {
			IRepositoryObject obj = repository
					.getStaticObjectWithNameAndVersion(IProgramConfig.class,
							programConfig.getName() + ":"
									+ programConfig.getVersion(),
							false);
			if (obj != null)
				obj.unregister();
		} catch (Exception e) {
		}

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IBackendServer#deleteRun(java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public boolean deleteRun(String clientId, final ISerializableRun run)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException {
		if (!getClientPermissions(clientId).canDelete())
			throw new OperationNotPermittedException();
		File f = new File(
				FileUtils.buildPath(repository.getBasePath(IRun.class),
						run.getName() + (run.getVersion().isEmpty()
								? ""
								: (".v" + run.getVersion())) + ".run"));
		if (!f.exists())
			return false;
		FileUtils.delete(f);
		try {
			IRepositoryObject obj = repository
					.getStaticObjectWithNameAndVersion(IRun.class,
							run.getName() + ":" + run.getVersion(), false);
			if (obj != null)
				obj.unregister();
		} catch (Exception e) {
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IBackendServer#deleteRunResult(java.lang.
	 * String, java.lang.String, java.lang.String)
	 */
	@Override
	public boolean deleteRunResult(String clientId, String name)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, ObjectNotRegisteredException {
		if (!getClientPermissions(clientId).canDelete())
			throw new OperationNotPermittedException();
		File f = new File(FileUtils
				.buildPath(repository.getBasePath(IRunResult.class), name));
		if (!f.exists())
			return false;
		FileUtils.delete(f);
		try {
			IRepositoryObject obj = repository
					.getStaticObjectWithNameAndVersion(IRunResult.class, name,
							false);
			if (obj != null)
				obj.unregister();
		} catch (Exception e) {
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IBackendServer#getErrorObjects()
	 */
	@Override
	public Set<ISerializableWrapperRepositoryObject<? extends IRepositoryObject>> getErrorObjects(
			final String clientId, Class<? extends IRepositoryObject> clazz)
			throws RemoteException, ObjectNotFoundException,
			ObjectVersionNotFoundException,
			RepositoryObjectSerializationException {
		Map<ISerializableWrapperRepositoryObject<IRepositoryObject>, IRepositoryObject> map = this.repository
				.getErrorObjects().get(clazz);

		Set<ISerializableWrapperRepositoryObject<? extends IRepositoryObject>> result = new HashSet<>();
		for (ISerializableWrapperRepositoryObject<IRepositoryObject> wrapper : map
				.keySet())
			result.add(map.get(wrapper).asSerializable());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IBackendServer#isRelativeDataSetFormat(de.
	 * clusteval.data.dataset.format.ISerializableDataSetFormat)
	 */
	@Override
	public boolean isRelativeDataSetFormat(final String clientId,
			ISerializableDataSetFormat format)
			throws RemoteException, DeserializationException {
		return format.deserialize(repository) instanceof IRelativeDataSetFormat;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IBackendServer#getClustEvalServerVersion()
	 */
	@Override
	public String getClustEvalServerVersion(final String clientId)
			throws RemoteException {
		ClassVersion anno = IBackendServer.class
				.getAnnotation(ClassVersion.class);
		return anno != null ? anno.version() : null;
	}

	public RUN_TYPE getRunTypeOfRunResult(String clientId, String uniqueRunId)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException, UnknownRunTypeException,
			UnknownRunResultException {
		Set<ISerializableRunResult<? extends IRunResult>> results = new HashSet<>();

		for (IRepositoryObject r : this.repository
				.getCollectionStaticEntities(IRunResult.class))
			if (((IRunResult) r).getIdentifier().equals(uniqueRunId))
				return RUN_TYPE
						.getForClass(((IRunResult) r).getRun().getClass());
		throw new UnknownRunResultException(String.format(
				"A run result with ID %s could not be found.", uniqueRunId));
	}

	@Override
	public <T extends IRepositoryObject> ISerializableWrapperDynamicComponent uploadDynamicComponent(
			final String clientId, Class<T> clazz, final String jarFileName,
			byte[] jarAsBytes) throws OperationNotPermittedException,
			IOException, UnknownClientException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException, RepositoryObjectParseException,
			RegisterException {
		if (!getClientPermissions(clientId).canUpload())
			throw new OperationNotPermittedException();

		String[] split = jarFileName.replaceAll(".jar$", "").split("-");

		final String filePath = FileUtils
				.buildPath(repository.getBasePath(clazz), jarFileName);
		new File(filePath).getParentFile().mkdirs();

		Files.write(Paths.get(filePath), jarAsBytes);

		IFinder<? extends RepositoryObject> finder = (IFinder<? extends RepositoryObject>) repository
				.getStaticObjectWithNameAndVersion(IFinder.class,
						String.format("Finder: %s", clazz.getSimpleName()));
		finder.doOnFileFound(new File(filePath));

		String className = split[0];
		ComparableVersion version = new ComparableVersion(split[1]);

		Class<? extends T> registeredClass = repository.getRegisteredClass(
				clazz, clazz.getPackage().getName() + "." + className, version);

		return new SerializableWrapperDynamicComponent(clazz,
				new File(filePath), className, split[1],
				Repository.getAliasOfDynamicClass(registeredClass));
	}

	/**
	 * 
	 * @return The newer version if available, null otherwise.
	 */
	@Override
	public String checkForServerUpdate(final String clientId,
			boolean includeSnapshotVersions) throws RemoteException {
		// TODO: for now every client is allowed to check for an update
		ComparableVersion v = this.repository.getMavenResolver()
				.checkForNewerVersionClustEvalServer(includeSnapshotVersions);

		if (v == null)
			return null;
		return v.toString();
	}

	/**
	 * 
	 * @return The newer version if available, null otherwise.
	 */
	@Override
	public String checkForClientUpdate(final String clientId,
			final String clientVersion, boolean includeSnapshotVersions)
			throws RemoteException {
		// TODO: for now every client is allowed to check for an update
		ComparableVersion v = this.repository.getMavenResolver()
				.checkForNewerVersionClustEvalClient(
						new ComparableVersion(clientVersion),
						includeSnapshotVersions);
		if (v == null)
			return null;
		return v.toString();
	}

	@Override
	public Map<String, Map<String, String>> checkForComponentUpdates(
			final String clientId, boolean includeSnapshotVersions)
			throws RemoteException, InvalidDynamicComponentNameException {
		// TODO: for now every client is allowed to check for an update
		Map<String, String> availableUpdates = new HashMap<>();
		Map<String, Set<ComparableVersion>> registeredClassesNamesAndVersions = new HashMap<>();

		for (Map.Entry<String, Map<ComparableVersion, Class<? extends IRepositoryObjectDynamicComponent>>> e : this.repository
				.getAllRegisteredClasses().entrySet()) {
			registeredClassesNamesAndVersions.put(e.getKey(),
					e.getValue().keySet());
		}

		return this.repository.getMavenResolver()
				.checkForNewerVersionsComponents(clientId,
						registeredClassesNamesAndVersions,
						includeSnapshotVersions);
	}

	@Override
	public <T extends IRepositoryObject> String getReleaseNotes(
			final String clientId, Class<T> clazz, final String name,
			final String version)
			throws ArtifactResolutionException, RemoteException {
		// TODO: for now every client is allowed
		return this.repository.getMavenResolver()
				.getReleaseNotes(clazz.getPackage().getName(), name, version);
	}

	@Override
	public <T extends IRepositoryObject> String getReleaseTitle(
			final String clientId, Class<T> clazz, final String name,
			final String version)
			throws ArtifactResolutionException, RemoteException {
		// TODO: for now every client is allowed
		return this.repository.getMavenResolver()
				.getReleaseTitle(clazz.getPackage().getName(), name, version);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IBackendServer#
	 * ensureRuntimeInformationForAllProgramConfigs()
	 */
	@Override
	public void ensureRuntimeInformationForAllProgramConfigs()
			throws UnknownContextException,
			UnknownClusteringQualityMeasureException,
			UnknownDataSetFormatException, UnknownDataSetTypeException,
			UnknownDistanceMeasureException, RepositoryObjectDumpException,
			DynamicComponentInitializationException, RegisterException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException,
			IOException, URISyntaxException, InterruptedException,
			RemoteException {
		this.repository.ensureRuntimeInformationForAllProgramConfigs();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IBackendServer#getExpectedRuntime(de.clusteval.
	 * program.IProgramConfig, int)
	 */
	@Override
	public long getExpectedRuntime(ISerializableProgramConfig<?> programConfig,
			int numberSamplesInDataset, int numberClusterings)
			throws RemoteException, DeserializationException {
		int numberCores = Math.min(Runtime.getRuntime().availableProcessors(),
				this.repository.getSupervisorThread().getRunScheduler()
						.getThreadPoolSize());
		return this.getExpectedRuntime(programConfig, numberSamplesInDataset,
				numberClusterings, numberCores);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IBackendServer#getExpectedRuntime(de.clusteval.
	 * program.IProgramConfig, int, int, int)
	 */
	@Override
	public long getExpectedRuntime(ISerializableProgramConfig<?> programConfig,
			int numberSamplesInDataset, int numberClusterings, int numberCores)
			throws RemoteException, DeserializationException {
		long msPerClustering = this.repository.getExpectedRuntimePerClustering(
				programConfig.deserialize(repository), numberSamplesInDataset);
		if (msPerClustering < 0)
			return -1;
		return (long) ((double) msPerClustering * numberClusterings
				/ numberCores);
	}
}
