/**
 * 
 */
package de.clusteval.framework.threading;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import de.clusteval.run.runnable.IIterationRunnable;

public class PriorityThreadPoolExecutor extends ThreadPoolExecutor {

	/**
	 * @param corePoolSize
	 * @param maximumPoolSize
	 * @param keepAliveTime
	 * @param unit
	 * @param workQueue
	 */
	public PriorityThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit,
			BlockingQueue<Runnable> workQueue) {
		super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
	}

	@Override
	protected <T extends Object> java.util.concurrent.RunnableFuture<T> newTaskFor(Runnable runnable, T value) {
		RunnableFuture<T> newTaskFor = super.newTaskFor(runnable, value);
		// calculate a priority based on how long ago the run has been started
		return new PriorityFuture<T>(runnable, newTaskFor, ((IIterationRunnable) runnable).getRun().getStartTime());
	};
}