/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.framework.threading;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.clusteval.cluster.paramOptimization.IParameterOptimizationMethodFinderThread;
import de.clusteval.cluster.paramOptimization.ParameterOptimizationMethodFinderThread;
import de.clusteval.cluster.quality.ClusteringQualityMeasureFinderThread;
import de.clusteval.cluster.quality.IClusteringQualityMeasureFinderThread;
import de.clusteval.data.dataset.DataSetConfigFinderThread;
import de.clusteval.data.dataset.IDataSetConfigFinderThread;
import de.clusteval.data.dataset.format.DataSetFormatFinderThread;
import de.clusteval.data.dataset.format.IDataSetFormatFinderThread;
import de.clusteval.data.statistics.DataStatisticFinderThread;
import de.clusteval.data.statistics.IDataStatisticFinderThread;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.run.IRunFinderThread;
import de.clusteval.run.RunFinderThread;
import de.clusteval.run.runresult.format.IRunResultFormatFinderThread;
import de.clusteval.run.runresult.format.RunResultFormatFinderThread;
import de.clusteval.run.statistics.IRunStatisticFinderThread;
import de.clusteval.run.statistics.RunStatisticFinderThread;

/**
 * This supervisor thread is responsible for starting and keeping alive all
 * other threads of the framework. He continuously checks whether the other
 * threads are still alive as they should. In case any of those threads
 * terminates in an uncontrolled way, the supervisor thread restarts a new
 * thread of the corresponding class of threads that was terminated
 * unexpectedly.
 * 
 * @author Christian Wiwie
 * 
 */
public abstract class SupervisorThread extends Thread
		implements
			ISupervisorThread {

	protected boolean interrupted;

	/**
	 * @param classes
	 *            The clusteval thread classes to add to a list.
	 * @return A list of clusteval thread classes.
	 */
	public static List<Class<? extends ClustevalThread>> createList(
			Class<? extends ClustevalThread>... classes) {
		List<Class<? extends ClustevalThread>> result = new ArrayList<Class<? extends ClustevalThread>>();
		for (Class<? extends ClustevalThread> clazz : classes)
			result.add(clazz);
		return result;
	}

	/**
	 * The repository this supervisor belongs to and for which all threads
	 * should be supervised.
	 */
	protected IRepository repository;

	/**
	 * A map containing all threads that were started by this supervisor
	 * together with their thread class.
	 * 
	 * <p>
	 * This map is used by the supervisor to iterate through the existing
	 * threads and checking whether they are still alive. If the supervisor
	 * thread finds a thread in this map that terminated unexpectedly, he
	 * creates a new thread of this class.
	 */
	final protected Map<Class<? extends ClustevalThread>, ClustevalThread> threads;

	/**
	 * The time between two checks of this thread.
	 */
	protected long supervisorSleepTime;

	/**
	 * A boolean indicating, whether this thread only should check once and then
	 * terminate. This can be useful in some subclasses of this class (e.g.
	 * {@link RunResultRepositorySupervisorThread}).
	 */
	protected boolean checkOnce;

	protected Logger log;

	/**
	 * Constructor of abstract supervisor threads.
	 * 
	 * @param repository
	 *            The repository this supervisor belongs to and for which all
	 *            threads should be supervised.
	 * @param threads
	 *            A list containing all threads this supervisor thread should
	 *            start and keep alive.
	 * @param threadSleepTimes
	 *            A map containing sleep times for threads (must not be
	 *            complete).
	 * @param checkOnce
	 *            A boolean indicating, whether this thread only should check
	 *            once and then terminate. This can be useful in some subclasses
	 *            of this class (e.g.
	 *            {@link RunResultRepositorySupervisorThread}).
	 */
	public SupervisorThread(final IRepository repository,
			final List<Class<? extends ClustevalThread>> threads,
			final Map<String, Long> threadSleepTimes, final boolean checkOnce) {
		super();
		this.setName(this.getName().replace("Thread", "Supervisor"));
		this.log = LoggerFactory.getLogger(this.getClass());

		this.repository = repository;
		this.supervisorSleepTime = threadSleepTimes.containsKey(
				"SupervisorThread") ? threadSleepTimes.get("SupervisorThread") :
		// if not specified, check every 5 seconds
						5000;
		this.checkOnce = checkOnce;

		this.threads = new LinkedHashMap<Class<? extends ClustevalThread>, ClustevalThread>();

		synchronized (this.threads) {
			for (Class<? extends ClustevalThread> thread : threads) {
				try {
					this.threads.put(thread, (threadSleepTimes
							.containsKey(thread.getSimpleName())) ?
					// if we have a specific sleep time for this thread
					// use
					// it
									thread.getConstructor(
											ISupervisorThread.class,
											IRepository.class, long.class,
											boolean.class).newInstance(this,
													repository,
													threadSleepTimes.get(thread
															.getSimpleName()),
													checkOnce)
									:
									// otherwise we use the default sleep time
									// of
									// the thread class
									thread.getConstructor(
											ISupervisorThread.class,
											IRepository.class, boolean.class)
											.newInstance(this, repository,
													checkOnce));
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (SecurityException e) {
					e.printStackTrace();
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Thread#run()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.threading.ISupervisorThread#run()
	 */
	@Override
	public void run() {
		// check, that all threads are running
		while (!this.isInterrupted()) {
			try {
				synchronized (this.threads) {
					// changed at 08.05.2012
					for (Class<? extends ClustevalThread> threadClass : this.threads
							.keySet()) {
						if (this.threads.get(threadClass) == null
								|| (this.threads.get(threadClass).getState()
										.equals(State.TERMINATED)
										&& !checkOnce)) {
							this.log.warn("Restarting "
									+ threadClass.getSimpleName());
							Constructor<? extends ClustevalThread> constr;
							try {
								if (IRunSchedulerThread.class
										.isAssignableFrom(threadClass)) {
									constr = threadClass.getConstructor(
											ISupervisorThread.class,
											IRepository.class);
									this.threads.put(threadClass, constr
											.newInstance(this, repository));
								} else {
									constr = threadClass.getConstructor(
											ISupervisorThread.class,
											IRepository.class, boolean.class);
									this.threads.put(threadClass,
											constr.newInstance(this, repository,
													false));
								}
							} catch (NoSuchMethodException e) {
								e.printStackTrace();
							} catch (SecurityException e) {
								e.printStackTrace();
							} catch (InstantiationException e) {
								e.printStackTrace();
							} catch (IllegalAccessException e) {
								e.printStackTrace();
							} catch (IllegalArgumentException e) {
								e.printStackTrace();
							} catch (InvocationTargetException e) {
								e.printStackTrace();
							}
						}
					}
				}
				if (checkOnce && this.repository.isInitialized())
					return;
				sleep(this.supervisorSleepTime);
			} catch (InterruptedException e) {
				this.interrupt();
			}
			// added 12.12.2012
			catch (ConcurrentModificationException e) {
				e.printStackTrace();
				// do nothing
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Thread#interrupt()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.threading.ISupervisorThread#interrupt()
	 */
	@Override
	// TODO: fixme: not all threads (iteration/runrunnable threads?!) are
	// terminated
	public void interrupt() {
		this.interrupted = true;
		synchronized (this.threads) {
			Map.Entry<Class<? extends ClustevalThread>, ClustevalThread>[] entries = this.threads
					.entrySet().toArray(new Map.Entry[0]);
			for (Map.Entry<Class<? extends ClustevalThread>, ClustevalThread> entry : entries) {
				this.threads.remove(entry.getKey());
				entry.getValue().interrupt();
				try {
					entry.getValue().join(0);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		super.interrupt();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.threading.ISupervisorThread#getDataSetFormatThread
	 * ()
	 */
	@Override
	public IDataSetFormatFinderThread getDataSetFormatThread() {
		return (IDataSetFormatFinderThread) this.threads
				.get(DataSetFormatFinderThread.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.threading.ISupervisorThread#getDataSetConfigThread
	 * ()
	 */
	@Override
	public IDataSetConfigFinderThread getDataSetConfigThread() {
		return (IDataSetConfigFinderThread) this.threads
				.get(DataSetConfigFinderThread.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.threading.ISupervisorThread#
	 * getRunResultFormatThread()
	 */
	@Override
	public IRunResultFormatFinderThread getRunResultFormatThread() {
		return (IRunResultFormatFinderThread) this.threads
				.get(RunResultFormatFinderThread.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.threading.ISupervisorThread#getRunFinderThread()
	 */
	@Override
	public IRunFinderThread getRunFinderThread() {
		return (IRunFinderThread) this.threads.get(RunFinderThread.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.threading.ISupervisorThread#
	 * getDataStatisticFinderThread()
	 */
	@Override
	public IDataStatisticFinderThread getDataStatisticFinderThread() {
		return (IDataStatisticFinderThread) this.threads
				.get(DataStatisticFinderThread.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.threading.ISupervisorThread#
	 * getRunStatisticFinderThread()
	 */
	@Override
	public IRunStatisticFinderThread getRunStatisticFinderThread() {
		return (IRunStatisticFinderThread) this.threads
				.get(RunStatisticFinderThread.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.threading.ISupervisorThread#
	 * getClusteringQualityMeasureFinderThread()
	 */
	@Override
	public IClusteringQualityMeasureFinderThread getClusteringQualityMeasureFinderThread() {
		return (IClusteringQualityMeasureFinderThread) this.threads
				.get(ClusteringQualityMeasureFinderThread.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.threading.ISupervisorThread#getRunScheduler()
	 */
	@Override
	public IRunSchedulerThread getRunScheduler() {
		return (IRunSchedulerThread) this.threads.get(RunSchedulerThread.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.threading.ISupervisorThread#
	 * getParameterOptimizationMethodFinderThread()
	 */
	@Override
	public IParameterOptimizationMethodFinderThread getParameterOptimizationMethodFinderThread() {
		return (IParameterOptimizationMethodFinderThread) this.threads
				.get(ParameterOptimizationMethodFinderThread.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.threading.ISupervisorThread#getThread(java.lang.
	 * Class)
	 */
	@Override
	public <T extends IClustevalThread> T getThread(Class<T> clazz) {
		return (T) this.threads.get(clazz);
	}
}
