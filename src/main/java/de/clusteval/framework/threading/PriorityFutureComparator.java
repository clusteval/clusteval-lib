/**
 * 
 */
package de.clusteval.framework.threading;

import java.util.Comparator;

import de.clusteval.run.runnable.ClusteringIterationRunnable;
import de.clusteval.run.runnable.IIterationRunnable;

public class PriorityFutureComparator implements Comparator<Runnable> {

	public int compare(Runnable o1, Runnable o2) {
		if (o1 == null && o2 == null)
			return 0;
		else if (o1 == null)
			return -1;
		else if (o2 == null)
			return 1;
		else {
			// take the future first, which has been started earlier (smaller
			// start time)
			PriorityFuture<?> f1 = (PriorityFuture<?>) o1, f2 = (PriorityFuture<?>) o2;
			long p1 = f1.getPriority();
			long p2 = f2.getPriority();

			int iterationNumber1 = -1, iterationNumber2 = -1;
			if (f1.getRunnable() instanceof ClusteringIterationRunnable) {
				iterationNumber1 = ((ClusteringIterationRunnable) f1.getRunnable()).getIterationNumber();
			}

			if (f2.getRunnable() instanceof ClusteringIterationRunnable) {
				iterationNumber2 = ((ClusteringIterationRunnable) f2.getRunnable()).getIterationNumber();
			}

			long runRunnableStartTime1 = ((IIterationRunnable) f1.getRunnable()).getParentRunnable().getStartTime(),
					runRunnableStartTime2 = ((IIterationRunnable) f2.getRunnable()).getParentRunnable().getStartTime();

			if (p1 > p2)
				return 1;
			else if (p1 < p2)
				return -1;

			if (runRunnableStartTime1 > runRunnableStartTime2)
				return 1;
			else if (runRunnableStartTime1 < runRunnableStartTime2)
				return -1;

			return Integer.compare(iterationNumber1, iterationNumber2);
		}
	}
}