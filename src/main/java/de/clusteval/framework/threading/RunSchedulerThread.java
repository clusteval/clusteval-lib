/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.framework.threading;

import static java.util.concurrent.TimeUnit.NANOSECONDS;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Future;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.clusteval.framework.AbstractClustevalServer;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.run.IRun;
import de.clusteval.run.MissingParameterValueException;
import de.clusteval.run.RUN_STATUS;
import de.clusteval.run.Run;
import de.clusteval.run.RunInitializationException;
import de.clusteval.run.runnable.DataAnalysisRunRunnable;
import de.clusteval.run.runnable.ExecutionRunRunnable;
import de.clusteval.run.runnable.IIterationRunnable;
import de.clusteval.run.runnable.IIterationWrapper;
import de.clusteval.run.runnable.IRunRunnable;
import de.clusteval.run.runnable.RunAnalysisRunRunnable;
import de.clusteval.run.runnable.RunRunnableInitializationException;
import de.clusteval.run.runresult.IRunResult;
import de.clusteval.run.runresult.NoRunResultFormatParserException;
import de.clusteval.run.runresult.ParameterOptimizationResult;
import dk.sdu.imada.compbio.file.FileUtils;
import dk.sdu.imada.compbio.format.Formatter;
import dk.sdu.imada.compbio.utils.Pair;
import dk.sdu.imada.compbio.utils.Triple;

/**
 * Threads of this class are responsible for scheduling, creating, starting and
 * terminating runs.
 * 
 * <p>
 * The methods {@link #schedule(String, String)},
 * {@link #scheduleResume(String, String)} and
 * {@link #terminate(String, String)} of this thread are invoked by the backend
 * server {@link AbstractClustevalServer} which in turn gets its commands from a
 * client.
 * 
 * @author Christian Wiwie
 * 
 */
public class RunSchedulerThread extends ClustevalThread
		implements
			IRunSchedulerThread {

	public static int NUMBER_THREADS = Math
			.max((int) (Runtime.getRuntime().availableProcessors() / 2.0), 1);

	/**
	 * A queue containing all the runs that were scheduled, but not yet
	 * executed. Every entry of the queue contains (clientId,runId,isResume).
	 */
	protected Queue<Triple<String, String, Boolean>> runQueue;

	/**
	 * A map containing all the runs that are executed right now. The map maps
	 * from client id to collections of runs.
	 */
	protected Map<String, Collection<IRun<?>>> clientToRuns;
	/**
	 * A map containing all the run resumes that are executed right now. The map
	 * maps from client id to collections of runs.
	 */
	protected Map<String, Collection<IRun<?>>> clientToRunResumes;

	/**
	 * The repository this run scheduler belongs to. This scheduler can only
	 * control runs that are contained in this repository.
	 */
	protected IRepository repository;

	protected Logger log;

	/**
	 * A thread pool containing all threads that were started to execute runs.
	 * This data structure allows convenient control over the number of threads
	 * to be started in parallel.
	 */
	protected ScheduledThreadPoolExecutor threadPool;

	/**
	 * A thread pool containing all threads that were started to execute
	 * iteration runnables. Iteration runnables are started by certain types of
	 * run runnables (e.g. ParameterOptimizationRunRunnables)
	 */
	protected Map<IRepository, ThreadPoolExecutor> iterationThreadPool;

	protected Map<Thread, IIterationRunnable<? extends IIterationWrapper, ?>> activeIterationRunnables;

	/**
	 * Constructor of run scheduler threads.
	 * 
	 * @param supervisorThread
	 * 
	 * @param repository
	 *            The repository this run scheduler belongs to. This scheduler
	 *            can only control runs that are contained in this repository.
	 */
	public RunSchedulerThread(final ISupervisorThread supervisorThread,
			final IRepository repository) {
		super(supervisorThread);
		this.setName(this.getName().replace("Thread", "RunScheduler"));
		this.runQueue = new ConcurrentLinkedQueue<Triple<String, String, Boolean>>();
		this.clientToRuns = new HashMap<String, Collection<IRun<?>>>();
		this.clientToRunResumes = new HashMap<String, Collection<IRun<?>>>();
		this.repository = repository;
		this.log = LoggerFactory.getLogger(this.getClass());
		this.threadPool = new ScheduledThreadPoolExecutor(5);
		this.threadPool.setMaximumPoolSize(this.threadPool.getCorePoolSize());
		// threads stored in the threadPool variable correspond to started
		// runrunnables. Some types of those threads start a thread for each
		// iteration they perform, which are then stored in the
		// iterationThreadPool variable. As soon as those threads are started,
		// the "mainthread" only waits until those threads are finished.
		// Therefore we can assume that at each time point we have roughly
		// numberThreads active threads.
		this.iterationThreadPool = new HashMap<IRepository, ThreadPoolExecutor>();
		this.iterationThreadPool.put(this.repository,
				new PriorityThreadPoolExecutor(NUMBER_THREADS,
						Integer.MAX_VALUE, 0, NANOSECONDS,
						new PriorityBlockingQueue(11,
								new PriorityFutureComparator())));
		// this.iterationThreadPool.setMaximumPoolSize(this.iterationThreadPool.getCorePoolSize());
		this.activeIterationRunnables = new HashMap<Thread, IIterationRunnable<? extends IIterationWrapper, ?>>();
		this.start();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.threading.IRunSchedulerThread#
	 * getOptimizationRunStatusForClientId(java.lang.String)
	 */
	// TODO
	@Override
	public Map<String, Pair<Pair<RUN_STATUS, Float>, Map<Pair<String, String>, Pair<Double, Map<String, Pair<Map<String, String>, String>>>>>> getOptimizationRunStatusForClientId(
			String clientId) {

		synchronized (this.runQueue) {
			Map<String, Pair<Pair<RUN_STATUS, Float>, Map<Pair<String, String>, Pair<Double, Map<String, Pair<Map<String, String>, String>>>>>> result = new HashMap<String, Pair<Pair<RUN_STATUS, Float>, Map<Pair<String, String>, Pair<Double, Map<String, Pair<Map<String, String>, String>>>>>>();

			// add the scheduled runs
			for (Triple<String, String, Boolean> runTriple : this.runQueue) {
				// check for correct client id
				if (runTriple.getFirst().equals(clientId)) {
					result.put(runTriple.getSecond(), Pair.getPair(
							Pair.getPair(RUN_STATUS.SCHEDULED, 100f),
							(Map<Pair<String, String>, Pair<Double, Map<String, Pair<Map<String, String>, String>>>>) new HashMap<Pair<String, String>, Pair<Double, Map<String, Pair<Map<String, String>, String>>>>()));
				}
			}

			Collection<IRun> toRemove = new HashSet<IRun>();

			// add the running runs
			if (this.clientToRuns.containsKey(clientId)) {
				for (IRun run : this.clientToRuns.get(clientId)) {
					result.put(run.getRunIdentificationString(),
							Pair.getPair(
									Pair.getPair(run.getStatus(),
											run.getPercentFinished()),
									run.getOptimizationStatus()));
					if (result.get(run.getRunIdentificationString()).getFirst()
							.equals(RUN_STATUS.FINISHED))
						toRemove.add(run);
				}
			}

			for (IRun run : toRemove)
				this.clientToRuns.get(clientId).remove(run);

			toRemove = new HashSet<IRun>();

			// add the running run resumes
			if (this.clientToRunResumes.containsKey(clientId)) {
				for (IRun run : this.clientToRunResumes.get(clientId)) {
					result.put(run.getRunIdentificationString(),
							Pair.getPair(
									Pair.getPair(run.getStatus(),
											run.getPercentFinished()),
									run.getOptimizationStatus()));
					if (result.get(run.getRunIdentificationString()).getFirst()
							.equals(RUN_STATUS.FINISHED))
						toRemove.add(run);
				}
			}

			for (IRun run : toRemove)
				this.clientToRunResumes.get(clientId).remove(run);

			return result;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.threading.IRunSchedulerThread#schedule(java.lang.
	 * String, java.lang.String)
	 */
	@Override
	public String schedule(final String clientId, String runId)
			throws ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException {
		final Triple<String, String, Boolean> newTriple = Triple
				.getTriple(clientId, runId, false);
		/*
		 * Check if this run exists
		 */
		IRun origRun = (IRun) this.repository
				.getStaticObjectWithNameAndVersion(IRun.class, runId);
		if (origRun == null) {
			this.log.warn("A job with id " + runId + " does not exist");
			return null;
		} else
			// make sure that we have the versioned run ID
			runId = origRun.toString();

		/*
		 * Job has already been scheduled
		 */
		if (this.runQueue.contains(newTriple)) {
			this.log.warn("The job " + runId + " has already been scheduled");
			return null;
		}
		for (String client : this.clientToRuns.keySet()) {
			boolean found = false;
			for (IRun run : this.clientToRuns.get(client))
				if (run.getName().equals(runId)) {
					if (run.getStatus().equals(RUN_STATUS.RUNNING)
							|| run.getStatus().equals(RUN_STATUS.SCHEDULED)) {
						found = true;
					}
					break;
				}
			if (found) {
				this.log.warn("The job " + runId + " is already running");
				return null;
			}
		}

		final String uniqueIdString = Formatter.currentTimeAsString(true,
				"yyyy_MM_dd-HH_mm_ss", Locale.UK) + "_"
				+ origRun.toString(".v");
		((IRun) this.repository.getStaticObjectWithNameAndVersion(IRun.class,
				runId)).setRunIdentificationString(uniqueIdString);
		((IRun) this.repository.getStaticObjectWithNameAndVersion(IRun.class,
				runId)).setStatus(RUN_STATUS.SCHEDULED);

		this.log.info("Run scheduled..." + runId);
		this.runQueue.add(newTriple);
		return uniqueIdString;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.threading.IRunSchedulerThread#scheduleResume(java.
	 * lang.String, java.lang.String)
	 */
	@Override
	public boolean scheduleResume(String clientId,
			String uniqueRunResultIdentifier) {
		final Triple<String, String, Boolean> newTriple = Triple
				.getTriple(clientId, uniqueRunResultIdentifier, true);
		if (!new File(FileUtils.buildPath(
				this.repository.getBasePath(IRunResult.class),
				uniqueRunResultIdentifier)).exists()) {
			this.log.warn("No run results were found under " + FileUtils
					.buildPath(this.repository.getBasePath(IRunResult.class),
							uniqueRunResultIdentifier));
			return false;
		}

		/*
		 * Job has already been scheduled
		 */
		if (this.runQueue.contains(newTriple)) {
			this.log.warn("The job " + uniqueRunResultIdentifier
					+ " has already been scheduled");
			return false;
		}
		for (String client : this.clientToRunResumes.keySet()) {
			boolean found = false;
			for (IRun run : this.clientToRunResumes.get(client))
				if (run.getRunIdentificationString()
						.equals(uniqueRunResultIdentifier)) {
					if (run.getStatus().equals(RUN_STATUS.RUNNING)
							|| run.getStatus().equals(RUN_STATUS.SCHEDULED)) {
						found = true;
					}
					break;
				}
			if (found) {
				this.log.warn("The job " + uniqueRunResultIdentifier
						+ " is already running");
				return false;
			}
		}

		this.log.info("Run resume scheduled..." + uniqueRunResultIdentifier);
		return this.runQueue.add(newTriple);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.threading.IRunSchedulerThread#terminate(java.lang.
	 * String, java.lang.String)
	 */
	@Override
	public boolean terminate(final String clientId, final String runId) {
		// assume the run is a run resume
		boolean result = terminate(clientId, runId, true);
		// otherwise it is assumed to be a run
		if (!result)
			result = terminate(clientId, runId, false);
		return result;
	}

	protected boolean terminate(final String clientId, final String runId,
			final boolean isResume) {
		final Triple<String, String, Boolean> triple = Triple
				.getTriple(clientId, runId, isResume);
		/*
		 * Job is in the queue, has not been executed
		 */
		if (this.runQueue.contains(triple)) {
			this.log.info(
					"Job " + runId + " has been removed from the runs queue.");
			return this.runQueue.remove(triple);
		}

		/*
		 * Run is being executed
		 */
		if (this.clientToRuns.containsKey(clientId)) {
			for (IRun run : this.clientToRuns.get(clientId))
				if (run.getRunIdentificationString().equals(runId)) {
					if (run.terminate()) {
						// return this.clientToRuns.get(clientId).remove(run);
					}
				}
		}
		/*
		 * Run resume is being executed
		 */
		if (this.clientToRunResumes.containsKey(clientId)) {
			for (IRun run : this.clientToRunResumes.get(clientId))
				if (run.getRunIdentificationString().equals(runId)) {
					if (run.terminate()) {
						// return
						// this.clientToRunResumes.get(clientId).remove(run);
					}
				}
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.threading.IRunSchedulerThread#getQueue()
	 */
	@Override
	public Queue<String> getQueue(final String clientId) {
		final Queue<String> result = new ConcurrentLinkedQueue<String>();

		if (this.clientToRuns.containsKey(clientId))
			for (IRun run : this.clientToRuns.get(clientId))
				if (run.getStatus().equals(RUN_STATUS.SCHEDULED))
					result.add(run.getRunIdentificationString());

		if (this.clientToRunResumes.containsKey(clientId))
			for (IRun run : this.clientToRunResumes.get(clientId))
				if (run.getStatus().equals(RUN_STATUS.SCHEDULED))
					result.add(run.getRunIdentificationString());

		for (Triple<String, String, Boolean> triple : runQueue)
			result.add(triple.getSecond());

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.threading.IRunSchedulerThread#getFinishedRuns()
	 */
	@Override
	public Collection<String> getFinishedRuns(final String clientId) {
		final Collection<String> result = new HashSet<String>();

		if (this.clientToRuns.containsKey(clientId))
			for (IRun run : this.clientToRuns.get(clientId))
				if (run.getStatus().equals(RUN_STATUS.FINISHED))
					result.add(run.getRunIdentificationString());

		if (this.clientToRunResumes.containsKey(clientId))
			for (IRun run : this.clientToRunResumes.get(clientId))
				if (run.getStatus().equals(RUN_STATUS.FINISHED))
					result.add(run.getRunIdentificationString());

		for (Triple<String, String, Boolean> triple : runQueue)
			if (triple.getFirst().equals(clientId))
				result.add(triple.getSecond());

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.threading.IRunSchedulerThread#getRunningRuns()
	 */
	@Override
	public Collection<String> getRunningRuns(final String clientId) {
		final Collection<String> result = new HashSet<String>();

		if (this.clientToRuns.containsKey(clientId))
			for (IRun run : this.clientToRuns.get(clientId))
				if (run.getStatus().equals(RUN_STATUS.RUNNING))
					result.add(run.getRunIdentificationString());

		if (this.clientToRunResumes.containsKey(clientId))
			for (IRun run : this.clientToRunResumes.get(clientId))
				if (run.getStatus().equals(RUN_STATUS.RUNNING))
					result.add(run.getRunIdentificationString());

		for (Triple<String, String, Boolean> triple : runQueue)
			result.add(triple.getSecond());

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.threading.IRunSchedulerThread#getTerminatedRuns()
	 */
	@Override
	public Collection<String> getTerminatedRuns(final String clientId) {
		final Collection<String> result = new HashSet<String>();

		if (this.clientToRuns.containsKey(clientId))
			for (IRun run : this.clientToRuns.get(clientId))
				if (run.getStatus().equals(RUN_STATUS.TERMINATED))
					result.add(run.getRunIdentificationString());

		if (this.clientToRunResumes.containsKey(clientId))
			for (IRun run : this.clientToRunResumes.get(clientId))
				if (run.getStatus().equals(RUN_STATUS.TERMINATED))
					result.add(run.getRunIdentificationString());

		for (Triple<String, String, Boolean> triple : runQueue)
			result.add(triple.getSecond());

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.threading.IRunSchedulerThread#forgetRunStatus(java
	 * .lang.String, java.lang.String)
	 */
	@Override
	public void forgetRunStatus(String clientId, String uniqueRunId) {
		if (this.clientToRuns.containsKey(clientId))
			for (IRun run : this.clientToRuns.get(clientId))
				if (run.getRunIdentificationString().equals(uniqueRunId))
					this.clientToRuns.get(clientId).remove(run);

		if (this.clientToRunResumes.containsKey(clientId))
			for (IRun run : this.clientToRunResumes.get(clientId))
				if (run.getRunIdentificationString().equals(uniqueRunId))
					this.clientToRunResumes.get(clientId).remove(run);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.threading.IRunSchedulerThread#getRuns()
	 */
	@Override
	public Set<IRun<?>> getRuns() {
		final Set<IRun<?>> result = new HashSet<IRun<?>>();
		for (Collection<IRun<?>> coll : this.clientToRunResumes.values())
			result.addAll(coll);
		for (Collection<IRun<?>> coll : this.clientToRuns.values())
			result.addAll(coll);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.threading.IRunSchedulerThread#
	 * getRunStatusForClientId(java.lang.String)
	 */
	@Override
	public Map<String, Pair<RUN_STATUS, Float>> getRunStatusForClientId(
			String clientId) {
		Map<String, Pair<RUN_STATUS, Float>> result = new HashMap<String, Pair<RUN_STATUS, Float>>();

		// iterate over all scheduled runs and run resumes
		for (Triple<String, String, Boolean> triple : this.runQueue) {
			if (triple.getFirst().equals(clientId))
				result.put(triple.getSecond(),
						Pair.getPair(RUN_STATUS.SCHEDULED, 100f));
		}

		// iterate over all executing runs
		if (this.clientToRuns.containsKey(clientId)) {
			for (IRun run : this.clientToRuns.get(clientId)) {
				result.put(run.getRunIdentificationString(), Pair
						.getPair(run.getStatus(), run.getPercentFinished()));
			}
		}

		/*
		 * iterate over all executing run resumes
		 */
		if (this.clientToRunResumes.containsKey(clientId)) {
			for (IRun run : this.clientToRunResumes.get(clientId)) {
				result.put(run.getRunIdentificationString(), Pair
						.getPair(run.getStatus(), run.getPercentFinished()));
			}
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.threading.IRunSchedulerThread#
	 * getEstimatedRemainingRuntimeForClientId(java.lang.String)
	 */
	@Override
	public Map<String, Long> getEstimatedRemainingRuntimeForClientId(
			String clientId) {
		Map<String, Long> result = new HashMap<String, Long>();

		int numberThreads = 0;
		for (ThreadPoolExecutor e : iterationThreadPool.values())
			numberThreads += e.getCorePoolSize();

		// iterate over all executing runs
		if (this.clientToRuns.containsKey(clientId)) {
			for (IRun run : this.clientToRuns.get(clientId)) {
				// long startTimeMillis = System.currentTimeMillis() -
				// run.getStartTime();
				// float percentFinished =
				// run.getPercentFinishedDuringCurrentExecution();
				// float totalPercentFinished = run.getPercentFinished();
				// // / 1000: convert to seconds
				// // * 100: scale up to 100 percent
				// long secondsFor100Percent = (long) (startTimeMillis /
				// percentFinished / 1000
				// * (100 - totalPercentFinished));
				long estimatedRunningTimeInSeconds = (long) ((double) run
						.getEstimatedRemainingCPUTimeInMs() / numberThreads
						/ 1000);
				result.put(run.getRunIdentificationString(),
						estimatedRunningTimeInSeconds);
			}
		}

		/*
		 * iterate over all executing run resumes
		 */
		if (this.clientToRunResumes.containsKey(clientId)) {
			for (IRun run : this.clientToRunResumes.get(clientId)) {
				// long startTimeMillis = System.currentTimeMillis() -
				// run.getStartTime();
				// float percentFinished =
				// run.getPercentFinishedDuringCurrentExecution();
				// float totalPercentFinished = run.getPercentFinished();
				// // / 1000: convert to seconds
				// // * 100: scale up to 100 percent
				// long secondsFor100Percent = (long) (startTimeMillis /
				// percentFinished / 1000
				// * (100 - totalPercentFinished));
				long estimatedRunningTimeInSeconds = (long) ((double) run
						.getEstimatedRemainingCPUTimeInMs() / numberThreads
						/ 1000);
				result.put(run.getRunIdentificationString(),
						estimatedRunningTimeInSeconds);
			}
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Thread#run()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.threading.IRunSchedulerThread#run()
	 */
	@Override
	public void run() {
		// wait for new runs and run resumes
		while (!this.isInterrupted()) {
			try {
				// check the run queue for a new run or run resume
				final Triple<String, String, Boolean> pair = this.runQueue
						.poll();
				if (pair != null) {
					String clientId = pair.getFirst();
					final String runId = pair.getSecond();
					final IRun<?> run;
					final IRunSchedulerThread finalScheduler = this;
					boolean isResume = pair.getThird();

					if (!isResume) {
						// take a cloned copy of the run
						String uniqueRunId = ((IRun) this.repository
								.getStaticObjectWithNameAndVersion(IRun.class,
										runId)).getRunIdentificationString();
						run = (Run) this.repository
								.getStaticObjectWithNameAndVersion(IRun.class,
										runId)
								.clone();
						run.setRunIdentificationString(uniqueRunId);

						if (!this.clientToRuns.containsKey(clientId))
							this.clientToRuns.put(clientId,
									new HashSet<IRun<?>>());
						if (this.clientToRuns.get(clientId).contains(run))
							this.clientToRuns.get(clientId).remove(run);
						this.clientToRuns.get(clientId).add(run);

						Thread t = new Thread() {

							/*
							 * (non-Javadoc)
							 * 
							 * @see java.lang.Runnable#run()
							 */
							@Override
							public void run() {
								try {
									run.perform(finalScheduler);
								} catch (IOException e1) {
									e1.printStackTrace();
								} catch (RunRunnableInitializationException e) {
									e.printStackTrace();
								} catch (RunInitializationException e) {
									e.printStackTrace();
								}
							}
						};
						t.start();
					} else {
						List<IRunResult> results = new ArrayList<IRunResult>();
						try {
							run = ParameterOptimizationResult
									.parseFromRunResultFolder(repository,
											new File(FileUtils.buildPath(
													repository.getBasePath(
															IRunResult.class),
													runId)),
											results, false, false, false)
									.clone();
							run.setStatus(RUN_STATUS.SCHEDULED);

							if (this.clientToRuns.containsKey(clientId)) {
								for (IRun<?> r : this.clientToRuns
										.get(clientId))
									if (r.getRunIdentificationString()
											.equals(runId))
										this.clientToRuns.get(clientId)
												.remove(r);
							}
							if (!this.clientToRunResumes.containsKey(clientId))
								this.clientToRunResumes.put(clientId,
										new HashSet<IRun<?>>());
							if (this.clientToRunResumes.get(clientId)
									.contains(run))
								this.clientToRunResumes.get(clientId)
										.remove(run);
							this.clientToRunResumes.get(clientId).add(run);

							Thread t = new Thread() {

								/*
								 * (non-Javadoc)
								 * 
								 * @see java.lang.Runnable#run()
								 */
								@Override
								public void run() {
									try {
										run.resume(finalScheduler, runId);
									} catch (MissingParameterValueException e1) {
										e1.printStackTrace();
									} catch (IOException e1) {
										e1.printStackTrace();
									} catch (NoRunResultFormatParserException e1) {
										e1.printStackTrace();
									} catch (RunRunnableInitializationException e) {
										e.printStackTrace();
									} catch (RunInitializationException e) {
										e.printStackTrace();
									}
								}
							};
							t.start();
						} catch (

						Exception e) {
							e.printStackTrace();
						}

					}
				}
			} catch (ObjectNotRegisteredException e) {
				// run not found; do nothing
			} catch (ObjectVersionNotRegisteredException e2) {
				// run version not found; do nothing
			}
			try {
				sleep(1000);
			} catch (InterruptedException e) {
				this.interrupt();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Thread#interrupt()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.threading.IRunSchedulerThread#interrupt()
	 */
	@Override
	public void interrupt() {
		for (Collection<IRun<?>> runs : this.clientToRuns.values())
			for (IRun<?> run : runs)
				run.terminate();
		this.threadPool.shutdownNow();
		for (ThreadPoolExecutor executor : this.iterationThreadPool.values()) {
			executor.shutdownNow();
		}
		super.interrupt();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.threading.IRunSchedulerThread#registerRunRunnable(
	 * de.clusteval.run.runnable.RunRunnable)
	 */
	@Override
	public Future<?> registerRunRunnable(IRunRunnable runRunnable) {
		return this.threadPool.submit(runRunnable);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.threading.IRunSchedulerThread#
	 * registerIterationRunnable(de.clusteval.run.runnable.IterationRunnable)
	 */
	@Override
	public Future<?> registerIterationRunnable(
			IIterationRunnable iterationRunnable) {
		double f = this.getLoadFactor(this.repository);
		//
		// // TODO: for now we only delegate clustering iteration runnables to
		// // remote servers;
		// if (iterationRunnable instanceof ClusteringIterationRunnable) {
		// for (IBackendServer s : this.iterationThreadPool.keySet()) {
		// double f2 = this.getLoadFactor(s);
		// if (f2 < f) {
		// server = s;
		// f = f2;
		// }
		// }
		// }
		//
		// this.log.debug("submitting iteration to server with lowest load
		// factor: " + server + " " + f);
		// if
		// (!server.equals(ClustevalBackendServer.getSingletonBackendServer()))
		// {
		// this.log.debug(
		// "Selected server is remote, i.e. we wrap the iteration runnable in a
		// remote iteration runnable ");
		// iterationRunnable = new RemoteIterationRunnable(iterationRunnable);
		// }

		return this.iterationThreadPool.get(this.repository)
				.submit(iterationRunnable);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.threading.IRunSchedulerThread#
	 * informOnStartedIterationRunnable(java.lang.Thread,
	 * de.clusteval.run.runnable.IterationRunnable)
	 */
	@Override
	public synchronized void informOnStartedIterationRunnable(final Thread t,
			final IIterationRunnable runnable) {
		this.activeIterationRunnables.put(t, runnable);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.threading.IRunSchedulerThread#
	 * informOnFinishedIterationRunnable(java.lang.Thread,
	 * de.clusteval.run.runnable.IterationRunnable)
	 */
	@Override
	public synchronized void informOnFinishedIterationRunnable(final Thread t,
			final IIterationRunnable runnable) {
		if (this.activeIterationRunnables.containsKey(t)
				&& this.activeIterationRunnables.get(t).equals(runnable))
			this.activeIterationRunnables.remove(t);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.threading.IRunSchedulerThread#
	 * getActiveIterationRunnables()
	 */
	@Override
	public synchronized Map<Thread, IIterationRunnable<? extends IIterationWrapper, ?>> getActiveIterationRunnables() {
		return this.activeIterationRunnables;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.threading.IRunSchedulerThread#
	 * getIterationRunnableQueue()
	 */
	@Override
	public synchronized List<Runnable> getIterationRunnableQueue() {
		List<Runnable> result = new ArrayList<Runnable>();

		for (ThreadPoolExecutor e : iterationThreadPool.values()) {
			for (Runnable priorityRunnable : e.getQueue()) {
				result.add(((PriorityFuture) priorityRunnable).getRunnable());
			}
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.threading.IRunSchedulerThread#updateThreadPoolSize
	 * (int)
	 */
	@Override
	public synchronized void updateThreadPoolSize(final int numberThreads) {
		this.threadPool.setCorePoolSize(numberThreads);
		this.threadPool.setMaximumPoolSize(numberThreads);
		this.iterationThreadPool.get(this.repository)
				.setCorePoolSize(numberThreads);
		this.iterationThreadPool.get(this.repository)
				.setMaximumPoolSize(numberThreads);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.threading.IRunSchedulerThread#getThreadPoolSize()
	 */
	@Override
	public synchronized int getThreadPoolSize() {
		return this.iterationThreadPool.get(this.repository).getCorePoolSize();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.threading.IRunSchedulerThread#
	 * getRunExceptionMessages(java.lang.String, java.lang.String)
	 */
	@Override
	public List<String> getRunExceptionMessages(final String clientId,
			final String uniqueRunId, final boolean getWarningExceptions) {
		List<String> result = new ArrayList<String>();
		Collection<IRun> runs = new HashSet<IRun>();
		if (clientToRunResumes.containsKey(clientId))
			runs.addAll(clientToRunResumes.get(clientId));
		if (clientToRuns.containsKey(clientId))
			runs.addAll(clientToRuns.get(clientId));
		for (IRun<?> run : runs) {
			if (run.getRunIdentificationString().equals(uniqueRunId)) {
				for (IRunRunnable<?, ?, ?, ?> r : getWarningExceptions
						? run.getWarningExceptions().keySet()
						: run.getExceptions().keySet()) {
					for (Exception e : getWarningExceptions
							? run.getWarningExceptions().get(r)
							: run.getExceptions().get(r)) {
						if (r instanceof DataAnalysisRunRunnable) {
							DataAnalysisRunRunnable darr = (DataAnalysisRunRunnable) r;
							result.add(String.format("(%s) %s",
									darr.getDataConfig(), e.getMessage()));
						} else if (r instanceof RunAnalysisRunRunnable) {
							RunAnalysisRunRunnable rarr = (RunAnalysisRunRunnable) r;
							result.add(String.format("(%s) %s",
									rarr.getRunIdentifier(), e.getMessage()));
						} else if (r instanceof ExecutionRunRunnable) {
							ExecutionRunRunnable<?, ?> porr = (ExecutionRunRunnable) r;
							result.add(String.format("(%s, %s) %s",
									porr.getProgramConfig(),
									porr.getDataConfig(), e.getMessage()));
						} else
							result.add(String.format("%s", e.getMessage()));

					}
				}
			}
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.threading.IRunSchedulerThread#getRunExceptions(
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public List<Throwable> getRunExceptions(final String clientId,
			final String uniqueRunId) {
		List<Throwable> result = new ArrayList<Throwable>();
		Collection<IRun<?>> runs = new HashSet<IRun<?>>();
		if (clientToRunResumes.containsKey(clientId))
			runs.addAll(clientToRunResumes.get(clientId));
		if (clientToRuns.containsKey(clientId))
			runs.addAll(clientToRuns.get(clientId));
		for (IRun<?> run : runs) {
			if (run.getRunIdentificationString().equals(uniqueRunId)) {
				for (IRunRunnable<?, ?, ?, ?> r : run.getExceptions()
						.keySet()) {
					result.addAll(run.getExceptions().get(r));
				}
			}
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.threading.IRunSchedulerThread#clearRunExceptions(
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public void clearRunExceptions(String clientId, String uniqueRunId) {
		Collection<IRun> runs = new HashSet<IRun>();
		if (clientToRunResumes.containsKey(clientId))
			runs.addAll(clientToRunResumes.get(clientId));
		if (clientToRuns.containsKey(clientId))
			runs.addAll(clientToRuns.get(clientId));
		for (IRun<?> run : runs) {
			if (run.getRunIdentificationString().equals(uniqueRunId)) {
				for (IRunRunnable<?, ?, ?, ?> r : run.getExceptions()
						.keySet()) {
					run.clearExceptions();
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.threading.IRunSchedulerThread#
	 * getRunWarningExceptionMessages(java.lang.String, java.lang.String)
	 */
	@Override
	public List<String> getRunWarningExceptionMessages(final String clientId,
			final String uniqueRunId) {
		List<String> result = new ArrayList<String>();
		Collection<IRun> runs = new HashSet<IRun>();
		if (clientToRunResumes.containsKey(clientId))
			runs.addAll(clientToRunResumes.get(clientId));
		if (clientToRuns.containsKey(clientId))
			runs.addAll(clientToRuns.get(clientId));
		for (IRun<?> run : runs) {
			if (run.getRunIdentificationString().equals(uniqueRunId)) {
				for (IRunRunnable<?, ?, ?, ?> r : run.getWarningExceptions()
						.keySet()) {
					for (Exception e : run.getWarningExceptions().get(r)) {
						if (r instanceof DataAnalysisRunRunnable) {
							DataAnalysisRunRunnable darr = (DataAnalysisRunRunnable) r;
							result.add(String.format("(%s) %s",
									darr.getDataConfig(), e.getMessage()));
						} else if (r instanceof RunAnalysisRunRunnable) {
							RunAnalysisRunRunnable rarr = (RunAnalysisRunRunnable) r;
							result.add(String.format("(%s) %s",
									rarr.getRunIdentifier(), e.getMessage()));
						} else if (r instanceof ExecutionRunRunnable) {
							ExecutionRunRunnable porr = (ExecutionRunRunnable) r;
							result.add(String.format("(%s, %s) %s",
									porr.getProgramConfig(),
									porr.getDataConfig(), e.getMessage()));
						} else
							result.add(String.format("%s", e.getMessage()));

					}
				}
			}
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.threading.IRunSchedulerThread#
	 * getRunWarningExceptions(java.lang.String, java.lang.String)
	 */
	@Override
	public List<Throwable> getRunWarningExceptions(final String clientId,
			final String uniqueRunId) {
		List<Throwable> result = new ArrayList<Throwable>();
		Collection<IRun> runs = new HashSet<IRun>();
		if (clientToRunResumes.containsKey(clientId))
			runs.addAll(clientToRunResumes.get(clientId));
		if (clientToRuns.containsKey(clientId))
			runs.addAll(clientToRuns.get(clientId));
		for (IRun<?> run : runs) {
			if (run.getRunIdentificationString().equals(uniqueRunId)) {
				for (IRunRunnable<?, ?, ?, ?> r : run.getWarningExceptions()
						.keySet()) {
					result.addAll(run.getWarningExceptions().get(r));
				}
			}
		}
		return result;
	}

	@Override
	public void clearRunWarningExceptions(String clientId, String uniqueRunId) {
		Collection<IRun> runs = new HashSet<IRun>();
		if (clientToRunResumes.containsKey(clientId))
			runs.addAll(clientToRunResumes.get(clientId));
		if (clientToRuns.containsKey(clientId))
			runs.addAll(clientToRuns.get(clientId));
		for (IRun<?> run : runs) {
			if (run.getRunIdentificationString().equals(uniqueRunId)) {
				for (IRunRunnable<?, ?, ?, ?> r : run.getWarningExceptions()
						.keySet()) {
					run.clearWarningExceptions();
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.threading.IRunSchedulerThread#
	 * getIterationThreadPool()
	 */
	@Override
	public Map<IRepository, ThreadPoolExecutor> getIterationThreadPool() {
		return iterationThreadPool;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.threading.IRunSchedulerThread#
	 * addBackendServerSlave(de.clusteval.serverclient.IBackendServer)
	 */
	@Override
	public boolean addBackendServerSlave(IRepository server) {
		iterationThreadPool.put(server, new PriorityThreadPoolExecutor(
				server.getSupervisorThread().getRunScheduler()
						.getThreadPoolSize(),
				Integer.MAX_VALUE, 0, NANOSECONDS,
				new PriorityBlockingQueue(11, new PriorityFutureComparator())));
		// redistribute runnables
		redistributeRunnables();
		return true;
	}

	private void redistributeRunnables() {
		System.out.println("redistributeRunnables");
		Map<IRepository, Double> loadFactors = new HashMap<IRepository, Double>();
		for (IRepository server : this.iterationThreadPool.keySet()) {
			double f = this.getLoadFactor(server);
			loadFactors.put(server, f);
			System.out.println(server + " " + f);
		}

	}

	private double getLoadFactor(IRepository server) {
		ThreadPoolExecutor threadPoolExecutor = this.iterationThreadPool
				.get(server);
		if (threadPoolExecutor.getMaximumPoolSize() == 0)
			return Double.POSITIVE_INFINITY;
		return (double) (threadPoolExecutor.getActiveCount()
				+ threadPoolExecutor.getQueue().size())
				/ threadPoolExecutor.getMaximumPoolSize();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.threading.IRunSchedulerThread#
	 * isBackendServerSlaveRegistered(de.clusteval.serverclient.IBackendServer)
	 */
	@Override
	public boolean isBackendServerSlaveRegistered(IRepository server) {
		return iterationThreadPool.containsKey(server);
	}
}