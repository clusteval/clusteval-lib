/**
 * 
 */
package de.clusteval.framework.threading;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class PriorityFuture<T> implements RunnableFuture<T> {

	private Runnable runnable;
	private RunnableFuture<T> src;
	private long runStartTime;

	public PriorityFuture(Runnable runnable, RunnableFuture<T> other, long priority) {
		this.runnable = runnable;
		this.src = other;
		this.runStartTime = priority;
	}

	/**
	 * @return the runnable
	 */
	public Runnable getRunnable() {
		return runnable;
	}

	public long getPriority() {
		return runStartTime;
	}

	@Override
	public boolean cancel(boolean mayInterruptIfRunning) {
		return src.cancel(mayInterruptIfRunning);
	}

	@Override
	public boolean isCancelled() {
		return src.isCancelled();
	}

	@Override
	public boolean isDone() {
		return src.isDone();
	}

	@Override
	public T get() throws InterruptedException, ExecutionException {
		return src.get();
	}

	@Override
	public T get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
		return src.get();
	}

	@Override
	public void run() {
		src.run();
	}
}