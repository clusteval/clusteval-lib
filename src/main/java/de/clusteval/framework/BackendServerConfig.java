/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.framework;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Christian Wiwie
 * 
 */
public class BackendServerConfig {

	protected boolean noR;

	protected Set<String> administratorUUIDs;

	/**
	 * 
	 */
	public BackendServerConfig() {
		super();
		this.noR = false;
		this.administratorUUIDs = new HashSet<String>();
	}

	/**
	 * @param noR
	 *            the noR to set
	 */
	public void setNoR(boolean noR) {
		this.noR = noR;
	}

	/**
	 * @return the noR
	 */
	public boolean isNoR() {
		return noR;
	}

	/**
	 * @return The UUIDs of administrators.
	 */
	public Set<String> getAdministratorUUIDs() {
		return administratorUUIDs;
	}

	/**
	 * @param administratorUUID
	 *            A UUID of an administrator
	 */
	public void addAdministratorUUID(String administratorUUID) {
		this.administratorUUIDs.add(administratorUUID);
	}
}
