/**
 * 
 */
package de.clusteval.utils;

import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.SerializableWrapperRepositoryObject;

/**
 * @author Christian Wiwie
 *
 * @param <T>
 */
public interface IErrorSerializableWrapper<T> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.SerializableWrapper#compareTo(de.
	 * clusteval.framework.repository.SerializableWrapper)
	 */
	int compareTo(SerializableWrapperRepositoryObject<IRepositoryObject> o);

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.SerializableWrapper#equals(java.
	 * lang.Object)
	 */
	boolean equals(Object obj);

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.SerializableWrapper#hashCode()
	 */
	int hashCode();

}