/**
 * 
 */
package de.clusteval.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.clusteval.data.dataset.DataSetAttributeParser;
import dk.sdu.imada.compbio.utils.Pair;
import dk.sdu.imada.compbio.utils.parse.TextFileParser;

/**
 * @author Christian Wiwie
 *
 */
public class MatrixParser extends TextFileParser {

	protected List<Pair<String, double[]>> idToCoordinates;

	/**
	 * @param absFilePath
	 * @throws IOException
	 */
	public MatrixParser(String absFilePath) throws IOException {
		super(absFilePath, new int[0], new int[0]);
		this.setLockTargetFile(true);
		this.idToCoordinates = new ArrayList<Pair<String, double[]>>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see utils.parse.TextFileParser#checkLine(java.lang.String)
	 */
	@Override
	protected boolean checkLine(String line) {
		return !DataSetAttributeParser.attributeLinePrefixPattern.matcher(line).matches();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see utils.parse.TextFileParser#processLine(java.lang.String[],
	 * java.lang.String[])
	 */
	@SuppressWarnings("unused")
	@Override
	protected void processLine(String[] key, String[] value) {
		double[] coords = new double[value.length - 1];
		for (int i = 1; i < value.length; i++)
			coords[i - 1] = Double.valueOf(value[i]);
		this.idToCoordinates.add(Pair.getPair(value[0], coords));
	}

	public List<Pair<String, double[]>> getCoordinates() {
		return this.idToCoordinates;
	}
}