/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
package de.clusteval.utils.plot;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RserveException;

import de.clusteval.data.DataConfig;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.AbsoluteDataSet;
import de.clusteval.data.dataset.DataMatrix;
import de.clusteval.data.dataset.IDataSet;
import de.clusteval.data.dataset.RelativeDataSet;
import de.clusteval.data.dataset.format.InvalidDataSetFormatVersionException;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.framework.repository.IMyRengine;
import de.clusteval.run.runresult.IParameterOptimizationResult;
import de.clusteval.utils.IInMemoryListener;
import dk.sdu.imada.compbio.utils.ArraysExt;
import dk.sdu.imada.compbio.utils.SimilarityMatrix;

/**
 * @author Christian Wiwie
 * 
 */
public abstract class Plotter {

	/**
	 * @param result
	 * @throws InterruptedException
	 */
	public static void plotParameterOptimizationResult(
			final IParameterOptimizationResult result)
			throws InterruptedException {

		IMyRengine rEngine;
		try {
			rEngine = result.getRepository().getRengineForCurrentThread();
			try {
				rEngine.voidEval("Sys.setlocale(category='LC_NUMERIC',locale='C')");
				/*
				 * Define functions
				 */
				rEngine.voidEval("getDensity <- function(x) {"
						+ "	return (c(strsplit(x=as.character(x[1]),split=',')[[1]][1],x[-1]))}");

				rEngine.voidEval(
						"plotDensityVSQuality <- function(title, path, densityParam) {"
								+ "data <- t(apply(read.table(path, sep='\t',header=TRUE), 1, getDensity));"
								+ "svg(filename=paste(path,'.svg',sep=''));"
								// + "par(cex=.5);"
								+ "matplot(x=data[,1],data[,-1],xlab=densityParam,ylab='Clustering quality',main=title,type='p',pch=20,col=1:6);"
								+ "legend('topleft',legend=colnames(data)[-1],col=1:6,pch=20);"
								+ "dev.off();"
								+ "png(filename=paste(path,'.png',sep=''));"
								// + "par(cex=.5);"
								+ "matplot(x=data[,1],data[,-1],xlab=densityParam,ylab='Clustering quality',main=title,type='p',pch=20,col=1:6);"
								+ "legend('topleft',legend=colnames(data)[-1],col=1:6,pch=20);"
								+ "dev.off()" + "}");
				rEngine.voidEval("plotDensityVSQuality(" + "'" + result.getMethod()
						.getProgramConfig().getProgram().getMajorName()
						+ " vs. "
						+ result.getMethod().getDataConfig().getDatasetConfig()
								.getDataSet().getFullName()
						+ "'," + "'" + result.getAbsolutePath() + "', " + "'"
						+ result.getMethod().getPlotDensityParameter() + "')");
			} finally {
				rEngine.clear();
			}
		} catch (RserveException e) {
		}

	}

	/**
	 * This method creates a file containing isoMDS coordinates using the
	 * similarity matrix file of the given data configuration.
	 * 
	 * @param dataConfig
	 * @throws UnknownDataSetFormatException
	 * @throws InvalidDataSetFormatVersionException
	 * @throws IllegalArgumentException
	 * @throws IOException
	 * @throws REngineException
	 * @throws InterruptedException
	 */
	public static void assessAndWriteIsoMDSCoordinates(
			final DataConfig dataConfig) throws UnknownDataSetFormatException,
			InvalidDataSetFormatVersionException, IllegalArgumentException,
			IOException, REngineException, InterruptedException {

		IInMemoryListener l = new IInMemoryListener() {
		};
		try {
			IMyRengine rEngine = dataConfig.getRepository()
					.getRengineForCurrentThread();
			try {
				IDataSet absStandard = dataConfig.getDatasetConfig()
						.getDataSet().getInStandardFormat();

				String newPath = dataConfig.getDatasetConfig().getDataSet()
						.getOriginalDataSet().getAbsolutePath() + ".isoMDS";
				if (new File(newPath).exists())
					return;

				boolean wasLoaded = absStandard.isInMemory();
				if (!wasLoaded)
					absStandard.loadIntoMemory(l);
				SimilarityMatrix simMatrix;
				double[][] sims;
				String[] ids;
				try {
					simMatrix = (SimilarityMatrix) dataConfig.getDatasetConfig()
							.getDataSet().getInStandardFormat()
							.getDataSetContent();
					sims = simMatrix.toArray();
					ids = new String[simMatrix.getIds().size()];
					for (Map.Entry<String, Integer> entry : simMatrix.getIds()
							.entrySet())
						ids[entry.getValue()] = entry.getKey();
				} finally {
					if (!wasLoaded)
						absStandard.unloadFromMemory(l);
				}
				rEngine.assign("isomds_x",
						ArraysExt.subtract(ArraysExt.max(sims), sims, true));
				rEngine.assign("isomds_labels", ids);
				rEngine.voidEval("rownames(isomds_x) <- isomds_labels;");
				rEngine.voidEval("colnames(isomds_x) <- isomds_labels;");
				rEngine.voidEval("isomds_dists <- as.dist(isomds_x+0.00000000000001);");
				rEngine.voidEval("library(MASS);");
				rEngine.voidEval("iso <- isoMDS(isomds_dists)$points;");
				double[][] isoMDS = rEngine.eval("iso").asDoubleMatrix();

				StringBuilder sb = new StringBuilder();

				for (int i = 0; i < ids.length; i++) {
					sb.append(ids[i]);
					sb.append("\t");
					double[] row = isoMDS[i];
					for (int c = 0; c < row.length; c++) {
						sb.append(row[c] + "");
						sb.append("\t");
					}
					sb.deleteCharAt(sb.length() - 1);
					sb.append(System.getProperty("line.separator"));
				}
				sb.deleteCharAt(sb.length() - 1);

				BufferedWriter bw = new BufferedWriter(new FileWriter(newPath));
				bw.append(sb.toString());
				bw.close();
			} catch (REXPMismatchException e) {
				e.printStackTrace();
			} finally {
				rEngine.voidEval("rm(isomds_x)");
				rEngine.voidEval("rm(isomds_labels)");
				rEngine.voidEval("rm(isomds_dists)");
				rEngine.voidEval("rm(iso)");
			}
		} catch (RserveException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param dataConfig
	 * @throws InvalidDataSetFormatVersionException
	 * @throws IllegalArgumentException
	 * @throws IOException
	 * @throws REngineException
	 * @throws UnknownDataSetFormatException
	 * @throws InterruptedException
	 */
	public static void assessAndWritePCACoordinates(
			final IDataConfig dataConfig)
			throws InvalidDataSetFormatVersionException,
			IllegalArgumentException, IOException, REngineException,
			UnknownDataSetFormatException, InterruptedException {

		IInMemoryListener l = new IInMemoryListener() {
		};
		try {
			IMyRengine rEngine = dataConfig.getRepository()
					.getRengineForCurrentThread();
			try {

				double[][] x;
				String[] ids;

				IDataSet standard = dataConfig.getDatasetConfig().getDataSet()
						.getOriginalDataSet();
				String newPath = standard.getAbsolutePath() + ".PCA";
				if (new File(newPath).exists())
					return;

				if (standard instanceof AbsoluteDataSet) {
					AbsoluteDataSet absStandard = (AbsoluteDataSet) standard;

					boolean wasLoaded = absStandard.isInMemory();
					if (!wasLoaded)
						absStandard.loadIntoMemory(l);
					DataMatrix dataMatrix;

					try {
						dataMatrix = absStandard.getDataSetContent();
						x = dataMatrix.getData();
						ids = dataMatrix.getIds();
					} finally {
						// if (!wasLoaded)
						// absStandard.unloadFromMemory();
					}
				} else {
					RelativeDataSet absStandard = (RelativeDataSet) standard;

					boolean wasLoaded = absStandard.isInMemory();
					if (!wasLoaded)
						absStandard.loadIntoMemory(l);
					SimilarityMatrix dataMatrix;

					try {
						dataMatrix = absStandard.getDataSetContent();
						x = dataMatrix.toArray();
						ids = dataMatrix.getIdsArray();
					} finally {
						// if (!wasLoaded)
						// absStandard.unloadFromMemory();
					}

				}

				rEngine.assign("pca_x", x);
				rEngine.assign("pca_labels", ids);
				rEngine.voidEval("rownames(pca_x) <- pca_labels;");
				rEngine.voidEval("pca <- prcomp(pca_x)$x;");
				double[][] pca = rEngine.eval("pca").asDoubleMatrix();

				StringBuilder sb = new StringBuilder();

				for (int i = 0; i < ids.length; i++) {
					sb.append(ids[i]);
					sb.append("\t");
					double[] row = pca[i];
					for (int c = 0; c < row.length; c++) {
						sb.append(row[c] + "");
						sb.append("\t");
					}
					sb.deleteCharAt(sb.length() - 1);
					sb.append(System.getProperty("line.separator"));
				}
				sb.deleteCharAt(sb.length() - 1);

				BufferedWriter bw = new BufferedWriter(new FileWriter(newPath));
				bw.append(sb.toString());
				bw.close();
			} catch (REXPMismatchException e) {
				e.printStackTrace();
			} finally {
				rEngine.voidEval("rm(pca_x)");
				rEngine.voidEval("rm(pca_labels)");
				rEngine.voidEval("rm(pca)");
			}
		} catch (RserveException e) {
			e.printStackTrace();
		}
	}
}
