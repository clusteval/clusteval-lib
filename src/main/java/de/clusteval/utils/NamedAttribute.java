/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.utils;

import java.io.File;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryObject;
import de.clusteval.framework.repository.SerializableWrapperRepositoryObject;

/**
 * This class is a wrapper for variables that should be shared throughout the
 * whole framework and should be unambigiously identifiable by their name.
 * 
 * @author Christian Wiwie
 * @param <T>
 * 
 */
public abstract class NamedAttribute<T> extends RepositoryObject
		implements
			INamedAttribute<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7430871994082296368L;
	protected String name;
	protected T value;

	/**
	 * @param repository
	 * @param name
	 * @param value
	 * @throws RegisterException
	 */
	public NamedAttribute(final IRepository repository, final String name,
			final T value) throws RegisterException {
		super(repository, System.currentTimeMillis(), new File(name));

		this.name = name;
		this.value = value;
	}

	/**
	 * The copy constructor of named attributes.
	 * 
	 * @param other
	 *            The object to clone.
	 * @throws RegisterException
	 */
	public NamedAttribute(final NamedAttribute<T> other)
			throws RegisterException {
		super(other);

		this.name = other.name;
		this.value = cloneValue(other.value);
	}

	protected abstract T cloneValue(final T value);

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.RepositoryObject#equals(java.lang.Object)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.INamedAttribute#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj.getClass().equals(this.getClass())))
			return false;

		@SuppressWarnings("unchecked")
		INamedAttribute<T> other = (INamedAttribute<T>) obj;
		return this.getName().equals(other.getName())
				&& this.getValue().equals(other.getValue());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.INamedAttribute#getName()
	 */
	@Override
	public String getName() {
		return this.name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.INamedAttribute#getValue()
	 */
	@Override
	public T getValue() {
		return this.value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.INamedAttribute#setValue(T)
	 */
	@Override
	public void setValue(final T value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.INamedAttribute#toString()
	 */
	@Override
	public String toString() {
		return this.getValue().toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#getVersion()
	 */
	@Override
	public ComparableVersion getVersion() {
		return new ComparableVersion("1");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#asSerializable()
	 */
	@Override
	public SerializableWrapperRepositoryObject<? extends IRepositoryObject> asSerializableInternal() {
		// TODO: do we need this?
		return null;
	}
}
