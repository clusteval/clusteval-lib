/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.utils;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.Repository;
import de.clusteval.framework.repository.SerializableWrapperRepositoryObject;
import dk.sdu.imada.compbio.file.FileUtils;
import dk.sdu.imada.compbio.utils.Pair;

/**
 * @author Christian Wiwie
 * @param <T>
 * 
 */
public abstract class JARFinder<T extends IRepositoryObject> extends Finder<T>
		implements
			IJARFinder<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4554250401504794871L;

	protected Map<URL, URLClassLoader> classLoaders;

	protected Map<String, List<File>> waitingFiles;

	protected Map<String, Long> loadedJarFileChangeDates;

	protected Map<Class<? extends IRepositoryObject>, Map<String, Set<ComparableVersion>>> fullClassNamesForPresentFiles;

	/**
	 * @param repository
	 * @throws RegisterException
	 */
	public JARFinder(IRepository repository, Class<T> classToFind)
			throws RegisterException {
		super(repository, classToFind);
		this.classLoaders = this.repository.getJARFinderClassLoaders();
		this.waitingFiles = this.repository.getJARFinderWaitingFiles();
		this.loadedJarFileChangeDates = this.repository
				.getFinderLoadedJarFileChangeDates();
		this.fullClassNamesForPresentFiles = this.repository
				.getFullClassNamesAndVersionsForPresentFiles();

		if (!fullClassNamesForPresentFiles.containsKey(this.classToFind))
			fullClassNamesForPresentFiles.put(this.classToFind,
					new HashMap<String, Set<ComparableVersion>>());
	}

	protected Pair<String, ComparableVersion> getClassNameAndVersionFromJARFile(
			final File f) throws DynamicComponentWrongNamingSchemeException {
		int dashPos = f.getName().indexOf("-");
		boolean valid = dashPos > -1 && f.getName().endsWith(".jar");

		String name = null;
		ComparableVersion version = null;

		// check name
		if (valid) {
			name = f.getName().substring(0, dashPos);

			Pattern namePattern = Pattern.compile("([a-zA-Z0-9]+)");
			Matcher m = namePattern.matcher(name);
			valid &= m.matches();

			name = getClassNamePrefix() + name;
		}
		// check version
		if (valid) {
			String versionStr = f.getName().substring(dashPos + 1)
					.replace(".jar", "");

			try {
				version = new ComparableVersion(versionStr);
			} catch (Exception e) {
				valid = false;
			}
		}

		if (!valid)
			throw new DynamicComponentWrongNamingSchemeException("The file "
					+ f.getName()
					+ " does not follow the correct naming schema: <Name><Type>-<Version>.jar");

		return Pair.getPair(name, version);
	}

	protected String getClassNamePrefix() {
		return String.format("%s.", this.classToFind.getPackage().getName());
	}

	protected String[] classNamesForJARFile(final File f) {
		return new String[]{String.format("%s%s", this.getClassNamePrefix(),
				f.getName().substring(0, f.getName().indexOf("-")))};
	}

	protected Class<?> loadClass(final String className,
			final URLClassLoader loader) throws ClassNotFoundException {
		return Class.forName(className, true, loader);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.Finder#doOnFileFound(java.io.File)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.IJARFinder#doOnFileFound(java.io.File)
	 */
	@Override
	public void doOnFileFound(File file) throws MalformedURLException {
		loadJAR(file);
	}

	/**
	 * Load jar.
	 * 
	 * @param f
	 *            the f
	 * @throws MalformedURLException
	 *             the malformed url exception
	 * @throws InstantiationException
	 *             the instantiation exception
	 * @throws IllegalAccessException
	 *             the illegal access exception
	 */
	protected void loadJAR(final File f) throws MalformedURLException {
		URLClassLoader loader = getURLClassLoaderAndStore(f);
		loadJAR(f, loader);
	}

	protected void loadJAR(final File f, final URLClassLoader loader)
			throws MalformedURLException {
		if (!isJARLoaded(f)) {
			Set<Pair<Class<?>, ComparableVersion>> loadedClasses = new HashSet<>();
			boolean anyClassFailedToLoad = false;

			try {
				// assume that the classname is equivalent to the jar-filename
				String[] classNames = classNamesForJARFile(f);
				ComparableVersion classVersion = null;
				SerializableWrapperRepositoryObject<IRepositoryObject> wrapper;
				for (String className : classNames) {
					String simpleClassName = className
							.replace(this.getClassNamePrefix(), "");
					try {
						if (classVersion == null)
							classVersion = getClassNameAndVersionFromJARFile(f)
									.getSecond();
						Class<?> c = loadClass(className, loader);

						wrapper = new ErrorSerializableWrapper<>(f, className,
								classVersion.toString());

						// Clear the exceptions associated with this object
						if (knownExceptions.containsKey(this.classToFind)) {
							knownExceptions.get(this.classToFind)
									.remove(wrapper);
							if (knownExceptions.get(this.classToFind).isEmpty())
								knownExceptions.remove(this.classToFind);
						}
						loadedClasses.add(Pair.getPair(c, classVersion));
					} catch (Throwable e) {
						// check if we already logged a warning for this
						boolean known = false;

						wrapper = new ErrorSerializableWrapper<>(f, className,
								classVersion.toString());

						if (knownExceptions.containsKey(this.classToFind)
								&& knownExceptions.get(this.classToFind)
										.containsKey(wrapper))
							for (Throwable other : knownExceptions
									.get(this.classToFind).get(wrapper))
								if ((e.getMessage() == null
										&& other.getMessage() == null)
										|| (e.getMessage() != null
												&& other.getMessage() != null
												&& other.getMessage().equals(
														e.getMessage()))) {
									known = true;
									break;
								}
						if (known) {
							if (e instanceof ClassNotFoundException) {
								this.log.debug(
										simpleClassName + " (v" + classVersion
												+ ") could not be loaded: "
												+ e.getMessage());
							} else if (e instanceof NoClassDefFoundError) {
								this.log.debug(simpleClassName + " (v"
										+ classVersion
										+ "): Could not load class in file " + f
										+ " because a required class is missing:"
										+ e.getMessage());
							}
						} else {
							wrapper = new ErrorSerializableWrapper<>(f,
									className, classVersion.toString());

							if (!knownExceptions.containsKey(this.classToFind))
								knownExceptions.put(this.classToFind,
										new ConcurrentHashMap<ISerializableWrapperRepositoryObject<? extends IRepositoryObject>, List<Throwable>>());
							if (!knownExceptions.get(this.classToFind)
									.containsKey(wrapper))
								knownExceptions.get(this.classToFind).put(
										wrapper, new ArrayList<Throwable>());
							knownExceptions.get(this.classToFind).get(wrapper)
									.add(e);

							if (e instanceof ClassNotFoundException) {
								if (e.getCause() != null) {
									Throwable cause = e.getCause();
									if (cause instanceof IncompatibleClustEvalVersionException)
										this.log.warn(
												((IExceptionWithDependent) cause)
														.getMessage(true));
									else if (cause instanceof MissingClustEvalVersionException
											|| e.getCause() instanceof DynamicComponentMissingVersionException)
										this.log.warn(simpleClassName + " (v"
												+ classVersion
												+ "): Incompatible with this ClustEval server: "
												+ e.getMessage());
									else
										this.log.warn(simpleClassName + " (v"
												+ classVersion
												+ ") could not be loaded: "
												+ e.getMessage());
								} else
									this.log.warn(simpleClassName + " (v"
											+ classVersion
											+ ") could not be loaded: "
											+ e.getMessage());
							} else if (e instanceof DynamicComponentWrongNamingSchemeException) {
								this.log.warn(
										simpleClassName + " (v" + classVersion
												+ ") could not be loaded: "
												+ e.getMessage());
							} else if (e instanceof NoClassDefFoundError) {
								// check whether the class has been loaded with
								// a different classlader
								String[] missingMessageSplit = e.getMessage()
										.split("/");
								String missingClassName = missingMessageSplit[missingMessageSplit.length
										- 1];

								this.log.debug("Trying to load " + className
										+ " (v" + classVersion
										+ ") with self-guessed parent class loader from file "
										+ f);
								Class<? extends T> clazz = this.getRepository()
										.resolveAndGetRegisteredClass(
												this.classToFind,
												missingClassName, true);
								if (clazz != null) {
									ClassLoader clLoader = clazz
											.getClassLoader();
									loadJAR(f, getURLClassLoaderAndStore(f,
											clLoader));
									continue;
								}
								if (e.getCause() != null && e
										.getCause() instanceof ClassNotFoundException
										&& e.getCause().getCause() != null
										&& e.getCause()
												.getCause() instanceof IExceptionWithDependent)
									this.log.warn(String.format(
											"Could not load class %s (v%s) because it depends on a class that could not be loaded (%s): %s",
											className, classVersion,
											((IExceptionWithDependent) e
													.getCause().getCause())
															.getDependent(),
											e.getCause().getMessage()));
								else
									this.log.warn("Could not load class "
											+ className + " (v" + classVersion
											+ ") because it depends on a missing class: "
											+ e.getMessage());
								// add this file to the waiting files
								if (!waitingFiles.containsKey(missingClassName))
									waitingFiles.put(missingClassName,
											new ArrayList<File>());
								waitingFiles.get(missingClassName).add(f);
							}
						}
						anyClassFailedToLoad = true;
						break;
					}

					// process files depending on this file
					simpleClassName = className
							.substring(className.lastIndexOf('.') + 1);
					if (waitingFiles.containsKey(simpleClassName)) {
						for (File waiting : waitingFiles.get(simpleClassName)) {
							if (waiting.equals(f))
								continue;
							loadJAR(waiting, getURLClassLoaderAndStore(waiting,
									classLoaders.get(f.toURI().toURL())));
						}
						waitingFiles.remove(simpleClassName);
					}
					String simpleClassNameWithVersion = simpleClassName + ":"
							+ classVersion.toString();
					if (waitingFiles.containsKey(simpleClassNameWithVersion)) {
						for (File waiting : waitingFiles
								.get(simpleClassNameWithVersion))
							loadJAR(waiting, getURLClassLoaderAndStore(waiting,
									classLoaders.get(f.toURI().toURL())));
						waitingFiles.remove(simpleClassNameWithVersion);
					}
				}
				loadedJarFileChangeDates.put(f.getAbsolutePath(),
						f.lastModified());
			} finally {
				try {
					Pair<String, ComparableVersion> nameAndVersion = getClassNameAndVersionFromJARFile(
							f);
					if (!fullClassNamesForPresentFiles.get(this.classToFind)
							.containsKey(nameAndVersion.getFirst()))
						fullClassNamesForPresentFiles.get(this.classToFind).put(
								nameAndVersion.getFirst(),
								new HashSet<ComparableVersion>());
					fullClassNamesForPresentFiles.get(this.classToFind)
							.get(nameAndVersion.getFirst())
							.add(nameAndVersion.getSecond());
				} catch (DynamicComponentWrongNamingSchemeException e) {
					// if this exception occurs here, it has happened above as
					// well
					// and we can't do anything about it
					// TODO: or can we?
				}

				// in case, any class fails to load, we unload all classes
				// of the JAR
				if (anyClassFailedToLoad) {
					for (Pair<Class<?>, ComparableVersion> loadedClassWithVersion : loadedClasses) {
						this.removeOldObject(
								(Class) loadedClassWithVersion.getFirst(),
								loadedClassWithVersion.getSecond());
					}
				}
			}
		}

	}

	protected final URLClassLoader getURLClassLoaderAndStore(final File f)
			throws MalformedURLException {
		URLClassLoader loader = getURLClassLoader0(f);
		classLoaders.put(f.toURI().toURL(), loader);
		return loader;
	}

	protected final URLClassLoader getURLClassLoaderAndStore(final File f,
			final ClassLoader parent) throws MalformedURLException {
		URLClassLoader loader = getURLClassLoader0(f, parent);
		classLoaders.put(f.toURI().toURL(), loader);
		return loader;
	}

	protected URLClassLoader getURLClassLoader0(final File f)
			throws MalformedURLException {
		return getURLClassLoader0(f, ClassLoader.getSystemClassLoader());
	}

	protected abstract URLClassLoader getURLClassLoader0(final File f,
			final ClassLoader parent) throws MalformedURLException;

	/**
	 * We check whether the jar file has been loaded, using its modification
	 * date. The jar is only assumed do be loaded, if the modification dates are
	 * equal.
	 * 
	 * @param f
	 *            The jar file we want to load.
	 * @return true, if the jar file has been loaded, false otherwise.
	 */
	protected boolean isJARLoaded(final File f) {
		boolean equalTimes = loadedJarFileChangeDates
				.containsKey(f.getAbsolutePath())
				&& loadedJarFileChangeDates.get(f.getAbsolutePath()) == f
						.lastModified();
		if (!equalTimes)
			return false;

		try {
			Pair<String, ComparableVersion> pair = getClassNameAndVersionFromJARFile(
					f);
			ComparableVersion classVersion = pair.getSecond();
			String className = pair.getFirst();
			return this.repository.isClassRegistered(this.classToFind,
					className, classVersion);
		} catch (DynamicComponentWrongNamingSchemeException e) {
			return false;
		}
	}

	protected Collection<Class<? extends T>> getRegisteredObjectSet() {
		return (Collection) this.repository.getClasses(this.getClassToFind());
	}

	protected void validateRegisteredObjects() {
		/*
		 * First check whether all registered objects currently in the
		 * repository do still exist
		 */
		Collection<Pair<Class<? extends T>, ComparableVersion>> toRemove = new HashSet<Pair<Class<? extends T>, ComparableVersion>>();
		for (Class<? extends T> object : getRegisteredObjectSet())
			try {
				ComparableVersion version = Repository
						.getVersionOfDynamicClass(object);
				if (!new File(FileUtils.buildPath(
						getBaseDir().getAbsolutePath(),
						object.getSimpleName() + "-" + version + ".jar"))
								.exists()) {
					toRemove.add(
							new Pair<Class<? extends T>, ComparableVersion>(
									object, version));
				}
			} catch (DynamicComponentMissingVersionException e) {
				// should not happen
				e.printStackTrace();
			}

		for (Pair<Class<? extends T>, ComparableVersion> object : toRemove) {
			removeOldObject(object.getFirst(), object.getSecond());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.Finder#findAndRegisterObjects()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.IJARFinder#findAndRegisterObjects()
	 */
	@Override
	public void findAndRegisterObjects()
			throws RegisterException, InterruptedException {
		validateRegisteredObjects();
		super.findAndRegisterObjects();
	}

	protected void removeOldObject(Class<? extends T> clazz,
			ComparableVersion version) {
		this.repository.unregisterClass(this.getClassToFind(), clazz);
		fullClassNamesForPresentFiles.get(this.classToFind).get(clazz.getName())
				.remove(version);
		repository.getMavenResolver()
				.informOnClassRemoved(clazz.getSimpleName(), version);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.Finder#checkFile(java.io.File)
	 */
	@Override
	protected boolean checkFile(File file) {
		// remove the I of the interface
		String name = this.classToFind.getSimpleName().substring(1);
		return file.getName().matches(String.format(".*%s-.+.jar", name));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.Finder#getIterator()
	 */
	@Override
	protected Iterator<File> getIterator() {
		return new RecursiveSubDirectoryIterator(getBaseDir());
	}
}
