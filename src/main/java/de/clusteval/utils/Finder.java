/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.utils;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.cluster.paramOptimization.ParameterOptimizationMethodFinder;
import de.clusteval.cluster.quality.ClusteringQualityMeasureFinder;
import de.clusteval.context.ContextFinder;
import de.clusteval.data.DataConfigFinder;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.DataSetConfigFinder;
import de.clusteval.data.dataset.DataSetFinder;
import de.clusteval.data.dataset.IDataSet;
import de.clusteval.data.dataset.IDataSetConfig;
import de.clusteval.data.dataset.format.DataSetFormatFinder;
import de.clusteval.data.dataset.generator.DataSetGeneratorFinder;
import de.clusteval.data.dataset.type.DataSetTypeFinder;
import de.clusteval.data.distance.DistanceMeasureFinder;
import de.clusteval.data.goldstandard.GoldStandardConfigFinder;
import de.clusteval.data.goldstandard.GoldStandardFinder;
import de.clusteval.data.goldstandard.IGoldStandard;
import de.clusteval.data.goldstandard.IGoldStandardConfig;
import de.clusteval.data.preprocessing.DataPreprocessorFinder;
import de.clusteval.data.randomizer.DataRandomizerFinder;
import de.clusteval.data.statistics.DataStatisticFinder;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;
import de.clusteval.framework.repository.NoRepositoryObjectFinderFoundException;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryEvent;
import de.clusteval.framework.repository.RepositoryObject;
import de.clusteval.framework.repository.RepositoryRemoveEvent;
import de.clusteval.framework.repository.RepositoryReplaceEvent;
import de.clusteval.framework.repository.SerializableWrapperRepositoryObject;
import de.clusteval.framework.repository.maven.InvalidDynamicComponentNameException;
import de.clusteval.framework.repository.parse.RepositoryObjectParseException;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IStandaloneProgram;
import de.clusteval.program.ProgramConfigFinder;
import de.clusteval.program.r.IRProgram;
import de.clusteval.program.r.RProgramFinder;
import de.clusteval.run.IRun;
import de.clusteval.run.RunFinder;
import de.clusteval.run.runresult.format.RunResultFormatFinder;
import de.clusteval.run.runresult.postprocessing.RunResultPostprocessorFinder;
import de.clusteval.run.statistics.RunDataStatisticFinder;
import de.clusteval.run.statistics.RunStatisticFinder;

/**
 * @author Christian Wiwie
 * @param <T>
 * 
 */
public abstract class Finder<T extends IRepositoryObject> extends RepositoryObject implements IFinder<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3311212879567900028L;

	public static Class<? extends RepositoryObject> getFinderClassFromSimpleClassName(final String simpleClassName)
			throws InvalidDynamicComponentNameException {
		Class<? extends RepositoryObject> c = null;
		if (simpleClassName.endsWith("ClusteringQualityMeasure"))
			c = ClusteringQualityMeasureFinder.class;
		else if (simpleClassName.endsWith("Context"))
			c = ContextFinder.class;
		else if (simpleClassName.endsWith("DataPreprocessor"))
			c = DataPreprocessorFinder.class;
		else if (simpleClassName.endsWith("DataRandomizer"))
			c = DataRandomizerFinder.class;
		else if (simpleClassName.endsWith("DataSetFormat"))
			c = DataSetFormatFinder.class;
		else if (simpleClassName.endsWith("DataSetGenerator"))
			c = DataSetGeneratorFinder.class;
		else if (simpleClassName.endsWith("DataSetType"))
			c = DataSetTypeFinder.class;
		else if (simpleClassName.endsWith("RunDataStatistic"))
			c = RunDataStatisticFinder.class;
		else if (simpleClassName.endsWith("DataStatistic"))
			c = DataStatisticFinder.class;
		else if (simpleClassName.endsWith("DistanceMeasure"))
			c = DistanceMeasureFinder.class;
		else if (simpleClassName.endsWith("ParameterOptimizationMethod"))
			c = ParameterOptimizationMethodFinder.class;
		else if (simpleClassName.endsWith("RProgram"))
			c = RProgramFinder.class;
		else if (simpleClassName.endsWith("RunResultFormat"))
			c = RunResultFormatFinder.class;
		else if (simpleClassName.endsWith("RunResultPostprocessor"))
			c = RunResultPostprocessorFinder.class;
		else if (simpleClassName.endsWith("RunStatistic"))
			c = RunStatisticFinder.class;
		if (c != null)
			return c;
		throw new InvalidDynamicComponentNameException(simpleClassName);
	}

	public static Class<? extends RepositoryObject> getFinderClassForClass(
			final Class<? extends IRepositoryObject> type) throws NoRepositoryObjectFinderFoundException {
		if (type.equals(IDataSet.class))
			return DataSetFinder.class;
		else if (type.equals(IDataSetConfig.class))
			return DataSetConfigFinder.class;
		else if (type.equals(IDataConfig.class))
			return DataConfigFinder.class;
		else if (type.equals(IGoldStandardConfig.class))
			return GoldStandardConfigFinder.class;
		else if (type.equals(IGoldStandard.class))
			return GoldStandardFinder.class;
		else if (type.equals(IRun.class))
			return RunFinder.class;
		else if (type.equals(IProgramConfig.class))
			return ProgramConfigFinder.class;
		else if (type.equals(IRProgram.class))
			return RProgramFinder.class;
		throw new NoRepositoryObjectFinderFoundException(type);
	}

	protected Class<T> classToFind;

	protected Map<Class<? extends IRepositoryObject>, Map<ISerializableWrapperRepositoryObject<? extends IRepositoryObject>, List<Throwable>>> knownExceptions;

	protected boolean foundInLastRun;

	/**
	 * @param repository
	 * @param classToFind
	 * @throws RegisterException
	 */
	public Finder(IRepository repository, Class<T> classToFind) throws RegisterException {
		super(repository, System.currentTimeMillis(), new File(repository.getBasePath(classToFind)));

		this.classToFind = classToFind;

		this.knownExceptions = this.repository.getKnownFinderExceptions();

		this.register();
	}

	/**
	 * The copy constructor of finder.
	 * 
	 * @param other
	 *            The object to clone.
	 * @throws RegisterException
	 */
	public Finder(final Finder<T> other) throws RegisterException {
		super(other);
		this.classToFind = other.classToFind;
		this.knownExceptions = this.repository.getKnownFinderExceptions();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.IFinder#clone()
	 */
	@Override
	public IFinder<T> clone() {
		try {
			return (IFinder<T>) this.getClass().getConstructor(Finder.class).newInstance(this);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		this.log.warn("Cloning instance of class " + this.getClass().getSimpleName() + " failed");
		return null;
	}

	protected void cleanUpKnownExceptions() {
		Set<ISerializableWrapperRepositoryObject<? extends IRepositoryObject>> toRemove = new HashSet<>();
		if (!this.knownExceptions.containsKey(classToFind))
			return;
		for (ISerializableWrapperRepositoryObject<? extends IRepositoryObject> w : this.knownExceptions.get(classToFind).keySet()) {
			if (!w.getFile().exists())
				toRemove.add(w);
		}
		this.knownExceptions.get(classToFind).keySet().removeAll(toRemove);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.IFinder#findAndRegisterObjects()
	 */
	@Override
	@SuppressWarnings("unused")
	public void findAndRegisterObjects() throws RegisterException, InterruptedException {
		cleanUpKnownExceptions();
		Iterator<File> fileIt = getIterator();

		while (fileIt.hasNext()) {
			if (Thread.interrupted())
				throw new InterruptedException();

			File programDir = fileIt.next();

			if (checkFile(programDir)) {
				foundInLastRun = true;
				try {
					doOnFileFound(programDir);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	protected File getBaseDir() {
		return new File(this.repository.getBasePath(this.getClassToFind()));
	}

	protected abstract Iterator<File> getIterator();

	protected abstract boolean checkFile(final File file);

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.IFinder#doOnFileFound(java.io.File)
	 */
	@Override
	public abstract void doOnFileFound(final File file)
			throws RegisterException, RepositoryObjectParseException, MalformedURLException;

	public final Class<T> getClassToFind() {
		return this.classToFind;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.IFinder#notify(de.clusteval.framework.repository.
	 * RepositoryEvent)
	 */
	@Override
	public void notify(RepositoryEvent e) {
		if (e instanceof RepositoryReplaceEvent) {
			RepositoryReplaceEvent event = (RepositoryReplaceEvent) e;
			IErrorSerializableWrapper<Object> wrapper = new ErrorSerializableWrapper<>(
					((RepositoryReplaceEvent) e).getOld());
			if (this.repository.getKnownFinderExceptions().containsKey(this.classToFind)
					&& this.repository.getKnownFinderExceptions().get(this.classToFind).containsKey(wrapper))
				this.repository.getKnownFinderExceptions().get(this.classToFind).remove(wrapper);
			event.getOld().removeListener(this);
			event.getReplacement().addListener(this);
		} else if (e instanceof RepositoryRemoveEvent) {
			RepositoryRemoveEvent event = (RepositoryRemoveEvent) e;
			// 31.01.2013: removed clearing of exceptions of this object. this
			// has to happen after a repeated and successful parsing of the same
			// file (e.g. FileFinder.doOnFileFound())
			event.getRemovedObject().removeListener(this);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.IFinder#toString()
	 */
	@Override
	public String toString() {
		// return this.getClass().getSimpleName();
		return this.getName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.IFinder#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj.getClass().equals(this.getClass())))
			return false;

		Finder rf = (Finder) obj;
		return this.repository.equals(rf.getRepository());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.IFinder#hashCode()
	 */
	@Override
	public int hashCode() {
		return this.repository.hashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.IFinder#foundInLastRun()
	 */
	@Override
	public boolean foundInLastRun() {
		return this.foundInLastRun;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#getVersion()
	 */
	@Override
	public ComparableVersion getVersion() {
		return new ComparableVersion("1");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#getName()
	 */
	@Override
	public String getName() {
		return "Finder: " + this.classToFind.getSimpleName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#asSerializable()
	 */
	@Override
	public SerializableWrapperRepositoryObject<? extends IRepositoryObject> asSerializableInternal() {
		return null;
	}
}
