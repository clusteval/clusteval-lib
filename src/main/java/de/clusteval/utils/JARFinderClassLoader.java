/**
 * 
 */
package de.clusteval.utils;

import java.net.URL;
import java.net.URLClassLoader;

import de.clusteval.framework.repository.IRepositoryObject;

/**
 * @author Christian Wiwie
 *
 *
 * @since Nov 4, 2017
 */
public abstract class JARFinderClassLoader<T extends IRepositoryObject>
		extends
			URLClassLoader {

	protected JARFinder<T> parent;

	/**
	 * @param urls
	 * @param parent
	 * @param loaderParent
	 */
	public JARFinderClassLoader(JARFinder<T> parent, URL[] urls,
			ClassLoader loaderParent) {
		super(urls, loaderParent);
		this.parent = parent;
	}

	protected void handleException(Throwable e) throws ClassNotFoundException {
		if (e instanceof IExceptionWithDependent)
			throw new ClassNotFoundException(
					((IExceptionWithDependent) e).getMessage(false), e);
		else if (e.getMessage() != null)
			throw new ClassNotFoundException(e.getMessage(), e);
		else
			throw new ClassNotFoundException("", e);
	}
}
