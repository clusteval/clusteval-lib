/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.utils;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;
import de.clusteval.framework.repository.ObjectNotFoundException;
import de.clusteval.framework.repository.ObjectVersionNotFoundException;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryRemoveEvent;
import de.clusteval.framework.repository.SerializableWrapperRepositoryObject;
import de.clusteval.framework.repository.parse.Parser;
import de.clusteval.framework.repository.parse.RepositoryObjectParseException;

/**
 * @author Christian Wiwie
 * @param <T>
 * 
 */
public abstract class FileFinder<T extends IRepositoryObject>
		extends
			Finder<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7254051048449243503L;

	protected Map<ISerializableWrapperRepositoryObject<IRepositoryObject>, IRepositoryObject> errorObjects;

	/**
	 * @param repository
	 * @param classToFind
	 * @throws RegisterException
	 */
	public FileFinder(IRepository repository, Class<T> classToFind)
			throws RegisterException {
		super(repository, classToFind);
		try {
			if (!this.repository.getErrorObjects().containsKey(classToFind))
				this.repository.getErrorObjects().put(classToFind,
						new HashMap<>());
			this.errorObjects = this.repository.getErrorObjects()
					.get(classToFind);
		} catch (ObjectNotFoundException e) {
			e.printStackTrace();
		} catch (ObjectVersionNotFoundException e) {
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.Finder#doOnFileFound(java.io.File)
	 */
	@Override
	public void doOnFileFound(File file) throws RegisterException,
			RepositoryObjectParseException, MalformedURLException {
		try {
			T newObject = parseObjectFromFile(file);
			/*
			 * We want to be informed when this object changed, such that we can
			 * clear our known exceptions
			 */
			newObject.addListener(this);

			// Clear the exceptions associated with this object
			ISerializableWrapperRepositoryObject<IRepositoryObject> wrapper = new ErrorSerializableWrapper<IRepositoryObject>(
					newObject);
			if (knownExceptions.containsKey(classToFind))
				if (knownExceptions.get(classToFind).containsKey(wrapper))
					knownExceptions.get(classToFind).remove(wrapper);

			wrapper = new ErrorSerializableWrapper<T>(file, newObject.getName(),
					newObject.getVersion().toString());

			if (errorObjects.containsKey(wrapper))
				errorObjects.remove(wrapper);
		} catch (RepositoryObjectParseException e) {
			handleException(file, classToFind, e.getName(), e.getVersion(),
					(Exception) e.getCause());
			IRepositoryObject errorObject = parseObjectFromFile(file, true);
			try {
				SerializableWrapperRepositoryObject<IRepositoryObject> wrapper = new ErrorSerializableWrapper<T>(
						file, e.getName(), e.getVersion());
				errorObjects.put(wrapper, errorObject);
			} catch (Exception e2) {
				// if we cannot get name and version
				// we just ignore this file
			}
		}
	}

	/**
	 * @param file
	 * @param e
	 * @throws RegisterException
	 */
	public void handleException(final File file,
			final Class<? extends IRepositoryObject> clazz,
			final String objectName, final String objectVersion,
			final Exception e) throws RegisterException {

		SerializableWrapperRepositoryObject<IRepositoryObject> wrapper = new ErrorSerializableWrapper<>(
				file, objectName, objectVersion);

		// check if we already logged a warning for this
		boolean known = false;
		if (knownExceptions.containsKey(clazz)
				&& knownExceptions.get(clazz).containsKey(wrapper)) {
			for (Throwable other : knownExceptions.get(clazz).get(wrapper))
				if ((e.getMessage() == null && other.getMessage() == null)
						|| (e.getMessage() != null && other.getMessage() != null
								&& other.getMessage().equals(e.getMessage()))) {
					known = true;
					break;
				}
		}
		if (known)
			this.getLog()
					.debug("Could not parse " + getClassToFind().getSimpleName()
							+ " " + file + ": " + e.getMessage());
		else {
			// clear all previous exceptions, such that if they happen to
			// reoccur we print them again
			if (knownExceptions.containsKey(clazz))
				knownExceptions.get(clazz).remove(wrapper);
			else
				knownExceptions.put(clazz,
						new ConcurrentHashMap<ISerializableWrapperRepositoryObject<? extends IRepositoryObject>, List<Throwable>>());
			if (!knownExceptions.get(clazz).containsKey(wrapper))
				knownExceptions.get(clazz).put(wrapper,
						new ArrayList<Throwable>());
			knownExceptions.get(clazz).get(wrapper).add(e);
			onUnknownExceptionWarning(file, e);
		}

		/*
		 * Remove the corresponding object from the repository
		 */
		IRepositoryObject object = this.repository.getRegisteredObject(file);
		if (object != null) {
			object.unregister();
			object.notify(new RepositoryRemoveEvent(object));
		}
	}

	protected void onUnknownExceptionWarning(final File file,
			final Exception e) {
		String message;
		if (e.getMessage() != null)
			message = e.getMessage();
		else if (e.getCause() != null) {
			StringWriter writer = new StringWriter();
			e.getCause().printStackTrace(new PrintWriter(writer));
			message = System.getProperty("line.separator") + writer.toString();
		} else {
			StringWriter writer = new StringWriter();
			e.printStackTrace(new PrintWriter(writer));
			message = System.getProperty("line.separator") + writer.toString();
		}
		String[] split = message.split(System.getProperty("line.separator"));
		this.getLog()
				.warn("Could not parse "
						+ getClassToFind().getSimpleName().substring(1) + " "
						+ repository.getRelativeFilePathToRepository(file)
						+ ": " + split[0]);
		for (int i = 1; i < split.length; i++)
			this.getLog().warn("|--> " + split[i]);
	}

	protected Collection<? extends IRepositoryObject> getRegisteredObjectSet() {
		return this.repository.getCollectionStaticEntities(getClassToFind());
	}

	protected void validateRegisteredObjects() throws RegisterException {

		/*
		 * First check whether all registered objects currently in the
		 * repository do still exist
		 */
		Collection<IRepositoryObject> toRemove = new HashSet<IRepositoryObject>();
		for (IRepositoryObject object : getRegisteredObjectSet())
			if (!new File(object.getAbsolutePath()).exists()) {
				toRemove.add(object);
			}

		for (IRepositoryObject object : toRemove) {
			object.unregister();
			object.notify(new RepositoryRemoveEvent(object));
			// TODO clear exceptions that are associated with this object
			// this.knownExceptions.remove(object.getAbsolutePath());
			repository.getMavenResolver().informOnRepositoryObjectRemoved(
					this.classToFind, object.getName(), object.getVersion());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.Finder#findAndRegisterObjects()
	 */
	@Override
	public void findAndRegisterObjects()
			throws RegisterException, InterruptedException {
		validateRegisteredObjects();
		super.findAndRegisterObjects();
	}

	protected T parseObjectFromFile(final File file)
			throws RepositoryObjectParseException, RegisterException,
			MalformedURLException {
		return this.parseObjectFromFile(file, false);
	}

	protected T parseObjectFromFile(final File file,
			final boolean recoverFromExceptions)
			throws RepositoryObjectParseException, RegisterException,
			MalformedURLException {
		return Parser.parseFromFile(this.getClassToFind(), file,
				recoverFromExceptions);
	}

	/**
	 * @return the errorObjects
	 */
	public Map<ISerializableWrapperRepositoryObject<IRepositoryObject>, IRepositoryObject> getErrorObjects() {
		return errorObjects;
	}
}
