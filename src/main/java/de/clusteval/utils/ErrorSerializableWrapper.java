/**
 * 
 */
package de.clusteval.utils;

import java.io.File;

import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.framework.repository.SerializableWrapperRepositoryObject;
import de.clusteval.framework.repository.UnknownDynamicComponentException;

public class ErrorSerializableWrapper<T> extends SerializableWrapperRepositoryObject<IRepositoryObject>
		implements
			IErrorSerializableWrapper<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -266729657495643397L;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 */
	public ErrorSerializableWrapper(File absPath, String name, String version) {
		super(absPath, name, version);
	}

	public ErrorSerializableWrapper(final IRepositoryObject object) {
		super(object);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.SerializableWrapper#compareTo(de.
	 * clusteval.framework.repository.SerializableWrapper)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.IErrorSerializableWrapper#compareTo(de.clusteval.
	 * framework.repository.SerializableWrapper)
	 */
	@Override
	public int compareTo(SerializableWrapperRepositoryObject<IRepositoryObject> o) {
		return this.toString().toLowerCase().compareTo(o.toString().toLowerCase());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.SerializableWrapper#equals(java.
	 * lang.Object)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.utils.IErrorSerializableWrapper#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof SerializableWrapperRepositoryObject))
			return false;

		ISerializableWrapperRepositoryObject other = (ISerializableWrapperRepositoryObject) obj;
		return this.name.equals(other.getName()) && this.version.equals(other.getVersion());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.SerializableWrapper#hashCode()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.IErrorSerializableWrapper#hashCode()
	 */
	@Override
	public int hashCode() {
		return (this.name + ":" + this.version).hashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.SerializableWrapper#deserializeInternal
	 * (de.clusteval.framework.repository.IRepository)
	 */
	@Override
	protected IRepositoryObject deserializeInternal(IRepository repository) throws DeserializationException {
		// TODO: do we need this?
		return null;
	}
}