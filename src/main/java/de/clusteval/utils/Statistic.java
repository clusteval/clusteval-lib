/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.utils;

import java.io.File;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.Repository;
import de.clusteval.framework.repository.RepositoryObjectDynamicComponent;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;

/**
 * An abstract class representing a property of some object, that can be
 * assessed in analysis runs.
 * 
 * @author Christian Wiwie
 * 
 */
@ClassVersion(version = "1")
public abstract class Statistic extends RepositoryObjectDynamicComponent
		implements
			IStatistic {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2229604299410320650L;

	/**
	 * @param repository
	 * @param changeDate
	 * @param absPath
	 * @throws RegisterException
	 */
	public Statistic(IRepository repository, long changeDate, File absPath)
			throws RegisterException {
		super(repository, changeDate, absPath);
	}

	/**
	 * The copy constructor of statistics.
	 * 
	 * @param other
	 *            The object to clone.
	 * @throws RegisterException
	 */
	public Statistic(final Statistic other) throws RegisterException {
		super(other);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.IStatistic#clone()
	 */
	@Override
	public abstract IStatistic clone();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.IStatistic#getIdentifier()
	 */
	@Override
	public final String getIdentifier() {
		return this.getClass().getSimpleName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.IStatistic#parseFromString(java.lang.String)
	 */
	@Override
	public abstract void parseFromString(final String contents);

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.IStatistic#getResultAsString()
	 */
	@Override
	public abstract String getResultAsString();

	/**
	 * @return The context of this statistic. A statistic can only be assessed
	 *         for runs of the right context.
	 */
	// TODO
	// public abstract Context getContext();

	@Override
	public ComparableVersion getVersion() {
		try {
			return Repository.getVersionOfObjectDynamicClass(this);
		} catch (Exception e) {
			// cannot happen because we loaded the class before;
			e.printStackTrace();
			return new ComparableVersion("1");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#getName()
	 */
	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObjectDynamicComponent#
	 * asSerializable()
	 */
	@Override
	public ISerializableStatistic<? extends IStatistic> asSerializable()
			throws RepositoryObjectSerializationException {
		return (ISerializableStatistic<? extends IStatistic>) super.asSerializable();
	}

	@Override
	public abstract ISerializableStatistic<? extends IStatistic> asSerializableInternal();
}
