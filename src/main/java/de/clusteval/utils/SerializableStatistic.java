/**
 * 
 */
package de.clusteval.utils;

import java.io.File;

import de.clusteval.framework.repository.IRepositoryObjectDynamicComponent;
import de.clusteval.framework.repository.SerializableWrapperDynamicComponent;

public abstract class SerializableStatistic<S extends IStatistic>
		extends
			SerializableWrapperDynamicComponent<S>
		implements
			ISerializableStatistic<S> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8412838025265811152L;

	/**
	 * @param type
	 * @param absPath
	 * @param name
	 * @param version
	 */
	public SerializableStatistic(
			Class<? extends IRepositoryObjectDynamicComponent> type,
			File absPath, String name, String version, final String alias) {
		super(type, absPath, name, version, alias);
	}

	/**
	 * @param wrappedComponent
	 */
	public SerializableStatistic(
			Class<? extends IRepositoryObjectDynamicComponent> type,
			S wrappedComponent) {
		super(type, wrappedComponent);
	}
}