/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.cluster;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * A cluster item can be part of several clusters.
 * 
 * @author Christian Wiwie
 */
public class ClusterItem implements IClusterItem, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8281308375338033286L;

	/**
	 * The id of the item, should be unique.
	 */
	protected String id;

	/**
	 * The clusters this item is contained in together with the corresponding
	 * fuzzy coefficients.
	 */
//	protected Map<ICluster, Float> fuzzyClusters;

	/**
	 * Instantiates a new cluster item with a certain id.
	 * 
	 * @param id
	 *            The id of the new cluster item.
	 */
	public ClusterItem(final String id) {
		super();

		this.id = id;
//		this.fuzzyClusters = new HashMap<ICluster, Float>();
	}

	/**
	 * The copy constructor of cluster items.
	 * 
	 * @param other
	 *            The object to clone.
	 */
	public ClusterItem(final IClusterItem other) {
		super();

		this.id = other.getId();
//		this.fuzzyClusters = cloneFuzzyClusters(other.getFuzzyClusters());
	}

	@Override
	public ClusterItem clone() {
		return new ClusterItem(this);
	}

	protected static Map<ICluster, Float> cloneFuzzyClusters(
			Map<ICluster, Float> fuzzyClusters) {
		final Map<ICluster, Float> result = new HashMap<ICluster, Float>();

		for (Map.Entry<ICluster, Float> entry : fuzzyClusters.entrySet()) {
			result.put(entry.getKey().clone(), new Float(entry.getValue()));
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof ClusterItem))
			return false;

		ClusterItem other = (ClusterItem) obj;

		return this.id.equals(other.id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return (this.id).hashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.IClusterItem#getId()
	 */
	@Override
	public String getId() {
		return this.id;
	}

//	/*
//	 * (non-Javadoc)
//	 * 
//	 * @see de.clusteval.cluster.IClusterItem#getFuzzyClusters()
//	 */
//	@Override
//	public Map<ICluster, Float> getFuzzyClusters() {
//		return this.fuzzyClusters;
//	}

//	/*
//	 * (non-Javadoc)
//	 * 
//	 * @see
//	 * de.clusteval.cluster.IClusterItem#addFuzzyCluster(de.clusteval.cluster.
//	 * Cluster, float)
//	 */
//	@Override
//	public boolean addFuzzyCluster(final ICluster cluster, float fuzzy) {
//		return this.fuzzyClusters.put(cluster, fuzzy) != null;
//	}
//
//	/*
//	 * (non-Javadoc)
//	 * 
//	 * @see
//	 * de.clusteval.cluster.IClusterItem#removeFuzzyCluster(de.clusteval.cluster
//	 * .Cluster)
//	 */
//	@Override
//	public boolean removeFuzzyCluster(final ICluster cluster) {
//		return this.fuzzyClusters.remove(cluster) != null;
//	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.IClusterItem#toString()
	 */
	@Override
	public String toString() {
		return this.id;
	}
}
