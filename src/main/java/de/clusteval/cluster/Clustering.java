/**
 * 
 */
package de.clusteval.cluster;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.cluster.quality.ClusteringQualityMeasureValue;
import de.clusteval.cluster.quality.ClusteringQualitySet;
import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.cluster.quality.IClusteringQualityMeasureValue;
import de.clusteval.cluster.quality.IClusteringQualitySet;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.format.InvalidDataSetFormatVersionException;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.goldstandard.InvalidGoldStandardFormatException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RepositoryObject;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.program.ParameterSet;
import de.clusteval.run.runresult.format.IRunResultFormat;
import de.clusteval.run.runresult.format.UnknownRunResultFormatException;
import de.clusteval.utils.DynamicComponentInitializationException;
import dk.sdu.imada.compbio.utils.Pair;
import dk.sdu.imada.compbio.utils.parse.TextFileParser;

/**
 * @author Christian Wiwie
 *
 *
 * @since Nov 6, 2017
 */
public class Clustering extends RepositoryObject implements IClustering {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3727410152654538756L;
	/**
	 * The fuzzy size of this clustering is the sum of all fuzzy coefficients of
	 * any item contained in any cluster.
	 */
	protected float fuzzySize;
	/**
	 * The clusters contained in this clustering.
	 */
	protected Set<ICluster> clusters;
	/**
	 * Used to get clusters in O(1) with their id.
	 */
	protected Map<String, ICluster> clusterIdToCluster;
	/**
	 * A map from cluster item to cluster and fuzzy coefficient. This serves for
	 * fast access and performance purposes only.
	 */
	protected Map<IClusterItem, Map<ICluster, Float>> itemToCluster;
	/**
	 * Used to get cluster items in O(1) with their id.
	 */
	protected Map<String, IClusterItem> itemIdToItem;
	/**
	 * If the qualities of this clustering were set using the method
	 * {@link #setQualities(ClusteringQualitySet)}, they are stored in this
	 * attribute.
	 */
	protected IClusteringQualitySet qualities;

	protected static Map<String, ICluster> cloneClusterIdToCluster(
			Map<String, ICluster> clusterIdToCluster) {
		final Map<String, ICluster> result = new HashMap<String, ICluster>();

		for (Map.Entry<String, ICluster> entry : clusterIdToCluster
				.entrySet()) {
			result.put(entry.getKey(), entry.getValue().clone());
		}

		return result;
	}

	protected static Map<String, IClusterItem> cloneItemIdToItem(
			Map<String, IClusterItem> itemIdToItem) {
		final Map<String, IClusterItem> result = new HashMap<String, IClusterItem>();

		for (Map.Entry<String, IClusterItem> entry : itemIdToItem.entrySet()) {
			result.put(entry.getKey(), entry.getValue().clone());
		}

		return result;
	}

	@Override
	public IClustering clone() {
		Clustering result = new Clustering(this.repository, this.changeDate,
				this.absPath);
		final Map<ICluster, ICluster> clusters = new HashMap<ICluster, ICluster>();
		final Map<IClusterItem, IClusterItem> items = new HashMap<IClusterItem, IClusterItem>();

		for (ICluster cl : this.clusters) {
			ICluster newCluster = new Cluster(cl.getId());
			clusters.put(cl, newCluster);

			for (Map.Entry<IClusterItem, Float> e : cl.getFuzzyItems()
					.entrySet()) {
				if (!items.containsKey(e.getKey())) {
					IClusterItem newItem = new ClusterItem(e.getKey().getId());
					items.put(e.getKey(), newItem);
				}
				IClusterItem item = items.get(e.getKey());
				newCluster.add(item, e.getValue());
			}

			result.addCluster(newCluster);
		}
		return result;
	}

	protected static Map<IClusterItem, Map<ICluster, Float>> cloneItemToClusters(
			Map<IClusterItem, Map<ICluster, Float>> itemToCluster2) {
		final Map<IClusterItem, Map<ICluster, Float>> result = new HashMap<IClusterItem, Map<ICluster, Float>>();

		for (Map.Entry<IClusterItem, Map<ICluster, Float>> entry : itemToCluster2
				.entrySet()) {
			final Map<ICluster, Float> newMap = new HashMap<ICluster, Float>();

			for (Map.Entry<ICluster, Float> entry2 : entry.getValue()
					.entrySet()) {
				newMap.put(entry2.getKey().clone(),
						new Float(entry2.getValue()));
			}

			result.put(entry.getKey().clone(), newMap);
		}

		return result;
	}

	protected static Set<ICluster> cloneClusters(Set<ICluster> clusters) {
		final Set<ICluster> result = new HashSet<ICluster>();
		final Set<IClusterItem> items = new HashSet<IClusterItem>();

		for (ICluster cl : clusters) {
			ICluster newCluster = new Cluster(cl.getId());

			for (Map.Entry<IClusterItem, Float> e : cl.getFuzzyItems()
					.entrySet()) {
				if (!items.contains(e.getKey())) {
					items.add(new ClusterItem(e.getKey().getId()));
				}
			}

			result.add(newCluster);
		}

		return result;
	}

	/**
	 * This method parses clusterings together with the corresponding parameter
	 * sets from a file.
	 * 
	 * @param repository
	 * 
	 * @param absFilePath
	 *            The absolute path to the input file.
	 * @param format
	 * @param parseQualities
	 *            True, if the qualities of the clusterings should also be
	 *            parsed. Those will be taken from .qual-files.
	 * @return A map containing parameter sets and corresponding clusterings.
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws DynamicComponentInitializationException
	 * @throws UnknownRunResultFormatException
	 */
	public static Pair<ParameterSet, IFileBackedClustering> parseFromFile(
			final IRepository repository, final File absFilePath,
			final IRunResultFormat format, final boolean parseQualities)
			throws IOException, UnknownRunResultFormatException,
			DynamicComponentInitializationException {
		ClusteringParser parser = new ClusteringParser(repository,
				absFilePath.getAbsolutePath(), parseQualities);
		parser.process();

		ParameterSet parameterSet = parser.getClusterings().getFirst();
		Clustering clustering = parser.getClusterings().getSecond();

		return Pair.getPair(parameterSet,
				new FileBackedClustering(clustering, format));
	}

	/**
	 * Convert an integer array holding cluster ids for every object to a fuzzy
	 * coefficient matrix.
	 * 
	 * @param clusterIds
	 *            The cluster ids of the objects.
	 * @return Fuzzy coefficient matrix. [i][j] holds the fuzzy coefficient for
	 *         object i and cluster j.
	 */
	public static float[][] clusterIdsToFuzzyCoeff(final int[] clusterIds) {
		Map<Integer, Integer> clusterPos = new HashMap<Integer, Integer>();
		for (int id : clusterIds)
			if (!(clusterPos.containsKey(id)))
				clusterPos.put(id, clusterPos.size());

		int numberClusters = clusterPos.keySet().size();

		float[][] fuzzy = new float[clusterIds.length][numberClusters];
		for (int i = 0; i < clusterIds.length; i++)
			fuzzy[i][clusterPos.get(clusterIds[i])] = 1.0f;
		return fuzzy;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Clustering))
			return false;
		Clustering other = (Clustering) obj;
		return (!this.absPath.equals("") && !other.absPath.equals("")
				&& this.absPath.equals(other.absPath))
				|| this.clusters.equals(other.clusters);
	}

	@Override
	public int hashCode() {
		// return this.clusters.hashCode();
		return this.absPath.hashCode();
	}

	@Override
	public Map<ICluster, Float> getClusterForItem(IClusterItem item) {
		return this.itemToCluster.get(item);
	}

	/**
	 * @param id
	 *            The id of the cluster.
	 * @return The cluster with the given id.
	 */
	public ICluster getClusterWithId(final String id) {
		return this.clusterIdToCluster.get(id);
	}

	@Override
	public Set<ICluster> getClusters() {
		return this.clusters;
	}

	/**
	 * @param qualitySet
	 *            Set the qualities of this clustering.
	 */
	public void setQualities(final ClusteringQualitySet qualitySet) {
		this.qualities = qualitySet;
	}

	/**
	 * @return Returns the qualities of this clustering.
	 * @see Clustering#qualities
	 */
	@Override
	public IClusteringQualitySet getQualities() {
		return this.qualities;
	}

	@Override
	public Set<IClusterItem> getClusterItems() {
		return this.itemToCluster.keySet();
	}

	@Override
	public IClusterItem getClusterItemWithId(final String id) {
		return this.itemIdToItem.get(id);
	}

	@Override
	public boolean addCluster(final ICluster cluster) {
		boolean b = this.clusters.add(cluster);
		if (b) {
			this.clusterIdToCluster.put(cluster.getId(), cluster);
			this.fuzzySize += cluster.fuzzySize();
			Map<IClusterItem, Float> items = cluster.getFuzzyItems();
			for (IClusterItem item : items.keySet()) {
				if (!this.itemToCluster.containsKey(item))
					this.itemToCluster.put(item,
							new HashMap<ICluster, Float>());
				this.itemToCluster.get(item).put(cluster, items.get(item));

				if (!itemIdToItem.containsKey(item.getId()))
					itemIdToItem.put(item.getId(), item);
			}
		}
		return b;
	}

	@Override
	public boolean removeClusterItem(final IClusterItem item) {
		Map<ICluster, Float> fuzzyClusters = this.itemToCluster.remove(item);
		boolean result = false;
		for (ICluster cl : fuzzyClusters.keySet()) {
			result = cl.remove(item);
			this.fuzzySize -= fuzzyClusters.get(cl);
		}
		return result;
	}

	@Override
	public boolean removeClusterItem(final IClusterItem item,
			final ICluster cluster) {
		if (!this.itemToCluster.containsKey(item))
			return false;

		float fuzzy = this.itemToCluster.get(item).remove(cluster);
		boolean result = cluster.remove(item);
		this.fuzzySize -= fuzzy;

		if (this.itemToCluster.get(item).size() == 0) {
			this.itemIdToItem.remove(item.getId());
			this.itemToCluster.remove(item);
		}

		return result;
	}

	/**
	 * @return The fuzzy size of this clustering.
	 * @see #fuzzySize
	 */
	@Override
	public float fuzzySize() {
		return this.fuzzySize;
	}

	@Override
	public int size() {
		return this.itemToCluster.keySet().size();
	}

	@Override
	public Iterator<ICluster> iterator() {
		return this.clusters.iterator();
	}

	@Override
	public String toString() {
		return "[Clustering: " + clusters.toString() + "]";
	}

	@Override
	public String toFormattedString() {
		StringBuilder sb = new StringBuilder();
		for (ICluster cluster : this.clusters) {
			for (Map.Entry<IClusterItem, Float> entry : cluster.getFuzzyItems()
					.entrySet()) {
				if (entry.getValue() > 0f) {
					sb.append(entry.getKey());
					sb.append(":");
					sb.append(entry.getValue());
					sb.append(",");
				}
			}
			if (sb.length() > 0)
				sb.deleteCharAt(sb.length() - 1);
			sb.append(";");
		}
		if (sb.length() > 0)
			sb.deleteCharAt(sb.length() - 1);
		return sb.toString();
	}

	@Override
	public void writeFormattedString(final Writer writer) throws IOException {
		List<ICluster> clusterList = new ArrayList<ICluster>(this.clusters);
		for (int c = 0; c < clusterList.size(); c++) {
			ICluster cluster = clusterList.get(c);
			List<Map.Entry<IClusterItem, Float>> entrySet = new ArrayList<Map.Entry<IClusterItem, Float>>(
					cluster.getFuzzyItems().entrySet());
			for (int i = 0; i < entrySet.size(); i++) {
				Map.Entry<IClusterItem, Float> entry = entrySet.get(i);
				if (entry.getValue() > 0f) {
					writer.append(entry.getKey().toString());
					writer.append(":");
					writer.append(entry.getValue().toString());
					if (i < entrySet.size() - 1)
						writer.append(",");
				}
			}
			if (c < clusterList.size() - 1)
				writer.append(";");
		}
		writer.flush();
	}

	@Override
	public Clustering toHardClustering() {
		Clustering result = new Clustering(this.repository, this.changeDate,
				this.absPath);

		// assign each item to the cluster with the maximal fuzzy
		// coefficient.
		Map<String, ICluster> newClusters = new HashMap<String, ICluster>();
		Set<IClusterItem> items = this.getClusterItems();
		for (IClusterItem item : items) {
			ICluster cl = null;
			double maxFuzzyCoeff = -0.1;

			for (Map.Entry<ICluster, Float> p : this.getClusterForItem(item)
					.entrySet()) {
				if (p.getValue() > maxFuzzyCoeff) {
					cl = p.getKey();
					maxFuzzyCoeff = p.getValue();
				}
			}

			if (cl == null)
				continue;

			if (!newClusters.containsKey(cl.getId()))
				newClusters.put(cl.getId(), new Cluster(cl.getId()));
			newClusters.get(cl.getId()).add(new ClusterItem(item.getId()),
					1.0f);
		}
		for (ICluster cl : newClusters.values())
			result.addCluster(cl);

		return result;
	}

	/**
	 * The passed clustering is assumed to be a hard (non-fuzzy) clustering.
	 * 
	 * @param objectIds
	 *            The ids of the cluster items.
	 * @param clusterIds
	 *            Position i holds the cluster id of cluster item i.
	 * @return A clustering wrapper object.
	 */
	public static Clustering parseFromIntArray(final IRepository repository,
			final File absPath, final String[] objectIds,
			final int[] clusterIds) {
		return parseFromFuzzyCoeffMatrix(repository, absPath, objectIds,
				clusterIdsToFuzzyCoeff(clusterIds));
	}

	@Override
	public boolean isInMemory() {
		return this.fuzzySize > 0.0;
	}

	protected void initAttributes() {
		this.clusters = new HashSet<ICluster>();
		this.clusterIdToCluster = new HashMap<String, ICluster>();
		this.itemToCluster = new HashMap<IClusterItem, Map<ICluster, Float>>();
		this.itemIdToItem = new HashMap<String, IClusterItem>();
	}

	/**
	 * Loads this clustering into memory (clusters + cluster items);
	 * 
	 * @throws ClusteringParseException
	 */
	@Override
	public void loadIntoMemory() throws ClusteringParseException {
		initAttributes();
		final IClustering result = this;

		TextFileParser p;
		try {
			p = new TextFileParser(this.getAbsolutePath(), new int[]{0},
					new int[]{1}) {

				@Override
				protected void processLine(String[] key, String[] value) {
					if (currentLine == 0)
						return;
					String clusteringString = value[0];
					String[] clusters = clusteringString.split(";");
					int no = 1;
					for (String cluster : clusters) {
						ICluster c = new Cluster((no++ + "").intern());
						String[] items = cluster.split(",");
						for (String item : items) {
							String[] itemSplit = item.split(":");
							String id = itemSplit[0].intern();
							IClusterItem cItem = result
									.getClusterItemWithId(id);
							if (cItem == null)
								cItem = new ClusterItem(id);
							c.add(cItem, Float.valueOf(
									Float.valueOf(itemSplit[1]).floatValue()));
						}
						result.addCluster(c);
					}
				}
			};
			p.process();
		} catch (IOException e) {
			throw new ClusteringParseException(
					"The clustering " + this.getAbsolutePath()
							+ " could not be load into memory");
		}
	}

	/**
	 * Unloads this clustering from memory.
	 */
	@Override
	public void unloadFromMemory() {
		if (this.clusterIdToCluster != null) {
			this.clusterIdToCluster.clear();
			this.clusterIdToCluster = null;
		}
		if (this.clusters != null) {
			this.clusters.clear();
			this.clusters = null;
		}
		if (this.itemIdToItem != null) {
			this.itemIdToItem.clear();
			this.itemIdToItem = null;
		}
		if (this.itemToCluster != null) {
			this.itemToCluster.clear();
			this.itemToCluster = null;
		}
	}

	/**
	 * @param objectIds
	 *            The ids of the cluster items.
	 * @param fuzzyCoeffs
	 *            Position [i,j] is the fuzzy coefficient of object i and
	 *            cluster j.
	 * @return A clustering wrapper object.
	 */
	public static Clustering parseFromFuzzyCoeffMatrix(
			final IRepository repository, final File absPath,
			final String[] objectIds, final float[][] fuzzyCoeffs) {
		if (objectIds.length != fuzzyCoeffs.length)
			throw new IllegalArgumentException(
					"The number of object ids and cluster ids needs to be the same.");
		Map<String, ICluster> clusters = new HashMap<String, ICluster>();

		for (int i = 0; i < fuzzyCoeffs.length; i++) {
			IClusterItem item = new ClusterItem(objectIds[i]);
			for (int j = 0; j < fuzzyCoeffs[i].length; j++) {
				final String clusterId = j + "";
				ICluster cluster = clusters.get(clusterId);
				if (cluster == null) {
					cluster = new Cluster(clusterId);
					clusters.put(clusterId, cluster);
				}

				cluster.add(item, fuzzyCoeffs[i][j]);
			}
		}
		Clustering clustering = new Clustering(repository,
				System.currentTimeMillis(), absPath);
		for (ICluster cl : clusters.values())
			clustering.addCluster(cl);
		return clustering;
	}

	/**
	 * @param repository
	 * @param changeDate
	 * @param absPath
	 */
	public Clustering(IRepository repository, long changeDate, File absPath) {
		super(repository, changeDate, absPath);
		initAttributes();
	}

	/**
	 * @param other
	 */
	public Clustering(RepositoryObject other) {
		super(other);
	}

	/**
	 * Assess quality.
	 * 
	 * @param dataConfig
	 * 
	 * @param qualityMeasures
	 *            the quality measures
	 * @return A set of qualities for every quality measure that was passed in
	 *         the list.
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws UnknownDataSetFormatException
	 * @throws InvalidDataSetFormatVersionException
	 * @throws ClusteringNotInitializedException
	 */
	@Override
	public IClusteringQualitySet assessQuality(final IDataConfig dataConfig,
			final List<IClusteringQualityMeasure> qualityMeasures)
			throws InvalidGoldStandardFormatException, IOException,
			UnknownDataSetFormatException, InvalidDataSetFormatVersionException,
			ClusteringNotInitializedException {
		if (!isInMemory())
			throw new ClusteringNotInitializedException();
		// added: 30.07.2014: assume all ids of the dataset missing in the
		// clustering to be singletons
		for (String id : dataConfig.getDatasetConfig().getDataSet()
				.getInStandardFormat().getIds()) {
			// if the object with this id is not in the clustering, add a
			// singleton cluster with this object
			if (!this.itemIdToItem.containsKey(id)) {
				ICluster cluster = new Cluster(
						this.clusterIdToCluster.size() + "");
				cluster.add(new ClusterItem(id), 1.0f);
				this.addCluster(cluster);
			}
		}

		// TODO: 20.08.2012 ensure, that this runresult is in standard format
		final ClusteringQualitySet resultSet = new ClusteringQualitySet();
		for (IClusteringQualityMeasure qualityMeasure : qualityMeasures) {
			// do not calculate, when there is no goldstandard
			if (qualityMeasure.requiresGoldstandard()
					&& !dataConfig.hasGoldStandardConfig())
				continue;
			IClusteringQualityMeasureValue quality;
			try {
				IClustering goldStandard = null;
				if (dataConfig.hasGoldStandardConfig())
					goldStandard = dataConfig.getGoldstandardConfig()
							.getGoldstandard().getClustering();
				// convert the clustering to a hard clustering if the measure
				// does not support fuzzy clusterings
				IClustering cl = this;
				if (!qualityMeasure.supportsFuzzyClusterings())
					cl = this.toHardClustering();

				quality = qualityMeasure.getQualityOfClustering(cl,
						goldStandard, dataConfig);
				// if (dataConfig.hasGoldStandardConfig())
				// dataConfig.getGoldstandardConfig().getGoldstandard()
				// .unloadFromMemory();
				// we rethrow some exceptions, since they mean, that we
				// cannot calculate ANY quality measures for this data
			} catch (InvalidGoldStandardFormatException e) {
				throw e;
			} catch (IOException e) {
				throw e;
			} catch (UnknownDataSetFormatException e) {
				throw e;
			} catch (InvalidDataSetFormatVersionException e) {
				throw e;
			} catch (Exception e) {
				// all the remaining exceptions are catched, because they
				// mean, that the quality measure calculation is flawed
				quality = ClusteringQualityMeasureValue
						.getForDouble(Double.NaN);
			}
			resultSet.put(qualityMeasure, quality);
		}
		return resultSet;
	}

	@Override
	public ComparableVersion getVersion() {
		return new ComparableVersion("1");
	}

	@Override
	public String getName() {
		return this.getAbsolutePath();
	}

	@Override
	public SerializableClustering asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableClustering) super.asSerializable();
	}

	@Override
	public SerializableClustering asSerializableInternal()
			throws RepositoryObjectSerializationException {
		return new SerializableClustering(this);
	}

	/**
	 * @return the clusterIdToCluster
	 */
	@Override
	public Map<String, ICluster> getClusterIdToCluster() {
		return clusterIdToCluster;
	}

}