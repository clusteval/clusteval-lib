/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.cluster;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * A cluster is part of a clustering and contains several item.
 * 
 * @author Christian Wiwie
 */
public class Cluster implements ICluster, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7934175975730492862L;

	/**
	 * The id of the cluster should be unique per clustering
	 */
	protected String id;

	/**
	 * The items contained in this cluster. Since we support fuzzy clusters, for
	 * every item we also have to store the fuzzy coefficient.
	 */
	protected Map<IClusterItem, Float> fuzzyItems;

	/**
	 * The (fuzzy) size of this cluster is the sum of the fuzzy coefficients of
	 * all items contained in this cluster.
	 */
	protected float fuzzySize;

	/**
	 * Instantiates a new cluster with a given id.
	 * 
	 * @param id
	 *            The id of the cluster.
	 */
	public Cluster(final String id) {
		super();
		this.id = id;
		this.fuzzyItems = new HashMap<IClusterItem, Float>();
	}

	/**
	 * The copy constructor of clusters.
	 * 
	 * @param other
	 *            The object to clone.
	 */
	public Cluster(final Cluster other) {
		super();
		this.id = other.id;
		this.fuzzyItems = cloneFuzzyItems(other.fuzzyItems);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Cluster clone() {
		return new Cluster(this);
	}

	protected static Map<IClusterItem, Float> cloneFuzzyItems(
			Map<IClusterItem, Float> fuzzyItems) {
		final Map<IClusterItem, Float> result = new HashMap<IClusterItem, Float>();

		for (Map.Entry<IClusterItem, Float> entry : fuzzyItems.entrySet()) {
			result.put(entry.getKey().clone(), new Float(entry.getValue()));
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.ICluster#getId()
	 */
	@Override
	public String getId() {
		return this.id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.cluster.ICluster#contains(de.clusteval.cluster.ClusterItem)
	 */
	@Override
	public boolean contains(IClusterItem item) {
		return this.fuzzyItems.containsKey(item);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.ICluster#getFuzzyItems()
	 */
	@Override
	public Map<IClusterItem, Float> getFuzzyItems() {
		return this.fuzzyItems;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Cluster))
			return false;

		Cluster other = (Cluster) o;
		// TODO: changed 2016/05/17: possible performance degeneration with
		// really large clusterings
		// return fuzzyItems.equals(other.fuzzyItems);
		return id.equals(other.id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		// return (this.fuzzyItems).hashCode();
		return this.id.hashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.ICluster#fuzzySize()
	 */
	@Override
	public float fuzzySize() {
		return this.fuzzySize;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.ICluster#add(de.clusteval.cluster.ClusterItem,
	 * float)
	 */
	// TODO: check that fuzzy coefficients add at most to 1
	@Override
	public boolean add(IClusterItem item, float fuzzy) {
		if (fuzzy == 0.0)
			return true;
		this.fuzzyItems.put(item, fuzzy);
		this.fuzzySize += fuzzy;
//		item.addFuzzyCluster(this, fuzzy);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.cluster.ICluster#remove(de.clusteval.cluster.ClusterItem)
	 */
	@Override
	public boolean remove(IClusterItem item) {
//		item.removeFuzzyCluster(this);
		float fuzzy = this.fuzzyItems.remove(item);
		this.fuzzySize -= fuzzy;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.ICluster#size()
	 */
	@Override
	public int size() {
		return this.fuzzyItems.keySet().size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.ICluster#toString()
	 */
	@Override
	public String toString() {
		return id + ": " + fuzzyItems.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.ICluster#iterator()
	 */
	@Override
	public Iterator<IClusterItem> iterator() {
		// TODO: not fuzzy so far
		return this.fuzzyItems.keySet().iterator();
	}
}
