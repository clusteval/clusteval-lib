/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.cluster.paramOptimization;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.maven.artifact.versioning.ComparableVersion;
import org.slf4j.LoggerFactory;

import de.clusteval.cluster.quality.IClusteringQualitySet;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.Repository;
import de.clusteval.framework.repository.RepositoryObjectDynamicComponent;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.parse.Parser;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.program.ParameterSet;
import de.clusteval.program.ProgramParameter;
import de.clusteval.run.IParameterOptimizationRun;
import de.clusteval.run.IRun;
import de.clusteval.run.runnable.ExecutionRunRunnable;
import de.clusteval.run.runresult.IParameterOptimizationResult;
import de.clusteval.run.runresult.ParameterOptimizationResult;
import de.clusteval.run.runresult.RunResultParseException;
import de.clusteval.utils.ClassVersion;
import de.clusteval.utils.ClassVersionRequirement;
import de.clusteval.utils.ClustEvalAlias;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.DynamicComponentVersion;
import de.clusteval.utils.InternalAttributeException;
import dk.sdu.imada.compbio.utils.Pair;

//@formatter:off
/**
 * A parameter optimization method determines how a parameter optimization run
 * is executed.
 * 
 * <p>
 * One parameter optimization method is created for every pair of program and
 * data configuration of the run in {@link Parser#parseFromFile(File)}, that
 * means for every run runnable one method. The method objects are instantiated
 * as soon as the run is parsed from the filesystem. That means, when the same
 * run is executed several times, the same method object is used.
 * 
 * <p>
 * The method determines the following aspects:
 * 
 * - <b>the number of iterations of the optimization process</b>
 * - <b>the parameter sets evaluated</b>
 * - <b>the handling of diverging iterations</b>
 * - <b>the storage of the iteration results</b>
 * 
 * # Basic Usage
 * 
 * The basic usage of this class is as follows:
 * 
 * - Instantiate an object of the parameter optimization method
 * - Invoke {@link #reset(File)} and tell the method to initialize itself. Results will be written to the passed file.
 * - As long as {@link #hasNext()} returns true, there are more iterations to evaluate
 * - Use {@link #next()} to get the next parameter set.
 * - Pass the assessed qualities of {@link ExecutionRunRunnable#assessQualities} to {@link #giveQualityFeedback(long, IClusteringQualitySet)}.
 * - At the end use {@link #getResult()} to get the results of the iterations
 * 
 * # Writing Custom Parameter Optimization Methods
 * 
 * A parameter optimization method `MyParameterOptimizationMethod` can be added to ClustEval by
 * 
 * 
 * 1. extending the {@link ParameterOptimizationMethod} class with your own class `MyParameterOptimizationMethod`.
 *     - Your class has to be decorated with the following annotations:
 *         - {@link DynamicComponentVersion}: Any component you add to ClustEval needs to have a version. This is needed to ensure, that results are fully reproducible by running runs using exactly the same versions as previously
 *         - {@link ClassVersionRequirement}: Any component you add to ClustEval needs to specify, which dependencies it has on the ClustEval interfaces. This information is used to ensure compatibility between the version of your component and the used ClustEval version. Whenever core interfaces in the ClustEval API change, their interface version is incremented. By using this annotation it can detect, which components are still compatible.
 *         - {@link ClustEvalAlias}: You need to provide a readable alias for your component. This alias is used when results and components should be presented in a accessible way.
 *         - [Optional] {@link LoadableClassParentAnnotation}: If your component depends on other ClustEval components being installed and loaded when your component is loaded, it needs to be decorated with this annotation. For instance, this is needed if your class is a subclass of another component.
 *     - You have to provide your own implementations for the following methods, otherwise the framework will not be able to load your component:
 *         * {@link #ParameterOptimizationMethod(IRepository, long, File, IParameterOptimizationRun, IProgramConfig, IDataConfig, List, int, boolean)}: The constructor for your parameter optimization method. This constructor has to be implemented and public, otherwise the framework will not be able to load your parameter optimization method.
 *         * {@link #ParameterOptimizationMethod(ParameterOptimizationMethod)}: The copy constructor for your parameter optimization method. This constructor has to be implemented and public, otherwise the framework will not be able to load your parameter optimization method.
 *         * {@link #getCompatibleDataSetFormatBaseClasses()}: A list of dataset formats, this parameter optimization method can be used for. If the list is empty, all dataset formats are assumed to be compatible.
 *         * {@link #getCompatibleProgramNames()}: A list of names of all programs that are compatible to this parameter optimization method. If the list is empty, all programs are assumed to be compatible. 
 *         * {@link #hasNext()}: This method indicates, whether their is another parameter set to evaluate. This method must not change the current parameter set, as it may be invoked several times before next() is invoked. 
 *         * {@link #getNextParameterSet(ParameterSet)}: Returns the next parameter set to evaluate. This method may change the internal status of the parameter optimization method, in that it stores the newly determined and returned parameter set as the current parameter set. 
 *         * {@link #getTotalIterationCount()}: This is the total iteration count this parameter optimization method will perform. The returned value might not correspond to the expected value, when the method is instantiated. Therefore always use the return value of this method, when trying to determine the finished percentage of the parameter optimization process.
 * 2. Creating a jar file named `MyParameterOptimizationMethod.jar` containing the `MyParameterOptimizationMethod.class` compiled on your machine in the correct folder structure corresponding to the packages: `de/clusteval/cluster/paramOptimization/MyParameterOptimizationMethod.class`
 * 3. Putting the `MyParameterOptimizationMethod.jar` into the parameter optimization methods folder of the repository: `<REPOSITORY ROOT>/supp/clustering/paramOptimization`
 *
 * 
 * The backend server will recognize and try to load the new parameter optimization method automatically the next time, the {@link ParameterOptimizationMethodFinderThread} checks the filesystem.
 * 
 * @author Christian Wiwie
 * 
 * 
 *         <dl>
 *         <dt>2.1</dt>
 *         <dd>Using compbio utils instead of Wiutils</dd>
 *         <dt>&#60; 2.1</dt>
 *         <dd>...</dd>
 *         </dl>
 * 
 */
//@formatter:on
@ClassVersion(version = "2.1")
public abstract class ParameterOptimizationMethod
		extends
			RepositoryObjectDynamicComponent
		implements
			IParameterOptimizationMethod {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2679149079255561302L;

	/**
	 * A helper method for cloning a list of optimization methods.
	 * 
	 * @param optimizationMethods
	 *            The optimization methods to clone.
	 * @return The list of cloned optimization methods.
	 */
	public static List<IParameterOptimizationMethod> cloneOptimizationMethods(
			List<IParameterOptimizationMethod> optimizationMethods) {
		List<IParameterOptimizationMethod> result = new ArrayList<IParameterOptimizationMethod>();

		for (IParameterOptimizationMethod method : optimizationMethods)
			result.add(method.clone());
		return result;
	}

	/**
	 * The run this method belongs to.
	 */
	protected IParameterOptimizationRun run;

	/**
	 * The program configuration this method was created for.
	 */
	protected IProgramConfig programConfig;

	/**
	 * The data configuration this method was created for.
	 */
	protected IDataConfig dataConfig;

	/**
	 * The number of iterations that has been performed so far.
	 */
	protected int currentCount;

	/**
	 * The number of iterations for which qualities have been returned.
	 */
	protected int finishedCount;

	/**
	 * The number of iterations that should be performed.
	 * 
	 * <p>
	 * However, this might not be exactly the number that is performed by the
	 * method and these numbers might be readjusted by the method after the
	 * constructor call. This is due to the fact, that every method handles the
	 * distribution of the iterations itself.
	 * 
	 * <p>
	 * The ordering of this list is assumed to be the same as the ordering of
	 * {@link #params}.
	 */
	protected int totalIterationCount;

	/**
	 * The parameters of the program encapsulated by the program configuration
	 * that are to be optimized.
	 */
	protected List<IProgramParameter<?>> params;

	/**
	 * This object holds the results that are calculated throughout execution of
	 * the parameter optimization run.
	 */
	private IParameterOptimizationResult result;

	/**
	 * This boolean indicates, whether the run is a resumption of a previous run
	 * execution or a completely new execution.
	 */
	protected boolean isResume;

	/**
	 * @param repository
	 *            The repository this object is registered in.
	 * @param changeDate
	 *            The changedate of this object can be used for identification
	 *            and equality checks of objects.
	 * @param absPath
	 *            The absolute path of this object is used for identification
	 *            and equality checks of objects.
	 * @param run
	 *            The run this method belongs to.
	 * @param programConfig
	 *            The program configuration this method was created for.
	 * @param dataConfig
	 *            The data configuration this method was created for.
	 * @param params
	 *            This list holds the program parameters that are to be
	 *            optimized by the parameter optimization run.
	 * @param totalIterationCount
	 *            The total number of iterations to be performed by this
	 *            parameter optimization.
	 * @param isResume
	 *            This boolean indiciates, whether the run is a resumption of a
	 *            previous run execution or a completely new execution.
	 */
	public ParameterOptimizationMethod(final IRepository repository,
			final long changeDate, final File absPath,
			final IParameterOptimizationRun run,
			final IProgramConfig programConfig, final IDataConfig dataConfig,
			final List<IProgramParameter<?>> params,
			final int totalIterationCount, final boolean isResume) {
		super(repository, changeDate, absPath);

		this.log = LoggerFactory.getLogger(this.getClass());
		this.run = run;
		this.programConfig = programConfig;
		this.dataConfig = dataConfig;
		this.params = params;
		this.totalIterationCount = totalIterationCount;
		this.isResume = isResume;
	}

	/**
	 * The copy constructor of parameter optimization methods
	 * 
	 * @param method
	 *            The method to clone.
	 */
	public ParameterOptimizationMethod(
			final ParameterOptimizationMethod method) {
		super(method);

		// do not clone upstream
		this.run = method.run;
		this.programConfig = method.programConfig.clone();
		this.dataConfig = method.dataConfig.clone();
		this.params = ProgramParameter.cloneParameterList(method.params);
		this.totalIterationCount = method.totalIterationCount;
		this.isResume = method.isResume;

		// here it is safe to set them in the parameters, as at this point they
		// have been cloned for this particular run execution.
		for (int p = 0; p < method.params.size(); p++) {
			IProgramParameter<?> param = method.params.get(p);
			if (method.getParameterMinValueOverrides().containsKey(param))
				this.params.set(p, this.params.get(p).withDifferentMin(
						method.getParameterMinValueOverrides().get(param)
								+ ""));
		}
		for (int p = 0; p < method.params.size(); p++) {
			IProgramParameter<?> param = method.params.get(p);
			if (method.getParameterMaxValueOverrides().containsKey(param))
				this.params.set(p, this.params.get(p).withDifferentMax(
						method.getParameterMaxValueOverrides().get(param)
								+ ""));
		}
		for (int p = 0; p < method.params.size(); p++) {
			IProgramParameter<?> param = method.params.get(p);
			if (method.getParameterOptionsOverrides().containsKey(param)) {
				String[] origOptions = method.getParameterOptionsOverrides()
						.get(param);
				this.params.set(p, this.params.get(p).withDifferentOptions(
						Arrays.copyOf(origOptions, origOptions.length)));
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#equals(java.lang.
	 * Object )
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod#
	 * equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof ParameterOptimizationMethod))
			return false;

		ParameterOptimizationMethod other = (ParameterOptimizationMethod) obj;

		return this.run.equals(other.run)
				&& this.dataConfig.equals(other.dataConfig)
				&& this.programConfig.equals(other.programConfig)
				&& this.params.equals(other.params)
				&& this.totalIterationCount == other.totalIterationCount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#hashCode()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod#
	 * hashCode()
	 */
	@Override
	public int hashCode() {
		return (this.run.toString() + this.dataConfig.toString()
				+ this.programConfig.toString() + this.params.toString()
				+ this.totalIterationCount).hashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod#
	 * setRun(de.clusteval.run.ParameterOptimizationRun)
	 */
	@Override
	public void setRun(final IParameterOptimizationRun run) {
		this.run = run;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod#
	 * getOptimizationParameter()
	 */
	@Override
	public List<IProgramParameter<?>> getOptimizationParameter() {
		return this.params;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod#
	 * giveQualityFeedback(long,
	 * de.clusteval.cluster.quality.ClusteringQualitySet)
	 */
	@Override
	public synchronized void giveQualityFeedback(final long iteration,
			final IClusteringQualitySet qualities) {
		if (this.result.get(iteration) == null)
			finishedCount++;
		this.result.put(iteration, qualities,
				this.result.getClustering(iteration));
	}

	protected Pair<Long, ParameterSet> getNextParameterSet()
			throws InternalAttributeException, RegisterException,
			NoParameterSetFoundException, InterruptedException,
			ParameterSetAlreadyEvaluatedException {
		return this.getNextParameterSet(null);
	}

	/**
	 * This method purely determines and calculates the next parameter set that
	 * follows from the current state of the method.
	 * 
	 * <p>
	 * If the force parameter set is given != null, this parameter set is forced
	 * to be evaluated. This scenario is used during resumption of an older run,
	 * where the parameter sets are already fixed and we want to feed them to
	 * this method together with their results exactly as they were performed
	 * last time.
	 * 
	 * <p>
	 * This is a helper-method for {@link #next()} and
	 * {@link #next(ParameterSet)}.
	 * 
	 * <p>
	 * It might happen that this method puts the calling thread to sleep, in
	 * case the next parameter set requires older parameter sets to finish
	 * first.
	 * 
	 * <p>
	 * It might happen that this method puts the calling thread to sleep, in
	 * case the next parameter set requires older parameter sets to finish
	 * first.
	 * 
	 * @param forcedParameterSet
	 *            If this parameter is set != null, this parameter set is forced
	 *            to be evaluated in the next iteration.
	 * @return The next parameter set.
	 * @throws InternalAttributeException
	 * @throws RegisterException
	 * @throws NoParameterSetFoundException
	 *             This exception is thrown, if no parameter set was found that
	 *             was not already evaluated before.
	 * @throws InterruptedException
	 * @throws ParameterSetAlreadyEvaluatedException
	 */
	protected abstract Pair<Long, ParameterSet> getNextParameterSet(
			ParameterSet forcedParameterSet) throws InternalAttributeException,
			RegisterException, NoParameterSetFoundException,
			InterruptedException, ParameterSetAlreadyEvaluatedException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod#
	 * hasNext()
	 */
	@Override
	public abstract boolean hasNext();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod#next(
	 * )
	 */
	@Override
	public final Pair<Long, ParameterSet> next()
			throws InternalAttributeException, RegisterException,
			NoParameterSetFoundException, InterruptedException,
			ParameterSetAlreadyEvaluatedException {
		return this.next(null, -1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod#next(
	 * de.clusteval.program.ParameterSet, long)
	 */
	@Override
	public final Pair<Long, ParameterSet> next(
			final ParameterSet forcedParameterSet, final long iterationNumber)
			throws InternalAttributeException, RegisterException,
			NoParameterSetFoundException, InterruptedException,
			ParameterSetAlreadyEvaluatedException {
		if (this.result == null)
			throw new IllegalStateException("reset(File) has not been called");
		Pair<Long, ParameterSet> result = null;

		if (forcedParameterSet != null) {
			// 2016.12.18: we remove those parameters, which are not optimized
			// by this method;
			// this affects for the resume functionality;
			// this is important, because otherwise duplicate parameter sets are
			// not recognized;
			Set<String> availableParams = new HashSet<String>();
			for (IProgramParameter<?> p : this.params)
				availableParams.add(p.getName());

			for (String param : new HashSet<String>(
					forcedParameterSet.keySet())) {
				if (!availableParams.contains(param))
					forcedParameterSet.remove(param);
			}
			result = getNextParameterSet(forcedParameterSet);
			// 04.04.2013: changed to adapt method to previously skipped
			// iterations
			this.currentCount = (int) iterationNumber;
		} else {
			result = getNextParameterSet();
			this.currentCount++;
		}

		Set<Long> previousIterations = this.result
				.getIterationNumberForParameterSet(result.getSecond());

		if (result == null || result.getSecond() == null)
			throw new NoParameterSetFoundException(
					"No new parameter set could be found.");

		// this.result.put(this.currentCount, null, null);
		this.result.addParameterSet(this.currentCount, result.getSecond());

		if (!previousIterations.isEmpty()) {
			long previousIteration = previousIterations.iterator().next();
			this.giveQualityFeedback(this.currentCount,
					this.result.get(previousIteration));
			throw new ParameterSetAlreadyEvaluatedException(this.currentCount,
					previousIterations, result.getSecond());
		}
		return Pair.getPair(new Long(this.currentCount), result.getSecond());
	}

	/**
	 * This method takes a string and some additional parameters and parses from
	 * it a parameter optimization method.
	 * 
	 * @param repository
	 *            The repository in which the method should look up the
	 *            parameter optimization method class and where the new object
	 *            should be registered.
	 * @param parameterOptimizationMethod
	 *            The name of the parameter optimization method class.
	 * @param run
	 *            The run the new parameter optimization method belongs to.
	 * @param programConfig
	 *            The program configuration the new parameter optimization
	 *            method belongs to.
	 * @param dataConfig
	 *            The data configuration the new parameter optimization method
	 *            belongs to.
	 * @param params
	 *            The parameters of the program encapsulated by the program
	 *            configuration that are to be optimized.
	 * @param totalIterationCount
	 *            This array holds the number of iterations that are to be
	 *            performed for each optimization parameter.
	 * @param isResume
	 *            This boolean indiciates, whether the run is a resumption of a
	 *            previous run execution or a completely new execution.
	 * @param parameterMinValueOverrides
	 *            A map containing overriding values for parameters. If null, no
	 *            values are overridden.
	 * @param parameterMaxValueOverrides
	 *            A map containing overriding values for parameters. If null, no
	 *            values are overridden.
	 * @param parameterOptionsOverrides
	 *            A map containing overriding values for parameters. If null, no
	 *            values are overridden.
	 * @return The parsed parameter optimization method.
	 * @throws UnknownParameterOptimizationMethodException
	 * @throws DynamicComponentInitializationException
	 */
	public static IParameterOptimizationMethod parseFromString(
			final IRepository repository,
			final String parameterOptimizationMethod, final IRun run,
			final IProgramConfig programConfig, final IDataConfig dataConfig,
			final List<IProgramParameter<?>> params,
			final int totalIterationCount, final boolean isResume)
			throws UnknownParameterOptimizationMethodException,
			DynamicComponentInitializationException {
		Class<? extends IParameterOptimizationMethod> c = repository
				.resolveAndGetRegisteredClass(
						IParameterOptimizationMethod.class,
						parameterOptimizationMethod, true);
		if (c == null) {
			Pair<String, ComparableVersion> fullClassNameAndVersion = repository
					.getFullClassNameAndVersion(
							IParameterOptimizationMethod.class,
							parameterOptimizationMethod, true);
			if (fullClassNameAndVersion.getSecond() != null
					&& fullClassNameAndVersion.getSecond() != null
					&& repository.hasError(IParameterOptimizationMethod.class,
							fullClassNameAndVersion.getFirst(),
							fullClassNameAndVersion.getSecond().toString()))
				throw new UnknownParameterOptimizationMethodException(
						parameterOptimizationMethod,
						repository.getErrors(IParameterOptimizationMethod.class,
								fullClassNameAndVersion.getFirst(),
								fullClassNameAndVersion.getSecond().toString())
								.iterator().next());
			else
				throw new UnknownParameterOptimizationMethodException(
						parameterOptimizationMethod,
						"Unknown parameter optimization method");
		}
		return getInstance(repository, c, run, programConfig, dataConfig,
				params, totalIterationCount, isResume);
	}

	public static IParameterOptimizationMethod getInstance(
			final IRepository repository,
			final Class<? extends IParameterOptimizationMethod> clazz,
			final IRun run, final IProgramConfig programConfig,
			final IDataConfig dataConfig,
			final List<IProgramParameter<?>> params,
			final int totalIterationCount, final boolean isResume)
			throws UnknownParameterOptimizationMethodException,
			DynamicComponentInitializationException {
		try {
			Constructor<? extends IParameterOptimizationMethod> constr = clazz
					.getConstructor(IRepository.class, long.class, File.class,
							IParameterOptimizationRun.class,
							IProgramConfig.class, IDataConfig.class, List.class,
							int.class, boolean.class);
			// changed 21.03.2013: do not register new parameter optimization
			// methods here, because run is not set yet
			IParameterOptimizationMethod method = constr.newInstance(repository,
					System.currentTimeMillis(), new File(clazz.getSimpleName()),
					run, programConfig, dataConfig, params, totalIterationCount,
					isResume);

			return method;
		} catch (NoSuchMethodException e) {
			throw new DynamicComponentInitializationException(
					IParameterOptimizationMethod.class, clazz.getSimpleName(),
					"The class is missing the correct constructor");
		} catch (Exception e) {
			e.printStackTrace();
			// this is likely because we passed faked parameters
			throw new DynamicComponentInitializationException(
					IParameterOptimizationMethod.class, clazz.getSimpleName(),
					"The class initialization failed: " + e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod#
	 * getIterationPerParameter()
	 */
	@Override
	public int getIterationPerParameter() {
		return this.totalIterationCount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod#
	 * getTotalIterationCount()
	 */
	@Override
	public abstract int getTotalIterationCount();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod#
	 * getResult()
	 */
	@Override
	public IParameterOptimizationResult getResult() {
		// Removed 27.11.2012
		// this.result.register();
		return this.result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod#reset
	 * (java.io.File)
	 */
	@Override
	public void reset(final File absResultPath)
			throws ParameterOptimizationException, InternalAttributeException,
			RegisterException, RunResultParseException, InterruptedException {
		this.result = new ParameterOptimizationResult(
				this.dataConfig.getRepository(), System.currentTimeMillis(),
				// changed 16.09.2012 -> getParentFile
				absResultPath,
				absResultPath.getParentFile().getParentFile().getName(), run,
				this);
		// 09.05.2014: removed, in order to allow loading of results after
		// finishing of run
		// this.result.register();
		initParameterValues();
		if (isResume) {
			/*
			 * We have to simulate the process with the given results, to ensure
			 * that all attributes of the method (also als subclass methods) are
			 * valid and correspond to the result in the file.
			 */
			final IParameterOptimizationResult oldResults = ParameterOptimizationResult
					.parseFromRunResultCompleteFile(
							this.dataConfig.getRepository(), this.run, this,
							absResultPath, false, false, false);
			oldResults.loadIntoMemory(false);
			try {
				Map<Long, ParameterSet> parameterSets = oldResults
						.getParameterSets();
				SortedMap<Long, ParameterSet> paramSetsWithOrdering = new TreeMap<Long, ParameterSet>(
						parameterSets);

				for (Entry<Long, ParameterSet> entry : paramSetsWithOrdering
						.entrySet()) {
					final ParameterSet paramSet = entry.getValue();
					long iterationNumber = entry.getKey();
					try {
						Pair<Long, ParameterSet> newParamSet = this
								.next(paramSet, iterationNumber);
						iterationNumber = newParamSet.getFirst();
					} catch (NoParameterSetFoundException e) {
						// doesn't occur
					} catch (ParameterSetAlreadyEvaluatedException e) {
						// we can ignore this exception here and give the same
						// feedback as before.
					}
					this.giveQualityFeedback(iterationNumber,
							oldResults.get(iterationNumber));
				}
				isResume = false;
			} finally {
				oldResults.unloadFromMemory();
			}
		}
	}

	/**
	 * This method initializes the parameter values of each optimization
	 * parameter that should be assessed during the process.
	 * 
	 * @throws InternalAttributeException
	 */
	@Override
	@SuppressWarnings("unused")
	public void initParameterValues()
			throws ParameterOptimizationException, InternalAttributeException {
	}

	// removed 08.01.2013
	// /**
	// * @param dataConfig
	// * The new data configuration.
	// */
	// public void setDataConfig(final DataConfig dataConfig) {
	// this.dataConfig = dataConfig;
	// }

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod#
	 * getProgramConfig()
	 */
	@Override
	public IProgramConfig getProgramConfig() {
		return this.programConfig;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod#
	 * getStartedCount()
	 */
	@Override
	public int getStartedCount() {
		return this.currentCount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod#
	 * getFinishedCount()
	 */
	@Override
	public int getFinishedCount() {
		return this.finishedCount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod#
	 * getDataConfig()
	 */
	@Override
	public IDataConfig getDataConfig() {
		return this.dataConfig;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod#clone
	 * ()
	 */
	@Override
	public IParameterOptimizationMethod clone() {
		try {
			return this.getClass().getConstructor(this.getClass())
					.newInstance(this);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		this.log.warn("Cloning instance of class "
				+ this.getClass().getSimpleName() + " failed");
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod#
	 * getPlotDensityParameter()
	 */
	@Override
	public IProgramParameter<?> getPlotDensityParameter() {
		return this.params.get(0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod#
	 * setResume(boolean)
	 */
	@Override
	public void setResume(boolean isResume) {
		this.isResume = isResume;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod#
	 * getCompatibleDataSetFormatBaseClasses()
	 */
	@Override
	public abstract List<Class<? extends IDataSetFormat>> getCompatibleDataSetFormatBaseClasses();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod#
	 * getCompatibleProgramNames()
	 */
	@Override
	public abstract List<String> getCompatibleProgramNames();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod#
	 * setDataConfig(de.clusteval.data.DataConfig)
	 */
	@Override
	public void setDataConfig(IDataConfig dataConfig) {
		this.dataConfig = dataConfig;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod#
	 * setProgramConfig(de.clusteval.program.ProgramConfig)
	 */
	@Override
	public void setProgramConfig(IProgramConfig programConfig) {
		this.programConfig = programConfig;
	}

	/**
	 * @return the parameterMinValueOverrides
	 */
	@Override
	public Map<IProgramParameter<?>, String> getParameterMinValueOverrides() {
		if (!this.run.getOptimizationParameterMinValueOverrides()
				.containsKey(this.programConfig))
			return new HashMap<>();
		return this.run.getOptimizationParameterMinValueOverrides()
				.get(this.programConfig);
	}

	/**
	 * @return the parameterMaxValueOverrides
	 */
	@Override
	public Map<IProgramParameter<?>, String> getParameterMaxValueOverrides() {
		if (!this.run.getOptimizationParameterMinValueOverrides()
				.containsKey(this.programConfig))
			return new HashMap<>();
		return this.run.getOptimizationParameterMaxValueOverrides()
				.get(this.programConfig);
	}

	/**
	 * @return the parameterOptionsOverrides
	 */
	@Override
	public Map<IProgramParameter<?>, String[]> getParameterOptionsOverrides() {
		if (!this.run.getOptimizationParameterMinValueOverrides()
				.containsKey(this.programConfig))
			return new HashMap<>();
		return this.run.getOptimizationParameterOptionsOverrides()
				.get(this.programConfig);
	}

	/**
	 * @return the currentCount
	 */
	@Override
	public int getCurrentCount() {
		return currentCount;
	}

	@Override
	public ComparableVersion getVersion() {
		try {
			return Repository.getVersionOfObjectDynamicClass(this);
		} catch (Exception e) {
			// cannot happen because we loaded the class before;
			e.printStackTrace();
			return new ComparableVersion("1");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#getName()
	 */
	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObjectDynamicComponent#
	 * asSerializable()
	 */
	@Override
	public SerializableParameterOptimizationMethod asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableParameterOptimizationMethod) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#asSerializable()
	 */
	@Override
	public SerializableParameterOptimizationMethod asSerializableInternal()
			throws RepositoryObjectSerializationException {
		return new SerializableParameterOptimizationMethod(this);
	}

	/**
	 * @return the run
	 */
	@Override
	public IParameterOptimizationRun getRun() {
		return run;
	}
}
