/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.cluster.paramOptimization;

import java.io.File;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;

import de.clusteval.data.IDataConfig;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.program.IProgramConfig;
import de.clusteval.run.IRun;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.DynamicComponentMissingVersionException;
import de.clusteval.utils.IncompatibleClustEvalVersionException;
import de.clusteval.utils.InvalidClustEvalVersionException;
import de.clusteval.utils.InvalidDependencyTargetException;
import de.clusteval.utils.JARFinder;
import de.clusteval.utils.JARFinderClassLoader;
import de.clusteval.utils.MissingClustEvalVersionException;

/**
 * @author Christian Wiwie
 */
public class ParameterOptimizationMethodFinder
		extends
			JARFinder<IParameterOptimizationMethod>
		implements
			IParameterOptimizationMethodFinder {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5262711345214820945L;

	/**
	 * Instantiates a new clustering quality measure finder.
	 * 
	 * @param repository
	 *            The repository to register the new data configurations at.
	 * @throws RegisterException
	 */
	public ParameterOptimizationMethodFinder(final IRepository repository)
			throws RegisterException {
		super(repository, IParameterOptimizationMethod.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.JARFinder#getURLClassLoader0(java.io.File)
	 */
	@Override
	protected URLClassLoader getURLClassLoader0(File f,
			final ClassLoader parent) throws MalformedURLException {
		URL url = f.toURI().toURL();
		return new ParameterOptimizationMethodURLClassLoader(this,
				new URL[]{url}, parent);
	}
}

class ParameterOptimizationMethodURLClassLoader
		extends
			JARFinderClassLoader<IParameterOptimizationMethod> {

	/**
	 * @param urls
	 * @param parent
	 * @param loaderParent
	 */
	public ParameterOptimizationMethodURLClassLoader(
			ParameterOptimizationMethodFinder parent, URL[] urls,
			ClassLoader loaderParent) {
		super(parent, urls, loaderParent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.net.URLClassLoader#loadClass(java.lang.String)
	 */
	@Override
	public Class<?> loadClass(String name) throws ClassNotFoundException {
		Class<?> result = super.loadClass(name);

		if (Modifier.isAbstract(result.getModifiers()) || result.isInterface())
			return result;

		// if the class to load has parent annotation, we have to make sure,
		// that the parent is loaded
		if (result.isAnnotationPresent(LoadableClassParentAnnotation.class)) {
			// get the annotation
			LoadableClassParentAnnotation anno = result
					.getAnnotation(LoadableClassParentAnnotation.class);
			// try to load the parent of the class
			Class<? extends IParameterOptimizationMethod> c = parent
					.getRepository().resolveAndGetRegisteredClass(
							this.parent.getClassToFind(), anno.parent(), true);
			if (c == null)
				throw new NoClassDefFoundError(
						String.format("%s/%s", name, anno.parent()));
			for (URL url : ((URLClassLoader) c.getClassLoader()).getURLs())
				this.addURL(url);
		}

		if (name.startsWith("de.clusteval.cluster.paramOptimization")) {
			if (name.endsWith("ParameterOptimizationMethod")) {
				@SuppressWarnings("unchecked")
				Class<? extends ParameterOptimizationMethod> parameterOptimizationMethod = (Class<? extends ParameterOptimizationMethod>) result;

				// the class needs to have a version annotation
				try {
					ParameterOptimizationMethod.getInstance(
							this.parent.getRepository(),
							parameterOptimizationMethod, (IRun) null,
							(IProgramConfig) null, (IDataConfig) null,
							new ArrayList<>(), 100, false);

					// loadClass may have been called not for the newly
					// discovered class of this JAR, but for a parent class
					// which had already been registered before;
					// thus, we only register this class, if it hasn't been
					// registered before
					if (!this.parent.getRepository()
							.isClassRegistered(parameterOptimizationMethod)) {
						this.parent.getRepository().registerClass(
								this.parent.getClassToFind(),
								parameterOptimizationMethod);
					}
				} catch (DynamicComponentMissingVersionException
						| UnknownParameterOptimizationMethodException
						| DynamicComponentInitializationException
						| IncompatibleClustEvalVersionException
						| MissingClustEvalVersionException
						| InvalidClustEvalVersionException
						| InvalidDependencyTargetException e) {
					handleException(e);
				}
			}
		}
		return result;
	}
}
