/**
 * 
 */
package de.clusteval.cluster.paramOptimization;

import java.io.File;
import java.util.List;
import java.util.Map;

import de.clusteval.data.ISerializableDataConfig;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RepositoryObjectDeserializationException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.SerializableWrapperDynamicComponent;
import de.clusteval.program.IProgramParameter;
import de.clusteval.program.ISerializableProgramConfig;
import de.clusteval.run.IParameterOptimizationRun;
import de.clusteval.run.ISerializableParameterOptimizationRun;
import de.clusteval.utils.DynamicComponentInitializationException;

/**
 * @author Christian Wiwie
 *
 */
public class SerializableParameterOptimizationMethod
		extends
			SerializableWrapperDynamicComponent<IParameterOptimizationMethod>
		implements
			ISerializableParameterOptimizationMethod {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2657592124518837201L;

	protected final ISerializableProgramConfig programConfig;
	protected final ISerializableDataConfig dataConfig;

	protected final int totalIterationCount;

	protected final List<IProgramParameter<?>> optimizationParameters;

	protected ISerializableParameterOptimizationRun<? extends IParameterOptimizationRun> run;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 * @param totalIterationCount
	 * @param optimizationParameters
	 * @param run
	 */
	public SerializableParameterOptimizationMethod(File absPath, String name,
			String version, final String alias, final ISerializableProgramConfig programConfig,
			final ISerializableDataConfig dataConfig, int totalIterationCount,
			List<IProgramParameter<?>> optimizationParameters,
			ISerializableParameterOptimizationRun<? extends IParameterOptimizationRun> run) {
		super(IParameterOptimizationMethod.class, absPath, name, version, alias);
		this.dataConfig = dataConfig;
		this.programConfig = programConfig;
		this.totalIterationCount = totalIterationCount;
		this.optimizationParameters = optimizationParameters;
		this.run = run;
	}

	/**
	 * @param wrappedComponent
	 * @throws RepositoryObjectSerializationException
	 */
	public SerializableParameterOptimizationMethod(
			IParameterOptimizationMethod wrappedComponent)
			throws RepositoryObjectSerializationException {
		super(IParameterOptimizationMethod.class, wrappedComponent);
		this.dataConfig = wrappedComponent.getDataConfig().asSerializable();
		this.programConfig = wrappedComponent.getProgramConfig()
				.asSerializable();
		this.totalIterationCount = wrappedComponent.getTotalIterationCount();
		this.optimizationParameters = wrappedComponent
				.getOptimizationParameter();
		// we don't set the run because this is invoked from serializing the
		// run; otherwise endless recursion
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.paramOptimization.
	 * ISerializableParameterOptimizationMethod#getOptimizationParameters()
	 */
	@Override
	public List<IProgramParameter<?>> getOptimizationParameters() {
		return optimizationParameters;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.paramOptimization.
	 * ISerializableParameterOptimizationMethod#getParameterMaxValueOverrides()
	 */
	@Override
	public Map<IProgramParameter<?>, String> getParameterMaxValueOverrides() {
		return this.run.getOptimizationParameterMaxValueOverrides()
				.get(programConfig);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.paramOptimization.
	 * ISerializableParameterOptimizationMethod#getParameterMinValueOverrides()
	 */
	@Override
	public Map<IProgramParameter<?>, String> getParameterMinValueOverrides() {
		return this.run.getOptimizationParameterMinValueOverrides()
				.get(programConfig);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.paramOptimization.
	 * ISerializableParameterOptimizationMethod#getParameterOptionsOverrides()
	 */
	@Override
	public Map<IProgramParameter<?>, String[]> getParameterOptionsOverrides() {
		return this.run.getOptimizationParameterOptionsOverrides()
				.get(programConfig);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.paramOptimization.
	 * ISerializableParameterOptimizationMethod#getTotalIterationCount()
	 */
	@Override
	public int getTotalIterationCount() {
		return totalIterationCount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.SerializableWrapper#deserializeInternal
	 * ()
	 */
	@Override
	protected IParameterOptimizationMethod deserializeInternal(
			final IRepository repository) throws DeserializationException {
		try {
			// We do not pass the run here, because this method is invoked from
			// deserializing that run and we would end up in an endless
			// recursion
			// The run has to be set later
			return ParameterOptimizationMethod.parseFromString(repository,
					this.name, null, this.programConfig.deserialize(repository),
					this.dataConfig.deserialize(repository),
					this.optimizationParameters, this.totalIterationCount,
					false);
		} catch (UnknownParameterOptimizationMethodException
				| DynamicComponentInitializationException e) {
			throw new RepositoryObjectDeserializationException(
					IParameterOptimizationMethod.class, this.name, e);
		}
	}

	/**
	 * @param run
	 *            the run to set
	 */
	@Override
	public void setRun(
			ISerializableParameterOptimizationRun<? extends IParameterOptimizationRun> run) {
		this.run = run;
	}
}
