/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.cluster;

import java.io.File;

import de.clusteval.framework.repository.IRepository;
import de.clusteval.run.runresult.format.IRunResultFormat;

/**
 * A clustering contains several clusters. Every cluster contains cluster items.
 */
public class FileBackedClustering extends Clustering
		implements
			IClustering,
			IFileBackedClustering {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5069430797507192517L;
	protected IRunResultFormat resultFormat;

	/**
	 * Instantiates a new clustering.
	 * 
	 * @param repository
	 * @param changeDate
	 * @param absPath
	 * 
	 * @param format
	 * 
	 */
	public FileBackedClustering(IRepository repository, long changeDate,
			File absPath, final IRunResultFormat format) {
		super(repository, changeDate, absPath);
		this.resultFormat = format;
	}

	/**
	 * Instantiates a new clustering.
	 * 
	 * @param clustering
	 * @param format
	 * 
	 */
	public FileBackedClustering(IClustering clustering,
			final IRunResultFormat format) {
		this(clustering.getRepository(), clustering.getChangeDate(),
				clustering.getFile(), format);
	}

	/**
	 * The copy constructor of clusterings.
	 * 
	 * @param other
	 *            The object to clone.
	 */
	public FileBackedClustering(final FileBackedClustering other) {
		super(other);
		this.clusters = cloneClusters(other.clusters);
		this.clusterIdToCluster = cloneClusterIdToCluster(
				other.clusterIdToCluster);
		this.itemToCluster = cloneItemToClusters(other.itemToCluster);
		this.itemIdToItem = cloneItemIdToItem(other.itemIdToItem);
		this.resultFormat = other.resultFormat.clone();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.IClustering#getFormat()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.IFileBackedClustering#getFormat()
	 */
	@Override
	public IRunResultFormat getFormat() {
		return resultFormat;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.cluster.IFileBackedClustering#setFormat(de.clusteval.run.
	 * runresult.format.IRunResultFormat)
	 */
	@Override
	public void setFormat(IRunResultFormat resultFormat) {
		this.resultFormat = resultFormat;
	}
}
