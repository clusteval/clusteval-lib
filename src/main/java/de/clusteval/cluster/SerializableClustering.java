/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.cluster;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import de.clusteval.cluster.quality.ISerializableClusteringQualitySet;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.SerializableWrapperRepositoryObject;

public class SerializableClustering
		extends
			SerializableWrapperRepositoryObject<IClustering>
		implements
			ISerializableClustering {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7028654674298046694L;

	protected float fuzzySize;

	protected Set<ICluster> clusters;

	protected Map<String, ICluster> clusterIdToCluster;

	protected Map<IClusterItem, Map<ICluster, Float>> itemToCluster;

	protected Map<String, IClusterItem> itemIdToItem;

	protected ISerializableClusteringQualitySet qualities;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 * @param fuzzySize
	 * @param clusters
	 * @param clusterIdToCluster
	 * @param itemToCluster
	 * @param itemIdToItem
	 * @param qualities
	 */
	public SerializableClustering(File absPath, String name, String version,
			float fuzzySize, Set<ICluster> clusters,
			Map<String, ICluster> clusterIdToCluster,
			Map<IClusterItem, Map<ICluster, Float>> itemToCluster,
			Map<String, IClusterItem> itemIdToItem,
			ISerializableClusteringQualitySet qualities) {
		super(absPath, name, version);
		this.fuzzySize = fuzzySize;
		this.clusters = clusters;
		this.clusterIdToCluster = clusterIdToCluster;
		this.itemToCluster = itemToCluster;
		this.itemIdToItem = itemIdToItem;
		this.qualities = qualities;
	}

	/**
	 * @param wrappedComponent
	 * @throws RepositoryObjectSerializationException
	 */
	public SerializableClustering(Clustering wrappedComponent)
			throws RepositoryObjectSerializationException {
		super(wrappedComponent);
		this.clusterIdToCluster = new HashMap<>(
				wrappedComponent.clusterIdToCluster);
		this.clusters = new HashSet<>(wrappedComponent.clusters);
		this.fuzzySize = wrappedComponent.fuzzySize;
		this.itemIdToItem = new HashMap<>(wrappedComponent.itemIdToItem);
		this.itemToCluster = new HashMap<>(wrappedComponent.itemToCluster);
		if (wrappedComponent.qualities != null)
			this.qualities = wrappedComponent.qualities.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.SerializableWrapper#deserializeInternal
	 * (de.clusteval.framework.repository.IRepository)
	 */
	@Override
	public IClustering deserializeInternal(IRepository repository)
			throws DeserializationException {
		Clustering result = new Clustering(repository,
				System.currentTimeMillis(), this.absPath);
		result.clusterIdToCluster = this.clusterIdToCluster;
		result.clusters = this.clusters;
		result.fuzzySize = this.fuzzySize;
		result.itemIdToItem = this.itemIdToItem;
		result.itemToCluster = this.itemToCluster;
		result.qualities = this.qualities.deserialize(repository);

		return result;
	}

	/**
	 * @return the clusterIdToCluster
	 */
	@Override
	public Map<String, ICluster> getClusterIdToCluster() {
		return clusterIdToCluster;
	}

	/**
	 * @return the clusters
	 */
	@Override
	public Set<ICluster> getClusters() {
		return clusters;
	}

	/**
	 * @return the fuzzySize
	 */
	@Override
	public float getFuzzySize() {
		return fuzzySize;
	}

	/**
	 * @return the itemIdToItem
	 */
	@Override
	public Map<String, IClusterItem> getItemIdToItem() {
		return itemIdToItem;
	}

	/**
	 * @return the itemToCluster
	 */
	@Override
	public Map<IClusterItem, Map<ICluster, Float>> getItemToCluster() {
		return itemToCluster;
	}

	/**
	 * @return the qualities
	 */
	@Override
	public ISerializableClusteringQualitySet getQualities() {
		return qualities;
	}
}
