/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.cluster.quality;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.cluster.FileBackedClustering;
import de.clusteval.cluster.IClustering;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.format.InvalidDataSetFormatVersionException;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.goldstandard.InvalidGoldStandardFormatException;
import de.clusteval.framework.RLibraryRequirement;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.Repository;
import de.clusteval.framework.repository.RepositoryObjectDynamicComponent;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.utils.ClassVersion;
import de.clusteval.utils.ClassVersionRequirement;
import de.clusteval.utils.ClustEvalAlias;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.DynamicComponentVersion;
import de.clusteval.utils.RCalculationException;
import de.clusteval.utils.RNotAvailableException;
import dk.sdu.imada.compbio.utils.Pair;

//@formatter:off
/**
 * A clustering quality measure is used to assess the quality of a
 * {@link FileBackedClustering} by invoking
 * {@link #getQualityOfClustering(IClustering, IClustering, IDataConfig)}.
 * 
 * It has a range of possible values between {@link #getMinimum()} and
 * {@link #getMaximum()}.
 * 
 * Some measures can only be assessed if a goldstandard is available (see
 * {@link #requiresGoldstandard()}).
 * 
 * Furthermore, some measures are better when maximized and some when minimized
 * (see {@link #isBetterThan} and {@link #isBetterThanHelper} ).
 * 
 * ## Writing clustering quality measures
 * 
 * A clustering quality measure MyClusteringQualityMeasure can be added to ClustEval by
 * 
 * 1. extending the {@link ClusteringQualityMeasure} with your own class `MyClusteringQualityMeasure`.
 *     - Your class has to be decorated with the following annotations:
 *         - {@link DynamicComponentVersion}: Any component you add to ClustEval needs to have a version. This is needed to ensure, that results are fully reproducible by running runs using exactly the same versions as previously
 *         - {@link ClassVersionRequirement}: Any component you add to ClustEval needs to specify, which dependencies it has on the ClustEval interfaces. This information is used to ensure compatibility between the version of your component and the used ClustEval version. Whenever core interfaces in the ClustEval API change, their interface version is incremented. By using this annotation it can detect, which components are still compatible.
 *         - {@link ClustEvalAlias}: You need to provide a readable alias for your component. This alias is used when results and components should be presented in a accessible way.
 *         - [Optional] {@link RLibraryRequirement}: If your component depends on R libraries being installed and loaded when your program is being executed, it needs to be decorated with this annotation.
 *     - You have to provide your own implementations for the following methods, otherwise the framework will not be able to load your clustering quality measure.
 *         * {@link #ClusteringQualityMeasure(IRepository, long, File, ClusteringQualityMeasureParameters)}: The constructor for your distance measure. This constructor has to be implemented and public.
 *         * {@link #ClusteringQualityMeasure(ClusteringQualityMeasure)}: The copy constructor for your distance measure. This constructor has to be implemented and public.
 *         * {@link #getMinimum()}: Returns the minimal value this measure can calculate.
 *         * {@link #getMaximum()}: Returns the maximal value this measure can calculate.
 *         * {@link #requiresGoldstandard()}: Indicates, whether this clustering quality measure requires a goldstandard to assess the quality of a given clustering.
 *         * {@link #getQualityOfClustering(IClustering, IClustering, IDataConfig)}: This method is the core of your clustering quality measure. It assesses and returns the quality of the given clustering.
 *         * {@link #isBetterThanHelper(IClusteringQualityMeasureValue, IClusteringQualityMeasureValue)}: This method is used by sorting algorithms of the framework to compare clustering quality measure results and find the optimal parameter sets.
 *         * {@link #supportsFuzzyClusterings()}: Whether your quality measure is able to take fuzziness in the clustering into account. If not, the clustering is converted to a crisp clustering prior to passing it to your class's {@link #getQualityOfClustering(IClustering, IClustering, IDataConfig)}.
 * 2. Creating a jar file named `MyClusteringQualityMeasure.jar` containing the `MyClusteringQualityMeasure` class compiled on your machine in the correct folder structure corresponding to the packages: `de/clusteval/cluster/quality/MyClusteringQualityMeasure.class`
 * 3. Putting the `MyClusteringQualityMeasure.jar` into the clustering quality measure folder of the repository: `<REPOSITORY ROOT>/supp/clustering/qualityMeasures`
 * 
 * The backend server will recognize and try to load the new clustering quality measure automatically the next time, the ClusteringQualityMeasureFinderThread checks the filesystem.
 *
 * @author Christian Wiwie
 */
//@formatter:on
@ClassVersion(version = "2")
public abstract class ClusteringQualityMeasure
		extends
			RepositoryObjectDynamicComponent
		implements
			IClusteringQualityMeasure {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1278850021762068277L;
	protected ClusteringQualityMeasureParameters parameters;

	/**
	 * 
	 * @param repo
	 * @param changeDate
	 * @param absPath
	 * @param parameters
	 *            Parameter values in case this is a parameterized quality
	 *            measure. May be null otherwise.
	 */
	public ClusteringQualityMeasure(final IRepository repo,
			final long changeDate, final File absPath,
			final ClusteringQualityMeasureParameters parameters) {
		super(repo, changeDate, absPath);

		this.parameters = parameters;
	}

	/**
	 * The copy constructor of clustering quality measures.
	 * 
	 * @param other
	 *            The quality measure to clone.
	 */
	public ClusteringQualityMeasure(final ClusteringQualityMeasure other) {
		super(other);
		if (other.parameters != null)
			this.parameters = other.parameters.clone();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.quality.IClusteringQualityMeasure#
	 * getQualityOfClustering(de.clusteval.cluster.IClustering,
	 * de.clusteval.cluster.IClustering, de.clusteval.data.IDataConfig)
	 */
	@Override
	public abstract IClusteringQualityMeasureValue getQualityOfClustering(
			IClustering clustering, IClustering goldStandard,
			IDataConfig dataConfig) throws InvalidGoldStandardFormatException,
			UnknownDataSetFormatException, IOException,
			InvalidDataSetFormatVersionException, RNotAvailableException,
			RCalculationException, InterruptedException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.quality.IClusteringQualityMeasure#
	 * supportsFuzzyClusterings()
	 */
	@Override
	public abstract boolean supportsFuzzyClusterings();

	/**
	 * This is a helper method for cloning a list of clustering quality
	 * measures.
	 * 
	 * @param qualityMeasures
	 *            The quality measures to be cloned.
	 * @return A list containining cloned objects of the given quality measures.
	 */
	public static List<IClusteringQualityMeasure> cloneQualityMeasures(
			final List<IClusteringQualityMeasure> qualityMeasures) {
		List<IClusteringQualityMeasure> result = new ArrayList<IClusteringQualityMeasure>();

		for (IClusteringQualityMeasure qualityMeasure : qualityMeasures)
			result.add(qualityMeasure.clone());

		return result;
	}

	/**
	 * Parses the from string.
	 * 
	 * @param repository
	 *            the repository
	 * @param qualityMeasure
	 *            the quality measure
	 * @param parameters
	 * @return the clustering quality measure
	 * @throws UnknownClusteringQualityMeasureException
	 *             the unknown clustering quality measure exception
	 * @throws DynamicComponentInitializationException
	 */
	public static IClusteringQualityMeasure parseFromString(
			final IRepository repository, String qualityMeasure,
			ClusteringQualityMeasureParameters parameters)
			throws UnknownClusteringQualityMeasureException,
			DynamicComponentInitializationException {

		Class<? extends IClusteringQualityMeasure> c = repository
				.resolveAndGetRegisteredClass(IClusteringQualityMeasure.class,
						qualityMeasure, true);
		if (c == null) {
			Pair<String, ComparableVersion> fullClassNameAndVersion = repository
					.getFullClassNameAndVersion(IClusteringQualityMeasure.class,
							qualityMeasure, true);
			if (fullClassNameAndVersion.getSecond() != null
					&& repository.hasError(IClusteringQualityMeasure.class,
							fullClassNameAndVersion.getFirst(),
							fullClassNameAndVersion.getSecond().toString()))
				throw new UnknownClusteringQualityMeasureException(
						qualityMeasure,
						repository.getErrors(IClusteringQualityMeasure.class,
								fullClassNameAndVersion.getFirst(),
								fullClassNameAndVersion.getSecond().toString())
								.iterator().next());
			else
				throw new UnknownClusteringQualityMeasureException(
						qualityMeasure, "Unknown clustering quality measure");
		}
		return getInstance(repository, c, parameters);
	}

	/**
	 * @param repository
	 * @param clazz
	 * @param parameters
	 * @return
	 * @throws DynamicComponentInitializationException
	 */
	public static IClusteringQualityMeasure getInstance(
			final IRepository repository,
			Class<? extends IClusteringQualityMeasure> clazz,
			ClusteringQualityMeasureParameters parameters)
			throws DynamicComponentInitializationException {
		try {
			IClusteringQualityMeasure measure = clazz
					.getConstructor(IRepository.class, long.class, File.class,
							ClusteringQualityMeasureParameters.class)
					.newInstance(repository, System.currentTimeMillis(),
							new File(clazz.getSimpleName()), parameters);

			return measure;
		} catch (NoSuchMethodException e1) {
			throw new DynamicComponentInitializationException(
					IClusteringQualityMeasure.class, clazz.getSimpleName(),
					"The class is missing the correct constructor");
		} catch (Exception e) {
			// this is likely because we passed faked parameters
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.cluster.quality.IClusteringQualityMeasure#equals(java.lang.
	 * Object)
	 */
	@Override
	public boolean equals(Object obj) {
		// changed 05.12.2013
		return this.getClass().getSimpleName()
				.equals(obj.getClass().getSimpleName());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.quality.IClusteringQualityMeasure#clone()
	 */
	@Override
	public ClusteringQualityMeasure clone() {
		try {
			return this.getClass().getConstructor(this.getClass())
					.newInstance(this);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		this.log.warn("Cloning instance of class "
				+ this.getClass().getSimpleName() + " failed");
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.quality.IClusteringQualityMeasure#hashCode()
	 */
	@Override
	public int hashCode() {
		return this.getClass().hashCode();
	}

	@Override
	public ComparableVersion getVersion() {
		try {
			return Repository.getVersionOfObjectDynamicClass(this);
		} catch (Exception e) {
			// cannot happen because we loaded the class before;
			e.printStackTrace();
			return new ComparableVersion("1");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#getName()
	 */
	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.cluster.quality.IClusteringQualityMeasure#isBetterThan(de.
	 * clusteval.cluster.quality.ClusteringQualityMeasureValue,
	 * de.clusteval.cluster.quality.ClusteringQualityMeasureValue)
	 */
	@Override
	public final boolean isBetterThan(IClusteringQualityMeasureValue quality1,
			IClusteringQualityMeasureValue quality2) {
		if (!quality1.isTerminated() || !quality1.hasValue())
			return false;
		if (!quality2.isTerminated() || !quality2.hasValue())
			return true;
		// 06.05.2014: if this quality is NaN, the new one is always considered
		// better
		if (Double.isNaN(quality2.getValue()))
			return true;
		// 06.05.2014: if the new quality is NaN, the old one is considered
		// better (if it is not NaN)
		if (Double.isNaN(quality1.getValue()))
			return false;
		return isBetterThanHelper(quality1, quality2);
	}

	protected abstract boolean isBetterThanHelper(
			IClusteringQualityMeasureValue quality1,
			IClusteringQualityMeasureValue quality2);

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.quality.IClusteringQualityMeasure#getMinimum()
	 */
	@Override
	public abstract double getMinimum();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.quality.IClusteringQualityMeasure#getMaximum()
	 */
	@Override
	public abstract double getMaximum();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.quality.IClusteringQualityMeasure#
	 * requiresGoldstandard()
	 */
	@Override
	public abstract boolean requiresGoldstandard();

	@Override
	public SerializableClusteringQualityMeasure asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableClusteringQualityMeasure) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#asSerializable()
	 */
	@Override
	public SerializableClusteringQualityMeasure asSerializableInternal() {
		return new SerializableClusteringQualityMeasure(this);
	}
}
