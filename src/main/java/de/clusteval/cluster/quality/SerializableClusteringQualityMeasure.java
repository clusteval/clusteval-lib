/**
 * 
 */
package de.clusteval.cluster.quality;

import java.io.File;

import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.IRepositoryObjectDynamicComponent;
import de.clusteval.framework.repository.RepositoryObjectDeserializationException;
import de.clusteval.framework.repository.SerializableWrapperDynamicComponent;
import de.clusteval.utils.DynamicComponentInitializationException;

public class SerializableClusteringQualityMeasure
		extends
			SerializableWrapperDynamicComponent<IClusteringQualityMeasure>
		implements
			ISerializableClusteringQualityMeasure {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2705897415817613032L;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 */
	public SerializableClusteringQualityMeasure(File absPath, String name,
			String version, final String alias) {
		super(IClusteringQualityMeasure.class, absPath, name, version, alias);
	}

	/**
	 * @param wrappedComponent
	 */
	public SerializableClusteringQualityMeasure(
			IClusteringQualityMeasure wrappedComponent) {
		super(IClusteringQualityMeasure.class, wrappedComponent);
	}

	@Override
	protected IClusteringQualityMeasure deserializeInternal(
			final IRepository repository) throws DeserializationException {
		try {
			return ClusteringQualityMeasure.parseFromString(repository,
					this.name, null);
		} catch (UnknownClusteringQualityMeasureException
				| DynamicComponentInitializationException e) {
			throw new RepositoryObjectDeserializationException(
					IClusteringQualityMeasure.class, this.name, e);
		}
	}
}