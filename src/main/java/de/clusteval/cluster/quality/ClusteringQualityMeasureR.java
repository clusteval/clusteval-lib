/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.cluster.quality;

import java.io.File;
import java.io.IOException;

import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RserveException;

import de.clusteval.cluster.IClustering;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.format.InvalidDataSetFormatVersionException;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.goldstandard.InvalidGoldStandardFormatException;
import de.clusteval.framework.repository.IMyRengine;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RException;
import de.clusteval.utils.RNotAvailableException;

/**
 * This type of clustering quality measure uses the R framework to calculate
 * cluster validities.
 * 
 * @author Christian Wiwie
 * 
 */
public abstract class ClusteringQualityMeasureR
		extends
			ClusteringQualityMeasure {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1479787698290363068L;

	/**
	 * Instantiates a new R clustering quality measure.
	 * 
	 * @param repo
	 * @param changeDate
	 * @param absPath
	 * @param parameters
	 */
	public ClusteringQualityMeasureR(final IRepository repo,
			final long changeDate, final File absPath,
			final ClusteringQualityMeasureParameters parameters) {
		super(repo, changeDate, absPath, parameters);
	}

	/**
	 * The copy constructor of R clustering quality measures.
	 * 
	 * @param other
	 *            The quality measure to clone.
	 */
	public ClusteringQualityMeasureR(final ClusteringQualityMeasureR other) {
		super(other);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.quality.ClusteringQualityMeasure#
	 * getQualityOfClustering (de.clusteval.cluster.Clustering,
	 * de.clusteval.cluster.Clustering, de.clusteval.data.DataConfig)
	 */
	@Override
	public final IClusteringQualityMeasureValue getQualityOfClustering(
			IClustering clustering, IClustering goldStandard,
			IDataConfig dataConfig) throws InvalidGoldStandardFormatException,
			UnknownDataSetFormatException, IOException,
			InvalidDataSetFormatVersionException, RNotAvailableException,
			InterruptedException {
		try {
			IMyRengine rEngine = repository.getRengineForCurrentThread();
			try {
				try {
					return getQualityOfClusteringHelper(clustering,
							goldStandard, dataConfig, rEngine);
				} catch (REXPMismatchException e) {
					// handle this type of exception as an REngineException
					throw new RException(rEngine, e.getMessage());
				}
			} catch (REngineException e) {
				this.log.warn("R-framework (" + this.getClass().getSimpleName()
						+ "): " + rEngine.getLastError());
				// ClusteringQualityMeasureValue min =
				// ClusteringQualityMeasureValue
				// .getForDouble(this.getMinimum());
				// ClusteringQualityMeasureValue max =
				// ClusteringQualityMeasureValue
				// .getForDouble(this.getMaximum());
				// if (this.isBetterThan(max, min))
				// return min;
				// return max;
				return ClusteringQualityMeasureValue.getForDouble(Double.NaN);
			} finally {
				rEngine.clear();
			}
		} catch (RserveException e) {
			throw new RNotAvailableException(e.getMessage());
		}
	}

	protected abstract IClusteringQualityMeasureValue getQualityOfClusteringHelper(
			IClustering clustering, IClustering goldStandard,
			IDataConfig dataConfig, final IMyRengine rEngine)
			throws InvalidGoldStandardFormatException,
			UnknownDataSetFormatException, IOException,
			InvalidDataSetFormatVersionException, REngineException,
			IllegalArgumentException, REXPMismatchException,
			InterruptedException;
}
