/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.cluster.quality;

import java.io.File;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.DynamicComponentMissingVersionException;
import de.clusteval.utils.IncompatibleClustEvalVersionException;
import de.clusteval.utils.InvalidClustEvalVersionException;
import de.clusteval.utils.InvalidDependencyTargetException;
import de.clusteval.utils.JARFinder;
import de.clusteval.utils.JARFinderClassLoader;
import de.clusteval.utils.MissingClustEvalVersionException;

/**
 * @author Christian Wiwie
 */
public class ClusteringQualityMeasureFinder
		extends
			JARFinder<IClusteringQualityMeasure> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2660191337380356728L;

	/**
	 * Instantiates a new clustering quality measure finder.
	 * 
	 * @param repository
	 *            The repository to register the new data configurations at.
	 * @throws RegisterException
	 */
	public ClusteringQualityMeasureFinder(final IRepository repository)
			throws RegisterException {
		super(repository, IClusteringQualityMeasure.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.JARFinder#getURLClassLoader0(java.io.File)
	 */
	@Override
	protected URLClassLoader getURLClassLoader0(File f,
			final ClassLoader parent) throws MalformedURLException {
		URL url = f.toURI().toURL();
		return new ClusteringQualityMeasureURLClassLoader(this, new URL[]{url},
				parent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return (this.getClass().toString() + this.repository).hashCode();
	}
}

class ClusteringQualityMeasureURLClassLoader
		extends
			JARFinderClassLoader<IClusteringQualityMeasure> {

	/**
	 * @param urls
	 * @param parent
	 * @param loaderParent
	 */
	public ClusteringQualityMeasureURLClassLoader(
			ClusteringQualityMeasureFinder parent, URL[] urls,
			ClassLoader loaderParent) {
		super(parent, urls, loaderParent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.ClassLoader#loadClass(java.lang.String)
	 */
	@Override
	public Class<?> loadClass(String name) throws ClassNotFoundException {
		Class<?> result = super.loadClass(name, true);

		if (Modifier.isAbstract(result.getModifiers()) || result.isInterface())
			return result;

		if (name.startsWith("de.clusteval.cluster.quality")) {
			if (name.endsWith("ClusteringQualityMeasure")) {
				@SuppressWarnings("unchecked")
				Class<? extends IClusteringQualityMeasure> qualityMeasure = (Class<? extends IClusteringQualityMeasure>) result;

				try {
					ClusteringQualityMeasure.getInstance(
							this.parent.getRepository(), qualityMeasure,
							new ClusteringQualityMeasureParameters());

					// loadClass may have been called not for the newly
					// discovered class of this JAR, but for a parent class
					// which had already been registered before;
					// thus, we only register this class, if it hasn't been
					// registered before
					if (!this.parent.getRepository()
							.isClassRegistered(qualityMeasure)) {
						this.parent.getRepository().registerClass(
								this.parent.getClassToFind(), qualityMeasure);
					}
				} catch (DynamicComponentMissingVersionException
						| DynamicComponentInitializationException
						| IncompatibleClustEvalVersionException
						| MissingClustEvalVersionException
						| InvalidClustEvalVersionException
						| InvalidDependencyTargetException e) {
					handleException(e);
				}
			}
		}
		return result;
	}
}
