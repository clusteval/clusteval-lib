/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.cluster.quality;

import java.util.HashMap;

import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.SerializableWrapper;

/**
 * A clustering quality set is a map with clustering quality measures mapped to
 * clustering quality measure values achieved for each of those.
 * 
 * @author Christian Wiwie
 * 
 */
public class SerializableClusteringQualitySet
		extends
			SerializableWrapper<IClusteringQualitySet>
		implements
			ISerializableClusteringQualitySet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6399103828897048146L;

	protected HashMap<ISerializableClusteringQualityMeasure, IClusteringQualityMeasureValue> qualities;

	/**
	 * @param qualities
	 */
	public SerializableClusteringQualitySet(
			HashMap<ISerializableClusteringQualityMeasure, IClusteringQualityMeasureValue> qualities) {
		super();
		this.qualities = qualities;
	}

	public SerializableClusteringQualitySet(
			final IClusteringQualitySet qualities) throws RepositoryObjectSerializationException {
		super();
		this.qualities = new HashMap<>();
		for (IClusteringQualityMeasure m : qualities.keySet())
			this.qualities.put(m.asSerializable(), qualities.get(m));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.SerializableWrapper#deserializeInternal
	 * (de.clusteval.framework.repository.IRepository)
	 */
	@Override
	protected IClusteringQualitySet deserializeInternal(IRepository repository)
			throws DeserializationException {
		ClusteringQualitySet result = new ClusteringQualitySet();
		for (ISerializableClusteringQualityMeasure m : this.qualities.keySet())
			result.put(m.deserialize(repository), this.qualities.get(m));
		return result;
	}

	/**
	 * @return the qualities
	 */
	@Override
	public HashMap<ISerializableClusteringQualityMeasure, IClusteringQualityMeasureValue> getQualities() {
		return qualities;
	}
}
