/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.cluster.quality;

import java.io.Serializable;

/**
 * This is a wrapper class for double values calculated by clustering quality
 * measures.
 * 
 * <p>
 * This wrapper class allows for the fact, that iterations of parameter
 * optimizations might not terminate. In this case {@link #toString()} returns
 * <b>"NT"</b>, where "NT" means <b>"Not Terminated"</b>. The factory method
 * {@link #getForNotTerminated()} returns such objects.
 * 
 * @author Christian Wiwie
 * 
 */
public class ClusteringQualityMeasureValue
		implements
			IClusteringQualityMeasureValue,
			Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4906177454245645305L;

	public static final ClusteringQualityMeasureValue NOT_TERMINATED = new ClusteringQualityMeasureValue(
			null, false);

	public static final ClusteringQualityMeasureValue NOT_AVAILABLE = new ClusteringQualityMeasureValue(
			null, true);

	/**
	 * The clustering quality assessed. Set this value to <b>null</b> if the
	 * iteration did not terminate.
	 */
	private Double value;

	/**
	 * A boolean indicating, whether the iteration belonging to this object
	 * terminated.
	 */
	protected boolean isTerminated;

	private ClusteringQualityMeasureValue(final Double value) {
		this(value, value != null);
	}

	private ClusteringQualityMeasureValue(final Double value,
			final boolean terminated) {
		super();
		this.value = value;
		this.isTerminated = terminated;
	}

	/**
	 * @param value
	 *            The quality of the clustering as a double value.
	 * @return A wrapper object for a clustering quality given as a double
	 *         value.
	 */
	public static ClusteringQualityMeasureValue getForDouble(
			final double value) {
		return new ClusteringQualityMeasureValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.cluster.quality.IClusteringQualityMeasureValue#toString()
	 */
	@Override
	public String toString() {
		if (!hasValue())
			return "NA";
		if (!isTerminated)
			return "NT";
		return value.toString();
	}

	/**
	 * This method returns a clustering quality measure value wrapper object
	 * corresponding to the given string.
	 * 
	 * <p>
	 * If the string equals <b>NT</b>, a wrapper object for a not terminated
	 * iteration is returned by invoking {@link #getForNotTerminated()}.
	 * 
	 * <p>
	 * Otherwise the string is parsed as a double value and the result of
	 * {@link #getForDouble(double)} is returned.
	 * 
	 * @param stringValue
	 *            A string representation of the clustering quality.
	 * @return A clustering quality value wrapper object corresponding to the
	 *         given string.
	 */
	public static ClusteringQualityMeasureValue parseFromString(
			String stringValue) {
		if (stringValue.equals("NT"))
			return NOT_TERMINATED;
		else if (stringValue.equals("NA")) {
			return NOT_AVAILABLE;
		}
		return ClusteringQualityMeasureValue
				.getForDouble(Double.valueOf(stringValue));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.cluster.quality.IClusteringQualityMeasureValue#getValue()
	 */
	@Override
	public double getValue() {
		return this.value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.cluster.quality.IClusteringQualityMeasureValue#isTerminated(
	 * )
	 */
	@Override
	public boolean isTerminated() {
		return this.isTerminated;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.cluster.quality.IClusteringQualityMeasureValue#hasValue()
	 */
	@Override
	public boolean hasValue() {
		return value != null;
	}
}
