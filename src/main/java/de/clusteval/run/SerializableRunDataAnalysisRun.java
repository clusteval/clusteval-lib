/**
 * 
 */
package de.clusteval.run;

import java.io.File;
import java.util.List;

import de.clusteval.context.ISerializableContext;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.run.statistics.IRunDataStatistic;
import de.clusteval.run.statistics.IRunStatistic;
import de.clusteval.utils.ISerializableStatistic;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * @author Christian Wiwie
 *
 */
public class SerializableRunDataAnalysisRun<R extends IRunDataAnalysisRun>
		extends
			SerializableAnalysisRun<R>
		implements
			ISerializableRunDataAnalysisRun<R> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7278741323038093428L;

	protected List<String> uniqueRunAnalysisRunIdentifiers,
			uniqueDataAnalysisRunIdentifiers;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 * @param context
	 * @param statistics
	 */
	public SerializableRunDataAnalysisRun(File absPath, String name,
			String version, ISerializableContext context,
			List<ISerializableStatistic> statistics,
			List<String> uniqueRunAnalysisRunIdentifiers,
			List<String> uniqueDataAnalysisRunIdentifiers) {
		super(absPath, name, version, context, statistics);
		this.uniqueRunAnalysisRunIdentifiers = uniqueRunAnalysisRunIdentifiers;
		this.uniqueDataAnalysisRunIdentifiers = uniqueDataAnalysisRunIdentifiers;
	}

	public SerializableRunDataAnalysisRun(final R run) throws RepositoryObjectSerializationException {
		super(run);
		this.uniqueRunAnalysisRunIdentifiers = run
				.getUniqueRunAnalysisRunIdentifiers();
		this.uniqueDataAnalysisRunIdentifiers = run
				.getUniqueDataAnalysisRunIdentifiers();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ISerializableRunDataAnalysisRun#
	 * getUniqueDataAnalysisRunIdentifiers()
	 */
	@Override
	public List<String> getUniqueDataAnalysisRunIdentifiers() {
		return this.uniqueDataAnalysisRunIdentifiers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ISerializableRunDataAnalysisRun#
	 * getUniqueRunAnalysisRunIdentifiers()
	 */
	@Override
	public List<String> getUniqueRunAnalysisRunIdentifiers() {
		return this.uniqueRunAnalysisRunIdentifiers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.SerializableRun#deserializeInternal(de.clusteval.
	 * framework.repository.IRepository)
	 */
	@Override
	protected R deserializeInternal(IRepository repository)
			throws DeserializationException {
		List<IRunDataStatistic> statistics = (List) deserializeStatistics(
				repository);

		return (R) new RunDataAnalysisRun(repository,
				this.context.deserialize(repository),
				System.currentTimeMillis(),
				new File(FileUtils.buildPath(repository.getBasePath(IRun.class),
						this.name + ".v" + version + ".run")),
				this.uniqueRunAnalysisRunIdentifiers,
				this.uniqueDataAnalysisRunIdentifiers, statistics);

	}
}
