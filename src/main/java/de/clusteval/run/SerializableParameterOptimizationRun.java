/**
 * 
 */
package de.clusteval.run;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod;
import de.clusteval.cluster.paramOptimization.ISerializableParameterOptimizationMethod;
import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.cluster.quality.ISerializableClusteringQualityMeasure;
import de.clusteval.context.IContext;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.ISerializableDataConfig;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.program.ISerializableProgramConfig;
import de.clusteval.run.runresult.postprocessing.IRunResultPostprocessor;
import de.clusteval.run.runresult.postprocessing.ISerializableRunResultPostprocessor;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * @author Christian Wiwie
 *
 */
public class SerializableParameterOptimizationRun<R extends IParameterOptimizationRun>
		extends
			SerializableExecutionRun<R>
		implements
			ISerializableParameterOptimizationRun<R> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6404524273038677731L;

	protected final List<ISerializableParameterOptimizationMethod> optimizationMethods;

	protected final ISerializableWrapperRepositoryObject<IClusteringQualityMeasure> optimizationCriterion;

	protected final int totalIterationCount;

	protected final Map<ISerializableProgramConfig<? extends IProgramConfig>, List<IProgramParameter<?>>> optimizationParameters;

	protected final Map<ISerializableProgramConfig<? extends IProgramConfig>, Map<IProgramParameter<?>, String>> optimizationParameterMaxValueOverrides;

	protected final Map<ISerializableProgramConfig<? extends IProgramConfig>, Map<IProgramParameter<?>, String>> optimizationParameterMinValueOverrides;

	protected final Map<ISerializableProgramConfig<? extends IProgramConfig>, Map<IProgramParameter<?>, String[]>> optimizationParameterOptionsOverrides;

	protected final String defaultOptimizationMethod;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 * @param context
	 * @param dataConfigs
	 * @param programConfigs
	 * @param postProcessors
	 * @param qualityMeasures
	 * @param fixedParameterValues
	 * @param optimizationParameters
	 * @param defaultOptimizationMethod
	 * @param optimizationMethods
	 * @param optimizationCriterion
	 * @param totalIterationCount
	 * @param optimizationParameterMaxValueOverrides
	 * @param optimizationParameterMinValueOverrides
	 * @param optimizationParameterOptionsOverrides
	 * @param maxExecutionTimes
	 */
	public SerializableParameterOptimizationRun(File absPath, String name,
			String version,
			ISerializableWrapperRepositoryObject<IContext> context,
			List<ISerializableDataConfig> dataConfigs,
			List<ISerializableProgramConfig<? extends IProgramConfig>> programConfigs,
			List<ISerializableRunResultPostprocessor> postProcessors,
			List<ISerializableClusteringQualityMeasure> qualityMeasures,
			final Map<ISerializableProgramConfig<? extends IProgramConfig>, Map<IProgramParameter<?>, String>> fixedParameterValues,
			Map<ISerializableProgramConfig<? extends IProgramConfig>, List<IProgramParameter<?>>> optimizationParameters,
			String defaultOptimizationMethod,
			List<ISerializableParameterOptimizationMethod> optimizationMethods,
			ISerializableWrapperRepositoryObject<IClusteringQualityMeasure> optimizationCriterion,
			final int totalIterationCount,
			Map<ISerializableProgramConfig<? extends IProgramConfig>, Map<IProgramParameter<?>, String>> optimizationParameterMaxValueOverrides,
			Map<ISerializableProgramConfig<? extends IProgramConfig>, Map<IProgramParameter<?>, String>> optimizationParameterMinValueOverrides,
			Map<ISerializableProgramConfig<? extends IProgramConfig>, Map<IProgramParameter<?>, String[]>> optimizationParameterOptionsOverrides,
			final Map<String, Integer> maxExecutionTimes) {
		super(absPath, name, version, context, dataConfigs, programConfigs,
				postProcessors, fixedParameterValues, qualityMeasures,
				maxExecutionTimes);
		this.optimizationParameters = optimizationParameters;
		this.optimizationCriterion = optimizationCriterion;
		this.defaultOptimizationMethod = defaultOptimizationMethod;
		this.optimizationMethods = optimizationMethods;
		this.totalIterationCount = totalIterationCount;
		this.optimizationParameterMaxValueOverrides = optimizationParameterMaxValueOverrides;
		this.optimizationParameterMinValueOverrides = optimizationParameterMinValueOverrides;
		this.optimizationParameterOptionsOverrides = optimizationParameterOptionsOverrides;
	}

	public SerializableParameterOptimizationRun(final R run)
			throws RepositoryObjectSerializationException {
		super(run);
		this.optimizationMethods = new ArrayList<>();
		for (IParameterOptimizationMethod m : run.getOptimizationMethods()) {
			ISerializableParameterOptimizationMethod serializedMethod = null;
			if (m != null) {
				serializedMethod = m.asSerializable();
				serializedMethod.setRun(this);
			}
			this.optimizationMethods.add(serializedMethod);
		}
		if (run.getOptimizationCriterion() != null)
			this.optimizationCriterion = run.getOptimizationCriterion()
					.asSerializable();
		else
			this.optimizationCriterion = null;

		this.optimizationParameters = new HashMap<>();
		for (IProgramConfig pc : run.getOptimizationParameters().keySet())
			this.optimizationParameters.put(pc.asSerializable(),
					run.getOptimizationParameters().get(pc));
		this.defaultOptimizationMethod = run.getDefaultOptimizationMethod();

		this.totalIterationCount = run.getDesiredTotalIterationCount();

		this.optimizationParameterMaxValueOverrides = new HashMap<>();
		for (Entry<IProgramConfig, Map<IProgramParameter<?>, String>> e : run
				.getOptimizationParameterMaxValueOverrides().entrySet())
			this.optimizationParameterMaxValueOverrides
					.put(e.getKey().asSerializable(), e.getValue());

		this.optimizationParameterMinValueOverrides = new HashMap<>();
		for (Entry<IProgramConfig, Map<IProgramParameter<?>, String>> e : run
				.getOptimizationParameterMinValueOverrides().entrySet())
			this.optimizationParameterMinValueOverrides
					.put(e.getKey().asSerializable(), e.getValue());

		this.optimizationParameterOptionsOverrides = new HashMap<>();
		for (Entry<IProgramConfig, Map<IProgramParameter<?>, String[]>> e : run
				.getOptimizationParameterOptionsOverrides().entrySet())
			this.optimizationParameterOptionsOverrides
					.put(e.getKey().asSerializable(), e.getValue());
	}

	/**
	 * @return the optimizationMethods
	 */
	@Override
	public List<ISerializableParameterOptimizationMethod> getOptimizationMethods() {
		return optimizationMethods;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ISerializableParameterOptimizationRun#
	 * getOptimizationCriterion()
	 */
	@Override
	public ISerializableWrapperRepositoryObject<IClusteringQualityMeasure> getOptimizationCriterion() {
		return optimizationCriterion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ISerializableParameterOptimizationRun#
	 * getTotalIterationCount()
	 */
	@Override
	public int getTotalIterationCount() {
		return this.totalIterationCount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.SerializableRun#deserializeInternal(de.clusteval.
	 * framework.repository.IRepository)
	 */
	@Override
	protected R deserializeInternal(IRepository repository)
			throws DeserializationException {
		List<IProgramConfig> programConfigs = deserializeProgramConfigs(
				repository);
		List<IDataConfig> dataConfigs = deserializeDataConfigs(repository);
		List<IClusteringQualityMeasure> qualityMeasures = deserializeClusteringQualityMeasures(
				repository);

		Map<IProgramConfig, List<IProgramParameter<?>>> optimizationParameters = new HashMap<>();
		for (ISerializableProgramConfig pc : this.optimizationParameters
				.keySet())
			optimizationParameters.put(pc.deserialize(repository),
					this.optimizationParameters.get(pc));

		Map<IProgramConfig, Map<IProgramParameter<?>, String>> optimizationParameterMaxValueOverrides = new HashMap<>();
		for (ISerializableProgramConfig pc : this.optimizationParameterMaxValueOverrides
				.keySet())
			optimizationParameterMaxValueOverrides.put(
					pc.deserialize(repository),
					this.optimizationParameterMaxValueOverrides.get(pc));

		Map<IProgramConfig, Map<IProgramParameter<?>, String>> optimizationParameterMinValueOverrides = new HashMap<>();
		for (ISerializableProgramConfig pc : this.optimizationParameterMinValueOverrides
				.keySet())
			optimizationParameterMinValueOverrides.put(
					pc.deserialize(repository),
					this.optimizationParameterMinValueOverrides.get(pc));

		Map<IProgramConfig, Map<IProgramParameter<?>, String[]>> optimizationParameterOptionsOverrides = new HashMap<>();
		for (ISerializableProgramConfig pc : this.optimizationParameterOptionsOverrides
				.keySet())
			optimizationParameterOptionsOverrides.put(
					pc.deserialize(repository),
					this.optimizationParameterOptionsOverrides.get(pc));

		List<IParameterOptimizationMethod> optimizationMethods = new ArrayList<>();
		List<IRunResultPostprocessor> postProcessors = deserializeRunResultPostprocessor(
				repository);

		Map<IProgramConfig, Map<IProgramParameter<?>, String>> fixedParameterValues = deserializeFixedParameterValues(
				repository);

		R deserializedRun = (R) new ParameterOptimizationRun(repository,
				this.context.deserialize(repository),
				System.currentTimeMillis(),
				new File(FileUtils.buildPath(repository.getBasePath(IRun.class),
						this.name + ".v" + version + ".run")),
				programConfigs, dataConfigs, qualityMeasures,
				fixedParameterValues, this.totalIterationCount,
				this.optimizationCriterion.deserialize(repository),
				optimizationParameters, optimizationParameterMinValueOverrides,
				optimizationParameterMaxValueOverrides,
				optimizationParameterOptionsOverrides,
				defaultOptimizationMethod, optimizationMethods, postProcessors,
				this.maxExecutionTimes);

		optimizationMethods.addAll(
				deserializeOptimizationMethods(deserializedRun, repository));
		return deserializedRun;
	}

	protected List<IParameterOptimizationMethod> deserializeOptimizationMethods(
			final R deserializedRun, final IRepository repository)
			throws DeserializationException {
		List<IParameterOptimizationMethod> optimizationMethods = new ArrayList<>();
		for (ISerializableParameterOptimizationMethod pom : this.optimizationMethods) {
			IParameterOptimizationMethod deserialize = pom
					.deserialize(repository);
			deserialize.setRun(deserializedRun);
			optimizationMethods.add(deserialize);
		}
		return optimizationMethods;
	}

	/**
	 * @return the optimizationParameterMaxValueOverrides
	 */
	@Override
	public Map<ISerializableProgramConfig<? extends IProgramConfig>, Map<IProgramParameter<?>, String>> getOptimizationParameterMaxValueOverrides() {
		return optimizationParameterMaxValueOverrides;
	}

	/**
	 * @return the optimizationParameterMinValueOverrides
	 */
	@Override
	public Map<ISerializableProgramConfig<? extends IProgramConfig>, Map<IProgramParameter<?>, String>> getOptimizationParameterMinValueOverrides() {
		return optimizationParameterMinValueOverrides;
	}

	/**
	 * @return the optimizationParameterOptionsOverrides
	 */
	@Override
	public Map<ISerializableProgramConfig<? extends IProgramConfig>, Map<IProgramParameter<?>, String[]>> getOptimizationParameterOptionsOverrides() {
		return optimizationParameterOptionsOverrides;
	}

	/**
	 * @return the optimizationParameters
	 */
	@Override
	public Map<ISerializableProgramConfig<? extends IProgramConfig>, List<IProgramParameter<?>>> getOptimizationParameters() {
		return optimizationParameters;
	}
}
