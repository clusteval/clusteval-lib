/**
 * 
 */
package de.clusteval.run;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import de.clusteval.context.ISerializableContext;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.ISerializableDataConfig;
import de.clusteval.data.statistics.IDataStatistic;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.utils.ISerializableStatistic;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * @author Christian Wiwie
 *
 */
public class SerializableDataAnalysisRun<R extends IDataAnalysisRun>
		extends
			SerializableAnalysisRun<R>
		implements
			ISerializableDataAnalysisRun<R> {

	protected List<ISerializableDataConfig> dataConfigs;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 * @param context
	 * @param statistics
	 * @param dataConfigs
	 */
	public SerializableDataAnalysisRun(File absPath, String name,
			String version, ISerializableContext context,
			List<ISerializableStatistic> statistics,
			List<ISerializableDataConfig> dataConfigs) {
		super(absPath, name, version, context, statistics);
		this.dataConfigs = dataConfigs;
	}

	public SerializableDataAnalysisRun(final R run)
			throws RepositoryObjectSerializationException {
		super(run);
		this.dataConfigs = new ArrayList<>();
		if (run.getDataConfigs() != null)
			for (IDataConfig dc : run.getDataConfigs())
				this.dataConfigs.add(dc != null ? dc.asSerializable() : null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ISerializableDataAnalysisRun#getDataConfigs()
	 */
	@Override
	public List<ISerializableDataConfig> getDataConfigs() {
		return dataConfigs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.SerializableRun#deserializeInternal(de.clusteval.
	 * framework.repository.IRepository)
	 */
	@Override
	protected R deserializeInternal(IRepository repository)
			throws DeserializationException {
		List<IDataConfig> deserializeDataConfigs = deserializeDataConfigs(
				repository);
		List<IDataStatistic> statistics = (List) deserializeStatistics(
				repository);

		return (R) new DataAnalysisRun(repository,
				this.context.deserialize(repository),
				System.currentTimeMillis(),
				new File(FileUtils.buildPath(repository.getBasePath(IRun.class),
						this.name + ".v" + version + ".run")),
				deserializeDataConfigs, statistics);
	}

	protected List<IDataConfig> deserializeDataConfigs(IRepository repository)
			throws DeserializationException {
		List<IDataConfig> dataConfigs = new ArrayList<>();
		for (ISerializableDataConfig obj : this.dataConfigs)
			dataConfigs.add(obj.deserialize(repository));
		return dataConfigs;
	}
}
