/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
package de.clusteval.run;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.HierarchicalINIConfiguration;
import org.apache.commons.configuration.SubnodeConfiguration;

import de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod;
import de.clusteval.cluster.paramOptimization.IncompatibleParameterOptimizationMethodException;
import de.clusteval.cluster.paramOptimization.ParameterOptimizationMethod;
import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.cluster.quality.IClusteringQualitySet;
import de.clusteval.context.IContext;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.format.AbsoluteDataSetFormat;
import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryEvent;
import de.clusteval.framework.repository.RepositoryObjectDumpException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.RepositoryRemoveEvent;
import de.clusteval.framework.threading.IRunSchedulerThread;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.program.ParameterSet;
import de.clusteval.program.ProgramConfig;
import de.clusteval.program.ProgramParameter;
import de.clusteval.run.runnable.IParameterOptimizationRunRunnable;
import de.clusteval.run.runnable.IRunRunnable;
import de.clusteval.run.runnable.ParameterOptimizationRunRunnable;
import de.clusteval.run.runresult.IParameterOptimizationResult;
import de.clusteval.run.runresult.RunResultParseException;
import de.clusteval.run.runresult.postprocessing.IRunResultPostprocessor;
import dk.sdu.imada.compbio.utils.Pair;
import dk.sdu.imada.compbio.utils.StringExt;

/**
 * A type of execution run that performs several clusterings with different
 * parameter sets determined in an automatized way for every pair of program and
 * data configuration.
 * 
 * <p>
 * The evaluated parameter sets during a parameter optimization for one pair of
 * program and data configuration are determined by the corresponding
 * {@link ParameterOptimizationMethod} stored in {@link #optimizationMethods}.
 * 
 * <p>
 * Every evaluated parameter set is stored in the
 * {@link #optimizationParameters} attribute, such that evaluation of the
 * results is possible after termination of the run.
 * 
 * <p>
 * The results of the clusterings evaluated for every parameter set are also
 * stored in the {@link ParameterOptimizationMethod} object.
 * 
 * @author Christian Wiwie
 * 
 */
public class ParameterOptimizationRun
		extends
			ExecutionRun<IParameterOptimizationRunRunnable>
		implements
			IParameterOptimizationRun {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9129218429105300822L;

	/**
	 * This method verifies compatibility between a parameter optimization
	 * method, the data input format and the program configuration.
	 * 
	 * <p>
	 * Some parameter optimization do only work for certain programs, e.g.
	 * {@link GapStatisticParameterOptimizationMethod } works only for
	 * {@link KMeansClusteringRProgram} and {@link AbsoluteDataSetFormat}.
	 * 
	 * @param dataConfigs
	 * @param programConfigs
	 * @param optimizationMethods
	 * @throws IncompatibleParameterOptimizationMethodException
	 * 
	 */
	public static void checkCompatibilityParameterOptimizationMethod(
			final List<IParameterOptimizationMethod> optimizationMethods,
			final List<IProgramConfig> programConfigs,
			final List<IDataConfig> dataConfigs)
			throws IncompatibleParameterOptimizationMethodException {
		for (IParameterOptimizationMethod method : optimizationMethods) {
			if (!method.getCompatibleDataSetFormatBaseClasses().isEmpty()) {
				// for every datasetformat we check, whether it class is
				// compatible
				for (IDataConfig dataConfig : dataConfigs) {
					Class<? extends IDataSetFormat> dataSetFormatClass = dataConfig
							.getDatasetConfig().getDataSet().getDataSetFormat()
							.getClass();
					boolean compatible = false;
					for (Class<? extends IDataSetFormat> parentClass : method
							.getCompatibleDataSetFormatBaseClasses()) {
						if (parentClass.isAssignableFrom(dataSetFormatClass)) {
							compatible = true;
							break;
						}
					}
					if (!compatible) {
						throw new IncompatibleParameterOptimizationMethodException(
								"The ParameterOptimizationMethod "
										+ method.getClass().getSimpleName()
										+ " cannot be applied to the dataset "
										+ dataConfig.getDatasetConfig()
												.getDataSet()
										+ " with the format "
										+ dataSetFormatClass.getSimpleName());
					}
				}
			}

			if (!method.getCompatibleProgramNames().isEmpty()) {
				// for every program we check, whether it class is
				// compatible
				for (IProgramConfig programConfig : programConfigs) {
					String programName = programConfig.getProgram()
							.getMajorName();
					boolean compatible = method.getCompatibleProgramNames()
							.contains(programName);
					if (!compatible) {
						throw new IncompatibleParameterOptimizationMethodException(
								"The ParameterOptimizationMethod "
										+ method.getClass().getSimpleName()
										+ " cannot be applied to the program "
										+ programName);
					}
				}
			}
		}
	}

	/**
	 * This list holds another list of optimization parameters for every program
	 * configuration. These optimization parameters are to be optimized by this
	 * run.
	 */
	protected Map<IProgramConfig, List<IProgramParameter<?>>> optimizationParameters;

	/**
	 * During a parameter optimization run for each calcuated clustering several
	 * quality measures are assessed. One of these quality measures is used, to
	 * rate the clusterings and to decide, which clusterings performed best.
	 * This quality measure is called the optimization criterion and is stored
	 * in this attribute.
	 */
	protected transient IClusteringQualityMeasure optimizationCriterion;

	protected String defaultOptimizationMethod;

	/**
	 * This list holds the parameter optimization methods for every pair of
	 * program and data configuration. These method objects control and keep
	 * track of the parameter sets and the results.
	 */
	protected transient List<IParameterOptimizationMethod> optimizationMethods;

	/**
	 * Parameter min and max values as well as options can be overriden on a
	 * per-run basis. These override values will be stored here.
	 */
	protected Map<IProgramConfig, Map<IProgramParameter<?>, String>> optimizationParameterMinValueOverrides,
			optimizationParameterMaxValueOverrides;
	protected Map<IProgramConfig, Map<IProgramParameter<?>, String[]>> optimizationParameterOptionsOverrides;

	protected int desiredTotalIterationCount;

	/**
	 * New objects of this type are automatically registered at the repository.
	 * 
	 * @param repository
	 *            the repository
	 * @param context
	 * @param changeDate
	 *            The date this run was performed.
	 * @param absPath
	 *            The absolute path to the file on the filesystem that
	 *            corresponds to this run.
	 * @param programConfigs
	 *            The program configurations of the new run.
	 * @param dataConfigs
	 *            The data configurations of the new run.
	 * @param qualityMeasures
	 *            The clustering quality measures of the new run.
	 * @param fixedParameterValues
	 *            A map for each pair of program config and data config, that
	 *            contains parameters which are fixed to specific values during
	 * @param totalIterationCount
	 * @param optimizationCriterion
	 * @param optimizationParameters
	 *            The parameters that are to be optimized during this run.
	 * @param optimizationParameterMinValueOverrides
	 * @param optimizationParameterMaxValueOverrides
	 * @param optimizationParameterOptionsOverrides
	 * @param defaultOptimizationMethod
	 * @param optimizationMethods
	 *            The parameter optimization methods determines which parameter
	 *            sets are to be evaluated and stores the results.
	 * @param postProcessors
	 * @param maxExecutionTimes
	 */
	public ParameterOptimizationRun(final IRepository repository,
			final IContext context, final long changeDate, final File absPath,
			final List<IProgramConfig> programConfigs,
			final List<IDataConfig> dataConfigs,
			final List<IClusteringQualityMeasure> qualityMeasures,
			final Map<IProgramConfig, Map<IProgramParameter<?>, String>> fixedParameterValues,
			final int totalIterationCount,
			final IClusteringQualityMeasure optimizationCriterion,
			final Map<IProgramConfig, List<IProgramParameter<?>>> optimizationParameters,
			final Map<IProgramConfig, Map<IProgramParameter<?>, String>> optimizationParameterMinValueOverrides,
			final Map<IProgramConfig, Map<IProgramParameter<?>, String>> optimizationParameterMaxValueOverrides,
			final Map<IProgramConfig, Map<IProgramParameter<?>, String[]>> optimizationParameterOptionsOverrides,
			final String defaultOptimizationMethod,
			final List<IParameterOptimizationMethod> optimizationMethods,
			final List<IRunResultPostprocessor> postProcessors,
			final Map<String, Integer> maxExecutionTimes) {
		super(repository, context, changeDate, absPath, programConfigs,
				dataConfigs, qualityMeasures, fixedParameterValues,
				postProcessors, maxExecutionTimes);

		this.desiredTotalIterationCount = totalIterationCount;
		this.optimizationCriterion = optimizationCriterion;
		this.optimizationParameters = optimizationParameters;
		this.optimizationParameterMinValueOverrides = optimizationParameterMinValueOverrides;
		this.optimizationParameterMaxValueOverrides = optimizationParameterMaxValueOverrides;
		this.optimizationParameterOptionsOverrides = optimizationParameterOptionsOverrides;
		this.defaultOptimizationMethod = defaultOptimizationMethod;
		this.optimizationMethods = optimizationMethods;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ExecutionRun#register()
	 */
	@Override
	public boolean register() throws RegisterException {
		if (super.register()) {
			// register this Run at all dataconfigs and programconfigs
			for (IDataConfig dataConfig : this.originalDataConfigs) {
				dataConfig.addListener(this);
			}
			for (IProgramConfig programConfig : this.originalProgramConfigs) {
				programConfig.addListener(this);
			}
			for (IParameterOptimizationMethod method : this.optimizationMethods)
				method.addListener(this);

			for (IClusteringQualityMeasure measure : this.qualityMeasures) {
				// added 21.03.2013: measures are only registered here, if this
				// run has been registered
				measure.register();
				measure.addListener(this);
			}
			return true;
		}
		return false;
	}

	/**
	 * Copy constructor of parameter optimization runs.
	 * 
	 * @param otherRun
	 *            The parameter optimization run to be cloned.
	 * @throws RegisterException
	 */
	protected ParameterOptimizationRun(final ParameterOptimizationRun otherRun)
			throws RegisterException {
		super(otherRun);
		this.optimizationCriterion = otherRun.optimizationCriterion.clone();
		this.optimizationMethods = ParameterOptimizationMethod
				.cloneOptimizationMethods(otherRun.optimizationMethods);
		this.optimizationParameters = ProgramParameter
				.cloneParameterListList(otherRun.optimizationParameters);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.ExecutionRun#createRunRunnableFor(de.clusteval.framework
	 * .threading.IRunSchedulerThread, de.clusteval.run.ExecutionRun,
	 * de.clusteval.program.IProgramConfig, de.clusteval.data.IDataConfig,
	 * java.lang.String, boolean, java.util.Map)
	 */
	@Override
	protected <T extends ExecutionRun<IParameterOptimizationRunRunnable>> IParameterOptimizationRunRunnable createRunRunnableFor(
			IRunSchedulerThread runScheduler, T run,
			IProgramConfig programConfig, IDataConfig dataConfig,
			String runIdentString, boolean isResume,
			Map<IProgramParameter<?>, String> runParams) {

		// 06.04.2013: changed from indexOf to this manual search, because at
		// this point the passed programConfig and dataConfig are moved clones
		// of the originals in #runPairs
		int p = -1;
		for (int i = 0; i < ((IParameterOptimizationRun) run)
				.getClonedProgramAndDataConfigPairs().size(); i++) {
			Pair<IProgramConfig, IDataConfig> pair = ((IParameterOptimizationRun) run)
					.getClonedProgramAndDataConfigPairs().get(i);
			if (pair.getFirst().getName().equals(programConfig.getName())
					&& pair.getSecond().getName()
							.equals(dataConfig.getName())) {
				p = i;
				break;
			}
		}

		IParameterOptimizationMethod optimizationMethod = ((IParameterOptimizationRun) run)
				.getOptimizationMethods().get(p);
		ParameterOptimizationRunRunnable t = new ParameterOptimizationRunRunnable(
				runScheduler, (IParameterOptimizationRun) run,
				(ProgramConfig) programConfig, (IDataConfig) dataConfig,
				optimizationMethod, runIdentString, isResume, runParams);
		((Run) run).progress.addSubProgress(t.getProgressPrinter(), 10000);
		((Run) run).progressBeforeExecution
				.addSubProgress(t.getProgressPrinterBeforeExecution(), 10000);
		return t;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.Run#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IParameterOptimizationRun#clone()
	 */
	@Override
	public ParameterOptimizationRun clone() {
		try {
			return new ParameterOptimizationRun(this);
		} catch (RegisterException e) {
			// should not occur
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.ExecutionRun#notify(framework.repository.RepositoryEvent)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.IParameterOptimizationRun#notify(de.clusteval.framework.
	 * repository.RepositoryEvent)
	 */
	@Override
	public void notify(RepositoryEvent e) throws RegisterException {
		super.notify(e);
		if (e instanceof RepositoryRemoveEvent) {
			RepositoryRemoveEvent event = (RepositoryRemoveEvent) e;
			if (optimizationMethods.contains(event.getRemovedObject())) {
				event.getRemovedObject().removeListener(this);
				this.log.info("Run " + this
						+ ": Removed, because ParameterOptimizationMethod "
						+ event.getRemovedObject() + " has changed.");
				RepositoryRemoveEvent newEvent = new RepositoryRemoveEvent(
						this);
				this.unregister();
				this.notify(newEvent);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.IParameterOptimizationRun#getOptimizationParameters()
	 */
	@Override
	public Map<IProgramConfig, List<IProgramParameter<?>>> getOptimizationParameters() {
		return this.optimizationParameters;
	}

	/**
	 * @return the defaultOptimizationMethod
	 */
	@Override
	public String getDefaultOptimizationMethod() {
		return defaultOptimizationMethod;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IParameterOptimizationRun#getOptimizationMethods()
	 */
	@Override
	public List<IParameterOptimizationMethod> getOptimizationMethods() {
		return this.optimizationMethods;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IParameterOptimizationRun#getOptimizationStatus()
	 */
	@Override
	public Map<Pair<String, String>, Pair<Double, Map<String, Pair<Map<String, String>, String>>>> getOptimizationStatus() {
		Map<Pair<String, String>, Pair<Double, Map<String, Pair<Map<String, String>, String>>>> result = new HashMap<Pair<String, String>, Pair<Double, Map<String, Pair<Map<String, String>, String>>>>();
		try {
			for (IRunRunnable t : this.runnables) {
				ParameterOptimizationRunRunnable thread = (ParameterOptimizationRunRunnable) t;
				Pair<String, String> configs = Pair.getPair(
						thread.getProgramConfig().toString(),
						thread.getDataConfig().toString());

				IParameterOptimizationResult paramOptRes = thread
						.getOptimizationMethod().getResult();

				if (paramOptRes != null) {
					boolean isInMemory = paramOptRes.isInMemory();
					if (!isInMemory)
						try {
							paramOptRes.loadIntoMemory();
							isInMemory = paramOptRes.isInMemory();
						} catch (RunResultParseException e) {
							isInMemory = false;
						}
					try {

						// measure -> best qualities
						Map<String, Pair<Map<String, String>, String>> qualities = new HashMap<String, Pair<Map<String, String>, String>>();
						// has the runnable already initialized the optimization
						// method
						// and result?
						if (paramOptRes != null && isInMemory) {
							// get the best achieved qualities
							IClusteringQualitySet bestQuals = thread
									.getOptimizationMethod().getResult()
									.getOptimalCriterionValue();
							// get the optimal parameter values
							Map<IClusteringQualityMeasure, Long> bestParams = thread
									.getOptimizationMethod().getResult()
									.getOptimalIterations();

							// measure -> best parameters
							Map<IClusteringQualityMeasure, Map<String, String>> bestParamsMap = new HashMap<IClusteringQualityMeasure, Map<String, String>>();
							for (IClusteringQualityMeasure measure : bestParams
									.keySet()) {
								ParameterSet pSet = thread
										.getOptimizationMethod().getResult()
										.getParameterSets()
										.get(bestParams.get(measure));
								Map<String, String> tmp = new HashMap<String, String>();
								for (String p : pSet.keySet())
									tmp.put(p.toString(), pSet.get(p));

								bestParamsMap.put(measure, tmp);
							}

							for (IClusteringQualityMeasure measure : bestQuals
									.keySet())
								qualities.put(measure.getAlias(), Pair.getPair(
										bestParamsMap.get(measure),
										bestQuals.get(measure).toString()));
						}

						result.put(configs,
								new Pair<Double, Map<String, Pair<Map<String, String>, String>>>(
										(double) t.getProgressPrinter()
												.getPercent(),
										qualities));
					} finally {
						if (!isInMemory)
							paramOptRes.unloadFromMemory();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.ExecutionRun#ensureVersionsForAllComponents(java.lang.
	 * String)
	 */
	@Override
	protected String ensureVersionsForAllComponents(String fileContents) {
		String newFileContents = super.ensureVersionsForAllComponents(
				fileContents);

		for (IRepositoryObject r : optimizationMethods) {
			newFileContents = ensureVersionForComponent(newFileContents, r);
		}

		return newFileContents;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ExecutionRun#asSerializable()
	 */
	@Override
	public SerializableParameterOptimizationRun asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableParameterOptimizationRun) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ExecutionRun#asSerializable()
	 */
	@Override
	public SerializableParameterOptimizationRun asSerializableInternal()
			throws RepositoryObjectSerializationException {
		return new SerializableParameterOptimizationRun(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod#
	 * getOptimizationCriterion()
	 */
	@Override
	public final IClusteringQualityMeasure getOptimizationCriterion() {
		return this.optimizationCriterion;
	}

	/**
	 * @return the totalIterationCount
	 */
	@Override
	public int getDesiredTotalIterationCount() {
		return desiredTotalIterationCount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ExecutionRun#dumpToFileHelper(org.apache.commons.
	 * configuration.HierarchicalINIConfiguration)
	 */
	@Override
	protected void dumpToFileHelper(HierarchicalINIConfiguration conf)
			throws RepositoryObjectDumpException {
		super.dumpToFileHelper(conf);

		conf.addProperty("optimizationCriterion", optimizationCriterion);
		conf.addProperty("optimizationIterations", desiredTotalIterationCount);
		conf.addProperty("optimizationMethod", defaultOptimizationMethod);

		for (IParameterOptimizationMethod method : this.optimizationMethods) {
			IProgramConfig programConfig = method.getProgramConfig();
			List<IProgramParameter<?>> optimizationParameter = method
					.getOptimizationParameter();

			if (optimizationParameter.isEmpty())
				continue;

			if (conf.getSections().contains(programConfig.toString()))
				continue;

			SubnodeConfiguration subConf = conf
					.getSection(programConfig.toString());
			subConf.addProperty("optimizationParameters",
					StringExt.paste(",", optimizationParameter));
			subConf.addProperty("optimizationMethod", method.toString());
		}

		dumpOptimizationParameterOverridesToFile(conf);
	}

	protected void dumpOptimizationParameterOverridesToFile(
			HierarchicalINIConfiguration conf) {
		for (IProgramConfig programConfig : this.originalProgramConfigs) {
			Map<IProgramParameter<?>, String> minValues = optimizationParameterMinValueOverrides
					.get(programConfig);
			Map<IProgramParameter<?>, String> maxValues = optimizationParameterMaxValueOverrides
					.get(programConfig);
			Map<IProgramParameter<?>, String[]> options = optimizationParameterOptionsOverrides
					.get(programConfig);

			for (IProgramParameter<?> programParameter : optimizationParameters
					.get(programConfig)) {
				SubnodeConfiguration section;
				if (minValues != null
						&& minValues.containsKey(programParameter)) {
					section = conf.getSection(programConfig.toString() + ":"
							+ programParameter.getName());
					section.addProperty("minValue",
							minValues.get(programParameter));
				}
				if (maxValues != null
						&& maxValues.containsKey(programParameter)) {
					section = conf.getSection(programConfig.toString() + ":"
							+ programParameter.getName());
					section.addProperty("maxValue",
							maxValues.get(programParameter));
				}
				if (options != null && options.containsKey(programParameter)) {
					section = conf.getSection(programConfig.toString() + ":"
							+ programParameter.getName());
					section.addProperty("options",
							options.get(programParameter));
				}
			}
		}
	}

	/**
	 * @return the optimizationParameterMaxValueOverrides
	 */
	@Override
	public Map<IProgramConfig, Map<IProgramParameter<?>, String>> getOptimizationParameterMaxValueOverrides() {
		return optimizationParameterMaxValueOverrides;
	}

	/**
	 * @return the optimizationParameterMinValueOverrides
	 */
	@Override
	public Map<IProgramConfig, Map<IProgramParameter<?>, String>> getOptimizationParameterMinValueOverrides() {
		return optimizationParameterMinValueOverrides;
	}

	/**
	 * @return the optimizationParameterOptionsOverrides
	 */
	@Override
	public Map<IProgramConfig, Map<IProgramParameter<?>, String[]>> getOptimizationParameterOptionsOverrides() {
		return optimizationParameterOptionsOverrides;
	}
}
