/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.cluster.quality.ClusteringQualityMeasure;
import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.cluster.quality.UnknownClusteringQualityMeasureException;
import de.clusteval.context.Context;
import de.clusteval.context.IContext;
import de.clusteval.context.UnknownContextException;
import de.clusteval.data.DataConfig;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.AbsoluteDataSet;
import de.clusteval.data.dataset.DataMatrix;
import de.clusteval.data.dataset.DataSetConfig;
import de.clusteval.data.dataset.IDataSet;
import de.clusteval.data.dataset.IDataSet.WEBSITE_VISIBILITY;
import de.clusteval.data.dataset.IDataSetConfig;
import de.clusteval.data.dataset.format.ConversionInputToStandardConfiguration;
import de.clusteval.data.dataset.format.ConversionStandardToInputConfiguration;
import de.clusteval.data.dataset.format.DataSetFormat;
import de.clusteval.data.dataset.format.IAbsoluteDataSetFormat;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.dataset.type.DataSetType;
import de.clusteval.data.dataset.type.UnknownDataSetTypeException;
import de.clusteval.data.distance.DistanceMeasure;
import de.clusteval.data.distance.UnknownDistanceMeasureException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryObjectDumpException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.threading.IRunSchedulerThread;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.run.runnable.IToolRuntimeEvaluationRunRunnable;
import de.clusteval.run.runnable.ToolRuntimeEvaluationRunRunnable;
import de.clusteval.run.runresult.IRunResult;
import de.clusteval.run.runresult.IToolRuntimeEvaluationRunResult;
import de.clusteval.run.runresult.postprocessing.IRunResultPostprocessor;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.MatrixParser;
import dk.sdu.imada.compbio.file.FileUtils;
import dk.sdu.imada.compbio.utils.Pair;
import dk.sdu.imada.compbio.utils.SimilarityMatrix.NUMBER_PRECISION;

/**
 * A type of execution run that performs exactly one clustering with one
 * parameter set for every pair of program and data configuration.
 * 
 * @author Christian Wiwie
 * 
 */
public class ToolRuntimeEvaluationRun
		extends
			ClusteringRun<IToolRuntimeEvaluationRunRunnable>
		implements
			IToolRuntimeEvaluationRun<IToolRuntimeEvaluationRunRunnable> {

	public static final String RUNTIME_EVALUATION_DATASET_MAJOR_NAME = "toolRuntimeEval";

	public static final String RUNTIME_EVALUATION_DATASET_MINOR_NAME_PREFIX = "toolRuntimeEval";

	protected static final Map<IRepository, List<IDataConfig>> TOOL_RUNTIME_EVALUATION_DATA_CONFIGS = new HashMap<>();

	// protected static final Map<IRepository, Map<IProgramConfig,
	// Map<IDataConfig, IToolRuntimeEvaluationRun<?>>>>
	// TOOL_RUNTIME_EVALUATION_RUNS = new HashMap<>();

	// protected static final Map<IRepository, Map<IProgramConfig,
	// Map<IDataConfig, Set<IToolRuntimeEvaluationRunResult>>>>
	// TOOL_RUNTIME_EVALUATION_RUN_RESULTS = new HashMap<>();

	// public static void informOnRuntimeEvaluationRun(
	// final IRepository repository, final IProgramConfig programConfig,
	// final IDataConfig dataConfig,
	// final IToolRuntimeEvaluationRun<?> run) {
	// if (!TOOL_RUNTIME_EVALUATION_RUNS.containsKey(repository))
	// TOOL_RUNTIME_EVALUATION_RUNS.put(repository, new HashMap<>());
	//
	// if (!TOOL_RUNTIME_EVALUATION_RUNS.get(repository)
	// .containsKey(programConfig))
	// TOOL_RUNTIME_EVALUATION_RUNS.get(repository).put(programConfig,
	// new HashMap<>());
	//
	// TOOL_RUNTIME_EVALUATION_RUNS.get(repository).get(programConfig)
	// .put(dataConfig, run);
	// }

	// public static void informOnRuntimeEvaluationRunResult(
	// final IRepository repository, final IProgramConfig programConfig,
	// final IDataConfig dataConfig,
	// final IToolRuntimeEvaluationRunResult result) {
	// if (!TOOL_RUNTIME_EVALUATION_RUN_RESULTS.containsKey(repository))
	// TOOL_RUNTIME_EVALUATION_RUN_RESULTS.put(repository,
	// new HashMap<>());
	//
	// if (!TOOL_RUNTIME_EVALUATION_RUN_RESULTS.get(repository)
	// .containsKey(programConfig))
	// TOOL_RUNTIME_EVALUATION_RUN_RESULTS.get(repository)
	// .put(programConfig, new HashMap<>());
	//
	// if (!TOOL_RUNTIME_EVALUATION_RUN_RESULTS.get(repository)
	// .get(programConfig).containsKey(dataConfig))
	// TOOL_RUNTIME_EVALUATION_RUN_RESULTS.get(repository)
	// .get(programConfig).put(dataConfig, new HashSet<>());
	//
	// TOOL_RUNTIME_EVALUATION_RUN_RESULTS.get(repository).get(programConfig)
	// .get(dataConfig).add(result);
	// }

	// public static Set<IProgramConfig>
	// getProgramConfigsWithoutRuntimeInformation(
	// final IRepository repository) {
	// Set<IProgramConfig> programConfigs = new HashSet<>();
	//
	// for (Map<ComparableVersion, IProgramConfig> e : repository
	// .getStaticEntitiesWithVersions(IProgramConfig.class).values()) {
	// for (IProgramConfig pc : e.values())
	// if (!TOOL_RUNTIME_EVALUATION_RUNS.containsKey(repository)
	// || !(TOOL_RUNTIME_EVALUATION_RUNS.get(repository)
	// .containsKey(pc)))
	// programConfigs.add(pc);
	// }
	//
	// return programConfigs;
	// }

	public static Set<IProgramConfig> getProgramConfigsWithoutRuntimeInformation(
			final IRepository repository) {
		Set<IProgramConfig> missingPC = new HashSet<>();

		Map<String, Map<ComparableVersion, IProgramConfig>> programConfigs = repository
				.getStaticEntitiesWithVersions(IProgramConfig.class);
		List<IToolRuntimeEvaluationRunResult> results = (List) repository
				.getStaticEntitiesWithVersions(IRunResult.class).values()
				.stream().map(m -> m.values()).flatMap(Collection::stream)
				.filter(r -> r instanceof IToolRuntimeEvaluationRunResult)
				.collect(Collectors.toList());

		for (Map<ComparableVersion, IProgramConfig> e : programConfigs
				.values()) {
			for (IProgramConfig pc : e.values()) {
				if (results.stream().filter(r -> r.getProgramConfig().toString()
						.equals(pc.toString())).count() == 0)
					missingPC.add(pc);
			}
		}

		return missingPC;
	}

	public static Map<IDataConfig, IToolRuntimeEvaluationRun<?>> getOrCreateRuntimeEvaluationRuns(
			final IRepository repository, final IProgramConfig programConfig)
			throws UnknownContextException,
			UnknownClusteringQualityMeasureException,
			DynamicComponentInitializationException,
			UnknownDataSetFormatException, UnknownDataSetTypeException,
			IOException, UnknownDistanceMeasureException,
			RepositoryObjectDumpException, RegisterException,
			URISyntaxException {

		List<IToolRuntimeEvaluationRun> runs = (List) repository
				.getStaticEntitiesWithVersions(IRun.class).values().stream()
				.map(m -> m.values()).flatMap(Collection::stream)
				.filter(r -> r instanceof IToolRuntimeEvaluationRun<?>)
				.map(r -> (IToolRuntimeEvaluationRun<?>) r).filter(r -> r
						.getOriginalProgramConfigs().contains(programConfig))
				.collect(Collectors.toList());

		Map<IDataConfig, IToolRuntimeEvaluationRun<?>> result = new HashMap<>();

		if (!runs.isEmpty()) {
			for (IToolRuntimeEvaluationRun<?> r : runs)
				for (IDataConfig dc : r.getOriginalDataConfigs())
					result.put(dc, r);
		} else {
			if (!TOOL_RUNTIME_EVALUATION_DATA_CONFIGS.containsKey(repository)) {
				List<IDataConfig> newDataConfigs = new ArrayList<>();

				for (String dataSetFileName : new String[]{
						String.format("%s_cassini25.tsv",
								RUNTIME_EVALUATION_DATASET_MINOR_NAME_PREFIX),
						String.format("%s_cassini50.tsv",
								RUNTIME_EVALUATION_DATASET_MINOR_NAME_PREFIX),
						String.format("%s_cassini100.tsv",
								RUNTIME_EVALUATION_DATASET_MINOR_NAME_PREFIX),
						String.format("%s_cassini500.tsv",
								RUNTIME_EVALUATION_DATASET_MINOR_NAME_PREFIX),
						String.format("%s_cassini1000.tsv",
								RUNTIME_EVALUATION_DATASET_MINOR_NAME_PREFIX)// ,
						// "toolRuntimeEval_cassini2500.tsv"
						// ,
						// "toolRuntimeEval_cassini5000.tsv"
				}) {
					InputStream resourceAsStream = ToolRuntimeEvaluationRun.class
							.getResourceAsStream("/" + dataSetFileName);
					File dataSetFile = File.createTempFile("clusteval",
							dataSetFileName);
					org.apache.commons.io.FileUtils.copyInputStreamToFile(
							resourceAsStream, dataSetFile);

					AbsoluteDataSet absoluteDataSet = new AbsoluteDataSet(
							repository, System.currentTimeMillis(),
							new File(FileUtils.buildPath(
									repository.getBasePath(IDataSet.class),
									RUNTIME_EVALUATION_DATASET_MAJOR_NAME,
									dataSetFileName + ".v1")),
							dataSetFileName,
							(IAbsoluteDataSetFormat) DataSetFormat
									.parseFromString(repository,
											"MatrixDataSetFormat"),
							DataSetType.parseFromString(repository,
									"SyntheticDataSetType"),
							WEBSITE_VISIBILITY.HIDE);

					MatrixParser parser = new MatrixParser(
							dataSetFile.getAbsolutePath());
					parser.process();
					List<Pair<String, double[]>> coords = parser
							.getCoordinates();
					String[] ids = new String[coords.size()];
					double[][] data = new double[coords.size()][];
					for (int i = 0; i < coords.size(); i++) {
						data[i] = coords.get(i).getSecond();
						ids[i] = coords.get(i).getFirst();
					}
					absoluteDataSet
							.setDataSetContent(new DataMatrix(ids, data));
					absoluteDataSet.writeToFile(true);

					DataSetConfig dataSetConfig = new DataSetConfig(repository,
							System.currentTimeMillis(),
							new File(FileUtils.buildPath(
									repository
											.getBasePath(IDataSetConfig.class),
									dataSetFileName + ".v1.dsconfig")),
							absoluteDataSet,
							new ConversionInputToStandardConfiguration(
									DistanceMeasure.parseFromString(repository,
											"EuclidianDistanceMeasure"),
									NUMBER_PRECISION.DOUBLE, new ArrayList<>(),
									new ArrayList<>()),
							new ConversionStandardToInputConfiguration());
					dataSetConfig.dumpToFile();

					DataConfig dataConfig = new DataConfig(repository,
							System.currentTimeMillis(),
							new File(FileUtils.buildPath(
									repository.getBasePath(IDataConfig.class),
									dataSetFileName + ".v1.dataconfig")),
							dataSetConfig, null);
					dataConfig.dumpToFile();

					newDataConfigs.add(dataConfig);
				}

				TOOL_RUNTIME_EVALUATION_DATA_CONFIGS.put(repository,
						newDataConfigs);
			}
			List<IDataConfig> dataConfigs = TOOL_RUNTIME_EVALUATION_DATA_CONFIGS
					.get(repository);

			Map<IDataConfig, IToolRuntimeEvaluationRun<?>> newRuns = new HashMap<>();

			for (IDataConfig dataConfig : dataConfigs) {
				Map<IProgramConfig, Map<IProgramParameter<?>, String>> fixedParameterValues = new HashMap<>();
				fixedParameterValues.put(programConfig, new HashMap<>());
				IProgramParameter<?> kParam = programConfig
						.getParameterForName("k");
				if (kParam != null)
					fixedParameterValues.get(programConfig).put(kParam,
							String.format("%d", dataConfig.getDatasetConfig()
									.getDataSet().getNumberOfSamples() / 2));

				Map<String, Integer> maxRunningTime = new HashMap<String, Integer>();
				maxRunningTime.put(programConfig.getName(), 10);

				ToolRuntimeEvaluationRun newRun = new ToolRuntimeEvaluationRun(
						repository,
						Context.parseFromString(repository,
								"ClusteringContext"),
						System.currentTimeMillis(),
						new File(FileUtils.buildPath(
								repository.getBasePath(IRun.class),
								String.format(
										"toolRuntimeEvaluation_%s_%s.v1.run",
										programConfig.toString("_v"),
										dataConfig.toString("_v")))),
						Arrays.asList(programConfig), Arrays.asList(dataConfig),
						Arrays.asList(ClusteringQualityMeasure.parseFromString(
								repository,
								"SilhouetteValueRClusteringQualityMeasure",
								null)),
						fixedParameterValues,
						new ArrayList<IRunResultPostprocessor>(),
						maxRunningTime);
				newRun.dumpToFile();
				newRun.register();
				newRuns.put(dataConfig, newRun);
				result = newRuns;
			}
		}

		return result;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 2845096456315471154L;

	/**
	 * @param repository
	 * @param context
	 * @param changeDate
	 * @param absPath
	 * @param programConfigs
	 * @param dataConfigs
	 * @param qualityMeasures
	 * @param fixedParameterValues
	 * @param postProcessors
	 * @param maxExecutionTimes
	 */
	public ToolRuntimeEvaluationRun(IRepository repository, IContext context,
			long changeDate, File absPath, List<IProgramConfig> programConfigs,
			List<IDataConfig> dataConfigs,
			List<IClusteringQualityMeasure> qualityMeasures,
			Map<IProgramConfig, Map<IProgramParameter<?>, String>> fixedParameterValues,
			List<IRunResultPostprocessor> postProcessors,
			Map<String, Integer> maxExecutionTimes) {
		super(repository, context, changeDate, absPath, programConfigs,
				dataConfigs, qualityMeasures, fixedParameterValues,
				postProcessors, maxExecutionTimes);
	}

	/**
	 * Copy constructor of clustering runs.
	 * 
	 * @param clusteringRun
	 *            The clustering run to be cloned.
	 */
	public ToolRuntimeEvaluationRun(
			final ToolRuntimeEvaluationRun clusteringRun) {
		super(clusteringRun);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ClusteringRun#asSerializable()
	 */
	@Override
	public SerializableToolRuntimeEvaluationRun<? extends IToolRuntimeEvaluationRun<IToolRuntimeEvaluationRunRunnable>> asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableToolRuntimeEvaluationRun) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ClusteringRun#asSerializableInternal()
	 */
	@Override
	public SerializableToolRuntimeEvaluationRun<IToolRuntimeEvaluationRun<?>> asSerializableInternal()
			throws RepositoryObjectSerializationException {
		return new SerializableToolRuntimeEvaluationRun<IToolRuntimeEvaluationRun<?>>(
				this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ClusteringRun#clone()
	 */
	@Override
	public ToolRuntimeEvaluationRun clone() {
		return new ToolRuntimeEvaluationRun(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ClusteringRun#createRunRunnableFor(de.clusteval.
	 * framework.threading.IRunSchedulerThread, de.clusteval.run.ExecutionRun,
	 * de.clusteval.program.IProgramConfig, de.clusteval.data.IDataConfig,
	 * java.lang.String, boolean, java.util.Map)
	 */
	@Override
	protected <T extends ExecutionRun<IToolRuntimeEvaluationRunRunnable>> IToolRuntimeEvaluationRunRunnable createRunRunnableFor(
			IRunSchedulerThread runScheduler, T run,
			IProgramConfig programConfig, IDataConfig dataConfig,
			String runIdentString, boolean isResume,
			Map<IProgramParameter<?>, String> runParams) {
		IToolRuntimeEvaluationRunRunnable r = new ToolRuntimeEvaluationRunRunnable(
				runScheduler, (IToolRuntimeEvaluationRun) run, programConfig,
				dataConfig, runIdentString, isResume, runParams);
		((Run) run).progress.addSubProgress(r.getProgressPrinter(), 10000);
		((Run) run).progressBeforeExecution
				.addSubProgress(r.getProgressPrinterBeforeExecution(), 10000);
		return r;
	}
}
