package de.clusteval.run.runnable;

import java.io.File;
import java.util.Map;

import de.clusteval.data.IDataConfig;
import de.clusteval.framework.threading.IRunSchedulerThread;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.run.IToolRuntimeEvaluationRun;
import de.clusteval.run.runresult.IToolRuntimeEvaluationRunResult;
import de.clusteval.run.runresult.ToolRuntimeEvaluationRunResult;

/**
 * @author Christian Wiwie
 *
 *
 * @since Nov 6, 2017
 */
public class ToolRuntimeEvaluationRunRunnable
		extends
			ClusteringRunRunnable<IToolRuntimeEvaluationRun<?>, IToolRuntimeEvaluationRunResult>
		implements
			IToolRuntimeEvaluationRunRunnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1875685575954541503L;

	/**
	 * @param runScheduler
	 * @param run
	 * @param programConfig
	 * @param dataConfig
	 * @param runIdentString
	 * @param isResume
	 * @param runParams
	 */
	public ToolRuntimeEvaluationRunRunnable(IRunSchedulerThread runScheduler,
			IToolRuntimeEvaluationRun<?> run, IProgramConfig programConfig,
			IDataConfig dataConfig, String runIdentString, boolean isResume,
			Map<IProgramParameter<?>, String> runParams) {
		super(runScheduler, run, programConfig, dataConfig, runIdentString,
				isResume, runParams);
	}

	@Override
	protected void initRunResult() {
		result = new ToolRuntimeEvaluationRunResult(
				this.getRun().getRepository(),
				new File(this.completeQualityOutput).lastModified(),
				new File(this.completeQualityOutput), dataConfig, programConfig,
				format, run.getRunIdentificationString(), run);
	}

}
