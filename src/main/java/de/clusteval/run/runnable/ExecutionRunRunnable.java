/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run.runnable;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import de.clusteval.cluster.FileBackedClustering;
import de.clusteval.cluster.IClusterItem;
import de.clusteval.cluster.IFileBackedClustering;
import de.clusteval.cluster.paramOptimization.NoParameterSetFoundException;
import de.clusteval.cluster.quality.ClusteringQualityMeasureValue;
import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.cluster.quality.IClusteringQualitySet;
import de.clusteval.data.DataConfig;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.AbsoluteDataSet;
import de.clusteval.data.dataset.IDataSet;
import de.clusteval.data.dataset.IDataSetConfig;
import de.clusteval.data.dataset.RelativeDataSet;
import de.clusteval.data.dataset.format.ConversionStandardToInputConfiguration;
import de.clusteval.data.dataset.format.DataSetConversionException;
import de.clusteval.data.dataset.format.IConversionInputToStandardConfiguration;
import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.data.dataset.format.IncompatibleDataSetFormatException;
import de.clusteval.data.dataset.format.InvalidDataSetFormatVersionException;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.goldstandard.IGoldStandard;
import de.clusteval.data.goldstandard.IGoldStandardConfig;
import de.clusteval.data.goldstandard.IncompleteGoldStandardException;
import de.clusteval.data.goldstandard.InvalidGoldStandardFormatException;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.program.IStandaloneProgramConfig;
import de.clusteval.program.ParameterSet;
import de.clusteval.program.r.AbsoluteAndRelativeDataRProgram;
import de.clusteval.program.r.AbsoluteDataRProgram;
import de.clusteval.program.r.IRProgram;
import de.clusteval.program.r.IRProgramConfig;
import de.clusteval.program.r.RProgram;
import de.clusteval.program.r.RelativeDataRProgram;
import de.clusteval.run.ExecutionRun;
import de.clusteval.run.IExecutionRun;
import de.clusteval.run.MissingParameterValueException;
import de.clusteval.run.runresult.ClusteringRunResult;
import de.clusteval.run.runresult.IExecutionRunResult;
import de.clusteval.run.runresult.NoRunResultFormatParserException;
import de.clusteval.run.runresult.format.IRunResultFormat;
import de.clusteval.run.runresult.format.IRunResultFormatParser;
import de.clusteval.run.runresult.format.RunResultFormat;
import de.clusteval.run.runresult.format.RunResultNotFoundException;
import de.clusteval.run.runresult.format.UnknownRunResultFormatException;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.FormatConversionException;
import de.clusteval.utils.InternalAttributeException;
import de.clusteval.utils.RNotAvailableException;
import dk.sdu.imada.compbio.file.FileUtils;
import dk.sdu.imada.compbio.format.Formatter;
import dk.sdu.imada.compbio.utils.Triple;

/**
 * A type of a runnable, that corresponds to {@link ExecutionRun}s and is
 * therefore responsible for performing program configurations and certain data
 * configurations.
 * 
 * @author Christian Wiwie
 * 
 */
public abstract class ExecutionRunRunnable<RUN_TYPE extends IExecutionRun<?>, RESULT extends IExecutionRunResult>
		extends
			RunRunnable<RUN_TYPE, IClusteringIterationRunnable<?>, IExecutionIterationWrapper, RESULT>
		implements
			IExecutionRunRunnable<RUN_TYPE, RESULT> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -196857718446004703L;

	/**
	 * The program configuration this thread combines with a data configuration.
	 */
	protected final IProgramConfig programConfig;

	/**
	 * The data configuration this thread combines with a program configuration.
	 */
	protected final IDataConfig dataConfig;

	/**
	 * This is the run result format of the program that is being executed by
	 * this runnable.
	 */
	protected IRunResultFormat format;

	/**
	 * A map containing all the parameter values set in the run.
	 */
	protected Map<IProgramParameter<?>, String> runParams;

	/**
	 * A temporary variable holding the absolute path to the current complete
	 * quality output file during execution of the runnable.
	 */
	protected String completeQualityOutput;

	protected String completeMetaDataFile;

	protected static final List<String> iterationMetaAttributes = Arrays
			.asList("walltime", "resultconvtime", "qualitytime", "totalwalltime");

	/**
	 * @param run
	 *            The run this runnable belongs to.
	 * @param runIdentString
	 *            The unique identification string of the run which is used to
	 *            store the results in a unique folder to avoid overwriting.
	 * @param programConfig
	 *            The program configuration encapsulating the program executed
	 *            by this runnable.
	 * @param dataConfig
	 *            The data configuration used by this runnable.
	 * @param isResume
	 *            True, if this run is a resumption of a previous execution or a
	 *            completely new execution.
	 */
	public ExecutionRunRunnable(RUN_TYPE run, IProgramConfig programConfig,
			IDataConfig dataConfig, String runIdentString, boolean isResume,
			Map<IProgramParameter<?>, String> runParams) {
		super(run, runIdentString, isResume);

		this.programConfig = programConfig;
		this.dataConfig = dataConfig;
		this.runParams = runParams;
	}

	/**
	 * A helper method to write a header into the complete quality output in the
	 * beginning.
	 * 
	 * <p>
	 * If at all, then this method is invoked by {@link #beginRun()} before
	 * anything has been executed by the runnable.
	 */
	protected void writeHeaderIntoCompleteFile(
			final String completeQualityOutput) {
		StringBuilder sb = new StringBuilder();
		// 04.04.2013: adding iteration numbers into complete file
		sb.append("iteration\t");
		for (int p = 0; p < programConfig.getOptimizableParameters()
				.size(); p++) {
			IProgramParameter<?> param = programConfig
					.getOptimizableParameters().get(p);
			if (p > 0)
				sb.append(",");
			sb.append(param);
		}
		sb.append("\t");
		for (IClusteringQualityMeasure measure : this.getRun()
				.getQualityMeasures()) {
			sb.append(measure.getClass().getSimpleName());
			sb.append("\t");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.append("\n");

		FileUtils.appendStringToFile(completeQualityOutput, sb.toString());
	}

	protected void writeHeaderIntoCompleteMetaDataFile(
			final String completeMetaDataFile) {
		StringBuilder sb = new StringBuilder();
		sb.append("iteration");

		for (String key : iterationMetaAttributes) {
			sb.append("\t");
			sb.append(key);
		}
		sb.append("\n");

		FileUtils.appendStringToFile(completeMetaDataFile, sb.toString());
	}

	/**
	 * This method checks, whether the format of the data input is compatible to
	 * the input formats of the program configuration.
	 * 
	 * @return True, if compatible, false otherwise.
	 * @throws IOException
	 * @throws RegisterException
	 * @throws InterruptedException
	 */
	protected boolean preprocessAndCheckCompatibleDataSetFormat()
			throws IOException, RegisterException, InterruptedException {

		IConversionInputToStandardConfiguration configInputToStandard = dataConfig
				.getDatasetConfig().getConversionInputToStandardConfiguration();
		ConversionStandardToInputConfiguration configStandardToInput = dataConfig
				.getDatasetConfig().getConversionStandardToInputConfiguration();

		/*
		 * Added 18.09.2012: only one conversion operation per dataset at a
		 * time. otherwise we can have problems
		 */
		File datasetFile = FileUtils.getCommonFile(new File(
				dataConfig.getDatasetConfig().getDataSet().getAbsolutePath()));
		synchronized (datasetFile) {
			/*
			 * Check, whether this program can be applied to this dataset, i.e.
			 * either they are directly compatible or the dataset can be
			 * converted to another compatible DataSetFormat.
			 */
			List<IDataSetFormat> compatibleDsFormats = new ArrayList<>(
					programConfig instanceof IRProgramConfig
							? ((IRProgram) programConfig.getProgram())
									.getCompatibleDataSetFormats()
							: ((IStandaloneProgramConfig) programConfig)
									.getCompatibleDataSetFormats());
			boolean found = false;
			// try to find a compatible format we can use and convert
			for (IDataSetFormat compFormat : compatibleDsFormats) {
				try {
					IDataSet convertedDataSet = dataConfig.getDatasetConfig()
							.getDataSet()
							.preprocessAndConvertTo(this.run.getContext(),
									compFormat, configInputToStandard,
									configStandardToInput);

					// added 23.01.2013: rename the new dataset, unique for the
					// program configuration
					// int indexOfLastExt =
					// convertedDataSet.getAbsolutePath().lastIndexOf(".");
					// if (indexOfLastExt == -1)
					// indexOfLastExt =
					// convertedDataSet.getAbsolutePath().length();
					// String newFileName =
					// convertedDataSet.getAbsolutePath().substring(0,
					// indexOfLastExt) + "_" + programConfig.getName()
					// +
					// convertedDataSet.getAbsolutePath().substring(indexOfLastExt);
					// // if the new dataset file is the same file as the old
					// one,
					// // we copy it instead of moving
					// if (convertedDataSet.getAbsolutePath()
					// .equals(dataConfig.getDatasetConfig().getDataSet()
					// .getAbsolutePath())) {
					// convertedDataSet.copyTo(new File(newFileName), false,
					// true, false);
					// } else
					// convertedDataSet.moveTo(new File(newFileName), false);

					// TODO: seems weird, should we really just overwrite the
					// original data set here?
					dataConfig.getDatasetConfig().setDataSet(convertedDataSet);
					// found a convertable compatible format
					found = true;
					break;
				} catch (FormatConversionException e) {
				} catch (InvalidDataSetFormatVersionException e) {
				} catch (RNotAvailableException e) {
				} catch (DataSetConversionException e) {
				}
			}
			return found;
		}
	}

	/**
	 * This method checks, whether the dataset is compatible to the
	 * goldstandard, by verifying, that all objects contained in the dataset
	 * have an entry in the goldstandard and vice versa.
	 * 
	 * @param dataSetConfig
	 *            The dataset configuration encapsulating the dataset to be
	 *            checked.
	 * @param goldStandardConfig
	 *            The goldstandard configuration encapsulating the
	 *            goldstandardto be checked.
	 * @throws IOException
	 * @throws InvalidGoldStandardFormatException
	 * @throws UnknownDataSetFormatException
	 * @throws IncompleteGoldStandardException
	 * @throws InvalidDataSetFormatVersionException
	 * @throws IllegalArgumentException
	 */
	protected void checkCompatibilityDataSetGoldStandard(
			IDataSetConfig dataSetConfig,
			IGoldStandardConfig goldStandardConfig)
			throws InvalidGoldStandardFormatException,
			IncompleteGoldStandardException, IllegalArgumentException {
		this.log.debug(
				"Checking compatibility of goldstandard and dataset ...");
		IDataSet dataSet = dataSetConfig.getDataSet().getInStandardFormat();
		File dataSetFile = FileUtils
				.getCommonFile(new File(dataSet.getAbsolutePath()));
		synchronized (dataSetFile) {
			IGoldStandard goldStandard = goldStandardConfig.getGoldstandard();
			File goldStandardFile = FileUtils
					.getCommonFile(new File(goldStandard.getAbsolutePath()));
			synchronized (goldStandardFile) {

				/*
				 * Check whether all ids in the dataset have a corresponding
				 * entry in the gold standard
				 */
				// dataSet.loadIntoMemory();
				goldStandard.loadIntoMemory();

				final Set<String> ids = new HashSet<String>(dataSet.getIds());
				final Set<IClusterItem> gsItems = goldStandard.getClustering()
						.getClusterItems();
				final Set<String> gsIds = new HashSet<String>();
				for (IClusterItem item : gsItems)
					gsIds.add(item.getId());

				if (!gsIds.containsAll(ids)) {
					ids.removeAll(gsIds);
					throw new IncompleteGoldStandardException(ids);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IExecutionRunRunnable#getProgramConfig()
	 */
	@Override
	public IProgramConfig getProgramConfig() {
		return this.programConfig;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IExecutionRunRunnable#getDataConfig()
	 */
	@Override
	public IDataConfig getDataConfig() {
		return this.dataConfig;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runnable.IExecutionRunRunnable#getCompleteQualityOutput(
	 * )
	 */
	@Override
	public String getCompleteQualityOutput() {
		return this.completeQualityOutput;
	}

	/**
	 * Helper method for
	 * {@link #parseInvocationLineAndEffectiveParameters(String, String, Map, Map, Map, StringBuilder)}
	 * <p>
	 * Get the original invocation line format from the program configuration
	 * without replacing of any parameters.
	 * 
	 * @return The invocation line.
	 */
	protected String getInvocationFormat() {
		// added 16.01.2013
		if (programConfig.getProgram() instanceof RProgram) {
			return ((IRProgram) programConfig.getProgram())
					.getInvocationFormat();
		}
		return programConfig
				.getInvocationFormat(!dataConfig.hasGoldStandardConfig());
	}

	/**
	 * Helper method for
	 * {@link #parseInvocationLineAndEffectiveParameters(String, String, Map, Map, Map, StringBuilder)}
	 * 
	 * <p>
	 * Replace the executable parameter %e% in the invocation line by the
	 * absolute path to the executable.
	 * 
	 * @param invocation
	 *            The invocation line without replaced executable parameter.
	 * @param internalParams
	 *            The map containing all internal parameters, e.g. the
	 *            executable path.
	 * @return The invocation line with replaced executable parameter.
	 */
	protected String[] parseExecutable(final String[] invocation,
			final Map<String, String> internalParams) {
		internalParams.put("e", programConfig.getProgram().getExecutable());
		String[] parsed = invocation.clone();
		for (int i = 0; i < parsed.length; i++)
			parsed[i] = parsed[i].replace("%e%",
					programConfig.getProgram().getExecutable());
		return parsed;
	}

	/**
	 * Helper method for
	 * {@link #parseInvocationLineAndEffectiveParameters(String, String, Map, Map, Map, StringBuilder)}
	 * 
	 * <p>
	 * Replace the input parameter %i% in the invocation line by the absolute
	 * path to the input file.
	 * 
	 * @param invocation
	 *            The invocation line without replaced input parameter.
	 * @param internalParams
	 *            The map containing all internal parameters, e.g. the input
	 *            path.
	 * @return The invocation line with replaced input parameter.
	 */
	protected String[] parseInput(final String[] invocation,
			final Map<String, String> internalParams) {
		// for R programs, we need to insert the variable name instead of the
		// absolute path to the input file.
		if (programConfig instanceof IRProgramConfig) {
			IRProgram program = (IRProgram) programConfig.getProgram();
			if (program instanceof RelativeDataRProgram)
				internalParams.put("i", "sim");
			else if (program instanceof AbsoluteDataRProgram)
				internalParams.put("i", "x");
			else if (program instanceof AbsoluteAndRelativeDataRProgram) {
				boolean absoluteData = dataConfig.getDatasetConfig()
						.getDataSet()
						.getOriginalDataSet() instanceof AbsoluteDataSet;
				if (absoluteData)
					internalParams.put("i", "x");
				else
					internalParams.put("i", "sim");
			}
		} else {
			internalParams.put("i", dataConfig.getDatasetConfig().getDataSet()
					.getAbsolutePath());
		}
		String[] parsed = invocation.clone();
		for (int i = 0; i < parsed.length; i++)
			parsed[i] = parsed[i].replace("%i%", internalParams.get("i"));
		return parsed;
	}

	/**
	 * Helper method for
	 * {@link #parseInvocationLineAndEffectiveParameters(String, String, Map, Map, Map, StringBuilder)}
	 * 
	 * <p>
	 * Replace the goldstandard parameter %gs% in the invocation line by the
	 * absolute path to the goldstandard.
	 * 
	 * @param invocation
	 *            The invocation line without replaced goldstandard parameter.
	 * @param internalParams
	 *            The map containing all internal parameters, e.g. the
	 *            goldstandard path.
	 * @return The invocation line with replaced goldstandard parameter.
	 */
	protected String[] parseGoldStandard(final String[] invocation,
			final Map<String, String> internalParams) {
		if (!dataConfig.hasGoldStandardConfig())
			return invocation;
		internalParams.put("gs", dataConfig.getGoldstandardConfig()
				.getGoldstandard().getAbsolutePath());
		String[] parsed = invocation.clone();
		for (int i = 0; i < parsed.length; i++)
			parsed[i] = parsed[i].replace("%gs%",
					dataConfig.getGoldstandardConfig().getGoldstandard()
							.getAbsolutePath());
		return parsed;
	}

	/**
	 * Helper method for
	 * {@link #parseInvocationLineAndEffectiveParameters(String, String, Map, Map, Map, StringBuilder)}
	 * 
	 * <p>
	 * Replace the output parameter %o% in the invocation line by the absolute
	 * path to the output file.
	 * 
	 * @param invocation
	 *            The invocation line without replaced output parameter.
	 * @param internalParams
	 *            The map containing all internal parameters, e.g. the output
	 *            file path.
	 * @return The invocation line with replaced output parameter.
	 */
	protected String[] parseOutput(final String clusteringOutput,
			final String qualityOutput, final String[] invocation,
			final Map<String, String> internalParams) {
		internalParams.put("o", clusteringOutput);
		internalParams.put("q", qualityOutput);
		String[] parsed = invocation.clone();
		for (int i = 0; i < parsed.length; i++)
			parsed[i] = parsed[i].replace("%o%", clusteringOutput);
		return parsed;
	}

	/**
	 * This method builds up the invocation line by replacing placeholders of
	 * internal parameters by their actual runtime values:
	 * <ul>
	 * <li><b>%e%</b>: The absolute path to the executable</li>
	 * <li><b>%i%</b>: The absolute path to the input file</li>
	 * <li><b>%gs%</b>: The absolute path to the goldstandard file</li>
	 * <li><b>%o%</b>: The absolute path to the output file</li>
	 * </ul>
	 * <p>
	 * Afterwards, non-internal parameters are replaced, that means parameters,
	 * that are defined in the configuration files of the run or program in
	 * {@link #replaceRunParameters(String[])}, e.g.:
	 * <ul>
	 * <li><b>%T%</b> is replaced by 2.0</li>
	 * </ul>
	 * <p>
	 * All placeholders that are not replaced at this point are replaced by the
	 * default values of the corresponding parameters by invoking
	 * {@link #replaceDefaultParameters(String[])}. If the invocation line
	 * contains placeholders that cannot be mapped to a parameter, an exception
	 * is thrown and the process is terminated.
	 * 
	 * @return The parsed invocation line.
	 * 
	 * @throws InternalAttributeException
	 * @throws RegisterException
	 * @throws NoParameterSetFoundException
	 *             This exception is thrown, if no parameter set was found that
	 *             was not already evaluated before.
	 * 
	 */
	protected String[] parseInvocationLineAndEffectiveParameters(
			final IExecutionIterationWrapper iterationWrapper)
			throws InternalAttributeException, RegisterException,
			NoParameterSetFoundException {

		final Map<String, String> internalParams = iterationWrapper
				.getInternalParams();
		final Map<String, String> effectiveParams = iterationWrapper
				.getEffectiveParams();

		/*
		 * We take the invocation line from the ProgramConfig and replace the
		 * variables %e%, %i%, %gs%, %o% by the absolute path to the executable,
		 * the input, the goldstandard and the output respectively.
		 */
		// split by spaces. this ensures compatibility for spaces in pathes that
		// might be inserted later.
		String[] invocation = getInvocationFormat().split(" ");

		/*
		 * Executable %e%
		 */
		invocation = parseExecutable(invocation, internalParams);

		/*
		 * input %i%
		 */
		invocation = parseInput(invocation, internalParams);

		/*
		 * goldstandard %gs%
		 */
		invocation = parseGoldStandard(invocation, internalParams);
		/*
		 * output %o%
		 */
		invocation = parseOutput(
				iterationWrapper.getClusteringResultFile().getAbsolutePath(),
				iterationWrapper.getResultQualityFile().getAbsolutePath(),
				invocation, internalParams);

		invocation = replaceRunParameters(invocation, effectiveParams);

		try {
			invocation = replaceDefaultParameters(invocation, effectiveParams);
		} catch (MissingParameterValueException e) {
			this.exceptions.add(e);
			return null;
		}

		return invocation;
	}

	/**
	 * Helper method for {@link #parseInvocationLineAndEffectiveParameters()}.
	 * 
	 * <p>
	 * All remaining parameters in the invocation line, that are not set to an
	 * actual value in the run configuration will be set to the default values
	 * of the corresponding parameters defined in the program configuration.
	 * Throw an exception if no value is set for a certain parameter-string.
	 * 
	 * @param invocation
	 * @return The invocation line with all parameters replaced.
	 * @throws MissingParameterValueException
	 * @throws InternalAttributeException
	 */
	protected String[] replaceDefaultParameters(String[] invocation,
			final Map<String, String> effectiveParams)
			throws MissingParameterValueException, InternalAttributeException {
		String[] parsed = invocation.clone();
		for (int i = 0; i < parsed.length; i++) {
			while (parsed[i].contains("%")) {
				int pos = parsed[i].indexOf("%");
				int endPos = parsed[i].indexOf("%", pos + 1);
				// variable string at very end of invocation line
				if (endPos < 0)
					endPos = parsed[i].length();

				String param = parsed[i].substring(pos + 1, endPos);
				IProgramParameter<?> pa = programConfig
						.getParameterForName(param);
				int arrPos = programConfig.getParameters().indexOf(pa);
				if (arrPos < 0) {
					throw new MissingParameterValueException(
							"No value for parameter \"" + param + "\" given");
				}

				String def = programConfig.getParameters().get(arrPos)
						.evaluateDefaultValue(dataConfig.getRepository(),
								dataConfig, programConfig)
						.toString();
				parsed[i] = parsed[i].replace("%" + param + "%", def);

				effectiveParams.put(pa.getName(), def);
			}
		}
		return parsed;
	}

	/**
	 * Helper method for {@link #parseInvocationLineAndEffectiveParameters()}.
	 * 
	 * <p>
	 * Non-internal parameters are replaced, that means parameters, that are
	 * defined in the configuration files of the run or program in
	 * {@link #replaceRunParameters(String)}.
	 * 
	 * @param invocation
	 * @return The invocation line with replaced run parameters.
	 * @throws InternalAttributeException
	 * @throws RegisterException
	 * @throws NoParameterSetFoundException
	 *             This exception is thrown, if no parameter set was found that
	 *             was not already evaluated before.
	 */
	@SuppressWarnings("unused")
	protected String[] replaceRunParameters(String[] invocation,
			final Map<String, String> effectiveParams)
			throws InternalAttributeException, RegisterException,
			NoParameterSetFoundException {
		/*
		 * Now, replace the remaining parameters given in the run configuration.
		 */

		String[] parsed = invocation.clone();
		for (int i = 0; i < parsed.length; i++)
			for (IProgramParameter<?> param : runParams.keySet()) {
				parsed[i] = parsed[i].replace("%" + param.getName() + "%",
						runParams.get(param));
				effectiveParams.put(param.getName(), runParams.get(param));
			}
		return parsed;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.RunRunnable#createIterationWrapper(int)
	 */
	@Override
	protected ExecutionIterationWrapper createIterationWrapper() {
		return new ExecutionIterationWrapper();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.RunRunnable#decorateIterationWrapper(de.
	 * clusteval .run.runnable.IterationWrapper, int)
	 */
	@Override
	protected void decorateIterationWrapper(
			IExecutionIterationWrapper iterationWrapper, int currentPos)
			throws RunIterationException {
		try {
			super.decorateIterationWrapper(iterationWrapper, currentPos);
			iterationWrapper.setRunnable(this);
			iterationWrapper.setDataConfig(dataConfig);
			iterationWrapper.setProgramConfig(programConfig.clone());

			this.initAndEnsureIterationFilesAndFolders(iterationWrapper);

			final String[] invocation = this
					.parseInvocationLineAndEffectiveParameters(
							iterationWrapper);
			iterationWrapper.setInvocation(invocation);

			/*
			 * An object that wraps up all results calculated during the
			 * execution of this runnable. The runnable is responsible for
			 * adding new results to this object during the execution.
			 */
			iterationWrapper.setClusteringRunResult(new FileBackedClustering(
					this.getRun().getRepository(), System.currentTimeMillis(),
					iterationWrapper.getClusteringResultFile(), format));
		} catch (Exception e) {
			throw new RunIterationException(e);
		}
	}

	/**
	 * Method invoked by {@link #doRun()} which performs a single iteration of
	 * the run. If this runnable is of type parameter optimization, this method
	 * is invoked several times. In case of a clustering run, it is invoked only
	 * once.
	 * 
	 * <p>
	 * First this method initializes all files and folder structures in
	 * {@link #initAndEnsureIterationFilesAndFolders()} such that the following
	 * computations can be performed smoothly.
	 * <p>
	 * It initializes all attribute variables needed throughout the process
	 * itself and by invoking
	 * {@link #parseInvocationLineAndEffectiveParameters()}.
	 * <p>
	 * The clustering method is executed with the given parameter values and
	 * settings asynchronously. It waits until the second process finishes.
	 * <p>
	 * The result file of the clustering method is converted to the standard
	 * result format by invoking {@link #convertResult()}.
	 * <p>
	 * Next the qualities of the converted result file are assessed in
	 * {@link #assessQualities(ClusteringRunResult)}.
	 * <p>
	 * Then it invokes {@link #writeQualitiesToFile(List)}, which writes the
	 * assessed cluster qualities into files on the filesystem.
	 * <p>
	 * In {@link #afterClustering(ClusteringRunResult)} all actions are
	 * performed, that require the clustering process to be finished beforehand.
	 * <p>
	 * Last the result is added to the list of run results of the corresponding
	 * run of this runnable.
	 * <p>
	 * In case the run result is missing or cannot be parsed successfully,
	 * {@link #handleMissingRunResult()} is responsible for performing actions
	 * ensuring, that the next iterations can be executed without problems.
	 * 
	 */
	@Override
	protected void doRunIteration(
			final IExecutionIterationWrapper iterationWrapper)
			throws RunIterationException {
		try {
			if (this.isPaused()) {
				log.info("Pausing...");
				this.runningTime += System.currentTimeMillis()
						- this.lastStartTime;
				while (this.isPaused()) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
					}
				}
				log.info("Resuming...");
				this.lastStartTime = System.currentTimeMillis();
			}

			/*
			 * We check from time to time, whether this run got the order to
			 * terminate.
			 */
			if (checkForInterrupted())
				throw new InterruptedException();

			// only create new iteration runnables, if none of the old iteration
			// runnables threw exceptions
			for (IIterationRunnable prevItRunnable : this.iterationRunnables) {
				if (prevItRunnable instanceof ClusteringIterationRunnable) {
					if (((ClusteringIterationRunnable) prevItRunnable)
							.getNoRunResultException() != null)
						throw ((ClusteringIterationRunnable) prevItRunnable)
								.getNoRunResultException();
				}
				if (prevItRunnable.getIoException() != null)
					throw prevItRunnable.getIoException();
				else if (prevItRunnable.getrLibraryException() != null)
					throw prevItRunnable.getrLibraryException();
				else if (prevItRunnable.getrNotAvailableException() != null)
					throw prevItRunnable.getrNotAvailableException();

			}

			ClusteringIterationRunnable iterationRunnable = this
					.createIterationRunnable(iterationWrapper);

			this.submitIterationRunnable(iterationRunnable);
		} catch (Exception e) {
			throw new RunIterationException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.RunRunnable#createIterationRunnable(de.
	 * clusteval .run.runnable.IterationWrapper)
	 */
	@Override
	protected ClusteringIterationRunnable createIterationRunnable(
			IExecutionIterationWrapper iterationWrapper) {

		return new ClusteringIterationRunnable(iterationWrapper, format);
	}

	/**
	 * This method is responsible for assessing the qualities of a clustering
	 * run result. It takes the clusterings and passes them to
	 * {@link ClusteringRunResult#assessQuality(List)}.
	 * 
	 * @param convertedResult
	 *            The clustering result converted to the default format, such
	 *            that it can be parsed.
	 * @throws RunResultNotFoundException
	 */
	@Override
	public IClusteringQualitySet assessQualities(
			final IFileBackedClustering convertedResult,
			final Map<String, String> internalParams)
			throws RunResultNotFoundException {
		try {
			final String qualityFile = internalParams.get("q");
			convertedResult.loadIntoMemory();
			try {
				// TODO temporary bugfix;
				dataConfig.getDatasetConfig().getDataSet().getInStandardFormat()
						.loadIntoMemory(this);
				IClusteringQualitySet quals = convertedResult.assessQuality(
						dataConfig, this.getRun().getQualityMeasures());
				for (IClusteringQualityMeasure qualityMeasure : quals
						.keySet()) {
					FileUtils.appendStringToFile(qualityFile,
							qualityMeasure.getClass().getSimpleName() + "\t"
									+ quals.get(qualityMeasure) + "\n");
				}
				return quals;
			} finally {
				convertedResult.unloadFromMemory();
			}
		} catch (Exception e) {
			throw new RunResultNotFoundException(
					"The result file " + convertedResult.getAbsolutePath()
							+ " does not exist or could not been parsed: "
							+ e.getMessage());
		}
	}

	/**
	 * Helper method of {@link #assessQualities(ClusteringRunResult)}, invoked
	 * to write the assessed clustering qualities into files.
	 * 
	 * @param qualities
	 *            A list containing pairs of parameter sets and corresponding
	 *            clustering qualities of different measures.
	 */
	@Override
	public void writeQualitiesToFile(
			List<Triple<ParameterSet, IClusteringQualitySet, Long>> qualities) {
		// 04.04.2013: adding iteration number into first column
		/*
		 * Write the qualities into the complete file
		 */
		for (Triple<ParameterSet, IClusteringQualitySet, Long> clustSet : qualities) {
			StringBuilder sb = new StringBuilder();
			sb.append(clustSet.getThird());
			sb.append("\t");
			for (int p = 0; p < programConfig.getOptimizableParameters()
					.size(); p++) {
				IProgramParameter<?> param = programConfig
						.getOptimizableParameters().get(p);
				if (p > 0)
					sb.append(",");
				sb.append(clustSet.getFirst().get(param.getName()));
			}
			sb.append("\t");
			for (IClusteringQualityMeasure measure : this.getRun()
					.getQualityMeasures()) {
				sb.append(clustSet.getSecond().get(measure));
				sb.append("\t");
			}
			sb.deleteCharAt(sb.length() - 1);
			sb.append("\n");

			FileUtils.appendStringToFile(completeQualityOutput, sb.toString());
		}
	}

	/**
	 * A wrapper method for the conversion of the run result, which handles
	 * logging and adding the converted result to the results of the run.
	 * 
	 * @return The result of the last iteration converted to the standard
	 *         format.
	 * @throws NoRunResultFormatParserException
	 * @throws RunResultNotFoundException
	 * @throws SecurityException
	 * @throws RunResultConversionException
	 */
	@Override
	public IFileBackedClustering convertResult(
			final IFileBackedClustering result,
			final Map<String, String> effectiveParams,
			final Map<String, String> internalParams)
			throws NoRunResultFormatParserException, RunResultNotFoundException,
			RunResultConversionException {
		/*
		 * Converting and Quality of result
		 */
		this.log.debug(this.getRun() + " (" + this.programConfig + ","
				+ this.dataConfig + ") Converting result files...");

		try {
			IFileBackedClustering convertedResult = convertTo(result,
					this.getRun().getContext().getStandardOutputFormat(),
					internalParams, effectiveParams);
			// TODO: this doesn't make any sense here...
			// ClusteringRunResult clusteringRunResult = new
			// ClusteringRunResult(
			// this.getRun().getRepository(),
			// convertedResult.getChangeDate(), convertedResult.getFile(),
			// dataConfig, programConfig, format,
			// getRun().getRunIdentificationString(), run);
			// synchronized (this.getRun().getResults()) {
			// this.getRun().getResults().add(clusteringRunResult);
			// }
			return convertedResult;
		} catch (NoRunResultFormatParserException e) {
			throw e;
		} catch (RunResultNotFoundException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RunResultConversionException(
					"The runresult could not be converted: " + e.getMessage());
		}
	}

	/**
	 * This method is invoked by {@link #doRunIteration()} before any
	 * calculations are done, to ensure, that all folders and files are created
	 * such that the remainder process can be performed without problems.
	 * 
	 * <p>
	 * This method also initializes the file object attribute variables that are
	 * used throughout the process: {@link #logFile},
	 * {@link #clusteringResultFile} and {@link #resultQualityFile}.
	 */
	protected void initAndEnsureIterationFilesAndFolders(
			final IExecutionIterationWrapper iterationWrapper) {

		int optId = iterationWrapper.getOptId();
		/*
		 * Insert the unique identifier created earlier into the result paths by
		 * replacing the string "%RUNIDENTSTRING". And replace the %OPTID
		 * placeholder by the id corresponding to this optimization iteration
		 */
		String clusteringOutput;
		if (!isResume)
			clusteringOutput = FileUtils.buildPath(
					this.getRun().getRepository().getClusterResultsBasePath()
							.replace("%RUNIDENTSTRING", runThreadIdentString),
					programConfig.toString(".v") + "_"
							+ dataConfig.toString(".v") + "." + optId
							+ ".results");
		else {
			clusteringOutput = FileUtils.buildPath(
					this.getRun().getRepository().getParent()
							.getClusterResultsBasePath()
							.replace("%RUNIDENTSTRING", runThreadIdentString),
					programConfig.toString(".v") + "_"
							+ dataConfig.toString(".v") + "." + optId
							+ ".results");
			if (!new File(clusteringOutput).exists()) {
				String fallback = FileUtils.buildPath(this.getRun()
						.getRepository().getParent().getClusterResultsBasePath()
						.replace("%RUNIDENTSTRING", runThreadIdentString),
						programConfig.getName() + "_" + dataConfig.getName()
								+ "." + optId + ".results");
				if (new File(fallback).exists())
					clusteringOutput = fallback;
			}
		}

		String qualityOutput;
		if (!isResume)
			qualityOutput = FileUtils.buildPath(
					this.getRun().getRepository()
							.getClusterResultsQualityBasePath()
							.replace("%RUNIDENTSTRING", runThreadIdentString),
					programConfig.toString(".v") + "_"
							+ dataConfig.toString(".v") + "." + optId
							+ ".results.qual");
		else {
			qualityOutput = FileUtils.buildPath(
					this.getRun().getRepository().getParent()
							.getClusterResultsQualityBasePath()
							.replace("%RUNIDENTSTRING", runThreadIdentString),
					programConfig.toString(".v") + "_"
							+ dataConfig.toString(".v") + "." + optId
							+ ".results.qual");
			if (!new File(qualityOutput).exists()) {
				String fallback = FileUtils.buildPath(
						this.getRun().getRepository().getParent()
								.getClusterResultsQualityBasePath()
								.replace("%RUNIDENTSTRING",
										runThreadIdentString),
						programConfig.getName() + "_" + dataConfig.getName()
								+ "." + optId + ".results.qual");
				if (new File(fallback).exists())
					qualityOutput = fallback;
			}
		}

		String logOutput;
		if (!isResume)
			logOutput = FileUtils.buildPath(
					this.getRun().getRepository().getLogBasePath()
							.replace("%RUNIDENTSTRING", runThreadIdentString),
					programConfig.toString(".v") + "_"
							+ dataConfig.toString(".v") + "." + optId + ".log");
		else {
			logOutput = FileUtils.buildPath(
					this.getRun().getRepository().getParent().getLogBasePath()
							.replace("%RUNIDENTSTRING", runThreadIdentString),
					programConfig.toString(".v") + "_"
							+ dataConfig.toString(".v") + "." + optId + ".log");
			if (!new File(logOutput).exists())
				logOutput = FileUtils.buildPath(
						this.getRun().getRepository().getParent()
								.getLogBasePath().replace("%RUNIDENTSTRING",
										runThreadIdentString),
						programConfig.getName() + "_" + dataConfig.getName()
								+ "." + optId + ".log");
		}

		/*
		 * if the output already exists, delete it to avoid complications safety
		 * TODO: why are we doing this?
		 */
		// if (new File(clusteringOutput).exists())
		// FileUtils.delete(new File(clusteringOutput));
		// if (new File(qualityOutput).exists())
		// FileUtils.delete(new File(qualityOutput));

		/*
		 * Ensure that the directories to the result and log files exist.
		 */
		iterationWrapper.setLogfile(new File(logOutput));
		iterationWrapper.getLogfile().getParentFile().mkdirs();
		iterationWrapper.setClusteringResultFile(new File(clusteringOutput));
		iterationWrapper.getClusteringResultFile().getParentFile().mkdirs();
		iterationWrapper.setResultQualityFile(new File(qualityOutput));
		iterationWrapper.getResultQualityFile().getParentFile().mkdirs();
	}

	@SuppressWarnings("unused")
	protected IFileBackedClustering convertTo(
			final IFileBackedClustering clustering,
			final IRunResultFormat format,
			final Map<String, String> internalParams,
			final Map<String, String> params)
			throws NoRunResultFormatParserException, RunResultNotFoundException,
			RegisterException, UnknownRunResultFormatException,
			DynamicComponentInitializationException {
		IFileBackedClustering result = null;
		IRunResultFormatParser p = null;

		if (!new File(clustering.getAbsolutePath()).exists())
			throw new RunResultNotFoundException("The result file "
					+ clustering.getAbsolutePath() + " does not exist!");

		File target = new File(clustering.getAbsolutePath() + ".conv");

		/*
		 * We already have the same format
		 */
		if (clustering.getFormat().equals(format)) {
			/*
			 * Just copy the result file and return the corresponding
			 * ClusteringRunResult
			 */
			clustering.copyTo(target);
			return new FileBackedClustering(getRun().getRepository(),
					System.currentTimeMillis(), target,
					RunResultFormat.parseFromString(getRun().getRepository(),
							"TabSeparatedRunResultFormat"));
		}

		try {
			p = clustering.getFormat().getRunResultFormatParser()
					.getConstructor(Map.class, Map.class, String.class,
							DataConfig.class)
					.newInstance(internalParams, params,
							clustering.getAbsolutePath(), getDataConfig());
			if (p != null) {
				p.convertToStandardFormat();
				result = new FileBackedClustering(getRun().getRepository(),
						System.currentTimeMillis(), target,
						RunResultFormat.parseFromString(
								getRun().getRepository(),
								"TabSeparatedRunResultFormat"));
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			if (e.getCause() instanceof FileNotFoundException
					| (e.getCause() instanceof IOException && e.getCause()
							.getMessage().startsWith("Empty file given"))) {
				/*
				 * Ensure, that all the files of this result are deleted
				 */
				FileUtils.delete(clustering.getFile());
				throw new RunResultNotFoundException(e.getCause().getMessage());
			}
			e.printStackTrace();
		} catch (IOException e) {
			/*
			 * Ensure, that all the files of this result are deleted
			 */
			FileUtils.delete(clustering.getFile());
			throw new RunResultNotFoundException(e.getMessage());
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * Set the internal attributes of the framework, e.g. the meanSimilarity
	 * attribute which holds the mean similarity of the input dataset. These
	 * internal attributes are then used later on, to replace parameter
	 * placeholders in the invocation line in
	 * {@link #parseInvocationLineAndEffectiveParameters()}.
	 * 
	 * <p>
	 * This method is invoked in {@link #beforeRun()}, thus is only evaluated
	 * once.
	 * 
	 * <p>
	 * The dataset in standard format is assumed to be loaded before this method
	 * is invoked and to be unloaded after return of this method.
	 * 
	 * @throws UnknownDataSetFormatException
	 * @throws InvalidDataSetFormatVersionException
	 * @throws IOException
	 * @throws IllegalArgumentException
	 */
	protected void setInternalAttributes() throws IllegalArgumentException {
		// TODO; make use of inheritance! (relative and absolute datasets)
		// TODO: move to place, where dataset is loaded?
		IDataSet ds = this.dataConfig.getDatasetConfig().getDataSet()
				.getInStandardFormat();

		if (ds instanceof RelativeDataSet) {
			RelativeDataSet dataSet = (RelativeDataSet) ds;
			this.dataConfig.getRepository()
					.getInternalDoubleAttribute("$("
							+ this.dataConfig.getDatasetConfig().getDataSet()
									.getOriginalDataSet().getAbsolutePath()
							+ ":minSimilarity)")
					.setValue(dataSet.getDataSetContent().getMinValue());
			this.dataConfig.getRepository()
					.getInternalDoubleAttribute("$("
							+ this.dataConfig.getDatasetConfig().getDataSet()
									.getOriginalDataSet().getAbsolutePath()
							+ ":maxSimilarity)")
					.setValue(dataSet.getDataSetContent().getMaxValue());
			this.dataConfig.getRepository()
					.getInternalDoubleAttribute("$("
							+ this.dataConfig.getDatasetConfig().getDataSet()
									.getOriginalDataSet().getAbsolutePath()
							+ ":meanSimilarity)")
					.setValue(dataSet.getDataSetContent().getMean());
		}
		this.dataConfig.getRepository()
				.getInternalIntegerAttribute("$("
						+ this.dataConfig.getDatasetConfig().getDataSet()
								.getOriginalDataSet().getAbsolutePath()
						+ ":numberOfElements)")
				.setValue(ds.getIds().size());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.runnable.RunRunnable#beforeRun()
	 */
	@Override
	protected void beforeRun() throws RunRunnableBeforeExecutionException {
		try {
			this.log.info("Run " + this.getRun() + " (" + this.programConfig
					+ "," + this.dataConfig + ") "
					+ (!isResume ? "started" : "RESUMED")
					+ " (asynchronously)");

			if (checkForInterrupted())
				throw new InterruptedException();

			lastStartTime = System.currentTimeMillis();

			FileUtils.appendStringToFile(this.getRun().getLogFilePath(),
					Formatter.currentTimeAsString(true, "yyyy_MM_dd-HH_mm_ss",
							Locale.UK) + "\tStarting runThread \""
							+ this.getRun() + " (" + this.programConfig + ","
							+ this.dataConfig + ")\""
							+ System.getProperty("line.separator"));

			this.format = programConfig.getOutputFormat();

			this.log.info("Converting and preprocessing dataset ...");
			boolean found = preprocessAndCheckCompatibleDataSetFormat();
			if (!found) {
				IncompatibleDataSetFormatException ex = new IncompatibleDataSetFormatException(
						"The program \"" + programConfig.getProgram()
								+ "\" cannot be run with the dataset format \""
								+ dataConfig.getDatasetConfig().getDataSet()
										.getDataSetFormat()
								+ "\"");
				// otherwise throw exception
				throw ex;
			}

			if (checkForInterrupted())
				throw new InterruptedException();

			try {
				this.log.debug(
						"Loading the input similarities into memory ...");
				// Load the dataset into memory
				IDataSet dataSet = this.dataConfig.getDatasetConfig()
						.getDataSet().getInStandardFormat();
				dataSet.loadIntoMemory(this,
						this.dataConfig.getDatasetConfig()
								.getConversionInputToStandardConfiguration()
								.getSimilarityPrecision());
				// if the original dataset is an absolute dataset, load it into
				// memory as well
				dataSet = this.dataConfig.getDatasetConfig().getDataSet()
						.getOriginalDataSet();
				if (dataSet instanceof AbsoluteDataSet) {
					this.log.debug(
							"Loading the input coordinates into memory ...");
					dataSet.loadIntoMemory(this);
				}

				if (checkForInterrupted())
					throw new InterruptedException();

				/*
				 * Check compatibility of dataset with goldstandard
				 */
				if (this.dataConfig.hasGoldStandardConfig())
					checkCompatibilityDataSetGoldStandard(
							this.dataConfig.getDatasetConfig(),
							this.dataConfig.getGoldstandardConfig());

				assert !this.dataConfig.hasGoldStandardConfig()
						|| this.dataConfig.getGoldstandardConfig()
								.getGoldstandard().isInMemory();
			} catch (IncompleteGoldStandardException e1) {
				// this.exceptions.add(e1);
				// 15.11.2012: missing entries in the goldstandard is no longer
				// a termination criterion. maybe introduce option
				// return;
				// 15.04.2013: since missing entries in the goldstandard distort
				// result qualities, we interrupt again when such an exception
				// is
				// thrown. The user has the possibility of removing samples from
				// the
				// data if this is the case.
				// 12.08.2014: removed again, because of randomized data sets
				// which
				// have points in data set but not in gold standard. -> make it
				// an
				// option definitely
				// throw e1;
			}
			if (checkForInterrupted())
				throw new InterruptedException();

			// 30.06.2014: performing isoMDS calculations in parallel
			ExecutionIterationWrapper wrapper = new ExecutionIterationWrapper();
			wrapper.setDataConfig(this.dataConfig);
			wrapper.setProgramConfig(programConfig);
			wrapper.setRunnable(this);
			wrapper.setOptId(-1);

			IsoMDSIterationRunnable iterationRunnable = new IsoMDSIterationRunnable(
					wrapper);

			this.submitIterationRunnable(iterationRunnable);

			if (checkForInterrupted())
				throw new InterruptedException();

			// 30.06.2014: performing isoMDS calculations in parallel
			wrapper = new ExecutionIterationWrapper();
			wrapper.setDataConfig(this.dataConfig);
			wrapper.setProgramConfig(programConfig);
			wrapper.setRunnable(this);
			wrapper.setOptId(-2);

			PCARunnable pcaRunnable = new PCARunnable(wrapper);

			this.submitIterationRunnable(pcaRunnable);

			setInternalAttributes();

			/*
			 * Ensure that the target directory exists
			 */
			if (!isResume)
				new File(this.getRun().getRepository()
						.getClusterResultsQualityBasePath()
						.replace("%RUNIDENTSTRING", runThreadIdentString))
								.mkdirs();
			else
				new File(this.getRun().getRepository().getParent()
						.getClusterResultsQualityBasePath()
						.replace("%RUNIDENTSTRING", runThreadIdentString))
								.mkdirs();

			/*
			 * Writing all the qualities of the optimization process into one
			 * file
			 */
			if (!isResume)
				completeQualityOutput = FileUtils.buildPath(this.getRun()
						.getRepository().getClusterResultsQualityBasePath()
						.replace("%RUNIDENTSTRING", runThreadIdentString),
						programConfig.toString(".v") + "_"
								+ dataConfig.toString(".v")
								+ ".results.qual.complete");
			else {
				// fallback to file names without version
				completeQualityOutput = FileUtils.buildPath(
						this.getRun().getRepository().getParent()
								.getClusterResultsQualityBasePath()
								.replace("%RUNIDENTSTRING",
										runThreadIdentString),
						programConfig.toString(".v") + "_"
								+ dataConfig.toString(".v")
								+ ".results.qual.complete");
				if (!new File(completeQualityOutput).exists()) {
					String completeQualityOutputFallback = FileUtils.buildPath(
							this.getRun().getRepository().getParent()
									.getClusterResultsQualityBasePath()
									.replace("%RUNIDENTSTRING",
											runThreadIdentString),
							programConfig.getName() + "_" + dataConfig.getName()
									+ ".results.qual.complete");
					if (new File(completeQualityOutputFallback).exists())
						completeQualityOutput = completeQualityOutputFallback;
				}
			}

			if (!isResume)
				completeMetaDataFile = FileUtils.buildPath(this.getRun()
						.getRepository().getClusterResultsQualityBasePath()
						.replace("%RUNIDENTSTRING", runThreadIdentString),
						programConfig.toString(".v") + "_"
								+ dataConfig.toString(".v")
								+ ".metadata.complete");
			else
				completeMetaDataFile = FileUtils.buildPath(
						this.getRun().getRepository().getParent()
								.getClusterResultsQualityBasePath()
								.replace("%RUNIDENTSTRING",
										runThreadIdentString),
						programConfig.toString(".v") + "_"
								+ dataConfig.toString(".v")
								+ ".metadata.complete");
		} catch (Throwable e) {
			throw new RunRunnableBeforeExecutionException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.runnable.RunRunnable#afterRun()
	 */
	@Override
	protected void afterRun() throws InterruptedException, RegisterException {
		try {
			super.afterRun();
			this.getResult().register();
		} finally {
			// unload the dataset from memory
			IDataSet dataSet = this.dataConfig.getDatasetConfig().getDataSet()
					.getInStandardFormat();
			if (dataSet != null)
				dataSet.unloadFromMemory(this);
			// if the original dataset is an absolute dataset, unload it from
			// memory
			// as well
			dataSet = this.dataConfig.getDatasetConfig().getDataSet()
					.getOriginalDataSet();
			if (dataSet != null && dataSet instanceof AbsoluteDataSet)
				dataSet.unloadFromMemory(this);

			if (dataConfig.hasGoldStandardConfig())
				dataConfig.getGoldstandardConfig().getGoldstandard()
						.unloadFromMemory();
		}

		FileUtils.appendStringToFile(this.getRun().getLogFilePath(),
				Formatter.currentTimeAsString(true, "yyyy_MM_dd-HH_mm_ss",
						Locale.UK) + "\tFinished runThread \"" + this.getRun()
						+ " (" + this.programConfig + "," + this.dataConfig
						+ ")\" (Duration "
						+ Formatter.formatMsToDuration(runningTime
								+ (System.currentTimeMillis() - lastStartTime))
						+ ")" + System.getProperty("line.separator"));

		this.log.info("Run " + this.getRun() + " (" + this.programConfig + ","
				+ this.dataConfig + ") finished");
	}

	/**
	 * Overwrite this method in your subclass, if you want to handle missing run
	 * results individually.
	 * 
	 * <p>
	 * This can comprise actions ensuring that further iterations can be
	 * executed smoothly.
	 */
	@Override
	public void handleMissingRunResult(
			final IExecutionIterationWrapper iterationWrapper) {
		final Map<String, String> effectiveParams = iterationWrapper
				.getEffectiveParams();
		final int optId = iterationWrapper.getOptId();
		/*
		 * There is no results file
		 */
		/*
		 * Write the minimal quality values into the complete results file
		 */
		StringBuilder sb = new StringBuilder();
		sb.append(optId);
		sb.append("\t");
		for (int p = 0; p < iterationWrapper.getProgramConfig()
				.getOptimizableParameters().size(); p++) {
			IProgramParameter<?> param = iterationWrapper.getProgramConfig()
					.getOptimizableParameters().get(p);
			if (p > 0)
				sb.append(",");
			sb.append(effectiveParams.get(param.getName()));
		}
		sb.append("\t");
		for (int i = 0; i < this.getRun().getQualityMeasures().size(); i++) {
			sb.append(ClusteringQualityMeasureValue.NOT_TERMINATED);
			sb.append("\t");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.append("\n");

		FileUtils.appendStringToFile(this.getCompleteQualityOutput(),
				sb.toString());
	}

	@Override
	public void writeIterationMetaDataToFile(int optId,
			Map<String, String> iterationMetaData) {
		StringBuilder sb = new StringBuilder();
		sb.append(optId);
		for (String key : iterationMetaAttributes) {
			sb.append("\t");
			if (iterationMetaData.containsKey(key))
				sb.append(iterationMetaData.get(key));
			else
				sb.append("");
		}
		sb.append("\n");

		FileUtils.appendStringToFile(completeMetaDataFile, sb.toString());
	}
}