/**
 * 
 */
package de.clusteval.run.runnable;

import java.util.Map;

import de.clusteval.cluster.quality.ClusteringQualityMeasureValue;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RunResultRepository;
import de.clusteval.framework.threading.IRunSchedulerThread;
import de.clusteval.program.IProgramParameter;
import de.clusteval.program.ParameterSet;
import de.clusteval.run.ExecutionRun;
import de.clusteval.run.runresult.NoRunResultFormatParserException;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * @author Christian Wiwie
 * 
 */
public class IsoMDSIterationRunnable
		extends
			IterationRunnable<ExecutionIterationWrapper, IExecutionRunRunnable> {

	protected Process proc;

	protected NoRunResultFormatParserException noRunResultException;

	/**
	 * @param iterationWrapper
	 */
	public IsoMDSIterationRunnable(
			final ExecutionIterationWrapper iterationWrapper) {
		super(iterationWrapper);
	}

	/**
	 * @return the noRunResultException
	 */
	public NoRunResultFormatParserException getNoRunResultException() {
		return noRunResultException;
	}

	public int getIterationNumber() {
		return this.iterationWrapper.getOptId();
	}

	public ParameterSet getParameterSet() {
		return this.iterationWrapper.parameterSet;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IterationRunnable#getRun()
	 */
	@Override
	public ExecutionRun getRun() {
		return (ExecutionRun) super.getRun();
	}

	public Process getProcess() {
		return this.proc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IterationRunnable#doRun()
	 */
	@Override
	protected void doRun() throws InterruptedException {
		IRunSchedulerThread scheduler = null;
		IRepository repo = getRun().getRepository();
		if (repo instanceof RunResultRepository)
			repo = repo.getParent();
		scheduler = repo.getSupervisorThread().getRunScheduler();
		scheduler.informOnStartedIterationRunnable(Thread.currentThread(),
				this);

		try {
			this.log.debug(
					"Assessing isoMDS coordinates of dataset samples ...");
			// Plotter.assessAndWriteIsoMDSCoordinates(dcMDS);
		} catch (Throwable e) {
			e.printStackTrace();
		} finally {
			scheduler.informOnFinishedIterationRunnable(Thread.currentThread(),
					this);
		}
	}

	/**
	 * Overwrite this method in your subclass, if you want to handle missing run
	 * results individually.
	 * 
	 * <p>
	 * This can comprise actions ensuring that further iterations can be
	 * executed smoothly.
	 */
	protected void handleMissingRunResult(
			final ExecutionIterationWrapper iterationWrapper) {
		final Map<String, String> effectiveParams = iterationWrapper
				.getEffectiveParams();
		final Map<String, String> internalParams = iterationWrapper
				.getInternalParams();
		final int optId = iterationWrapper.getOptId();
		/*
		 * There is no results file
		 */
		/*
		 * Write the minimal quality values into the complete results file
		 */
		StringBuilder sb = new StringBuilder();
		sb.append(optId);
		sb.append("\t");
		for (int p = 0; p < iterationWrapper.programConfig
				.getOptimizableParameters().size(); p++) {
			IProgramParameter<?> param = iterationWrapper.programConfig
					.getOptimizableParameters().get(p);
			if (p > 0)
				sb.append(",");
			sb.append(effectiveParams.get(param.getName()));
		}
		sb.append("\t");
		for (int i = 0; i < this.getRun().getQualityMeasures().size(); i++) {
			sb.append(ClusteringQualityMeasureValue.NOT_TERMINATED);
			sb.append("\t");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.append("\n");

		FileUtils.appendStringToFile(
				getParentRunnable().getCompleteQualityOutput(), sb.toString());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IterationRunnable#getParentRunnable()
	 */
	@Override
	public ExecutionRunRunnable getParentRunnable() {
		return (ExecutionRunRunnable) super.getParentRunnable();
	}
}
