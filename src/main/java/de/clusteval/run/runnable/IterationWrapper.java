/**
 * 
 */
package de.clusteval.run.runnable;

import java.io.File;

import de.clusteval.data.IDataConfig;

public class IterationWrapper implements IIterationWrapper {

	/**
	 * A temporary variable holding a file object pointing to the absolute path
	 * of the current log output file during execution of the runnable
	 */
	protected File logfile;

	protected IRunRunnable runnable;

	protected IDataConfig dataConfig;

	protected boolean isResume;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IIterationWrapper#isResume()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IIterationWrapper#isResume()
	 */
	@Override
	public boolean isResume() {
		return isResume;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IIterationWrapper#setResume(boolean)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IIterationWrapper#setResume(boolean)
	 */
	@Override
	public void setResume(boolean isResume) {
		this.isResume = isResume;
	}

	public IterationWrapper() {
		super();
	}

	@Override
	public File getLogfile() {
		return logfile;
	}

	@Override
	public void setLogfile(File logfile) {
		this.logfile = logfile;
	}

	@Override
	public IDataConfig getDataConfig() {
		return dataConfig;
	}

	@Override
	public void setDataConfig(IDataConfig dataConfig) {
		this.dataConfig = dataConfig;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IIterationWrapper#getRunnable()
	 */
	@Override
	public IRunRunnable getRunnable() {
		return runnable;
	}

	@Override
	public void setRunnable(IRunRunnable runnable) {
		this.runnable = runnable;
	}
}