/**
 * 
 */
package de.clusteval.run.runnable;

import de.clusteval.run.statistics.IRunDataStatistic;

/**
 * @author Christian Wiwie
 *
 */
public class RunDataAnalysisIterationWrapper
		extends
			AnalysisIterationWrapper<IRunDataStatistic>
		implements
			IRunDataAnalysisIterationWrapper {

	protected String uniqueRunAnalysisRunIdentifier;

	protected String uniqueDataAnalysisRunIdentifier;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IRunDataAnalysisIterationWrapper#
	 * getUniqueDataAnalysisRunIdentifier()
	 */
	@Override
	public String getUniqueDataAnalysisRunIdentifier() {
		return uniqueDataAnalysisRunIdentifier;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IRunDataAnalysisIterationWrapper#
	 * setUniqueDataAnalysisRunIdentifier(java.lang.String)
	 */
	@Override
	public void setUniqueDataAnalysisRunIdentifier(
			String uniqueDataAnalysisRunIdentifier) {
		this.uniqueDataAnalysisRunIdentifier = uniqueDataAnalysisRunIdentifier;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IRunDataAnalysisIterationWrapper#
	 * getRunIdentifier()
	 */
	@Override
	public String getRunIdentifier() {
		return this.uniqueRunAnalysisRunIdentifier;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IRunDataAnalysisIterationWrapper#
	 * setRunIdentifier(java.lang.String)
	 */
	@Override
	public void setRunIdentifier(final String runIdentifier) {
		this.uniqueRunAnalysisRunIdentifier = runIdentifier;
	}
}
