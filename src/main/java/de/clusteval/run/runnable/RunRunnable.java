/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run.runnable;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.clusteval.data.dataset.format.IncompatibleDataSetFormatException;
import de.clusteval.data.dataset.format.InvalidDataSetFormatVersionException;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.goldstandard.IncompleteGoldStandardException;
import de.clusteval.data.goldstandard.format.UnknownGoldStandardFormatException;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RunResultRepository;
import de.clusteval.framework.threading.IRunSchedulerThread;
import de.clusteval.framework.threading.RunSchedulerThread;
import de.clusteval.run.IRun;
import de.clusteval.run.RUN_STATUS;
import de.clusteval.run.Run;
import de.clusteval.run.runresult.IRunResult;
import de.clusteval.utils.InternalAttributeException;
import dk.sdu.imada.compbio.utils.ProgressPrinter;

/**
 * An abstract class that corresponds to a smaller atomic part of a {@link Run}.
 * A runnable is executed asynchronously and no order among runnables is
 * guaranteed.
 * 
 * <p>
 * Objects of subclasses of this class are created in
 * {@link Run#perform(RunSchedulerThread)} (in subclasses) and later executed by
 * the RunScheduler. One instance represents the execution of one subunit of the
 * overall run. The results are added and stored in {@link Run#results} in a
 * synchronized way.
 * 
 * <p>
 * In the class hierarchy this class corresponds to the {@link Run} class.
 * 
 * @author Christian Wiwie
 */
public abstract class RunRunnable<RUN_TYPE extends IRun<?>, IR extends IIterationRunnable<?, ?>, IW extends IIterationWrapper, RESULT extends IRunResult>
		implements
			IRunRunnable<RUN_TYPE, IR, IW, RESULT> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2296368219576025824L;

	/**
	 * The run this runnable object was created by.
	 */
	protected RUN_TYPE run;

	/**
	 * If exceptions are thrown during the execution it is stored in the
	 * following attributes. It will not been thrown automatically, to avoid
	 * disrupting the successive optimization iterations. If one wants to check
	 * for these exceptions afterwards, one can use the corresponding getter
	 * methods.
	 */
	protected List<Throwable> exceptions;

	/**
	 * Exceptions that occurred during execution of this runnable, that are
	 * handled during execution and do not led to termination of the run
	 * runnable.
	 */
	protected List<Throwable> warningExceptions;

	/**
	 * Keep track of the progress of this runnable. In case of parameter
	 * optimization mode, it will increase by one after every percent reached of
	 * the parameter sets to evaluate.
	 */
	protected ProgressPrinter progress, progressBeforeExecution;

	/**
	 * This attribute indicates, whether this run is a resumption of a previous
	 * execution or a completely new execution.
	 */
	protected boolean isResume;

	/**
	 * A logger that keeps track of all actions done by the runnable.
	 */
	protected transient Logger log;

	/**
	 * This object can be used to get the status of the runnable thread.
	 */
	protected transient Future<?> future;

	/**
	 * True, if this runnable is paused, false otherwise.
	 */
	protected boolean paused;

	/**
	 * This attribute holds the running time after finishing the runnable.
	 */
	protected long runningTime;

	/**
	 * This attribute is used to store the last start time in case this runnable
	 * is paused and resumed.
	 */
	protected long lastStartTime;

	/**
	 * The unique identification string of the run which is used to store the
	 * results in a unique folder to avoid overwriting.
	 */
	protected String runThreadIdentString;

	protected transient List<IterationRunnable> iterationRunnables;

	protected volatile long iterationRunnableRunningTimeMax;
	protected volatile long iterationRunnableRunningTimeSum;
	protected volatile int finishedIterationRunnableCount;

	/**
	 * This list holds wrapper objects for each iteration runnable started.
	 */
	protected transient List<Future<?>> futures;

	/**
	 * This boolean helper indicates, whether this run runnable has been
	 * terminated. We use this boolean instead of the future of this run
	 * runnable to signal termination, because the run thread belonging to this
	 * runnable will immediately terminate if this run runnable future is
	 * cancelled. However, in some cases the termination of this run runnable's
	 * iteration runnables takes some time. Then we want to set the future of
	 * this runnabel to cancalled only after all iteration runnables are
	 * terminated.
	 */
	protected boolean terminated;

	/**
	 * A map from futures to iteration runnables to be able to handle
	 * termination of threads and processes started in the respective iteration
	 * run runnable in {@link #afterRun()}
	 */
	protected transient Map<Future<?>, IterationRunnable> futureToIterationRunnable;

	/**
	 * Instantiates a new run runnable.
	 * 
	 * @param run
	 *            The run this runnable belongs to.
	 * @param runIdentString
	 *            The unique identification string of the run which is used to
	 *            store the results in a unique folder to avoid overwriting.
	 * @param isResume
	 *            True, if this run is a resumption of a previous execution or a
	 *            completely new execution.
	 */
	public RunRunnable(final RUN_TYPE run, final String runIdentString,
			final boolean isResume) {
		super();
		this.run = run;
		this.isResume = isResume;
		this.exceptions = new ArrayList<Throwable>();
		this.warningExceptions = new ArrayList<Throwable>();
		this.runThreadIdentString = runIdentString;
		this.iterationRunnables = new ArrayList<IterationRunnable>();
		this.futures = new ArrayList<Future<?>>();
		this.futureToIterationRunnable = new HashMap<Future<?>, IterationRunnable>();
		this.log = LoggerFactory.getLogger(this.getClass());
		this.progress = new ProgressPrinter(10000, false);
		this.progressBeforeExecution = new ProgressPrinter(10000, false);
	}

	/**
	 * Checks whether this runnable thread has been interrupted. If yes, it
	 * prints out a simple log statement and.
	 * 
	 * @return True, if this thread was interrupted, false otherwise.
	 */
	@Override
	public final boolean checkForInterrupted() {
		if (isInterrupted()) {
			this.log.info(
					"Caught the signal to terminate. Terminating as soon as possible...");
			return true;
		}
		return false;
	}

	/**
	 * Checks whether this runnable thread has been interrupted.
	 * 
	 * @return True, if this thread was interrupted, false otherwise.
	 */
	@Override
	public final boolean isInterrupted() {
		// return this.future.isCancelled();
		return this.terminated;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IRunRunnable#run()
	 */
	@Override
	public final void run() {
		this.run.setStatus(RUN_STATUS.RUNNING);
		try {
			beforeRun();
			doRun();
		} catch (Throwable e) {
			if (!(e.getCause() instanceof InterruptedException)) {
				this.exceptions.add(e);
			}
		} finally {
			try {
				afterRun();
			} catch (InterruptedException e) {
			} catch (RegisterException e) {
				this.exceptions.add(e);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IRunRunnable#terminate()
	 */
	@Override
	public void terminate() {
		this.log.info("Terminating runnable ...");
		this.terminated = true;
		for (Future<?> f : this.futures)
			f.cancel(true);
	}

	/**
	 * This method is invoked by {@link #run()} before anything else is done. It
	 * can be overwritten and used in subclasses to do any precalculations or
	 * requirements like copying or moving files.
	 * 
	 * @throws IOException
	 * @throws IllegalArgumentException
	 * @throws InvalidDataSetFormatVersionException
	 * @throws UnknownDataSetFormatException
	 * @throws RegisterException
	 * @throws InternalAttributeException
	 * @throws IncompatibleDataSetFormatException
	 * @throws UnknownGoldStandardFormatException
	 * @throws IncompleteGoldStandardException
	 * @throws InterruptedException
	 * @throws RunRunnableBeforeExecutionException
	 */

	@SuppressWarnings("unused")
	protected void beforeRun() throws RunRunnableBeforeExecutionException {
		this.futures.clear();
		this.iterationRunnableRunningTimeMax = 0l;
	}

	/**
	 * This method is invoked by {@link #run()} after {@link #beforeRun()} has
	 * finished and is responsible for the operation and execution of the
	 * runnable itself.
	 */
	protected void doRun() throws RunIterationException {
		while (this.hasNextIteration()) {
			if (checkForInterrupted())
				return;
			final IW iterationWrapper = this.createIterationWrapper();
			try {
				this.decorateIterationWrapper(iterationWrapper,
						consumeNextIteration());
				this.doRunIteration(iterationWrapper);
			} catch (RunIterationException e) {
				if (e.getCause() != null
						&& (e.getCause() instanceof SkipIterationException))
					// we skip this iteration and continue with the next
					continue;
				throw e;
			}
		}
	}

	/**
	 * @return the run
	 */
	public RUN_TYPE getRun() {
		return run;
	}

	protected abstract boolean hasNextIteration();

	protected abstract int consumeNextIteration() throws RunIterationException;

	protected abstract void doRunIteration(IW iterationWrapper)
			throws RunIterationException;

	/**
	 * This method is invoked by {@link #run()} after {@link #doRun()} has
	 * finished. It is responsible for cleaning up all files, folders and for
	 * doing all kinds of postcalculations.
	 * 
	 * @throws InterruptedException
	 * @throws RegisterException
	 */
	protected void afterRun() throws InterruptedException, RegisterException {
		// wait for all iteration runnables to finish
		for (Future<?> f : this.futures)
			try {
				f.get();
				this.exceptions.addAll(
						this.futureToIterationRunnable.get(f).getExceptions());
				this.warningExceptions.addAll(this.futureToIterationRunnable
						.get(f).getWarningExceptions());
			} catch (InterruptedException e) {
				// here we handle termination of all threads or processes , that
				// have been started by the iteration run runnables.
				Runnable run = this.futureToIterationRunnable.remove(f);
				if (run instanceof ClusteringIterationRunnable) {
					Process p = ((ClusteringIterationRunnable) run)
							.getProcess();
					if (p != null)
						p.destroyForcibly();
				}
			} catch (ExecutionException e) {
				e.printStackTrace();
			}

		// print exceptions
		if (this.exceptions.size() > 0) {
			this.log.warn(
					"During the execution of this run runnable exceptions were thrown:");
			for (Throwable t : this.exceptions) {
				this.log.warn(t.toString());

				StringWriter writer = new StringWriter();
				t.printStackTrace(new PrintWriter(writer));
				String message = writer.toString();
				String[] split = message
						.split(System.getProperty("line.separator"));
				for (int i = 1; i < split.length; i++)
					this.log.warn("|--> " + split[i]);
			}
		}
		this.progress.update(10000);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IRunRunnable#waitFor()
	 */
	@Override
	public final void waitFor()
			throws InterruptedException, ExecutionException {
		while (!this.futures.isEmpty()) {
			boolean nullPointerException = true;
			while (nullPointerException) {
				try {
					Future<?> f = this.futures.remove(0);
					f.get();
				} catch (NullPointerException e) {
					continue;
				} catch (CancellationException e) {
				}
				nullPointerException = false;
			}
		}

		boolean nullPointerException = true;
		while (nullPointerException) {
			try {
				this.future.get();
			} catch (NullPointerException e) {
				continue;
			} catch (CancellationException e) {
			}
			nullPointerException = false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IRunRunnable#isPaused()
	 */
	@Override
	public boolean isPaused() {
		return this.paused;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IRunRunnable#isCancelled()
	 */
	@Override
	public boolean isCancelled() {
		if (this.future != null)
			return this.future.isCancelled();
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IRunRunnable#isDone()
	 */
	@Override
	public boolean isDone() {
		if (this.future != null)
			return this.future.isDone();
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IRunRunnable#pause()
	 */
	@Override
	public void pause() {
		this.paused = true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IRunRunnable#resume()
	 */
	@Override
	public void resume() {
		this.paused = false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IRunRunnable#getFuture()
	 */
	@Override
	public Future<?> getFuture() {
		return this.future;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IRunRunnable#getExceptions()
	 */
	@Override
	public List<Throwable> getExceptions() {
		return this.exceptions;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IRunRunnable#clearExceptions()
	 */
	@Override
	public void clearExceptions() {
		this.exceptions.clear();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IRunRunnable#getWarningExceptions()
	 */
	@Override
	public List<Throwable> getWarningExceptions() {
		return warningExceptions;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IRunRunnable#clearWarningExceptions()
	 */
	@Override
	public void clearWarningExceptions() {
		this.warningExceptions.clear();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IRunRunnable#getProgressPrinter()
	 */
	@Override
	public ProgressPrinter getProgressPrinter() {
		return this.progress;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runnable.IRunRunnable#getProgressPrinterBeforeExecution(
	 * )
	 */
	@Override
	public ProgressPrinter getProgressPrinterBeforeExecution() {
		return this.progressBeforeExecution;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IRunRunnable#getStartTime()
	 */
	@Override
	public long getStartTime() {
		return lastStartTime;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IRunRunnable#getPercentFinished()
	 */
	@Override
	public float getPercentFinished() {
		synchronized (this.progress) {
			return this.progress.getPercent();
		}
	}

	protected float getPercentFinishedBeforeExecution() {
		synchronized (this.progressBeforeExecution) {
			return this.progressBeforeExecution.getPercent();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IRunRunnable#
	 * getPercentFinishedDuringCurrentExecution()
	 */
	@Override
	public float getPercentFinishedDuringCurrentExecution() {
		return getPercentFinished() - getPercentFinishedBeforeExecution();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runnable.IRunRunnable#getIterationRunnableRunningTimeSum
	 * ()
	 */
	@Override
	public long getIterationRunnableRunningTimeSum() {
		return iterationRunnableRunningTimeSum;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runnable.IRunRunnable#getEstimatedRemainingCPUTimeInMs()
	 */
	@Override
	public long getEstimatedRemainingCPUTimeInMs() {
		// long startTimeMillis = this.getIterationRunnableRunningTimeSum();
		float percentFinished = this.getPercentFinishedDuringCurrentExecution();
		float totalPercentFinished = this.getPercentFinished();
		// * 100: scale up to 100 percent
		// long msecondsFor100Percent = (long) (startTimeMillis /
		// percentFinished * (100 - totalPercentFinished));
		long startTimeMillis = this.iterationRunnableRunningTimeMax
				* this.finishedIterationRunnableCount;
		long msecondsFor100Percent = (long) (startTimeMillis / percentFinished
				* (100 - totalPercentFinished));
		return msecondsFor100Percent;
	}

	protected abstract IR createIterationRunnable(final IW iterationWrapper);

	protected abstract IW createIterationWrapper();

	protected void decorateIterationWrapper(final IW iterationWrapper,
			final int currentPos) throws RunIterationException {
		iterationWrapper.setResume(isResume);
	}

	protected void submitIterationRunnable(
			final IterationRunnable iterationRunnable) {
		// we do not accept new runnables if this run has been terminated
		// before.
		if (this.terminated)
			return;
		this.iterationRunnables.add(iterationRunnable);

		final IRunSchedulerThread runScheduler;
		if (this.getRun().getRepository() instanceof RunResultRepository)
			runScheduler = this.getRun().getRepository().getParent()
					.getSupervisorThread().getRunScheduler();
		else
			runScheduler = this.getRun().getRepository().getSupervisorThread()
					.getRunScheduler();
		Future<?> f = runScheduler.registerIterationRunnable(iterationRunnable);
		this.futures.add(f);
		this.futureToIterationRunnable.put(f, iterationRunnable);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runnable.IRunRunnable#reportIterationRunnableRunningTime
	 * (long)
	 */
	@Override
	public synchronized void reportIterationRunnableRunningTime(
			final long runningTime) {
		this.iterationRunnableRunningTimeSum += runningTime;
		this.finishedIterationRunnableCount++;
		this.iterationRunnableRunningTimeMax = Math
				.max(iterationRunnableRunningTimeMax, runningTime);
	}
}

/**
 * This class is responsible for reading and emptying the streams of the started
 * thread. This has to be done in order to avoid overflowing streams and thus
 * possible termination of the thread by the operating system.
 * 
 */
class StreamGobbler extends Thread {

	InputStream is;
	BufferedWriter bw;

	public StreamGobbler(InputStream is, BufferedWriter bw) {
		super();
		this.setName(this.getName().replace("Thread",
				this.getClass().getSimpleName()));
		// TODO this.setPriority(NORM_PRIORITY-1);
		this.is = is;
		this.bw = bw;
	}

	@Override
	public void run() {
		try {
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line = null;
			while ((line = br.readLine()) != null)
				synchronized (bw) {
					bw.append(line);
					bw.newLine();
					bw.flush();
				}
		} catch (IOException ioe) {
			// stream closed
			// ioe.printStackTrace();
		}
	}
}
