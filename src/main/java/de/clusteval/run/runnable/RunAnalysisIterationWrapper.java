/**
 * 
 */
package de.clusteval.run.runnable;

import de.clusteval.run.statistics.IRunStatistic;

/**
 * @author Christian Wiwie
 *
 */
public class RunAnalysisIterationWrapper
		extends
			AnalysisIterationWrapper<IRunStatistic>
		implements
			IRunAnalysisIterationWrapper {

	protected String uniqueRunAnalysisRunIdentifier;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runnable.IRunAnalysisIterationWrapper#getRunIdentifier()
	 */
	@Override
	public String getRunIdentifier() {
		return this.uniqueRunAnalysisRunIdentifier;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runnable.IRunAnalysisIterationWrapper#setRunIdentifier(
	 * java.lang.String)
	 */
	@Override
	public void setRunIdentifier(final String runIdentifier) {
		this.uniqueRunAnalysisRunIdentifier = runIdentifier;
	}
}
