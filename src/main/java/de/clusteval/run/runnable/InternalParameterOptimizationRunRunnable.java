/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run.runnable;

import java.io.File;
import java.util.Map;

import de.clusteval.data.IDataConfig;
import de.clusteval.framework.threading.IRunSchedulerThread;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.run.IInternalParameterOptimizationRun;
import de.clusteval.run.runresult.IParameterOptimizationResult;

/**
 * @author Christian Wiwie
 * 
 */
public class InternalParameterOptimizationRunRunnable
		extends
			ExecutionRunRunnable<IInternalParameterOptimizationRun, IParameterOptimizationResult>
		implements
			IInternalParameterOptimizationRunRunnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7582851016110602390L;
	protected boolean hasNext = true;

	/**
	 * @param runScheduler
	 * @param run
	 * @param programConfig
	 * @param dataConfig
	 * @param runIdentString
	 * @param isResume
	 */
	public InternalParameterOptimizationRunRunnable(
			IRunSchedulerThread runScheduler,
			IInternalParameterOptimizationRun run, IProgramConfig programConfig,
			IDataConfig dataConfig, String runIdentString, boolean isResume,
			Map<IProgramParameter<?>, String> runParams) {
		super(run, programConfig, dataConfig, runIdentString, isResume,
				runParams);
		this.future = runScheduler.registerRunRunnable(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.runnable.ExecutionRunRunnable#getInvocationFormat()
	 */
	@Override
	protected String getInvocationFormat() {
		return programConfig.getInvocationFormatParameterOptimization(
				!dataConfig.hasGoldStandardConfig());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.runnable.ExecutionRunRunnable#beforeClustering()
	 */
	@Override
	protected void beforeRun() throws RunRunnableBeforeExecutionException {
		super.beforeRun();
		try {
			if (!new File(completeQualityOutput).exists())
				writeHeaderIntoCompleteFile(completeQualityOutput);

			if (!new File(completeMetaDataFile).exists())
				writeHeaderIntoCompleteMetaDataFile(completeMetaDataFile);
		} catch (Throwable t) {
			throw new RunRunnableBeforeExecutionException(t);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runnable.ExecutionRunRunnable#decorateIterationWrapper
	 * (de.clusteval.run.runnable.ExecutionIterationWrapper, int)
	 */
	@Override
	protected void decorateIterationWrapper(
			IExecutionIterationWrapper iterationWrapper, int currentPos)
			throws RunIterationException {
		super.decorateIterationWrapper(iterationWrapper, currentPos);
		iterationWrapper.setOptId(1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.runnable.ExecutionRunRunnable#handleMissingRunResult()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IInternalParameterOptimizationRunRunnable#
	 * handleMissingRunResult(de.clusteval.run.runnable.
	 * IExecutionIterationWrapper)
	 */
	@Override
	public void handleMissingRunResult(
			final IExecutionIterationWrapper iterationWrapper) {
		this.log.info(this.getRun() + " (" + this.programConfig + ","
				+ this.dataConfig
				+ ") The result of this run could not be found. Please consult the log files of the program");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.RunRunnable#hasNextIteration()
	 */
	@Override
	protected boolean hasNextIteration() {
		return this.hasNext;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.RunRunnable#consumeNextIteration()
	 */
	@Override
	protected int consumeNextIteration() throws RunIterationException {
		this.hasNext = false;
		return 1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IRunRunnable#getRunResult()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IInternalParameterOptimizationRunRunnable#
	 * getRunResult()
	 */
	@Override
	public IParameterOptimizationResult getResult() {
		// TODO
		return null;
	}
}
