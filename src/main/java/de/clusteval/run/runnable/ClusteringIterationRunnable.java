/**
 * 
 */
package de.clusteval.run.runnable;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.slf4j.LoggerFactory;

import de.clusteval.cluster.FileBackedClustering;
import de.clusteval.cluster.IClustering;
import de.clusteval.cluster.IFileBackedClustering;
import de.clusteval.cluster.quality.IClusteringQualitySet;
import de.clusteval.data.IDataConfig;
import de.clusteval.framework.RLibraryNotLoadedException;
import de.clusteval.framework.repository.Repository;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.ParameterSet;
import de.clusteval.program.r.IRProgram;
import de.clusteval.program.r.RProcess;
import de.clusteval.program.r.RProgram;
import de.clusteval.run.ExecutionRun;
import de.clusteval.run.runresult.NoRunResultFormatParserException;
import de.clusteval.run.runresult.format.IRunResultFormat;
import de.clusteval.run.runresult.format.RunResultNotFoundException;
import de.clusteval.run.runresult.postprocessing.IRunResultPostprocessor;
import de.clusteval.utils.RNotAvailableException;
import dk.sdu.imada.compbio.utils.StringExt;
import dk.sdu.imada.compbio.utils.Triple;
import dk.sdu.imada.compbio.utils.parse.TextFileParser;
import dk.sdu.imada.compbio.utils.parse.TextFileParser.OUTPUT_MODE;

/**
 * @author Christian Wiwie
 * 
 */
public class ClusteringIterationRunnable<PARENT_TYPE extends IExecutionRunRunnable<?, ?>>
		extends
			IterationRunnable<IExecutionIterationWrapper, PARENT_TYPE>
		implements
			IClusteringIterationRunnable<PARENT_TYPE> {

	protected Process proc;

	protected NoRunResultFormatParserException noRunResultException;
	protected IRunResultFormat format;
	protected Map<String, String> iterationMetaData;

	/**
	 * @param iterationWrapper
	 */
	public ClusteringIterationRunnable(
			final IExecutionIterationWrapper iterationWrapper,
			final IRunResultFormat format) {
		super(iterationWrapper);
		this.format = format;
		this.iterationMetaData = new HashMap<>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IClusteringIterationRunnable#
	 * getNoRunResultException()
	 */
	@Override
	public NoRunResultFormatParserException getNoRunResultException() {
		return noRunResultException;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runnable.IClusteringIterationRunnable#getIterationNumber
	 * ()
	 */
	@Override
	public int getIterationNumber() {
		return this.iterationWrapper.getOptId();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runnable.IClusteringIterationRunnable#getParameterSet()
	 */
	@Override
	public ParameterSet getParameterSet() {
		return this.iterationWrapper.getParameterSet();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IterationRunnable#getRun()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IClusteringIterationRunnable#getRun()
	 */
	@Override
	public ExecutionRun getRun() {
		return (ExecutionRun) super.getRun();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IClusteringIterationRunnable#getProcess()
	 */
	@Override
	public Process getProcess() {
		return this.proc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IterationRunnable#doRun()
	 */
	@Override
	protected void doRun() throws InterruptedException {
		this.currentState = "Started";
		try {
			try {
				if (getParentRunnable().isInterrupted())
					return;

				IProgramConfig programConfig = iterationWrapper
						.getProgramConfig();
				IDataConfig dataConfig = iterationWrapper.getDataConfig();

				this.log.info(String.format("%s (%s,%s, Iteration %d) %s",
						getRun(), programConfig, dataConfig,
						iterationWrapper.getOptId(),
						iterationWrapper.getEffectiveParams()));

				this.log.debug(getRun() + " (" + programConfig + ","
						+ dataConfig + ") Invoking command line: " + StringExt
								.paste(" ", iterationWrapper.getInvocation()));
				this.log.debug(getRun() + " (" + programConfig + ","
						+ dataConfig + ") Log-File is located at: \""
						+ iterationWrapper.getLogfile().getAbsolutePath()
						+ "\"");

				final BufferedWriter bw = new BufferedWriter(
						new FileWriter(iterationWrapper.getLogfile()));

				this.log = LoggerFactory.getLogger(this.getClass());
				try {
					if (iterationWrapper.getClusteringResultFile().exists()
							&& iterationWrapper.getClusteringResultFile()
									.length() > 0) {
						this.log.info(String.format(
								"%s (%s,%s, Iteration %d) Clustering file already present. Skipping execution of clustering method.",
								getRun(), programConfig, dataConfig,
								iterationWrapper.getOptId()));
					} else if (Repository.DEBUG_NO_NEW_CLUSTERINGS) {
						this.log.info(String.format(
								"%s (%s,%s, Iteration %d) Debug: No calculation of new clusterings. Skipping execution of clustering method.",
								getRun(), programConfig, dataConfig,
								iterationWrapper.getOptId()));
					} else {
						long toolStartTime = System.currentTimeMillis();
						proc = programConfig.getProgram().exec(dataConfig,
								programConfig, iterationWrapper.getInvocation(),
								iterationWrapper.getEffectiveParams(),
								iterationWrapper.getInternalParams());
						this.currentState = "Program started";

						if (proc != null) {
							if (proc instanceof RProcess) {
							} else {
								new StreamGobbler(proc.getInputStream(), bw)
										.start();
								new StreamGobbler(proc.getErrorStream(), bw)
										.start();
							}
							try {
								int maxExecTime = getRun()
										.hasMaxExecutionTime(programConfig)
												? getRun().getMaxExecutionTime(
														programConfig)
												: programConfig
														.getMaxExecutionTimeMinutes();
								long programWallTime = -1;
								if (maxExecTime == -1) {
									proc.waitFor();
									programWallTime = System.currentTimeMillis()
											- toolStartTime;
									iterationMetaData.put("walltime",
											programWallTime + "");
								} else if (!proc.waitFor(maxExecTime,
										TimeUnit.MINUTES)) {
									// still running
									this.log.info(String.format(
											"%s (%s,%s, Iteration %d) Going to terminate clustering method as it has been running longer than "
													+ maxExecTime + "mins.",
											getRun(), programConfig, dataConfig,
											iterationWrapper.getOptId()));
									wasInterrupted = true;
									proc.destroyForcibly();
									proc.waitFor();
									return;
								} else {
									programWallTime = System.currentTimeMillis()
											- toolStartTime;
									iterationMetaData.put("walltime",
											programWallTime + "");
								}
							} catch (InterruptedException e) {
								// e.printStackTrace();
								proc.destroy();
							}
						}
					}

					/*
					 * We check from time to time, whether this run got the
					 * order to terminate.
					 */
					if (getParentRunnable().checkForInterrupted())
						throw new InterruptedException();

					this.currentState = "Program finished - converting run result";
					long resultconvtime = System.currentTimeMillis();
					iterationWrapper.setConvertedClusteringRunResult(
							getParentRunnable().convertResult(
									iterationWrapper.getClusteringRunResult(),
									iterationWrapper.getEffectiveParams(),
									iterationWrapper.getInternalParams()));
					iterationMetaData.put("resultconvtime",
							"" + (System.currentTimeMillis() - resultconvtime));

					if (iterationWrapper
							.getConvertedClusteringRunResult() != null) {
						this.log.debug(getRun() + " (" + programConfig + ","
								+ dataConfig
								+ ") Finished converting result files");

						// execute runresult postprocessors
						ExecutionRun<?> run = (ExecutionRun) this.iterationWrapper
								.getRunnable().getRun();
						IFileBackedClustering tmpResult = iterationWrapper
								.getConvertedClusteringRunResult();

						this.currentState = "Clustering converted - applying postprocessors";
						for (IRunResultPostprocessor postprocessor : run
								.getPostProcessors()) {
							try {
								tmpResult.loadIntoMemory();

								final IClustering cl = postprocessor
										.postprocess(tmpResult);
								tmpResult.unloadFromMemory();

								String targetFile = tmpResult.getAbsolutePath()
										+ "." + postprocessor.getClass()
												.getSimpleName();

								TextFileParser p = new TextFileParser(
										tmpResult.getAbsolutePath(), new int[0],
										new int[0], targetFile,
										OUTPUT_MODE.STREAM) {

									@Override
									protected void processLine(String[] key,
											String[] value) {
									}

									@Override
									protected String getLineOutput(String[] key,
											String[] value) {
										if (currentLine == 0)
											return this.combineColumns(value)
													+ "\n";
										return String.format("%s%s", value[0],
												this.inSplit);
									};
								};
								p.process();

								FileWriter fw = new FileWriter(
										new File(targetFile), true);
								cl.writeFormattedString(fw);
								fw.append("\n");
								fw.close();

								tmpResult = new FileBackedClustering(
										tmpResult.getRepository(),
										System.currentTimeMillis(),
										new File(targetFile),
										tmpResult.getFormat());
							} catch (Exception e) {
								throw new RunResultConversionException(
										"An error occurred when applying postprocessor "
												+ postprocessor);
							}
						}

						iterationWrapper
								.setConvertedClusteringRunResult(tmpResult);

						/*
						 * We check from time to time, whether this run got the
						 * order to terminate.
						 */
						if (getParentRunnable().checkForInterrupted())
							throw new InterruptedException();

						this.log.info(
								String.format("%s (%s,%s, Iteration %d) %s",
										getRun(), programConfig, dataConfig,
										iterationWrapper.getOptId(),
										"Assessing quality of results..."));
						this.currentState = "Calculating clustering quality";
						long qualitytimeStart = System.currentTimeMillis();
						IClusteringQualitySet qualities = getParentRunnable()
								.assessQualities(iterationWrapper
										.getConvertedClusteringRunResult(),
										iterationWrapper.getInternalParams());
						iterationMetaData.put("qualitytime",
								"" + (System.currentTimeMillis()
										- qualitytimeStart));
						this.log.debug(
								String.format("%s (%s,%s, Iteration %d) %s",
										getRun(), programConfig, dataConfig,
										iterationWrapper.getOptId(),
										"Finished quality calculations"));
						this.log.info(
								String.format("%s (%s,%s, Iteration %d) %s",
										getRun(), programConfig, dataConfig,
										iterationWrapper.getOptId(),
										qualities.toString()));

						this.currentState = "Clustering qualities calculated - writing to file";
						synchronized (getParentRunnable()
								.getCompleteQualityOutput()) {
							// 04.04.2013: adding iteration number to
							// qualities
							List<Triple<ParameterSet, IClusteringQualitySet, Long>> qualitiesWithIterations = new ArrayList<Triple<ParameterSet, IClusteringQualitySet, Long>>();
							qualitiesWithIterations.add(Triple.getTriple(
									iterationWrapper.getParameterSet(),
									qualities,
									new Long(iterationWrapper.getOptId())));

							getParentRunnable().writeQualitiesToFile(
									qualitiesWithIterations);
						}
					}
				} catch (RunResultNotFoundException
						| RunResultConversionException | REngineException
						| REXPMismatchException e1) {
					this.getParentRunnable()
							.handleMissingRunResult(iterationWrapper);
					this.warningExceptions.add(e1);
					this.wasInterrupted = true;
				} finally {
					if (programConfig.getProgram() instanceof RProgram) {
						synchronized (bw) {
							if (((IRProgram) (programConfig.getProgram()))
									.getRengine() != null)
								bw.append(((IRProgram) (programConfig
										.getProgram())).getRengine()
												.getLastError());
							bw.close();
						}
					}
				}
			} catch (InterruptedException e) {
				// don't do anything
				// e.printStackTrace();
				this.wasInterrupted = true;
			} catch (NoRunResultFormatParserException e) {
				noRunResultException = e;
				this.exceptions.add(e);
				this.wasInterrupted = true;
			} catch (IOException e) {
				ioException = e;
				this.warningExceptions.add(e);
				this.wasInterrupted = true;
			} catch (RLibraryNotLoadedException e) {
				rLibraryException = e;
				this.exceptions.add(e);
				this.wasInterrupted = true;
			} catch (RNotAvailableException e) {
				rNotAvailableException = e;
				this.exceptions.add(e);
				this.wasInterrupted = true;
			} catch (Throwable t) {
				t.printStackTrace();
				if (t instanceof Exception)
					this.exceptions.add((Exception) t);
				this.wasInterrupted = true;
			}
		} catch (Throwable t) {
			// print and catch all throwables, otherwise we will see odd
			// behaviour in the thread pool
			t.printStackTrace();
			if (t instanceof Exception)
				this.exceptions.add((Exception) t);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IterationRunnable#afterRun()
	 */
	@Override
	protected void afterRun() {
		if (!wasInterrupted)
			iterationMetaData.put("totalwalltime",
					"" + (System.currentTimeMillis() - startTime));
		getParentRunnable().writeIterationMetaDataToFile(
				iterationWrapper.getOptId(), iterationMetaData);
		this.getParentRunnable().getResult().setMetaDataForIteration(
				this.getIterationNumber(), iterationMetaData);
		super.afterRun();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runnable.IClusteringIterationRunnable#getParentRunnable(
	 * )
	 */
	@Override
	public PARENT_TYPE getParentRunnable() {
		return (PARENT_TYPE) super.getParentRunnable();
	}
}
