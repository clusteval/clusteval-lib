/**
 * 
 */
package de.clusteval.run.runnable;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.Repository;
import de.clusteval.run.statistics.IRunStatistic;
import de.clusteval.run.statistics.IRunStatisticCalculator;
import de.clusteval.utils.IStatisticCalculator;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * @author Christian Wiwie
 *
 */
public class RunAnalysisIterationRunnable
		extends
			AnalysisIterationRunnable<IRunStatistic, IRunAnalysisIterationWrapper, IRunAnalysisRunRunnable>
		implements
			IRunAnalysisIterationRunnable {

	/**
	 * @param iterationWrapper
	 */
	public RunAnalysisIterationRunnable(
			IRunAnalysisIterationWrapper iterationWrapper) {
		super(iterationWrapper);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runnable.IRunAnalysisIterationRunnable#getRunIdentifier(
	 * )
	 */
	@Override
	public String getRunIdentifier() {
		return this.iterationWrapper.getRunIdentifier();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.AnalysisIterationRunnable#
	 * beforeStatisticCalculate ()
	 */
	protected void beforeStatisticCalculate() {
		this.log.info("Run " + this.getRun() + " - (" + this.getRunIdentifier()
				+ ") Analysing " + getStatistic().toString());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runnable.AnalysisIterationRunnable#onSkipBecausePresent(
	 * )
	 */
	@Override
	protected void onSkipBecausePresent() {
		this.log.info("Run " + this.getRun() + " - (" + this.getRunIdentifier()
				+ ") Analysing " + getStatistic().toString()
				+ ": Skipping, already present");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * run.runnable.AnalysisRunRunnable#getStatisticCalculator(java.lang.Class)
	 */
	@SuppressWarnings({"rawtypes", "unchecked"})
	@Override
	protected IStatisticCalculator<IRunStatistic> getStatisticCalculator()
			throws SecurityException, NoSuchMethodException,
			IllegalArgumentException, InstantiationException,
			IllegalAccessException, InvocationTargetException {
		Class<? extends IRunStatisticCalculator> calcClass = getRun()
				.getRepository()
				.getRunStatisticCalculator(getStatistic().getClass().getName(),
						Repository.getVersionOfObjectDynamicClass(
								getStatistic()));
		Constructor<? extends IRunStatisticCalculator> constr = calcClass
				.getConstructor(IRepository.class, long.class, File.class,
						String.class);
		IRunStatisticCalculator calc = constr.newInstance(
				this.getRun().getRepository(), calcFile.lastModified(),
				calcFile, this.getRunIdentifier());
		return calc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.AnalysisIterationRunnable#getOutputPath()
	 */
	@Override
	protected String getOutputPath() {
		return FileUtils.buildPath(this.iterationWrapper.getAnalysesFolder(),
				this.getRunIdentifier() + "_"
						+ this.getStatistic().toString(".v") + ".txt");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.AnalysisIterationRunnable#
	 * afterStatisticCalculate()
	 */
	@Override
	protected void afterStatisticCalculate() {
	}
}
