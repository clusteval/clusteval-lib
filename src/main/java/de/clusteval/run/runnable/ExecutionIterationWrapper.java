/**
 * 
 */
package de.clusteval.run.runnable;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import de.clusteval.cluster.IFileBackedClustering;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.ParameterSet;

/**
 * @author Christian Wiwie
 *
 *
 * @since Nov 6, 2017
 */
public class ExecutionIterationWrapper extends IterationWrapper
		implements
			IExecutionIterationWrapper {

	/**
	 * A temporary variable holding a file object pointing to the absolute path
	 * of the current clustering output file during execution of the runnable
	 */
	private File clusteringResultFile;

	/**
	 * A temporary variable holding a file object pointing to the absolute path
	 * of the current clustering quality output file during execution of the
	 * runnable
	 */
	private File resultQualityFile;

	/**
	 * This number indicates the current iteration performed by the runnable
	 * object.
	 * 
	 * <p>
	 * This is only larger than 1, if we are in PARAMETER_OPTIMIZATION mode.
	 * Then the optimization method will determine, how often we iterate in
	 * total and this attribute will be increased by the runnable after every
	 * iteration.
	 */
	private int optId;

	/**
	 * A map containing the parameters of {@link #runParams} and additionally
	 * internal parameters like file paths that are used throughout execution of
	 * this runnable.
	 */
	final private Map<String, String> effectiveParams;

	/**
	 * The internal parameters are parameters, that cannot be directly
	 * influenced by the user, e.g. the absolute input or output path.
	 */
	final protected Map<String, String> internalParams;

	protected IFileBackedClustering clusteringRunResult;

	protected IFileBackedClustering convertedClusteringRunResult;

	protected ParameterSet parameterSet;

	protected IProgramConfig programConfig;

	protected String[] invocation;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IExecutionIterationWrapper#getInvocation()
	 */
	@Override
	public String[] getInvocation() {
		return invocation;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runnable.IExecutionIterationWrapper#setInvocation(java.
	 * lang.String[])
	 */
	@Override
	public void setInvocation(String[] invocation) {
		this.invocation = invocation;
	}

	/**
	 * 
	 */
	public ExecutionIterationWrapper() {
		super();
		this.internalParams = new HashMap<String, String>();
		this.effectiveParams = new HashMap<String, String>();
	}

	@Override
	public File getClusteringResultFile() {
		return clusteringResultFile;
	}

	@Override
	public void setClusteringResultFile(File clusteringResultFile) {
		this.clusteringResultFile = clusteringResultFile;
	}

	@Override
	public File getResultQualityFile() {
		return resultQualityFile;
	}

	@Override
	public void setResultQualityFile(File resultQualityFile) {
		this.resultQualityFile = resultQualityFile;
	}

	@Override
	public int getOptId() {
		return optId;
	}

	@Override
	public void setOptId(int optId) {
		this.optId = optId;
	}

	@Override
	public Map<String, String> getEffectiveParams() {
		return effectiveParams;
	}

	@Override
	public Map<String, String> getInternalParams() {
		return internalParams;
	}

	@Override
	public IFileBackedClustering getClusteringRunResult() {
		return clusteringRunResult;
	}

	@Override
	public IFileBackedClustering getConvertedClusteringRunResult() {
		return convertedClusteringRunResult;
	}

	@Override
	public void setClusteringRunResult(
			IFileBackedClustering clusteringRunResult) {
		this.clusteringRunResult = clusteringRunResult;
	}

	@Override
	public IProgramConfig getProgramConfig() {
		return programConfig;
	}

	@Override
	public void setProgramConfig(IProgramConfig programConfig) {
		this.programConfig = programConfig;
	}

	@Override
	public void setConvertedClusteringRunResult(
			IFileBackedClustering clusteringRunResult) {
		this.convertedClusteringRunResult = clusteringRunResult;
	}

	@Override
	public ParameterSet getParameterSet() {
		return parameterSet;
	}

	@Override
	public void setParameterSet(ParameterSet parameterSet) {
		this.parameterSet = parameterSet;
	}
}