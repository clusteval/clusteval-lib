/**
 * 
 */
package de.clusteval.run.runnable;

import de.clusteval.utils.IStatistic;

/**
 * @author Christian Wiwie
 *
 */
public class AnalysisIterationWrapper<S extends IStatistic>
		extends
			IterationWrapper
		implements
			IAnalysisIterationWrapper<S> {

	protected S statistic;

	protected String analysesFolder;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runnable.IAnalysisIterationWrapper#getAnalysesFolder()
	 */
	@Override
	public String getAnalysesFolder() {
		return analysesFolder;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runnable.IAnalysisIterationWrapper#setAnalysesFolder(
	 * java.lang.String)
	 */
	@Override
	public void setAnalysesFolder(final String analysesFolder) {
		this.analysesFolder = analysesFolder;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IAnalysisIterationWrapper#getStatistic()
	 */
	@Override
	public S getStatistic() {
		return statistic;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IAnalysisIterationWrapper#setStatistic(S)
	 */
	@Override
	public void setStatistic(S statistic) {
		this.statistic = statistic;
	}
}
