/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run.runnable;

import java.io.File;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.Map;

import de.clusteval.cluster.paramOptimization.IDivergingParameterOptimizationMethod;
import de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod;
import de.clusteval.cluster.paramOptimization.NoParameterSetFoundException;
import de.clusteval.cluster.paramOptimization.ParameterOptimizationException;
import de.clusteval.cluster.paramOptimization.ParameterSetAlreadyEvaluatedException;
import de.clusteval.cluster.quality.ClusteringQualityMeasureValue;
import de.clusteval.cluster.quality.ClusteringQualitySet;
import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.cluster.quality.IClusteringQualitySet;
import de.clusteval.data.IDataConfig;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.threading.IRunSchedulerThread;
import de.clusteval.program.IProgramParameter;
import de.clusteval.program.ParameterSet;
import de.clusteval.program.ProgramConfig;
import de.clusteval.run.IParameterOptimizationRun;
import de.clusteval.run.ParameterOptimizationRun;
import de.clusteval.run.runresult.IParameterOptimizationResult;
import de.clusteval.run.runresult.RunResultParseException;
import de.clusteval.utils.InternalAttributeException;
import de.clusteval.utils.plot.Plotter;
import dk.sdu.imada.compbio.file.FileUtils;
import dk.sdu.imada.compbio.utils.Pair;
import dk.sdu.imada.compbio.utils.Triple;

/**
 * A type of an execution runnable, that corresponds to
 * {@link ParameterOptimizationRun}s and is therefore responsible for performing
 * several clusterings iteratively.
 * 
 * <p>
 * In {@link #doRun()} the optimization method {@link #optimizationMethod}
 * determines, how many iterations are to be performed.
 * 
 * @author Christian Wiwie
 * 
 */
public class ParameterOptimizationRunRunnable
		extends
			ExecutionRunRunnable<IParameterOptimizationRun, IParameterOptimizationResult>
		implements
			IParameterOptimizationRunRunnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2587615304261310518L;

	/**
	 * This attribute is set to some instance of an parameter optimization
	 * method, that will determine the sequence of parameter sets during the
	 * optimization process.
	 */
	protected IParameterOptimizationMethod optimizationMethod;

	/**
	 * A temporary variable holding the last consumed parameter set for the next
	 * iteration.
	 */
	protected Pair<Long, ParameterSet> lastConsumedParamSet;

	/**
	 * @param runScheduler
	 *            The run scheduler that the newly created runnable should be
	 *            passed to and executed by.
	 * @param run
	 *            The run this runnable belongs to.
	 * @param runIdentString
	 *            The unique identification string of the run which is used to
	 *            store the results in a unique folder to avoid overwriting.
	 * @param programConfig
	 *            The program configuration encapsulating the program executed
	 *            by this runnable.
	 * @param dataConfig
	 *            The data configuration used by this runnable.
	 * @param optimizationMethod
	 *            The optimization method which determines the parameter sets
	 *            during the optimization process and stores the results.
	 * @param isResume
	 *            True, if this run is a resumption of a previous execution or a
	 *            completely new execution.
	 */
	public ParameterOptimizationRunRunnable(IRunSchedulerThread runScheduler,
			IParameterOptimizationRun run, ProgramConfig programConfig,
			IDataConfig dataConfig,
			IParameterOptimizationMethod optimizationMethod,
			String runIdentString, boolean isResume,
			Map<IProgramParameter<?>, String> runParams) {
		super(run, programConfig, dataConfig, runIdentString, isResume,
				runParams);

		this.optimizationMethod = optimizationMethod;
		if (optimizationMethod != null) {
			this.optimizationMethod.setResume(isResume);
		}
		this.future = runScheduler.registerRunRunnable(this);
	}

	/**
	 * This method replaces the optimization parameters with the values given in
	 * the run configuration.
	 */
	protected String[] parseOptimizationParameters(String[] invocation,
			final Map<String, String> effectiveParams) {
		final String[] parsed = invocation.clone();
		try {
			// 15.04.2013: changed invocation of next() to beginning of
			// doRunIteration() in order to get the right iteration numbner
			// now here: get the parameter set created there
			// TODO: change this to iterationWrapper
			List<ParameterSet> paramSets = optimizationMethod.getResult()
					.getParameterSetsList();
			ParameterSet optimizationParamValues = paramSets
					.get(paramSets.size() - 1);
			for (int i = 0; i < parsed.length; i++) {
				for (String param : optimizationParamValues.keySet()) {
					parsed[i] = parsed[i].replace("%" + param + "%",
							optimizationParamValues.get(param) + "");
					effectiveParams.put(param,
							optimizationParamValues.get(param) + "");
				}
			}
		} catch (IllegalStateException e) {
			e.printStackTrace();
		}
		return parsed;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * run.runnable.ExecutionRunRunnable#replaceRemainingParameters(java.util
	 * .Map, java.lang.String, java.util.Map)
	 */
	@Override
	protected String[] replaceRunParameters(String[] invocation,
			final Map<String, String> effectiveParams)
			throws InternalAttributeException, RegisterException,
			NoParameterSetFoundException {
		invocation = this.parseOptimizationParameters(invocation,
				effectiveParams);
		return super.replaceRunParameters(invocation, effectiveParams);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IParameterOptimizationRunRunnable#
	 * getOptimizationMethod()
	 */
	@Override
	public IParameterOptimizationMethod getOptimizationMethod() {
		return this.optimizationMethod;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.runnable.ExecutionRunRunnable#beforeRun()
	 */
	@Override
	protected void beforeRun() throws RunRunnableBeforeExecutionException {
		super.beforeRun();
		try {
			if (!new File(completeQualityOutput).exists())
				writeHeaderIntoCompleteFile(completeQualityOutput);

			if (!new File(completeMetaDataFile).exists())
				writeHeaderIntoCompleteMetaDataFile(completeMetaDataFile);

			/*
			 * Pass converted data configuration to optimization method
			 */
			this.optimizationMethod.setDataConfig(this.dataConfig);
			this.optimizationMethod.setProgramConfig(this.programConfig);
			try {
				this.optimizationMethod.reset(new File(completeQualityOutput));
				if (isResume) {
					// in case of resume, we have to update the current
					// percentage
					int iterationPercent = Math.min(
							(int) (this.optimizationMethod.getFinishedCount()
									/ (double) this.optimizationMethod
											.getTotalIterationCount()
									* 10000),
							10000);
					this.progress.update(iterationPercent);
					this.progressBeforeExecution.update(iterationPercent);
				}
			} catch (ParameterOptimizationException e) {
				e.printStackTrace();
			} catch (RunResultParseException e) {
				e.printStackTrace();
			} catch (ConcurrentModificationException e) {
			}

			// this.optId = this.optimizationMethod.getCurrentCount();
		} catch (Throwable t) {
			throw new RunRunnableBeforeExecutionException(t);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.runnable.ExecutionRunRunnable#endRun()
	 */
	@Override
	protected void afterRun() throws InterruptedException, RegisterException {
		super.afterRun();

		if (this.optimizationMethod != null
				&& this.optimizationMethod.getResult() != null) {
			try {
				if (this.optimizationMethod.getResult()
						.getOptimalParameterSet() != null)
					this.log.info("Optimal Parameter set for " + programConfig
							+ " & " + dataConfig + ":\t"
							+ this.optimizationMethod.getResult()
									.getOptimalParameterSet()
							+ "" + this.optimizationMethod.getResult()
									.getOptimalCriterionValue());
				/*
				 * TODO: option, whether to plot
				 */
				Plotter.plotParameterOptimizationResult(
						this.optimizationMethod.getResult());
			} finally {
				// clear memory-hungry internal attributes of clustering results
				IParameterOptimizationResult result = this.optimizationMethod
						.getResult();
				result.unloadFromMemory();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.RunRunnable#hasNextIteration()
	 */
	@Override
	protected boolean hasNextIteration() {
		return this.optimizationMethod.hasNext();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.RunRunnable#consumeNextIteration()
	 */
	@Override
	protected int consumeNextIteration() throws RunIterationException {
		try {
			this.lastConsumedParamSet = this.optimizationMethod.next();
		} catch (ParameterSetAlreadyEvaluatedException e) {
			this.log.debug(run.toString() + " (" + programConfig + ","
					+ dataConfig + ") " + "Skipping calculation of iteration "
					+ e.getParameterSet() + " (has already been assessed)");

			// if this parameter set has already been evaluated, write into
			// the complete file
			StringBuilder sb = new StringBuilder();
			sb.append(e.getIterationNumber());
			sb.append("*\t");
			sb.append(e.getPreviousIterationNumber().iterator().next());
			sb.append(System.getProperty("line.separator"));

			FileUtils.appendStringToFile(completeQualityOutput, sb.toString());
			throw new RunIterationException(new SkipIterationException(e));
		} catch (Exception e) {
			throw new RunIterationException(e);
		}
		return this.optimizationMethod.getStartedCount();
	}

	@Override
	protected void doRun() throws RunIterationException {
		try {
			super.doRun();
		} catch (RunIterationException e) {
			if (e.getCause() instanceof NoParameterSetFoundException) {
				// this exception just indicates, that no parameter set has been
				// found and the parameter optimization terminated earlier than
				// expected.
				this.log.warn(e.getCause().getMessage());
				this.warningExceptions.add(e.getCause());
			} else
				throw e;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runnable.ExecutionRunRunnable#decorateIterationWrapper
	 * (de.clusteval.run.runnable.ExecutionIterationWrapper, int)
	 */
	@Override
	protected void decorateIterationWrapper(
			IExecutionIterationWrapper iterationWrapper, int currentPos)
			throws RunIterationException {
		iterationWrapper.setParameterSet(lastConsumedParamSet.getSecond());
		iterationWrapper.setOptId(this.optimizationMethod.getStartedCount());

		super.decorateIterationWrapper(iterationWrapper, currentPos);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.runnable.ExecutionRunRunnable#doRunIteration()
	 */
	@Override
	protected void doRunIteration(IExecutionIterationWrapper iterationWrapper)
			throws RunIterationException {
		try {
			super.doRunIteration(iterationWrapper);
		} finally {
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.runnable.ExecutionRunRunnable#handleMissingRunResult()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IParameterOptimizationRunRunnable#
	 * handleMissingRunResult(de.clusteval.run.runnable.
	 * IExecutionIterationWrapper)
	 */
	@Override
	public void handleMissingRunResult(
			final IExecutionIterationWrapper iterationWrapper) {
		if (this.optimizationMethod instanceof IDivergingParameterOptimizationMethod) {
			this.log.info(this.getRun() + " (" + this.programConfig + ","
					+ this.dataConfig + ", Iteration "
					+ iterationWrapper.getOptId()
					+ ") The result of this run could not be found. Probably the program did not converge with this parameter set.");
		} else {
			this.log.info(this.getRun() + " (" + this.programConfig + ","
					+ this.dataConfig + ", Iteration "
					+ iterationWrapper.getOptId()
					+ ") The result of this run could not be found. Please consult the log files of the program");
		}

		super.handleMissingRunResult(iterationWrapper);

		ClusteringQualitySet minimalQualities = new ClusteringQualitySet();
		for (IClusteringQualityMeasure measure : this.getRun()
				.getQualityMeasures())
			minimalQualities.put(measure,
					ClusteringQualityMeasureValue.NOT_TERMINATED);

		if (this.optimizationMethod instanceof IDivergingParameterOptimizationMethod) {
			((IDivergingParameterOptimizationMethod) this.optimizationMethod)
					.giveFeedbackNotTerminated(iterationWrapper.getOptId(),
							minimalQualities);
		} else {
			this.optimizationMethod.giveQualityFeedback(
					iterationWrapper.getOptId(), minimalQualities);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * run.runnable.ExecutionRunRunnable#afterQualityAssessment(java.util.List)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IParameterOptimizationRunRunnable#
	 * writeQualitiesToFile(java.util.List)
	 */
	@Override
	// 04.04.2013: adding iteration number into complete file
	public void writeQualitiesToFile(
			List<Triple<ParameterSet, IClusteringQualitySet, Long>> qualities) {
		// in this case, the list contains only one element

		// the parameter set contains all optimizable parameters of the program,
		// not only those which actually have been optimized and are stored in
		// the optimization method. We therefore have to adapt the parameter set
		// accordingly.
		ParameterSet paramSet = new ParameterSet();
		for (IProgramParameter<?> param : optimizationMethod
				.getOptimizationParameter())
			paramSet.put(param.getName(),
					qualities.get(0).getFirst().get(param.getName()));
		this.optimizationMethod.giveQualityFeedback(qualities.get(0).getThird(),
				qualities.get(0).getSecond());
		super.writeQualitiesToFile(qualities);

		synchronized (this) {
			// changed 25.01.2013
			int iterationPercent = Math.min((int) (this.optimizationMethod
					.getFinishedCount()
					/ (double) this.optimizationMethod.getTotalIterationCount()
					* 10000), 10000);
			this.progress.update(iterationPercent);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IRunRunnable#getRunResult()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runnable.IParameterOptimizationRunRunnable#getRunResult(
	 * )
	 */
	@Override
	public IParameterOptimizationResult getResult() {
		if (optimizationMethod != null)
			return optimizationMethod.getResult();
		return null;
	}
}
