/**
 * 
 */
package de.clusteval.run.runnable;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.Repository;
import de.clusteval.run.statistics.IRunDataStatistic;
import de.clusteval.run.statistics.IRunDataStatisticCalculator;
import de.clusteval.utils.IStatisticCalculator;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * @author Christian Wiwie
 *
 */
public class RunDataAnalysisIterationRunnable
		extends
			AnalysisIterationRunnable<IRunDataStatistic, IRunDataAnalysisIterationWrapper, IRunDataAnalysisRunRunnable>
		implements
			IRunDataAnalysisIterationRunnable {

	/**
	 * @param iterationWrapper
	 */
	public RunDataAnalysisIterationRunnable(
			IRunDataAnalysisIterationWrapper iterationWrapper) {
		super(iterationWrapper);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IRunDataAnalysisIterationRunnable#
	 * getRunIdentifier()
	 */
	@Override
	public String getRunIdentifier() {
		return this.iterationWrapper.getRunIdentifier();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IRunDataAnalysisIterationRunnable#
	 * getDataAnalysisIdentifier()
	 */
	@Override
	public String getDataAnalysisIdentifier() {
		return this.iterationWrapper.getUniqueDataAnalysisRunIdentifier();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.AnalysisIterationRunnable#
	 * beforeStatisticCalculate ()
	 */
	protected void beforeStatisticCalculate() {
		this.log.info("Run " + this.getRun() + " - (" + this.getRunIdentifier()
				+ "," + this.getDataAnalysisIdentifier() + ") Analysing "
				+ this.getStatistic().toString());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runnable.AnalysisIterationRunnable#onSkipBecausePresent(
	 * )
	 */
	@Override
	protected void onSkipBecausePresent() {
		this.log.info("Run " + this.getRun() + " - (" + this.getRunIdentifier()
				+ "," + this.getDataAnalysisIdentifier() + ") Analysing "
				+ this.getStatistic().toString()
				+ ": Skipping, already present");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * run.runnable.AnalysisRunRunnable#getStatisticCalculator(java.lang.Class)
	 */
	@SuppressWarnings({"rawtypes", "unchecked"})
	@Override
	protected IStatisticCalculator<IRunDataStatistic> getStatisticCalculator()
			throws SecurityException, NoSuchMethodException,
			IllegalArgumentException, InstantiationException,
			IllegalAccessException, InvocationTargetException {
		Class<? extends IRunDataStatisticCalculator> calcClass = getRun()
				.getRepository().getRunDataStatisticCalculator(
						this.getStatistic().getClass().getName(),
						Repository.getVersionOfObjectDynamicClass(
								getStatistic()));
		Constructor<? extends IRunDataStatisticCalculator> constr = calcClass
				.getConstructor(IRepository.class, long.class, File.class,
						List.class, List.class);
		IRunDataStatisticCalculator calc = constr.newInstance(
				this.getRun().getRepository(), calcFile.lastModified(),
				calcFile, this.iterationWrapper.getRunIdentifier(),
				this.iterationWrapper.getUniqueDataAnalysisRunIdentifier());
		return calc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.AnalysisIterationRunnable#getOutputPath()
	 */
	@Override
	protected String getOutputPath() {
		return FileUtils.buildPath(this.iterationWrapper.getAnalysesFolder(),
				this.getStatistic().toString(".v") + ".txt");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.AnalysisIterationRunnable#
	 * afterStatisticCalculate()
	 */
	@Override
	protected void afterStatisticCalculate() {
	}
}
