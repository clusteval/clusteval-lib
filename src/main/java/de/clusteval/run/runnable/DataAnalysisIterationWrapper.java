/**
 * 
 */
package de.clusteval.run.runnable;

import de.clusteval.data.IDataConfig;
import de.clusteval.data.statistics.IDataStatistic;

/**
 * @author Christian Wiwie
 *
 */
public class DataAnalysisIterationWrapper
		extends
			AnalysisIterationWrapper<IDataStatistic>
		implements
			IDataAnalysisIterationWrapper {

	protected IDataConfig dataConfig;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runnable.IDataAnalysisIterationWrapper#getDataConfig()
	 */
	@Override
	public IDataConfig getDataConfig() {
		return this.dataConfig;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runnable.IDataAnalysisIterationWrapper#setDataConfig(de.
	 * clusteval.data.IDataConfig)
	 */
	@Override
	public void setDataConfig(final IDataConfig dataConfig) {
		this.dataConfig = dataConfig;
	}
}
