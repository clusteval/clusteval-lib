/**
 * 
 */
package de.clusteval.run.runnable;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import de.clusteval.data.IDataConfig;
import de.clusteval.data.statistics.IDataStatistic;
import de.clusteval.data.statistics.IDataStatisticCalculator;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.Repository;
import de.clusteval.utils.IStatisticCalculator;
import dk.sdu.imada.compbio.file.FileUtils;
import dk.sdu.imada.compbio.utils.ProgressPrinter;

/**
 * @author Christian Wiwie
 *
 */
public class DataAnalysisIterationRunnable
		extends
			AnalysisIterationRunnable<IDataStatistic, IDataAnalysisIterationWrapper, IDataAnalysisRunRunnable>
		implements
			IDataAnalysisIterationRunnable {

	// TODO: move upwards in hierarchy?
	protected ProgressPrinter progress;

	/**
	 * @param iterationWrapper
	 */
	public DataAnalysisIterationRunnable(
			IDataAnalysisIterationWrapper iterationWrapper) {
		super(iterationWrapper);
		progress = new ProgressPrinter(1, false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runnable.IDataAnalysisIterationRunnable#getDataConfig()
	 */
	@Override
	public IDataConfig getDataConfig() {
		return this.iterationWrapper.getDataConfig();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IDataAnalysisIterationRunnable#
	 * getProgressPrinter()
	 */
	@Override
	public ProgressPrinter getProgressPrinter() {
		return progress;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.AnalysisIterationRunnable#
	 * beforeStatisticCalculate ()
	 */
	protected void beforeStatisticCalculate() {
		this.log.info("Run " + this.getRun() + " - (" + this.getDataConfig()
				+ ") Analysing " + getStatistic().toString());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.AnalysisIterationRunnable#
	 * afterStatisticCalculate()
	 */
	@Override
	protected void afterStatisticCalculate() {
		this.progress.update(1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runnable.AnalysisIterationRunnable#onSkipBecausePresent(
	 * )
	 */
	@Override
	protected void onSkipBecausePresent() {
		this.log.info("Run " + this.getRun() + " - (" + this.getDataConfig()
				+ ") Analysing " + getStatistic().toString()
				+ ": Skipping, already present");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * run.runnable.AnalysisRunRunnable#getStatisticCalculator(java.lang.Class)
	 */
	@SuppressWarnings({"rawtypes", "unchecked"})
	@Override
	protected IStatisticCalculator<IDataStatistic> getStatisticCalculator()
			throws SecurityException, NoSuchMethodException,
			IllegalArgumentException, InstantiationException,
			IllegalAccessException, InvocationTargetException {

		Class<? extends IDataStatisticCalculator> calcClass = getRun()
				.getRepository()
				.getDataStatisticCalculator(getStatistic().getClass().getName(),
						Repository.getVersionOfObjectDynamicClass(
								getStatistic()));
		Constructor<? extends IDataStatisticCalculator> constr = calcClass
				.getConstructor(IRepository.class, long.class, File.class,
						IDataConfig.class);
		IDataStatisticCalculator calc = constr.newInstance(
				this.getRun().getRepository(), calcFile.lastModified(),
				calcFile, this.getDataConfig());
		return calc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.AnalysisIterationRunnable#getOutputPath()
	 */
	@Override
	protected String getOutputPath() {
		return FileUtils.buildPath(this.iterationWrapper.getAnalysesFolder(),
				this.getDataConfig().toString(".v") + "_"
						+ this.getStatistic().toString(".v") + ".txt");
	}
}
