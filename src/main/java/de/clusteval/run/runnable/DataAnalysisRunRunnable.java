/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run.runnable;

import java.io.File;
import java.util.List;

import de.clusteval.data.DataConfig;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.IDataSet;
import de.clusteval.data.dataset.format.DataSetConversionException;
import de.clusteval.data.statistics.IDataStatistic;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.threading.IRunSchedulerThread;
import de.clusteval.run.DataAnalysisRun;
import de.clusteval.run.IDataAnalysisRun;
import de.clusteval.run.runresult.DataAnalysisRunResult;
import de.clusteval.run.runresult.IDataAnalysisRunResult;
import dk.sdu.imada.compbio.utils.ProgressPrinter;

/**
 * A type of analysis runnable, that corresponds to {@link DataAnalysisRun} and
 * is responsible for analysing a data configuration (dataset and goldstandard).
 * 
 * @author Christian Wiwie
 * 
 */
public class DataAnalysisRunRunnable
		extends
			AnalysisRunRunnable<IDataAnalysisRun, IDataStatistic, IDataAnalysisRunResult, IDataAnalysisIterationWrapper, IDataAnalysisIterationRunnable>
		implements
			IDataAnalysisRunRunnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1852197416415004776L;

	/**
	 * The data configuration to be analysed by this runnable.
	 */
	protected IDataConfig dataConfig;

	protected int currentIteration = -1;

	/**
	 * @param runScheduler
	 *            The run scheduler that the newly created runnable should be
	 *            passed to and executed by.
	 * 
	 * @param run
	 *            The run this runnable belongs to.
	 * @param runIdentString
	 *            The unique identification string of the run which is used to
	 *            store the results in a unique folder to avoid overwriting.
	 * @param dataConfig
	 *            The data configuration to be analysed by this runnable.
	 * @param statistics
	 *            The statistics that should be assessed during execution of
	 *            this runnable.
	 * @param isResume
	 *            True, if this run is a resumption of a previous execution or a
	 *            completely new execution.
	 */
	public DataAnalysisRunRunnable(IRunSchedulerThread runScheduler,
			IDataAnalysisRun run, String runIdentString, final boolean isResume,
			IDataConfig dataConfig, List<IDataStatistic> statistics) {
		super(run, runIdentString, statistics, isResume);
		this.dataConfig = dataConfig;
		this.future = runScheduler.registerRunRunnable(this);
		this.progress = new ProgressPrinter(this.statistics.size() * 10000,
				false);
		this.progressBeforeExecution = new ProgressPrinter(
				this.statistics.size() * 10000, false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.runnable.AnalysisRunRunnable#createRunResult()
	 */
	@Override
	protected IDataAnalysisRunResult createRunResult()
			throws RegisterException {
		return new DataAnalysisRunResult(this.getRun().getRepository(),
				System.currentTimeMillis(), new File(analysesFolder),
				this.runThreadIdentString, run);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.runnable.AnalysisRunRunnable#afterRun()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IDataAnalysisRunRunnable#afterRun()
	 */
	@Override
	public void afterRun() throws InterruptedException, RegisterException {
		super.afterRun();
		result.put(this.dataConfig, results);
		this.getRun().getResults().add(result);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.AnalysisRunRunnable#beforeRun()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IDataAnalysisRunRunnable#beforeRun()
	 */
	@Override
	public void beforeRun() throws RunRunnableBeforeExecutionException {
		super.beforeRun();

		IDataSet current = this.dataConfig.getDatasetConfig().getDataSet();

		IDataSet ds;
		try {
			ds = current.preprocessAndConvertTo(this.run.getContext(),
					this.run.getContext().getStandardInputFormat(),
					this.dataConfig.getDatasetConfig()
							.getConversionInputToStandardConfiguration(),
					this.dataConfig.getDatasetConfig()
							.getConversionStandardToInputConfiguration());

			this.dataConfig = new DataConfig((DataConfig) this.dataConfig);
			this.dataConfig.getDatasetConfig().setDataSet(ds);
		} catch (Exception e) {
			throw new RunRunnableBeforeExecutionException(
					new DataSetConversionException(
							"The given data configuration could not be converted: "
									+ e.getMessage()));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runnable.AnalysisRunRunnable#createIterationRunnable
	 * (de.clusteval.run.runnable.AnalysisIterationWrapper)
	 */
	@Override
	protected DataAnalysisIterationRunnable createIterationRunnable(
			IDataAnalysisIterationWrapper iterationWrapper) {
		return new DataAnalysisIterationRunnable(iterationWrapper);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runnable.AnalysisRunRunnable#createIterationWrapper()
	 */
	@Override
	protected DataAnalysisIterationWrapper createIterationWrapper() {
		return new DataAnalysisIterationWrapper();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runnable.AnalysisRunRunnable#decorateIterationWrapper
	 * (de.clusteval.run.runnable.AnalysisIterationWrapper, int)
	 */
	@Override
	protected void decorateIterationWrapper(
			IDataAnalysisIterationWrapper iterationWrapper, int currentPos)
			throws RunIterationException {
		super.decorateIterationWrapper(iterationWrapper, currentPos);
		iterationWrapper.setDataConfig(dataConfig);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.RunRunnable#hasNextIteration()
	 */
	@Override
	protected boolean hasNextIteration() {
		return currentIteration < this.statistics.size() - 1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.RunRunnable#consumeNextIteration()
	 */
	@Override
	protected int consumeNextIteration() throws RunIterationException {
		return ++currentIteration;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runnable.RunRunnable#doRunIteration(de.clusteval.run
	 * .runnable.IterationWrapper)
	 */
	@Override
	protected void doRunIteration(
			IDataAnalysisIterationWrapper iterationWrapper)
			throws RunIterationException {
		DataAnalysisIterationRunnable iterationRunnable = this
				.createIterationRunnable(iterationWrapper);
		this.progress.addSubProgress(iterationRunnable.getProgressPrinter(),
				10000);

		this.submitIterationRunnable(iterationRunnable);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IDataAnalysisRunRunnable#getDataConfig()
	 */
	@Override
	public IDataConfig getDataConfig() {
		return this.dataConfig;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IRunRunnable#getRunResult()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IDataAnalysisRunRunnable#getRunResult()
	 */
	@Override
	public IDataAnalysisRunResult getResult() {
		// TODO
		return null;
	}
}
