/**
 * 
 */
package de.clusteval.run.runnable;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.rosuda.REngine.REngineException;

import de.clusteval.data.dataset.format.InvalidDataSetFormatVersionException;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.statistics.StatisticCalculateException;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.run.statistics.IRunStatistic;
import de.clusteval.utils.FormatConversionException;
import de.clusteval.utils.IStatistic;
import de.clusteval.utils.IStatisticCalculator;
import de.clusteval.utils.RNotAvailableException;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * @author Christian Wiwie
 * 
 */
public abstract class AnalysisIterationRunnable<S extends IStatistic, IW extends IAnalysisIterationWrapper<S>, PARENT_TYPE extends IAnalysisRunRunnable>
		extends
			IterationRunnable<IW, PARENT_TYPE>
		implements
			IAnalysisIterationRunnable<S, IW, PARENT_TYPE> {

	/**
	 * A temporary variable needed during execution of this runnable.
	 */
	protected File calcFile;

	protected S result;

	/**
	 * @param iterationWrapper
	 */
	public AnalysisIterationRunnable(final IW iterationWrapper) {
		super(iterationWrapper);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IAnalysisIterationRunnable#getStatistic()
	 */
	@Override
	public S getStatistic() {
		return this.iterationWrapper.getStatistic();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IAnalysisIterationRunnable#setStatistic(S)
	 */
	@Override
	public void setStatistic(final S statistic) {
		this.iterationWrapper.setStatistic(statistic);
	}

	/**
	 * A helper method of {@link #doRun()}, which can be overridden to do any
	 * kind of precalculations and operations needed before a statistic is
	 * assessed.
	 */
	protected abstract void beforeStatisticCalculate();

	/**
	 * A helper method for {@link #doRun()} which returns the statistic
	 * calculator for the current statistic to be calculated.
	 * 
	 * <p>
	 * This method is abstract since it has to provide different behaviour for
	 * different subtypes of this class.
	 * 
	 * @return
	 * @throws FormatConversionException
	 * @throws IOException
	 * @throws InvalidDataSetFormatVersionException
	 * @throws RegisterException
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 * @throws IllegalArgumentException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws UnknownDataSetFormatException
	 */
	protected abstract IStatisticCalculator<S> getStatisticCalculator()
			throws FormatConversionException, IOException,
			InvalidDataSetFormatVersionException, RegisterException,
			SecurityException, NoSuchMethodException, IllegalArgumentException,
			InstantiationException, IllegalAccessException,
			InvocationTargetException, UnknownDataSetFormatException;

	/**
	 * A helper method for {@link #doRun()}. It returns the absolute path to the
	 * result file for the current statistic to be calculated.
	 * 
	 * <p>
	 * This method is abstract since it has to provide different behaviour for
	 * different subtypes of this class.
	 * 
	 * @return Abstract path to the output file.
	 */
	protected abstract String getOutputPath();

	protected abstract void onSkipBecausePresent();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IAnalysisIterationRunnable#doRun()
	 */
	@Override
	public void doRun() throws InterruptedException {
		try {
			IStatistic statistic = iterationWrapper.getStatistic();
			String output = getOutputPath();

			final File outputFile = new File(output);
			if (this.iterationWrapper.isResume() && outputFile.exists()
					&& outputFile.length() > 0) {
				onSkipBecausePresent();
				return;
			}

			BufferedWriter bw;
			bw = new BufferedWriter(new FileWriter(outputFile));

			this.calcFile = new File(FileUtils.buildPath(
					this.getRun().getRepository()
							.getBasePath(IRunStatistic.class),
					statistic.toString("-") + ".jar")).getAbsoluteFile();

			IStatisticCalculator<S> calc = getStatisticCalculator();

			this.beforeStatisticCalculate();
			result = calc.calculate();
			bw.write(result.getResultAsString());
			bw.close();

			calc.writeOutputTo(new File(output));
		} catch (IOException e) {
			e.printStackTrace();
			this.exceptions.add(e);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (StatisticCalculateException e) {
			e.printStackTrace();
			this.exceptions.add(e);
		} catch (RNotAvailableException e) {
			e.printStackTrace();
			this.exceptions.add(e);
		} catch (REngineException e) {
			e.printStackTrace();
			this.exceptions.add(e);
		} catch (InvalidDataSetFormatVersionException e) {
			e.printStackTrace();
			this.exceptions.add(e);
		} catch (UnknownDataSetFormatException e) {
			e.printStackTrace();
			this.exceptions.add(e);
		} catch (RegisterException e) {
			e.printStackTrace();
		} catch (FormatConversionException e) {
			e.printStackTrace();
			this.exceptions.add(e);
		} finally {
			this.afterStatisticCalculate();
			// TODO
			// currentPos++;
			// int iterationPercent = Math.min((int) (currentPos
			// / (double) this.statistics.size() * 100), 100);
			// this.progress.update(iterationPercent);
		}
	}

	/**
	 * 
	 */
	protected abstract void afterStatisticCalculate();
}
