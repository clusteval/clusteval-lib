/**
 * 
 */
package de.clusteval.run.runnable;

import java.util.Map;

import de.clusteval.data.IDataConfig;
import de.clusteval.framework.threading.IRunSchedulerThread;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.run.IRobustnessAnalysisRun;
import de.clusteval.run.runresult.IClusteringRunResult;

/**
 * @author Christian Wiwie
 *
 */
public class RobustnessAnalysisRunRunnable
		extends
			ClusteringRunRunnable<IRobustnessAnalysisRun, IClusteringRunResult>
		implements
			IRobustnessAnalysisRunRunnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5353111184776797149L;

	/**
	 * @param runScheduler
	 * @param run
	 * @param programConfig
	 * @param dataConfig
	 * @param runIdentString
	 * @param isResume
	 */
	public RobustnessAnalysisRunRunnable(IRunSchedulerThread runScheduler,
			IRobustnessAnalysisRun run, IProgramConfig programConfig,
			IDataConfig dataConfig, String runIdentString, boolean isResume,
			Map<IProgramParameter<?>, String> runParams) {
		super(runScheduler, run, programConfig, dataConfig, runIdentString,
				isResume, runParams);
	}
}
