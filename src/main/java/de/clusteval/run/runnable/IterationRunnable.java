/**
 * 
 */
package de.clusteval.run.runnable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.clusteval.framework.RLibraryNotLoadedException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RunResultRepository;
import de.clusteval.framework.threading.IRunSchedulerThread;
import de.clusteval.run.IRun;
import de.clusteval.utils.RNotAvailableException;

/**
 * @author Christian Wiwie
 * 
 */
public abstract class IterationRunnable<IW extends IIterationWrapper, PARENT_TYPE extends IRunRunnable>
		implements
			IIterationRunnable<IW, PARENT_TYPE> {

	protected Logger log;
	protected IW iterationWrapper;
	// TODO think of nicer design
	protected IOException ioException;
	protected RLibraryNotLoadedException rLibraryException;
	protected RNotAvailableException rNotAvailableException;
	protected InterruptedException interruptedException;
	protected List<Exception> exceptions, warningExceptions;

	protected long startTime, finishTime;
	protected boolean wasInterrupted;

	/**
	 * Holds the current state of this runnable.
	 */
	protected String currentState;

	protected boolean isRemote;

	public IterationRunnable(final IW iterationWrapper) {
		super();
		this.iterationWrapper = iterationWrapper;
		this.log = LoggerFactory.getLogger(getClass());
		this.exceptions = new ArrayList<Exception>();
		this.warningExceptions = new ArrayList<Exception>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runnable.IIterationRunnable#getrNotAvailableException()
	 */
	@Override
	public RNotAvailableException getrNotAvailableException() {
		return rNotAvailableException;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IIterationRunnable#getIoException()
	 */
	@Override
	public IOException getIoException() {
		return ioException;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IIterationRunnable#getrLibraryException()
	 */
	@Override
	public RLibraryNotLoadedException getrLibraryException() {
		return rLibraryException;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runnable.IIterationRunnable#getInterruptedException()
	 */
	@Override
	public InterruptedException getInterruptedException() {
		return interruptedException;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IIterationRunnable#getExceptions()
	 */
	@Override
	public List<Exception> getExceptions() {
		return exceptions;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IIterationRunnable#getWarningExceptions()
	 */
	@Override
	public List<Exception> getWarningExceptions() {
		return warningExceptions;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IIterationRunnable#getParentRunnable()
	 */
	@Override
	public PARENT_TYPE getParentRunnable() {
		return (PARENT_TYPE) this.iterationWrapper.getRunnable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IIterationRunnable#getRun()
	 */
	@Override
	public IRun getRun() {
		return this.iterationWrapper.getRunnable().getRun();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IIterationRunnable#run()
	 */
	@Override
	public final void run() {
		try {
			beforeRun();
			try {
				doRun();
			} catch (InterruptedException e) {
			} catch (Exception e) {
				this.exceptions.add(e);
			} finally {
				afterRun();
			}
		} catch (Exception e) {
			this.exceptions.add(e);
		}
	}

	protected void beforeRun() {
		this.startTime = System.currentTimeMillis();

		IRepository repo = getRun().getRepository();
		if (repo instanceof RunResultRepository)
			repo = repo.getParent();
		IRunSchedulerThread scheduler = repo.getSupervisorThread()
				.getRunScheduler();
		scheduler.informOnStartedIterationRunnable(Thread.currentThread(),
				this);
	}

	protected abstract void doRun() throws InterruptedException;

	protected void afterRun() {
		finishTime = System.currentTimeMillis();
		this.iterationWrapper.getRunnable()
				.reportIterationRunnableRunningTime(this.getRunningTime());
		IRepository repo = getRun().getRepository();
		if (repo instanceof RunResultRepository)
			repo = repo.getParent();
		IRunSchedulerThread scheduler = repo.getSupervisorThread()
				.getRunScheduler();
		scheduler.informOnFinishedIterationRunnable(Thread.currentThread(),
				this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IIterationRunnable#getStartTime()
	 */
	@Override
	public long getStartTime() {
		return this.startTime;
	}

	@Override
	public String getCurrentState() {
		return this.currentState;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IIterationRunnable#getRunningTime()
	 */
	@Override
	public long getRunningTime() {
		return this.finishTime - this.startTime;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IIterationRunnable#setRemote(boolean)
	 */
	@Override
	public void setRemote(boolean isRemote) {
		this.isRemote = isRemote;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runnable.IIterationRunnable#isRemote()
	 */
	@Override
	public boolean isRemote() {
		return isRemote;
	}
}
