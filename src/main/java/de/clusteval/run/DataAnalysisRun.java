/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration.HierarchicalINIConfiguration;

import de.clusteval.context.IContext;
import de.clusteval.data.DataConfig;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.statistics.IDataStatistic;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryObjectDumpException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.threading.IRunSchedulerThread;
import de.clusteval.run.runnable.DataAnalysisRunRunnable;
import de.clusteval.run.runnable.IDataAnalysisRunRunnable;
import dk.sdu.imada.compbio.file.FileUtils;
import dk.sdu.imada.compbio.utils.StringExt;

/**
 * A type of analysis run that conducts analyses of data inputs encapsulated by
 * data configurations.
 * 
 * <p>
 * A data analysis run has a list of data configurations in {@link #dataConfigs}
 * , that should be assessed during execution of the run. Additionally they
 * inherit a list of data statistics in {@link AnalysisRun#statistics} that
 * should be assessed for every data configuration.
 * 
 * @author Christian Wiwie
 * 
 */
public class DataAnalysisRun
		extends
			AnalysisRun<IDataStatistic, IDataAnalysisRunRunnable>
		implements
			IDataAnalysisRun {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6277972470544027906L;
	/**
	 * A list of data configurations, that should be assessed during execution
	 * of the run.
	 */
	protected List<IDataConfig> dataConfigs;

	/**
	 * @param repository
	 *            The repository this run should be registered at.
	 * @param context
	 * @param changeDate
	 *            The date this run was performed.
	 * @param absPath
	 *            The absolute path to the file on the filesystem that
	 *            corresponds to this run.
	 * @param dataConfigs
	 *            The list of data configurations, that should be assessed
	 *            during execution of the run.
	 * @param statistics
	 *            The statistics that should be assessed for the objects of
	 *            analysis.
	 */
	public DataAnalysisRun(IRepository repository, final IContext context,
			long changeDate, File absPath, List<IDataConfig> dataConfigs,
			List<IDataStatistic> statistics) {
		super(repository, context, changeDate, absPath, statistics);
		this.dataConfigs = dataConfigs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#register()
	 */
	@Override
	public boolean register() throws RegisterException {
		if (super.register()) {
			// register this Run at all dataconfigs
			for (IDataConfig dataConfig : this.dataConfigs) {
				dataConfig.addListener(this);
			}

			for (IDataStatistic statistic : this.statistics) {
				// added 21.03.2013
				statistic.register();
				statistic.addListener(this);
			}
			return true;
		}
		return false;
	}

	/**
	 * Copy constructor of data analysis runs.
	 * 
	 * @param other
	 *            The data analysis run to be cloned.
	 * @throws RegisterException
	 */
	protected DataAnalysisRun(final DataAnalysisRun other)
			throws RegisterException {
		super(other);
		this.dataConfigs = DataConfig
				.cloneDataConfigurations(other.dataConfigs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.AnalysisRun#cloneStatistics(java.util.List)
	 */
	@Override
	protected List<IDataStatistic> cloneStatistics(
			List<IDataStatistic> statistics) {
		final List<IDataStatistic> result = new ArrayList<IDataStatistic>();

		for (IDataStatistic st : statistics)
			result.add(st.clone());

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.Run#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IDataAnalysisRun#clone()
	 */
	@Override
	public DataAnalysisRun clone() {
		try {
			return new DataAnalysisRun(this);
		} catch (RegisterException e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IDataAnalysisRun#getDataConfigs()
	 */
	@Override
	public List<IDataConfig> getDataConfigs() {
		return dataConfigs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.Run#terminate()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IDataAnalysisRun#terminate()
	 */
	@Override
	public boolean terminate() {
		return true;
	}

	@Override
	protected IDataAnalysisRunRunnable createAndScheduleRunnableForRunPair(
			IRunSchedulerThread runScheduler, int p) {

		File movedConfigsDir = getMovedConfigsDir();

		/*
		 * We only operate on this copy, in order to avoid multithreading
		 * problems.
		 */
		// changed 13.02.2013
		// DataAnalysisRun runCopy = this.clone();
		DataAnalysisRun runCopy = this;

		IDataConfig dataConfig = this.getDataConfigs().get(p);

		/*
		 * Copy to results directory
		 */
		dataConfig.copyToFolder(movedConfigsDir);
		dataConfig.getDatasetConfig().copyToFolder(movedConfigsDir);
		if (dataConfig.hasGoldStandardConfig())
			dataConfig.getGoldstandardConfig().copyToFolder(movedConfigsDir);

		String input = dataConfig.getDatasetConfig().getDataSet()
				.getAbsolutePath();

		/*
		 * To avoid overwriting of the input or conversion files, we copy it to
		 * the results directory (which is unique for this run).
		 */

		String movedInput = FileUtils.buildPath(
				getMovedInputsDir().getAbsolutePath(),
				dataConfig.toString(".v"),
				new File(input).getParentFile().getName(),
				new File(input).getName());
		if (!(new File(movedInput).exists()))
			dataConfig.getDatasetConfig().getDataSet()
					.copyTo(new File(movedInput));

		/*
		 * Change the path to the input in the DataSetConfig.
		 */
		dataConfig.getDatasetConfig().getDataSet()
				.setAbsolutePath(new File(movedInput));

		/*
		 * Copy gold standard
		 */
		if (dataConfig.hasGoldStandardConfig()) {
			String goldStandard = dataConfig.getGoldstandardConfig()
					.getGoldstandard().getAbsolutePath();

			String movedGoldStandard = FileUtils.buildPath(
					getMovedGoldStandardsDir().getAbsolutePath(),
					new File(goldStandard).getParentFile().getName(),
					new File(goldStandard).getName());
			if (!(new File(movedGoldStandard).exists()))
				dataConfig.getGoldstandardConfig().getGoldstandard()
						.copyTo(new File(movedGoldStandard), false);

			/*
			 * Change the path to the goldstandard in the GoldstandardConfig.
			 */
			dataConfig.getGoldstandardConfig().getGoldstandard()
					.setAbsolutePath(new File(movedGoldStandard));
		}

		/*
		 * Start a thread with the invocation line and a path to the log file.
		 * The RunThread redirects all the output of the program into the
		 * logFile.
		 */
		final DataAnalysisRunRunnable t = new DataAnalysisRunRunnable(
				runScheduler, runCopy, runIdentString, false, dataConfig,
				runCopy.getStatistics());
		runCopy.progress.addSubProgress(t.getProgressPrinter(), 10000);
		runCopy.progressBeforeExecution
				.addSubProgress(t.getProgressPrinterBeforeExecution(), 10000);
		return t;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.Run#getUpperLimitProgress()
	 */
	@Override
	protected long getUpperLimitProgress() {
		return this.dataConfigs.size() * 10000;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.Run#getNumberOfRunRunnables()
	 */
	@Override
	protected int getNumberOfRunRunnables() {
		return this.getDataConfigs().size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.Run#createAndAddRunnableForResumePair(framework.RunScheduler,
	 * int)
	 */
	@Override
	protected DataAnalysisRunRunnable createAndScheduleRunnableForResumePair(
			IRunSchedulerThread runScheduler, int p) {

		File movedConfigsDir = getMovedConfigsDir();

		/*
		 * We only operate on this copy, in order to avoid multithreading
		 * problems.
		 */
		// changed 13.02.2013
		// DataAnalysisRun runCopy = this.clone();
		DataAnalysisRun runCopy = this;

		IDataConfig dataConfig = this.getDataConfigs().get(p);

		/*
		 * Copy to results directory
		 */
		dataConfig.copyToFolder(movedConfigsDir, false);
		dataConfig.getDatasetConfig().copyToFolder(movedConfigsDir, false);
		if (dataConfig.hasGoldStandardConfig())
			dataConfig.getGoldstandardConfig().copyToFolder(movedConfigsDir,
					false);

		String input = dataConfig.getDatasetConfig().getDataSet()
				.getAbsolutePath();

		/*
		 * To avoid overwriting of the input or conversion files, we copy it to
		 * the results directory (which is unique for this run).
		 */

		String movedInput = FileUtils.buildPath(
				getMovedInputsDir().getAbsolutePath(),
				dataConfig.toString(".v"),
				new File(input).getParentFile().getName(),
				new File(input).getName());
		if (!(new File(movedInput).exists()))
			dataConfig.getDatasetConfig().getDataSet()
					.copyTo(new File(movedInput), false);

		/*
		 * Change the path to the input in the DataSetConfig.
		 */
		dataConfig.getDatasetConfig().getDataSet()
				.setAbsolutePath(new File(movedInput));

		/*
		 * Copy gold standard
		 */
		if (dataConfig.hasGoldStandardConfig()) {
			String goldStandard = dataConfig.getGoldstandardConfig()
					.getGoldstandard().getAbsolutePath();

			String movedGoldStandard = FileUtils.buildPath(
					getMovedGoldStandardsDir().getAbsolutePath(),
					new File(goldStandard).getParentFile().getName(),
					new File(goldStandard).getName());
			if (!(new File(movedGoldStandard).exists()))
				dataConfig.getGoldstandardConfig().getGoldstandard()
						.copyTo(new File(movedGoldStandard), false);

			/*
			 * Change the path to the goldstandard in the GoldstandardConfig.
			 */
			dataConfig.getGoldstandardConfig().getGoldstandard()
					.setAbsolutePath(new File(movedGoldStandard));
		}

		/*
		 * Start a thread with the invocation line and a path to the log file.
		 * The RunThread redirects all the output of the program into the
		 * logFile.
		 */
		final DataAnalysisRunRunnable t = new DataAnalysisRunRunnable(
				runScheduler, runCopy, runIdentString, true, dataConfig,
				runCopy.getStatistics());
		runCopy.progress.addSubProgress(t.getProgressPrinter(), 10000);
		runCopy.progressBeforeExecution
				.addSubProgress(t.getProgressPrinterBeforeExecution(), 10000);
		return t;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.AnalysisRun#ensureVersionsForAllComponents(java.lang.
	 * String)
	 */
	@Override
	protected String ensureVersionsForAllComponents(String fileContents) {
		String newfileContents = super.ensureVersionsForAllComponents(
				fileContents);

		for (IDataConfig dc : this.dataConfigs) {
			String old = dc.getName();
			String withVersion = dc.toString();
			newfileContents = newfileContents
					.replaceAll(String.format("%s(?!\\:)", old), withVersion);
		}

		return newfileContents;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.AnalysisRun#asSerializable()
	 */
	@Override
	public SerializableDataAnalysisRun<? extends IDataAnalysisRun> asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableDataAnalysisRun<? extends IDataAnalysisRun>) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.AnalysisRun#asSerializable()
	 */
	@Override
	public SerializableDataAnalysisRun<? extends IDataAnalysisRun> asSerializableInternal()
			throws RepositoryObjectSerializationException {
		return new SerializableDataAnalysisRun(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.Run#dumpToFileHelper(org.apache.commons.configuration.
	 * HierarchicalINIConfiguration)
	 */
	@Override
	protected void dumpToFileHelper(HierarchicalINIConfiguration conf)
			throws RepositoryObjectDumpException {
		super.dumpToFileHelper(conf);

		this.dumpDataConfigsToFile(conf);
		this.dumpDataStatisticsToFile(conf);

	}

	protected void dumpDataStatisticsToFile(HierarchicalINIConfiguration conf) {
		conf.addProperty("dataStatistics", StringExt.paste(",", statistics));
	}

	protected void dumpDataConfigsToFile(HierarchicalINIConfiguration conf) {
		conf.addProperty("dataConfig", StringExt.paste(",", dataConfigs));
	}
}
