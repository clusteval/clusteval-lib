/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.HierarchicalINIConfiguration;

import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.context.IContext;
import de.clusteval.data.IDataConfig;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryObjectDumpException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.threading.IRunSchedulerThread;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.run.runnable.ClusteringRunRunnable;
import de.clusteval.run.runnable.IClusteringRunRunnable;
import de.clusteval.run.runresult.postprocessing.IRunResultPostprocessor;

/**
 * A type of execution run that performs exactly one clustering with one
 * parameter set for every pair of program and data configuration.
 * 
 * @author Christian Wiwie
 * 
 */
public class ClusteringRun<RUNNABLE_TYPE extends IClusteringRunRunnable<?, ?>>
		extends
			ExecutionRun<RUNNABLE_TYPE>
		implements
			IClusteringRun<RUNNABLE_TYPE> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5484003551707005631L;

	/**
	 * New objects of this type are automatically registered at the repository.
	 * 
	 * @param repository
	 *            the repository
	 * @param context
	 * @param changeDate
	 *            The date this run was performed.
	 * @param absPath
	 *            The absolute path to the file on the filesystem that
	 *            corresponds to this run.
	 * @param programConfigs
	 *            The program configurations of the new run.
	 * @param dataConfigs
	 *            The data configurations of the new run.
	 * @param qualityMeasures
	 *            The clustering quality measures of the new run.
	 * @param fixedParameterValues
	 *            A map for each pair of program config and data config, that
	 *            contains parameters which are fixed to specific values during
	 * @param postProcessors
	 * @param maxExecutionTimes
	 */
	public ClusteringRun(IRepository repository, final IContext context,
			long changeDate, File absPath, List<IProgramConfig> programConfigs,
			List<IDataConfig> dataConfigs,
			List<IClusteringQualityMeasure> qualityMeasures,
			Map<IProgramConfig, Map<IProgramParameter<?>, String>> fixedParameterValues,
			final List<IRunResultPostprocessor> postProcessors,
			final Map<String, Integer> maxExecutionTimes) {
		super(repository, context, changeDate, absPath, programConfigs,
				dataConfigs, qualityMeasures, fixedParameterValues,
				postProcessors, maxExecutionTimes);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ExecutionRun#register()
	 */
	@Override
	public boolean register() throws RegisterException {
		if (super.register()) {
			// register this Run at all dataconfigs and programconfigs
			for (IDataConfig dataConfig : this.originalDataConfigs) {
				dataConfig.addListener(this);
			}
			for (IProgramConfig programConfig : this.originalProgramConfigs) {
				programConfig.addListener(this);
			}

			for (IClusteringQualityMeasure measure : this.qualityMeasures) {
				// added 21.03.2013: measures are only registered here, if this
				// run has been registered
				measure.register();
				measure.addListener(this);
			}
			return true;
		}
		return false;
	}

	/**
	 * Copy constructor of clustering runs.
	 * 
	 * @param clusteringRun
	 *            The clustering run to be cloned.
	 */
	public ClusteringRun(final ClusteringRun clusteringRun) {
		super(clusteringRun);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.ExecutionRun#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IClusteringRun#clone()
	 */
	@Override
	public ClusteringRun clone() {
		return new ClusteringRun(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.ExecutionRun#createRunRunnableFor(de.clusteval.framework
	 * .threading.IRunSchedulerThread, de.clusteval.run.ExecutionRun,
	 * de.clusteval.program.IProgramConfig, de.clusteval.data.IDataConfig,
	 * java.lang.String, boolean, java.util.Map)
	 */
	@Override
	protected <T extends ExecutionRun<RUNNABLE_TYPE>> RUNNABLE_TYPE createRunRunnableFor(
			IRunSchedulerThread runScheduler, T run,
			IProgramConfig programConfig, IDataConfig dataConfig,
			String runIdentString, boolean isResume,
			Map<IProgramParameter<?>, String> runParams) {
		ClusteringRunRunnable r = new ClusteringRunRunnable(runScheduler,
				(ClusteringRun) run, programConfig, dataConfig, runIdentString,
				isResume, runParams);
		((Run) run).progress.addSubProgress(r.getProgressPrinter(), 10000);
		((Run) run).progressBeforeExecution
				.addSubProgress(r.getProgressPrinterBeforeExecution(), 10000);
		return (RUNNABLE_TYPE) r;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ExecutionRun#asSerializable()
	 */
	@Override
	public SerializableClusteringRun<? extends IClusteringRun<RUNNABLE_TYPE>> asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableClusteringRun<? extends IClusteringRun<RUNNABLE_TYPE>>) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.AnalysisRun#asSerializable()
	 */
	@Override
	public SerializableClusteringRun<? extends IClusteringRun<?>> asSerializableInternal()
			throws RepositoryObjectSerializationException {
		return new SerializableClusteringRun(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ExecutionRun#dumpToFileHelper(org.apache.commons.
	 * configuration.HierarchicalINIConfiguration)
	 */
	@Override
	protected void dumpToFileHelper(HierarchicalINIConfiguration conf)
			throws RepositoryObjectDumpException {
		super.dumpToFileHelper(conf);
	}
}
