/**
 * 
 */
package de.clusteval.run;

import java.io.File;
import java.util.List;

import de.clusteval.context.ISerializableContext;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.run.statistics.IRunStatistic;
import de.clusteval.utils.ISerializableStatistic;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * @author Christian Wiwie
 *
 */
public class SerializableRunAnalysisRun<R extends IRunAnalysisRun>
		extends
			SerializableAnalysisRun<R>
		implements
			ISerializableRunAnalysisRun<R> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3916498486247139885L;

	protected List<String> uniqueRunAnalysisRunIdentifiers;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 * @param context
	 * @param statistics
	 */
	public SerializableRunAnalysisRun(File absPath, String name, String version,
			ISerializableContext context,
			List<ISerializableStatistic> statistics,
			final List<String> uniqueRunAnalysisRunIdentifiers) {
		super(absPath, name, version, context, statistics);
		this.uniqueRunAnalysisRunIdentifiers = uniqueRunAnalysisRunIdentifiers;
	}

	public SerializableRunAnalysisRun(final R run) throws RepositoryObjectSerializationException {
		super(run);
		this.uniqueRunAnalysisRunIdentifiers = run
				.getUniqueRunAnalysisRunIdentifiers();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ISerializableRunAnalysisRun#
	 * getUniqueRunAnalysisRunIdentifiers()
	 */
	@Override
	public List<String> getUniqueRunAnalysisRunIdentifiers() {
		return this.uniqueRunAnalysisRunIdentifiers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.SerializableRun#deserializeInternal(de.clusteval.
	 * framework.repository.IRepository)
	 */
	@Override
	protected R deserializeInternal(IRepository repository)
			throws DeserializationException {
		List<IRunStatistic> statistics = (List) deserializeStatistics(
				repository);

		return (R) new RunAnalysisRun(repository,
				this.context.deserialize(repository),
				System.currentTimeMillis(),
				new File(FileUtils.buildPath(repository.getBasePath(IRun.class),
						this.name + ".v" + version + ".run")),
				this.uniqueRunAnalysisRunIdentifiers, statistics);

	}
}
