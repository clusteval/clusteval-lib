/**
 * 
 */
package de.clusteval.run;

import java.io.File;
import java.util.List;
import java.util.Map;

import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.cluster.quality.ISerializableClusteringQualityMeasure;
import de.clusteval.context.ISerializableContext;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.ISerializableDataConfig;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.program.ISerializableProgramConfig;
import de.clusteval.run.runnable.IClusteringRunRunnable;
import de.clusteval.run.runresult.postprocessing.IRunResultPostprocessor;
import de.clusteval.run.runresult.postprocessing.ISerializableRunResultPostprocessor;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * @author Christian Wiwie
 *
 */
public class SerializableClusteringRun<R extends IClusteringRun<?>>
		extends
			SerializableExecutionRun<R>
		implements
			ISerializableClusteringRun<R> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9135203545416477564L;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 * @param context
	 * @param dataConfigs
	 * @param programConfigs
	 * @param postProcessors
	 * @param qualityMeasures
	 * @param fixedParameterValues
	 * @param maxExecutionTimes
	 */
	public SerializableClusteringRun(File absPath, String name, String version,
			ISerializableContext context,
			List<ISerializableDataConfig> dataConfigs,
			List<ISerializableProgramConfig<? extends IProgramConfig>> programConfigs,
			List<ISerializableRunResultPostprocessor> postProcessors,
			List<ISerializableClusteringQualityMeasure> qualityMeasures,
			final Map<ISerializableProgramConfig<? extends IProgramConfig>, Map<IProgramParameter<?>, String>> fixedParameterValues,
			final Map<String, Integer> maxExecutionTimes) {
		super(absPath, name, version, context, dataConfigs, programConfigs,
				postProcessors, fixedParameterValues, qualityMeasures,
				maxExecutionTimes);
	}

	public SerializableClusteringRun(final R run)
			throws RepositoryObjectSerializationException {
		super(run);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.SerializableRun#deserializeInternal(de.clusteval.
	 * framework.repository.IRepository)
	 */
	@Override
	protected R deserializeInternal(IRepository repository)
			throws DeserializationException {
		List<IProgramConfig> programConfigs = deserializeProgramConfigs(
				repository);
		List<IDataConfig> dataConfigs = deserializeDataConfigs(repository);
		List<IClusteringQualityMeasure> qualityMeasures = deserializeClusteringQualityMeasures(
				repository);

		List<IRunResultPostprocessor> postProcessors = deserializeRunResultPostprocessor(
				repository);

		Map<IProgramConfig, Map<IProgramParameter<?>, String>> fixedParameterValues = deserializeFixedParameterValues(
				repository);

		R deserializedRun = (R) new ClusteringRun(repository,
				this.context.deserialize(repository),
				System.currentTimeMillis(),
				new File(FileUtils.buildPath(repository.getBasePath(IRun.class),
						this.name + ".v" + version + ".run")),
				programConfigs, dataConfigs, qualityMeasures,
				fixedParameterValues, postProcessors, this.maxExecutionTimes);
		return deserializedRun;

	}
}
