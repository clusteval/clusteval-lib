/**
 * 
 */
package de.clusteval.run;

import java.io.File;
import java.util.List;
import java.util.Map;

import de.clusteval.cluster.quality.ISerializableClusteringQualityMeasure;
import de.clusteval.context.IContext;
import de.clusteval.data.ISerializableDataConfig;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.program.ISerializableProgramConfig;
import de.clusteval.run.runresult.postprocessing.ISerializableRunResultPostprocessor;

/**
 * @author Christian Wiwie
 *
 */
public class SerializableInternalParameterOptimizationRun<R extends IInternalParameterOptimizationRun>
		extends
			SerializableExecutionRun<R>
		implements
			ISerializableInternalParameterOptimizationRun<R> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6853848034774905606L;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 * @param context
	 * @param dataConfigs
	 * @param programConfigs
	 * @param postProcessors
	 * @param qualityMeasures
	 * @param fixedParameterValues
	 * @param maxExecutionTimes
	 */
	public SerializableInternalParameterOptimizationRun(File absPath,
			String name, String version, ISerializableWrapperRepositoryObject<IContext> context,
			List<ISerializableDataConfig> dataConfigs,
			List<ISerializableProgramConfig<? extends IProgramConfig>> programConfigs,
			List<ISerializableRunResultPostprocessor> postProcessors,
			List<ISerializableClusteringQualityMeasure> qualityMeasures,
			final Map<ISerializableProgramConfig<? extends IProgramConfig>, Map<IProgramParameter<?>, String>> fixedParameterValues,
			final Map<String, Integer> maxExecutionTimes) {
		super(absPath, name, version, context, dataConfigs, programConfigs,
				postProcessors, fixedParameterValues, qualityMeasures,
				maxExecutionTimes);
	}

	public SerializableInternalParameterOptimizationRun(final R run)
			throws RepositoryObjectSerializationException {
		super(run);
	}
}
