/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.configuration.HierarchicalINIConfiguration;
import org.apache.commons.configuration.SubnodeConfiguration;

import de.clusteval.cluster.quality.ClusteringQualityMeasure;
import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.context.IContext;
import de.clusteval.data.DataConfig;
import de.clusteval.data.IDataConfig;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryEvent;
import de.clusteval.framework.repository.RepositoryObjectDumpException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.RepositoryRemoveEvent;
import de.clusteval.framework.repository.RepositoryReplaceEvent;
import de.clusteval.framework.threading.IRunSchedulerThread;
import de.clusteval.framework.threading.RunSchedulerThread;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.program.ProgramConfig;
import de.clusteval.run.runnable.IExecutionRunRunnable;
import de.clusteval.run.runnable.IRunRunnable;
import de.clusteval.run.runnable.RunRunnableInitializationException;
import de.clusteval.run.runresult.NoRunResultFormatParserException;
import de.clusteval.run.runresult.postprocessing.IRunResultPostprocessor;
import dk.sdu.imada.compbio.file.FileUtils;
import dk.sdu.imada.compbio.utils.Pair;
import dk.sdu.imada.compbio.utils.StringExt;

/**
 * An abstract class for all run types, that involve execution of clustering
 * tools and applying them to datasets.
 * 
 * <p>
 * An execution run has a list of program and data configurations. These are
 * pairwise combined and for every pair (see
 * {@link #clonedProgramAndDataConfigPairs}) a runnable is created and executed
 * asynchronously by the {@link RunSchedulerThread}.
 * 
 * <p>
 * The data and program configurations passed to this run are the same as the
 * objects stored in the repository. Thus those objects can change during
 * runtime of this run. To avoid those changes affecting the run during its
 * execution the original objects are cloned in
 * {@link #initClonedProgramAndDataConfigPairs(List, List)} before they are
 * passed to the run runnables which are performed asynchronously.
 * 
 * @author Christian Wiwie
 * 
 */
public abstract class ExecutionRun<RUNNABLE_TYPE extends IExecutionRunRunnable<?, ?>>
		extends
			Run<RUNNABLE_TYPE>
		implements
			IExecutionRun<RUNNABLE_TYPE> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1945031005612369178L;

	protected static Map<IProgramConfig, Map<IProgramParameter<?>, String>> cloneParameterValues(
			final Map<IProgramConfig, Map<IProgramParameter<?>, String>> parameterValues) {
		Map<IProgramConfig, Map<IProgramParameter<?>, String>> result = new HashMap<IProgramConfig, Map<IProgramParameter<?>, String>>();

		for (Entry<IProgramConfig, Map<IProgramParameter<?>, String>> e : parameterValues
				.entrySet()) {
			Map<IProgramParameter<?>, String> newMap = new HashMap<IProgramParameter<?>, String>();

			for (Map.Entry<IProgramParameter<?>, String> entry : e.getValue()
					.entrySet()) {
				newMap.put(entry.getKey().clone(), entry.getValue() + "");
			}

			result.put(e.getKey(), newMap);
		}

		return result;
	}

	protected static List<IRunResultPostprocessor> clonePostProcessors(
			final List<IRunResultPostprocessor> postProcessors) {
		List<IRunResultPostprocessor> result = new ArrayList<IRunResultPostprocessor>();

		for (IRunResultPostprocessor postpro : postProcessors) {
			result.add(postpro.clone());
		}

		return result;
	}

	/**
	 * A list of program configurations contained in this run.
	 * 
	 * <p>
	 * The references to program configurations in this list are the same as
	 * those stored in the repository. That means the objects in this list can
	 * change during runtime of the run.
	 */
	protected List<IProgramConfig> originalProgramConfigs;

	/**
	 * The maximal number of minutes a clustering iteration can run before it is
	 * terminated.
	 */
	protected Map<String, Integer> maxExecutionTimes;

	/**
	 * A list of data configurations contained in this run.
	 * 
	 * <p>
	 * The references to data configurations in this list are the same as those
	 * stored in the repository. That means the objects in this list can change
	 * during runtime of the run.
	 */
	protected List<IDataConfig> originalDataConfigs;

	/**
	 * The pairwise combinations of data and program configurations that are
	 * used to create the runnables.
	 */
	protected List<Pair<IProgramConfig, IDataConfig>> clonedProgramAndDataConfigPairs;

	/**
	 * During execution of this run for every clustering that is calculated a
	 * set of clustering quality measures is calculated.
	 */
	protected transient List<IClusteringQualityMeasure> qualityMeasures;

	/**
	 * The parameter values for every pair of program and data configuration.
	 */
	protected Map<IProgramConfig, Map<IProgramParameter<?>, String>> fixedParameterValues;

	protected transient List<IRunResultPostprocessor> postProcessors;

	/**
	 * This constructor is protected to encourage usage of the static factory
	 * method.
	 * 
	 * @param repository
	 *            the repository
	 * @param name
	 *            The name of this run.
	 * @param changeDate
	 *            The date this run was performed.
	 * @param absPath
	 *            The absolute path to the file on the filesystem that
	 *            corresponds to this run.
	 * @param programConfigs
	 *            The program configurations of the new run.
	 * @param dataConfigs
	 *            The data configurations of the new run.
	 * @param qualityMeasures
	 *            The clustering quality measures of the new run.
	 * @param fixedParameterValues
	 *            A map for each pair of program config and data config, that
	 *            contains parameters which are fixed to specific values during
	 *            respective execution.
	 */
	protected ExecutionRun(final IRepository repository, final IContext context,
			final long changeDate, final File absPath,
			final List<IProgramConfig> programConfigs,
			final List<IDataConfig> dataConfigs,
			final List<IClusteringQualityMeasure> qualityMeasures,
			final Map<IProgramConfig, Map<IProgramParameter<?>, String>> fixedParameterValues,
			final List<IRunResultPostprocessor> postProcessors,
			final Map<String, Integer> maxExecutionTimes) {
		super(repository, context, changeDate, absPath);

		this.originalProgramConfigs = programConfigs;
		this.originalDataConfigs = dataConfigs;

		this.fixedParameterValues = fixedParameterValues;
		this.qualityMeasures = qualityMeasures;
		this.postProcessors = postProcessors;
		this.maxExecutionTimes = maxExecutionTimes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#register()
	 */
	@Override
	public boolean register() throws RegisterException {
		if (super.register()) {
			// register this Run at all dataconfigs and programconfigs
			for (IDataConfig dataConfig : this.originalDataConfigs) {
				dataConfig.addListener(this);
			}
			for (IProgramConfig programConfig : this.originalProgramConfigs) {
				programConfig.addListener(this);
			}

			for (IClusteringQualityMeasure measure : this.qualityMeasures) {
				// added 21.03.2013: measures are only registered here, if this
				// run has been registered
				measure.register();
				measure.addListener(this);
			}

			for (IRunResultPostprocessor postpro : this.postProcessors) {
				postpro.addListener(this);
			}
			return true;
		}
		return false;
	}

	/**
	 * Copy constructor for execution runs.
	 * 
	 * @param other
	 *            The execution run to be cloned.
	 */
	protected ExecutionRun(final ExecutionRun other) {
		super(other);

		this.fixedParameterValues = cloneParameterValues(
				other.fixedParameterValues);
		this.qualityMeasures = ClusteringQualityMeasure
				.cloneQualityMeasures(other.qualityMeasures);
		this.postProcessors = clonePostProcessors(other.postProcessors);
		this.maxExecutionTimes = new HashMap<String, Integer>(
				other.maxExecutionTimes);

		initClonedProgramAndDataConfigPairs(
				ProgramConfig.cloneProgramConfigurations(
						other.originalProgramConfigs),
				DataConfig.cloneDataConfigurations(other.originalDataConfigs));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.repository.RepositoryObject#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IExecutionRun#clone()
	 */
	@Override
	public abstract ExecutionRun clone();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IExecutionRun#terminate()
	 */
	@Override
	public boolean terminate() {
		synchronized (this.runnables) {
			for (IRunRunnable runnable : this.runnables) {
				runnable.terminate();
			}
			this.setStatus(RUN_STATUS.TERMINATED);
			return true;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.Run#getUpperLimitProgress()
	 */
	@Override
	protected long getUpperLimitProgress() {
		// we set the number of steps of this run to 100% for every run pair.
		// return getRunPairs().size() * 10000;
		return this.originalDataConfigs.size()
				* this.originalProgramConfigs.size() * 10000;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.Run#beforePerform()
	 */
	@Override
	protected void beforePerform()
			throws IOException, RunInitializationException {
		super.beforePerform();
		try {
			initClonedProgramAndDataConfigPairs(originalProgramConfigs,
					originalDataConfigs);
		} catch (Exception e) {
			throw new RunInitializationException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.IExecutionRun#perform(de.clusteval.framework.threading.
	 * IRunSchedulerThread)
	 */
	@Override
	public void perform(final IRunSchedulerThread runScheduler)
			throws IOException, RunRunnableInitializationException,
			RunInitializationException {
		/*
		 * Before we start we check, whether this run has been terminated by
		 * invoking terminate(). This is also the reason, why we have to
		 * synchronize here in order to avoid only partial termination.
		 */
		synchronized (this.runnables) {
			if (this.getStatus().equals(RUN_STATUS.TERMINATED))
				return;

			beforePerform();
			doPerform(runScheduler);
		}
		waitForRunnablesToFinish();
		afterPerform();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.Run#beforeResume(java.lang.String)
	 */
	@Override
	protected void beforeResume(String runIdentString)
			throws RunInitializationException {
		super.beforeResume(runIdentString);
		try {
			initClonedProgramAndDataConfigPairs(originalProgramConfigs,
					originalDataConfigs);
		} catch (Exception e) {
			throw new RunInitializationException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.IExecutionRun#resume(de.clusteval.framework.threading.
	 * IRunSchedulerThread, java.lang.String)
	 */
	@SuppressWarnings("unused")
	@Override
	public void resume(final IRunSchedulerThread runScheduler,
			final String runIdentString) throws MissingParameterValueException,
			IOException, NoRunResultFormatParserException,
			RunRunnableInitializationException, RunInitializationException {
		/*
		 * Before we start we check, whether this run has been terminated by
		 * invoking terminate(). This is also the reason, why we have to
		 * synchronize here in order to avoid only partial termination.
		 */
		synchronized (this.runnables) {
			if (this.getStatus().equals(RUN_STATUS.TERMINATED))
				return;

			beforeResume(runIdentString);
			doResume(runScheduler, runIdentString);
		}
		waitForRunnablesToFinish();
		afterResume(runIdentString);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.Run#getNumberOfRunRunnables()
	 */
	@Override
	protected int getNumberOfRunRunnables() {
		return getClonedProgramAndDataConfigPairs().size();
	}

	@Override
	protected RUNNABLE_TYPE createAndScheduleRunnableForRunPair(
			IRunSchedulerThread runScheduler, int p) {
		File movedConfigsDir = getMovedConfigsDir();

		/*
		 * We only operate on this copy, in order to avoid multithreading
		 * problems.
		 */
		// changed 22.01.2013
		// ExecutionRun runCopy = this.clone();
		ExecutionRun<RUNNABLE_TYPE> runCopy = this;

		final Pair<IProgramConfig, IDataConfig> pair = runCopy
				.getClonedProgramAndDataConfigPairs().get(p);

		IProgramConfig programConfig = pair.getFirst();
		IDataConfig dataConfig = pair.getSecond();

		/*
		 * Copy to results directory
		 */
		// 06.04.2013: create File objects for later use in order to create new
		// data configuration and program configuration objects
		File copiedDataConfig = new File(
				FileUtils.buildPath(movedConfigsDir.getAbsolutePath(),
						new File(dataConfig.getAbsolutePath()).getName()));
		dataConfig.copyTo(copiedDataConfig);
		File copiedDataSetConfig = new File(FileUtils.buildPath(
				movedConfigsDir.getAbsolutePath(),
				new File(dataConfig.getDatasetConfig().getAbsolutePath())
						.getName()));
		dataConfig.getDatasetConfig().copyTo(copiedDataSetConfig);
		File copiedGoldstandardConfig = null;
		if (dataConfig.hasGoldStandardConfig()) {
			copiedGoldstandardConfig = new File(
					FileUtils.buildPath(movedConfigsDir.getAbsolutePath(),
							new File(dataConfig.getGoldstandardConfig()
									.getAbsolutePath()).getName()));
			dataConfig.getGoldstandardConfig().copyTo(copiedGoldstandardConfig);
		}

		File copiedProgramConfig = new File(
				FileUtils.buildPath(movedConfigsDir.getAbsolutePath(),
						new File(programConfig.getAbsolutePath()).getName()));
		programConfig.copyTo(copiedProgramConfig);

		String input = dataConfig.getDatasetConfig().getDataSet()
				.getAbsolutePath();

		/*
		 * To avoid overwriting of the input or conversion files, we copy it to
		 * the results directory (which is unique for this run).
		 */

		// changed 01.09.2012, don't create subdirectory for
		// programConfig_dataConfig
		// undo: 26.02.2013: not creating subdirectory lead to a bug of
		// overwriting same dataset file for different dataconfigs
		String movedInput = FileUtils.buildPath(
				getMovedInputsDir().getAbsolutePath(),
				programConfig.toString(".v") + "_" + dataConfig.toString(".v"),
				new File(input).getParentFile().getName(),
				new File(input).getName());
		if (!(new File(movedInput).exists()))
			dataConfig.getDatasetConfig().getDataSet()
					.copyTo(new File(movedInput));

		/*
		 * Copy gold standard
		 */
		String movedGoldStandard = null;
		if (dataConfig.hasGoldStandardConfig()) {
			String goldStandard = dataConfig.getGoldstandardConfig()
					.getGoldstandard().getAbsolutePath();

			movedGoldStandard = FileUtils.buildPath(
					getMovedGoldStandardsDir().getAbsolutePath(),
					new File(goldStandard).getParentFile().getName(),
					new File(goldStandard).getName());
			if (!(new File(movedGoldStandard).exists()))
				dataConfig.getGoldstandardConfig().getGoldstandard()
						.copyTo(new File(movedGoldStandard), false);
		}

		/*
		 * 06.04.2013: create a new data configuration object for use within the
		 * runnable bugfix: the runnable so far accessed the old data
		 * configuration and program configuration objects. this lead to
		 * inconsistent behaviour when accessing internal attributes, because
		 * all threads were operating on the same objects.
		 */
		IDataConfig newDataConfig = dataConfig.clone();
		newDataConfig.setAbsolutePath(copiedDataConfig);
		newDataConfig.getDatasetConfig().setAbsolutePath(copiedDataSetConfig);
		newDataConfig.getDatasetConfig().getDataSet()
				.setAbsolutePath(new File(movedInput));
		if (newDataConfig.hasGoldStandardConfig()) {
			newDataConfig.getGoldstandardConfig()
					.setAbsolutePath(copiedGoldstandardConfig);
			newDataConfig.getGoldstandardConfig().getGoldstandard()
					.setAbsolutePath(new File(movedGoldStandard));
		}
		IProgramConfig newProgramConfig = programConfig.clone();
		newProgramConfig.setAbsolutePath(copiedProgramConfig);

		/*
		 * Start a thread with the invocation line and a path to the log file.
		 * The RunThread redirects all the output of the program into the
		 * logFile.
		 */
		final RUNNABLE_TYPE t = createRunRunnableFor(runScheduler, runCopy,
				newProgramConfig, newDataConfig, runIdentString, false,
				this.getRunParameterForRunPair(p));
		return t;
	}

	@Override
	protected RUNNABLE_TYPE createAndScheduleRunnableForResumePair(
			IRunSchedulerThread runScheduler, int p) {

		File movedConfigsDir = getMovedConfigsDir();

		/*
		 * We only operate on this copy, in order to avoid multithreading
		 * problems.
		 */
		// changed 22.01.2013
		// ExecutionRun runCopy = this.clone();
		ExecutionRun<RUNNABLE_TYPE> runCopy = this;

		final Pair<IProgramConfig, IDataConfig> pair = runCopy
				.getClonedProgramAndDataConfigPairs().get(p);

		IProgramConfig programConfig = pair.getFirst();
		IDataConfig dataConfig = pair.getSecond();

		/*
		 * Copy to results directory
		 */
		// 06.04.2013: create File objects for later use in order to create new
		// data configuration and program configuration objects
		File copiedDataConfig = new File(
				FileUtils.buildPath(movedConfigsDir.getAbsolutePath(),
						new File(dataConfig.getAbsolutePath()).getName()));
		dataConfig.copyTo(copiedDataConfig, false);

		File copiedDataSetConfig = new File(FileUtils.buildPath(
				movedConfigsDir.getAbsolutePath(),
				new File(dataConfig.getDatasetConfig().getAbsolutePath())
						.getName()));
		dataConfig.getDatasetConfig().copyTo(copiedDataSetConfig, false);

		File copiedGoldstandardConfig = null;
		if (dataConfig.hasGoldStandardConfig()) {
			copiedGoldstandardConfig = new File(
					FileUtils.buildPath(movedConfigsDir.getAbsolutePath(),
							new File(dataConfig.getGoldstandardConfig()
									.getAbsolutePath()).getName()));
			dataConfig.getGoldstandardConfig().copyTo(copiedGoldstandardConfig,
					false);
		}
		File copiedProgramConfig = new File(
				FileUtils.buildPath(movedConfigsDir.getAbsolutePath(),
						new File(programConfig.getAbsolutePath()).getName()));
		programConfig.copyTo(copiedProgramConfig, false);

		String input = dataConfig.getDatasetConfig().getDataSet()
				.getAbsolutePath();

		/*
		 * To avoid overwriting of the input or conversion files, we copy it to
		 * the results directory (which is unique for this run).
		 */

		// changed 01.09.2012, don't create subdirectory for
		// programConfig_dataConfig
		// undo: 26.02.2013: not creating subdirectory lead to a bug of
		// overwriting same dataset file for different dataconfigs
		String movedInput = FileUtils.buildPath(
				getMovedInputsDir().getAbsolutePath(),
				programConfig.toString(".v") + "_" + dataConfig.toString(".v"),
				new File(input).getParentFile().getName(),
				new File(input).getName());
		if (!(new File(movedInput).exists()))
			dataConfig.getDatasetConfig().getDataSet()
					.copyTo(new File(movedInput), false);

		/*
		 * Change the path to the input in the DataSetConfig.
		 */
		dataConfig.getDatasetConfig().getDataSet()
				.setAbsolutePath(new File(movedInput));

		/*
		 * Copy gold standard
		 */
		String movedGoldStandard = null;
		if (dataConfig.hasGoldStandardConfig()) {
			String goldStandard = dataConfig.getGoldstandardConfig()
					.getGoldstandard().getAbsolutePath();
			movedGoldStandard = FileUtils.buildPath(
					getMovedGoldStandardsDir().getAbsolutePath(),
					new File(goldStandard).getParentFile().getName(),
					new File(goldStandard).getName());
			if (!(new File(movedGoldStandard).exists()))
				dataConfig.getGoldstandardConfig().getGoldstandard()
						.copyTo(new File(movedGoldStandard), false);
		}
		/*
		 * 06.04.2013: create a new data configuration object for use within the
		 * runnable bugfix: the runnable so far accessed the old data
		 * configuration and program configuration objects. this lead to
		 * inconsistent behaviour when accessing internal attributes, because
		 * all threads were operating on the same objects.
		 */
		IDataConfig newDataConfig = dataConfig.clone();
		newDataConfig.setAbsolutePath(copiedDataConfig);
		newDataConfig.getDatasetConfig().setAbsolutePath(copiedDataSetConfig);

		newDataConfig.getDatasetConfig().getDataSet()
				.setAbsolutePath(new File(movedInput));
		if (newDataConfig.hasGoldStandardConfig()) {
			newDataConfig.getGoldstandardConfig()
					.setAbsolutePath(copiedGoldstandardConfig);
			newDataConfig.getGoldstandardConfig().getGoldstandard()
					.setAbsolutePath(new File(movedGoldStandard));
		}
		IProgramConfig newProgramConfig = programConfig.clone();
		newProgramConfig.setAbsolutePath(copiedProgramConfig);

		/*
		 * Start a thread with the invocation line and a path to the log file.
		 * The RunThread redirects all the output of the program into the
		 * logFile.
		 */
		final RUNNABLE_TYPE t = createRunRunnableFor(runScheduler, runCopy,
				newProgramConfig, newDataConfig, runIdentString, true,
				this.getRunParameterForRunPair(p));
		return t;

	}

	protected Map<IProgramParameter<? extends Object>, String> getRunParameterForRunPair(
			final int p) {
		return this.fixedParameterValues
				.get(this.originalProgramConfigs.get((int) Math.round(Math
						.floor(p / (double) this.originalDataConfigs.size()))));
	}

	/**
	 * This is a helper method for the
	 * {@link #createAndScheduleRunnableForResumePair(RunSchedulerThread, int)}
	 * and {@link #createAndScheduleRunnableForRunPair(RunSchedulerThread, int)}
	 * methods. This method is responsible to instanciate objects of the
	 * corresponding runnable runtime type for the runtime type of this run.
	 * Override it in your own sub type of the ExecutionRun class.
	 * 
	 * @param runScheduler
	 *            The run scheduler to which the newly created runnable should
	 *            be passed.
	 * @param run
	 *            A reference to a cloned copy of this run that should be
	 *            executed.
	 * @param programConfig
	 *            The program configuration that is used by the resulting
	 *            runnable.
	 * @param dataConfig
	 *            The data configuration that is used by the resulting runnable.
	 * @param runIdentString
	 *            The unique run identification string, that identifies this
	 *            execution of the run.
	 * @param isResume
	 *            A boolean which indicates, whether the created runnable should
	 *            perform a run or resume one.
	 * @return The runnable being executed asynchronously.
	 */
	protected abstract <T extends ExecutionRun<RUNNABLE_TYPE>> RUNNABLE_TYPE createRunRunnableFor(
			IRunSchedulerThread runScheduler, T run,
			IProgramConfig programConfig, IDataConfig dataConfig,
			String runIdentString, boolean isResume,
			Map<IProgramParameter<?>, String> runParams);

	/**
	 * This method verifies that all quality measures can be calculated for
	 * every data configuration. This can be due to the fact, that some quality
	 * measures require a goldstandard. If a data configuration does not contain
	 * a goldstandard, such quality measures cannot be calculated.
	 * 
	 * @param dataConfigs
	 *            The data configurations to check.
	 * @param qualityMeasures
	 *            The quality measures to check.
	 * @throws RunException
	 *             An exception that indicates, that some quality measures and
	 *             data configurations are not compatible.
	 */
	public static void checkCompatibilityQualityMeasuresDataConfigs(
			final List<IDataConfig> dataConfigs,
			final List<IClusteringQualityMeasure> qualityMeasures)
			throws RunException {
		/*
		 * Check whether some dataconfigs don't have goldstandards but quality
		 * measures require them
		 */

		Set<IClusteringQualityMeasure> qualityMeasuresRequireGS = new HashSet<IClusteringQualityMeasure>();
		for (IClusteringQualityMeasure qualityMeasure : qualityMeasures)
			if (qualityMeasure.requiresGoldstandard())
				qualityMeasuresRequireGS.add(qualityMeasure);

		Set<IDataConfig> dataConfigsWithoutGS = new HashSet<IDataConfig>();
		for (IDataConfig dataConfig : dataConfigs)
			if (!dataConfig.hasGoldStandardConfig()
					&& qualityMeasuresRequireGS.size() > 0) {
				dataConfigsWithoutGS.add(dataConfig);
			}

		if (dataConfigsWithoutGS.size() > 0)
			throw new RunException(
					"This Run contains dataconfigs that do not provide goldstandards "
							+ dataConfigsWithoutGS
							+ ", but also ClusteringQualityMeasures that require goldstandards "
							+ qualityMeasuresRequireGS);
	}

	/**
	 * For every pair of program and data configuration we create an object and
	 * add it to the list of runpairs.
	 * 
	 * <p>
	 * This method clones the data and program configurations of this execution
	 * run, such that only cloned objects are passed to the threads. This
	 * ensures, that changes that happen to the configurations during runtime do
	 * not affect the runs currently performing.
	 * 
	 * @param programConfigs
	 *            The list of program configurations of this run.
	 * @param dataConfigs
	 *            The list of data configurations of this run.
	 */
	protected void initClonedProgramAndDataConfigPairs(
			final List<IProgramConfig> programConfigs,
			final List<IDataConfig> dataConfigs) {

		if (this.originalProgramConfigs == null)
			this.originalProgramConfigs = programConfigs;
		if (this.originalDataConfigs == null)
			this.originalDataConfigs = dataConfigs;

		this.clonedProgramAndDataConfigPairs = new ArrayList<Pair<IProgramConfig, IDataConfig>>();

		for (IProgramConfig programConfig : this.originalProgramConfigs) {
			for (IDataConfig dataConfig : this.originalDataConfigs) {
				clonedProgramAndDataConfigPairs
						.add(new Pair<IProgramConfig, IDataConfig>(
								((IProgramConfig) programConfig).clone(),
								((IDataConfig) dataConfig).clone()));
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IExecutionRun#getRunPairs()
	 */
	@Override
	public List<Pair<IProgramConfig, IDataConfig>> getClonedProgramAndDataConfigPairs() {
		return this.clonedProgramAndDataConfigPairs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IExecutionRun#getFixedParameterValues()
	 */
	@Override
	public Map<IProgramConfig, Map<IProgramParameter<?>, String>> getFixedParameterValues() {
		return fixedParameterValues;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IExecutionRun#getPostProcessors()
	 */
	@Override
	public List<IRunResultPostprocessor> getPostProcessors() {
		return this.postProcessors;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IExecutionRun#getProgramConfigs()
	 */
	@Override
	public List<IProgramConfig> getOriginalProgramConfigs() {
		return this.originalProgramConfigs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IExecutionRun#getDataConfigs()
	 */
	@Override
	public List<IDataConfig> getOriginalDataConfigs() {
		return this.originalDataConfigs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IExecutionRun#getQualityMeasures()
	 */
	@Override
	public List<IClusteringQualityMeasure> getQualityMeasures() {
		return this.qualityMeasures;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.wiwie.wiutils.utils.RepositoryObject#notify(utils.RepositoryEvent)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.IExecutionRun#notify(de.clusteval.framework.repository.
	 * RepositoryEvent)
	 */
	@Override
	public void notify(RepositoryEvent e) throws RegisterException {
		if (e instanceof RepositoryReplaceEvent) {
			RepositoryReplaceEvent event = (RepositoryReplaceEvent) e;
			if (event.getOld().equals(this))
				super.notify(event);
			else {
				if (event.getOld() instanceof DataConfig) {
					event.getOld().removeListener(this);
					int ind = this.originalDataConfigs.indexOf(event.getOld());
					this.log.info("Run " + this.getName()
							+ ": DataConfig reloaded due to modifications in filesystem");
					event.getReplacement().addListener(this);
					this.originalDataConfigs.set(ind,
							(DataConfig) event.getReplacement());

					initClonedProgramAndDataConfigPairs(originalProgramConfigs,
							originalDataConfigs);
				} else if (event.getOld() instanceof ProgramConfig) {
					event.getOld().removeListener(this);
					int ind = this.originalProgramConfigs
							.indexOf(event.getOld());
					this.log.info("Run " + this.getName()
							+ ": ProgramConfig reloaded due to modifications in filesystem");
					event.getReplacement().addListener(this);
					this.originalProgramConfigs.set(ind,
							(ProgramConfig) event.getReplacement());

					initClonedProgramAndDataConfigPairs(originalProgramConfigs,
							originalDataConfigs);
				}
			}
		} else if (e instanceof RepositoryRemoveEvent) {
			RepositoryRemoveEvent event = (RepositoryRemoveEvent) e;
			if (event.getRemovedObject().equals(this))
				super.notify(event);
			else {
				if (this.originalProgramConfigs
						.contains(event.getRemovedObject())) {
					event.getRemovedObject().removeListener(this);
					this.log.info("Run " + this
							+ ": Removed, because ProgramConfig "
							+ event.getRemovedObject() + " was removed.");
					RepositoryRemoveEvent newEvent = new RepositoryRemoveEvent(
							this);
					this.notify(newEvent);
					this.unregister();
				} else if (this.originalDataConfigs
						.contains(event.getRemovedObject())) {
					event.getRemovedObject().removeListener(this);
					this.log.info("Run " + this
							+ ": Removed, because DataConfig "
							+ event.getRemovedObject() + " was removed.");
					RepositoryRemoveEvent newEvent = new RepositoryRemoveEvent(
							this);
					this.notify(newEvent);
					this.unregister();
				} else if (this.qualityMeasures
						.contains(event.getRemovedObject())) {
					event.getRemovedObject().removeListener(this);
					this.log.info("Run " + this
							+ ": Removed, because ClusteringQualityMeasure "
							+ event.getRemovedObject().getClass()
									.getSimpleName()
							+ " was removed.");
					RepositoryRemoveEvent newEvent = new RepositoryRemoveEvent(
							this);
					this.notify(newEvent);
					this.unregister();
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.IExecutionRun#hasMaxExecutionTime(de.clusteval.program.
	 * IProgramConfig)
	 */
	@Override
	public boolean hasMaxExecutionTime(final IProgramConfig pc) {
		return this.maxExecutionTimes.containsKey(pc.getName());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.IExecutionRun#getMaxExecutionTime(de.clusteval.program.
	 * IProgramConfig)
	 */
	@Override
	public int getMaxExecutionTime(final IProgramConfig pc) {
		if (!this.maxExecutionTimes.containsKey(pc.getName()))
			return -1;
		return this.maxExecutionTimes.get(pc.getName());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#
	 * ensureVersionsForAllComponents(java.lang.String)
	 */
	@Override
	protected String ensureVersionsForAllComponents(String fileContents) {
		String newfileContents = super.ensureVersionsForAllComponents(
				fileContents);

		for (IProgramConfig pc : this.originalProgramConfigs) {
			String old = pc.getName();
			String withVersion = pc.toString();
			newfileContents = newfileContents
					.replaceAll(String.format("%s(?!\\:)", old), withVersion);
		}

		for (IDataConfig dc : this.originalDataConfigs) {
			String old = dc.getName();
			String withVersion = dc.toString();
			newfileContents = newfileContents
					.replaceAll(String.format("%s(?!\\:)", old), withVersion);
		}

		for (IClusteringQualityMeasure r : qualityMeasures) {
			newfileContents = ensureVersionForComponent(newfileContents, r);
		}
		for (IRunResultPostprocessor r : postProcessors) {
			newfileContents = ensureVersionForComponent(newfileContents, r);
		}

		return newfileContents;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#asSerializable()
	 */
	@Override
	public SerializableExecutionRun<? extends IExecutionRun<?>> asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableExecutionRun) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.Run#dumpToFileHelper(org.apache.commons.configuration.
	 * HierarchicalINIConfiguration)
	 */
	@Override
	protected void dumpToFileHelper(HierarchicalINIConfiguration conf)
			throws RepositoryObjectDumpException {
		super.dumpToFileHelper(conf);

		this.dumpDataConfigsToFile(conf);
		this.dumpProgramConfigsToFile(conf);
		this.dumpQualityMeasureToFile(conf);
		this.dumpFixedParameterValuesToFile(conf);
	}

	protected void dumpFixedParameterValuesToFile(
			HierarchicalINIConfiguration conf) {
		for (int p = 0; p < this.originalProgramConfigs.size(); p++) {
			IProgramConfig pc = this.originalProgramConfigs.get(p);
			if (!fixedParameterValues.containsKey(pc))
				continue;
			for (Map.Entry<IProgramParameter<?>, String> paramValue : fixedParameterValues
					.get(pc).entrySet()) {
				SubnodeConfiguration section = conf.getSection(pc.toString());
				section.addProperty(paramValue.getKey().getName(),
						paramValue.getValue());
			}
		}
	}

	protected void dumpQualityMeasureToFile(HierarchicalINIConfiguration conf) {
		conf.addProperty("qualityMeasures",
				StringExt.paste(",", qualityMeasures));
	}

	protected void dumpProgramConfigsToFile(HierarchicalINIConfiguration conf) {
		conf.addProperty("programConfig",
				StringExt.paste(",", originalProgramConfigs));
	}

	protected void dumpDataConfigsToFile(HierarchicalINIConfiguration conf) {
		conf.addProperty("dataConfig",
				StringExt.paste(",", originalDataConfigs));
	}
}
