/**
 * 
 */
package de.clusteval.run;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.clusteval.cluster.quality.ISerializableClusteringQualityMeasure;
import de.clusteval.context.ISerializableContext;
import de.clusteval.data.ISerializableDataConfig;
import de.clusteval.data.randomizer.ISerializableDataRandomizer;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.program.ISerializableProgramConfig;
import de.clusteval.program.ParameterSet;
import de.clusteval.run.runresult.postprocessing.ISerializableRunResultPostprocessor;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * @author Christian Wiwie
 *
 */
public class SerializableRobustnessAnalysisRun<R extends IRobustnessAnalysisRun>
		extends
			SerializableClusteringRun<R>
		implements
			ISerializableRobustnessAnalysisRun<R> {

	public static Map<ISerializableProgramConfig<? extends IProgramConfig>, Map<IProgramParameter<?>, String>> createFixedEmptyParameterValues(
			final List<ISerializableProgramConfig<? extends IProgramConfig>> programConfigs) {
		Map<ISerializableProgramConfig<? extends IProgramConfig>, Map<IProgramParameter<?>, String>> parameterValues = new HashMap<>();

		for (ISerializableProgramConfig<? extends IProgramConfig> programConfig : programConfigs)
			parameterValues.put(programConfig, new HashMap<>());

		return parameterValues;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 6149844699807390371L;

	protected List<String> runResultIdentifiers;

	protected List<ParameterSet> distortionParams;

	protected int numberOfDistortedDataSets;

	protected ISerializableDataRandomizer randomizer;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 * @param context
	 * @param dataConfigs
	 * @param programConfigs
	 * @param postProcessors
	 * @param qualityMeasures
	 * @param runResultIdentifiers
	 * @param distortionParams
	 * @param numberOfDistortedDataSets
	 * @param randomizer
	 * @param maxExecutionTimes
	 */
	public SerializableRobustnessAnalysisRun(File absPath, String name,
			String version, ISerializableContext context,
			List<ISerializableDataConfig> dataConfigs,
			List<ISerializableProgramConfig<? extends IProgramConfig>> programConfigs,
			List<ISerializableRunResultPostprocessor> postProcessors,
			List<ISerializableClusteringQualityMeasure> qualityMeasures,
			List<String> runResultIdentifiers,
			List<ParameterSet> distortionParams, int numberOfDistortedDataSets,
			ISerializableDataRandomizer randomizer,
			final Map<String, Integer> maxExecutionTimes) {
		super(absPath, name, version, context, dataConfigs, programConfigs,
				postProcessors, qualityMeasures,
				createFixedEmptyParameterValues(programConfigs),
				maxExecutionTimes);
		this.runResultIdentifiers = runResultIdentifiers;
		this.distortionParams = distortionParams;
		this.numberOfDistortedDataSets = numberOfDistortedDataSets;
		this.randomizer = randomizer;
	}

	public SerializableRobustnessAnalysisRun(final R run)
			throws RepositoryObjectSerializationException {
		super(run);
		this.runResultIdentifiers = run.getRunResultIdentifiers();
		this.distortionParams = run.getDistortionParams();
		this.numberOfDistortedDataSets = run.getNumberOfDistortedDataSets();
		this.randomizer = run.getRandomizer().asSerializable();
	}

	/**
	 * @return the runResultIdentifiers
	 */
	public List<String> getRunResultIdentifiers() {
		return runResultIdentifiers;
	}

	/**
	 * @return the distortionParams
	 */
	public List<ParameterSet> getDistortionParams() {
		return distortionParams;
	}

	/**
	 * @return the numberOfDistortedDataSets
	 */
	public int getNumberOfDistortedDataSets() {
		return numberOfDistortedDataSets;
	}

	/**
	 * @return the randomizer
	 */
	public ISerializableDataRandomizer getRandomizer() {
		return randomizer;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.SerializableClusteringRun#deserializeInternal(de.
	 * clusteval.framework.repository.IRepository)
	 */
	@Override
	protected R deserializeInternal(IRepository repository)
			throws DeserializationException {

		return (R) new RobustnessAnalysisRun(repository,
				context.deserialize(repository), System.currentTimeMillis(),
				new File(FileUtils.buildPath(repository.getBasePath(IRun.class),
						this.name + ".v" + version + ".run")),
				runResultIdentifiers, deserializeProgramConfigs(repository),
				deserializeDataConfigs(repository),
				deserializeDataConfigs(repository),
				deserializeClusteringQualityMeasures(repository),
				deserializeRunResultPostprocessor(repository),
				randomizer.deserialize(repository), distortionParams,
				numberOfDistortedDataSets, this.maxExecutionTimes);
	}
}
