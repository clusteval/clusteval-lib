/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration.HierarchicalINIConfiguration;

import de.clusteval.context.IContext;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryObjectDumpException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.threading.IRunSchedulerThread;
import de.clusteval.run.runnable.IRunAnalysisRunRunnable;
import de.clusteval.run.runnable.RunAnalysisRunRunnable;
import de.clusteval.run.statistics.IRunStatistic;
import dk.sdu.imada.compbio.utils.StringExt;

/**
 * A type of analysis run that conducts analyses of run results.
 * 
 * <p>
 * A run analysis run has a list of unique run identifiers in
 * {@link #uniqueRunResultIdentifiers} , that should be assessed during
 * execution of the run. Additionally they inherit a list of run statistics in
 * {@link AnalysisRun#statistics} that should be assessed for every run result
 * corresponding to a unique run identifier.
 * 
 * @author Christian Wiwie
 * 
 */
public class RunAnalysisRun
		extends
			AnalysisRun<IRunStatistic, IRunAnalysisRunRunnable>
		implements
			IRunAnalysisRun {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6182066955422593211L;
	/**
	 * A list of unique run identifiers, that should be assessed during
	 * execution of the run
	 */
	protected List<String> uniqueRunResultIdentifiers;

	/**
	 * @param repository
	 *            The repository this run should be registered at.
	 * @param context
	 * @param changeDate
	 *            The date this run was performed.
	 * @param absPath
	 *            The absolute path to the file on the filesystem that
	 *            corresponds to this run.
	 * @param uniqueRunIdentifiers
	 *            The list of unique run identifiers, that should be assessed
	 *            during execution of the run.
	 * @param statistics
	 *            The statistics that should be assessed for the objects of
	 *            analysis.
	 */
	public RunAnalysisRun(IRepository repository, final IContext context,
			long changeDate, File absPath, List<String> uniqueRunIdentifiers,
			List<IRunStatistic> statistics) {
		super(repository, context, changeDate, absPath, statistics);
		this.uniqueRunResultIdentifiers = uniqueRunIdentifiers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#register()
	 */
	@Override
	public boolean register() throws RegisterException {
		if (super.register()) {
			for (IRunStatistic statistic : this.statistics) {
				// added 21.03.2013
				statistic.register();
				statistic.addListener(this);
			}
			return true;
		}
		return false;
	}

	/**
	 * Copy constructor of run analysis runs.
	 * 
	 * @param other
	 *            The run analysis run to be cloned.
	 * @throws RegisterException
	 */
	protected RunAnalysisRun(final RunAnalysisRun other)
			throws RegisterException {
		super(other);

		this.uniqueRunResultIdentifiers = new ArrayList<String>();

		for (String s : other.uniqueRunResultIdentifiers)
			this.uniqueRunResultIdentifiers.add(s);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.AnalysisRun#cloneStatistics(java.util.List)
	 */
	@Override
	protected List<IRunStatistic> cloneStatistics(
			List<IRunStatistic> statistics) {
		final List<IRunStatistic> result = new ArrayList<IRunStatistic>();

		for (IRunStatistic st : statistics)
			result.add(st.clone());

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.Run#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IRunAnalysisRun#clone()
	 */
	@Override
	public RunAnalysisRun clone() {
		try {
			return new RunAnalysisRun(this);
		} catch (RegisterException e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.Run#terminate()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IRunAnalysisRun#terminate()
	 */
	@Override
	public boolean terminate() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.Run#getUpperLimitProgress()
	 */
	@Override
	protected long getUpperLimitProgress() {
		return this.statistics.size() * 100;
	}

	@Override
	protected IRunAnalysisRunRunnable createAndScheduleRunnableForRunPair(
			IRunSchedulerThread runScheduler, int p) {

		/*
		 * We only operate on this copy, in order to avoid multithreading
		 * problems.
		 */
		RunAnalysisRun runCopy = this.clone();

		String uniqueRunIdentifier = this.getUniqueRunAnalysisRunIdentifiers()
				.get(p);

		/*
		 * Start a thread with the invocation line and a path to the log file.
		 * The RunThread redirects all the output of the pr ogram into the
		 * logFile.
		 */
		final IRunAnalysisRunRunnable t = new RunAnalysisRunRunnable(
				runScheduler, runCopy, runIdentString, false,
				uniqueRunIdentifier, runCopy.getStatistics());
		return t;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.Run#getNumberOfRunRunnables()
	 */
	@Override
	protected int getNumberOfRunRunnables() {
		return this.getUniqueRunAnalysisRunIdentifiers().size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.Run#createAndAddRunnableForResumePair(framework.RunScheduler,
	 * int)
	 */
	@Override
	protected IRunAnalysisRunRunnable createAndScheduleRunnableForResumePair(
			IRunSchedulerThread runScheduler, int p) {

		/*
		 * We only operate on this copy, in order to avoid multithreading
		 * problems.
		 */
		// changed 13.02.2013
		// RunAnalysisRun runCopy = this.clone();
		RunAnalysisRun runCopy = this;

		String uniqueRunIdentifier = this.getUniqueRunAnalysisRunIdentifiers()
				.get(p);

		/*
		 * Start a thread with the invocation line and a path to the log file.
		 * The RunThread redirects all the output of the pr ogram into the
		 * logFile.
		 */
		final IRunAnalysisRunRunnable t = new RunAnalysisRunRunnable(
				runScheduler, runCopy, runIdentString, true,
				uniqueRunIdentifier, runCopy.getStatistics());
		return t;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.IRunAnalysisRun#getUniqueRunAnalysisRunIdentifiers()
	 */
	@Override
	public List<String> getUniqueRunAnalysisRunIdentifiers() {
		return this.uniqueRunResultIdentifiers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.AnalysisRun#asSerializable()
	 */
	@Override
	public SerializableRunAnalysisRun<? extends IRunAnalysisRun> asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableRunAnalysisRun<? extends IRunAnalysisRun>) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.AnalysisRun#asSerializable()
	 */
	@Override
	public SerializableRunAnalysisRun<? extends IRunAnalysisRun> asSerializableInternal()
			throws RepositoryObjectSerializationException {
		return new SerializableRunAnalysisRun(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.Run#dumpToFileHelper(org.apache.commons.configuration.
	 * HierarchicalINIConfiguration)
	 */
	@Override
	protected void dumpToFileHelper(HierarchicalINIConfiguration conf)
			throws RepositoryObjectDumpException {
		super.dumpToFileHelper(conf);

		this.dumpRunResultIdentifiers(conf);
		this.dumpRunStatisticsToFile(conf);
	}

	protected void dumpRunResultIdentifiers(HierarchicalINIConfiguration conf) {
		conf.addProperty("uniqueRunIdentifiers",
				StringExt.paste(",", uniqueRunResultIdentifiers));
	}

	protected void dumpRunStatisticsToFile(HierarchicalINIConfiguration conf) {
		conf.addProperty("runStatistics", StringExt.paste(",", statistics));
	}
}
