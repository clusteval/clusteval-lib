/**
 * 
 */
package de.clusteval.run;

import java.io.File;
import java.util.List;
import java.util.Map;

import de.clusteval.context.IContext;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.SerializableWrapperRepositoryObject;
import de.clusteval.run.runnable.IRunRunnable;
import dk.sdu.imada.compbio.utils.Pair;
import dk.sdu.imada.compbio.utils.ProgressPrinter;

public abstract class SerializableRun<R extends IRun>
		extends
			SerializableWrapperRepositoryObject<R>
		implements
			ISerializableRun<R> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4885072693620240123L;
	protected ISerializableWrapperRepositoryObject<IContext> context;

	// TODO: do we need runResults and runRunnables in serialized version?

	/**
	 * @param run
	 * @throws RepositoryObjectSerializationException
	 */
	public SerializableRun(final R run)
			throws RepositoryObjectSerializationException {
		super(run);
		this.context = run.getContext() != null
				? run.getContext().asSerializable()
				: null;
	}

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 * @param context
	 */
	public SerializableRun(File absPath, String name, String version,
			ISerializableWrapperRepositoryObject<IContext> context) {
		super(absPath, name, version);
		this.context = context;
	}

	/**
	 * @return the context
	 */
	@Override
	public ISerializableWrapperRepositoryObject<IContext> getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ISerializableRun#getEstimatedRemainingCPUTimeInMs()
	 */
	@Override
	public long getEstimatedRemainingCPUTimeInMs() {
		return this.wrappedComponent.getEstimatedRemainingCPUTimeInMs();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ISerializableRun#getLogFilePath()
	 */
	@Override
	public String getLogFilePath() {
		return this.wrappedComponent.getLogFilePath();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ISerializableRun#getOptimizationStatus()
	 */
	@Override
	public Map<Pair<String, String>, Pair<Double, Map<String, Pair<Map<String, String>, String>>>> getOptimizationStatus() {
		return this.wrappedComponent.getOptimizationStatus();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ISerializableRun#getPercentFinished()
	 */
	@Override
	public float getPercentFinished() {
		return this.wrappedComponent.getPercentFinished();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ISerializableRun#getRunIdentificationString()
	 */
	@Override
	public String getRunIdentificationString() {
		return this.wrappedComponent.getRunIdentificationString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ISerializableRun#getStartTime()
	 */
	@Override
	public long getStartTime() {
		return this.wrappedComponent.getStartTime();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ISerializableRun#getStatus()
	 */
	@Override
	public RUN_STATUS getStatus() {
		return this.wrappedComponent.getStatus();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ISerializableRun#getWarningExceptions()
	 */
	@Override
	public Map<IRunRunnable, List<Exception>> getWarningExceptions() {
		return this.wrappedComponent.getWarningExceptions();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ISerializableRun#getRunRunnables()
	 */
	@Override
	public List<IRunRunnable> getRunRunnables() {
		return this.wrappedComponent.getRunRunnables();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ISerializableRun#getProgress()
	 */
	@Override
	public ProgressPrinter getProgress() {
		return this.wrappedComponent.getProgress();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.SerializableWrapper#deserializeInternal
	 * (de.clusteval.framework.repository.IRepository)
	 */
	@Override
	protected R deserializeInternal(IRepository repository)
			throws DeserializationException {
		try {
			return (R) repository.getStaticObjectWithNameAndVersion(IRun.class,
					name + ":" + version);
		} catch (ObjectNotRegisteredException
				| ObjectVersionNotRegisteredException e) {
			e.printStackTrace();
		}
		return null;
	}
}