/**
 * 
 */
package de.clusteval.run;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import de.clusteval.context.ISerializableContext;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.utils.ISerializableStatistic;
import de.clusteval.utils.IStatistic;

/**
 * @author Christian Wiwie
 *
 */
public abstract class SerializableAnalysisRun<R extends IAnalysisRun<? extends IStatistic, ?>>
		extends
			SerializableRun<R>
		implements
			ISerializableAnalysisRun<R> {

	protected List<ISerializableStatistic> statistics;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 * @param context
	 * @param statistics
	 */
	public SerializableAnalysisRun(File absPath, String name, String version,
			ISerializableContext context,
			List<ISerializableStatistic> statistics) {
		super(absPath, name, version, context);
		this.statistics = statistics;
	}

	public SerializableAnalysisRun(final R run)
			throws RepositoryObjectSerializationException {
		super(run);

		this.statistics = new ArrayList<>();
		if (run.getStatistics() != null)
			for (IStatistic s : run.getStatistics())
				this.statistics.add(s != null ? s.asSerializable() : null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ISerializableAnalysisRun#getStatistics()
	 */
	@Override
	public List<ISerializableStatistic> getStatistics() {
		return statistics;
	}

	protected List<IStatistic> deserializeStatistics(IRepository repository)
			throws DeserializationException {
		List<IStatistic> statistics = new ArrayList<>();
		for (ISerializableStatistic<IStatistic> s : this.statistics)
			statistics.add(s.deserialize(repository));
		return statistics;
	}
}
