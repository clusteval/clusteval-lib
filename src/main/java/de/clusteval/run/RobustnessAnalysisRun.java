/**
 * 
 */
package de.clusteval.run;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.cli.Options;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalINIConfiguration;
import org.apache.commons.configuration.SubnodeConfiguration;

import de.clusteval.cluster.IClustering;
import de.clusteval.cluster.paramOptimization.IncompatibleParameterOptimizationMethodException;
import de.clusteval.cluster.paramOptimization.InvalidOptimizationParameterException;
import de.clusteval.cluster.paramOptimization.UnknownParameterOptimizationMethodException;
import de.clusteval.cluster.quality.ClusteringQualityMeasureValue;
import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.cluster.quality.IClusteringQualityMeasureValue;
import de.clusteval.cluster.quality.IClusteringQualitySet;
import de.clusteval.cluster.quality.UnknownClusteringQualityMeasureException;
import de.clusteval.context.IContext;
import de.clusteval.context.IncompatibleContextException;
import de.clusteval.context.UnknownContextException;
import de.clusteval.data.DataConfig;
import de.clusteval.data.DataConfigNotFoundException;
import de.clusteval.data.DataConfigurationException;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.DataSet;
import de.clusteval.data.dataset.DataSetConfigNotFoundException;
import de.clusteval.data.dataset.DataSetConfigurationException;
import de.clusteval.data.dataset.DataSetNotFoundException;
import de.clusteval.data.dataset.IDataSet;
import de.clusteval.data.dataset.IncompatibleDataSetConfigPreprocessorException;
import de.clusteval.data.dataset.NoDataSetException;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.dataset.type.UnknownDataSetTypeException;
import de.clusteval.data.distance.UnknownDistanceMeasureException;
import de.clusteval.data.goldstandard.GoldStandardConfigNotFoundException;
import de.clusteval.data.goldstandard.GoldStandardConfigurationException;
import de.clusteval.data.goldstandard.GoldStandardNotFoundException;
import de.clusteval.data.goldstandard.IGoldStandard;
import de.clusteval.data.goldstandard.format.UnknownGoldStandardFormatException;
import de.clusteval.data.preprocessing.InvalidDataPreprocessorOptionsException;
import de.clusteval.data.preprocessing.UnknownDataPreprocessorException;
import de.clusteval.data.randomizer.DataRandomizeException;
import de.clusteval.data.randomizer.IDataRandomizer;
import de.clusteval.data.randomizer.UnknownDataRandomizerException;
import de.clusteval.data.statistics.UnknownDataStatisticException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.InvalidRepositoryException;
import de.clusteval.framework.repository.NoRepositoryFoundException;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryCouldNotBeMigratedException;
import de.clusteval.framework.repository.RepositoryObjectDumpException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.config.RepositoryConfigNotFoundException;
import de.clusteval.framework.repository.config.RepositoryConfigurationException;
import de.clusteval.framework.repository.parse.ParseException;
import de.clusteval.framework.repository.parse.Parser;
import de.clusteval.framework.threading.IRunSchedulerThread;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.program.NoOptimizableProgramParameterException;
import de.clusteval.program.ParameterSet;
import de.clusteval.program.UnknownParameterType;
import de.clusteval.program.UnknownProgramParameterException;
import de.clusteval.program.UnknownProgramTypeException;
import de.clusteval.program.r.UnknownRProgramException;
import de.clusteval.run.runnable.IRobustnessAnalysisRunRunnable;
import de.clusteval.run.runnable.RobustnessAnalysisRunRunnable;
import de.clusteval.run.runresult.ClusteringRunResult;
import de.clusteval.run.runresult.IRunResult;
import de.clusteval.run.runresult.ParameterOptimizationResult;
import de.clusteval.run.runresult.RunResult;
import de.clusteval.run.runresult.RunResultParseException;
import de.clusteval.run.runresult.format.UnknownRunResultFormatException;
import de.clusteval.run.runresult.postprocessing.IRunResultPostprocessor;
import de.clusteval.run.runresult.postprocessing.UnknownRunResultPostprocessorException;
import de.clusteval.run.statistics.UnknownRunDataStatisticException;
import de.clusteval.run.statistics.UnknownRunStatisticException;
import de.clusteval.utils.InvalidConfigurationFileException;
import dk.sdu.imada.compbio.file.FileUtils;
import dk.sdu.imada.compbio.utils.StringExt;
import dk.sdu.imada.compbio.utils.Triple;

/**
 * @author Christian Wiwie
 *
 */
public class RobustnessAnalysisRun
		extends
			ClusteringRun<IRobustnessAnalysisRunRunnable>
		implements
			IRobustnessAnalysisRun {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4789364756606080174L;

	public static Map<IProgramConfig, Map<IProgramParameter<?>, String>> createFixedEmptyParameterValues(
			final List<IProgramConfig> programConfigs) {
		Map<IProgramConfig, Map<IProgramParameter<?>, String>> parameterValues = new HashMap<>();

		for (IProgramConfig programConfig : programConfigs)
			parameterValues.put(programConfig, new HashMap<>());

		return parameterValues;
	}

	protected transient List<IDataConfig> originalDataConfigs;

	/**
	 * A list of unique run identifiers, that should be assessed during
	 * execution of the run
	 */
	protected List<String> runResultIdentifiers;

	protected transient IDataRandomizer randomizer;

	protected List<ParameterSet> distortionParams;

	protected int numberOfDistortedDataSets;

	/**
	 * @param repository
	 *            The repository this run should be registered at.
	 * @param context
	 * @param changeDate
	 *            The date this run was performed.
	 * @param absPath
	 *            The absolute path to the file on the filesystem that
	 *            corresponds to this run.
	 * @param uniqueRunIdentifiers
	 *            The list of unique run identifiers, that should be assessed
	 *            during execution of the run.
	 * @param programConfigs
	 * @param dataConfigs
	 * @param originalDataConfigs
	 * @param qualityMeasures
	 * @param postProcessors
	 * @param randomizer
	 * @param randomizerParams
	 * @param numberOfRandomizedDataSets
	 * @param maxExecutionTimes
	 */
	public RobustnessAnalysisRun(IRepository repository, final IContext context,
			long changeDate, File absPath, List<String> uniqueRunIdentifiers,
			List<IProgramConfig> programConfigs, List<IDataConfig> dataConfigs,
			List<IDataConfig> originalDataConfigs,
			List<IClusteringQualityMeasure> qualityMeasures,
			final List<IRunResultPostprocessor> postProcessors,
			final IDataRandomizer randomizer,
			final List<ParameterSet> randomizerParams,
			final int numberOfRandomizedDataSets,
			final Map<String, Integer> maxExecutionTimes) {
		// the parameter values are just dummies; they will be parsed from the
		// uniqueRunIdentifiers later and override these values here;
		// TODO: Robustness analysis runs should probably not inherit from
		// ClusteringRun - maybe not even ExecutionRun;
		super(repository, context, changeDate, absPath, programConfigs,
				dataConfigs, qualityMeasures,
				createFixedEmptyParameterValues(programConfigs), postProcessors,
				maxExecutionTimes);
		this.runResultIdentifiers = uniqueRunIdentifiers;

		this.randomizer = randomizer;
		this.distortionParams = randomizerParams;
		this.numberOfDistortedDataSets = numberOfRandomizedDataSets;
		this.originalDataConfigs = originalDataConfigs;
	}

	/**
	 * Copy constructor of run analysis runs.
	 * 
	 * @param other
	 *            The run analysis run to be cloned.
	 */
	protected RobustnessAnalysisRun(final RobustnessAnalysisRun other) {
		super(other);

		this.runResultIdentifiers = new ArrayList<String>();

		for (String s : other.runResultIdentifiers)
			this.runResultIdentifiers.add(s);

		this.randomizer = other.randomizer.clone();
		this.distortionParams = new ArrayList<ParameterSet>();
		for (ParameterSet paramSet : other.distortionParams)
			this.distortionParams.add(paramSet.clone());
		this.numberOfDistortedDataSets = other.numberOfDistortedDataSets;
		this.originalDataConfigs = new ArrayList<IDataConfig>(
				other.originalDataConfigs);
		this.originalDataConfigs = new ArrayList<IDataConfig>(
				other.originalDataConfigs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ClusteringRun#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IRobustnessAnalysisRun#clone()
	 */
	@Override
	public RobustnessAnalysisRun clone() {
		return new RobustnessAnalysisRun(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.Run#beforeResume(java.lang.String)
	 */
	@Override
	protected void beforeResume(String runIdentString)
			throws RunInitializationException {
		super.beforeResume(runIdentString);

		this.log.info("Finding best parameters in run results");
		// find best parameters in run results
		try {
			this.findBestParamsAndInitParameterValues(
					this.getRepository().getParent());

			// generate randomized data sets
			// the directory, the new data sets will be stored in
			String dataSetBasePath = this.getRepository().getParent()
					.getBasePath(IDataSet.class);
			File newDataSetDir = new File(FileUtils.buildPath(dataSetBasePath,
					this.getRunIdentificationString()));
			newDataSetDir.mkdir();
			// the directory, the new gold standards will be stored in
			String goldStandardBasePath = this.getRepository().getParent()
					.getBasePath(IGoldStandard.class);
			File newGoldStandardDir = new File(FileUtils.buildPath(
					goldStandardBasePath, this.getRunIdentificationString()));
			newGoldStandardDir.mkdir();

			Map<IDataConfig, List<IDataConfig>> newDataConfigs = new HashMap<IDataConfig, List<IDataConfig>>();

			Options options = this.randomizer.getAllOptions();

			// generate those randomized data sets which are not generated yet
			for (IDataConfig dataConfig : this.originalDataConfigs) {
				this.log.info(
						"... for data config '" + dataConfig.getName() + "'");
				newDataConfigs.put(dataConfig, new ArrayList<IDataConfig>());
				for (ParameterSet paramSet : this.distortionParams) {

					for (int i = 1; i <= this.numberOfDistortedDataSets; i++) {
						List<String> params = new ArrayList<String>();
						for (String param : paramSet.keySet()) {
							if (options.hasOption(param)) {
								params.add("-" + param);
								params.add(paramSet.get(param));
							}
						}
						params.add("-dataConfig");
						params.add(dataConfig.getName());
						params.add("-uniqueId");
						params.add(this.runIdentString + "_" + i);
						try {
							IDataConfig newDataConfig = this.randomizer
									.randomize(params.toArray(new String[0]),
											true);

							File targetDataSetFile = new File(FileUtils
									.buildPath(newDataSetDir.getAbsolutePath(),
											newDataConfig.getDatasetConfig()
													.getDataSet()
													.getMinorName()));
							File targetGoldStandardFile = new File(
									FileUtils.buildPath(
											newGoldStandardDir
													.getAbsolutePath(),
											newDataConfig
													.getGoldstandardConfig()
													.getGoldstandard()
													.getMinorName()));

							if (!targetDataSetFile.exists()
									|| !targetGoldStandardFile.exists()) {
								newDataConfig.getDatasetConfig().getDataSet()
										.moveTo(targetDataSetFile);
								newDataConfig.getGoldstandardConfig()
										.getGoldstandard()
										.moveTo(targetGoldStandardFile);
								newDataConfigs.get(dataConfig)
										.add(newDataConfig);
							} else {
								this.log.info(
										"Randomized data config existed; using old data");
								new File(newDataConfig.getDatasetConfig()
										.getDataSet().getAbsolutePath())
												.delete();
								new File(newDataConfig.getGoldstandardConfig()
										.getGoldstandard().getAbsolutePath())
												.delete();

								DataSet ds = (DataSet) this.getRepository()
										.getParent()
										.getStaticObjectWithNameAndVersion(
												IDataSet.class,
												targetDataSetFile
														.getParentFile()
														.getName() + "/"
														+ targetDataSetFile
																.getName());

								newDataConfig.getDatasetConfig().setDataSet(ds);
								// newDataConfig.getDatasetConfig().dumpToFile();
								newDataConfig.getGoldstandardConfig()
										.setGoldStandard(Parser.parseFromFile(
												IGoldStandard.class,
												targetGoldStandardFile));
								// newDataConfig.getGoldstandardConfig()
								// .dumpToFile();
								newDataConfigs.get(dataConfig)
										.add((DataConfig) this.getRepository()
												.getParent()
												.getStaticObjectWithNameAndVersion(
														IDataConfig.class,
														newDataConfig
																.getName()));
							}

						} catch (DataRandomizeException e) {
							throw new RunInitializationException(e);
						}
					}
				}
			}

			this.originalDataConfigs = this.originalDataConfigs;
			this.originalDataConfigs = new ArrayList<IDataConfig>();
			for (IDataConfig dc : this.originalDataConfigs)
				this.originalDataConfigs.addAll(newDataConfigs.get(dc));

			this.log.info(
					"Get dataconfigs corresponding to original data configs");

			// override the old run pairs from constructor
			initClonedProgramAndDataConfigPairs(originalProgramConfigs,
					originalDataConfigs);
		} catch (Exception e) {
			throw new RunInitializationException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.Run#afterResume(java.lang.String)
	 */
	@Override
	protected void afterResume(String runIdentString) {
		this.createAnalysesDirectory();

		super.afterResume(runIdentString);
	}

	/**
	 * 
	 */
	protected void createAnalysesDirectory() {
		FileUtils.delete(new File(FileUtils.buildPath(
				this.repository.getBasePath(IRunResult.class),
				this.getRunIdentificationString(), "analyses")));
		new File(FileUtils.buildPath(
				this.repository.getBasePath(IRunResult.class),
				this.getRunIdentificationString(), "analyses")).mkdir();

		Set<String> paths = new HashSet<String>();

		for (int i = 0; i < this.results.size(); i++) {
			ClusteringRunResult result = (ClusteringRunResult) this.results
					.get(i);
			try {
				result.loadIntoMemory();
				IClusteringQualitySet quals = result.getClustering()
						.getQualities();
				result.unloadFromMemory();

				de.clusteval.program.IProgramConfig programConfig = result
						.getProgramConfig();
				IDataConfig dataConfig = result.getDataConfig();

				String resultPath = FileUtils.buildPath(
						this.repository.getBasePath(IRunResult.class),
						this.getRunIdentificationString(), "analyses",
						programConfig.getName() + ".robustness");

				// new File(resultPath).delete();

				StringBuilder sb = new StringBuilder();
				if (!paths.contains(resultPath)) {
					sb.append("DataConfig");
					sb.append("\t");
					// first time we write into this file
					for (IClusteringQualityMeasure measure : this.qualityMeasures) {
						sb.append(measure.getClass().getSimpleName());
						sb.append("\t");
					}
					sb.deleteCharAt(sb.length() - 1);
					sb.append(System.getProperty("line.separator"));
				}

				sb.append(dataConfig.getName());
				sb.append("\t");
				for (IClusteringQualityMeasure measure : this.qualityMeasures) {
					sb.append(quals.get(measure));
					sb.append("\t");
				}
				sb.deleteCharAt(sb.length() - 1);
				sb.append(System.getProperty("line.separator"));
				FileUtils.appendStringToFile(resultPath, sb.toString());

				paths.add(resultPath);
			} catch (Exception e) {
				// just skip that run result when
				System.out.println(result.getAbsolutePath());
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.Run#beforePerform()
	 */
	@Override
	protected void beforePerform() throws RunInitializationException {
		this.log.info("Finding best parameters in run results");
		// find best parameters in run results
		try {
			this.findBestParamsAndInitParameterValues(this.getRepository());

			this.log.info("Generate randomized data sets...");
			// generate randomized data sets
			// the directory, the new data sets will be stored in
			String dataSetBasePath = this.getRepository()
					.getBasePath(IDataSet.class);
			File newDataSetDir = new File(FileUtils.buildPath(dataSetBasePath,
					this.getRunIdentificationString()));
			newDataSetDir.mkdir();
			// the directory, the new gold standards will be stored in
			String goldStandardBasePath = this.getRepository()
					.getBasePath(IGoldStandard.class);
			File newGoldStandardDir = new File(FileUtils.buildPath(
					goldStandardBasePath, this.getRunIdentificationString()));
			newGoldStandardDir.mkdir();

			Map<IDataConfig, List<IDataConfig>> newDataConfigs = new HashMap<IDataConfig, List<IDataConfig>>();

			Options options = this.randomizer.getAllOptions();

			// generate randomized data sets
			for (IDataConfig dataConfig : this.originalDataConfigs) {
				this.log.info(
						"... for data config '" + dataConfig.getName() + "'");
				newDataConfigs.put(dataConfig, new ArrayList<IDataConfig>());
				for (ParameterSet paramSet : this.distortionParams) {

					for (int i = 1; i <= this.numberOfDistortedDataSets; i++) {
						List<String> params = new ArrayList<String>();
						for (String param : paramSet.keySet()) {
							if (options.hasOption(param)) {
								params.add("-" + param);
								params.add(paramSet.get(param));
							}
						}
						params.add("-dataConfig");
						params.add(dataConfig.getName());
						params.add("-uniqueId");
						params.add(this.runIdentString + "_" + i);
						try {
							IDataConfig newDataConfig = this.randomizer
									.randomize(params.toArray(new String[0]));

							File targetDataSetFile = new File(FileUtils
									.buildPath(newDataSetDir.getAbsolutePath(),
											newDataConfig.getDatasetConfig()
													.getDataSet()
													.getMinorName()));
							File targetGoldStandardFile = new File(
									FileUtils.buildPath(
											newGoldStandardDir
													.getAbsolutePath(),
											newDataConfig
													.getGoldstandardConfig()
													.getGoldstandard()
													.getMinorName()));

							newDataConfig.getDatasetConfig().getDataSet()
									.moveTo(targetDataSetFile);
							newDataConfig.getGoldstandardConfig()
									.getGoldstandard()
									.moveTo(targetGoldStandardFile);

							newDataConfigs.get(dataConfig).add(newDataConfig);

						} catch (DataRandomizeException e) {
							throw new RunInitializationException(e);
						}
					}
				}
			}

			this.originalDataConfigs = this.originalDataConfigs;
			this.originalDataConfigs = new ArrayList<IDataConfig>();
			for (IDataConfig dc : this.originalDataConfigs)
				this.originalDataConfigs.addAll(newDataConfigs.get(dc));

			// override the old run pairs
			super.beforePerform();
			// initRunPairs(programConfigs, dataConfigs);
		} catch (Exception e) {
			throw new RunInitializationException(e);
		}
	}

	/**
	 * @throws UnknownDataRandomizerException
	 * @throws UnknownRunResultPostprocessorException
	 * @throws InterruptedException
	 * @throws IncompatibleContextException
	 * @throws IncompatibleDataSetConfigPreprocessorException
	 * @throws UnknownDataPreprocessorException
	 * @throws UnknownRunDataStatisticException
	 * @throws UnknownDataSetTypeException
	 * @throws RepositoryConfigurationException
	 * @throws RepositoryConfigNotFoundException
	 * @throws UnknownRunStatisticException
	 * @throws UnknownDistanceMeasureException
	 * @throws IncompatibleParameterOptimizationMethodException
	 * @throws UnknownRProgramException
	 * @throws UnknownProgramTypeException
	 * @throws UnknownDataStatisticException
	 * @throws RunException
	 * @throws InvalidOptimizationParameterException
	 * @throws NoRepositoryFoundException
	 * @throws InvalidRepositoryException
	 * @throws InvalidConfigurationFileException
	 * @throws UnknownProgramParameterException
	 * @throws NoOptimizableProgramParameterException
	 * @throws UnknownParameterOptimizationMethodException
	 * @throws InvalidRunModeException
	 * @throws UnknownClusteringQualityMeasureException
	 * @throws UnknownRunResultFormatException
	 * @throws IOException
	 * @throws UnknownParameterType
	 * @throws UnknownContextException
	 * @throws RegisterException
	 * @throws ConfigurationException
	 * @throws RunResultParseException
	 * @throws NumberFormatException
	 * @throws DataConfigNotFoundException
	 * @throws DataConfigurationException
	 * @throws NoDataSetException
	 * @throws GoldStandardConfigNotFoundException
	 * @throws DataSetConfigNotFoundException
	 * @throws DataSetNotFoundException
	 * @throws DataSetConfigurationException
	 * @throws GoldStandardConfigurationException
	 * @throws GoldStandardNotFoundException
	 * @throws UnknownGoldStandardFormatException
	 * @throws UnknownDataSetFormatException
	 * @throws InvalidDataPreprocessorOptionsException
	 * @throws RepositoryCouldNotBeMigratedException
	 * 
	 */
	protected void findBestParamsAndInitParameterValues(
			final IRepository repository) throws ParseException,
			RunResultParseException, InterruptedException {

		List<String> programConfigNames = new ArrayList<String>();
		for (de.clusteval.program.IProgramConfig programConfig : originalProgramConfigs)
			programConfigNames.add(programConfig.getName());
		List<String> dataConfigNames = new ArrayList<String>();
		for (IDataConfig dataConfig : this.originalDataConfigs)
			dataConfigNames.add(dataConfig.getName());

		Map<String, Map<String, Triple<List<ParameterSet>, IClusteringQualityMeasure, IClusteringQualityMeasureValue>>> bestParams = new HashMap<String, Map<String, Triple<List<ParameterSet>, IClusteringQualityMeasure, IClusteringQualityMeasureValue>>>();

		// get best parameters for each pair of program and dataset
		// from run results
		for (String runIdentifier : this.runResultIdentifiers) {
			this.log.info("... parsing run result '" + runIdentifier + "'");
			List<IRunResult> results = new ArrayList<IRunResult>();
			RunResult.parseFromRunResultFolder(repository,
					new File(FileUtils.buildPath(
							repository.getBasePath(IRunResult.class),
							runIdentifier)),
					results, false, false, false);
			for (IRunResult runResult : results) {
				if (runResult instanceof ParameterOptimizationResult) {
					this.log.info("...... " + runResult.getAbsolutePath());
					ParameterOptimizationResult paramOptResult = (ParameterOptimizationResult) runResult;
					paramOptResult.loadIntoMemory();
					de.clusteval.program.IProgramConfig pc = paramOptResult
							.getProgramConfig();
					IDataConfig dc = paramOptResult.getDataConfig();
					IClusteringQualityMeasure measure = paramOptResult.getRun()
							.getOptimizationCriterion();
					// ClusteringQualityMeasureValue min =
					// ClusteringQualityMeasureValue
					// .getForDouble(measure.getMinimum());
					// ClusteringQualityMeasureValue max =
					// ClusteringQualityMeasureValue
					// .getForDouble(measure.getMaximum());
					// ClusteringQualityMeasureValue def;
					// if (measure.isBetterThan(max, min))
					// def = min;
					// else
					// def = max;
					ClusteringQualityMeasureValue def = ClusteringQualityMeasureValue
							.getForDouble(Double.NaN);

					if (programConfigNames.contains(pc.getName())
							&& dataConfigNames.contains(dc.getName())) {
						if (!bestParams.containsKey(pc.getName()))
							bestParams.put(pc.getName(),
									new HashMap<String, Triple<List<ParameterSet>, IClusteringQualityMeasure, IClusteringQualityMeasureValue>>());
						if (!bestParams.get(pc.getName())
								.containsKey(dc.getName()))
							bestParams.get(pc.getName()).put(dc.getName(),
									new Triple<List<ParameterSet>, IClusteringQualityMeasure, IClusteringQualityMeasureValue>(
											new ArrayList<ParameterSet>(),
											measure, def));

						IClusteringQualityMeasureValue currentOpt = bestParams
								.get(pc.getName()).get(dc.getName()).getThird();
						Map<IClusteringQualityMeasure, Long> optParams = paramOptResult
								.getOptimalIterations();
						long bestIteration = optParams.get(measure);
						ParameterSet bestParamSet = paramOptResult
								.getParameterSets().get(bestIteration);
						IClustering cl = paramOptResult
								.getClustering(bestIteration);
						if (paramOptResult.get(bestIteration) == null)
							continue;
						IClusteringQualityMeasureValue newBestValue = paramOptResult
								.get(bestIteration).get(measure);

						if (measure.isBetterThan(newBestValue, currentOpt)) {
							// we found a better quality, so we empty
							// the list and add the new parameter sets
							List<ParameterSet> paramSets = bestParams
									.get(pc.getName()).get(dc.getName())
									.getFirst();
							paramSets.clear();
							paramSets.add(bestParamSet);

							bestParams.get(pc.getName()).get(dc.getName())
									.setThird(newBestValue);
						} else if (newBestValue.isTerminated() && newBestValue
								.getValue() == currentOpt.getValue()) {
							// we found a parameter set with the same
							// quality, add it to the list
							List<ParameterSet> paramSets = bestParams
									.get(pc.getName()).get(dc.getName())
									.getFirst();
							paramSets.add(bestParamSet);
						}
					}
					paramOptResult.unloadFromMemory();
				}
			}
		}

		for (String pc : bestParams.keySet()) {
			for (String dc : bestParams.get(pc).keySet()) {
				this.log.info(String.format("%s\t%s\t%s\n", pc, dc,
						bestParams.get(pc).get(dc).getFirst().get(0)));
			}
		}

		this.log.info(
				"Taking one parameter set for each pair of program config and data config");
		this.fixedParameterValues.clear();
		// TODO: for numerical parameter ranges, take the mean
		// for string parameters, just take any one
		for (de.clusteval.program.IProgramConfig pc : originalProgramConfigs) {
			for (IDataConfig dc : originalDataConfigs) {
				Map<IProgramParameter<? extends Object>, String> m = new HashMap<IProgramParameter<? extends Object>, String>();

				if (bestParams.containsKey(pc.getName()) && bestParams
						.get(pc.getName()).containsKey(dc.getName())) {
					Triple<List<ParameterSet>, IClusteringQualityMeasure, IClusteringQualityMeasureValue> params = bestParams
							.get(pc.getName()).get(dc.getName());

					if (params.getFirst().size() > 0) {
						for (String p : params.getFirst().get(0).keySet())
							m.put(pc.getParameterForName(p),
									params.getFirst().get(0).get(p));
					}
				}
				// add it several times, once for each randomized dataset
				// per data config
				for (int i = 0; i < this.numberOfDistortedDataSets; i++)
					for (int j = 0; j < this.distortionParams.size(); j++)
						this.fixedParameterValues.put(pc,
								new HashMap<IProgramParameter<? extends Object>, String>(
										m));
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.Run#afterPerform()
	 */
	@Override
	protected void afterPerform() {
		this.createAnalysesDirectory();

		super.afterPerform();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ClusteringRun#createRunRunnableFor(de.clusteval.
	 * framework.threading.IRunSchedulerThread, de.clusteval.run.ExecutionRun,
	 * de.clusteval.program.IProgramConfig, de.clusteval.data.IDataConfig,
	 * java.lang.String, boolean, java.util.Map)
	 */
	@Override
	protected <T extends ExecutionRun<IRobustnessAnalysisRunRunnable>> IRobustnessAnalysisRunRunnable createRunRunnableFor(
			IRunSchedulerThread runScheduler, T run,
			IProgramConfig programConfig, IDataConfig dataConfig,
			String runIdentString, boolean isResume,
			Map<IProgramParameter<?>, String> runParams) {
		RobustnessAnalysisRunRunnable r = new RobustnessAnalysisRunRunnable(
				runScheduler, (IRobustnessAnalysisRun) run, programConfig,
				dataConfig, runIdentString, isResume, runParams);
		((Run) run).progress.addSubProgress(r.getProgressPrinter(), 10000);
		((Run) run).progressBeforeExecution
				.addSubProgress(r.getProgressPrinterBeforeExecution(), 10000);
		return r;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.ExecutionRun#createAndScheduleRunnableForResumePair(
	 * de.clusteval.framework.threading.RunSchedulerThread, int)
	 */
	@Override
	protected IRobustnessAnalysisRunRunnable createAndScheduleRunnableForResumePair(
			IRunSchedulerThread runScheduler, int p) {
		this.log.info(String.format("%s\t%s\t%s",
				this.clonedProgramAndDataConfigPairs.get(p).getFirst(),
				this.clonedProgramAndDataConfigPairs.get(p).getSecond(),
				this.fixedParameterValues
						.get(this.originalProgramConfigs.get(p))));
		return super.createAndScheduleRunnableForResumePair(runScheduler, p);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ExecutionRun#getRunParameterForRunPair(int)
	 */
	@Override
	protected Map<IProgramParameter<? extends Object>, String> getRunParameterForRunPair(
			int p) {
		// we have one parameter set for each run pair
		return this.fixedParameterValues
				.get(this.originalProgramConfigs.get(p));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.IRobustnessAnalysisRun#setOriginalDataConfigurations(
	 * java.util.List)
	 */
	@Override
	public void setOriginalDataConfigurations(
			final List<IDataConfig> dataConfigs) {
		this.originalDataConfigs = dataConfigs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.AnalysisRun#asSerializable()
	 */
	@Override
	public SerializableRobustnessAnalysisRun<? extends IRobustnessAnalysisRun> asSerializable()
			throws RepositoryObjectSerializationException {
		return new SerializableRobustnessAnalysisRun(this);
	}

	/**
	 * @return the uniqueRunResultIdentifiers
	 */
	@Override
	public List<String> getRunResultIdentifiers() {
		return runResultIdentifiers;
	}

	/**
	 * @return the distortionParams
	 */
	@Override
	public List<ParameterSet> getDistortionParams() {
		return distortionParams;
	}

	/**
	 * @return the numberOfDistortedDataSets
	 */
	@Override
	public int getNumberOfDistortedDataSets() {
		return numberOfDistortedDataSets;
	}

	/**
	 * @return the randomizer
	 */
	@Override
	public IDataRandomizer getRandomizer() {
		return randomizer;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ClusteringRun#dumpToFileHelper(org.apache.commons.
	 * configuration.HierarchicalINIConfiguration)
	 */
	@Override
	protected void dumpToFileHelper(HierarchicalINIConfiguration conf)
			throws RepositoryObjectDumpException {
		super.dumpToFileHelper(conf);

		this.dumpRunResultIdentifiers(conf);

		conf.addProperty("randomizer", randomizer);
		conf.addProperty("numberOfRandomizedDataSets",
				numberOfDistortedDataSets);

		int randomizerCounter = 1;
		for (ParameterSet paramSet : this.distortionParams) {
			SubnodeConfiguration section = conf
					.getSection(randomizer + "_" + randomizerCounter);
			for (Entry<String, String> paramValue : paramSet.entrySet()) {
				if (!paramValue.getValue().isEmpty()) {
					section.addProperty(paramValue.getKey(),
							paramValue.getValue());
				}
			}
			randomizerCounter++;
		}
	}

	protected void dumpRunResultIdentifiers(HierarchicalINIConfiguration conf) {
		conf.addProperty("uniqueRunIdentifiers",
				StringExt.paste(",", runResultIdentifiers));
	}
}
