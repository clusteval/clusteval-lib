/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run.runresult;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.clusteval.cluster.ClusteringParseException;
import de.clusteval.cluster.FileBackedClustering;
import de.clusteval.cluster.IFileBackedClustering;
import de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod;
import de.clusteval.cluster.quality.ClusteringQualityMeasureValue;
import de.clusteval.cluster.quality.ClusteringQualitySet;
import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.program.IProgramParameter;
import de.clusteval.program.ParameterSet;
import de.clusteval.run.IParameterOptimizationRun;
import de.clusteval.run.runresult.format.RunResultFormat;
import de.clusteval.run.runresult.postprocessing.IRunResultPostprocessor;
import dk.sdu.imada.compbio.utils.StringExt;
import dk.sdu.imada.compbio.utils.parse.TextFileParser;

/**
 * @author Christian Wiwie
 * 
 */
public class ParameterOptimizationResultParser extends TextFileParser {

	protected List<IProgramParameter<?>> parameters = new ArrayList<IProgramParameter<?>>();
	protected List<IClusteringQualityMeasure> qualityMeasures = new ArrayList<IClusteringQualityMeasure>();
	protected IParameterOptimizationMethod method;
	protected IParameterOptimizationRun run;
	protected ParameterOptimizationResult tmpResult;
	protected boolean parseClusterings, storeClusterings;

	/**
	 * @param method
	 * @param run
	 * @param tmpResult
	 * @param absFilePath
	 * @param keyColumnIds
	 * @param valueColumnIds
	 * @param parseClusterings
	 * @param storeClusterings
	 * @throws IOException
	 */
	public ParameterOptimizationResultParser(
			final IParameterOptimizationMethod method,
			final IParameterOptimizationRun run,
			final ParameterOptimizationResult tmpResult,
			final String absFilePath, int[] keyColumnIds, int[] valueColumnIds,
			final boolean parseClusterings, final boolean storeClusterings)
			throws IOException {
		super(absFilePath, keyColumnIds, valueColumnIds);
		this.setLockTargetFile(true);
		this.method = method;
		this.run = run;
		this.tmpResult = tmpResult;
		this.parseClusterings = parseClusterings;
		this.storeClusterings = storeClusterings;
	}

	@SuppressWarnings("unused")
	@Override
	protected void processLine(String[] key, String[] value) {
		if (this.currentLine == 0) {
			/*
			 * Parse header line
			 */
			// iteration number in first column
			String[] paramSplit = StringExt.split(value[1], ",");
			for (String p : paramSplit)
				parameters
						.add(method.getProgramConfig().getParameterForName(p));
			for (int i = 2; i < value.length; i++) {
				String q = value[i];
				for (IClusteringQualityMeasure other : run.getQualityMeasures())
					if (other.getClass().getSimpleName().equals(q)) {
						qualityMeasures.add(other);
						break;
					}
			}
		} else {
			try {
				// duplicated parameter set -> skipped iteration
				if (value[0].contains("*")) {
					// we don't parse duplicated iterations
					return;
				}
				long iterationNumber = Long.valueOf(value[0]);
				ParameterSet paramSet = new ParameterSet();
				String[] paramSplit = StringExt.split(value[1], ",");
				for (int pos = 0; pos < paramSplit.length; pos++) {
					IProgramParameter<?> p = this.parameters.get(pos);
					paramSet.put(p.getName(), paramSplit[pos]);
				}

				ClusteringQualitySet qualitySet = new ClusteringQualitySet();

				String iterationId = iterationNumber + "";
				// find the file with the longest name with this prefix
				String clusteringFilePath = new File(this.getAbsoluteFilePath()
						.replace("results.qual.complete",
								iterationId + ".results.conv"))
										.getAbsolutePath();
				for (IRunResultPostprocessor postprocessor : run
						.getPostProcessors()) {
					String newPath = String.format("%s.%s", clusteringFilePath,
							postprocessor.getClass().getSimpleName());
					if (new File(newPath).exists()) {
						clusteringFilePath = newPath;
					} else
						break;
				}

				File absFile = new File(clusteringFilePath);
				for (int pos = 2; pos < value.length; pos++) {
					IClusteringQualityMeasure other = this.qualityMeasures
							.get(pos - 2);
					qualitySet.put(other, ClusteringQualityMeasureValue
							.parseFromString(value[pos]));
				}
				tmpResult.iterationToParameterSets.put(iterationNumber,
						paramSet);
				tmpResult.iterationNumbers.add(iterationNumber);

				IFileBackedClustering clustering = null;
				if (absFile.exists())
					clustering = FileBackedClustering.parseFromFile(
							method.getRepository(), absFile,
							RunResultFormat.parseFromString(run.getRepository(),
									"TabSeparatedRunResultFormat"),
							false).getSecond();

				tmpResult.put(iterationNumber, qualitySet, clustering);

				// added 20.08.2012
				if (parseClusterings) {
					// if (absFile.exists()) {
					try {
						if (storeClusterings && clustering != null)
							clustering.loadIntoMemory();
						return;
					} catch (ClusteringParseException e) {
						e.printStackTrace();
					}
				}
			} catch (Exception e) {
				this.log.warn(
						"An error occurred while parsing the parameter optimization result '{}': {}",
						this.getAbsoluteFilePath(), e.getMessage());
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.parse.TextFileParser#split(java.lang.String)
	 */
	@Override
	protected String[] split(String line) {
		return this.splitLines
				? StringExt.split(line, this.inSplit)
				: new String[]{line};
	}
}
