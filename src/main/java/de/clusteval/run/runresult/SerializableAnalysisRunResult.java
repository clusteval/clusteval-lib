/**
 * 
 */
package de.clusteval.run.runresult;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.run.IRun;
import de.clusteval.run.ISerializableRun;
import de.clusteval.utils.IObjectToBeSerialized;
import de.clusteval.utils.ISerializableStatistic;
import de.clusteval.utils.IStatistic;

/**
 * @author Christian Wiwie
 *
 * @param <O>
 * @param <S>
 * @param <R>
 */
public abstract class SerializableAnalysisRunResult<O extends IObjectToBeSerialized, S extends IStatistic, R extends IAnalysisRunResult<O, S>>
		extends
			SerializableRunResult<R>
		implements
			ISerializableAnalysisRunResult<O, S, R> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6472009418761456472L;
	protected Map<O, Map<ISerializableStatistic<S>, String>> statisticValues;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 * @param run
	 * @param runIdentString
	 */
	public SerializableAnalysisRunResult(File absPath, String name,
			String version, ISerializableRun<? extends IRun> run,
			String runIdentString) {
		super(absPath, name, version, run, runIdentString);
		this.statisticValues = new HashMap<>();
	}

	/**
	 * @param wrappedComponent
	 * @throws RepositoryObjectSerializationException
	 */
	public SerializableAnalysisRunResult(R wrappedComponent)
			throws RepositoryObjectSerializationException {
		super(wrappedComponent);
		this.statisticValues = new HashMap<>();
		for (O o : wrappedComponent.getStatisticValues().keySet()) {
			Map<ISerializableStatistic<S>, String> l = new HashMap<>();
			for (S s : wrappedComponent.getStatisticValues().get(o).keySet())
				l.put(s.asSerializable(),
						wrappedComponent.getStatisticValues().get(o).get(s));
			this.statisticValues.put(o, l);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runresult.ISerializableAnalysisRunResult#getStatistics()
	 */
	@Override
	public Map<O, Map<ISerializableStatistic<S>, String>> getStatistics() {
		return statisticValues;
	}

}