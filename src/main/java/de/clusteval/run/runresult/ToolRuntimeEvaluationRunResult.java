/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
package de.clusteval.run.runresult;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.data.IDataConfig;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.InvalidRepositoryException;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryAlreadyExistsException;
import de.clusteval.framework.repository.RepositoryCouldNotBeMigratedException;
import de.clusteval.framework.repository.RepositoryVersionTooNewException;
import de.clusteval.framework.repository.RepositoryVersionTooOldException;
import de.clusteval.framework.repository.RunResultRepository;
import de.clusteval.framework.repository.config.RepositoryConfigNotFoundException;
import de.clusteval.framework.repository.config.RepositoryConfigurationException;
import de.clusteval.framework.repository.db.DatabaseException;
import de.clusteval.framework.repository.parse.ParseException;
import de.clusteval.framework.repository.parse.Parser;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.ProgramConfig;
import de.clusteval.run.ClusteringRun;
import de.clusteval.run.IRun;
import de.clusteval.run.IToolRuntimeEvaluationRun;
import de.clusteval.run.runresult.format.IRunResultFormat;
import de.clusteval.run.runresult.format.RunResultFormat;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * The Class ClusteringResult.
 * 
 * @author Christian Wiwie
 * 
 */
public class ToolRuntimeEvaluationRunResult extends ClusteringRunResult
		implements
			IToolRuntimeEvaluationRunResult {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6255931084330750577L;

	/**
	 * @param repository
	 * @param changeDate
	 * @param absPath
	 * @param dataConfig
	 * @param programConfig
	 * @param format
	 * @param runIdentString
	 * @param run
	 */
	public ToolRuntimeEvaluationRunResult(IRepository repository,
			long changeDate, File absPath, IDataConfig dataConfig,
			IProgramConfig programConfig, IRunResultFormat format,
			String runIdentString, IToolRuntimeEvaluationRun<?> run) {
		super(repository, changeDate, absPath, dataConfig, programConfig,
				format, runIdentString, run);

		if (repository instanceof RunResultRepository) {
			repository = repository.getParent();
			try {
				programConfig = repository.getStaticObjectWithNameAndVersion(
						IProgramConfig.class, programConfig.toString());
				dataConfig = repository.getStaticObjectWithNameAndVersion(
						IDataConfig.class, dataConfig.toString());
			} catch (ObjectNotRegisteredException
					| ObjectVersionNotRegisteredException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * @param other
	 */
	public ToolRuntimeEvaluationRunResult(
			ToolRuntimeEvaluationRunResult other) {
		super(other);
	}

	/**
	 * @param repository
	 * @param run
	 * @param dataConfig
	 * @param programConfig
	 * @param completeFile
	 * @return The parameter optimization run result parsed from the given
	 *         runresult folder.
	 * @throws RegisterException
	 */
	public static ToolRuntimeEvaluationRunResult parseFromRunResultCompleteFile(
			IRepository repository, IToolRuntimeEvaluationRun run,
			final IDataConfig dataConfig, final IProgramConfig programConfig,
			final File completeFile, final boolean register)
			throws RegisterException, RunResultParseException {
		ToolRuntimeEvaluationRunResult result = null;
		if (completeFile.exists()) {
			result = new ToolRuntimeEvaluationRunResult(repository,
					completeFile.lastModified(), completeFile, dataConfig,
					programConfig,
					(RunResultFormat) programConfig.getOutputFormat(),
					completeFile.getParentFile().getParentFile().getName(),
					run);

			if (register) {
				/*
				 * Register after parsing
				 */
				result.loadIntoMemory();
				try {
					result.register();
				} finally {
					result.unloadFromMemory();
				}
			}
		}
		return result;
	}

	/**
	 * @param run
	 *            The run corresponding to the runresult folder.
	 * @param repository
	 *            The repository in which we want to register the runresult.
	 * @param runResultFolder
	 *            A file object referencing the runresult folder.
	 * @param result
	 *            The list of runresults this method fills.
	 * @return The parameter optimization run parsed from the runresult folder.
	 * @throws RegisterException
	 */
	public static IRun parseFromRunResultFolder(
			final IToolRuntimeEvaluationRun<?> run,
			final IRepository repository, final File runResultFolder,
			final List<IRunResult> result, final boolean register)
			throws RegisterException, RunResultParseException {

		File clusterFolder = new File(FileUtils
				.buildPath(runResultFolder.getAbsolutePath(), "clusters"));

		for (final IDataConfig dataConfig : run.getOriginalDataConfigs()) {
			for (final IProgramConfig programConfig : run
					.getOriginalProgramConfigs()) {
				File completeFile = new File(
						FileUtils.buildPath(clusterFolder.getAbsolutePath(),
								programConfig.toString(".v") + "_"
										+ dataConfig.toString(".v")
										+ ".results.qual.complete"));
				// fallback to file names without version
				if (!completeFile.exists()
						&& dataConfig.getVersion()
								.equals(new ComparableVersion("1"))
						&& programConfig.getVersion()
								.equals(new ComparableVersion("1"))) {

					String fallback = FileUtils
							.buildPath(clusterFolder.getAbsolutePath(),
									programConfig.getName() + "_"
											+ dataConfig.getName()
											+ ".results.qual.complete");
					if (new File(fallback).exists())
						completeFile = new File(fallback);
				}
				final ToolRuntimeEvaluationRunResult tmpResult = parseFromRunResultCompleteFile(
						repository, run, (IDataConfig) dataConfig,
						(ProgramConfig) programConfig, completeFile, register);
				if (tmpResult != null)
					result.add(tmpResult);
			}
		}

		return run;
	}

	/**
	 * @param parentRepository
	 * @param runResultFolder
	 * @param result
	 * @throws IOException
	 * @throws InvalidRepositoryException
	 * @throws RepositoryAlreadyExistsException
	 * @return The parameter optimization run result parsed from the given
	 *         runresult folder.
	 * @throws RepositoryConfigurationException
	 * @throws RepositoryConfigNotFoundException
	 * @throws RegisterException
	 * @throws NumberFormatException
	 * @throws InterruptedException
	 * @throws RepositoryVersionTooNewException
	 * @throws RepositoryVersionTooOldException
	 * @throws ParseException
	 * @throws RepositoryCouldNotBeMigratedException
	 */
	public static IRun parseFromRunResultFolder(
			final IRepository parentRepository, final File runResultFolder,
			final List<ExecutionRunResult> result, final boolean register)
			throws IOException, RepositoryVersionTooOldException,
			RepositoryVersionTooNewException, ParseException,
			RepositoryAlreadyExistsException, InvalidRepositoryException,
			RepositoryConfigNotFoundException, RepositoryConfigurationException,
			InterruptedException, RegisterException,
			RepositoryCouldNotBeMigratedException, RunResultParseException {

		IRepository childRepository;
		try {
			childRepository = new RunResultRepository(
					runResultFolder.getAbsolutePath(),
					(IRepository) parentRepository);
			childRepository.initialize();

			File runFile = null;
			File configFolder = new File(FileUtils
					.buildPath(runResultFolder.getAbsolutePath(), "configs"));
			if (!configFolder.exists())
				return null;
			for (File child : configFolder.listFiles())
				if (child.getName().endsWith(".run")) {
					runFile = child;
					break;
				}
			if (runFile == null)
				return null;
			final IRun run = Parser.parseRunFromFile(runFile);

			if (run instanceof ClusteringRun) {
				final IToolRuntimeEvaluationRun<?> paramRun = (IToolRuntimeEvaluationRun) run;

				File clusterFolder = new File(FileUtils.buildPath(
						runResultFolder.getAbsolutePath(), "clusters"));
				for (final IDataConfig dataConfig : paramRun
						.getOriginalDataConfigs()) {
					for (final IProgramConfig programConfig : paramRun
							.getOriginalProgramConfigs()) {
						File completeFile = new File(FileUtils.buildPath(
								clusterFolder.getAbsolutePath(),
								programConfig.toString(".v") + "_"
										+ dataConfig.toString(".v")
										+ ".results.qual.complete"));
						// fallback to file names without version
						if (!completeFile.exists()
								&& dataConfig.getVersion()
										.equals(new ComparableVersion("1"))
								&& programConfig.getVersion()
										.equals(new ComparableVersion("1"))) {
							String fallback = FileUtils.buildPath(
									clusterFolder.getAbsolutePath(),
									programConfig.getName() + "_"
											+ dataConfig.getName()
											+ ".results.qual.complete");
							if (new File(fallback).exists())
								completeFile = new File(fallback);
						}
						final ToolRuntimeEvaluationRunResult tmpResult = parseFromRunResultCompleteFile(
								parentRepository, paramRun, dataConfig,
								programConfig, completeFile, register);
						if (tmpResult != null)
							result.add(tmpResult);
					}
				}
			}
			return run;
		} catch (DatabaseException e) {
			// cannot happen
			return null;
		}
	}
}
