/**
 * 
 */
package de.clusteval.run.runresult.postprocessing;

import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RepositoryObjectDeserializationException;
import de.clusteval.framework.repository.SerializableWrapperDynamicComponent;
import de.clusteval.utils.DynamicComponentInitializationException;

public class SerializableRunResultPostprocessor extends SerializableWrapperDynamicComponent<IRunResultPostprocessor>
		implements
			ISerializableRunResultPostprocessor {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2674047583199998675L;

	/**
	 * @param wrappedComponent
	 */
	public SerializableRunResultPostprocessor(IRunResultPostprocessor wrappedComponent) {
		super(IRunResultPostprocessor.class, wrappedComponent);
	}

	@Override
	protected IRunResultPostprocessor deserializeInternal(final IRepository repository)
			throws DeserializationException {
		// TODO: no params?
		try {
			return RunResultPostprocessor.parseFromString(repository, this.name,
					new RunResultPostprocessorParameters());
		} catch (UnknownRunResultPostprocessorException | DynamicComponentInitializationException e) {
			throw new RepositoryObjectDeserializationException(IRunResultPostprocessor.class, this.name, e);
		}
	}
}