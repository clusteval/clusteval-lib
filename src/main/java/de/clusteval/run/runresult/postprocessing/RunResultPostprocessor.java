/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run.runresult.postprocessing;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.cluster.IClustering;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.Repository;
import de.clusteval.framework.repository.RepositoryObjectDynamicComponent;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.utils.ClassVersion;
import de.clusteval.utils.DynamicComponentInitializationException;
import dk.sdu.imada.compbio.utils.Pair;

/**
 * @author Christian Wiwie
 * 
 */
@ClassVersion(version = "2")
public abstract class RunResultPostprocessor
		extends
			RepositoryObjectDynamicComponent
		implements
			IRunResultPostprocessor {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6173934804904349875L;
	protected RunResultPostprocessorParameters parameters;

	/**
	 * @param repository
	 * @param changeDate
	 * @param absPath
	 * @throws RegisterException
	 */
	public RunResultPostprocessor(IRepository repository, long changeDate,
			File absPath, RunResultPostprocessorParameters parameters)
			throws RegisterException {
		super(repository, changeDate, absPath);
		this.parameters = parameters;
	}

	/**
	 * The copy constructor of data preprocessors.
	 * 
	 * @param other
	 *            The object to clone.
	 * @throws RegisterException
	 */
	public RunResultPostprocessor(RunResultPostprocessor other)
			throws RegisterException {
		super(other);
		this.parameters = other.parameters;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.repository.RepositoryObject#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runresult.postprocessing.IRunResultPostprocessor#clone()
	 */
	@Override
	public RunResultPostprocessor clone() {
		try {
			return this.getClass().getConstructor(this.getClass())
					.newInstance(this);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		this.log.warn("Cloning instance of class "
				+ this.getClass().getSimpleName() + " failed");
		return null;
	}

	/**
	 * Parses a list of runresult postprocessor from a string array.
	 * 
	 * @param repository
	 *            the repository
	 * @param runResultPostprocessor
	 *            The array containing simple names of the runresult
	 *            postprocessor class.
	 * @return A list containing runresult postprocessor.
	 * @throws UnknownRunResultPostprocessorException
	 * @throws DynamicComponentInitializationException
	 */
	public static List<IRunResultPostprocessor> parseFromString(
			final IRepository repository, String[] runResultPostprocessor,
			RunResultPostprocessorParameters[] parameters)
			throws UnknownRunResultPostprocessorException,
			DynamicComponentInitializationException {
		List<IRunResultPostprocessor> result = new ArrayList<IRunResultPostprocessor>();

		for (int i = 0; i < runResultPostprocessor.length; i++) {
			result.add(parseFromString(repository, runResultPostprocessor[i],
					parameters[i]));
		}

		return result;
	}

	/**
	 * Parses a data preprocessor from string.
	 * 
	 * @param repository
	 *            the repository
	 * @param runResultPostProcessor
	 *            The simple name of the data preprocessor class.
	 * @return the data preprocessor
	 * @throws UnknownRunResultPostprocessorException
	 * @throws DynamicComponentInitializationException
	 */
	public static IRunResultPostprocessor parseFromString(
			final IRepository repository, String runResultPostProcessor,
			RunResultPostprocessorParameters parameters)
			throws UnknownRunResultPostprocessorException,
			DynamicComponentInitializationException {

		Class<? extends IRunResultPostprocessor> c = repository
				.resolveAndGetRegisteredClass(IRunResultPostprocessor.class,
						runResultPostProcessor, true);
		if (c == null) {
			Pair<String, ComparableVersion> fullClassNameAndVersion = repository
					.getFullClassNameAndVersion(IRunResultPostprocessor.class,
							runResultPostProcessor, true);
			if (fullClassNameAndVersion.getSecond() != null
					&& repository.hasError(IRunResultPostprocessor.class,
							fullClassNameAndVersion.getFirst(),
							fullClassNameAndVersion.getSecond().toString()))
				throw new UnknownRunResultPostprocessorException(
						runResultPostProcessor,
						repository.getErrors(IRunResultPostprocessor.class,
								fullClassNameAndVersion.getFirst(),
								fullClassNameAndVersion.getSecond().toString())
								.iterator().next());
			else
				throw new UnknownRunResultPostprocessorException(
						runResultPostProcessor,
						"Unknown runresult postprocessor.");
		}
		return getInstance(repository, runResultPostProcessor, parameters, c);
	}

	protected static IRunResultPostprocessor getInstance(
			final IRepository repository, String runResultPostProcessor,
			RunResultPostprocessorParameters parameters,
			Class<? extends IRunResultPostprocessor> c)
			throws UnknownRunResultPostprocessorException,
			DynamicComponentInitializationException {
		try {
			IRunResultPostprocessor preprocessor = c
					.getConstructor(IRepository.class, long.class, File.class,
							RunResultPostprocessorParameters.class)
					.newInstance(repository, System.currentTimeMillis(),
							new File(runResultPostProcessor), parameters);
			preprocessor.register();
			return preprocessor;
		} catch (NoSuchMethodException e) {
			throw new DynamicComponentInitializationException(
					IRunResultPostprocessor.class, runResultPostProcessor,
					"The class is missing the correct constructor");
		} catch (Exception e) {
			// this is likely because we passed faked parameters
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.postprocessing.IRunResultPostprocessor#
	 * postprocess(de.clusteval.cluster.IClustering)
	 */
	@Override
	public abstract IClustering postprocess(final IClustering clustering);

	@Override
	public ComparableVersion getVersion() {
		try {
			return Repository.getVersionOfObjectDynamicClass(this);
		} catch (Exception e) {
			// cannot happen because we loaded the class before;
			e.printStackTrace();
			return new ComparableVersion("1");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#getName()
	 */
	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObjectDynamicComponent#
	 * asSerializable()
	 */
	@Override
	public SerializableRunResultPostprocessor asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableRunResultPostprocessor) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#asSerializable()
	 */
	@Override
	public SerializableRunResultPostprocessor asSerializableInternal() {
		return new SerializableRunResultPostprocessor(this);
	}
}
