/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run.runresult;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import de.clusteval.framework.repository.IRepository;
import de.clusteval.run.IRun;
import de.clusteval.utils.IStatistic;

/**
 * @author Christian Wiwie
 * @param <O>
 *            Type of the objects, on which the statistics are applied.
 * @param <S>
 *            Type of the Statistics
 * 
 */
public abstract class AnalysisRunResult<O extends Object, S extends IStatistic>
		extends
			RunResult
		implements
			IAnalysisRunResult<O, S> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2177954559179302600L;
	protected Map<O, Map<S, String>> statisticValues;

	/**
	 * @param repository
	 * @param changeDate
	 * @param absPath
	 * @param runIdentString
	 * @param run
	 */
	public AnalysisRunResult(IRepository repository, long changeDate,
			File absPath, String runIdentString, final IRun run) {
		super(repository, changeDate, absPath, runIdentString, run);
		this.statisticValues = new HashMap<O, Map<S, String>>();
	}

	/**
	 * The copy constructor for analysis run results.
	 * 
	 * @param other
	 *            The object to clone.
	 */
	public AnalysisRunResult(final AnalysisRunResult<O, S> other) {
		super(other);
		this.statisticValues = cloneStatistics(other.statisticValues);
	}

	/**
	 * A helper method for cloning a map of statistics.
	 * 
	 * @param statistics
	 *            The map to clone.
	 * @return The cloned map.
	 */
	protected abstract Map<O, Map<S, String>> cloneStatistics(
			Map<O, Map<S, String>> statistics);

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.runresult.RunResult#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IAnalysisRunResult#clone()
	 */
	@Override
	public abstract IAnalysisRunResult<O, S> clone();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IAnalysisRunResult#put(S, java.util.List)
	 */
	@Override
	public void put(final O object, final Map<S, String> statisticValues) {
		this.statisticValues.put(object, statisticValues);
	}

	/**
	 * @return the statistics
	 */
	@Override
	public Map<O, Map<S, String>> getStatisticValues() {
		return statisticValues;
	}
}
