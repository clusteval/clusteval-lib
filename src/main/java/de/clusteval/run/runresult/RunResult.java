/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run.runresult;

import java.io.File;
import java.util.List;

import org.apache.maven.artifact.versioning.ComparableVersion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.Repository;
import de.clusteval.framework.repository.RepositoryObject;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.RunResultRepository;
import de.clusteval.framework.repository.db.DatabaseException;
import de.clusteval.framework.repository.parse.Parser;
import de.clusteval.framework.repository.parse.RepositoryObjectParseException;
import de.clusteval.run.ClusteringRun;
import de.clusteval.run.DataAnalysisRun;
import de.clusteval.run.IParameterOptimizationRun;
import de.clusteval.run.IRun;
import de.clusteval.run.IToolRuntimeEvaluationRun;
import de.clusteval.run.ParameterOptimizationRun;
import de.clusteval.run.RunAnalysisRun;
import de.clusteval.run.RunDataAnalysisRun;
import de.clusteval.run.ToolRuntimeEvaluationRun;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * A wrapper class for runresults produced by runs of the framework.
 * 
 * @author Christian Wiwie
 * 
 */
public abstract class RunResult extends RepositoryObject implements IRunResult {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4500193830348975574L;

	/**
	 * @param parentRepository
	 * @param runResultFolder
	 * @param result
	 * @param parseClusterings
	 * @param storeClusterings
	 * @param register
	 * @return A runresult object for the given runresult folder.
	 * @throws RepositoryObjectParseException
	 * @throws InterruptedException
	 * @throws NumberFormatException
	 */
	// TODO: we cannot move this method into Parser#RunResultParser, because
	// ParameterOptimizationRun.parseFromRunResultFolder() returns several
	// RunResult objects per invocation. This is not compatible with the
	// structure of the Parser class. Best solution: rewrite
	// ParameterOptimizationRunResult class, such that it contains all results
	// of the folder in one object.
	public static IRun parseFromRunResultFolder(
			final IRepository parentRepository, final File runResultFolder,
			final List<IRunResult> result, final boolean parseClusterings,
			final boolean storeClusterings, final boolean register)
			throws RepositoryObjectParseException, InterruptedException {
		try {
			Logger log = LoggerFactory.getLogger(RunResult.class);
			log.debug("Parsing run result from '" + runResultFolder + "'");
			IRepository childRepository = Repository.getRepositoryForExactPath(
					runResultFolder.getAbsolutePath());
			if (childRepository == null) {
				childRepository = new RunResultRepository(
						runResultFolder.getAbsolutePath(), parentRepository);
			}
			childRepository.initialize();
			try {

				File runFile = null;
				File configFolder = new File(FileUtils.buildPath(
						runResultFolder.getAbsolutePath(), "configs"));
				if (!configFolder.exists())
					return null;
				for (File child : configFolder.listFiles())
					if (child.getName().endsWith(".run")) {
						runFile = child;
						break;
					}
				if (runFile == null)
					return null;
				final IRun run = Parser.parseRunFromFile(runFile);

				if (run instanceof ToolRuntimeEvaluationRun) {
					return ToolRuntimeEvaluationRunResult
							.parseFromRunResultFolder(
									(IToolRuntimeEvaluationRun) run,
									childRepository, runResultFolder, result,
									register);
				} else if (run instanceof ClusteringRun) {
					return ClusteringRunResult.parseFromRunResultFolder(
							(ClusteringRun) run, childRepository,
							runResultFolder, result, register);
				} else if (run instanceof ParameterOptimizationRun) {
					return ParameterOptimizationResult.parseFromRunResultFolder(
							(IParameterOptimizationRun) run, childRepository,
							runResultFolder, result, parseClusterings,
							storeClusterings, register);
				} else if (run instanceof DataAnalysisRun) {
					DataAnalysisRunResult.parseFromRunResultFolder(
							(DataAnalysisRun) run, childRepository,
							runResultFolder, result, register);
					return run;
				} else if (run instanceof RunDataAnalysisRun) {
					RunDataAnalysisRunResult.parseFromRunResultFolder(
							(RunDataAnalysisRun) run, childRepository,
							runResultFolder, result, register);
					return run;
				} else if (run instanceof RunAnalysisRun) {
					RunAnalysisRunResult.parseFromRunResultFolder(
							(RunAnalysisRun) run, childRepository,
							runResultFolder, result, register);
					return run;
				}
				return run;
			} finally {
				childRepository.terminateSupervisorThread();
			}
		} catch (RepositoryObjectParseException e) {
			throw new RepositoryObjectParseException(IRunResult.class,
					runResultFolder.getName(), "1", e);
		} catch (DatabaseException e) {
			// cannot happen
			return null;
		} catch (InterruptedException e) {
			throw e;
		} catch (Exception e) {
			// run results are not versioned -> always use version '1'
			throw new RepositoryObjectParseException(IRunResult.class,
					runResultFolder.getName(), "1", e);
		}
	}

	/** The run ident string. */
	protected String runIdentString;

	protected IRun run;

	protected boolean changedSinceLastRegister;

	/**
	 * @param repository
	 * @param changeDate
	 * @param absPath
	 * @param runIdentString
	 * @param run
	 */
	public RunResult(IRepository repository, long changeDate, File absPath,
			final String runIdentString, final IRun run) {
		super(repository, changeDate, absPath);
		this.runIdentString = runIdentString;
		this.run = run;
	}

	/**
	 * The copy constructor of run results.
	 * 
	 * @param other
	 *            The object to clone.
	 */
	public RunResult(final RunResult other) {
		super(other);
		this.runIdentString = other.runIdentString;
		this.run = other.run.clone();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.repository.RepositoryObject#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IRunResult#clone()
	 */
	@Override
	public abstract IRunResult clone();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IRunResult#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return this.runIdentString;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IRunResult#getRun()
	 */
	@Override
	public IRun getRun() {
		return this.run;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IRunResult#isInMemory()
	 */
	@Override
	public abstract boolean isInMemory();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IRunResult#loadIntoMemory()
	 */
	@Override
	public abstract void loadIntoMemory() throws RunResultParseException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IRunResult#unloadFromMemory()
	 */
	@Override
	public abstract void unloadFromMemory();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IRunResult#hasChangedSinceLastRegister()
	 */
	@Override
	public boolean hasChangedSinceLastRegister() {
		return this.changedSinceLastRegister;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#register()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IRunResult#register()
	 */
	@Override
	public boolean register() throws RegisterException {
		boolean result = super.register();
		if (result)
			this.changedSinceLastRegister = false;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#toString()
	 */
	@Override
	public String toString() {
		return this.repository.getRelativeFilePathToRepository(this.getFile());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#getVersion()
	 */
	@Override
	public ComparableVersion getVersion() {
		return new ComparableVersion("1");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#getName()
	 */
	@Override
	public String getName() {
		return this.getAbsolutePath();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#asSerializable()
	 */
	@Override
	public ISerializableRunResult<? extends IRunResult> asSerializable()
			throws RepositoryObjectSerializationException {
		return (ISerializableRunResult<? extends IRunResult>) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#asSerializable()
	 */
	@Override
	public abstract ISerializableRunResult<? extends IRunResult> asSerializableInternal()
			throws RepositoryObjectSerializationException;
}
