/**
 * 
 */
package de.clusteval.run.runresult;

import java.io.File;

import de.clusteval.cluster.ISerializableClustering;
import de.clusteval.data.ISerializableDataConfig;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.ISerializableProgramConfig;
import de.clusteval.program.ParameterSet;
import de.clusteval.run.IRun;
import de.clusteval.run.ISerializableRun;
import de.clusteval.run.runresult.format.ISerializableRunResultFormat;
import dk.sdu.imada.compbio.file.FileUtils;
import dk.sdu.imada.compbio.utils.Pair;

public class SerializableClusteringRunResult<R extends IClusteringRunResult>
		extends
			SerializableExecutionRunResult<R>
		implements
			ISerializableClusteringRunResult<R> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5514485876823954531L;

	protected ISerializableRunResultFormat resultFormat;

	protected ParameterSet parameterSet;
	protected ISerializableClustering clustering;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 * @param run
	 * @param runIdentString
	 * @param dataConfig
	 * @param programConfig
	 * @param resultFormat
	 * @param clustering
	 */
	public SerializableClusteringRunResult(File absPath, String name,
			String version, ISerializableRun<? extends IRun> run,
			String runIdentString, ISerializableDataConfig dataConfig,
			ISerializableProgramConfig<? extends IProgramConfig> programConfig,
			ISerializableRunResultFormat resultFormat,
			Pair<ParameterSet, ISerializableClustering> clustering) {
		super(absPath, name, version, run, runIdentString, dataConfig,
				programConfig);
		this.resultFormat = resultFormat;
		this.parameterSet = clustering.getFirst();
		this.clustering = clustering.getSecond();
	}

	/**
	 * @param wrappedComponent
	 */
	public SerializableClusteringRunResult(R wrappedComponent)
			throws RepositoryObjectSerializationException {
		super(wrappedComponent);
		this.resultFormat = wrappedComponent.getFormat().asSerializable();
		this.clustering = wrappedComponent.getClustering().asSerializable();
		this.parameterSet = wrappedComponent.getParameterSet();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runresult.ISerializableClusteringRunResult#getClustering
	 * ()
	 */
	@Override
	public ISerializableClustering getClustering() {
		return clustering;
	}

	/**
	 * @return the parameterSet
	 */
	@Override
	public ParameterSet getParameterSet() {
		return parameterSet;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.ISerializableClusteringRunResult#
	 * getResultFormat()
	 */
	@Override
	public ISerializableRunResultFormat getResultFormat() {
		return resultFormat;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runresult.SerializableRunResult#deserializeInternal(de.
	 * clusteval.framework.repository.IRepository)
	 */
	@Override
	protected R deserializeInternal(IRepository repository)
			throws DeserializationException {
		return (R) new ClusteringRunResult(repository,
				System.currentTimeMillis(),
				new File(FileUtils.buildPath(
						repository.getBasePath(IRunResult.class),
						this.getIdentifier(), "clusters",
						programConfig.getName() + "_" + dataConfig.getName()
								+ ".results.qual.complete")),
				dataConfig.deserialize(repository),
				programConfig.deserialize(repository),
				this.resultFormat.deserialize(repository), runIdentString,
				this.run.deserialize(repository));
	}
}