/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
package de.clusteval.run.runresult;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.cluster.FileBackedClustering;
import de.clusteval.cluster.IClustering;
import de.clusteval.cluster.IFileBackedClustering;
import de.clusteval.data.IDataConfig;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.InvalidRepositoryException;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryAlreadyExistsException;
import de.clusteval.framework.repository.RepositoryCouldNotBeMigratedException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.RepositoryVersionTooNewException;
import de.clusteval.framework.repository.RepositoryVersionTooOldException;
import de.clusteval.framework.repository.RunResultRepository;
import de.clusteval.framework.repository.config.RepositoryConfigNotFoundException;
import de.clusteval.framework.repository.config.RepositoryConfigurationException;
import de.clusteval.framework.repository.db.DatabaseException;
import de.clusteval.framework.repository.parse.ParseException;
import de.clusteval.framework.repository.parse.Parser;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.ParameterSet;
import de.clusteval.program.ProgramConfig;
import de.clusteval.run.ClusteringRun;
import de.clusteval.run.IRun;
import de.clusteval.run.runresult.format.IRunResultFormat;
import de.clusteval.run.runresult.format.RunResultFormat;
import dk.sdu.imada.compbio.file.FileUtils;
import dk.sdu.imada.compbio.utils.Pair;

/**
 * The Class ClusteringResult.
 * 
 * @author Christian Wiwie
 * 
 */
public class ClusteringRunResult extends ExecutionRunResult
		implements
			IClusteringRunResult {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8065662655242940320L;

	protected ParameterSet parameterSet;

	protected IClustering clustering;

	/**
	 * This is the run result format of the program that is being executed by
	 * this runnable.
	 */
	protected IRunResultFormat format;

	/**
	 * Instantiates a new clustering result.
	 * 
	 * @param repository
	 *            the repository
	 * @param changeDate
	 * @param dataConfig
	 *            the data config
	 * @param programConfig
	 *            the program config
	 * @param absPath
	 *            the abs file path
	 * @param format
	 * @param runIdentString
	 *            the run ident string
	 * @param run
	 */
	public ClusteringRunResult(final IRepository repository,
			final long changeDate, final File absPath,
			final IDataConfig dataConfig, final IProgramConfig programConfig,
			final IRunResultFormat format, final String runIdentString,
			final IRun<?> run) {
		super(repository, changeDate, absPath, runIdentString, run, dataConfig,
				programConfig);
		this.format = format;
	}

	/**
	 * The copy constructor of run results.
	 * 
	 * @param other
	 *            The object to clone.
	 */
	public ClusteringRunResult(final ClusteringRunResult other) {
		super(other);

		this.parameterSet = other.parameterSet.clone();
		this.clustering = other.clustering.clone();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.runresult.RunResult#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IClusteringRunResult#clone()
	 */
	@Override
	public IClusteringRunResult clone() {
		return new ClusteringRunResult(this);
	}

	/**
	 * @return the parameterSet
	 */
	@Override
	public ParameterSet getParameterSet() {
		return parameterSet;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IClusteringRunResult#getClustering()
	 */
	@Override
	public IClustering getClustering() {
		return this.clustering;
	}

	/**
	 * @param run
	 *            The run corresponding to the runresult folder.
	 * @param repository
	 *            The repository in which we want to register the runresult.
	 * @param runResultFolder
	 *            A file object referencing the runresult folder.
	 * @param result
	 *            The list of runresults this method fills.
	 * @return The parameter optimization run parsed from the runresult folder.
	 * @throws RegisterException
	 */
	public static IRun parseFromRunResultFolder(final ClusteringRun<?> run,
			final IRepository repository, final File runResultFolder,
			final List<IRunResult> result, final boolean register)
			throws RegisterException, RunResultParseException {

		File clusterFolder = new File(FileUtils
				.buildPath(runResultFolder.getAbsolutePath(), "clusters"));

		for (final IDataConfig dataConfig : run.getOriginalDataConfigs()) {
			for (final IProgramConfig programConfig : run
					.getOriginalProgramConfigs()) {
				File completeFile = new File(
						FileUtils.buildPath(clusterFolder.getAbsolutePath(),
								programConfig.toString(".v") + "_"
										+ dataConfig.toString(".v")
										+ ".results.qual.complete"));
				// fallback to file names without version
				if (!completeFile.exists()
						&& dataConfig.getVersion()
								.equals(new ComparableVersion("1"))
						&& programConfig.getVersion()
								.equals(new ComparableVersion("1"))) {

					String fallback = FileUtils
							.buildPath(clusterFolder.getAbsolutePath(),
									programConfig.getName() + "_"
											+ dataConfig.getName()
											+ ".results.qual.complete");
					if (new File(fallback).exists())
						completeFile = new File(fallback);
				}
				final ClusteringRunResult tmpResult = parseFromRunResultCompleteFile(
						repository, run, (IDataConfig) dataConfig,
						(ProgramConfig) programConfig, completeFile, register);
				if (tmpResult != null)
					result.add(tmpResult);
			}
		}

		return run;
	}

	/**
	 * @param repository
	 * @param run
	 * @param dataConfig
	 * @param programConfig
	 * @param completeFile
	 * @return The parameter optimization run result parsed from the given
	 *         runresult folder.
	 * @throws RegisterException
	 */
	public static ClusteringRunResult parseFromRunResultCompleteFile(
			IRepository repository, ClusteringRun run,
			final IDataConfig dataConfig, final IProgramConfig programConfig,
			final File completeFile, final boolean register)
			throws RegisterException, RunResultParseException {
		ClusteringRunResult result = null;
		if (completeFile.exists()) {
			result = new ClusteringRunResult(repository,
					completeFile.lastModified(), completeFile, dataConfig,
					programConfig,
					(RunResultFormat) programConfig.getOutputFormat(),
					completeFile.getParentFile().getParentFile().getName(),
					run);

			if (register) {
				/*
				 * Register after parsing
				 */
				result.loadIntoMemory();
				try {
					result.register();
				} finally {
					result.unloadFromMemory();
				}
			}
		}
		return result;
	}

	/**
	 * @param parentRepository
	 * @param runResultFolder
	 * @param result
	 * @throws IOException
	 * @throws InvalidRepositoryException
	 * @throws RepositoryAlreadyExistsException
	 * @return The parameter optimization run result parsed from the given
	 *         runresult folder.
	 * @throws RepositoryConfigurationException
	 * @throws RepositoryConfigNotFoundException
	 * @throws RegisterException
	 * @throws NumberFormatException
	 * @throws InterruptedException
	 * @throws RepositoryVersionTooNewException
	 * @throws RepositoryVersionTooOldException
	 * @throws ParseException
	 * @throws RepositoryCouldNotBeMigratedException
	 */
	public static IRun parseFromRunResultFolder(
			final IRepository parentRepository, final File runResultFolder,
			final List<ExecutionRunResult> result, final boolean register)
			throws IOException, RepositoryVersionTooOldException,
			RepositoryVersionTooNewException, ParseException,
			RepositoryAlreadyExistsException, InvalidRepositoryException,
			RepositoryConfigNotFoundException, RepositoryConfigurationException,
			InterruptedException, RegisterException,
			RepositoryCouldNotBeMigratedException, RunResultParseException {

		IRepository childRepository;
		try {
			childRepository = new RunResultRepository(
					runResultFolder.getAbsolutePath(),
					(IRepository) parentRepository);
			childRepository.initialize();

			File runFile = null;
			File configFolder = new File(FileUtils
					.buildPath(runResultFolder.getAbsolutePath(), "configs"));
			if (!configFolder.exists())
				return null;
			for (File child : configFolder.listFiles())
				if (child.getName().endsWith(".run")) {
					runFile = child;
					break;
				}
			if (runFile == null)
				return null;
			final IRun<?> run = Parser.parseRunFromFile(runFile);

			if (run instanceof ClusteringRun) {
				final ClusteringRun<?> paramRun = (ClusteringRun) run;

				File clusterFolder = new File(FileUtils.buildPath(
						runResultFolder.getAbsolutePath(), "clusters"));
				for (final IDataConfig dataConfig : paramRun
						.getOriginalDataConfigs()) {
					for (final IProgramConfig programConfig : paramRun
							.getOriginalProgramConfigs()) {
						File completeFile = new File(FileUtils.buildPath(
								clusterFolder.getAbsolutePath(),
								programConfig.toString(".v") + "_"
										+ dataConfig.toString(".v")
										+ ".results.qual.complete"));
						// fallback to file names without version
						if (!completeFile.exists()
								&& dataConfig.getVersion()
										.equals(new ComparableVersion("1"))
								&& programConfig.getVersion()
										.equals(new ComparableVersion("1"))) {
							String fallback = FileUtils.buildPath(
									clusterFolder.getAbsolutePath(),
									programConfig.getName() + "_"
											+ dataConfig.getName()
											+ ".results.qual.complete");
							if (new File(fallback).exists())
								completeFile = new File(fallback);
						}
						final ClusteringRunResult tmpResult = parseFromRunResultCompleteFile(
								parentRepository, paramRun, dataConfig,
								programConfig, completeFile, register);
						if (tmpResult != null)
							result.add(tmpResult);
					}
				}
			}
			return run;
		} catch (DatabaseException e) {
			// cannot happen
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#loadIntoMemory()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IClusteringRunResult#loadIntoMemory()
	 */
	@Override
	public void loadIntoMemory() throws RunResultParseException {
		if (absPath.exists()) {
			try {
				final Pair<ParameterSet, IFileBackedClustering> pair = FileBackedClustering
						.parseFromFile(repository,
								new File(absPath.getAbsolutePath().replace(
										"results.qual.complete",
										"1.results.conv")),
								format, true);

				ParameterSet paramSet = new ParameterSet();
				for (String param : pair.getFirst().keySet())
					paramSet.put(param, pair.getFirst().get(param));
				this.parameterSet = pair.getFirst();
				this.clustering = pair.getSecond();
				this.clustering.loadIntoMemory();

				super.loadIntoMemory();
			} catch (Exception e) {
				unloadFromMemory();
				throw new RunResultParseException(e.getMessage());
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#isInMemory()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IClusteringRunResult#isInMemory()
	 */
	@Override
	public boolean isInMemory() {
		return this.clustering != null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#unloadFromMemory()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IClusteringRunResult#unloadFromMemory()
	 */
	@Override
	public void unloadFromMemory() {
		if (isInMemory() && this.clustering != null) {
			this.clustering.unloadFromMemory();
			this.clustering = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.ExecutionRunResult#asSerializable()
	 */
	@Override
	public SerializableClusteringRunResult<? extends IClusteringRunResult> asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableClusteringRunResult<? extends IClusteringRunResult>) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#asSerializable()
	 */
	@Override
	public SerializableClusteringRunResult<? extends IClusteringRunResult> asSerializableInternal()
			throws RepositoryObjectSerializationException {
		// ensure that the clustering is set before serialization
		boolean isLoaded = this.isInMemory();
		try {
			this.loadIntoMemory();
			SerializableClusteringRunResult<IClusteringRunResult> serializableClusteringRunResult = new SerializableClusteringRunResult<IClusteringRunResult>(
					this);
			if (!isLoaded)
				this.unloadFromMemory();
			return serializableClusteringRunResult;
		} catch (Exception e) {
			throw new RepositoryObjectSerializationException(IRunResult.class,
					this.getName(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.IClustering#getFormat()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.cluster.IFileBackedClustering#getFormat()
	 */
	@Override
	public IRunResultFormat getFormat() {
		return format;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.cluster.IFileBackedClustering#setFormat(de.clusteval.run.
	 * runresult.format.IRunResultFormat)
	 */
	@Override
	public void setFormat(IRunResultFormat resultFormat) {
		this.format = resultFormat;
	}
}
