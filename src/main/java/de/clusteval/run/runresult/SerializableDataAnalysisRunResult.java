/**
 * 
 */
package de.clusteval.run.runresult;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import de.clusteval.data.IDataConfig;
import de.clusteval.data.statistics.IDataStatistic;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.run.IRun;
import de.clusteval.run.ISerializableRun;
import de.clusteval.utils.ISerializableStatistic;

/**
 * @author Christian Wiwie
 *
 */
public class SerializableDataAnalysisRunResult
		extends
			SerializableAnalysisRunResult<IDataConfig, IDataStatistic, IDataAnalysisRunResult>
		implements
			ISerializableDataAnalysisRunResult {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8060258486270404013L;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 * @param run
	 * @param runIdentString
	 */
	public SerializableDataAnalysisRunResult(File absPath, String name,
			String version, ISerializableRun<? extends IRun> run,
			String runIdentString) {
		super(absPath, name, version, run, runIdentString);
	}

	/**
	 * @param wrappedComponent
	 * @throws RepositoryObjectSerializationException
	 */
	public SerializableDataAnalysisRunResult(
			IDataAnalysisRunResult wrappedComponent)
			throws RepositoryObjectSerializationException {
		super(wrappedComponent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runresult.SerializableRunResult#deserializeInternal(de.
	 * clusteval.framework.repository.IRepository)
	 */
	@Override
	protected IDataAnalysisRunResult deserializeInternal(IRepository repository)
			throws DeserializationException {
		DataAnalysisRunResult result = new DataAnalysisRunResult(repository,
				System.currentTimeMillis(), absPath, runIdentString,
				run.deserialize(repository));

		for (IDataConfig dc : this.statisticValues.keySet()) {
			Map<IDataStatistic, String> l = new HashMap<>();
			for (ISerializableStatistic<IDataStatistic> s : this.statisticValues
					.get(dc).keySet())
				l.put(s.deserialize(repository),
						this.statisticValues.get(dc).get(s));
			result.statisticValues.put(dc, l);
		}

		return result;
	}
}
