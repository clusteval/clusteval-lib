package de.clusteval.run.runresult;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dk.sdu.imada.compbio.utils.StringExt;
import dk.sdu.imada.compbio.utils.parse.TextFileParser;

/**
 * @author Christian Wiwie
 * 
 */
public class RunResultMetaDataParser extends TextFileParser {

	protected List<String> metaAttributes;
	protected ExecutionRunResult tmpResult;

	/**
	 * @param tmpResult
	 * @param absFilePath
	 * @throws IOException
	 */
	public RunResultMetaDataParser(final ExecutionRunResult tmpResult,
			final String absFilePath) throws IOException {
		super(absFilePath, new int[0], new int[0]);
		this.setLockTargetFile(true);
		this.tmpResult = tmpResult;
	}

	@SuppressWarnings("unused")
	@Override
	protected void processLine(String[] key, String[] value) {
		if (this.currentLine == 0) {
			/*
			 * Parse header line
			 */
			metaAttributes = new ArrayList<>();
			for (int i = 1; i < value.length; i++) {
				metaAttributes.add(value[i]);
			}
		} else {
			try {
				// duplicated parameter set -> skipped iteration
				if (value[0].contains("*")) {
					// we don't parse duplicated iterations
					return;
				}
				long iterationNumber = Long.valueOf(value[0]);

				Map<String, String> metaData = new HashMap<>();
				for (int i = 1; i < value.length; i++) {
					if (!value[i].isEmpty())
						metaData.put(this.metaAttributes.get(i - 1), value[i]);
				}
				tmpResult.setMetaDataForIteration(iterationNumber, metaData);
			} catch (Exception e) {
				this.log.warn(
						"An error occurred while parsing the meta data '{}': {}",
						this.getAbsoluteFilePath(), e.getMessage());
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.parse.TextFileParser#split(java.lang.String)
	 */
	@Override
	protected String[] split(String line) {
		return this.splitLines
				? StringExt.split(line, this.inSplit)
				: new String[]{line};
	}
}
