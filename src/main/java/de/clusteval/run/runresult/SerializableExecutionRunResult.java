/**
 * 
 */
package de.clusteval.run.runresult;

import java.io.File;

import de.clusteval.data.ISerializableDataConfig;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.ISerializableProgramConfig;
import de.clusteval.run.IRun;
import de.clusteval.run.ISerializableRun;

public abstract class SerializableExecutionRunResult<R extends IExecutionRunResult>
		extends
			SerializableRunResult<R>
		implements
			ISerializableExecutionRunResult<R> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -973711295515501180L;

	protected ISerializableDataConfig dataConfig;

	protected ISerializableProgramConfig<? extends IProgramConfig> programConfig;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 * @param run
	 * @param runIdentString
	 * @param dataConfig
	 * @param programConfig
	 */
	public SerializableExecutionRunResult(File absPath, String name,
			String version, ISerializableRun<? extends IRun> run,
			String runIdentString, ISerializableDataConfig dataConfig,
			ISerializableProgramConfig<? extends IProgramConfig> programConfig) {
		super(absPath, name, version, run, runIdentString);
		this.dataConfig = dataConfig;
		this.programConfig = programConfig;
	}

	/**
	 * @param wrappedComponent
	 */
	public SerializableExecutionRunResult(R wrappedComponent)
			throws RepositoryObjectSerializationException {
		super(wrappedComponent);
		this.dataConfig = wrappedComponent.getDataConfig().asSerializable();
		this.programConfig = wrappedComponent.getProgramConfig()
				.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runresult.ISerializableExecutionRunResult#getDataConfig(
	 * )
	 */
	@Override
	public ISerializableDataConfig getDataConfig() {
		return dataConfig;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.ISerializableExecutionRunResult#
	 * getProgramConfig()
	 */
	@Override
	public ISerializableProgramConfig<? extends IProgramConfig> getProgramConfig() {
		return programConfig;
	}
}