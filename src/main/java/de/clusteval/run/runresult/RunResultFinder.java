/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run.runresult;

import java.io.File;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.parse.RepositoryObjectParseException;
import de.clusteval.framework.threading.IRunSchedulerThread;
import de.clusteval.run.IRun;
import de.clusteval.run.RUN_STATUS;
import de.clusteval.utils.FileFinder;
import dk.sdu.imada.compbio.utils.ArrayIterator;

/**
 * @author Christian Wiwie
 * 
 */
public class RunResultFinder extends FileFinder<IRunResult> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8254641354891312446L;
	// the iterator to have access to the parsed runresult objects
	protected RunResultIterator iter;

	/**
	 * Instantiates a new run result finder.
	 * 
	 * @param repository
	 *            The repository to register the new run results at.
	 * @throws RegisterException
	 */
	public RunResultFinder(IRepository repository) throws RegisterException {
		super(repository, IRunResult.class);
	}

	@Override
	protected IRunResult parseObjectFromFile(File file,
			final boolean recoverFromExceptions) throws RegisterException,
			RepositoryObjectParseException, MalformedURLException {
		try {
			iter.getRunResult().loadIntoMemory();
			try {
				iter.getRunResult().register();
			} finally {
				iter.getRunResult().unloadFromMemory();
			}
			return iter.getRunResult();
		} catch (RunResultParseException e) {
			throw new RepositoryObjectParseException(IRunResult.class,
					iter.getRunResult().getName(), "1", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.utils.FileFinder#onUnknownExceptionWarning(java.io.File,
	 * java.lang.Exception)
	 */
	@Override
	protected void onUnknownExceptionWarning(File file, Exception e) {
		// we do not print anything because these errors have been printed
		// already
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.Finder#getIterator()
	 */
	@Override
	protected Iterator<File> getIterator() {
		iter = new RunResultIterator(this.repository, this.getBaseDir(), this);
		return iter;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.Finder#checkFile(java.io.File)
	 */
	@Override
	// Fixed 17.03.2014: changed !isRunning(file.getName()) to
	// !isRunning(file.getParentFile().getParentFile().getName())
	protected boolean checkFile(File file) {
		String uniqueRunId;
		File f = file;
		while (!f.getParentFile().getName().equals("results"))
			f = f.getParentFile();
		uniqueRunId = f.getName();
		IRepositoryObject registered = repository.getRegisteredObject(file);
		return !isRunning(uniqueRunId) && (registered == null
				|| registered.getChangeDate() < file.lastModified());
	}

	protected boolean isRunning(final String uniqueRunIdentifier) {
		IRunSchedulerThread runScheduler = repository.getSupervisorThread()
				.getRunScheduler();
		Collection<IRun<?>> runs = runScheduler.getRuns();
		for (IRun<?> run : runs) {
			if ((run.getStatus().equals(RUN_STATUS.RUNNING)
					|| run.getStatus().equals(RUN_STATUS.SCHEDULED))
					&& run.getRunIdentificationString() != null
					&& run.getRunIdentificationString()
							.equals(uniqueRunIdentifier)) {
				return true;
			}
		}
		return false;
	}
}

class RunResultIterator implements Iterator<File> {

	protected IRepository repo;

	protected ArrayIterator<File> basePath;

	protected List<IRunResult> parsedResults;

	protected IRunResult lastReturnedResult;

	protected RunResultFinder finder;

	public RunResultIterator(final IRepository repo, final File basePath,
			final RunResultFinder finder) {
		super();
		this.repo = repo;
		this.basePath = new ArrayIterator<File>(basePath.listFiles());
		this.parsedResults = new ArrayList<IRunResult>();
		this.finder = finder;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Iterator#hasNext()
	 */
	@Override
	public boolean hasNext() {
		// take already parsed runresults
		if (this.parsedResults.size() > 0)
			return true;

		// parse new runresults
		boolean exception = true;
		while ((exception || this.parsedResults.size() == 0)
				&& basePath.hasNext()) {
			File file = basePath.next();
			try {
				List<IRunResult> newResults = new ArrayList<IRunResult>();
				RunResult.parseFromRunResultFolder(repo, file, newResults,
						false, false, true);
				this.parsedResults.addAll(newResults);
				exception = false;
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				return false;
			} catch (RepositoryObjectParseException e) {
				try {
					this.finder.handleException(file, e.getClazz(), e.getName(),
							e.getVersion(), (Exception) e.getCause());
				} catch (RegisterException e1) {
					e1.printStackTrace();
				}
			}
		}
		return this.parsedResults.size() > 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Iterator#next()
	 */
	@Override
	public File next() {
		this.lastReturnedResult = this.parsedResults.remove(0);
		return new File(lastReturnedResult.getAbsolutePath());
	}

	public IRunResult getRunResult() {
		return this.lastReturnedResult;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Iterator#remove()
	 */
	@Override
	public void remove() {
		// unsupported
	}
}