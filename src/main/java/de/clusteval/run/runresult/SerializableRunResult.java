/**
 * 
 */
package de.clusteval.run.runresult;

import java.io.File;

import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.SerializableWrapperRepositoryObject;
import de.clusteval.run.IRun;
import de.clusteval.run.ISerializableRun;

public abstract class SerializableRunResult<R extends IRunResult>
		extends
			SerializableWrapperRepositoryObject<R>
		implements
			ISerializableRunResult<R> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -832865116442553815L;

	protected ISerializableRun<? extends IRun> run;

	protected String runIdentString;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 * @param run
	 * @param runIdentString
	 */
	public SerializableRunResult(File absPath, String name, String version,
			ISerializableRun<? extends IRun> run, String runIdentString) {
		super(absPath, name, version);
		this.run = run;
		this.runIdentString = runIdentString;
	}

	/**
	 * @param wrappedComponent
	 */
	public SerializableRunResult(R wrappedComponent)
			throws RepositoryObjectSerializationException {
		super(wrappedComponent);
		this.run = wrappedComponent.getRun().asSerializable();
		this.runIdentString = wrappedComponent.getIdentifier();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.ISerializableRunResult#getRun()
	 */
	@Override
	public ISerializableRun<? extends IRun> getRun() {
		return run;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.ISerializableRunResult#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return runIdentString;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.SerializableWrapper#deserializeInternal
	 * (de.clusteval.framework.repository.IRepository)
	 */
	@Override
	protected R deserializeInternal(IRepository repository)
			throws DeserializationException {
		try {
			return (R) repository.getStaticObjectWithNameAndVersion(
					IRunResult.class, name + ":" + version);
		} catch (ObjectNotRegisteredException
				| ObjectVersionNotRegisteredException e) {
			e.printStackTrace();
		}
		return null;
	}
}