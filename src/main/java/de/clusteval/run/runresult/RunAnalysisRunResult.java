/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run.runresult;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.configuration.ConfigurationException;

import de.clusteval.cluster.paramOptimization.IncompatibleParameterOptimizationMethodException;
import de.clusteval.cluster.paramOptimization.InvalidOptimizationParameterException;
import de.clusteval.cluster.paramOptimization.UnknownParameterOptimizationMethodException;
import de.clusteval.cluster.quality.UnknownClusteringQualityMeasureException;
import de.clusteval.context.IncompatibleContextException;
import de.clusteval.context.UnknownContextException;
import de.clusteval.data.DataConfigNotFoundException;
import de.clusteval.data.DataConfigurationException;
import de.clusteval.data.dataset.DataSetConfigNotFoundException;
import de.clusteval.data.dataset.DataSetConfigurationException;
import de.clusteval.data.dataset.DataSetNotFoundException;
import de.clusteval.data.dataset.IncompatibleDataSetConfigPreprocessorException;
import de.clusteval.data.dataset.NoDataSetException;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.dataset.type.UnknownDataSetTypeException;
import de.clusteval.data.distance.UnknownDistanceMeasureException;
import de.clusteval.data.goldstandard.GoldStandardConfigNotFoundException;
import de.clusteval.data.goldstandard.GoldStandardConfigurationException;
import de.clusteval.data.goldstandard.GoldStandardNotFoundException;
import de.clusteval.data.goldstandard.format.UnknownGoldStandardFormatException;
import de.clusteval.data.preprocessing.InvalidDataPreprocessorOptionsException;
import de.clusteval.data.preprocessing.UnknownDataPreprocessorException;
import de.clusteval.data.randomizer.UnknownDataRandomizerException;
import de.clusteval.data.statistics.UnknownDataStatisticException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.InvalidRepositoryException;
import de.clusteval.framework.repository.NoRepositoryFoundException;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryAlreadyExistsException;
import de.clusteval.framework.repository.RepositoryCouldNotBeMigratedException;
import de.clusteval.framework.repository.RepositoryVersionTooNewException;
import de.clusteval.framework.repository.RepositoryVersionTooOldException;
import de.clusteval.framework.repository.RunResultRepository;
import de.clusteval.framework.repository.config.RepositoryConfigNotFoundException;
import de.clusteval.framework.repository.config.RepositoryConfigurationException;
import de.clusteval.framework.repository.db.DatabaseException;
import de.clusteval.framework.repository.parse.ParseException;
import de.clusteval.framework.repository.parse.Parser;
import de.clusteval.program.NoOptimizableProgramParameterException;
import de.clusteval.program.UnknownParameterType;
import de.clusteval.program.UnknownProgramParameterException;
import de.clusteval.program.UnknownProgramTypeException;
import de.clusteval.program.r.UnknownRProgramException;
import de.clusteval.run.IRun;
import de.clusteval.run.InvalidRunModeException;
import de.clusteval.run.RunAnalysisRun;
import de.clusteval.run.RunException;
import de.clusteval.run.runresult.format.UnknownRunResultFormatException;
import de.clusteval.run.runresult.postprocessing.UnknownRunResultPostprocessorException;
import de.clusteval.run.statistics.IRunStatistic;
import de.clusteval.run.statistics.RunStatistic;
import de.clusteval.run.statistics.UnknownRunDataStatisticException;
import de.clusteval.run.statistics.UnknownRunStatisticException;
import de.clusteval.utils.IStatistic;
import de.clusteval.utils.InvalidConfigurationFileException;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * @author Christian Wiwie
 * 
 */
public class RunAnalysisRunResult
		extends
			AnalysisRunResult<String, IRunStatistic>
		implements
			IRunAnalysisRunResult {

	/**
	 * 
	 */
	private static final long serialVersionUID = 741209977706928635L;

	/**
	 * @param repository
	 * @param register
	 * @param changeDate
	 * @param absPath
	 * @param runIdentString
	 * @param run
	 * @throws RegisterException
	 */
	public RunAnalysisRunResult(IRepository repository, boolean register,
			long changeDate, File absPath, String runIdentString,
			final IRun run) throws RegisterException {
		super(repository, changeDate, absPath, runIdentString, run);

		if (register)
			this.register();
	}

	/**
	 * The copy constructor for run analysis run results.
	 * 
	 * @param other
	 *            The object to clone.
	 * @throws RegisterException
	 */
	public RunAnalysisRunResult(final RunAnalysisRunResult other)
			throws RegisterException {
		super(other);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.runresult.AnalysisRunResult#cloneStatistics(java.util.Map)
	 */
	@Override
	protected Map<String, Map<IRunStatistic, String>> cloneStatistics(
			Map<String, Map<IRunStatistic, String>> statistics) {
		final Map<String, Map<IRunStatistic, String>> result = new HashMap<>();

		for (Map.Entry<String, Map<IRunStatistic, String>> entry : statistics
				.entrySet()) {
			Map<IRunStatistic, String> newList = new HashMap<>();

			for (IRunStatistic elem : entry.getValue().keySet()) {
				newList.put(elem.clone(), entry.getValue().get(elem));
			}

			result.put(new String(entry.getKey()), newList);
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.runresult.AnalysisRunResult#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IRunAnalysisRunResult#clone()
	 */
	@Override
	public IRunAnalysisRunResult clone() {
		try {
			return new RunAnalysisRunResult(this);
		} catch (RegisterException e) {
			// should not occur
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @param repository
	 *            The repository in which we want to register the parsed
	 *            runresult.
	 * @param runResultFolder
	 *            The runresult folder from which we want to parse the
	 *            runresult.
	 * @return The run analysis runresult parsed from the runresult folder.
	 * 
	 * @throws RepositoryAlreadyExistsException
	 * @throws RepositoryAlreadyExistsException
	 * @throws InvalidRepositoryException
	 * @throws UnknownRunStatisticException
	 * @throws UnknownDistanceMeasureException
	 * @throws IncompatibleParameterOptimizationMethodException
	 * @throws UnknownRProgramException
	 * @throws UnknownProgramTypeException
	 * @throws UnknownDataStatisticException
	 * @throws RunException
	 * @throws InvalidOptimizationParameterException
	 * @throws GoldStandardNotFoundException
	 * @throws NoRepositoryFoundException
	 * @throws UnknownProgramParameterException
	 * @throws NoOptimizableProgramParameterException
	 * @throws UnknownParameterOptimizationMethodException
	 * @throws InvalidRunModeException
	 * @throws UnknownClusteringQualityMeasureException
	 * @throws InvalidConfigurationFileException
	 * @throws UnknownDataSetFormatException
	 * @throws UnknownRunResultFormatException
	 * @throws IOException
	 * @throws DataConfigNotFoundException
	 * @throws DataConfigurationException
	 * @throws GoldStandardConfigNotFoundException
	 * @throws DataSetConfigNotFoundException
	 * @throws DataSetNotFoundException
	 * @throws DataSetConfigurationException
	 * @throws GoldStandardConfigurationException
	 * @throws UnknownGoldStandardFormatException
	 * @throws RepositoryConfigurationException
	 * @throws RepositoryConfigNotFoundException
	 * @throws ConfigurationException
	 * @throws RegisterException
	 * @throws UnknownDataSetTypeException
	 * @throws NoDataSetException
	 * @throws NumberFormatException
	 * @throws UnknownRunDataStatisticException
	 * @throws RunResultParseException
	 * @throws UnknownDataPreprocessorException
	 * @throws IncompatibleDataSetConfigPreprocessorException
	 * @throws UnknownContextException
	 * @throws IncompatibleContextException
	 * @throws UnknownParameterType
	 * @throws InterruptedException
	 * @throws UnknownRunResultPostprocessorException
	 * @throws UnknownDataRandomizerException
	 * @throws InvalidDataPreprocessorOptionsException
	 * @throws RepositoryVersionTooNewException
	 * @throws RepositoryVersionTooOldException
	 * @throws FileNotFoundException
	 * @throws RepositoryCouldNotBeMigratedException
	 */
	public static IRunAnalysisRunResult parseFromRunResultFolder(
			final IRepository repository, final File runResultFolder)
			throws RepositoryAlreadyExistsException, InvalidRepositoryException,
			ParseException, RepositoryVersionTooOldException,
			RepositoryVersionTooNewException, FileNotFoundException,
			RepositoryConfigNotFoundException, RepositoryConfigurationException,
			InterruptedException, RegisterException, RunResultParseException,
			RepositoryCouldNotBeMigratedException {
		try {
			IRepository childRepository = new RunResultRepository(
					runResultFolder.getAbsolutePath(), repository);
			childRepository.initialize();

			File runFile = null;
			File configFolder = new File(FileUtils
					.buildPath(runResultFolder.getAbsolutePath(), "configs"));
			if (!configFolder.exists())
				return null;
			for (File child : configFolder.listFiles())
				if (child.getName().endsWith(".run")) {
					runFile = child;
					break;
				}
			if (runFile == null)
				return null;
			final IRun object = Parser.parseRunFromFile(runFile);

			RunAnalysisRunResult analysisResult = null;

			if (object instanceof RunAnalysisRun) {
				final RunAnalysisRun run = (RunAnalysisRun) object;

				File analysesFolder = new File(FileUtils.buildPath(
						runResultFolder.getAbsolutePath(), "analyses"));

				analysisResult = new RunAnalysisRunResult(repository, false,
						analysesFolder.lastModified(), analysesFolder,
						analysesFolder.getParentFile().getName(), run);

				analysisResult.loadIntoMemory();
				try {
					analysisResult.register();
				} finally {
					analysisResult.unloadFromMemory();
				}
			}
			return analysisResult;
		} catch (DatabaseException e) {
			// cannot happen
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#getRun()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IRunAnalysisRunResult#getRun()
	 */
	@Override
	public RunAnalysisRun getRun() {
		return (RunAnalysisRun) super.getRun();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#loadIntoMemory()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IRunAnalysisRunResult#loadIntoMemory()
	 */
	@Override
	public void loadIntoMemory() throws RunResultParseException {
		for (final String uniqueRunIdentifier : this.getRun()
				.getUniqueRunAnalysisRunIdentifiers()) {

			Map<IRunStatistic, String> statistics = new HashMap<>();
			for (final IStatistic runStatistic : this.getRun()
					.getStatistics()) {
				final File completeFile = new File(FileUtils.buildPath(
						absPath.getAbsolutePath(), uniqueRunIdentifier + "_"
								+ runStatistic.toString(".v") + ".txt"));
				if (!completeFile.exists()) {
					throw new RunResultParseException(
							"The result file of (" + uniqueRunIdentifier + ","
									+ runStatistic.toString(".v")
									+ ") could not be found: " + completeFile);
				}
				final String fileContents = FileUtils
						.readStringFromFile(completeFile.getAbsolutePath());

				runStatistic.parseFromString(fileContents);
				statistics.put((RunStatistic) runStatistic, fileContents);

			}
			this.put(uniqueRunIdentifier, statistics);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#isInMemory()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IRunAnalysisRunResult#isInMemory()
	 */
	@Override
	public boolean isInMemory() {
		return this.statisticValues.isEmpty();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#unloadFromMemory()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IRunAnalysisRunResult#unloadFromMemory()
	 */
	@Override
	public void unloadFromMemory() {
		this.statisticValues.clear();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runresult.IRunAnalysisRunResult#getUniqueRunIdentifier()
	 */
	@Override
	public Set<String> getUniqueRunIdentifier() {
		return this.statisticValues.keySet();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runresult.IRunAnalysisRunResult#getRunStatistics(java.
	 * lang.String)
	 */
	@Override
	public Map<IRunStatistic, String> getRunStatisticValues(
			final String uniqueRunIdentifier) {
		return this.statisticValues.get(uniqueRunIdentifier);
	}

	/**
	 * @param run
	 *            The run analysis run corresponding to the given runresult
	 *            folder.
	 * @param repository
	 *            The repository in which we want to register the parsed
	 *            runresult.
	 * @param runResultFolder
	 *            A file object referencing the runresult folder.
	 * @return The run analysis runresult parsed from the given runresult
	 *         folder.
	 * @throws RunResultParseException
	 * @throws RegisterException
	 * 
	 */
	public static IRunAnalysisRunResult parseFromRunResultFolder(
			final RunAnalysisRun run, final IRepository repository,
			final File runResultFolder, final List<IRunResult> result,
			final boolean register)
			throws RunResultParseException, RegisterException {

		RunAnalysisRunResult analysisResult = null;

		File analysesFolder = new File(FileUtils
				.buildPath(runResultFolder.getAbsolutePath(), "analyses"));

		analysisResult = new RunAnalysisRunResult(repository, false,
				analysesFolder.lastModified(), analysesFolder,
				analysesFolder.getParentFile().getName(), run);

		for (final String uniqueRunIdentifier : run
				.getUniqueRunAnalysisRunIdentifiers()) {

			Map<IRunStatistic, String> statistics = new HashMap<>();
			for (final IStatistic runStatistic : run.getStatistics()) {
				final File completeFile = new File(FileUtils.buildPath(
						analysesFolder.getAbsolutePath(), uniqueRunIdentifier
								+ "_" + runStatistic.toString(".v") + ".txt"));
				if (!completeFile.exists())
					throw new RunResultParseException(
							"The result file of (" + uniqueRunIdentifier + ","
									+ runStatistic.toString(".v")
									+ ") could not be found: " + completeFile);
				final String fileContents = FileUtils
						.readStringFromFile(completeFile.getAbsolutePath());

				runStatistic.parseFromString(fileContents);
				statistics.put((RunStatistic) runStatistic, fileContents);

			}
			analysisResult.put(uniqueRunIdentifier, statistics);
		}

		result.add(analysisResult);

		if (register)
			analysisResult.register();
		return analysisResult;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#asSerializable()
	 */
	@Override
	public ISerializableRunResult<? extends IRunResult> asSerializableInternal() {
		// TODO
		return null;
	}
}
