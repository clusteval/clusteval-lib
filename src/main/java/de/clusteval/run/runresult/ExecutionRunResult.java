/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run.runresult;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import de.clusteval.data.IDataConfig;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.program.IProgramConfig;
import de.clusteval.run.IRun;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * @author Christian Wiwie
 * 
 */
public abstract class ExecutionRunResult extends RunResult
		implements
			IExecutionRunResult {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8122991200500348751L;

	/** The data config. */
	protected IDataConfig dataConfig;

	/** The program config. */
	protected IProgramConfig programConfig;

	protected Map<Long, Map<String, String>> iterationToMetaAttributeValues;

	/**
	 * @param repository
	 * @param changeDate
	 * @param absPath
	 * @param runIdentString
	 * @param run
	 * @param dataConfig
	 * @param programConfig
	 */
	public ExecutionRunResult(IRepository repository, long changeDate,
			File absPath, String runIdentString, final IRun run,
			final IDataConfig dataConfig, final IProgramConfig programConfig) {
		super(repository, changeDate, absPath, runIdentString, run);
		this.dataConfig = dataConfig;
		this.programConfig = programConfig;
		this.iterationToMetaAttributeValues = new HashMap<Long, Map<String, String>>();
	}

	/**
	 * The copy constructor of run results.
	 * 
	 * @param other
	 *            The object to clone.
	 */
	public ExecutionRunResult(final ExecutionRunResult other) {
		super(other);
		this.dataConfig = other.dataConfig.clone();
		this.programConfig = other.programConfig.clone();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.runresult.RunResult#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IExecutionRunResult#clone()
	 */
	@Override
	public abstract IExecutionRunResult clone();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IExecutionRunResult#getProgramConfig()
	 */
	@Override
	public IProgramConfig getProgramConfig() {
		return this.programConfig;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IExecutionRunResult#getDataConfig()
	 */
	@Override
	public IDataConfig getDataConfig() {
		return this.dataConfig;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#asSerializable()
	 */
	@Override
	public ISerializableExecutionRunResult<? extends IExecutionRunResult> asSerializable()
			throws RepositoryObjectSerializationException {
		return (ISerializableExecutionRunResult<? extends IExecutionRunResult>) super.asSerializable();
	}

	@Override
	public void setMetaDataForIteration(final long iteration,
			final Map<String, String> metaData) {
		this.iterationToMetaAttributeValues.put(iteration, metaData);
	}

	@Override
	public Map<String, String> getMetaDataOfIteration(final long iteration) {
		if (this.iterationToMetaAttributeValues.containsKey(iteration))
			return this.iterationToMetaAttributeValues.get(iteration);
		return Collections.emptyMap();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runresult.IExecutionRunResult#getMetaDataOfAllIterations
	 * ()
	 */
	@Override
	public Map<Long, Map<String, String>> getMetaDataOfAllIterations() {
		return this.iterationToMetaAttributeValues;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#loadIntoMemory()
	 */
	@Override
	public void loadIntoMemory() throws RunResultParseException {
		try {
			// parse meta data
			String metaDataPath = absPath.getAbsolutePath()
					.replaceAll("results.qual", "metadata");
			if (new File(metaDataPath).exists()) {
				RunResultMetaDataParser parser2 = new RunResultMetaDataParser(
						this, metaDataPath);
				parser2.process();
			} else {
				metaDataPath = FileUtils.buildPath(
						new File(absPath.getAbsolutePath()).getParent(),
						String.format("%s_%s.metadata.complete",
								programConfig.toString(".v"),
								dataConfig.toString(".v")));
				if (new File(metaDataPath).exists()) {
					RunResultMetaDataParser parser2 = new RunResultMetaDataParser(
							this, metaDataPath);
					parser2.process();
				}
			}
		} catch (Exception e) {
			unloadFromMemory();
			throw new RunResultParseException(e.getMessage());
		}
	}
}
