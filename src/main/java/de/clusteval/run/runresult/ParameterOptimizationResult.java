/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run.runresult;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.cluster.FileBackedClustering;
import de.clusteval.cluster.IClustering;
import de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod;
import de.clusteval.cluster.quality.ClusteringQualitySet;
import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.cluster.quality.IClusteringQualitySet;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.RunResultRepository;
import de.clusteval.framework.repository.db.DatabaseException;
import de.clusteval.framework.repository.parse.Parser;
import de.clusteval.framework.repository.parse.RepositoryObjectParseException;
import de.clusteval.program.ParameterSet;
import de.clusteval.run.IParameterOptimizationRun;
import de.clusteval.run.IRun;
import de.clusteval.run.ParameterOptimizationRun;
import dk.sdu.imada.compbio.file.FileUtils;
import dk.sdu.imada.compbio.utils.Pair;

/**
 * A wrapper class for parameter optimization runresults produced by parameter
 * optimization runs.
 * 
 * @author Christian Wiwie
 * 
 */

public class ParameterOptimizationResult extends ExecutionRunResult
		implements
			Iterable<Pair<ParameterSet, IClusteringQualitySet>>,
			IParameterOptimizationResult {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3436978259181459985L;

	/**
	 * @param parentRepository
	 * @param runResultFolder
	 * @param result
	 * @param parseClusterings
	 * @param storeClusterings
	 * @param register
	 *            A boolean indicating whether to register the parsed runresult.
	 * @throws InterruptedException
	 * @return The parameter optimization run result parsed from the given
	 *         runresult folder.
	 * @throws NumberFormatException
	 */
	public static IRun parseFromRunResultFolder(
			final IRepository parentRepository, final File runResultFolder,
			final List<IRunResult> result, final boolean parseClusterings,
			final boolean storeClusterings, final boolean register)
			throws RepositoryObjectParseException, InterruptedException {
		try {
			IRepository childRepository = new RunResultRepository(
					runResultFolder.getAbsolutePath(), parentRepository);
			childRepository.initialize();
			try {

				File runFile = null;
				File configFolder = new File(FileUtils.buildPath(
						runResultFolder.getAbsolutePath(), "configs"));
				if (!configFolder.exists())
					return null;
				for (File child : configFolder.listFiles())
					if (child.getName().endsWith(".run")) {
						runFile = child;
						break;
					}
				if (runFile == null)
					return null;
				final IRun run = Parser.parseRunFromFile(runFile);

				if (run instanceof ParameterOptimizationRun) {
					final IParameterOptimizationRun paramRun = (IParameterOptimizationRun) run;

					// TODO
					// List<ParameterOptimizationResult> result = new
					// ArrayList<ParameterOptimizationResult>();

					File clusterFolder = new File(FileUtils.buildPath(
							runResultFolder.getAbsolutePath(), "clusters"));
					for (final IParameterOptimizationMethod method : paramRun
							.getOptimizationMethods()) {
						File completeFile = new File(FileUtils.buildPath(
								clusterFolder.getAbsolutePath(),
								method.getProgramConfig().toString(".v") + "_"
										+ method.getDataConfig().toString(".v")
										+ ".results.qual.complete"));
						// fallback to file names without version
						if (!completeFile.exists()
								&& method.getDataConfig().getVersion()
										.equals(new ComparableVersion("1"))
								&& method.getProgramConfig().getVersion()
										.equals(new ComparableVersion("1"))) {
							String fallback = FileUtils.buildPath(
									clusterFolder.getAbsolutePath(),
									method.getProgramConfig().getName() + "_"
											+ method.getDataConfig().getName()
											+ ".results.qual.complete");
							if (new File(fallback).exists())
								completeFile = new File(fallback);

						}
						final ParameterOptimizationResult tmpResult = parseFromRunResultCompleteFile(
								parentRepository, paramRun, method,
								completeFile, parseClusterings,
								storeClusterings, register);
						if (tmpResult != null)
							result.add(tmpResult);

					}
				}
				return run;
			} finally {
				childRepository.terminateSupervisorThread();
			}
		} catch (RepositoryObjectParseException e) {
			throw e;
		} catch (DatabaseException e) {
			// cannot happen
			return null;
		} catch (InterruptedException e) {
			throw e;
		} catch (Exception e) {
			// run results are not versioned -> always use version '1'
			throw new RepositoryObjectParseException(RunResult.class,
					runResultFolder.getName(), "1", e);
		}
	}

	/**
	 * @param repository
	 * @param run
	 * @param method
	 * @param completeFile
	 * @param parseClusterings
	 * @param storeClusterings
	 * @param register
	 *            A boolean indicating whether to register the parsed runresult.
	 * @return The parameter optimization run result parsed from the given
	 *         runresult folder.
	 * @throws RegisterException
	 * @throws RunResultParseException
	 */
	public static ParameterOptimizationResult parseFromRunResultCompleteFile(
			IRepository repository, IParameterOptimizationRun run,
			IParameterOptimizationMethod method, File completeFile,
			final boolean parseClusterings, final boolean storeClusterings,
			final boolean register)
			throws RegisterException, RunResultParseException {
		ParameterOptimizationResult result = null;
		if (completeFile.exists()) {
			result = new ParameterOptimizationResult(repository,
					completeFile.lastModified(), completeFile,
					completeFile.getParentFile().getParentFile().getName(), run,
					method, parseClusterings, storeClusterings);

			if (register) {
				result.loadIntoMemory();
				try {
					result.register();
				} finally {
					result.unloadFromMemory();
				}
			}
		}
		return result;
	}

	/*
	 * Belongs to a optimization method
	 */
	protected IParameterOptimizationMethod method;

	protected Map<Long, IClusteringQualitySet> iterationToQualities;

	// added 20.08.2012
	protected Map<Long, IClustering> iterationToClustering;

	// added 04.04.2013
	protected Map<ParameterSet, Set<Long>> parameterSetToIterationNumber;

	/*
	 * We keep track of the optimal parameter sets
	 */
	protected Map<IClusteringQualityMeasure, Long> optimalIterations;

	protected IClusteringQualitySet optimalCriterionValue;

	protected Map<IClusteringQualityMeasure, IClustering> optimalClustering;

	protected Map<Long, ParameterSet> iterationToParameterSets;

	protected List<Long> iterationNumbers;

	protected boolean parseClusterings, storeClusterings;

	protected Set<Long> iterationsWithoutQuality;

	/**
	 * By default we do not parse clusterings.
	 * 
	 * @param repository
	 * @param changeDate
	 * @param absPath
	 * @param runIdentString
	 * @param run
	 * @param method
	 * 
	 */
	public ParameterOptimizationResult(final IRepository repository,
			final long changeDate, final File absPath,
			final String runIdentString, final IRun run,
			final IParameterOptimizationMethod method) {
		this(repository, changeDate, absPath, runIdentString, run, method,
				false, false);
	}

	/**
	 * Use this constructor if you want to parse clusterings as well. They will
	 * be stored in a map from parameter sets to the clusterings.
	 * 
	 * @param repository
	 * @param changeDate
	 * @param absPath
	 * @param runIdentString
	 * @param run
	 * @param method
	 * @param parseClusterings
	 *            Whether to parse the clusterings from the file system.
	 * @param storeClusterings
	 *            Whether to store parsed clusterings in RAM.
	 */
	public ParameterOptimizationResult(final IRepository repository,
			final long changeDate, final File absPath,
			final String runIdentString, final IRun run,
			final IParameterOptimizationMethod method,
			final boolean parseClusterings, final boolean storeClusterings) {
		super(repository, changeDate, absPath, runIdentString, run,
				method.getDataConfig(), method.getProgramConfig());
		this.method = method;
		this.parseClusterings = parseClusterings;
		this.storeClusterings = storeClusterings;

		this.initAttributes(true);
	}

	protected void initAttributes(final boolean onlyOptimalityAttributes) {
		this.optimalCriterionValue = new ClusteringQualitySet();
		this.optimalIterations = new HashMap<IClusteringQualityMeasure, Long>();
		this.optimalClustering = new HashMap<IClusteringQualityMeasure, IClustering>();
		this.iterationToParameterSets = new TreeMap<Long, ParameterSet>();
		this.iterationsWithoutQuality = Collections
				.synchronizedSet(new HashSet<Long>());
		this.iterationNumbers = new ArrayList<Long>();
		this.parameterSetToIterationNumber = new HashMap<ParameterSet, Set<Long>>();
		this.iterationToMetaAttributeValues = new HashMap<>();
		if (!onlyOptimalityAttributes) {
			this.iterationToQualities = new TreeMap<Long, IClusteringQualitySet>();
			this.iterationToClustering = new TreeMap<Long, IClustering>();
		}
	}

	/**
	 * The copy constructor of run results.
	 * 
	 * @param other
	 *            The object to clone.
	 */
	public ParameterOptimizationResult(
			final ParameterOptimizationResult other) {
		super(other);

		this.method = other.method.clone();
		this.iterationToQualities = cloneParameterSetQualities(
				other.iterationToQualities);
		this.iterationsWithoutQuality = new HashSet<Long>(
				other.iterationsWithoutQuality);
		this.optimalCriterionValue = other.optimalCriterionValue.clone();
		this.optimalIterations = cloneOptimalParameterSets(
				other.optimalIterations);
		this.iterationToParameterSets = cloneParameterSets(
				other.iterationToParameterSets);
		this.iterationNumbers = cloneIterationNumbers(other.iterationNumbers);
		this.parameterSetToIterationNumber = cloneParameterSetToIterationNumbers(
				other.parameterSetToIterationNumber);
		this.iterationToClustering = cloneParameterSetsToClustering(
				other.iterationToClustering);
		this.optimalClustering = cloneOptimalClustering(
				other.optimalClustering);
		this.iterationToMetaAttributeValues = new HashMap<>(
				other.iterationToMetaAttributeValues);
	}

	private Map<IClusteringQualityMeasure, IClustering> cloneOptimalClustering(
			Map<IClusteringQualityMeasure, IClustering> optimalClustering) {
		final Map<IClusteringQualityMeasure, IClustering> result = new HashMap<IClusteringQualityMeasure, IClustering>();

		for (Map.Entry<IClusteringQualityMeasure, IClustering> entry : optimalClustering
				.entrySet()) {
			result.put(entry.getKey().clone(), entry.getValue().clone());
		}

		return result;
	}

	private Map<Long, IClustering> cloneParameterSetsToClustering(
			Map<Long, IClustering> parameterSetToClustering) {
		final Map<Long, IClustering> result = new TreeMap<Long, IClustering>();

		for (Map.Entry<Long, IClustering> entry : parameterSetToClustering
				.entrySet()) {
			result.put(entry.getKey(), entry.getValue().clone());
		}

		return result;
	}

	private Map<Long, ParameterSet> cloneParameterSets(
			Map<Long, ParameterSet> parameterSets) {
		final Map<Long, ParameterSet> result = new TreeMap<Long, ParameterSet>();

		for (Long l : parameterSets.keySet()) {
			result.put(l, parameterSets.get(l).clone());
		}

		return result;
	}

	private List<Long> cloneIterationNumbers(List<Long> iterationNumbers) {
		return new ArrayList<Long>(iterationNumbers);
	}

	private Map<ParameterSet, Set<Long>> cloneParameterSetToIterationNumbers(
			Map<ParameterSet, Set<Long>> iterationNumbers) {
		final Map<ParameterSet, Set<Long>> result = new HashMap<ParameterSet, Set<Long>>();

		for (Map.Entry<ParameterSet, Set<Long>> entry : iterationNumbers
				.entrySet()) {
			result.put(entry.getKey().clone(),
					new HashSet<Long>(entry.getValue()));
		}

		return result;
	}

	private Map<IClusteringQualityMeasure, Long> cloneOptimalParameterSets(
			Map<IClusteringQualityMeasure, Long> optimalParameterSet) {
		final Map<IClusteringQualityMeasure, Long> result = new HashMap<IClusteringQualityMeasure, Long>();

		for (Map.Entry<IClusteringQualityMeasure, Long> entry : optimalParameterSet
				.entrySet()) {
			result.put(entry.getKey().clone(), entry.getValue());
		}

		return result;
	}

	private Map<Long, IClusteringQualitySet> cloneParameterSetQualities(
			Map<Long, IClusteringQualitySet> parameterSetToQualities) {
		final Map<Long, IClusteringQualitySet> result = new TreeMap<Long, IClusteringQualitySet>();

		for (Map.Entry<Long, IClusteringQualitySet> entry : parameterSetToQualities
				.entrySet()) {
			result.put(entry.getKey(), entry.getValue().clone());
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.runresult.RunResult#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IParameterOptimizationResult#clone()
	 */
	@Override
	public IParameterOptimizationResult clone() {
		return new ParameterOptimizationResult(this);
	}

	/**
	 * A convenience method for
	 * {@link #put(long, ParameterSet, ClusteringQualitySet, FileBackedClustering)}.
	 * 
	 * @param iterationNumber
	 *            The number of the iteration.
	 * 
	 * @param last
	 *            The parameter set for which we want to add clustering
	 *            qualities.
	 * @param qualities
	 *            The qualities which we want to add for the parameter set.
	 * @return The old value, if this operation replaced an old mapping,
	 */
	// public ClusteringQualitySet put(long iterationNumber, ParameterSet last,
	// ClusteringQualitySet qualities) {
	// return this.put(iterationNumber, last, qualities, null);
	// }

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IParameterOptimizationResult#put(long,
	 * de.clusteval.cluster.quality.ClusteringQualitySet,
	 * de.clusteval.cluster.Clustering)
	 */
	@Override
	public IClusteringQualitySet put(long iterationNumber,
			IClusteringQualitySet qualities, IClustering clustering) {
		// we want that this result is parsed again
		changedSinceLastRegister = true;

		IClusteringQualitySet result = null;

		if (this.iterationToClustering != null)
			this.iterationToClustering.put(iterationNumber, clustering);

		if (qualities != null) {
			if (this.iterationToQualities != null)
				result = this.iterationToQualities.put(iterationNumber,
						qualities);
			for (IClusteringQualityMeasure measure : qualities.keySet()) {
				if (optimalCriterionValue.get(measure) == null
						|| measure.isBetterThan(qualities.get(measure),
								this.optimalCriterionValue.get(measure))) {

					this.optimalCriterionValue.put(measure,
							qualities.get(measure));
					this.optimalIterations.put(measure, iterationNumber);

					if (this.iterationToClustering != null)
						this.optimalClustering.put(measure, clustering);
				}
			}
		}
		iterationsWithoutQuality.remove(iterationNumber);

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IParameterOptimizationResult#
	 * getOptimalParameterSet()
	 */
	@Override
	public ParameterSet getOptimalParameterSet() {
		if (this.optimalIterations != null)
			return this.iterationToParameterSets.get(this.optimalIterations
					.get(this.getRun().getOptimizationCriterion()));
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IParameterOptimizationResult#
	 * getOptimalParameterSets()
	 */
	@Override
	public Map<IClusteringQualityMeasure, Long> getOptimalIterations() {
		return this.optimalIterations;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IParameterOptimizationResult#
	 * getOptimalCriterionValue()
	 */
	@Override
	public IClusteringQualitySet getOptimalCriterionValue() {
		return this.optimalCriterionValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IParameterOptimizationResult#
	 * getOptimalClustering()
	 */
	@Override
	public IClustering getOptimalClustering() {
		if (this.optimalClustering != null)
			return this.optimalClustering
					.get(this.getRun().getOptimizationCriterion());
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IParameterOptimizationResult#
	 * getOptimalClusterings()
	 */
	@Override
	public Map<IClusteringQualityMeasure, IClustering> getOptimalClusterings() {
		return this.optimalClustering;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IParameterOptimizationResult#
	 * getOptimizationQualities()
	 */
	@Override
	public Map<Long, IClusteringQualitySet> getOptimizationQualities() {
		return this.iterationToQualities;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runresult.IParameterOptimizationResult#addParameterSet(
	 * long, de.clusteval.program.ParameterSet)
	 */
	@Override
	public void addParameterSet(final long iterationNumber,
			final ParameterSet paramSet) {
		this.iterationToParameterSets.put(iterationNumber, paramSet);
		if (!this.parameterSetToIterationNumber.containsKey(paramSet)) {
			this.parameterSetToIterationNumber.put(paramSet,
					new HashSet<Long>());
		}
		this.parameterSetToIterationNumber.get(paramSet).add(iterationNumber);
		this.iterationsWithoutQuality.add(iterationNumber);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runresult.IParameterOptimizationResult#getParameterSets(
	 * )
	 */
	@Override
	public Map<Long, ParameterSet> getParameterSets() {
		return this.iterationToParameterSets;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IParameterOptimizationResult#
	 * getParameterSetsList()
	 */
	@Override
	public List<ParameterSet> getParameterSetsList() {
		ParameterSet[] params = new ParameterSet[this.iterationToParameterSets
				.size()];
		Iterator<Entry<Long, ParameterSet>> iterator = this.iterationToParameterSets
				.entrySet().iterator();
		int pos = 0;
		while (iterator.hasNext()) {
			Map.Entry<Long, ParameterSet> e = iterator.next();
			params[pos] = e.getValue();
			pos++;
		}

		return Arrays.asList(params);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IParameterOptimizationResult#
	 * getIterationsWithoutQuality()
	 */
	@Override
	public Set<Long> getIterationsWithoutQuality() {
		return iterationsWithoutQuality;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IParameterOptimizationResult#
	 * getIterationNumbers()
	 */
	@Override
	public List<Long> getIterationNumbers() {
		return this.iterationNumbers;
	}

	/**
	 * @return the iterationToClustering
	 */
	@Override
	public Map<Long, IClustering> getIterationToClustering() {
		return iterationToClustering;
	}

	/**
	 * @return the parseClusterings
	 */
	@Override
	public boolean isParseClusterings() {
		return parseClusterings;
	}

	/**
	 * @return the storeClusterings
	 */
	@Override
	public boolean isStoreClusterings() {
		return storeClusterings;
	}

	/**
	 * @param parseClusterings
	 *            the parseClusterings to set
	 */
	@Override
	public void setParseClusterings(boolean parseClusterings) {
		this.parseClusterings = parseClusterings;
	}

	/**
	 * @param storeClusterings
	 *            the storeClusterings to set
	 */
	@Override
	public void setStoreClusterings(boolean storeClusterings) {
		this.storeClusterings = storeClusterings;
	}

	/**
	 * @return the iterationToParameterSets
	 */
	@Override
	public Map<Long, ParameterSet> getIterationToParameterSets() {
		return iterationToParameterSets;
	}

	/**
	 * @return the iterationToQualities
	 */
	@Override
	public Map<Long, IClusteringQualitySet> getIterationToQualities() {
		return iterationToQualities;
	}

	/**
	 * @return the parameterSetToIterationNumber
	 */
	@Override
	public Map<ParameterSet, Set<Long>> getParameterSetToIterationNumber() {
		return parameterSetToIterationNumber;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IParameterOptimizationResult#
	 * getIterationNumberForParameterSet(de.clusteval.program.ParameterSet)
	 */
	@Override
	public Set<Long> getIterationNumberForParameterSet(
			final ParameterSet parameterSet) {
		try {
			if (this.parameterSetToIterationNumber.containsKey(parameterSet))
				return this.parameterSetToIterationNumber.get(parameterSet);
			return new HashSet<Long>();
		} catch (NullPointerException e) {
			System.out.println("bla");
			throw e;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IParameterOptimizationResult#
	 * getOptimizationClusterings()
	 */
	@Override
	public List<Pair<ParameterSet, IClustering>> getOptimizationClusterings() {
		List<Pair<ParameterSet, IClustering>> result = new ArrayList<Pair<ParameterSet, IClustering>>();
		for (ParameterSet paramSet : this.iterationToParameterSets.values())
			result.add(Pair.getPair(paramSet,
					this.iterationToClustering.get(paramSet)));
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IParameterOptimizationResult#getMethod()
	 */
	@Override
	public IParameterOptimizationMethod getMethod() {
		return this.method;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#getRun()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IParameterOptimizationResult#getRun()
	 */
	@Override
	public IParameterOptimizationRun getRun() {
		return (IParameterOptimizationRun) super.getRun();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#isInMemory()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IParameterOptimizationResult#isInMemory()
	 */
	@Override
	public boolean isInMemory() {
		return this.iterationNumbers != null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#loadIntoMemory()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runresult.IParameterOptimizationResult#loadIntoMemory()
	 */
	@Override
	public void loadIntoMemory() throws RunResultParseException {
		this.loadIntoMemory(true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runresult.IParameterOptimizationResult#loadIntoMemory(
	 * boolean)
	 */
	@Override
	public void loadIntoMemory(final boolean onlyOptimalResults)
			throws RunResultParseException {

		this.initAttributes(onlyOptimalResults);
		// the parser fills the attributes of this run result object
		ParameterOptimizationResultParser parser;
		try {
			parser = new ParameterOptimizationResultParser(method,
					this.getRun(), this, absPath.getAbsolutePath(), new int[]{},
					new int[]{}, parseClusterings, storeClusterings);
			parser.process();

			super.loadIntoMemory();
		} catch (Exception e) {
			unloadFromMemory();
			throw new RunResultParseException(e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#register()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IParameterOptimizationResult#register()
	 */
	@Override
	public boolean register() throws RegisterException {
		// TODO: is this the smoothest way?
		// we need to somewhere register / unregister the clusterings before we
		// register this result, because we are not by default storing the
		// clusterings anymore in the repository
		// register all the clusterings
		try {
			this.loadIntoMemory(false);
			if (this.iterationToClustering != null) {
				for (IClustering cl : this.iterationToClustering.values()) {
					if (cl != null)
						cl.register();
				}
			}
			boolean res = super.register();
			if (this.iterationToClustering != null) {
				// unregister all the clusterings
				for (IClustering cl : this.iterationToClustering.values()) {
					if (cl != null)
						cl.unregister();
				}
			}
			return res;
		} catch (RunResultParseException e) {
			e.printStackTrace();
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runresult.IParameterOptimizationResult#unloadFromMemory(
	 * )
	 */
	@Override
	public void unloadFromMemory() {
		if (this.iterationNumbers != null) {
			this.iterationNumbers.clear();
			this.iterationNumbers = null;
		}
		if (this.optimalClustering != null) {
			this.optimalClustering.clear();
			this.optimalClustering = null;
		}
		if (this.optimalCriterionValue != null) {
			this.optimalCriterionValue.clear();
			this.optimalCriterionValue = null;
		}
		if (this.optimalIterations != null) {
			this.optimalIterations.clear();
			this.optimalIterations = null;
		}
		if (this.iterationToParameterSets != null) {
			this.iterationToParameterSets.clear();
			this.iterationToParameterSets = null;
		}
		if (this.iterationNumbers != null) {
			this.iterationNumbers.clear();
			this.iterationNumbers = null;
		}
		if (this.iterationToClustering != null) {
			this.iterationToClustering.clear();
			this.iterationToClustering = null;
		}
		if (this.iterationToQualities != null) {
			this.iterationToQualities.clear();
			this.iterationToQualities = null;
		}
		if (this.iterationsWithoutQuality != null) {
			this.iterationsWithoutQuality.clear();
			this.iterationsWithoutQuality = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IParameterOptimizationResult#get(long)
	 */
	@Override
	public IClusteringQualitySet get(final long iterationNumber) {
		return this.iterationToQualities != null
				? this.iterationToQualities.get(iterationNumber)
				: null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runresult.IParameterOptimizationResult#getClustering(
	 * long)
	 */
	@Override
	public IClustering getClustering(final long iteration) {
		return this.iterationToClustering != null
				? this.iterationToClustering.get(iteration)
				: null;
	}

	/**
	 * @param run
	 *            The run corresponding to the runresult folder.
	 * @param childRepository
	 *            The repository in which we want to register the runresult.
	 * @param runResultFolder
	 *            A file object referencing the runresult folder.
	 * @param result
	 *            The list of runresults this method fills.
	 * @param parseClusterings
	 *            Whether to parse clusterings.
	 * @param storeClusterings
	 *            Whether to store clusterings, if they are parsed.
	 * @param register
	 *            A boolean indicating whether to register the parsed runresult.
	 * @return The parameter optimization run parsed from the runresult folder.
	 * @throws RegisterException
	 * @throws RunResultParseException
	 */
	public static IRun parseFromRunResultFolder(
			final IParameterOptimizationRun run, final IRepository childRepository,
			final File runResultFolder, final List<IRunResult> result,
			final boolean parseClusterings, final boolean storeClusterings,
			final boolean register)
			throws RegisterException, RunResultParseException {

		File clusterFolder = new File(FileUtils
				.buildPath(runResultFolder.getAbsolutePath(), "clusters"));
		for (final IParameterOptimizationMethod method : run
				.getOptimizationMethods()) {
			File completeFile = new File(
					FileUtils.buildPath(clusterFolder.getAbsolutePath(),
							method.getProgramConfig().toString(".v") + "_"
									+ method.getDataConfig().toString(".v")
									+ ".results.qual.complete"));
			// fallback to file names without version
			if (!completeFile.exists()
					&& method.getDataConfig().getVersion()
							.equals(new ComparableVersion("1"))
					&& method.getProgramConfig().getVersion()
							.equals(new ComparableVersion("1"))) {

				String fallback = FileUtils.buildPath(
						clusterFolder.getAbsolutePath(),
						method.getProgramConfig().getName() + "_"
								+ method.getDataConfig().getName()
								+ ".results.qual.complete");
				if (new File(fallback).exists())
					completeFile = new File(fallback);
			}
			final ParameterOptimizationResult tmpResult = parseFromRunResultCompleteFile(
					childRepository, run, method, completeFile, parseClusterings,
					storeClusterings, register);
			if (tmpResult != null)
				result.add(tmpResult);

		}
		return run;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IParameterOptimizationResult#iterator()
	 */
	@Override
	public Iterator<Pair<ParameterSet, IClusteringQualitySet>> iterator() {
		return new ParameterOptimizationResultIterator(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.ExecutionRunResult#asSerializable()
	 */
	@Override
	public SerializableParameterOptimizationRunResult asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableParameterOptimizationRunResult) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#asSerializable()
	 */
	@Override
	public SerializableParameterOptimizationRunResult asSerializableInternal()
			throws RepositoryObjectSerializationException {
		// ensure that the clustering is set before serialization
		boolean isLoaded = this.isInMemory(), isParsing = this.parseClusterings,
				isStoring = this.storeClusterings;
		try {
			this.parseClusterings = false;
			this.storeClusterings = false;
			this.loadIntoMemory(false);
			SerializableParameterOptimizationRunResult<IParameterOptimizationResult> serializableClusteringRunResult = new SerializableParameterOptimizationRunResult<IParameterOptimizationResult>(
					this);
			if (!isLoaded)
				this.unloadFromMemory();
			this.parseClusterings = isParsing;
			this.storeClusterings = isStoring;
			return serializableClusteringRunResult;
		} catch (Exception e) {
			throw new RepositoryObjectSerializationException(IRunResult.class,
					this.getName(), e);
		}
	}
}

class ParameterOptimizationResultIterator
		implements
			Iterator<Pair<ParameterSet, IClusteringQualitySet>> {

	protected ParameterOptimizationResult result;

	protected int currPos;

	protected Iterator<Long> iterationIter;

	public ParameterOptimizationResultIterator(
			final ParameterOptimizationResult result) {
		super();
		this.result = result;
		this.currPos = 0;
		this.iterationIter = this.result.iterationToParameterSets.keySet()
				.iterator();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Iterator#hasNext()
	 */
	@Override
	public boolean hasNext() {
		/*
		 * Number of iterations
		 */
		return this.currPos < result.getParameterSets().size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Iterator#next()
	 */
	@Override
	public Pair<ParameterSet, IClusteringQualitySet> next() {
		long iteration = iterationIter.next();
		return Pair.getPair(result.getParameterSets().get(iteration),
				result.getOptimizationQualities().get(iteration));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Iterator#remove()
	 */
	@Override
	public void remove() {
		// not supported
	}
}
