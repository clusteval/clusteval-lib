/**
 * 
 */
package de.clusteval.run.runresult.format;

import java.io.File;

import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RepositoryObjectDeserializationException;
import de.clusteval.framework.repository.SerializableWrapperDynamicComponent;
import de.clusteval.utils.DynamicComponentInitializationException;

/**
 * @author Christian Wiwie
 *
 */
public class SerializableRunResultFormat
		extends
			SerializableWrapperDynamicComponent<IRunResultFormat>
		implements
			ISerializableRunResultFormat {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5242095812777089910L;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 */
	public SerializableRunResultFormat(File absPath, String name,
			String version, final String alias) {
		super(IRunResultFormat.class, absPath, name, version, alias);
	}

	/**
	 * @param wrappedComponent
	 */
	public SerializableRunResultFormat(IRunResultFormat wrappedComponent) {
		super(IRunResultFormat.class, wrappedComponent);
	}

	@Override
	protected IRunResultFormat deserializeInternal(final IRepository repository)
			throws DeserializationException {
		try {
			return RunResultFormat.parseFromString(repository, this.name);
		} catch (UnknownRunResultFormatException
				| DynamicComponentInitializationException e) {
			throw new RepositoryObjectDeserializationException(
					IRunResultFormat.class, this.name, e);
		}
	}
}