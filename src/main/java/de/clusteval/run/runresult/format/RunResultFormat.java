/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run.runresult.format;

import java.io.File;
import java.lang.reflect.InvocationTargetException;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.Repository;
import de.clusteval.framework.repository.RepositoryObjectDynamicComponent;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.utils.ClassVersion;
import de.clusteval.utils.ClassVersionRequirement;
import de.clusteval.utils.ClustEvalAlias;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.DynamicComponentVersion;
import dk.sdu.imada.compbio.utils.Pair;

//@formatter:off
/**
 * Run results (e.g. clusterings) can have different formats. For all kinds of
 * operations the framework needs to know which format a runresult has and how
 * it can be converted to an understandable (standard) format.
 * 
 * <p>
 * Every runresult format comes together with a parser class (see
 * {@link RunResultFormatParser}).
 * 
 * A runresult format `MyRunResultFormat` can be added to ClustEval by
 * 
 * 1. extending the {@link RunResultFormat} class with your own class `MyRunResultFormat`.
 *     - Your class has to be decorated with the following annotations:
 *         - {@link DynamicComponentVersion}: Any component you add to ClustEval needs to have a version. This is needed to ensure, that results are fully reproducible by running runs using exactly the same versions as previously
 *         - {@link ClassVersionRequirement}: Any component you add to ClustEval needs to specify, which dependencies it has on the ClustEval interfaces. This information is used to ensure compatibility between the version of your component and the used ClustEval version. Whenever core interfaces in the ClustEval API change, their interface version is incremented. By using this annotation it can detect, which components are still compatible.
 *         - {@link ClustEvalAlias}: You need to provide a readable alias for your component. This alias is used when results and components should be presented in a accessible way.
 *     - You have to provide your own implementations for the following methods:
 *         * {@link #RunResultFormat(IRepository, long, File)}: The constructor of your runresult format class. This constructor has to be implemented and public, otherwise the framework will not be able to load your runresult format.
 *         * {@link #RunResultFormat(RunResultFormat)}: The copy constructor of your class taking another instance of your class. This constructor has to be implemented and public.
 * 2. extending the class {@link RunResultFormatParser} with your own class `MyRunResultFormatParser`.
 *     - Your class has to be decorated with the following annotations:
 *         - {@link DynamicComponentVersion}: Any component you add to ClustEval needs to have a version. This is needed to ensure, that results are fully reproducible by running runs using exactly the same versions as previously
 *         - {@link ClassVersionRequirement}: Any component you add to ClustEval needs to specify, which dependencies it has on the ClustEval interfaces. This information is used to ensure compatibility between the version of your component and the used ClustEval version. Whenever core interfaces in the ClustEval API change, their interface version is incremented. By using this annotation it can detect, which components are still compatible.
 *     - You have to provide your own implementations for the following methods, otherwise the framework will not be able to load your class.
 *         * {@link RunResultFormatParser#convertToStandardFormat()}: This method converts the given runresult to the standard runresult format of the framework. The converted runresult has to be named exactly as the input file postfixed with the extension ".conv". The original runresult <REPOSITORY ROOT>/results/<runIdentifier>/clusters/TransClust sfld.1.result has to be converted to <REPOSITORY ROOT>/results/<runIdentifier>/clusters/TransClust sfld.1.result.conv by this method. A wrapper object for the converted runresult has be stored in the result attribute.
 * 3. Creating a jar file named `MyRunResultFormat.jar` containing the `MyRunResultFormat.class` and `MyRunResultFormatParser.class` compiled on your machine in the correct folder structure corresponding to the packages:
 *     * `de/clusteval/run/result/format/MyRunResultFormat.class`
 *     * `de/clusteval/run/result/format/MyRunResultFormatParser.class`
 * 4. Putting the `MyRunResultFormat.jar` into the runresult formats folder of the repository: `<REPOSITORY ROOT>/supp/formats/runresult`
 * 
 * 
 * The backend server will recognize and try to load the new runresult format automatically the next time, the {@link RunResultFormatFinderThread} checks the filesystem.
 * 
 * @author Christian Wiwie
 */
//@formatter:on
@ClassVersion(version = "2.1")
public abstract class RunResultFormat extends RepositoryObjectDynamicComponent
		implements
			IRunResultFormat {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6136079287296448012L;

	/**
	 * This method parses a runresult format from the given string, containing a
	 * runresult format class name.
	 * 
	 * @param repository
	 *            The repository where to look up the runresult format class.
	 * @param runResultFormat
	 *            The runresult format class name as string.
	 * @return The parsed runresult format.
	 * @throws UnknownRunResultFormatException
	 * @throws DynamicComponentInitializationException
	 */
	public static IRunResultFormat parseFromString(final IRepository repository,
			String runResultFormat) throws UnknownRunResultFormatException,
			DynamicComponentInitializationException {

		Class<? extends IRunResultFormat> c = repository
				.resolveAndGetRegisteredClass(IRunResultFormat.class,
						runResultFormat, true);
		if (c == null) {
			Pair<String, ComparableVersion> fullClassNameAndVersion = repository
					.getFullClassNameAndVersion(IRunResultFormat.class,
							runResultFormat, true);
			if (fullClassNameAndVersion.getSecond() != null
					&& repository.hasError(IRunResultFormat.class,
							fullClassNameAndVersion.getFirst(),
							fullClassNameAndVersion.getSecond().toString()))
				throw new UnknownRunResultFormatException(runResultFormat,
						repository.getErrors(IRunResultFormat.class,
								fullClassNameAndVersion.getFirst(),
								fullClassNameAndVersion.getSecond().toString())
								.iterator().next());
			else
				throw new UnknownRunResultFormatException(runResultFormat,
						"Unknown run result format");
		}
		return getInstance(repository, runResultFormat, c);
	}

	protected static IRunResultFormat getInstance(final IRepository repository,
			String runResultFormat, Class<? extends IRunResultFormat> c)
			throws UnknownRunResultFormatException,
			DynamicComponentInitializationException {
		try {
			return c.getConstructor(IRepository.class, long.class, File.class)
					.newInstance(repository, System.currentTimeMillis(),
							new File(runResultFormat));
		} catch (NoSuchMethodException e) {
			throw new DynamicComponentInitializationException(
					IRunResultFormat.class, runResultFormat,
					"The class is missing the correct constructor");
		} catch (Exception e) {
			// this is likely because we passed faked parameters
			return null;
		}
	}

	/**
	 * Instantiates a new runresult format.
	 * 
	 * @param repo
	 * @param changeDate
	 * @param absPath
	 * @throws RegisterException
	 */
	public RunResultFormat(final IRepository repo, final long changeDate,
			final File absPath) throws RegisterException {
		super(repo, changeDate, absPath);
	}

	/**
	 * The copy constructor of runresult formats.
	 * 
	 * @param other
	 *            The object to clone.
	 * @throws RegisterException
	 */
	public RunResultFormat(final RunResultFormat other)
			throws RegisterException {
		super(other);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.format.IRunResultFormat#equals(java.lang.
	 * Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof RunResultFormat))
			return false;

		RunResultFormat other = (RunResultFormat) obj;

		return this.getClass().equals(other.getClass());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.format.IRunResultFormat#hashCode()
	 */
	@Override
	public int hashCode() {
		return this.getClass().hashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.format.IRunResultFormat#clone()
	 */
	@Override
	public final RunResultFormat clone() {
		try {
			return this.getClass().getConstructor(this.getClass())
					.newInstance(this);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		this.log.warn("Cloning instance of class "
				+ this.getClass().getSimpleName() + " failed");
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#getName()
	 */
	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	@Override
	public ComparableVersion getVersion() {
		try {
			return Repository.getVersionOfObjectDynamicClass(this);
		} catch (Exception e) {
			// cannot happen because we loaded the class before;
			e.printStackTrace();
			return new ComparableVersion("1");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObjectDynamicComponent#
	 * asSerializable()
	 */
	@Override
	public SerializableRunResultFormat asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableRunResultFormat) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#asSerializable()
	 */
	@Override
	public SerializableRunResultFormat asSerializableInternal() {
		return new SerializableRunResultFormat(this);
	}

	/**
	 * 
	 * @return An instance of the run result format parser corresponding to this
	 *         format class.
	 */
	@Override
	public abstract Class<? extends RunResultFormatParser> getRunResultFormatParser();
}
