/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run.runresult.format;

import java.io.IOException;
import java.util.Map;

import de.clusteval.data.IDataConfig;
import de.clusteval.run.runresult.IClusteringRunResult;
import dk.sdu.imada.compbio.utils.parse.TextFileParser;

/**
 * @author Christian Wiwie
 */
public abstract class RunResultFormatParser extends TextFileParser
		implements
			IRunResultFormatParser {

	protected Map<String, String> params;

	protected Map<String, String> internalParams;

	protected IClusteringRunResult result;

	protected IDataConfig dataConfig;

	/**
	 * Instantiates a new run result format parser.
	 * 
	 * @param internalParams
	 * 
	 * @param params
	 *            the params
	 * @param absFilePath
	 *            the abs file path
	 * @param dataConfig
	 *            The data configuration that was clustered to come up with this
	 *            run result.
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public RunResultFormatParser(final Map<String, String> internalParams,
			final Map<String, String> params, final String absFilePath,
			final IDataConfig dataConfig) throws IOException {
		super(absFilePath, new int[0], new int[0], true, null,
				absFilePath + ".conv", OUTPUT_MODE.STREAM);
		this.setLockTargetFile(true);
		this.params = params;
		this.internalParams = internalParams;
		this.dataConfig = dataConfig;
	}

	/**
	 * Instantiates a new run result format parser.
	 * 
	 * @param internalParams
	 * 
	 * @param params
	 *            the params
	 * @param absFilePath
	 *            the abs file path
	 * @param splitChar
	 *            The character to use to split the input file
	 * @param dataConfig
	 *            The data configuration that was clustered to come up with this
	 *            run result.
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public RunResultFormatParser(final Map<String, String> internalParams,
			final Map<String, String> params, final String absFilePath,
			String splitChar, final IDataConfig dataConfig) throws IOException {
		super(absFilePath, new int[0], new int[0], true, splitChar,
				absFilePath + ".conv", OUTPUT_MODE.STREAM);
		this.setLockTargetFile(true);
		this.params = params;
		this.internalParams = internalParams;
		this.dataConfig = dataConfig;
	}

	/**
	 * Instantiates a new run result format parser.
	 * 
	 * @param internalParams
	 * 
	 * @param params
	 *            the params
	 * @param absFilePath
	 *            the abs file path
	 * @param splitLines
	 *            the split lines
	 * @param outputMode
	 *            the output mode
	 * @param dataConfig
	 *            The data configuration that was clustered to come up with this
	 *            run result.
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public RunResultFormatParser(final Map<String, String> internalParams,
			final Map<String, String> params, final String absFilePath,
			final boolean splitLines, final OUTPUT_MODE outputMode,
			final IDataConfig dataConfig) throws IOException {
		super(absFilePath, new int[0], new int[0], splitLines, null,
				absFilePath + ".conv", outputMode);
		this.params = params;
		this.internalParams = internalParams;
		this.dataConfig = dataConfig;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runresult.format.IRunResultFormatParser#getRunResult()
	 */
	@Override
	public IClusteringRunResult getRunResult() {
		return this.result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.format.IRunResultFormatParser#
	 * convertToStandardFormat()
	 */
	@Override
	public abstract void convertToStandardFormat() throws IOException;
}
