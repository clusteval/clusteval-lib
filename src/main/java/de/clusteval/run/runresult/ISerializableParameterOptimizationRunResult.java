/**
 * 
 */
package de.clusteval.run.runresult;

import java.util.List;
import java.util.Map;
import java.util.Set;

import de.clusteval.cluster.ISerializableClustering;
import de.clusteval.cluster.paramOptimization.ISerializableParameterOptimizationMethod;
import de.clusteval.cluster.quality.ISerializableClusteringQualityMeasure;
import de.clusteval.cluster.quality.ISerializableClusteringQualitySet;
import de.clusteval.program.ParameterSet;
import dk.sdu.imada.compbio.utils.Pair;

/**
 * @author Christian Wiwie
 *
 * @param <R>
 */
public interface ISerializableParameterOptimizationRunResult<R extends IParameterOptimizationResult>
		extends
			ISerializableExecutionRunResult<R> {

	/**
	 * @return the iterationNumbers
	 */
	List<Long> getIterationNumbers();

	/**
	 * @return the iterationsWithoutQuality
	 */
	Set<Long> getIterationsWithoutQuality();

	/**
	 * @return the iterationToClustering
	 */
	Map<Long, ISerializableClustering> getIterationToClustering();

	/**
	 * @return the iterationToParameterSets
	 */
	Map<Long, ParameterSet> getIterationToParameterSets();

	/**
	 * @return the iterationToQualities
	 */
	Map<Long, ISerializableClusteringQualitySet> getIterationToQualities();

	/**
	 * @return the method
	 */
	ISerializableParameterOptimizationMethod getMethod();

	/**
	 * @return the optimalClustering
	 */
	Map<ISerializableClusteringQualityMeasure, ISerializableClustering> getOptimalClustering();

	/**
	 * @return the optimalCriterionValue
	 */
	ISerializableClusteringQualitySet getOptimalCriterionValue();

	/**
	 * @return the optimalParameterSet
	 */
	Map<ISerializableClusteringQualityMeasure, Long> getOptimalIterations();

	/**
	 * @return the parameterSetToIterationNumber
	 */
	Map<ParameterSet, Set<Long>> getParameterSetToIterationNumber();

	/**
	 * @return the parseClusterings
	 */
	boolean isParseClusterings();

	/**
	 * @return the storeClusterings
	 */
	boolean isStoreClusterings();

}