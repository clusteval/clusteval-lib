/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run.runresult;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.data.IDataConfig;
import de.clusteval.data.statistics.DataStatistic;
import de.clusteval.data.statistics.IDataStatistic;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.InvalidRepositoryException;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryAlreadyExistsException;
import de.clusteval.framework.repository.RepositoryCouldNotBeMigratedException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.RepositoryVersionTooNewException;
import de.clusteval.framework.repository.RepositoryVersionTooOldException;
import de.clusteval.framework.repository.RunResultRepository;
import de.clusteval.framework.repository.config.RepositoryConfigNotFoundException;
import de.clusteval.framework.repository.config.RepositoryConfigurationException;
import de.clusteval.framework.repository.db.DatabaseException;
import de.clusteval.framework.repository.parse.ParseException;
import de.clusteval.framework.repository.parse.Parser;
import de.clusteval.run.DataAnalysisRun;
import de.clusteval.run.IRun;
import de.clusteval.utils.ISerializableStatistic;
import de.clusteval.utils.IStatistic;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * @author Christian Wiwie
 * 
 */
public class DataAnalysisRunResult
		extends
			AnalysisRunResult<IDataConfig, IDataStatistic>
		implements
			IDataAnalysisRunResult {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1811717253450333798L;

	/**
	 * @param repository
	 * @param changeDate
	 * @param absPath
	 * @param runIdentString
	 * @param run
	 */
	public DataAnalysisRunResult(IRepository repository, long changeDate,
			File absPath, String runIdentString, final IRun run) {
		super(repository, changeDate, absPath, runIdentString, run);
	}

	/**
	 * The copy constructor for data analysis run results.
	 * 
	 * @param other
	 *            The object to clone.
	 */
	public DataAnalysisRunResult(final DataAnalysisRunResult other) {
		super(other);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.runresult.AnalysisRunResult#cloneStatistics(java.util.Map)
	 */
	@Override
	protected Map<IDataConfig, Map<IDataStatistic, String>> cloneStatistics(
			Map<IDataConfig, Map<IDataStatistic, String>> statistics) {
		final Map<IDataConfig, Map<IDataStatistic, String>> result = new HashMap<>();

		for (Map.Entry<IDataConfig, Map<IDataStatistic, String>> entry : statistics
				.entrySet()) {
			Map<IDataStatistic, String> newList = new HashMap<>();

			for (IDataStatistic elem : entry.getValue().keySet()) {
				newList.put(elem.clone(), entry.getValue().get(elem));
			}

			result.put(entry.getKey().clone(), newList);
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.runresult.AnalysisRunResult#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IDataAnalysisRunResult#clone()
	 */
	@Override
	public IDataAnalysisRunResult clone() {
		return new DataAnalysisRunResult(this);
	}

	/**
	 * @param parentRepository
	 * @param runResultFolder
	 * @return The data analysis run result parsed from the given runresult
	 *         folder.
	 * 
	 * @throws RepositoryAlreadyExistsException
	 * @throws RepositoryAlreadyExistsException
	 * @throws InvalidRepositoryException
	 * @throws ParseException
	 * @throws AnalysisRunResultException
	 * @throws RepositoryConfigurationException
	 * @throws RepositoryConfigNotFoundException
	 * @throws RegisterException
	 * @throws NumberFormatException
	 * @throws InterruptedException
	 * @throws RepositoryVersionTooNewException
	 * @throws RepositoryVersionTooOldException
	 * @throws FileNotFoundException
	 * @throws RepositoryCouldNotBeMigratedException
	 */
	public static IDataAnalysisRunResult parseFromRunResultFolder(
			final IRepository parentRepository, final File runResultFolder)
			throws RepositoryAlreadyExistsException, InvalidRepositoryException,
			ParseException, RepositoryVersionTooOldException,
			RepositoryVersionTooNewException, FileNotFoundException,
			RepositoryConfigNotFoundException, RepositoryConfigurationException,
			RegisterException, InterruptedException, AnalysisRunResultException,
			RepositoryCouldNotBeMigratedException {
		try {
			IRepository childRepository = new RunResultRepository(
					runResultFolder.getAbsolutePath(), parentRepository);
			childRepository.initialize();

			File runFile = null;
			File configFolder = new File(FileUtils
					.buildPath(runResultFolder.getAbsolutePath(), "configs"));
			if (!configFolder.exists())
				return null;
			for (File child : configFolder.listFiles())
				if (child.getName().endsWith(".run")) {
					runFile = child;
					break;
				}
			if (runFile == null)
				return null;
			final IRun object = Parser.parseRunFromFile(runFile);

			DataAnalysisRunResult analysisResult = null;

			if (object instanceof DataAnalysisRun) {
				final DataAnalysisRun run = (DataAnalysisRun) object;

				File analysesFolder = new File(FileUtils.buildPath(
						runResultFolder.getAbsolutePath(), "analyses"));

				analysisResult = new DataAnalysisRunResult(parentRepository,
						analysesFolder.lastModified(), analysesFolder,
						analysesFolder.getParentFile().getName(), run);

				for (final IDataConfig dataConfig : run.getDataConfigs()) {

					Map<IDataStatistic, String> statistics = new HashMap<>();
					for (final IStatistic dataStatistic : run.getStatistics()) {
						final File completeFile = new File(FileUtils.buildPath(
								analysesFolder.getAbsolutePath(),
								dataConfig.toString(".v") + "_"
										+ dataStatistic.toString(".v")
										+ ".txt"));
						if (!completeFile.exists())
							throw new AnalysisRunResultException(
									"The result file of (" + dataConfig + ","
											+ dataStatistic.toString()
											+ ") could not be found: "
											+ completeFile);
						final String fileContents = FileUtils
								.readStringFromFile(
										completeFile.getAbsolutePath());

						dataStatistic.parseFromString(fileContents);
						statistics.put((DataStatistic) dataStatistic,
								fileContents);

					}
					analysisResult.put(dataConfig, statistics);
				}
				analysisResult.register();
			}
			return analysisResult;
		} catch (DatabaseException e) {
			// cannot happen
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IDataAnalysisRunResult#getDataConfigs()
	 */
	@Override
	public Set<IDataConfig> getDataConfigs() {
		return this.statisticValues.keySet();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runresult.IDataAnalysisRunResult#getDataStatistics(de.
	 * clusteval.data.DataConfig)
	 */
	@Override
	public Map<IDataStatistic, String> getDataStatisticValues(
			final IDataConfig dataConfig) {
		return this.statisticValues.get(dataConfig);
	}

	/**
	 * @param run
	 * @param parentRepository
	 * @param runResultFolder
	 * @return The data analysis run result parsed from the given runresult
	 *         folder.
	 * @throws RegisterException
	 * @throws RunResultParseException
	 * 
	 */
	public static IDataAnalysisRunResult parseFromRunResultFolder(
			final DataAnalysisRun run, final IRepository parentRepository,
			final File runResultFolder, final List<IRunResult> result,
			final boolean register)
			throws RegisterException, RunResultParseException {

		DataAnalysisRunResult analysisResult = null;

		File analysesFolder = new File(FileUtils
				.buildPath(runResultFolder.getAbsolutePath(), "analyses"));

		analysisResult = new DataAnalysisRunResult(parentRepository,
				analysesFolder.lastModified(), analysesFolder,
				analysesFolder.getParentFile().getName(), run);

		if (register) {
			analysisResult.loadIntoMemory();
			try {
				analysisResult.register();
			} finally {
				analysisResult.unloadFromMemory();
			}
		}
		result.add(analysisResult);
		return analysisResult;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#loadIntoMemory()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IDataAnalysisRunResult#loadIntoMemory()
	 */
	@Override
	public void loadIntoMemory() throws RunResultParseException {
		for (final IDataConfig dataConfig : getRun().getDataConfigs()) {

			Map<IDataStatistic, String> statistics = new HashMap<>();
			for (final IStatistic dataStatistic : getRun().getStatistics()) {
				File completeFile = new File(FileUtils.buildPath(
						absPath.getAbsolutePath(), dataConfig.toString(".v")
								+ "_" + dataStatistic.toString(".v") + ".txt"));
				if (!completeFile.exists()) {
					// backwards compatibility
					if (dataConfig.getVersion()
							.equals(new ComparableVersion("1"))) {
						completeFile = new File(
								FileUtils.buildPath(absPath.getAbsolutePath(),
										dataConfig.getName() + "_"
												+ dataStatistic.toString(".v")
												+ ".txt"));
						if (!completeFile.exists()) {
							this.log.warn("The result file of (" + dataConfig
									+ "," + dataStatistic.toString()
									+ ") could not be found: " + completeFile
									+ "; Skipping ...");
							continue;
						}
					} else {
						this.log.warn("The result file of (" + dataConfig + ","
								+ dataStatistic.toString()
								+ ") could not be found: " + completeFile
								+ "; Skipping ...");
						continue;
					}
				}
				final String fileContents = FileUtils
						.readStringFromFile(completeFile.getAbsolutePath());

				try {
					dataStatistic.parseFromString(fileContents);
					statistics.put((DataStatistic) dataStatistic, fileContents);
				} catch (Exception e) {
					// throw new RunResultParseException(String.format(
					// "The data statistic %s could not be parsed: %s",
					// dataStatistic.toString(), e.getMessage()));
				}

			}
			this.put(dataConfig, statistics);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#isInMemory()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IDataAnalysisRunResult#isInMemory()
	 */
	@Override
	public boolean isInMemory() {
		return this.statisticValues.isEmpty();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#unloadFromMemory()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IDataAnalysisRunResult#unloadFromMemory()
	 */
	@Override
	public void unloadFromMemory() {
		this.statisticValues.clear();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#getRun()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.IDataAnalysisRunResult#getRun()
	 */
	@Override
	public DataAnalysisRun getRun() {
		return (DataAnalysisRun) super.getRun();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.runresult.RunResult#asSerializable()
	 */
	@Override
	public ISerializableRunResult<? extends IRunResult> asSerializableInternal()
			throws RepositoryObjectSerializationException {
		// ensure that the clustering is set before serialization
		boolean isLoaded = this.isInMemory();
		try {
			this.loadIntoMemory();
			SerializableDataAnalysisRunResult result = new SerializableDataAnalysisRunResult(
					absPath, this.getName(), this.getVersion().toString(),
					run.asSerializable(), runIdentString);
			for (IDataConfig dc : this.statisticValues.keySet()) {
				Map<ISerializableStatistic<IDataStatistic>, String> l = new HashMap<>();
				for (IDataStatistic s : this.statisticValues.get(dc).keySet())
					l.put(s.asSerializable(),
							this.statisticValues.get(dc).get(s));
				result.statisticValues.put(dc, l);
			}
			if (!isLoaded)
				this.unloadFromMemory();
			return result;
		} catch (Exception e) {
			throw new RepositoryObjectSerializationException(IRunResult.class,
					this.getName(), e);
		}
	}
}
