/**
 * 
 */
package de.clusteval.run.runresult;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.clusteval.cluster.ISerializableClustering;
import de.clusteval.cluster.paramOptimization.ISerializableParameterOptimizationMethod;
import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.cluster.quality.ISerializableClusteringQualityMeasure;
import de.clusteval.cluster.quality.ISerializableClusteringQualitySet;
import de.clusteval.data.ISerializableDataConfig;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.ISerializableProgramConfig;
import de.clusteval.program.ParameterSet;
import de.clusteval.run.IRun;
import de.clusteval.run.ISerializableRun;

public class SerializableParameterOptimizationRunResult<R extends IParameterOptimizationResult>
		extends
			SerializableExecutionRunResult<R>
		implements
			ISerializableParameterOptimizationRunResult<R> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2432908530772784578L;
	protected List<Long> iterationNumbers;
	protected Set<Long> iterationsWithoutQuality;
	protected Map<Long, ISerializableClustering> iterationToClustering;
	protected Map<Long, ParameterSet> iterationToParameterSets;
	protected Map<Long, ISerializableClusteringQualitySet> iterationToQualities;
	protected ISerializableParameterOptimizationMethod method;
	protected Map<ISerializableClusteringQualityMeasure, ISerializableClustering> optimalClustering;
	protected ISerializableClusteringQualitySet optimalCriterionValue;
	protected Map<ISerializableClusteringQualityMeasure, Long> optimalParameterSet;
	protected Map<ParameterSet, Set<Long>> parameterSetToIterationNumber;
	protected boolean parseClusterings;
	protected boolean storeClusterings;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 * @param run
	 * @param runIdentString
	 * @param dataConfig
	 * @param programConfig
	 * @param iterationNumbers
	 * @param iterationsWithoutQuality
	 * @param iterationToClustering
	 * @param iterationToParameterSets
	 * @param iterationToQualities
	 * @param method
	 * @param optimalClustering
	 * @param optimalCriterionValue
	 * @param optimalParameterSet
	 * @param parameterSetToIterationNumber
	 * @param parseClusterings
	 * @param storeClusterings
	 */
	public SerializableParameterOptimizationRunResult(File absPath, String name,
			String version, ISerializableRun<? extends IRun> run,
			String runIdentString, ISerializableDataConfig dataConfig,
			ISerializableProgramConfig<? extends IProgramConfig> programConfig,
			List<Long> iterationNumbers, Set<Long> iterationsWithoutQuality,
			Map<Long, ISerializableClustering> iterationToClustering,
			Map<Long, ParameterSet> iterationToParameterSets,
			Map<Long, ISerializableClusteringQualitySet> iterationToQualities,
			ISerializableParameterOptimizationMethod method,
			Map<ISerializableClusteringQualityMeasure, ISerializableClustering> optimalClustering,
			ISerializableClusteringQualitySet optimalCriterionValue,
			Map<ISerializableClusteringQualityMeasure, Long> optimalParameterSet,
			Map<ParameterSet, Set<Long>> parameterSetToIterationNumber,
			boolean parseClusterings, boolean storeClusterings) {
		super(absPath, name, version, run, runIdentString, dataConfig,
				programConfig);
		this.iterationNumbers = iterationNumbers;
		this.iterationsWithoutQuality = iterationsWithoutQuality;
		this.iterationToClustering = iterationToClustering;
		this.iterationToParameterSets = iterationToParameterSets;
		this.iterationToQualities = iterationToQualities;
		this.method = method;
		this.optimalClustering = optimalClustering;
		this.optimalCriterionValue = optimalCriterionValue;
		this.optimalParameterSet = optimalParameterSet;
		this.parameterSetToIterationNumber = parameterSetToIterationNumber;
		this.parseClusterings = parseClusterings;
		this.storeClusterings = storeClusterings;
	}

	/**
	 * @param wrappedComponent
	 * @throws RepositoryObjectSerializationException
	 */
	public SerializableParameterOptimizationRunResult(R wrappedComponent)
			throws RepositoryObjectSerializationException {
		super(wrappedComponent);
		this.iterationNumbers = new ArrayList<>(
				wrappedComponent.getIterationNumbers());
		this.iterationsWithoutQuality = new HashSet<>(
				wrappedComponent.getIterationsWithoutQuality());
		if (wrappedComponent.getIterationToClustering() != null) {
			this.iterationToClustering = new HashMap<>();
			for (long it : wrappedComponent.getIterationToClustering().keySet())
				if (wrappedComponent.getIterationToClustering().get(it) != null)
					this.iterationToClustering.put(it,
							wrappedComponent.getIterationToClustering().get(it)
									.asSerializable());
		}
		this.iterationToParameterSets = new HashMap<>(
				wrappedComponent.getIterationToParameterSets());

		if (wrappedComponent.getIterationToQualities() != null) {
			this.iterationToQualities = new HashMap<>();
			for (long it : wrappedComponent.getIterationToQualities().keySet())
				if (wrappedComponent.getIterationToQualities().get(it) != null)
					this.iterationToQualities.put(it,
							wrappedComponent.getIterationToQualities().get(it)
									.asSerializable());
		}

		this.method = wrappedComponent.getMethod().asSerializable();

		if (wrappedComponent.getOptimalClusterings() != null) {
			this.optimalClustering = new HashMap<>();
			for (IClusteringQualityMeasure m : wrappedComponent
					.getOptimalClusterings().keySet())
				if (wrappedComponent.getOptimalClusterings().get(m) != null)
					this.optimalClustering.put(m.asSerializable(),
							wrappedComponent.getOptimalClusterings().get(m)
									.asSerializable());
		}
		if (wrappedComponent.getOptimalCriterionValue() != null)
			this.optimalCriterionValue = wrappedComponent
					.getOptimalCriterionValue().asSerializable();
		this.optimalParameterSet = new HashMap<>();
		for (IClusteringQualityMeasure m : wrappedComponent
				.getOptimalIterations().keySet())
			this.optimalParameterSet.put(m.asSerializable(),
					wrappedComponent.getOptimalIterations().get(m));

		if (wrappedComponent.getParameterSetToIterationNumber() != null) {
			this.parameterSetToIterationNumber = new HashMap<>(
					wrappedComponent.getParameterSetToIterationNumber());
		}

		this.parseClusterings = wrappedComponent.isParseClusterings();
		this.storeClusterings = wrappedComponent.isStoreClusterings();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runresult.ISerializableParameterOptimizationRunResult#
	 * getIterationNumbers()
	 */
	@Override
	public List<Long> getIterationNumbers() {
		return iterationNumbers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runresult.ISerializableParameterOptimizationRunResult#
	 * getIterationsWithoutQuality()
	 */
	@Override
	public Set<Long> getIterationsWithoutQuality() {
		return iterationsWithoutQuality;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runresult.ISerializableParameterOptimizationRunResult#
	 * getIterationToClustering()
	 */
	@Override
	public Map<Long, ISerializableClustering> getIterationToClustering() {
		return iterationToClustering;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runresult.ISerializableParameterOptimizationRunResult#
	 * getIterationToParameterSets()
	 */
	@Override
	public Map<Long, ParameterSet> getIterationToParameterSets() {
		return iterationToParameterSets;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runresult.ISerializableParameterOptimizationRunResult#
	 * getIterationToQualities()
	 */
	@Override
	public Map<Long, ISerializableClusteringQualitySet> getIterationToQualities() {
		return iterationToQualities;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runresult.ISerializableParameterOptimizationRunResult#
	 * getMethod()
	 */
	@Override
	public ISerializableParameterOptimizationMethod getMethod() {
		return method;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runresult.ISerializableParameterOptimizationRunResult#
	 * getOptimalClustering()
	 */
	@Override
	public Map<ISerializableClusteringQualityMeasure, ISerializableClustering> getOptimalClustering() {
		return optimalClustering;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runresult.ISerializableParameterOptimizationRunResult#
	 * getOptimalCriterionValue()
	 */
	@Override
	public ISerializableClusteringQualitySet getOptimalCriterionValue() {
		return optimalCriterionValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runresult.ISerializableParameterOptimizationRunResult#
	 * getOptimalParameterSet()
	 */
	@Override
	public Map<ISerializableClusteringQualityMeasure, Long> getOptimalIterations() {
		return optimalParameterSet;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runresult.ISerializableParameterOptimizationRunResult#
	 * getParameterSetToIterationNumber()
	 */
	@Override
	public Map<ParameterSet, Set<Long>> getParameterSetToIterationNumber() {
		return parameterSetToIterationNumber;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runresult.ISerializableParameterOptimizationRunResult#
	 * isParseClusterings()
	 */
	@Override
	public boolean isParseClusterings() {
		return parseClusterings;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runresult.ISerializableParameterOptimizationRunResult#
	 * isStoreClusterings()
	 */
	@Override
	public boolean isStoreClusterings() {
		return storeClusterings;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.runresult.SerializableRunResult#deserializeInternal(de.
	 * clusteval.framework.repository.IRepository)
	 */
	@Override
	protected R deserializeInternal(IRepository repository)
			throws DeserializationException {
		ParameterOptimizationResult result = new ParameterOptimizationResult(
				repository, System.currentTimeMillis(), absPath, runIdentString,
				run.deserialize(repository), method.deserialize(repository));
		result.iterationNumbers = iterationNumbers;
		result.iterationsWithoutQuality = iterationsWithoutQuality;
		result.iterationToClustering = new HashMap<>();
		for (long i : iterationToClustering.keySet()) {
			result.iterationToClustering.put(i,
					iterationToClustering.get(i).deserialize(repository));
		}
		result.iterationToParameterSets = iterationToParameterSets;
		result.iterationToQualities = new HashMap<>();
		for (long i : iterationToQualities.keySet())
			result.iterationToQualities.put(i,
					iterationToQualities.get(i).deserialize(repository));
		result.method = method.deserialize(repository);
		result.optimalClustering = new HashMap<>();
		for (ISerializableClusteringQualityMeasure m : optimalClustering
				.keySet())
			result.optimalClustering.put(m.deserialize(repository),
					optimalClustering.get(m).deserialize(repository));
		result.optimalCriterionValue = optimalCriterionValue
				.deserialize(repository);
		result.optimalIterations = new HashMap<>();
		for (ISerializableClusteringQualityMeasure m : optimalParameterSet
				.keySet())
			result.optimalIterations.put(m.deserialize(repository),
					optimalParameterSet.get(m));
		result.parameterSetToIterationNumber = parameterSetToIterationNumber;
		result.parseClusterings = parseClusterings;
		result.storeClusterings = storeClusterings;

		return (R) result;
	}
}