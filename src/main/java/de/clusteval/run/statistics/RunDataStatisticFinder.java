/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run.statistics;

import java.io.File;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.DynamicComponentMissingVersionException;
import de.clusteval.utils.IncompatibleClustEvalVersionException;
import de.clusteval.utils.InvalidClustEvalVersionException;
import de.clusteval.utils.InvalidDependencyTargetException;
import de.clusteval.utils.JARFinder;
import de.clusteval.utils.JARFinderClassLoader;
import de.clusteval.utils.MissingClustEvalVersionException;

/**
 * @author Christian Wiwie
 */
public class RunDataStatisticFinder extends JARFinder<IRunDataStatistic> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4737688536551882860L;

	/**
	 * Instantiates a new data set format finder.
	 * 
	 * @param repository
	 *            the repository
	 * @throws RegisterException
	 */
	public RunDataStatisticFinder(final IRepository repository)
			throws RegisterException {
		super(repository, IRunDataStatistic.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.JARFinder#getURLClassLoader(java.io.File)
	 */
	@Override
	protected URLClassLoader getURLClassLoader0(File f,
			final ClassLoader parent) throws MalformedURLException {
		List<URL> urls = this.search(
				new File(this.repository.getBasePath(this.classToFind)));
		// load corresponding classes of URLs in list
		return new RunDataStatisticURLClassLoader(this,
				urls.toArray(new URL[0]), parent);
	}

	protected List<URL> search(final File f) throws MalformedURLException {
		List<URL> result = new ArrayList<URL>();
		if (f.isDirectory())
			for (File child : f.listFiles())
				result.addAll(search(child));
		else if (f.getName().matches(String.format(".*%s-.+.jar",
				this.classToFind.getSimpleName().substring(1))))
			result.add(f.toURI().toURL());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.JARFinder#classNamesForJARFile(java.io.File)
	 */
	@Override
	protected String[] classNamesForJARFile(File f) {
		return new String[]{
				String.format("%s%s", this.getClassNamePrefix(),
						f.getName().substring(0, f.getName().indexOf("-"))),
				String.format("%s%sCalculator", this.getClassNamePrefix(),
						f.getName().substring(0, f.getName().indexOf("-")))};
	}
}

class RunDataStatisticURLClassLoader
		extends
			JARFinderClassLoader<IRunDataStatistic> {

	/**
	 * @param urls
	 * @param parent
	 * @param loaderParent
	 */
	public RunDataStatisticURLClassLoader(RunDataStatisticFinder parent,
			URL[] urls, ClassLoader loaderParent) {
		super(parent, urls, loaderParent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.ClassLoader#loadClass(java.lang.String)
	 */
	@Override
	public Class<?> loadClass(String name) throws ClassNotFoundException {
		Class<?> result = super.loadClass(name);

		if (Modifier.isAbstract(result.getModifiers()) || result.isInterface())
			return result;

		if (name.startsWith("de.clusteval.run.statistics")) {
			if (name.endsWith("RunDataStatistic")) {
				@SuppressWarnings("unchecked")
				Class<? extends RunDataStatistic> runStatistic = (Class<? extends RunDataStatistic>) result;
				try {

					// loadClass may have been called not for the newly
					// discovered class of this JAR, but for a parent class
					// which had already been registered before;
					// thus, we only register this class, if it hasn't been
					// registered before
					if (!this.parent.getRepository()
							.isClassRegistered(runStatistic)) {
						this.parent.getRepository().registerClass(
								this.parent.getClassToFind(), runStatistic);
					}
				} catch (DynamicComponentMissingVersionException
						| DynamicComponentInitializationException
						| IncompatibleClustEvalVersionException
						| MissingClustEvalVersionException
						| InvalidClustEvalVersionException
						| InvalidDependencyTargetException e) {
					handleException(e);
				}

			} else if (name.endsWith("RunDataStatisticCalculator") && !name
					.equals("de.clusteval.run.statistics.RunDataStatisticRCalculator")) {
				@SuppressWarnings("unchecked")
				Class<? extends RunDataStatisticCalculator<? extends RunDataStatistic>> runStatisticCalculator = (Class<? extends RunDataStatisticCalculator<? extends RunDataStatistic>>) result;
				try {

					// loadClass may have been called not for the newly
					// discovered class of this JAR, but for a parent class
					// which had already been registered before;
					// thus, we only register this class, if it hasn't been
					// registered before
					if (!this.parent.getRepository()
							.isClassRegistered(runStatisticCalculator)) {
						this.parent.getRepository()
								.registerRunDataStatisticCalculator(
										runStatisticCalculator);
					}
				} catch (DynamicComponentMissingVersionException
						| IncompatibleClustEvalVersionException
						| MissingClustEvalVersionException
						| InvalidClustEvalVersionException
						| InvalidDependencyTargetException e) {
					handleException(e);
				}
			}
		}
		return result;
	}
}
