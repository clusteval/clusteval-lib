/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run.statistics;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.List;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.run.RunAnalysisRun;
import de.clusteval.utils.ClassVersion;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.Statistic;
import dk.sdu.imada.compbio.utils.Pair;

/**
 * A run statistic is a {@link Statistic}, which summarizes properties of
 * clustering run results. Run statistics are assessed by a
 * {@link RunAnalysisRun}.
 * <p/>
 * 
 * 
 * {@code
 * 
 * 
 * A run statistic MyRunStatistic can be added to ClustEval by
 * 
 * 1. extending the class :java:ref:`RunStatistic` with your own class MyRunStatistic. You have to provide your own implementations for the following methods, otherwise the framework will not be able to load your class.
 * 
 *   * :java:ref:`RunStatistic(Repository, boolean, long, File)` : The constructor for your run statistic. This constructor has to be implemented and public.
 *   * :java:ref:`RunStatistic(MyRunStatistic)` : The copy constructor for your run statistic. This constructor has to be implemented and public.
 *   * :java:ref:`Statistic.getAlias()` : See :java:ref:`Statistic.getAlias()`.
 *   * :java:ref:`Statistic.parseFromString(String)` : See :java:ref:`Statistic.parseFromString(String)`.
 *   
 * 2. extending the class :java:ref:`RunStatisticCalculator` with your own class MyRunStatisticCalculator . You have to provide your own implementations for the following methods.
 * 
 *   * :java:ref:`RunStatisticCalculator(Repository, long, File, DataConfig)` : The constructor for your run statistic calculator. This constructor has to be implemented and public.
 *   * :java:ref:`RunStatisticCalculator(MyRunStatisticCalculator)` : The copy constructor for your run statistic calculator. This constructor has to be implemented and public.
 *   * :java:ref:`RunStatisticCalculator.calculateResult()`: See :java:ref:`StatisticCalculator.calculateResult()`.
 *   * :java:ref:`StatisticCalculator.writeOutputTo(File)`: See :java:ref:`StatisticCalculator.writeOutputTo(File)`.
 *   
 * 3. Creating a jar file named MyRunStatisticCalculator.jar containing the MyRunStatistic.class and MyRunStatisticCalculator.class compiled on your machine in the correct folder structure corresponding to the packages:
 * 
 *   * de/clusteval/run/statistics/MyRunStatistic.class
 *   * de/clusteval/run/statistics/MyRunStatisticCalculator.class
 *   
 * 4. Putting the MyRunStatistic.jar into the run statistics folder of the repository:
 * 
 *   * <REPOSITORY ROOT>/supp/statistics/run
 *   * The backend server will recognize and try to load the new run statistics automatically the next time, the RunStatisticFinderThread checks the filesystem.
 * 
 * }
 * 
 * @author Christian Wiwie
 * 
 */
@ClassVersion(version = "1")
public abstract class RunStatistic extends Statistic implements IRunStatistic {

	/**
	 * 
	 */
	private static final long serialVersionUID = 118675569219912343L;

	/**
	 * @param repository
	 * @param changeDate
	 * @param absPath
	 * @throws RegisterException
	 */
	public RunStatistic(IRepository repository, long changeDate, File absPath)
			throws RegisterException {
		super(repository, changeDate, absPath);
	}

	/**
	 * The copy constructor of run statistics.
	 * 
	 * @param other
	 *            The object to clone.
	 * @throws RegisterException
	 */
	public RunStatistic(final RunStatistic other) throws RegisterException {
		super(other);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.statistics.IRunStatistic#clone()
	 */
	@Override
	public final RunStatistic clone() {
		try {
			return this.getClass().getConstructor(this.getClass())
					.newInstance(this);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		this.log.warn("Cloning instance of class "
				+ this.getClass().getSimpleName() + " failed");
		return null;
	}

	/**
	 * This method parses a string and maps it to a subclass of
	 * {@link RunStatistic} looking it up in the given repository.
	 * 
	 * @param repository
	 *            The repository to look for the classes.
	 * @param runStatistic
	 *            The string representation of a run statistic subclass.
	 * @return A subclass of {@link RunStatistic}.
	 * @throws UnknownRunStatisticException
	 * @throws DynamicComponentInitializationException
	 */
	public static IRunStatistic parseFromString(final IRepository repository,
			String runStatistic) throws UnknownRunStatisticException,
			DynamicComponentInitializationException {
		Class<? extends IRunStatistic> c = repository
				.resolveAndGetRegisteredClass(IRunStatistic.class, runStatistic,
						true);
		if (c == null) {
			Pair<String, ComparableVersion> fullClassNameAndVersion = repository
					.getFullClassNameAndVersion(IRunStatistic.class,
							runStatistic, true);
			if (fullClassNameAndVersion.getSecond() != null
					&& repository.hasError(IRunStatistic.class,
							fullClassNameAndVersion.getFirst(),
							fullClassNameAndVersion.getSecond().toString()))
				throw new UnknownRunStatisticException(runStatistic,
						repository.getErrors(IRunStatistic.class,
								fullClassNameAndVersion.getFirst(),
								fullClassNameAndVersion.getSecond().toString())
								.iterator().next());
			else
				throw new UnknownRunStatisticException(runStatistic,
						"Unknown run statistic.");
		}
		return getInstance(repository, runStatistic, c);
	}

	protected static IRunStatistic getInstance(final IRepository repository,
			String runStatistic, Class<? extends IRunStatistic> c)
			throws UnknownRunStatisticException,
			DynamicComponentInitializationException {
		try {
			IRunStatistic statistic = c
					.getConstructor(IRepository.class, long.class, File.class)
					.newInstance(repository, System.currentTimeMillis(),
							new File(runStatistic));
			return statistic;
		} catch (NoSuchMethodException e) {
			throw new DynamicComponentInitializationException(
					IRunStatistic.class, runStatistic,
					"The class is missing the correct constructor");
		} catch (Exception e) {
			// this is likely because we passed faked parameters
			return null;
		}
	}

	/**
	 * This method parses several strings and maps them to subclasses of
	 * {@link RunStatistic} looking them up in the given repository.
	 * 
	 * @param repo
	 *            The repository to look for the classes.
	 * @param runStatistics
	 *            The string representation of a run statistic subclass.
	 * @return A subclass of {@link RunStatistic}.
	 * @throws UnknownRunStatisticException
	 * @throws DynamicComponentInitializationException
	 */
	public static List<IRunStatistic> parseFromString(final IRepository repo,
			String[] runStatistics) throws UnknownRunStatisticException,
			DynamicComponentInitializationException {
		List<IRunStatistic> result = new LinkedList<IRunStatistic>();
		for (String runStatistic : runStatistics) {
			result.add(parseFromString(repo, runStatistic));
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.Statistic#asSerializable()
	 */
	@Override
	public SerializableRunStatistic asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableRunStatistic) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObjectDynamicComponent#
	 * asSerializable()
	 */
	@Override
	public SerializableRunStatistic asSerializableInternal() {
		return new SerializableRunStatistic(this);
	}
}
