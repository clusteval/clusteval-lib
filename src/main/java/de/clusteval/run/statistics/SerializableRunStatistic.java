/**
 * 
 */
package de.clusteval.run.statistics;

import java.io.File;

import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RepositoryObjectDeserializationException;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.SerializableStatistic;

public class SerializableRunStatistic
		extends
			SerializableStatistic<IRunStatistic> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4518656298879321707L;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 */
	public SerializableRunStatistic(File absPath, String name, String version,
			final String alias) {
		super(IRunStatistic.class, absPath, name, version, alias);
	}

	/**
	 * @param wrappedComponent
	 */
	public SerializableRunStatistic(IRunStatistic wrappedComponent) {
		super(IRunStatistic.class, wrappedComponent);
	}

	@Override
	protected IRunStatistic deserializeInternal(final IRepository repository)
			throws DeserializationException {
		try {
			return RunStatistic.parseFromString(repository, this.name);
		} catch (UnknownRunStatisticException
				| DynamicComponentInitializationException e) {
			throw new RepositoryObjectDeserializationException(
					IRunStatistic.class, this.name, e);
		}
	}
}