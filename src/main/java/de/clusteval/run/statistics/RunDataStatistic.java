/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run.statistics;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.List;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.run.RunDataAnalysisRun;
import de.clusteval.utils.ClassVersion;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.Statistic;
import dk.sdu.imada.compbio.utils.Pair;

/**
 * A run-data statistic is a {@link Statistic}, which summarizes relationships
 * of clustering run results and data set properties. Run-data statistics are
 * assessed by a {@link RunDataAnalysisRun}.
 * <p/>
 * 
 * {@code
 * 
 * A run-data statistic MyRunDataStatistic can be added to ClustEval by
 * 
 * 1. extending the class :java:ref:`RunDataStatistic` with your own class MyRunDataStatistic. You have to provide your own implementations for the following methods, otherwise the framework will not be able to load your class.
 * 
 *   * :java:ref:`RunDataStatistic(Repository, boolean, long, File)` : The constructor for your run-data statistic. This constructor has to be implemented and public.
 *   * :java:ref:`RunDataStatistic(MyRunDataStatistic)` : The copy constructor for your run-data statistic. This constructor has to be implemented and public.
 *   * :java:ref:`Statistic.getAlias()` : See :java:ref:`Statistic.getAlias()`.
 *   * :java:ref:`Statistic.parseFromString(String)` : See :java:ref:`Statistic.parseFromString(String)`.
 *   
 * 2. extending the class :java:ref:`RunDataStatisticCalculator` with your own class MyRunDataStatisticCalculator . You have to provide your own implementations for the following methods.
 * 
 *   * :java:ref:`RunDataStatisticCalculator(Repository, long, File, DataConfig)` : The constructor for your run-data statistic calculator. This constructor has to be implemented and public.
 *   * :java:ref:`RunDataStatisticCalculator(MyRunDataStatisticCalculator)` : The copy constructor for your run-data statistic calculator. This constructor has to be implemented and public.
 *   * :java:ref:`RunDataStatisticCalculator.calculateResult()`: See :java:ref:`StatisticCalculator.calculateResult()`.
 *   * :java:ref:`StatisticCalculator.writeOutputTo(File)`: See :java:ref:`StatisticCalculator.writeOutputTo(File)`.
 *   
 * 3. Creating a jar file named MyRunDataStatisticCalculator.jar containing the MyRunDataStatistic.class and MyRunDataStatisticCalculator.class compiled on your machine in the correct folder structure corresponding to the packages:
 * 
 *   * de/clusteval/run/statistics/MyRunDataStatistic.class
 *   * de/clusteval/run/statistics/MyRunDataStatisticCalculator.class
 *   
 * 4. Putting the MyRunDataStatistic.jar into the run-data statistics folder of the repository:
 * 
 *   * <REPOSITORY ROOT>/supp/statistics/rundata
 *   * The backend server will recognize and try to load the new run statistics automatically the next time, the RunDataStatisticFinderThread checks the filesystem.
 * 
 * }
 * 
 * @author Christian Wiwie
 * 
 */
@ClassVersion(version = "1")
public abstract class RunDataStatistic extends Statistic
		implements
			IRunDataStatistic {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2863651584023668794L;

	/**
	 * @param repository
	 * @param changeDate
	 * @param absPath
	 * @throws RegisterException
	 */
	public RunDataStatistic(IRepository repository, long changeDate,
			File absPath) throws RegisterException {
		super(repository, changeDate, absPath);
	}

	/**
	 * The copy constructor of run data statistics.
	 * 
	 * @param other
	 *            The object to clone.
	 * @throws RegisterException
	 */
	public RunDataStatistic(final RunDataStatistic other)
			throws RegisterException {
		super(other);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.statistics.IRunDataStatistic#clone()
	 */
	@Override
	public final RunDataStatistic clone() {
		try {
			return this.getClass().getConstructor(this.getClass())
					.newInstance(this);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		this.log.warn("Cloning instance of class "
				+ this.getClass().getSimpleName() + " failed");
		return null;
	}

	/**
	 * This method parses a string and maps it to a subclass of
	 * {@link RunDataStatistic} looking it up in the given repository.
	 * 
	 * @param repository
	 *            The repository to look for the classes.
	 * @param runDataStatistic
	 *            The string representation of a run-data statistic subclass.
	 * @return A subclass of {@link RunDataStatistic}.
	 * @throws UnknownRunDataStatisticException
	 * @throws DynamicComponentInitializationException
	 */
	public static IRunDataStatistic parseFromString(
			final IRepository repository, String runDataStatistic)
			throws UnknownRunDataStatisticException,
			DynamicComponentInitializationException {
		Class<? extends IRunDataStatistic> c = repository
				.resolveAndGetRegisteredClass(IRunDataStatistic.class,
						runDataStatistic, true);
		if (c == null) {
			Pair<String, ComparableVersion> fullClassNameAndVersion = repository
					.getFullClassNameAndVersion(IRunDataStatistic.class,
							runDataStatistic, true);
			if (fullClassNameAndVersion.getSecond() != null
					&& repository.hasError(IRunDataStatistic.class,
							fullClassNameAndVersion.getFirst(),
							fullClassNameAndVersion.getSecond().toString()))
				throw new UnknownRunDataStatisticException(runDataStatistic,
						repository.getErrors(IRunDataStatistic.class,
								fullClassNameAndVersion.getFirst(),
								fullClassNameAndVersion.getSecond().toString())
								.iterator().next());
			else
				throw new UnknownRunDataStatisticException(runDataStatistic,
						"Unknown RunDataStatistic.");
		}
		return getInstance(repository, runDataStatistic, c);
	}

	protected static IRunDataStatistic getInstance(final IRepository repository,
			String runDataStatistic, Class<? extends IRunDataStatistic> c)
			throws UnknownRunDataStatisticException,
			DynamicComponentInitializationException {
		try {
			IRunDataStatistic statistic = c
					.getConstructor(IRepository.class, long.class, File.class)
					.newInstance(repository, System.currentTimeMillis(),
							new File(runDataStatistic));
			return statistic;
		} catch (NoSuchMethodException e) {
			throw new DynamicComponentInitializationException(
					IRunDataStatistic.class, runDataStatistic,
					"The class is missing the correct constructor");
		} catch (Exception e) {
			// this is likely because we passed faked parameters
			return null;
		}
	}

	/**
	 * This method parses several strings and maps them to subclasses of
	 * {@link RunDataStatistic} looking them up in the given repository.
	 * 
	 * @param repo
	 *            The repository to look for the classes.
	 * @param runStatistics
	 *            The string representation of a run-data statistic subclass.
	 * @return A subclass of {@link RunDataStatistic}.
	 * @throws UnknownRunDataStatisticException
	 * @throws DynamicComponentInitializationException
	 */
	public static List<IRunDataStatistic> parseFromString(
			final IRepository repo, String[] runStatistics)
			throws UnknownRunDataStatisticException,
			DynamicComponentInitializationException {
		List<IRunDataStatistic> result = new LinkedList<IRunDataStatistic>();
		for (String runStatistic : runStatistics) {
			result.add(parseFromString(repo, runStatistic));
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.Statistic#asSerializable()
	 */
	@Override
	public SerializableRunDataStatistic asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableRunDataStatistic) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObjectDynamicComponent#
	 * asSerializable()
	 */
	@Override
	public SerializableRunDataStatistic asSerializableInternal() {
		return new SerializableRunDataStatistic(this);
	}
}
