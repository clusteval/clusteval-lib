/**
 * 
 */
package de.clusteval.run.statistics;

import java.io.File;

import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RepositoryObjectDeserializationException;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.SerializableStatistic;

public class SerializableRunDataStatistic
		extends
			SerializableStatistic<IRunDataStatistic> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1445299499080941958L;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 */
	public SerializableRunDataStatistic(File absPath, String name,
			String version, final String alias) {
		super(IRunDataStatistic.class, absPath, name, version, alias);
	}

	/**
	 * @param wrappedComponent
	 */
	public SerializableRunDataStatistic(IRunDataStatistic wrappedComponent) {
		super(IRunDataStatistic.class, wrappedComponent);
	}

	@Override
	protected IRunDataStatistic deserializeInternal(
			final IRepository repository) throws DeserializationException {
		try {
			return RunDataStatistic.parseFromString(repository, this.name);
		} catch (UnknownRunDataStatisticException
				| DynamicComponentInitializationException e) {
			throw new RepositoryObjectDeserializationException(
					IRunDataStatistic.class, this.name, e);
		}
	}
}