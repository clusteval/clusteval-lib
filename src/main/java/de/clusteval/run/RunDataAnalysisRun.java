/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration.HierarchicalINIConfiguration;

import de.clusteval.context.IContext;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryObjectDumpException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.threading.IRunSchedulerThread;
import de.clusteval.run.runnable.IRunDataAnalysisRunRunnable;
import de.clusteval.run.runnable.RunDataAnalysisRunRunnable;
import de.clusteval.run.statistics.IRunDataStatistic;
import dk.sdu.imada.compbio.utils.StringExt;

/**
 * A type of analysis run that conducts analyses on both run results and data
 * inputs together.
 * 
 * <p>
 * A run data analysis run has a list of unique run analysis run identifiers in
 * {@link #runAnalysisRunResultIdentifiers} and a list of unique data analysis
 * run identifiers in {@link #dataAnalysisRunResultIdentifiers}, that should be
 * assessed during execution of the run. Additionally they inherit a list of run
 * statistics in {@link AnalysisRun#statistics} that should be assessed for
 * every pair of run analysis and data analysis run identifier.
 * 
 * @author Christian Wiwie
 * 
 */
public class RunDataAnalysisRun
		extends
			AnalysisRun<IRunDataStatistic, IRunDataAnalysisRunRunnable>
		implements
			IRunDataAnalysisRun {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3895906327105698421L;

	/**
	 * A list of unique run analysis run identifiers to be assessed during this
	 * run.
	 */
	protected List<String> runAnalysisRunResultIdentifiers;

	/**
	 * A list of unique data analysis run identifiers to be assessed during this
	 * run.
	 */

	protected List<String> dataAnalysisRunResultIdentifiers;

	/**
	 * @param repository
	 *            The repository this run should be registered at.
	 * @param context
	 * @param name
	 *            The name of this run.
	 * @param changeDate
	 *            The date this run was performed.
	 * @param absPath
	 *            The absolute path to the file on the filesystem that
	 *            corresponds to this run.
	 * @param uniqueRunAnalysisRunIdentifiers
	 *            The list of unique run analysis run identifiers, that should
	 *            be assessed during execution of the run.
	 * @param uniqueDataAnalysisRunIdentifiers
	 *            The list of unique data analysis run identifiers, that should
	 *            be assessed during execution of the run.
	 * @param statistics
	 *            The statistics that should be assessed for the objects of
	 *            analysis.
	 */
	public RunDataAnalysisRun(IRepository repository, final IContext context,
			long changeDate, File absPath,
			List<String> uniqueRunAnalysisRunIdentifiers,
			List<String> uniqueDataAnalysisRunIdentifiers,
			List<IRunDataStatistic> statistics) {
		super(repository, context, changeDate, absPath, statistics);
		this.runAnalysisRunResultIdentifiers = uniqueRunAnalysisRunIdentifiers;
		this.dataAnalysisRunResultIdentifiers = uniqueDataAnalysisRunIdentifiers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#register()
	 */
	@Override
	public boolean register() throws RegisterException {
		if (super.register()) {
			for (IRunDataStatistic statistic : this.statistics) {
				// added 21.03.2013
				statistic.register();
				statistic.addListener(this);
			}
			return true;
		}
		return false;
	}

	/**
	 * Copy constructor of run data analysis runs.
	 * 
	 * @param other
	 *            The run to be cloned.
	 * @throws RegisterException
	 */
	protected RunDataAnalysisRun(final RunDataAnalysisRun other)
			throws RegisterException {
		super(other);

		this.runAnalysisRunResultIdentifiers = new ArrayList<String>();
		for (String s : other.runAnalysisRunResultIdentifiers)
			this.runAnalysisRunResultIdentifiers.add(s);

		this.dataAnalysisRunResultIdentifiers = new ArrayList<String>();
		for (String s : other.dataAnalysisRunResultIdentifiers)
			this.dataAnalysisRunResultIdentifiers.add(s);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.AnalysisRun#cloneStatistics(java.util.List)
	 */
	@Override
	protected List<IRunDataStatistic> cloneStatistics(
			List<IRunDataStatistic> statistics) {
		final List<IRunDataStatistic> result = new ArrayList<IRunDataStatistic>();

		for (IRunDataStatistic st : statistics)
			result.add(st.clone());

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.RunAnalysisRun#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IRunDataAnalysisRun#clone()
	 */
	@Override
	public RunDataAnalysisRun clone() {
		try {
			return new RunDataAnalysisRun(this);
		} catch (RegisterException e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.Run#getNumberOfRunRunnables()
	 */
	@Override
	protected int getNumberOfRunRunnables() {
		return 1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.Run#createAndAddRunnableForResumePair(framework.RunScheduler,
	 * int)
	 */
	@SuppressWarnings("unused")
	@Override
	protected IRunDataAnalysisRunRunnable createAndScheduleRunnableForResumePair(
			IRunSchedulerThread runScheduler, int p) {

		/*
		 * We only operate on this copy, in order to avoid multithreading
		 * problems.
		 */
		// changed 13.02.2013
		// RunDataAnalysisRun runCopy = this.clone();
		RunDataAnalysisRun runCopy = this;

		List<String> uniqueRunAnalysisRunIdentifier = this
				.getUniqueRunAnalysisRunIdentifiers();
		List<String> uniqueDataAnalysisRunIdentifier = this
				.getUniqueDataAnalysisRunIdentifiers();

		/*
		 * Start a thread with the invocation line and a path to the log file.
		 * The RunThread redirects all the output of the pr ogram into the
		 * logFile.
		 */
		final RunDataAnalysisRunRunnable t = new RunDataAnalysisRunRunnable(
				runScheduler, runCopy, runIdentString, true,
				uniqueRunAnalysisRunIdentifier, uniqueDataAnalysisRunIdentifier,
				runCopy.getStatistics());
		return t;
	}

	@Override
	protected IRunDataAnalysisRunRunnable createAndScheduleRunnableForRunPair(
			IRunSchedulerThread runScheduler,
			@SuppressWarnings("unused") int p) {

		/*
		 * We only operate on this copy, in order to avoid multithreading
		 * problems.
		 */
		RunDataAnalysisRun runCopy = this.clone();

		List<String> uniqueRunAnalysisRunIdentifier = this
				.getUniqueRunAnalysisRunIdentifiers();
		List<String> uniqueDataAnalysisRunIdentifier = this
				.getUniqueDataAnalysisRunIdentifiers();

		/*
		 * Start a thread with the invocation line and a path to the log file.
		 * The RunThread redirects all the output of the pr ogram into the
		 * logFile.
		 */
		final RunDataAnalysisRunRunnable t = new RunDataAnalysisRunRunnable(
				runScheduler, runCopy, runIdentString, false,
				uniqueRunAnalysisRunIdentifier, uniqueDataAnalysisRunIdentifier,
				runCopy.getStatistics());
		return t;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.IRunDataAnalysisRun#getUniqueRunAnalysisRunIdentifiers()
	 */
	@Override
	public List<String> getUniqueRunAnalysisRunIdentifiers() {
		return this.runAnalysisRunResultIdentifiers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.IRunDataAnalysisRun#getUniqueDataAnalysisRunIdentifiers(
	 * )
	 */
	@Override
	public List<String> getUniqueDataAnalysisRunIdentifiers() {
		return this.dataAnalysisRunResultIdentifiers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.Run#terminate()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IRunDataAnalysisRun#terminate()
	 */
	@Override
	public boolean terminate() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.Run#getUpperLimitProgress()
	 */
	@Override
	protected long getUpperLimitProgress() {
		return this.statistics.size() * 100;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.AnalysisRun#asSerializable()
	 */
	@Override
	public SerializableRunDataAnalysisRun<? extends IRunDataAnalysisRun> asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableRunDataAnalysisRun<? extends IRunDataAnalysisRun>) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.AnalysisRun#asSerializable()
	 */
	@Override
	public SerializableRunDataAnalysisRun<? extends IRunDataAnalysisRun> asSerializableInternal()
			throws RepositoryObjectSerializationException {
		return new SerializableRunDataAnalysisRun(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.Run#dumpToFileHelper(org.apache.commons.configuration.
	 * HierarchicalINIConfiguration)
	 */
	@Override
	protected void dumpToFileHelper(HierarchicalINIConfiguration conf)
			throws RepositoryObjectDumpException {
		super.dumpToFileHelper(conf);

		this.dumpRunDataStatisticsToFile(conf);
		this.dumpRunAnalysisRunResultIdentifiers(conf);
		this.dumpDataAnalysisRunResultIdentifiers(conf);
	}

	protected void dumpRunDataStatisticsToFile(
			HierarchicalINIConfiguration conf) {
		conf.addProperty("runDataStatistics", StringExt.paste(",", statistics));
	}

	protected void dumpRunAnalysisRunResultIdentifiers(
			HierarchicalINIConfiguration conf) {
		conf.addProperty("uniqueRunIdentifiers",
				StringExt.paste(",", runAnalysisRunResultIdentifiers));
	}

	protected void dumpDataAnalysisRunResultIdentifiers(
			HierarchicalINIConfiguration conf) {
		conf.addProperty("uniqueDataIdentifiers",
				StringExt.paste(",", dataAnalysisRunResultIdentifiers));
	}
}
