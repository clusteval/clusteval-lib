/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run;

import de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod;
import de.clusteval.cluster.paramOptimization.ParameterOptimizationMethodFinderThread;
import de.clusteval.cluster.quality.ClusteringQualityMeasureFinderThread;
import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.context.ContextFinderThread;
import de.clusteval.context.IContext;
import de.clusteval.data.DataConfigFinderThread;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.statistics.DataStatisticFinderThread;
import de.clusteval.data.statistics.IDataStatistic;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.threading.ISupervisorThread;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.ProgramConfigFinderThread;
import de.clusteval.run.statistics.IRunDataStatistic;
import de.clusteval.run.statistics.IRunStatistic;
import de.clusteval.run.statistics.RunDataStatisticFinderThread;
import de.clusteval.run.statistics.RunStatisticFinderThread;
import de.clusteval.utils.FinderThread;
import de.clusteval.utils.IFinder;

/**
 * A thread that uses a {@link RunFinder} to check the repository for new runs.
 * 
 * @author Christian Wiwie
 * 
 */
public class RunFinderThread extends FinderThread<IRun>
		implements
			IRunFinderThread {

	/**
	 * @param supervisorThread
	 * @param repository
	 *            The repository to check for new runs.
	 * @param checkOnce
	 *            If true, this thread only checks once for new runs.
	 * 
	 */
	public RunFinderThread(final ISupervisorThread supervisorThread,
			final IRepository repository, final boolean checkOnce) {
		super(supervisorThread, repository, IRun.class, 30000, checkOnce);
	}

	/**
	 * @param supervisorThread
	 * @param repository
	 *            The repository to check for new runs.
	 * @param sleepTime
	 *            The time between two checks.
	 * @param checkOnce
	 *            If true, this thread only checks once for new runs.
	 * 
	 */
	public RunFinderThread(final ISupervisorThread supervisorThread,
			final IRepository repository, final long sleepTime,
			final boolean checkOnce) {
		super(supervisorThread, repository, IRun.class, sleepTime, checkOnce);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.FinderThread#beforeFind()
	 */
	@Override
	protected void beforeFind() {
		if (!this.repository.isInitialized(IDataConfig.class))
			this.supervisorThread.getThread(DataConfigFinderThread.class)
					.waitFor();

		if (!this.repository.isInitialized(IProgramConfig.class))
			this.supervisorThread.getThread(ProgramConfigFinderThread.class)
					.waitFor();

		if (!this.repository.isInitialized(IDataStatistic.class))
			this.supervisorThread.getThread(DataStatisticFinderThread.class)
					.waitFor();

		if (!this.repository.isInitialized(IRunDataStatistic.class))
			this.supervisorThread.getThread(RunDataStatisticFinderThread.class)
					.waitFor();

		if (!this.repository.isInitialized(IRunStatistic.class))
			this.supervisorThread.getThread(RunStatisticFinderThread.class)
					.waitFor();

		if (!this.repository.isInitialized(IClusteringQualityMeasure.class))
			this.supervisorThread
					.getThread(ClusteringQualityMeasureFinderThread.class)
					.waitFor();

		if (!this.repository.isInitialized(IParameterOptimizationMethod.class))
			this.supervisorThread
					.getThread(ParameterOptimizationMethodFinderThread.class)
					.waitFor();

		if (!this.repository.isInitialized(IContext.class))
			this.supervisorThread.getThread(ContextFinderThread.class)
					.waitFor();

		super.beforeFind();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.FinderThread#getFinder()
	 */
	@Override
	public IFinder<IRun> getFinder() throws RegisterException {
		return new RunFinder(repository);
	}
}
