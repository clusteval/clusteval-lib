/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run;

import java.io.File;
import java.util.List;

import de.clusteval.context.IContext;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryEvent;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.RepositoryRemoveEvent;
import de.clusteval.run.runnable.IAnalysisRunRunnable;
import de.clusteval.utils.IStatistic;

/**
 * An abstract class for all run types, that involve conduction of any analyses.
 * This can involve analyses of data and/or clustering results.
 * 
 * <p>
 * An analysis run has a list of statistics in {@link #statistics}, that should
 * be assessed for the objects of analysis.
 * 
 * @author Christian Wiwie
 * @param <S>
 * 
 */
public abstract class AnalysisRun<S extends IStatistic, RUNNABLE_TYPE extends IAnalysisRunRunnable<?, ?, ?, ?, ?>>
		extends
			Run<RUNNABLE_TYPE>
		implements
			IAnalysisRun<S, RUNNABLE_TYPE> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3354282131813873856L;
	/**
	 * The statistics that should be assessed for the objects of analysis.
	 */
	protected transient List<S> statistics;

	/**
	 * @param repository
	 *            the repository
	 * @param context
	 * @param changeDate
	 *            The date this run was performed.
	 * @param absPath
	 *            The absolute path to the file on the filesystem that
	 *            corresponds to this run.
	 * @param statistics
	 *            The statistics that should be assessed for the objects of
	 *            analysis.
	 */
	public AnalysisRun(IRepository repository, final IContext context,
			long changeDate, File absPath, List<S> statistics) {
		super(repository, context, changeDate, absPath);
		this.statistics = statistics;
	}

	/**
	 * Copy constructor for analysis runs.
	 * 
	 * @param other
	 *            The analysis run to be cloned.
	 * @throws RegisterException
	 */
	public AnalysisRun(AnalysisRun<S, RUNNABLE_TYPE> other) {
		super(other);

		this.statistics = cloneStatistics(other.statistics);
	}

	/**
	 * @param statistics
	 * @return
	 */
	protected abstract List<S> cloneStatistics(List<S> statistics);

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IAnalysisRun#getStatistics()
	 */
	@Override
	public List<S> getStatistics() {
		return this.statistics;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.Run#notify(framework.repository.RepositoryEvent)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.IAnalysisRun#notify(de.clusteval.framework.repository.
	 * RepositoryEvent)
	 */
	@Override
	public void notify(RepositoryEvent e) throws RegisterException {
		super.notify(e);
		if (e instanceof RepositoryRemoveEvent) {
			RepositoryRemoveEvent event = (RepositoryRemoveEvent) e;

			if (this.statistics.contains(event.getRemovedObject())) {
				event.getRemovedObject().removeListener(this);
				this.log.info("Run " + this + ": Removed, because Statistic "
						+ event.getRemovedObject().getClass().getSimpleName()
						+ " has changed.");
				RepositoryRemoveEvent newEvent = new RepositoryRemoveEvent(
						this);
				this.unregister();
				this.notify(newEvent);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#
	 * ensureVersionsForAllComponents(java.lang.String)
	 */
	@Override
	protected String ensureVersionsForAllComponents(String fileContents) {
		String newFileContents = super.ensureVersionsForAllComponents(
				fileContents);

		for (IRepositoryObject o : this.statistics) {
			newFileContents = ensureVersionForComponent(newFileContents, o);
		}

		return newFileContents;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.Run#asSerializable()
	 */
	@Override
	public SerializableAnalysisRun<? extends IAnalysisRun<? extends IStatistic, ?>> asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableAnalysisRun<? extends IAnalysisRun<? extends IStatistic, ?>>) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#asSerializable()
	 */
	@Override
	public abstract SerializableAnalysisRun<? extends IAnalysisRun<? extends IStatistic, ?>> asSerializableInternal()
			throws RepositoryObjectSerializationException;
}
