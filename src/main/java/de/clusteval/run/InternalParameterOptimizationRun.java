/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.run;

import java.io.File;
import java.util.List;
import java.util.Map;

import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.context.IContext;
import de.clusteval.data.IDataConfig;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.threading.IRunSchedulerThread;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.run.runnable.IInternalParameterOptimizationRunRunnable;
import de.clusteval.run.runnable.InternalParameterOptimizationRunRunnable;
import de.clusteval.run.runresult.postprocessing.IRunResultPostprocessor;

/**
 * A type of execution run that does the same as
 * {@link ParameterOptimizationRun}, by using a programs internal parameter
 * optimization mode instead of doing the parameter optimization itself within
 * the framework.
 * 
 * @author Christian Wiwie
 * 
 */
public class InternalParameterOptimizationRun
		extends
			ExecutionRun<IInternalParameterOptimizationRunRunnable>
		implements
			IInternalParameterOptimizationRun {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4528384622638674693L;

	/**
	 * New objects of this type are automatically registered at the repository.
	 * 
	 * @param repository
	 *            the repository
	 * @param context
	 * @param changeDate
	 *            The date this run was performed.
	 * @param absPath
	 *            The absolute path to the file on the filesystem that
	 *            corresponds to this run.
	 * @param programConfigs
	 *            The program configurations of the new run.
	 * @param dataConfigs
	 *            The data configurations of the new run.
	 * @param qualityMeasures
	 *            The clustering quality measures of the new run.
	 * @param parameterValues
	 *            The parameter values of this run.
	 * @throws RegisterException
	 */
	public InternalParameterOptimizationRun(IRepository repository,
			final IContext context, long changeDate, File absPath,
			List<IProgramConfig> programConfigs, List<IDataConfig> dataConfigs,
			List<IClusteringQualityMeasure> qualityMeasures,
			Map<IProgramConfig, Map<IProgramParameter<?>, String>> parameterValues,
			final List<IRunResultPostprocessor> postProcessors,
			final Map<String, Integer> maxExecutionTimes)
			throws RegisterException {
		super(repository, context, changeDate, absPath, programConfigs,
				dataConfigs, qualityMeasures, parameterValues, postProcessors,
				maxExecutionTimes);
	}

	/**
	 * Copy constructor of internal parameter optimization runs.
	 * 
	 * @param otherRun
	 *            The internal parameter optimization run to be cloned.
	 * @throws RegisterException
	 */
	protected InternalParameterOptimizationRun(
			final InternalParameterOptimizationRun other)
			throws RegisterException {
		super(other);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.run.ExecutionRun#createRunRunnableFor(de.clusteval.framework
	 * .threading.IRunSchedulerThread, de.clusteval.run.ExecutionRun,
	 * de.clusteval.program.IProgramConfig, de.clusteval.data.IDataConfig,
	 * java.lang.String, boolean, java.util.Map)
	 */
	@Override
	protected <T extends ExecutionRun<IInternalParameterOptimizationRunRunnable>> IInternalParameterOptimizationRunRunnable createRunRunnableFor(
			IRunSchedulerThread runScheduler, T run,
			IProgramConfig programConfig, IDataConfig dataConfig,
			String runIdentString, boolean isResume,
			Map<IProgramParameter<?>, String> runParams) {
		return new InternalParameterOptimizationRunRunnable(runScheduler,
				(IInternalParameterOptimizationRun) run, programConfig,
				dataConfig, runIdentString, isResume, runParams);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see run.ExecutionRun#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IInternalParameterOptimizationRun#clone()
	 */
	@Override
	public InternalParameterOptimizationRun clone() {
		try {
			return new InternalParameterOptimizationRun(this);
		} catch (RegisterException e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ExecutionRun#asSerializable()
	 */
	@Override
	public SerializableInternalParameterOptimizationRun<? extends IInternalParameterOptimizationRun> asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableInternalParameterOptimizationRun<? extends IInternalParameterOptimizationRun>) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.AnalysisRun#asSerializable()
	 */
	@Override
	public SerializableInternalParameterOptimizationRun<? extends IInternalParameterOptimizationRun> asSerializableInternal()
			throws RepositoryObjectSerializationException {
		return new SerializableInternalParameterOptimizationRun(this);
	}
}
