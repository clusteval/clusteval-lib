/**
 * 
 */
package de.clusteval.run;

/**
 * @author Christian Wiwie
 *
 * @param <R>
 */
public interface ISerializableInternalParameterOptimizationRun<R extends IInternalParameterOptimizationRun>
		extends
			ISerializableExecutionRun<R> {

}