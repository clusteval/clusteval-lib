/**
 * 
 */
package de.clusteval.run;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.cluster.quality.ISerializableClusteringQualityMeasure;
import de.clusteval.context.IContext;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.ISerializableDataConfig;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.program.ISerializableProgramConfig;
import de.clusteval.run.runresult.postprocessing.IRunResultPostprocessor;
import de.clusteval.run.runresult.postprocessing.ISerializableRunResultPostprocessor;

public abstract class SerializableExecutionRun<R extends IExecutionRun<?>>
		extends
			SerializableRun<R>
		implements
			ISerializableExecutionRun<R> {

	protected final List<ISerializableDataConfig> dataConfigs;

	protected final List<ISerializableProgramConfig<? extends IProgramConfig>> programConfigs;

	protected final List<ISerializableRunResultPostprocessor> postProcessors;

	protected final List<ISerializableClusteringQualityMeasure> qualityMeasures;

	protected final Map<ISerializableProgramConfig<? extends IProgramConfig>, Map<IProgramParameter<?>, String>> fixedParameterValues;

	protected final Map<String, Integer> maxExecutionTimes;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 * @param context
	 * @param dataConfigs
	 * @param programConfigs
	 * @param postProcessors
	 * @param fixedParameterValues
	 * @param qualityMeasures
	 * @param maxExecutionTimes
	 */
	public SerializableExecutionRun(final File absPath, final String name,
			final String version,
			final ISerializableWrapperRepositoryObject<IContext> context,
			final List<ISerializableDataConfig> dataConfigs,
			final List<ISerializableProgramConfig<? extends IProgramConfig>> programConfigs,
			final List<ISerializableRunResultPostprocessor> postProcessors,
			final Map<ISerializableProgramConfig<? extends IProgramConfig>, Map<IProgramParameter<?>, String>> fixedParameterValues,
			final List<ISerializableClusteringQualityMeasure> qualityMeasures,
			final Map<String, Integer> maxExecutionTimes) {
		super(absPath, name, version, context);
		this.dataConfigs = dataConfigs;
		this.programConfigs = programConfigs;
		this.postProcessors = postProcessors;
		this.fixedParameterValues = fixedParameterValues;
		this.qualityMeasures = qualityMeasures;
		this.maxExecutionTimes = maxExecutionTimes;
	}

	/**
	 * @param run
	 */
	public SerializableExecutionRun(final R run)
			throws RepositoryObjectSerializationException {
		super(run);
		this.dataConfigs = new ArrayList<>();
		for (IDataConfig dc : run.getOriginalDataConfigs())
			this.dataConfigs.add(dc != null ? dc.asSerializable() : null);
		this.programConfigs = new ArrayList<>();
		for (IProgramConfig dc : run.getOriginalProgramConfigs())
			this.programConfigs.add(dc != null ? dc.asSerializable() : null);

		this.postProcessors = new ArrayList<>();
		for (IRunResultPostprocessor dc : run.getPostProcessors())
			this.postProcessors.add(dc != null ? dc.asSerializable() : null);
		this.qualityMeasures = new ArrayList<>();
		for (IClusteringQualityMeasure dc : run.getQualityMeasures())
			this.qualityMeasures.add(dc != null ? dc.asSerializable() : null);

		this.fixedParameterValues = new HashMap<>();
		for (Entry<IProgramConfig, Map<IProgramParameter<?>, String>> e : run
				.getFixedParameterValues().entrySet())
			this.fixedParameterValues.put(e.getKey().asSerializable(),
					e.getValue());

		this.maxExecutionTimes = new HashMap<>();
		for (IProgramConfig pc : run.getOriginalProgramConfigs())
			this.maxExecutionTimes.put(pc.getName(),
					run.getMaxExecutionTime(pc));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ISerializableExecutionRun#getDataConfigs()
	 */
	@Override
	public List<ISerializableDataConfig> getDataConfigs() {
		return dataConfigs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ISerializableExecutionRun#getProgramConfigs()
	 */
	@Override
	public List<ISerializableProgramConfig<? extends IProgramConfig>> getProgramConfigs() {
		return programConfigs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ISerializableExecutionRun#getQualityMeasures()
	 */
	@Override
	public List<ISerializableClusteringQualityMeasure> getQualityMeasures() {
		return qualityMeasures;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.ISerializableExecutionRun#getPostProcessors()
	 */
	@Override
	public List<ISerializableRunResultPostprocessor> getPostProcessors() {
		return postProcessors;
	}

	protected List<IClusteringQualityMeasure> deserializeClusteringQualityMeasures(
			IRepository repository) throws DeserializationException {
		List<IClusteringQualityMeasure> qualityMeasures = new ArrayList<>();
		for (ISerializableClusteringQualityMeasure obj : this.qualityMeasures)
			qualityMeasures.add(obj.deserialize(repository));
		return qualityMeasures;
	}

	protected List<IProgramConfig> deserializeProgramConfigs(
			IRepository repository) throws DeserializationException {
		List<IProgramConfig> programConfigs = new ArrayList<>();
		for (ISerializableProgramConfig<? extends IProgramConfig> obj : this.programConfigs)
			programConfigs.add(obj.deserialize(repository));
		return programConfigs;
	}

	/**
	 * @return the fixedParameterValues
	 */
	public Map<ISerializableProgramConfig<? extends IProgramConfig>, Map<IProgramParameter<?>, String>> getFixedParameterValues() {
		return fixedParameterValues;
	}

	protected List<IRunResultPostprocessor> deserializeRunResultPostprocessor(
			IRepository repository) throws DeserializationException {
		List<IRunResultPostprocessor> postProcessors = new ArrayList<>();
		for (ISerializableRunResultPostprocessor pp : this.postProcessors)
			postProcessors.add(pp.deserialize(repository));
		return postProcessors;
	}

	protected List<IDataConfig> deserializeDataConfigs(IRepository repository)
			throws DeserializationException {
		List<IDataConfig> dataConfigs = new ArrayList<>();
		for (ISerializableDataConfig obj : this.dataConfigs)
			dataConfigs.add(obj.deserialize(repository));
		return dataConfigs;
	}

	protected Map<IProgramConfig, Map<IProgramParameter<?>, String>> deserializeFixedParameterValues(
			IRepository repository) throws DeserializationException {
		Map<IProgramConfig, Map<IProgramParameter<?>, String>> fixedParameterValues = new HashMap<>();
		for (Entry<ISerializableProgramConfig<? extends IProgramConfig>, Map<IProgramParameter<?>, String>> e : this.fixedParameterValues
				.entrySet())
			fixedParameterValues.put(e.getKey().deserialize(repository),
					e.getValue());
		return fixedParameterValues;
	}
}