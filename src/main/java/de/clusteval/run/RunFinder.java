/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
package de.clusteval.run;

import java.io.File;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.Repository;
import de.clusteval.framework.repository.parse.Parser;
import de.clusteval.framework.repository.parse.RepositoryObjectParseException;
import de.clusteval.utils.FileFinder;
import dk.sdu.imada.compbio.utils.ArrayIterator;

/**
 * Objects of this class look for new run-files in the run-directory defined in
 * the corresponding repository (see {@link Repository#runBasePath}).
 * 
 * @author Christian Wiwie
 * 
 * 
 */
public class RunFinder extends FileFinder<IRun> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8025616786551210297L;

	/**
	 * Instantiates a new run finder.
	 * 
	 * @param repository
	 *            The repository to register the new runs at.
	 * @throws RegisterException
	 */
	public RunFinder(final IRepository repository) throws RegisterException {
		super(repository, IRun.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.Finder#checkFile(java.io.File)
	 */
	@Override
	protected boolean checkFile(File file) {
		return file.isFile() && file.getName().endsWith(".run");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.Finder#getIterator()
	 */
	@Override
	protected Iterator<File> getIterator() {
		return new ArrayIterator<File>(getBaseDir().listFiles());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.FileFinder#parseObjectFromFile(java.io.File)
	 */
	@Override
	protected Run parseObjectFromFile(File file) throws RepositoryObjectParseException {
		return (Run) this.parseObjectFromFile(file, false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.FileFinder#parseObjectFromFile(java.io.File,
	 * boolean)
	 */
	@Override
	protected IRun parseObjectFromFile(File file, boolean recoverFromExceptions) throws RepositoryObjectParseException {
		return Parser.parseRunFromFile(file, recoverFromExceptions);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.Finder#cleanUpKnownExceptions()
	 */
	@Override
	protected void cleanUpKnownExceptions() {
		for (Class<? extends IRun> c : new Class[]{IClusteringRun.class, IParameterOptimizationRun.class,
				IInternalParameterOptimizationRun.class, IDataAnalysisRun.class, IRunAnalysisRun.class,
				IRunDataAnalysisRun.class, IRun.class}) {
			Set<ISerializableWrapperRepositoryObject<? extends IRepositoryObject>> toRemove = new HashSet<>();
			if (!this.knownExceptions.containsKey(c))
				continue;
			for (ISerializableWrapperRepositoryObject<? extends IRepositoryObject> w : this.knownExceptions.get(c).keySet()) {
				if (!w.getFile().exists())
					toRemove.add(w);
			}
			this.knownExceptions.get(c).keySet().removeAll(toRemove);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.utils.FileFinder#handleException(java.io.File,
	 * java.lang.Class, java.lang.String, java.lang.String, java.lang.Exception)
	 */
	@Override
	public void handleException(File file, Class<? extends IRepositoryObject> clazz, String objectName,
			String objectVersion, Exception e) throws RegisterException {
		super.handleException(file, clazz, objectName, objectVersion, e);
	}
}
