/**
 * 
 */
package de.clusteval.run;

import java.io.File;
import java.util.List;
import java.util.Map;

import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.data.IDataConfig;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.run.runresult.postprocessing.IRunResultPostprocessor;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * @author Christian Wiwie
 *
 */
public class SerializableToolRuntimeEvaluationRun<R extends IToolRuntimeEvaluationRun<?>>
		extends
			SerializableClusteringRun<R>
		implements
			ISerializableToolRuntimeEvaluationRun<R> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9135203545416477564L;

	public SerializableToolRuntimeEvaluationRun(final R run)
			throws RepositoryObjectSerializationException {
		super(run);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.SerializableRun#deserializeInternal(de.clusteval.
	 * framework.repository.IRepository)
	 */
	@Override
	protected R deserializeInternal(IRepository repository)
			throws DeserializationException {
		List<IProgramConfig> programConfigs = deserializeProgramConfigs(
				repository);
		List<IDataConfig> dataConfigs = deserializeDataConfigs(repository);
		List<IClusteringQualityMeasure> qualityMeasures = deserializeClusteringQualityMeasures(
				repository);

		List<IRunResultPostprocessor> postProcessors = deserializeRunResultPostprocessor(
				repository);

		Map<IProgramConfig, Map<IProgramParameter<?>, String>> fixedParameterValues = deserializeFixedParameterValues(
				repository);

		R deserializedRun = (R) new ToolRuntimeEvaluationRun(repository,
				this.context.deserialize(repository),
				System.currentTimeMillis(),
				new File(FileUtils.buildPath(repository.getBasePath(IRun.class),
						this.name + ".v" + version + ".run")),
				programConfigs, dataConfigs, qualityMeasures,
				fixedParameterValues, postProcessors, this.maxExecutionTimes);
		return deserializedRun;

	}
}
