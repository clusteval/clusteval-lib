/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
package de.clusteval.run;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

import org.apache.commons.configuration.HierarchicalINIConfiguration;
import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.context.IContext;
import de.clusteval.framework.repository.DumpableRepositoryObject;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryEvent;
import de.clusteval.framework.repository.RepositoryObjectDumpException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.RepositoryRemoveEvent;
import de.clusteval.framework.repository.RepositoryReplaceEvent;
import de.clusteval.framework.threading.IRunSchedulerThread;
import de.clusteval.framework.threading.RunSchedulerThread;
import de.clusteval.program.IProgramParameter;
import de.clusteval.run.runnable.DataAnalysisRunRunnable;
import de.clusteval.run.runnable.ExecutionRunRunnable;
import de.clusteval.run.runnable.IRunRunnable;
import de.clusteval.run.runnable.RunRunnable;
import de.clusteval.run.runnable.RunRunnableInitializationException;
import de.clusteval.run.runresult.ClusteringRunResult;
import de.clusteval.run.runresult.IRunResult;
import de.clusteval.run.runresult.NoRunResultFormatParserException;
import dk.sdu.imada.compbio.file.FileUtils;
import dk.sdu.imada.compbio.format.Formatter;
import dk.sdu.imada.compbio.utils.Pair;
import dk.sdu.imada.compbio.utils.ProgressPrinter;

/**
 * A representation of an abstract run including configurations and results. A
 * run is an entity, that can be performed by the framework. Depending on the
 * concrete subclass of the run, actions during execution differ. After
 * execution of the run results are stored in {@link #results}.
 * 
 * <p>
 * A run corresponds to a *.run-file on the file system in the run-directory of
 * the repository. The name of the run is deduced from the filesystem in
 * {@link #getName()}, thus it is unique for every repository.
 * 
 * <p>
 * Every time a run is performed, its results are also stored in a new
 * subdirectory in the results-directory of the repository. The subdirectory is
 * named after the {@link #runIdentString}, which consists of a time-stamp and
 * the name of this run.
 * 
 * <p>
 * When a run is performed, it's divided into a number of atomic operations that
 * can be performed in parallel, objects of subclasses of {@link RunRunnable}.
 * Those are then passed to the run scheduler and are performed in any order in
 * parallel.
 * 
 * @author Christian Wiwie
 * 
 */
public abstract class Run<RUNNABLE_TYPE extends IRunRunnable<?, ?, ?, ?>>
		extends
			DumpableRepositoryObject
		implements
			IRun<RUNNABLE_TYPE> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9078062467491263111L;

	/**
	 * This method is invoked by different copy-constructors of subclasses of
	 * this Run, e.g. by ExecutionRun.ExecutionRun(ExecutionRun) to clone a
	 * given run object.
	 * 
	 * <p>
	 * It is a convenience method to clone a map containing parameter values.
	 * 
	 * @param parameterValues
	 * @return
	 */
	protected static List<Map<IProgramParameter<?>, String>> cloneParameterValues(
			List<Map<IProgramParameter<?>, String>> parameterValues) {
		List<Map<IProgramParameter<?>, String>> result = new ArrayList<Map<IProgramParameter<?>, String>>();

		for (Map<IProgramParameter<?>, String> map : parameterValues) {
			Map<IProgramParameter<?>, String> copyMap = new HashMap<IProgramParameter<?>, String>();
			for (Map.Entry<IProgramParameter<?>, String> entry : map
					.entrySet()) {
				copyMap.put(entry.getKey().clone(), entry.getValue() + "");
			}
			result.add(copyMap);
		}

		return result;
	}

	/**
	 * The starting time of the execution of this run. Is used to calculate the
	 * duration of the execution afterwards.
	 */
	protected long startTime;

	/**
	 * Unique identifier of this run - consists of a time & date stamp, as well
	 * as the name of the run. This string is inserted into the results path of
	 * every run, to avoid overwriting of any files (input, output, whatsoever)
	 * between several runs.
	 */
	protected String runIdentString;

	/**
	 * After this run was performed using the {@link #perform()} method, all the
	 * results are stored in this list. If this run is a ExecutionRun this list
	 * contains one {@link ClusteringRunResult} object for every executed
	 * combination of program and dataset.
	 */
	protected transient List<IRunResult> results;

	/**
	 * The path to the log file in the results-directory of the execution of
	 * this run.
	 */
	protected String logFilePath;

	/**
	 * Keeps track of the progress of this run when it is executed. Can be used
	 * to get a percental status.
	 */
	protected volatile ProgressPrinter progress, progressBeforeExecution;

	protected long lastEstimatedRemainingTimeSum = -1;

	/**
	 * The status of this run.
	 * <p>
	 * Initially when a Run object is created its status is
	 * {@link RUN_STATUS.INACTIVE}.
	 * 
	 * <p>
	 * When a Run should be performed it is passed to the run scheduler. Then
	 * the runs status is {@link RUN_STATUS.SCHEDULED}.
	 * 
	 * <p>
	 * As soon as the run is started by the scheduler, the runs status is
	 * {@link RUN_STATUS.RUNNING}.
	 * 
	 * <p>
	 * After the runs completion its status is {@link RUN_STATUS.FINISHED}.
	 * 
	 * <p>
	 * When the run is terminated by the user during its execution and before
	 * it's finished its status is {@link RUN_STATUS.TERMINATED}.
	 */
	protected RUN_STATUS status;

	/**
	 * Contains the runnable objects created during the execution of this run.
	 */
	protected transient List<RUNNABLE_TYPE> runnables;

	/**
	 * Every run belongs to a context.
	 */
	protected transient IContext context;

	/**
	 * The constructor of this class takes a date and configuration. It is
	 * protected, to force usage of the static method
	 * 
	 * @param repository
	 *            the repository
	 * @param context
	 *            The context of this run
	 * @param changeDate
	 *            The date this run was performed.
	 * @param absPath
	 *            The absolute path to the file on the filesystem that
	 *            corresponds to this run.
	 */
	protected Run(final IRepository repository, final IContext context,
			final long changeDate, final File absPath) {
		super(repository, changeDate, absPath);

		this.runnables = new ArrayList<RUNNABLE_TYPE>();
		this.status = RUN_STATUS.INACTIVE;
		this.context = context;
	}

	/**
	 * A copy constructor for the Run class.
	 * 
	 * <p>
	 * Runnables, run results and status of the given run are not copied into
	 * the new run.
	 * 
	 * @param otherRun
	 *            The run to clone.
	 */
	protected Run(final Run otherRun) {
		super(otherRun);

		this.runnables = new ArrayList<RUNNABLE_TYPE>();
		this.status = RUN_STATUS.INACTIVE;
		this.context = otherRun.context;
		if (otherRun.progress != null)
			this.progress = new ProgressPrinter(otherRun.progress);
		if (otherRun.progressBeforeExecution != null)
			this.progressBeforeExecution = new ProgressPrinter(
					otherRun.progressBeforeExecution);
		this.logFilePath = otherRun.logFilePath;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see framework.repository.RepositoryObject#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IRun#clone()
	 */
	@Override
	public abstract Run clone();

	/**
	 * This method is invoked by {@link #perform(RunSchedulerThread)} after
	 * completion of {@link #doPerform(RunSchedulerThread)}.
	 * 
	 * <p>
	 * Override this method to do any postcalculations on the run results after
	 * everything is finished.
	 */
	protected void afterPerform() {
		FileUtils.appendStringToFile(this.logFilePath,
				Formatter.currentTimeAsString(true, "yyyy_MM_dd-HH_mm_ss",
						Locale.UK) + "\tFinished run \"" + this.getName()
						+ "\" (Duration "
						+ Formatter.formatMsToDuration(
								System.currentTimeMillis() - startTime)
						+ ")" + System.getProperty("line.separator"));
		this.log.info("Run " + this + " - All processes finished");
		if (this.getStatus() != RUN_STATUS.TERMINATED) {
			this.setStatus(RUN_STATUS.FINISHED);
		}
	}

	/**
	 * This method is invoked by {@link #resume(RunSchedulerThread, String)}
	 * after completion of {@link #doResume(RunSchedulerThread, String)}.
	 * 
	 * <p>
	 * Override this method to do any postcalculations on the run results after
	 * everything is finished.
	 * 
	 * @param runIdentString
	 *            The unique run identifier of the results directory,
	 *            corresponding to an execution of a run, that should by
	 *            resumed.
	 */
	@SuppressWarnings("unused")
	protected void afterResume(final String runIdentString) {
		FileUtils.appendStringToFile(this.logFilePath,
				Formatter.currentTimeAsString(true, "yyyy_MM_dd-HH_mm_ss",
						Locale.UK) + "\tFinished run \"" + this.getName()
						+ "\" (Duration "
						+ Formatter.formatMsToDuration(
								System.currentTimeMillis() - startTime)
						+ ")" + System.getProperty("line.separator"));
		this.log.info("Run " + this + " - All processes finished");
		if (this.getStatus() != RUN_STATUS.TERMINATED) {
			this.setStatus(RUN_STATUS.FINISHED);
		}
	}

	/**
	 * This method is invoked by {@link #perform(RunSchedulerThread)} before
	 * {@link #doPerform(RunSchedulerThread)} is invoked.
	 * 
	 * <p>
	 * Override this method in subclasses to do any operation, that should only
	 * be done once per run execution, and before any runnable is started. This
	 * can be useful for logging or for filesystem operations like copying any
	 * input files to avoid overriding files unintentionally when they would be
	 * performed in the runnables asynchronously instead.
	 * 
	 * @throws RunInitializationException
	 * @throws RegisterException
	 */
	protected void beforePerform()
			throws IOException, RunInitializationException {
		log.info("Starting run \"" + this.getName() + "\"");
		log.info("Run mode is \"" + this.getClass().getSimpleName() + "\"");
		/*
		 * Change the status of this run
		 */
		// this.setStatus(RUN_STATUS.RUNNING);

		/*
		 * Initialize a ProgressPrinter, which keeps the progression of the run
		 */
		this.progress = new ProgressPrinter(getUpperLimitProgress(), true);
		// keep track of how much percent we already had when we started (or
		// resumed) the run
		this.progressBeforeExecution = new ProgressPrinter(
				getUpperLimitProgress(), true);

		this.startTime = System.currentTimeMillis();

		this.copyConfigurationFiles(false);

		this.logFilePath = FileUtils.buildPath(
				new File(this.getRepository().getClusterResultsBasePath())
						.getParentFile().getAbsolutePath()
						.replace("%RUNIDENTSTRING", runIdentString),
				"logs", runIdentString + ".log");
		if (!new File(this.logFilePath).exists()) {
			new File(this.logFilePath).getParentFile().mkdirs();
			new File(this.logFilePath).createNewFile();
		}

		FileUtils.appendStringToFile(this.logFilePath,
				Formatter.currentTimeAsString(true, "yyyy_MM_dd-HH_mm_ss",
						Locale.UK) + "\tStarting run \"" + this.getName() + "\""
						+ System.getProperty("line.separator"));
		/*
		 * All threads are stored in this list, to be able to wait for them
		 * asynchronously later on.
		 */
		this.runnables.clear();

		/*
		 * Reset the results list
		 */
		this.results = new ArrayList<IRunResult>();
	}

	/**
	 * This method is invoked by {@link #resume(RunSchedulerThread, String)}
	 * before {@link #doResume(RunSchedulerThread, String)} is invoked.
	 * 
	 * <p>
	 * Override this method in subclasses to do any operation, that should only
	 * be done once per run execution, and before any runnable is started. This
	 * can be useful for logging or for filesystem operations like copying any
	 * input files to avoid overriding files unintentionally when they would be
	 * performed in the runnables asynchronously instead.
	 * 
	 * @param runIdentString
	 *            The unique run identifier of the results directory,
	 *            corresponding to an execution of a run, that should by
	 *            resumed.
	 * 
	 * @throws IOException
	 * @throws RunInitializationException
	 */
	protected void beforeResume(final String runIdentString)
			throws RunInitializationException {
		try {
			log.info("RESUMING run \"" + this.getName() + "\"");
			log.info("Run mode is \"" + this.getClass().getSimpleName() + "\"");
			/*
			 * Change the status of this run
			 */
			// this.status = RUN_STATUS.RUNNING;

			/*
			 * Initialize a ProgressPrinter, which keeps the progression of the
			 * run
			 */
			this.progress = new ProgressPrinter(getUpperLimitProgress(), true);
			// keep track of how much percent we already had when we started (or
			// resumed) the run
			this.progressBeforeExecution = new ProgressPrinter(
					getUpperLimitProgress(), true);

			this.runIdentString = runIdentString;

			this.startTime = System.currentTimeMillis();

			this.copyConfigurationFiles(true);

			this.logFilePath = FileUtils.buildPath(
					new File(this.getRepository().getParent()
							.getClusterResultsBasePath()).getParentFile()
									.getAbsolutePath()
									.replace("%RUNIDENTSTRING", runIdentString),
					"logs", runIdentString + ".log");
			if (!new File(this.logFilePath).exists()) {
				new File(this.logFilePath).getParentFile().mkdirs();
				new File(this.logFilePath).createNewFile();
			}

			FileUtils.appendStringToFile(this.logFilePath,
					Formatter.currentTimeAsString(true, "yyyy_MM_dd-HH_mm_ss",
							Locale.UK) + "\tRESUMING run \"" + this.getName()
							+ "\"" + System.getProperty("line.separator"));

			/*
			 * All threads are stored in this list, to be able to wait for them
			 * asynchronously later on.
			 */
			this.runnables.clear();

			/*
			 * Reset the results list
			 */
			this.results = new ArrayList<IRunResult>();
		} catch (Exception e) {
			throw new RunInitializationException(e);
		}
	}

	/**
	 * When this run is performed, this method copies all configuration files to
	 * the results directory.
	 * <p>
	 * This method is invoked by {@link #beforePerform()} or
	 * {@link #beforeResume(String)}. Thus it is not executed asynchronously to
	 * avoid overwriting of several threads in the result directory.
	 * 
	 * @param isResume
	 *            Indicates, whether the execution of this run is a resumption
	 *            or not.
	 */
	protected void copyConfigurationFiles(final boolean isResume) {
		/*
		 * Copy all the configuration files for later reproducability
		 */
		if (!isResume) {
			File movedConfigsDir = getMovedConfigsDir();
			movedConfigsDir.mkdirs();
			this.copyToFolder(movedConfigsDir);
		} else {
			File movedConfigsDir = new File(FileUtils.buildPath(
					new File(this.getRepository().getParent()
							.getClusterResultsBasePath()).getParentFile()
									.getAbsolutePath()
									.replace("%RUNIDENTSTRING", runIdentString),
					"configs"));
			movedConfigsDir.mkdirs();
			this.copyToFolder(movedConfigsDir, false);
		}
	}

	/**
	 * This method will be invoked by {@link #doResume(RunSchedulerThread)} to
	 * create the p'th runnable for the resumption of an execution of this run
	 * and to submit it to the run scheduler.
	 * 
	 * @param runScheduler
	 *            The run scheduler to which the newly created runnable should
	 *            be passed.
	 * @param p
	 *            The index of the runnable to be created.
	 * @throws RunRunnableInitializationException
	 */
	protected abstract RUNNABLE_TYPE createAndScheduleRunnableForResumePair(
			IRunSchedulerThread runScheduler, int p)
			throws RunRunnableInitializationException;

	/**
	 * This method will be invoked by {@link #doPerform(RunSchedulerThread)} to
	 * create the p'th runnable for the execution of this run and to submit it
	 * to the run scheduler.
	 * 
	 * @param runScheduler
	 *            The run scheduler to which the newly created runnable should
	 *            be passed.
	 * @param p
	 *            The index of the runnable to be created.
	 * @throws RunRunnableInitializationException
	 */
	protected abstract RUNNABLE_TYPE createAndScheduleRunnableForRunPair(
			IRunSchedulerThread runScheduler, int p)
			throws RunRunnableInitializationException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IRun#doPerform(de.clusteval.framework.threading.
	 * RunSchedulerThread)
	 */
	@Override
	public void doPerform(final IRunSchedulerThread runScheduler)
			throws RunRunnableInitializationException {

		for (int p = 0; p < getNumberOfRunRunnables(); p++) {
			RUNNABLE_TYPE r = createAndScheduleRunnableForRunPair(runScheduler,
					p);
			this.runnables.add(r);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IRun#doResume(de.clusteval.framework.threading.
	 * RunSchedulerThread, java.lang.String)
	 */
	@Override
	@SuppressWarnings("unused")
	public void doResume(final IRunSchedulerThread runScheduler,
			final String runIdentString)
			throws RunRunnableInitializationException {

		/*
		 * Execute every "runpair", consisting of a program (i.e. its
		 * configuration) and a dataset (i.e. its configuration).
		 */
		for (int p = 0; p < getNumberOfRunRunnables(); p++) {
			RUNNABLE_TYPE r = createAndScheduleRunnableForResumePair(
					runScheduler, p);
			this.runnables.add(r);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IRun#getLogFilePath()
	 */
	@Override
	public String getLogFilePath() {
		return this.logFilePath;
	}

	/**
	 * This method constructs and returns the path to the configuration
	 * subdirectory in the results directory of this run execution.
	 * 
	 * <p>
	 * This method should only be invoked while a run is executed, because only
	 * then the unique run identification string is set.
	 * 
	 * @return The path to the configuration subdirectory in the results
	 *         directory of this run execution.
	 */
	protected File getMovedConfigsDir() {
		return new File(FileUtils.buildPath(
				new File(this.getRepository().getClusterResultsBasePath())
						.getParentFile().getAbsolutePath().replace(
								"%RUNIDENTSTRING", runIdentString),
				"configs"));
	}

	/**
	 * This method constructs and returns the path to the goldstandard
	 * subdirectory in the results directory of this run execution.
	 * 
	 * <p>
	 * This method should only be invoked while a run is executed, because only
	 * then the unique run identification string is set.
	 * 
	 * @return The path to the goldstandard subdirectory in the results
	 *         directory of this run execution.
	 */
	protected File getMovedGoldStandardsDir() {
		return new File(FileUtils.buildPath(
				new File(this.getRepository().getClusterResultsBasePath())
						.getParentFile().getAbsolutePath()
						.replace("%RUNIDENTSTRING", runIdentString),
				"goldstandards"));
	}

	/**
	 * This method constructs and returns the path to the dataset subdirectory
	 * in the results directory of this run execution.
	 * 
	 * <p>
	 * This method should only be invoked while a run is executed, because only
	 * then the unique run identification string is set.
	 * 
	 * @return The path to the dataset subdirectory in the results directory of
	 *         this run execution.
	 */
	protected File getMovedInputsDir() {
		return new File(FileUtils.buildPath(
				new File(this.getRepository().getClusterResultsBasePath())
						.getParentFile().getAbsolutePath()
						.replace("%RUNIDENTSTRING", runIdentString),
				"inputs"));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IRun#getName()
	 */
	@Override
	public String getName() {
		return this.absPath.getName().replaceAll(
				"(" + getAbsPathVersionMatchString() + "?).run$", "");
	}

	/**
	 * @return The number of run runnables this run will create. This number
	 *         will be used in the {@link #doPerform(RunSchedulerThread)} method
	 *         to create the correct number of runnables.
	 */
	protected abstract int getNumberOfRunRunnables();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IRun#getPercentFinished()
	 */
	@Override
	public float getPercentFinished() {
		if (this.status.equals(RUN_STATUS.SCHEDULED))
			return 0;
		else if (this.status.equals(RUN_STATUS.RUNNING))
			synchronized (this.progress) {
				return this.progress.getPercent();
			}
		else if (this.status.equals(RUN_STATUS.FINISHED))
			return 100;
		else if (this.status.equals(RUN_STATUS.TERMINATED))
			return this.progress.getPercent();
		return 0;
	}

	protected float getPercentFinishedBeforeExecution() {
		if (this.status.equals(RUN_STATUS.SCHEDULED))
			return 0;
		else if (this.status.equals(RUN_STATUS.RUNNING))
			synchronized (this.progressBeforeExecution) {
				return this.progressBeforeExecution.getPercent();
			}
		else if (this.status.equals(RUN_STATUS.FINISHED))
			return progressBeforeExecution.getPercent();
		else if (this.status.equals(RUN_STATUS.TERMINATED))
			return this.progressBeforeExecution.getPercent();
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IRun#getPercentFinishedDuringCurrentExecution()
	 */
	@Override
	public float getPercentFinishedDuringCurrentExecution() {
		return getPercentFinished() - getPercentFinishedBeforeExecution();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IRun#getEstimatedRemainingCPUTimeInMs()
	 */
	@Override
	public long getEstimatedRemainingCPUTimeInMs() {
		long maxEstimatedTotalTimeAllRunnables = 0l;
		Map<Object, Long> maxEstimatedTotalTime = new HashMap<Object, Long>();
		long estimatedRemainingTimeSum = 0;
		List<IRunRunnable> notStartedRunnables = new ArrayList<IRunRunnable>();
		for (IRunRunnable runnable : this.runnables) {
			if (runnable.getPercentFinished() > 0.0f
					&& runnable.getIterationRunnableRunningTimeSum() > 0l) {
				long estimatedTotalTime = (long) (runnable
						.getIterationRunnableRunningTimeSum()
						/ runnable.getPercentFinished() * 100.0f);
				// estimatedTotalTime *= (1.5 * (100 -
				// runnable.getPercentFinished()) / 100);
				List<Object> groupingObjects = getGroupingObjectForRunnable(
						runnable);
				for (Object groupingObject : groupingObjects) {
					if (!maxEstimatedTotalTime.containsKey(groupingObject)) {
						maxEstimatedTotalTime.put(groupingObject, 0l);
					}
					// how certain are we about this estimate?
					// we multiply by a factor that decreases over time from 1.5
					// to
					// 0
					if (estimatedTotalTime > maxEstimatedTotalTime
							.get(groupingObject)) {
						maxEstimatedTotalTime.put(groupingObject,
								(long) (estimatedTotalTime));// *
																// (1.5
																// *
																// (100
																// -
																// runnable.getPercentFinished())
																// /
																// 100)));
						// this.log.warn(groupingObject + ": "
						// +
						// Formatter.formatMsToDuration(maxEstimatedTotalTime.get(groupingObject)));

					}
					if (estimatedTotalTime > maxEstimatedTotalTimeAllRunnables)
						maxEstimatedTotalTimeAllRunnables = estimatedTotalTime;
				}
				estimatedRemainingTimeSum += runnable
						.getEstimatedRemainingCPUTimeInMs();
			} else {
				notStartedRunnables.add(runnable);
			}
		}

		// this.log.warn("maxEstimatedTotalTimeAllRunnables: "
		// + Formatter.formatMsToDuration(maxEstimatedTotalTimeAllRunnables));

		// for the runnables that have not been started yet, we estimate the
		// longest time of the runnables that have been started
		for (IRunRunnable runnable : notStartedRunnables) {
			List<Object> groupingObjects = getGroupingObjectForRunnable(
					runnable);
			long estimatedRemainingTimeSumAvg = 0;
			for (Object groupingObject : groupingObjects) {
				if (maxEstimatedTotalTime.containsKey(groupingObject))
					// estimatedRemainingTimeSumAvg +=
					// maxEstimatedTotalTime.get(groupingObject);
					estimatedRemainingTimeSumAvg = Math.max(
							estimatedRemainingTimeSumAvg,
							maxEstimatedTotalTime.get(groupingObject));
				else
					// estimatedRemainingTimeSumAvg +=
					// maxEstimatedTotalTimeAllRunnables * 1.5;
					estimatedRemainingTimeSumAvg = Math.max(
							estimatedRemainingTimeSumAvg,
							(long) (maxEstimatedTotalTimeAllRunnables));// *
																		// 1.5));
			}
			// estimatedRemainingTimeSumAvg /= groupingObjects.size();
			estimatedRemainingTimeSum += estimatedRemainingTimeSumAvg;
		}

		if (lastEstimatedRemainingTimeSum > -1) {
			return (lastEstimatedRemainingTimeSum + estimatedRemainingTimeSum)
					/ 2;
		}
		lastEstimatedRemainingTimeSum = estimatedRemainingTimeSum;
		return estimatedRemainingTimeSum;
	}

	private List<Object> getGroupingObjectForRunnable(IRunRunnable runnable) {
		List<Object> groupingObjects = new ArrayList<Object>();
		if (runnable instanceof ExecutionRunRunnable) {
			groupingObjects
					.add(((ExecutionRunRunnable) runnable).getProgramConfig());
			groupingObjects
					.add(((ExecutionRunRunnable) runnable).getDataConfig());
		} else if (runnable instanceof DataAnalysisRunRunnable) {
			groupingObjects
					.add(((DataAnalysisRunRunnable) runnable).getDataConfig());
		} else {
			// dummy;
			groupingObjects.add("");
		}
		return groupingObjects;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IRun#getResults()
	 */
	@Override
	public List<IRunResult> getResults() {
		return this.results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IRun#getRunIdentificationString()
	 */
	@Override
	public String getRunIdentificationString() {
		return this.runIdentString;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IRun#setRunIdentificationString(java.lang.String)
	 */
	@Override
	public void setRunIdentificationString(final String runIdentString) {
		this.runIdentString = runIdentString;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IRun#getRunRunnables()
	 */
	@Override
	public List<RUNNABLE_TYPE> getRunRunnables() {
		return this.runnables;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IRun#getStatus()
	 */
	@Override
	public RUN_STATUS getStatus() {
		return this.status;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IRun#getStartTime()
	 */
	@Override
	public long getStartTime() {
		return startTime;
	}

	/**
	 * Implement this method in subclasses to provide the number of steps this
	 * run performs before it is finished. This method is then later on used by
	 * the method {@link #getPercentFinished()} to calculate the finished
	 * percentage.
	 * 
	 * @return
	 */
	protected abstract long getUpperLimitProgress();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.wiwie.wiutils.utils.RepositoryObject#notify(utils.RepositoryEvent)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IRun#notify(de.clusteval.framework.repository.
	 * RepositoryEvent)
	 */
	@Override
	public void notify(RepositoryEvent e) throws RegisterException {
		if (e instanceof RepositoryReplaceEvent) {
			RepositoryReplaceEvent event = (RepositoryReplaceEvent) e;
			if (event.getOld().equals(this))
				super.notify(event);
		} else if (e instanceof RepositoryRemoveEvent) {
			RepositoryRemoveEvent event = (RepositoryRemoveEvent) e;
			if (event.getRemovedObject().equals(this))
				super.notify(event);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IRun#perform(de.clusteval.framework.threading.
	 * RunSchedulerThread)
	 */
	@Override
	public void perform(final IRunSchedulerThread runScheduler)
			throws IOException, RunRunnableInitializationException,
			RunInitializationException {
		beforePerform();
		doPerform(runScheduler);
		waitForRunnablesToFinish();
		afterPerform();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IRun#resume(de.clusteval.framework.threading.
	 * RunSchedulerThread, java.lang.String)
	 */
	@Override
	@SuppressWarnings("unused")
	public void resume(final IRunSchedulerThread runScheduler,
			final String runIdentString) throws MissingParameterValueException,
			IOException, NoRunResultFormatParserException,
			RunRunnableInitializationException, RunInitializationException {
		beforeResume(runIdentString);
		doResume(runScheduler, runIdentString);
		waitForRunnablesToFinish();
		afterResume(runIdentString);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IRun#setStatus(de.clusteval.run.RUN_STATUS)
	 */
	@Override
	public void setStatus(RUN_STATUS status) {
		this.status = status;
		// 31.08.2012
		// update status in DB
		this.getRepository().updateStatusOfRun(this, status.toString());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IRun#terminate()
	 */
	@Override
	public abstract boolean terminate();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IRun#getOptimizationStatus()
	 */
	@Override
	@Deprecated
	/*
	 * This method is now obsolete because we made the involved classes
	 * serializable
	 */
	public Map<Pair<String, String>, Pair<Double, Map<String, Pair<Map<String, String>, String>>>> getOptimizationStatus() {
		return null;
	}

	/**
	 * This method is invoked by {@link #perform(RunSchedulerThread)}, after
	 * completion of {@link #doPerform(RunSchedulerThread)}.
	 * 
	 * <p>
	 * It waits, until all threads (corresponding to created runnables) are
	 * finished.
	 * 
	 * <p>
	 * During this time, it updates the progress after completion of single
	 * threads, such that {@link #getPercentFinished()} returns the correct
	 * value.
	 * 
	 * <p>
	 * Additionally it checks, whether any of the threads threw exceptions and
	 * prints those exceptions.
	 * 
	 */
	protected void waitForRunnablesToFinish() {
		this.log.info("Run " + this + " - Waiting for runs to finish...");
		for (IRunRunnable r : runnables) {
			IRunRunnable t = r;
			try {
				try {
					t.waitFor();
				} catch (CancellationException e) {
				} catch (ExecutionException e) {
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IRun#getContext()
	 */
	@Override
	public IContext getContext() {
		return this.context;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IRun#getExceptions()
	 */
	@Override
	public Map<IRunRunnable, List<Exception>> getExceptions() {
		Map<IRunRunnable, List<Exception>> result = new HashMap<IRunRunnable, List<Exception>>();

		for (IRunRunnable runnable : this.runnables) {
			result.put(runnable,
					new ArrayList<Exception>(runnable.getExceptions()));
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IRun#clearExceptions()
	 */
	@Override
	public void clearExceptions() {
		for (IRunRunnable runnable : this.runnables) {
			runnable.clearExceptions();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IRun#getWarningExceptions()
	 */
	@Override
	public Map<IRunRunnable, List<Exception>> getWarningExceptions() {
		Map<IRunRunnable, List<Exception>> result = new HashMap<IRunRunnable, List<Exception>>();

		for (IRunRunnable runnable : this.runnables) {
			result.put(runnable,
					new ArrayList<Exception>(runnable.getWarningExceptions()));
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.run.IRun#clearWarningExceptions()
	 */
	@Override
	public void clearWarningExceptions() {
		for (IRunRunnable runnable : this.runnables) {
			runnable.clearWarningExceptions();
		}
	}

	/**
	 * @return the progress
	 */
	public ProgressPrinter getProgress() {
		return progress;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.IHasVersion#getVersion()
	 */
	@Override
	public ComparableVersion getVersion() {
		String name = absPath.getName().replaceAll(".run", "");
		int colonInd = name.lastIndexOf(".");
		return new ComparableVersion(name.substring(colonInd + 2));
	}

	@Override
	public SerializableRun asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableRun) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#asSerializable()
	 */
	@Override
	protected abstract SerializableRun<? extends IRun<?>> asSerializableInternal()
			throws RepositoryObjectSerializationException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#isSerializable()
	 */
	@Override
	public boolean isSerializable() {
		return true;
	}

	protected void dumpToFileHelper(final HierarchicalINIConfiguration conf)
			throws RepositoryObjectDumpException {
		try {
			conf.addProperty("mode", RUN_TYPE.getForClass(this.getClass()));
		} catch (UnknownRunTypeException e) {
			throw new RepositoryObjectDumpException(e);
		}
	}
}
