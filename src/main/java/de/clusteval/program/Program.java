/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
package de.clusteval.program;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;

import de.clusteval.context.IContext;
import de.clusteval.context.UnknownContextException;
import de.clusteval.data.DataConfig;
import de.clusteval.data.IDataConfig;
import de.clusteval.framework.RLibraryNotLoadedException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryEvent;
import de.clusteval.framework.repository.RepositoryObject;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.RepositoryRemoveEvent;
import de.clusteval.framework.repository.RepositoryReplaceEvent;
import de.clusteval.framework.repository.RunResultRepository;
import de.clusteval.utils.ClustEvalAlias;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.RNotAvailableException;

/**
 * A wrapper class for programs used by this framework.
 * 
 * <p>
 * A program object encapsulates a executable, that can be executed using the
 * {@link #exec(DataConfig, ProgramConfig, String[], Map, Map)} method. This
 * method takes the data and its configuration, the program and its
 * configuration, the complete invocation line and all parameters used for this
 * invocation.
 * 
 * @author Christian Wiwie
 * 
 */
public abstract class Program extends RepositoryObject implements IProgram {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4652579987260038020L;

	/**
	 * Instantiates a new program.
	 * 
	 * @param repository
	 *            the repository this program should be registered at.
	 * @param changeDate
	 *            The change date of this program is used for equality checks.
	 * @param absPath
	 *            The absolute path of this program.
	 */
	public Program(final IRepository repository, final long changeDate,
			final File absPath) {
		super(repository instanceof RunResultRepository
				? repository.getParent()
				: repository, changeDate, absPath);
	}

	/**
	 * The copy constructor for programs.
	 * 
	 * @param program
	 *            The program to clone.
	 * @throws RegisterException
	 */
	protected Program(final Program program) {
		super(program);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgram#clone()
	 */
	@Override
	public abstract IProgram clone();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgram#getExecutable()
	 */
	@Override
	public String getExecutable() {
		return absPath.getAbsolutePath();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#getName()
	 */
	@Override
	public String getName() {
		return this.getFullName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgram#getMajorName()
	 */
	@Override
	public String getMajorName() {
		return this.absPath.getParentFile().getName()
				.replaceAll("(" + getAbsPathVersionMatchString() + "?)$", "");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgram#getMinorName()
	 */
	@Override
	public String getMinorName() {
		return this.absPath.getName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgram#getFullName()
	 */
	@Override
	public String getFullName() {
		return getMajorName() + "/" + getMinorName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.wiwie.wiutils.utils.RepositoryObject#notify(utils.RepositoryEvent)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.IProgram#notify(de.clusteval.framework.repository.
	 * RepositoryEvent)
	 */
	@Override
	public void notify(RepositoryEvent e) throws RegisterException {
		if (e instanceof RepositoryReplaceEvent) {
			RepositoryReplaceEvent event = (RepositoryReplaceEvent) e;
			if (event.getOld().equals(this))
				super.notify(event);
		} else if (e instanceof RepositoryRemoveEvent) {
			RepositoryRemoveEvent event = (RepositoryRemoveEvent) e;
			if (event.getRemovedObject().equals(this))
				super.notify(event);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgram#exec(de.clusteval.data.DataConfig,
	 * de.clusteval.program.IProgramConfig, java.lang.String[], java.util.Map,
	 * java.util.Map)
	 */
	@Override
	public abstract Process exec(final IDataConfig dataConfig,
			final IProgramConfig programConfig, final String[] invocationLine,
			final Map<String, String> effectiveParams,
			final Map<String, String> internalParams) throws IOException,
			RNotAvailableException, RLibraryNotLoadedException,
			REngineException, REXPMismatchException, InterruptedException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgram#getContext()
	 */
	@Override
	public abstract IContext getContext() throws UnknownContextException,
			DynamicComponentInitializationException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#asSerializable()
	 */
	@Override
	public SerializableProgram<? extends IProgram> asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableProgram<? extends IProgram>) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#asSerializable()
	 */
	@Override
	public abstract SerializableProgram<? extends IProgram> asSerializableInternal()
			throws RepositoryObjectSerializationException;
}
