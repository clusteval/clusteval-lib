/**
 * 
 */
package de.clusteval.program;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.data.dataset.format.ISerializableDataSetFormat;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.run.runresult.format.ISerializableRunResultFormat;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * @author Christian Wiwie
 *
 */
public class SerializableStandaloneProgramConfig
		extends
			SerializableProgramConfig<IStandaloneProgramConfig>
		implements
			ISerializableStandaloneProgramConfig {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4651063197400895963L;

	protected List<ISerializableDataSetFormat> compatibleDataSetFormats;

	protected ISerializableRunResultFormat outputFormat;

	protected ISerializableStandaloneProgram program;

	protected String invocationFormat;

	protected String invocationFormatWithoutGoldStandard;

	protected String invocationFormatParameterOptimization;

	protected String invocationFormatParameterOptimizationWithoutGoldStandard;

	protected List<IProgramParameter<?>> params;

	protected boolean expectsNormalizedDataSet;

	protected int maxExecutionTimeMinutes;

	protected String alias;

	protected Map<String, String> envVars;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 * @param compatibleDataSetFormats
	 * @param outputFormat
	 * @param program
	 * @param invocationFormat
	 * @param invocationFormatWithoutGoldStandard
	 * @param invocationFormatParameterOptimization
	 * @param invocationFormatParameterOptimizationWithoutGoldStandard
	 * @param params
	 * @param optimizableParameters
	 * @param expectsNormalizedDataSet
	 * @param maxExecutionTimeMinutes
	 * @param alias
	 * @param envVars
	 */
	public SerializableStandaloneProgramConfig(File absPath, String name,
			String version,
			List<ISerializableDataSetFormat> compatibleDataSetFormats,
			ISerializableRunResultFormat outputFormat,
			ISerializableStandaloneProgram program,
			final String invocationFormat,
			final String invocationFormatWithoutGoldStandard,
			final String invocationFormatParameterOptimization,
			final String invocationFormatParameterOptimizationWithoutGoldStandard,
			final List<IProgramParameter<?>> params,
			final List<IProgramParameter<?>> optimizableParameters,
			final boolean expectsNormalizedDataSet,
			final int maxExecutionTimeMinutes, final String alias,
			final Map<String, String> envVars) {
		super(absPath, name, version, optimizableParameters);
		this.compatibleDataSetFormats = compatibleDataSetFormats;
		this.outputFormat = outputFormat;
		this.program = program;

		this.invocationFormat = invocationFormat;
		this.invocationFormatWithoutGoldStandard = invocationFormatWithoutGoldStandard;
		this.invocationFormatParameterOptimization = invocationFormatParameterOptimization;
		this.invocationFormatParameterOptimizationWithoutGoldStandard = invocationFormatParameterOptimizationWithoutGoldStandard;

		this.params = params;
		this.optimizableParameters = optimizableParameters;

		this.expectsNormalizedDataSet = expectsNormalizedDataSet;
		this.maxExecutionTimeMinutes = maxExecutionTimeMinutes;
		this.alias = alias;
		this.envVars = envVars;
	}

	/**
	 * @param programConfig
	 * @throws RepositoryObjectSerializationException
	 */
	public SerializableStandaloneProgramConfig(
			final IStandaloneProgramConfig programConfig)
			throws RepositoryObjectSerializationException {
		super(programConfig);
		this.compatibleDataSetFormats = new ArrayList<>();
		if (programConfig.getCompatibleDataSetFormats() != null)
			for (IDataSetFormat f : programConfig.getCompatibleDataSetFormats())
				this.compatibleDataSetFormats
						.add(f != null ? f.asSerializable() : null);
		if (programConfig.getOutputFormat() != null)
			this.outputFormat = programConfig.getOutputFormat()
					.asSerializable();
		if (programConfig.getProgram() != null)
			this.program = programConfig.getProgram().asSerializable();

		this.invocationFormat = programConfig.getInvocationFormat(false);
		this.invocationFormatWithoutGoldStandard = programConfig
				.getInvocationFormat(true);
		this.invocationFormatParameterOptimization = programConfig
				.getInvocationFormatParameterOptimization(false);
		this.invocationFormatParameterOptimizationWithoutGoldStandard = programConfig
				.getInvocationFormatParameterOptimization(true);

		this.params = programConfig.getParameters();
		this.optimizableParameters = programConfig.getOptimizableParameters();

		this.expectsNormalizedDataSet = programConfig
				.expectsNormalizedDataSet();
		this.maxExecutionTimeMinutes = programConfig
				.getMaxExecutionTimeMinutes();
		this.alias = programConfig.getProgramAlias();
		this.envVars = programConfig.getEnvVars();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ISerializableStandaloneProgramConfig#
	 * getCompatibleDataSetFormats()
	 */
	@Override
	public List<ISerializableDataSetFormat> getCompatibleDataSetFormats() {
		return compatibleDataSetFormats;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.ISerializableStandaloneProgramConfig#getOutputFormat
	 * ()
	 */
	@Override
	public ISerializableRunResultFormat getOutputFormat() {
		return outputFormat;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.ISerializableStandaloneProgramConfig#getProgram()
	 */
	@Override
	public ISerializableStandaloneProgram getProgram() {
		return program;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.ISerializableProgramConfig#expectsNormalizedDataSet(
	 * )
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ISerializableStandaloneProgramConfig#
	 * expectsNormalizedDataSet()
	 */
	@Override
	public boolean expectsNormalizedDataSet() {
		return this.expectsNormalizedDataSet;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ISerializableProgramConfig#getInvocationFormat(
	 * boolean)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ISerializableStandaloneProgramConfig#
	 * getInvocationFormat(boolean)
	 */
	@Override
	public String getInvocationFormat(boolean withoutGoldStandard) {
		if (withoutGoldStandard)
			return invocationFormatWithoutGoldStandard;
		return invocationFormat;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ISerializableProgramConfig#
	 * getInvocationFormatParameterOptimization(boolean)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ISerializableStandaloneProgramConfig#
	 * getInvocationFormatParameterOptimization(boolean)
	 */
	@Override
	public String getInvocationFormatParameterOptimization(
			boolean withoutGoldStandard) {
		if (withoutGoldStandard)
			return invocationFormatParameterOptimizationWithoutGoldStandard;
		return invocationFormatParameterOptimization;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ISerializableStandaloneProgramConfig#
	 * getMaxExecutionTimeMinutes()
	 */
	@Override
	public int getMaxExecutionTimeMinutes() {
		return this.maxExecutionTimeMinutes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.ISerializableStandaloneProgramConfig#getParams()
	 */
	@Override
	public List<IProgramParameter<?>> getParameters() {
		return params;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ISerializableProgramConfig#
	 * supportsInternalParameterOptimization()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ISerializableStandaloneProgramConfig#
	 * supportsInternalParameterOptimization()
	 */
	@Override
	public boolean supportsInternalParameterOptimization() {
		return this.getInvocationFormatParameterOptimization(false) != null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.SerializableWrapper#deserializeInternal
	 * (de.clusteval.framework.repository.IRepository)
	 */
	@Override
	protected IStandaloneProgramConfig deserializeInternal(
			IRepository repository) throws DeserializationException {
		try {
			return (IStandaloneProgramConfig) repository
					.getStaticObjectWithNameAndVersion(IProgramConfig.class,
							name + ":" + version);
		} catch (ObjectNotRegisteredException
				| ObjectVersionNotRegisteredException e) {
		}
		List<IDataSetFormat> compatibleDataSetFormats = new ArrayList<>();
		for (ISerializableWrapperRepositoryObject<IDataSetFormat> f : this.compatibleDataSetFormats)
			compatibleDataSetFormats.add(f.deserialize(repository));

		return new StandaloneProgramConfig(repository,
				System.currentTimeMillis(),
				new File(FileUtils.buildPath(
						repository.getBasePath(IProgramConfig.class),
						this.name + ".v" + version + ".config")),
				program.deserialize(repository),
				outputFormat.deserialize(repository), compatibleDataSetFormats,
				invocationFormat, invocationFormatWithoutGoldStandard,
				invocationFormatParameterOptimization,
				invocationFormatParameterOptimizationWithoutGoldStandard,
				params, optimizableParameters, expectsNormalizedDataSet,
				maxExecutionTimeMinutes, alias, envVars);
	}

	/**
	 * @return the alias
	 */
	public String getAlias() {
		return alias;
	}

	@Override
	public Map<String, String> getEnvVars() {
		return this.envVars;
	}
}
