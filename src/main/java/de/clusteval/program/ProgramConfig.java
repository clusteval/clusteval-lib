/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
package de.clusteval.program;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.framework.repository.DumpableRepositoryObject;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.Repository;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.run.ParameterOptimizationRun;

/**
 * A program configuration encapsulates a program together with options and
 * settings.
 * 
 * <p>
 * A program configuration corresponds to and is parsed from a file on the
 * filesystem in the corresponding folder of the repository (see
 * {@link Repository#programConfigBasePath} and {@link ProgramConfigFinder}).
 * 
 * <p>
 * There are several options, that can be specified in the program configuration
 * file (see {@link #parseFromFile(File)}).
 * 
 * 
 * @author Christian Wiwie
 * 
 */
public abstract class ProgramConfig extends DumpableRepositoryObject
		implements
			IProgramConfig {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1837790005392664163L;

	/**
	 * A helper method for cloning a list of program configurations.
	 * 
	 * @param programConfigs
	 *            The list of program configurations to clone.
	 * @return The list containing the cloned program configurations of the
	 *         input list.
	 */
	public static List<IProgramConfig> cloneProgramConfigurations(
			final List<IProgramConfig> programConfigs) {
		List<IProgramConfig> result = new ArrayList<IProgramConfig>();

		for (IProgramConfig programConfig : programConfigs) {
			result.add(programConfig.clone());
		}

		return result;
	}

	/**
	 * The program this configuration belongs to.
	 */
	protected transient IProgram program;

	/**
	 * This boolean indicates, whether the encapsulated program requires a
	 * normalized dataset, i.e. similarities between 0 and 1. This is then
	 * handled before the data is passed to the clustering method.
	 */
	protected boolean expectsNormalizedDataSet;

	protected int maxExecutionTimeMinutes;

	/**
	 * A list holding all optimizable parameter of the program. Optimizable
	 * parameters are those parameters, that can in principle be optimized in
	 * parameter optimization runs (see {@link ParameterOptimizationRun}).
	 */
	protected List<IProgramParameter<?>> optimizableParameters;

	/**
	 * Instantiates a new program config.
	 * 
	 * @param repository
	 *            The repository this program configuration should be registered
	 *            at.
	 * @param changeDate
	 *            The change date of this program configuration is used for
	 *            equality checks.
	 * @param absPath
	 *            The absolute path of this program configuration.
	 * @param program
	 *            The program this program configuration belongs to.
	 * @param optimizableParameters
	 *            The parameters of the program, that can be optimized.
	 * @param expectsNormalizedDataSet
	 *            Whether the encapsulated program requires normalized input.
	 * @param maxExecutionTimeMinutes
	 */
	public ProgramConfig(final IRepository repository, final long changeDate,
			final File absPath, final IProgram program,
			List<IProgramParameter<?>> optimizableParameters,
			final boolean expectsNormalizedDataSet,
			final int maxExecutionTimeMinutes) {
		super(repository, changeDate, absPath);

		this.program = program;

		this.optimizableParameters = optimizableParameters;
		this.expectsNormalizedDataSet = expectsNormalizedDataSet;
		this.maxExecutionTimeMinutes = maxExecutionTimeMinutes;
	}

	/**
	 * The copy constructor of program configurations.
	 * 
	 * @param programConfig
	 *            The program configuration to be cloned.
	 */
	public ProgramConfig(ProgramConfig programConfig) {
		super(programConfig);

		this.program = programConfig.program.clone();
		this.optimizableParameters = ProgramParameter
				.cloneParameterList(programConfig.optimizableParameters);

		this.expectsNormalizedDataSet = programConfig.expectsNormalizedDataSet;
		this.maxExecutionTimeMinutes = programConfig.maxExecutionTimeMinutes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramConfig#expectsNormalizedDataSet()
	 */
	@Override
	public boolean expectsNormalizedDataSet() {
		return this.expectsNormalizedDataSet;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramConfig#getMaxExecutionTimeMinutes()
	 */
	@Override
	public int getMaxExecutionTimeMinutes() {
		return this.maxExecutionTimeMinutes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramConfig#setMaxExecutionTimeMinutes(int)
	 */
	@Override
	public void setMaxExecutionTimeMinutes(final int maxExecutionTimeMinutes) {
		this.maxExecutionTimeMinutes = maxExecutionTimeMinutes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.IProgramConfig#supportsInternalParameterOptimization
	 * ()
	 */
	@Override
	public boolean supportsInternalParameterOptimization() {
		// TODO: r programs
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramConfig#getProgram()
	 */
	@Override
	public IProgram getProgram() {
		return program;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramConfig#getName()
	 */
	@Override
	public String getName() {
		return this.absPath.getName().replaceAll(
				"(" + getAbsPathVersionMatchString() + "?).config$", "");
	}

	/**
	 * @return the version
	 */
	@Override
	public ComparableVersion getVersion() {
		String name = absPath.getName().replaceAll(".config", "");
		int colonInd = name.lastIndexOf(".");
		return new ComparableVersion(name.substring(colonInd + 2));
	}

	@Override
	public ISerializableProgramConfig<? extends IProgramConfig> asSerializable()
			throws RepositoryObjectSerializationException {
		return (ISerializableProgramConfig<? extends IProgramConfig>) super.asSerializable();
	}

	@Override
	public abstract ISerializableProgramConfig<? extends IProgramConfig> asSerializableInternal()
			throws RepositoryObjectSerializationException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#isSerializable()
	 */
	@Override
	public boolean isSerializable() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#clone()
	 */
	@Override
	public abstract IProgramConfig clone();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramConfig#getOptimizableParams()
	 */
	@Override
	public List<IProgramParameter<?>> getOptimizableParameters() {
		return optimizableParameters;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.IProgramConfig#getParameterForName(java.lang.String)
	 */
	@Override
	public IProgramParameter<?> getParameterForName(final String name) {
		IProgramParameter<?> pa = null;
		for (int i = 0; i < this.getParameters().size(); i++) {
			pa = this.getParameters().get(i);
			if (pa.toString().equals(name) || pa.getName().equals(name)) {
				return pa;
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramConfig#getParamWithId(java.lang.String)
	 */
	@Override
	public IProgramParameter<?> getParamWithId(final String id)
			throws UnknownProgramParameterException {
		for (IProgramParameter<?> param : this.getParameters())
			if (param.getName().equals(id))
				return param;
		throw new UnknownProgramParameterException(
				"The program parameter with id \"" + id + "\" is unknown.");
	}
}
