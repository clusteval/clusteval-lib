/**
 * 
 */
package de.clusteval.program;

import java.util.Map;

/**
 * @author Christian Wiwie
 *
 */
public interface ISerializableStandaloneProgramConfig
		extends
			ISerializableProgramConfig<IStandaloneProgramConfig> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ISerializableProgramConfig#getProgram()
	 */
	ISerializableProgram getProgram();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.ISerializableProgramConfig#expectsNormalizedDataSet(
	 * )
	 */
	boolean expectsNormalizedDataSet();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ISerializableProgramConfig#
	 * getMaxExecutionTimeMinutes()
	 */
	int getMaxExecutionTimeMinutes();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ISerializableProgramConfig#
	 * supportsInternalParameterOptimization()
	 */
	boolean supportsInternalParameterOptimization();

	/**
	 * @return the envVars
	 */
	Map<String, String> getEnvVars();

}