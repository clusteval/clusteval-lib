/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.program;

import javax.script.ScriptException;

import de.clusteval.data.IDataConfig;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.utils.InternalAttributeException;

/**
 * A type of program parameter that only holds string values.
 * 
 * @author Christian Wiwie
 * 
 */
public class StringProgramParameter extends ProgramParameter<String> implements IStringProgramParameter {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1065085704349191982L;

	/**
	 * The constructor of string program parameters.
	 * 
	 * @param name
	 *            The name of the parameter.
	 * @param desc
	 *            The description of the parameter.
	 * @param options
	 *            The possible values of this parameter.
	 * @param def
	 *            The default value of the parameter.
	 */
	public StringProgramParameter(final String name, final String desc, String[] options, String def) {
		super(name, desc, options, def);
	}

	/**
	 * The copy constructor of string program parameters.
	 * 
	 * @param other
	 *            The object to clone.
	 */
	public StringProgramParameter(final StringProgramParameter other) {
		super(other);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.ProgramParameter#clone(program.ProgramParameter)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IStringProgramParameter#clone()
	 */
	@Override
	public StringProgramParameter clone() {
		return new StringProgramParameter(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.ProgramParameter#isMinValueSet()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IStringProgramParameter#isMinValueSet()
	 */
	@Override
	public boolean isMinValueSet() {
		return !this.minValue.equals("");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.ProgramParameter#isMaxValueSet()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IStringProgramParameter#isMaxValueSet()
	 */
	@Override
	public boolean isMaxValueSet() {
		return !this.maxValue.equals("");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.ProgramParameter#evaluateMinValue()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IStringProgramParameter#evaluateMinValue(de.
	 * clusteval.data.IDataConfig, de.clusteval.program.IProgramConfig)
	 */
	@Override
	public String evaluateMinValue(final IRepository repository, final IDataConfig dataConfig,
			final IProgramConfig programConfig) throws InternalAttributeException {

		/*
		 * Parse minValue
		 */
		String newMinValue = repository.evaluateInternalAttributes(minValue, dataConfig, programConfig);

		try {
			newMinValue = repository.evaluateJavaScript(newMinValue);
		} catch (ScriptException e) {
			throw new InternalAttributeException("The expression '" + minValue + "' for parameter attribute "
					+ programConfig + "/" + this.name + "/minValue is invalid");
		}

		return newMinValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.ProgramParameter#evaluateMaxValue()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IStringProgramParameter#evaluateMaxValue(de.
	 * clusteval.data.IDataConfig, de.clusteval.program.IProgramConfig)
	 */
	@Override
	public String evaluateMaxValue(final IRepository repository, final IDataConfig dataConfig,
			final IProgramConfig programConfig) throws InternalAttributeException {

		/*
		 * Parse maxValue
		 */
		String newMaxValue = repository.evaluateInternalAttributes(maxValue, dataConfig, programConfig);

		try {
			newMaxValue = repository.evaluateJavaScript(newMaxValue);
		} catch (ScriptException e) {
			throw new InternalAttributeException("The expression '" + maxValue + "' for parameter attribute "
					+ programConfig + "/" + this.name + "/maxValue is invalid");
		}

		return newMaxValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.ProgramParameter#evaluateDefaultValue()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.IStringProgramParameter#evaluateDefaultValue(de.
	 * clusteval.data.IDataConfig, de.clusteval.program.IProgramConfig)
	 */
	@Override
	public String evaluateDefaultValue(final IRepository repository, final IDataConfig dataConfig,
			final IProgramConfig programConfig) throws InternalAttributeException {

		/*
		 * Parse default
		 */
		String newDefaultValue = repository.evaluateInternalAttributes(def, dataConfig, programConfig);

		return newDefaultValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.ProgramParameter#evaluateOptions(de.clusteval.data
	 * .DataConfig, de.clusteval.program.ProgramConfig)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.IStringProgramParameter#evaluateOptions(de.clusteval
	 * .data.IDataConfig, de.clusteval.program.IProgramConfig)
	 */
	@Override
	public String[] evaluateOptions(final IRepository repository, IDataConfig dataConfig, IProgramConfig programConfig)
			throws InternalAttributeException {
		/*
		 * Parse options
		 */
		String[] newOptions = new String[this.options.length];
		for (int i = 0; i < this.options.length; i++) {
			newOptions[i] = repository.evaluateInternalAttributes(options[i], dataConfig, programConfig);
			try {
				newOptions[i] = repository.evaluateJavaScript(newOptions[i]);
			} catch (ScriptException e) {
				throw new InternalAttributeException("The expression '" + newOptions[i] + "' for parameter attribute "
						+ programConfig + "/" + this.name + "/options is invalid");
			}
		}

		return newOptions;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ProgramParameter#isOptionsSet()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IStringProgramParameter#isOptionsSet()
	 */
	@Override
	public boolean isOptionsSet() {
		return !(this.options.length == 0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.IProgramParameter#withDifferentMin(java.lang.String)
	 */
	@Override
	public IProgramParameter<String> withDifferentMin(String minValue) {
		return this.clone();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.IProgramParameter#withDifferentMax(java.lang.String)
	 */
	@Override
	public IProgramParameter<String> withDifferentMax(String maxValue) {
		return this.clone();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.IProgramParameter#withDifferentOptions(java.lang.
	 * String[])
	 */
	@Override
	public IProgramParameter<String> withDifferentOptions(String[] options) {
		return new StringProgramParameter(this.name, this.description, options, def);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.IProgramParameter#withDifferentDefault(java.lang.
	 * String)
	 */
	@Override
	public IProgramParameter<String> withDifferentDefault(String def) {
		return new StringProgramParameter(this.name, this.description, options.clone(), def);
	}
}
