/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
package de.clusteval.program;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.SubnodeConfiguration;

import de.clusteval.data.IDataConfig;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.utils.InternalAttributeException;

/**
 * An object of this class corresponds to a parameter of a program.
 * 
 * <p>
 * Therefore a program parameter has a reference to a program configuration in
 * which it was defined.
 * 
 * <p>
 * A program parameter has a certain {@link #name} unique for the program
 * configuration, a {@link #description}, a minimal value {@link #minValue}, a
 * maximal value {@link #maxValue} and a default value {@link #def}.
 * 
 * @author Christian Wiwie
 * @param <T>
 * 
 */
public abstract class ProgramParameter<T> implements IProgramParameter<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -406866086227886253L;

	/**
	 * A parameter for the number of desired clusters which is used by many
	 * clustering methods.
	 */
	public static final IntegerProgramParameter K_NUMBER_OF_CLUSTERS_PARAMETER = new IntegerProgramParameter("k",
			"number of clusters", "2", "$(numberOfElements)", "2");

	/**
	 * Placeholder variables that can be used as parameter values which will be
	 * replaced on runtime during execution of a run.
	 * 
	 * @author Christian Wiwie
	 *
	 */
	public static enum PLACEHOLDER_VARIABLE {
		/**
		 * The number of elements or objects contained in the data set that is
		 * being clustered.
		 */
		NUMBER_ELEMENTS_DATASET("$(numberOfElements)"),
		/**
		 * The minimal similarity of pairs of objects of the data set that is
		 * being clustered.
		 */
		MINIMAL_SIMILARITY_IN_DATASET("$(minSimilarity)"),
		/**
		 * The maximal similarity of pairs of objects of the data set that is
		 * being clustered.
		 */
		MAXIMAL_SIMILARITY_IN_DATASET("$(maxSimilarity)");

		private final String text;

		private PLACEHOLDER_VARIABLE(final String text) {
			this.text = text;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Enum#toString()
		 */
		@Override
		public String toString() {
			return text;
		}
	}

	/**
	 * A helper method for cloning a list of parameters.
	 * 
	 * @param programParameters
	 *            The optimization parameters to clone.
	 * @return The list of cloned optimization parameters.
	 */
	public static Map<IProgramConfig, List<IProgramParameter<?>>> cloneParameterListList(
			Map<IProgramConfig, List<IProgramParameter<?>>> programParameters) {
		Map<IProgramConfig, List<IProgramParameter<?>>> result = new HashMap<IProgramConfig, List<IProgramParameter<?>>>();

		for (IProgramConfig pc: programParameters.keySet()) {
			List<IProgramParameter<?>> list = programParameters.get(pc);
			List<IProgramParameter<?>> copyList = new ArrayList<IProgramParameter<?>>();

			for (IProgramParameter<?> param : list)
				copyList.add(param.clone());

			result.put(pc, copyList);
		}

		return result;
	}

	/**
	 * A helper method for cloning a list of parameters.
	 * 
	 * @param programParameters
	 *            The optimization parameters to clone.
	 * @return The list of cloned optimization parameters.
	 */
	public static List<IProgramParameter<?>> cloneParameterList(List<IProgramParameter<?>> programParameters) {
		List<IProgramParameter<?>> result = new ArrayList<IProgramParameter<?>>();

		for (IProgramParameter<?> param : programParameters)
			result.add(param.clone());

		return result;
	}

	/**
	 * The name of this parameter has to be unique for the program configuration
	 * and program.
	 */
	protected String name;

	/**
	 * A program parameter can have a description.
	 */
	protected String description;

	/**
	 * The minimal value this parameter can be set to. The attribute holds a
	 * string, because the value of this variable can hold a placeholder which
	 * is replaced dynamically by the framework, e.g $(meanSimilarity).
	 */
	protected final String minValue;

	/**
	 * The maximal value this parameter can be set to. The attribute holds a
	 * string, because the value of this variable can hold a placeholder which
	 * is replaced dynamically by the framework, e.g $(meanSimilarity).
	 */
	protected final String maxValue;

	/**
	 * The possible values this parameter can be set to. The attribute holds a
	 * string[], because the values of this variable can hold placeholders which
	 * are replaced dynamically by the framework, e.g $(meanSimilarity).
	 */
	protected final String[] options;

	/**
	 * The default value of this parameter. The attribute holds a string,
	 * because the value of this variable can hold a placeholder which is
	 * replaced dynamically by the framework, e.g $(meanSimilarity).
	 */
	protected final String def;

	/**
	 * Instantiates a new program parameter.
	 * 
	 * <p>
	 * The absolute path of a program parameter is defined as the absolute path
	 * of its program configuration concatenated with its name, separated by a
	 * colon.
	 * 
	 * @param name
	 *            The name of this parameter has to be unique for the program
	 *            configuration and program.
	 * @param desc
	 *            The name of this parameter has to be unique for the program
	 *            configuration and program.
	 * @param minValue
	 *            The minimal value this parameter can be set to (see
	 *            {@link #minValue}).
	 * @param maxValue
	 *            The maximal value this parameter can be set to (see
	 *            {@link #maxValue}).
	 * @param def
	 *            The default value of this parameter (see {@link #def}).
	 */
	public ProgramParameter(final String name, final String desc, final String minValue, final String maxValue,
			final String def) {
		super();

		this.name = name;
		this.description = desc;
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.options = new String[0];
		this.def = def;
	}

	/**
	 * Instantiates a new program parameter.
	 * 
	 * <p>
	 * The absolute path of a program parameter is defined as the absolute path
	 * of its program configuration concatenated with its name, separated by a
	 * colon.
	 * 
	 * @param name
	 *            The name of this parameter has to be unique for the program
	 *            configuration and program.
	 * @param desc
	 *            The name of this parameter has to be unique for the program
	 *            configuration and program.
	 * @param options
	 *            The possible values of this parameter.
	 * @param def
	 *            The default value of this parameter (see {@link #def}).
	 */
	public ProgramParameter(final String name, final String desc, final String[] options, final String def) {
		super();

		this.name = name;
		this.description = desc;
		this.options = options;
		this.minValue = "";
		this.maxValue = "";
		this.def = def;
	}

	/**
	 * The copy constructor of program parameters.
	 * 
	 * @param other
	 *            The object to clone.
	 */
	public ProgramParameter(final ProgramParameter<T> other) {
		super();

		this.name = other.name;
		this.description = other.description;
		this.minValue = other.minValue;
		this.maxValue = other.maxValue;
		this.options = other.options;
		this.def = other.def;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramParameter#clone()
	 */
	@Override
	public abstract ProgramParameter<T> clone();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramParameter#getMinValue()
	 */
	@Override
	public String getMinValue() {
		return this.minValue;
	}

	@Override
	public abstract T evaluateMinValue(final IRepository repository, final IDataConfig dataConfig,
			final IProgramConfig programConfig) throws InternalAttributeException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramParameter#isMinValueSet()
	 */
	@Override
	public abstract boolean isMinValueSet();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramParameter#getMaxValue()
	 */
	@Override
	public String getMaxValue() {
		return this.maxValue;
	}

	@Override
	public abstract T evaluateMaxValue(final IRepository repository, final IDataConfig dataConfig,
			final IProgramConfig programConfig) throws InternalAttributeException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramParameter#isMaxValueSet()
	 */
	@Override
	public abstract boolean isMaxValueSet();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramParameter#getOptions()
	 */
	@Override
	public String[] getOptions() {
		return this.options;
	}

	@Override
	public abstract T[] evaluateOptions(final IRepository repository, final IDataConfig dataConfig,
			final IProgramConfig programConfig) throws InternalAttributeException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramParameter#isOptionsSet()
	 */
	@Override
	public abstract boolean isOptionsSet();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramParameter#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	// changed 12.07.2012
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramParameter#toString()
	 */
	@Override
	public String toString() {
		return this.name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramParameter#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o) {
		if (!(this.getClass().equals(o.getClass())))
			return false;
		ProgramParameter<?> other = (ProgramParameter<?>) o;
		return this.name.equals(other.name)
				&& ((this.getDefault() == null && other.getDefault() == null) || (this.getDefault() != null
						&& other.getDefault() != null && this.getDefault().equals(other.getDefault())))
				&& ((this.getMinValue() == null && other.getMinValue() == null) || (this.getMinValue() != null
						&& other.getMinValue() != null && this.getMinValue().equals(other.getMinValue())))
				&& ((this.getMaxValue() == null && other.getMaxValue() == null) || (this.getMaxValue() != null
						&& other.getMaxValue() != null && this.getMaxValue().equals(other.getMaxValue())));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramParameter#getDefault()
	 */
	@Override
	public String getDefault() {
		return this.def;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramParameter#getDescription()
	 */
	@Override
	public String getDescription() {
		return this.description;
	}

	@Override
	public abstract T evaluateDefaultValue(final IRepository repository, final IDataConfig dataConfig,
			final IProgramConfig programConfig) throws InternalAttributeException;

	/**
	 * Parses a program parameter from a section of a configuration file.
	 * 
	 * <p>
	 * This method only delegates depending on the type of the program parameter
	 * to the methods
	 * {@link DoubleProgramParameter#parseFromStrings(ProgramConfig, String, String, String, String, String)},
	 * {@link IntegerProgramParameter#parseFromStrings(ProgramConfig, String, String, String, String, String)}
	 * and
	 * {@link StringProgramParameter#parseFromStrings(ProgramConfig, String, String, String, String, String)}.
	 * 
	 * @param programConfig
	 *            The program configuration in which the program parameter has
	 *            been defined.
	 * @param name
	 *            The name of the program parameter.
	 * @param config
	 *            The section of the configuration, which contains the
	 *            information about this parameter.
	 * @return The parsed program parameter.
	 * @throws RegisterException
	 * @throws UnknownParameterType
	 */
	public static IProgramParameter<?> parseFromConfiguration(final ProgramConfig programConfig, final String name,
			final SubnodeConfiguration config) throws RegisterException, UnknownParameterType {
		Map<String, String> paramValues = new HashMap<String, String>();
		paramValues.put("name", name);

		Iterator<String> itSubParams = config.getKeys();
		while (itSubParams.hasNext()) {
			final String subParam = itSubParams.next();
			final String value = config.getString(subParam);

			paramValues.put(subParam, value);
		}
		ParameterType type = ProgramParameter.parseTypeFromString(paramValues.get("type"));

		String na = paramValues.get("name");
		String description = paramValues.get("desc");
		String def = paramValues.get("def");

		// 23.05.2014: added support for options for float and integer
		// parameters. if options are given, we set minValue and maxValue to the
		// empty string.
		IProgramParameter<?> param = null;
		String[] options = config.getStringArray("options");
		String minValue = paramValues.get("minValue");
		String maxValue = paramValues.get("maxValue");
		if (config.containsKey("options")) {
			if (type.equals(ParameterType.FLOAT)) {
				param = new DoubleProgramParameter(na, description, options, def);
			} else if (type.equals(ParameterType.INTEGER)) {
				param = new IntegerProgramParameter(na, description, options, def);
			} else if (type.equals(ParameterType.STRING)) {
				param = new StringProgramParameter(na, description, options, def);
			}
		} else {
			if (type.equals(ParameterType.FLOAT)) {
				param = new DoubleProgramParameter(na, description, minValue, maxValue, def);
			} else if (type.equals(ParameterType.INTEGER)) {
				param = new IntegerProgramParameter(na, description, minValue, maxValue, def);
			} else if (type.equals(ParameterType.STRING)) {
				// TODO: is not valid, are we handling this somewhere?
				param = new StringProgramParameter(na, description, options, def);
			}
		}
		return param;
	}

	/**
	 * Parses a parameter type from a string. There are different strings
	 * representing the types of parameters:
	 * 
	 * <p>
	 * <ul>
	 * <li><b>0</b>: A String parameter holding only string values.</li>
	 * <li><b>1</b>: An integer parameter holding integer values.</li>
	 * <li><b>2</b>: A double parameter holding double values.</li>
	 * </ul>
	 * 
	 * <p>
	 * A helper method for
	 * {@link #parseFromConfiguration(ProgramConfig, String, SubnodeConfiguration)}.
	 * 
	 * @param value
	 *            A string indicating a number corresponding to a parameter
	 *            type.
	 * @return the parameter type
	 * @throws UnknownParameterType
	 */
	private static ParameterType parseTypeFromString(String value) throws UnknownParameterType {

		// String
		if (value.equals("0")) {
			return ParameterType.STRING;
		}
		// Integer
		else if (value.equals("1")) {
			return ParameterType.INTEGER;
		}
		// Float
		else if (value.equals("2")) {
			return ParameterType.FLOAT;
		}

		throw new UnknownParameterType("The parameter type " + value + " is unknown");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramParameter#hashCode()
	 */
	@Override
	public int hashCode() {
		return this.name.hashCode();
	}

	/**
	 * An enumeration for parameter types. Program parameters can be of
	 * different type and this enumeration helps distinguishing them.
	 * 
	 * @author Christian Wiwie
	 * 
	 */
	public enum ParameterType {
		/**
		 * A string parameter holding string values.
		 */
		STRING,
		/**
		 * An integer parameter holding integer values.
		 */
		INTEGER,
		/**
		 * A float parameter holding float values.
		 */
		FLOAT
	}
}
