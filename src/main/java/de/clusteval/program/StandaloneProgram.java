/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.program;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.context.IContext;
import de.clusteval.data.IDataConfig;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;

/**
 * A type of program that corresponds to executables on the filesystem.
 * 
 * @author Christian Wiwie
 * 
 */
public class StandaloneProgram extends Program implements IStandaloneProgram {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2840623109511899442L;

	protected transient IContext context;

	protected String alias;

	/**
	 * @param repository
	 *            the repository this program should be registered at.
	 * @param context
	 *            The context of this program
	 * @param changeDate
	 *            The change date of this program is used for equality checks.
	 * @param absPath
	 *            The absolute path of this program.
	 * @param alias
	 */
	public StandaloneProgram(IRepository repository, final IContext context,
			long changeDate, File absPath, String alias) {
		super(repository, changeDate, absPath);
		this.context = context;
		this.alias = alias;
	}

	/**
	 * The copy constructor of standalone programs.
	 * 
	 * @param program
	 *            The standalone program to clone.
	 */
	public StandaloneProgram(final StandaloneProgram program) {
		super(program);
		this.context = program.context;
		this.alias = program.alias;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.Program#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IStandaloneProgram#clone()
	 */
	@Override
	public StandaloneProgram clone() {
		return new StandaloneProgram(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.Program#exec(program.ProgramConfig, java.lang.String,
	 * java.util.Map)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IStandaloneProgram#exec(de.clusteval.data.
	 * IDataConfig, de.clusteval.program.IProgramConfig, java.lang.String[],
	 * java.util.Map, java.util.Map)
	 */
	@SuppressWarnings("unused")
	@Override
	public Process exec(final IDataConfig dataConfig,
			final IProgramConfig programConfig, final String[] invocationLine,
			Map<String, String> effectiveParams,
			Map<String, String> internalParams) throws IOException {
		String[] envVarsArray = new String[((IStandaloneProgramConfig) programConfig)
				.getEnvVars().size() + 1];
		// TODO, check whether this works everywhere
		envVarsArray[0] = "TERM=xterm";
		int i = 1;
		for (Map.Entry<String, String> e : ((IStandaloneProgramConfig) programConfig)
				.getEnvVars().entrySet()) {
			envVarsArray[i] = String.format("%s=%s", e.getKey(), e.getValue());
			i++;
		}

		return Runtime.getRuntime().exec(invocationLine, envVarsArray,
				new File(programConfig.getProgram().getAbsolutePath())
						.getParentFile());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IStandaloneProgram#getContext()
	 */
	@Override
	public IContext getContext() {
		return this.context;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#getName()
	 */
	@Override
	public String getName() {
		return this.getFullName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IStandaloneProgram#getVersion()
	 */
	@Override
	public ComparableVersion getVersion() {
		String folderName = absPath.getParentFile().getName();
		int colonInd = folderName.lastIndexOf(".");
		if (colonInd < 0)
			return null;
		return new ComparableVersion(folderName.substring(colonInd + 2));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.Program#asSerializable()
	 */
	@Override
	public SerializableStandaloneProgram asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableStandaloneProgram) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#asSerializable()
	 */
	@Override
	public SerializableStandaloneProgram asSerializableInternal()
			throws RepositoryObjectSerializationException {
		return new SerializableStandaloneProgram(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#isSerializable()
	 */
	@Override
	public boolean isSerializable() {
		return true;
	}

	/**
	 * @return the alias
	 */
	@Override
	public String getAlias() {
		return alias;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgram#hasAlias()
	 */
	@Override
	public boolean hasAlias() {
		return this.alias != null && !this.alias.isEmpty();
	}
}
