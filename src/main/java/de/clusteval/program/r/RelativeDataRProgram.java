/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.program.r;

import java.util.Map;

import org.rosuda.REngine.REngineException;

import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.RelativeDataSet;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.program.IProgramConfig;
import de.clusteval.run.runresult.format.UnknownRunResultFormatException;
import de.clusteval.utils.DynamicComponentInitializationException;
import dk.sdu.imada.compbio.utils.SimilarityMatrix;

/**
 * @author Christian Wiwie
 * 
 */
public abstract class RelativeDataRProgram extends RProgram {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4546957143688244437L;

	/**
	 * @param repository
	 *            the repository this program should be registered at.
	 * @throws DynamicComponentInitializationException
	 * @throws UnknownRunResultFormatException
	 * @throws UnknownDataSetFormatException
	 */
	public RelativeDataRProgram(IRepository repository)
			throws UnknownDataSetFormatException,
			UnknownRunResultFormatException,
			DynamicComponentInitializationException {
		super(repository);
	}

	/**
	 * The copy constructor for rprograms.
	 * 
	 * @param rProgram
	 *            The object to clone.
	 */
	public RelativeDataRProgram(final RelativeDataRProgram rProgram) {
		super(rProgram);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.r.RProgram#initData(de.clusteval.data.IDataConfig,
	 * de.clusteval.program.IProgramConfig, java.lang.String[], java.util.Map,
	 * java.util.Map)
	 */
	@Override
	public void initData(IDataConfig dataConfig, IProgramConfig programConfig,
			String[] invocationLine, Map<String, String> effectiveParams,
			Map<String, String> internalParams)
			throws REngineException, InterruptedException {
		super.initData(dataConfig, programConfig, invocationLine,
				effectiveParams, internalParams);

		rEngine.assign("sim", x);
		rEngine.voidEval("sim <- max(sim)-sim");
		rEngine.voidEval("rownames(sim) <- as.character(1:nrow(sim))");
		rEngine.voidEval("colnames(sim) <- as.character(1:nrow(sim))");
		rEngine.voidEval("sim <- as.dist(sim)");
		rEngine.voidEval("lockBinding('sim', .GlobalEnv)");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.r.RProgram#extractDataSetContent(de.clusteval.data
	 * .DataConfig)
	 */
	@Override
	protected SimilarityMatrix extractDataSetContent(IDataConfig dataConfig) {
		RelativeDataSet dataSet = (RelativeDataSet) (dataConfig
				.getDatasetConfig().getDataSet().getInStandardFormat());
		SimilarityMatrix simMatrix = dataSet.getDataSetContent();
		this.ids = dataSet.getIds().toArray(new String[0]);
		this.x = simMatrix.toArray();
		return simMatrix;
	}
}
