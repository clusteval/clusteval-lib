/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.program.r;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.maven.artifact.versioning.ComparableVersion;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RserveException;

import de.clusteval.cluster.FileBackedClustering;
import de.clusteval.cluster.IClustering;
import de.clusteval.data.DataConfig;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.format.DataSetFormat;
import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.framework.RLibraryNotLoadedException;
import de.clusteval.framework.RLibraryRequirement;
import de.clusteval.framework.repository.IMyRengine;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.Repository;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.program.IStandaloneProgram;
import de.clusteval.program.Program;
import de.clusteval.program.ProgramConfig;
import de.clusteval.run.runresult.format.IRunResultFormat;
import de.clusteval.run.runresult.format.UnknownRunResultFormatException;
import de.clusteval.utils.ClustEvalAlias;
import de.clusteval.utils.ClassVersion;
import de.clusteval.utils.ClassVersionRequirement;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.DynamicComponentVersion;
import de.clusteval.utils.RNotAvailableException;
import dk.sdu.imada.compbio.file.FileUtils;
import dk.sdu.imada.compbio.utils.StringExt;

// @formatter:off
/**
 * A type of progam that encapsulates a program embedded in R.
 * 
 * ## Writing R Programs
 * 
 * R programs can be added to ClustEval by
 * 
 * 1. extending the {@link RProgram} class with your own class `MyRProgram`.
 *     - Your class has to be decorated with the following annotations:
 *         - {@link DynamicComponentVersion}: Any component you add to ClustEval needs to have a version. This is needed to ensure, that results are fully reproducible by running runs using exactly the same versions as previously
 *         - {@link ClassVersionRequirement}: Any component you add to ClustEval needs to specify, which dependencies it has on the ClustEval interfaces. This information is used to ensure compatibility between the version of your component and the used ClustEval version. Whenever core interfaces in the ClustEval API change, their interface version is incremented. By using this annotation it can detect, which components are still compatible.
 *         - {@link ClustEvalAlias}: You need to provide a readable alias for your component. This alias is used when results and components should be presented in a accessible way.
 *         - [Optional] {@link RLibraryRequirement}: If your component depends on R libraries being installed and loaded when your program is being executed, it needs to be decorated with this annotation.
 *     - You have to provide your own implementations for the following methods, otherwise the framework will not be able to load your class.
 *         - {@link #RProgram(IRepository)}: The constructor of your class taking a repository parameter. This constructor has to be implemented and public, otherwise the framework will not be able to load your class.
 *         - {@link #RProgram(RProgram)}: The copy constructor of your class taking another instance of your class. This constructor has to be implemented and public.
 *         - {@link #getContext()}: This function serves no purpose as of yet, and can be implemented as follows:
 *         ```java
 *         .@Override
 *         public IContext getContext() throws UnknownContextException, DynamicComponentInitializationException {
 *            return Context.parseFromString(repository, "ClusteringContext");
 *         }
 *         ```
 *         - {@link #getFuzzyCoeffMatrixFromExecResult()}:  After your program has been executed, this function should return an array of fuzzy coefficients for each object and cluster.
 *         - {@link #getInvocationFormat()}: This is the invocation of the R method including potential parameters, that have to be defined in the program configuration.
 *         - {@link #initCompatibleDataSetFormats()}: This function should return this data set formats that this program can be applied to.
 *         - {@link #initRunResultFormat()}: This function should return the format, that this program stores resulting clusterings in.
 * 2. Creating a jar file named `MyRProgram.jar` containing the `MyRProgram.class` compiled on your machine in the correct folder structure corresponding to the packages: `de/clusteval/program/r/MyRProgram.class`
 * 3. Putting the MyRProgram.jar into the programs folder of the repository: `<REPOSITORY ROOT>/programs`
 * 
 * The backend server will recognize and try to load the new program automatically the next time, the `RProgramFinderThread` checks the filesystem.
 * 
 * 
 * 
 * @author Christian Wiwie
 * 
 */
//@formatter:on
@ClassVersion(version = "5")
public abstract class RProgram extends Program implements IRProgram {

	/**
	 * 
	 */
	private static final long serialVersionUID = 887071668620904743L;

	/**
	 * Attribute used to store an rengine instance during execution of this
	 * program.
	 */
	protected transient IMyRengine rEngine;

	/**
	 * Attribute used to store the dataset content during execution of this
	 * program.
	 */
	protected transient Object dataSetContent;

	/**
	 * Attribute to store the data object ids during execution of this program.
	 */
	protected transient String[] ids;

	/**
	 * Attribute to store the pairwise similarites or absolute coordinates of
	 * data objects during execution of this program.
	 */
	protected transient double[][] x;

	protected final List<IProgramParameter<?>> parameters;

	protected Set<IDataSetFormat> compatibleDataSetFormats;

	protected IRunResultFormat runResultFormat;

	/**
	 * This method parses the major name given as a string, looks up the
	 * corresponding RProgram in the repository and returns a new instance.
	 * 
	 * @param repository
	 *            The repository to lookup the RProgram.
	 * @param rProgram
	 *            The major name (see {@link #getMajorName()}) of the RProgram
	 *            to return.
	 * @param tryMaven
	 *            If true and a program with the specified name is not available
	 *            in the repository, maven resolution will be attempted if
	 *            available.
	 * @return An instance of an RProgram corresponding to the passed string.
	 * @throws UnknownRProgramException
	 * @throws DynamicComponentInitializationException
	 * @throws UnknownRunResultFormatException
	 * @throws UnknownDataSetFormatException
	 */
	public static IRProgram parseFromString(final IRepository repository,
			String rProgram, boolean tryMaven) throws UnknownRProgramException,
			DynamicComponentInitializationException,
			UnknownDataSetFormatException, UnknownRunResultFormatException {

		Class<? extends IRProgram> c = repository.resolveAndGetRegisteredClass(
				IRProgram.class, rProgram, tryMaven);
		if (c == null)
			throw new UnknownRProgramException(rProgram, "Unknown R program");
		return getInstance(repository, rProgram, c);
	}

	protected static IRProgram getInstance(final IRepository repository,
			String rProgram, Class<? extends IRProgram> c)
			throws DynamicComponentInitializationException,
			UnknownDataSetFormatException, UnknownRunResultFormatException {
		try {
			Constructor<? extends IRProgram> constr = c
					.getConstructor(IRepository.class);
			IRProgram program = constr.newInstance(repository);
			return program;
		} catch (NoSuchMethodException e) {
			throw new DynamicComponentInitializationException(
					IStandaloneProgram.class, rProgram,
					"The class is missing the correct constructor");
		} catch (InvocationTargetException e) {
			if (e.getCause() instanceof UnknownDataSetFormatException)
				throw (UnknownDataSetFormatException) e.getCause();
			else if (e
					.getCause() instanceof DynamicComponentInitializationException)
				throw (DynamicComponentInitializationException) e.getCause();
			else if (e.getCause() instanceof UnknownRunResultFormatException)
				throw (UnknownRunResultFormatException) e.getCause();
			return null;
		} catch (Exception e) {
			// this is likely because we passed faked parameters
			return null;
		}
	}

	/**
	 * @param repository
	 *            the repository this program should be registered at.
	 * @throws DynamicComponentInitializationException
	 * @throws UnknownDataSetFormatException
	 * @throws UnknownRunResultFormatException
	 */
	public RProgram(final IRepository repository)
			throws UnknownDataSetFormatException,
			DynamicComponentInitializationException,
			UnknownRunResultFormatException {
		super(repository, 0, null);
		File file = new File(FileUtils.buildPath(
				repository.getBasePath(IStandaloneProgram.class),
				this.getClass().getSimpleName() + "-" + this.getVersion()
						+ ".jar"));
		this.absPath = file;
		this.changeDate = file.lastModified();
		this.parameters = createParameters();
		this.compatibleDataSetFormats = initCompatibleDataSetFormats();
		this.runResultFormat = initRunResultFormat();
	}

	/**
	 * The copy constructor for rprograms.
	 * 
	 * @param rProgram
	 *            The object to clone.
	 */
	public RProgram(final RProgram rProgram) {
		super(rProgram);
		this.parameters = createParameters();
		this.rEngine = rProgram.rEngine;
		this.compatibleDataSetFormats = new HashSet<>(
				DataSetFormat.cloneDataSetFormats(
						new ArrayList<>(rProgram.compatibleDataSetFormats)));
		this.runResultFormat = rProgram.runResultFormat.clone();

		this.ids = rProgram.ids;
		this.x = rProgram.x;
		this.dataSetContent = rProgram.dataSetContent;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.Program#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.r.IRProgram#clone()
	 */
	@Override
	public final IRProgram clone() {
		try {
			return this.getClass().getConstructor(this.getClass())
					.newInstance(this);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		this.log.warn("Cloning instance of class "
				+ this.getClass().getSimpleName() + " failed");
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.r.IRProgram#getMajorName()
	 */
	@Override
	public String getMajorName() {
		return "";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.Program#getMinorName()
	 */
	@Override
	public String getMinorName() {
		return this.getClass().getSimpleName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.Program#getFullName()
	 */
	@Override
	public String getFullName() {
		return getMinorName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.r.IRProgram#getInvocationFormat()
	 */
	@Override
	public abstract String getInvocationFormat();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.r.IRProgram#getCompatibleDataSetFormats()
	 */
	@Override
	public Set<IDataSetFormat> getCompatibleDataSetFormats() {
		return this.compatibleDataSetFormats;
	}

	protected abstract Set<IDataSetFormat> initCompatibleDataSetFormats()
			throws UnknownDataSetFormatException,
			DynamicComponentInitializationException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.r.IRProgram#getRunResultFormat()
	 */
	@Override
	public IRunResultFormat getRunResultFormat() {
		return this.runResultFormat;
	}

	protected String getThreadSafeResultVariableName() {
		// TODO: this will only work for thread pool names as they are now, i.e.
		// pool-X-thread-Y
		return String.format("result_%s",
				Thread.currentThread().getName().replaceAll("-", "_"));
	}

	protected abstract IRunResultFormat initRunResultFormat()
			throws UnknownRunResultFormatException,
			DynamicComponentInitializationException;

	/**
	 * @return the parameters
	 */
	@Override
	public List<IProgramParameter<?>> getParameters() {
		return parameters;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.Program#exec(de.clusteval.data.DataConfig,
	 * de.clusteval.program.ProgramConfig, java.lang.String[], java.util.Map,
	 * java.util.Map)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.r.IRProgram#exec(de.clusteval.data.IDataConfig,
	 * de.clusteval.program.IProgramConfig, java.lang.String[], java.util.Map,
	 * java.util.Map)
	 */
	@Override
	public final Process exec(final IDataConfig dataConfig,
			final IProgramConfig programConfig, final String[] invocationLine,
			final Map<String, String> effectiveParams,
			final Map<String, String> internalParams) throws REngineException {
		try {
			// 06.07.2014: execute r command in a thread.
			// then this thread can check for interrupt signal and forward it to
			// the
			// rengine.

			RProgramThread t = new RProgramThread(Thread.currentThread(), this,
					dataConfig, programConfig, invocationLine, effectiveParams,
					internalParams);
			t.start();
			return new RProcess(t);
		} finally {
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.r.IRProgram#getRengine()
	 */
	@Override
	public IMyRengine getRengine() {
		return this.rEngine;
	}

	/**
	 * @param rEngine
	 *            the rEngine to set
	 */
	@Override
	public void setRengine(IMyRengine rEngine) {
		this.rEngine = rEngine;
	}

	@Override
	public void initData(IDataConfig dataConfig, IProgramConfig programConfig,
			String[] invocationLine, Map<String, String> effectiveParams,
			Map<String, String> internalParams)
			throws REngineException, InterruptedException {
		// this will init the ids attribute
		this.dataSetContent = extractDataSetContent(dataConfig);

		rEngine.assign("ids", ids);
		rEngine.voidEval("lockBinding('ids', .GlobalEnv)");
	}

	@Override
	@SuppressWarnings("unused")
	public void beforeExec(IDataConfig dataConfig, IProgramConfig programConfig,
			String[] invocationLine, Map<String, String> effectiveParams,
			Map<String, String> internalParams)
			throws REngineException, RLibraryNotLoadedException,
			RNotAvailableException, InterruptedException {
		// load the required R libraries
		String[] requiredLibraries;
		if (this.getClass().isAnnotationPresent(RLibraryRequirement.class))
			requiredLibraries = this.getClass()
					.getAnnotation(RLibraryRequirement.class)
					.requiredRLibraries();
		else
			requiredLibraries = new String[0];
		for (String library : requiredLibraries)
			rEngine.loadLibrary(library, this.getClass().getSimpleName());

		if (Thread.currentThread().isInterrupted())
			throw new InterruptedException();
	}

	/**
	 * This method is required to initialize the attributes
	 * {@link #dataSetContent}, {@link #ids} and all other attributes of the
	 * data, which are needed in
	 * {@link #doExec(DataConfig, ProgramConfig, String[], Map, Map)}.
	 * 
	 * @param dataConfig
	 * @return
	 */
	protected abstract Object extractDataSetContent(IDataConfig dataConfig);

	@Override
	@SuppressWarnings("unused")
	public void doExec(IDataConfig dataConfig, IProgramConfig programConfig,
			final String[] invocationLine, Map<String, String> effectiveParams,
			Map<String, String> internalParams)
			throws RserveException, InterruptedException {
		rEngine.voidEval(
				String.format("%s <- " + StringExt.paste(" ", invocationLine),
						getThreadSafeResultVariableName()));

		// try {
		// t.join();
		// // rethrow exception from the r process, if any
		// if (t.getException() != null)
		// throw t.getException();
		// } catch (InterruptedException e) {
		// // forward the interruption to the r process
		// rEngine.interrupt();
		// repository.clearRengineForCurrentThread();
		// throw e;
		// }
	}

	@Override
	public void afterExec(IDataConfig dataConfig, IProgramConfig programConfig,
			String[] invocationLine, Map<String, String> effectiveParams,
			Map<String, String> internalParams) throws REXPMismatchException,
			REngineException, IOException, InterruptedException {
		try {
			File output = new File(internalParams.get("o"));
			Writer w = new FileWriter(output);
			writeResultToWriter(dataConfig, programConfig, invocationLine,
					effectiveParams, internalParams, w);
			w.close();
		} catch (StringIndexOutOfBoundsException e) {
			REngineException e2 = new REngineException(null,
					"The R program returned an empty clustering");
			e2.initCause(e);
			throw e2;
		} finally {
			rEngine.clear();
			this.x = null;
			this.dataSetContent = null;
			this.ids = null;
		}
	}

	@SuppressWarnings("unused")
	protected void writeResultToWriter(IDataConfig dataConfig,
			IProgramConfig programConfig, String[] invocationLine,
			Map<String, String> effectiveParams,
			Map<String, String> internalParams, Writer sb)
			throws RserveException, REXPMismatchException, InterruptedException,
			IOException {
		float[][] fuzzyMatrix = getFuzzyCoeffMatrixFromExecResult();
		IClustering resultClustering = FileBackedClustering.parseFromFuzzyCoeffMatrix(
				dataConfig.getRepository(), new File(internalParams.get("o")),
				ids, fuzzyMatrix);

		// TODO: changed 31.01.2014
		List<IProgramParameter<?>> params = this.getParameters();
		for (int i = 0; i < params.size(); i++) {
			IProgramParameter<?> p = params.get(i);
			sb.append(p.getName());
			if (i < params.size() - 1)
				sb.append(",");
		}
		sb.append("\tClustering\n");

		for (int i = 0; i < params.size(); i++) {
			IProgramParameter<?> p = params.get(i);
			sb.append(effectiveParams.get(p.getName()));
			if (i < params.size() - 1)
				sb.append(",");
		}
		sb.append("\t");

		resultClustering.writeFormattedString(sb);
		sb.append("\n");
		sb.flush();
	}

	/**
	 * This method extracts the results after executing
	 * {@link #doExec(DataConfig, ProgramConfig, String[], Map, Map)}. By
	 * default, the result is stored in the R variable as specified by
	 * {@link #getThreadSafeResultVariableName()}.
	 * 
	 * @return A two dimensional float array, containing fuzzy coefficients for
	 *         each object and cluster. Rows correspond to objects and columns
	 *         correspond to clusters. The order of objects is the same as in
	 *         {@link #ids}.
	 * @throws RserveException
	 * @throws REXPMismatchException
	 * @throws InterruptedException
	 */
	protected abstract float[][] getFuzzyCoeffMatrixFromExecResult()
			throws RserveException, REXPMismatchException, InterruptedException;

	@Override
	public ComparableVersion getVersion() {
		try {
			return Repository.getVersionOfObjectDynamicClass(this);
		} catch (Exception e) {
			// cannot happen because we loaded the class before;
			e.printStackTrace();
			return new ComparableVersion("1");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.Program#asSerializable()
	 */
	@Override
	public SerializableRProgram asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableRProgram) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#asSerializable()
	 */
	@Override
	public SerializableRProgram asSerializableInternal()
			throws RepositoryObjectSerializationException {
		try {
			return new SerializableRProgram(this);
		} catch (RepositoryObjectSerializationException e) {
			throw new RepositoryObjectSerializationException(IRProgram.class,
					this.getName(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgram#hasAlias()
	 */
	@Override
	public boolean hasAlias() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgram#getAlias()
	 */
	@Override
	public final String getAlias() {
		return this.getClass().getAnnotation(ClustEvalAlias.class).alias();
	}
}
