/**
 * 
 */
package de.clusteval.program.r;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import de.clusteval.data.dataset.format.ISerializableDataSetFormat;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.program.SerializableProgramConfig;
import de.clusteval.run.runresult.format.IRunResultFormat;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * @author Christian Wiwie
 *
 */
public class SerializableRProgramConfig
		extends
			SerializableProgramConfig<IRProgramConfig>
		implements
			ISerializableRProgramConfig {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4651063197400895963L;

	protected ISerializableRProgram program;

	protected boolean expectsNormalizedDataSet;

	protected int maxExecutionTimeMinutes;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 * @param program
	 * @param optimizableParameters
	 * @param expectsNormalizedDataSet
	 * @param maxExecutionTimeMinutes
	 */
	public SerializableRProgramConfig(File absPath, String name, String version,
			ISerializableRProgram program,
			List<IProgramParameter<?>> optimizableParameters,
			final boolean expectsNormalizedDataSet,
			final int maxExecutionTimeMinutes) {
		super(absPath, name, version, optimizableParameters);
		this.program = program;

		this.expectsNormalizedDataSet = expectsNormalizedDataSet;
		this.maxExecutionTimeMinutes = maxExecutionTimeMinutes;
	}

	/**
	 * @param programConfig
	 * @throws RepositoryObjectSerializationException
	 */
	public SerializableRProgramConfig(final IRProgramConfig programConfig)
			throws RepositoryObjectSerializationException {
		super(programConfig);
		if (programConfig.getProgram() != null)
			this.program = programConfig.getProgram().asSerializable();

		this.expectsNormalizedDataSet = programConfig
				.expectsNormalizedDataSet();
		this.maxExecutionTimeMinutes = programConfig
				.getMaxExecutionTimeMinutes();
	}

	@Override
	public ISerializableRProgram getProgram() {
		return program;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.ISerializableRProgramConfig#expectsNormalizedDataSet
	 * ()
	 */
	@Override
	public boolean expectsNormalizedDataSet() {
		return this.wrappedComponent.expectsNormalizedDataSet();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ISerializableRProgramConfig#
	 * getMaxExecutionTimeMinutes()
	 */
	@Override
	public int getMaxExecutionTimeMinutes() {
		return this.wrappedComponent.getMaxExecutionTimeMinutes();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ISerializableRProgramConfig#
	 * supportsInternalParameterOptimization()
	 */
	@Override
	public boolean supportsInternalParameterOptimization() {
		return wrappedComponent
				.getInvocationFormatParameterOptimization(false) != null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.SerializableWrapper#deserializeInternal
	 * (de.clusteval.framework.repository.IRepository)
	 */
	@Override
	protected IRProgramConfig deserializeInternal(IRepository repository)
			throws DeserializationException {
		try {
			return (IRProgramConfig) repository
					.getStaticObjectWithNameAndVersion(IProgramConfig.class,
							name + ":" + version);
		} catch (ObjectNotRegisteredException
				| ObjectVersionNotRegisteredException e) {
		}
		return new RProgramConfig(repository, System.currentTimeMillis(),
				new File(FileUtils.buildPath(
						repository.getBasePath(IProgramConfig.class),
						this.name + ".v" + version + ".config")),
				program.deserialize(repository), optimizableParameters,
				expectsNormalizedDataSet, maxExecutionTimeMinutes);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ISerializableProgramConfig#getParameters()
	 */
	@Override
	public List<IProgramParameter<?>> getParameters() {
		return this.program.getParameters();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ISerializableProgramConfig#
	 * getCompatibleDataSetFormats()
	 */
	@Override
	public List<ISerializableDataSetFormat> getCompatibleDataSetFormats() {
		return new ArrayList<>(this.getProgram().getCompatibleDataSetFormats());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ISerializableProgramConfig#getInvocationFormat(
	 * boolean)
	 */
	@Override
	public String getInvocationFormat(boolean withoutGoldStandard) {
		return this.getProgram().getInvocationFormat();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ISerializableProgramConfig#
	 * getInvocationFormatParameterOptimization(boolean)
	 */
	@Override
	public String getInvocationFormatParameterOptimization(
			boolean withoutGoldStandard) {
		return this.getProgram().getInvocationFormat();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ISerializableProgramConfig#getOutputFormat()
	 */
	@Override
	public ISerializableWrapperRepositoryObject<IRunResultFormat> getOutputFormat() {
		return this.getProgram().getRunResultFormat();
	}
}
