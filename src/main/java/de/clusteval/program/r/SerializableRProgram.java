/**
 * 
 */
package de.clusteval.program.r;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.data.dataset.format.ISerializableDataSetFormat;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RepositoryObjectDeserializationException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.program.IProgramParameter;
import de.clusteval.program.IStandaloneProgram;
import de.clusteval.program.SerializableProgram;
import de.clusteval.run.runresult.format.ISerializableRunResultFormat;
import de.clusteval.run.runresult.format.UnknownRunResultFormatException;
import de.clusteval.utils.DynamicComponentInitializationException;

/**
 * @author Christian Wiwie
 *
 */
public class SerializableRProgram extends SerializableProgram<IRProgram>
		implements
			ISerializableRProgram {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6862900122058354974L;

	protected List<IProgramParameter<?>> parameters;

	protected Set<ISerializableDataSetFormat> compatibleInputFormats;

	protected ISerializableRunResultFormat runResultFormat;

	protected String invocationFormat;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 * @param minorName
	 * @param majorName
	 * @param alias
	 * @param params
	 * @param compatibleInputFormats
	 * @param runResultFormat
	 * @param invocationFormat
	 */
	public SerializableRProgram(final File absPath, final String name,
			final String version, final String minorName,
			final String majorName, final String alias,
			final List<IProgramParameter<?>> params,
			final Set<ISerializableDataSetFormat> compatibleInputFormats,
			final ISerializableRunResultFormat runResultFormat,
			final String invocationFormat) {
		super(absPath, name, version, minorName, majorName, alias);
		this.parameters = params;
		this.compatibleInputFormats = compatibleInputFormats;
		this.runResultFormat = runResultFormat;
		this.invocationFormat = invocationFormat;
	}

	/**
	 * @param program
	 * @throws RepositoryObjectSerializationException
	 */
	public SerializableRProgram(final IRProgram program)
			throws RepositoryObjectSerializationException {
		super(program);
		this.parameters = program.getParameters();
		this.compatibleInputFormats = new HashSet<>();
		for (IDataSetFormat f : program.getCompatibleDataSetFormats())
			this.compatibleInputFormats.add(f.asSerializable());
		this.runResultFormat = program.getRunResultFormat().asSerializable();
		this.invocationFormat = program.getInvocationFormat();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.SerializableWrapper#deserializeInternal
	 * (de.clusteval.framework.repository.IRepository)
	 */
	@Override
	protected IRProgram deserializeInternal(IRepository repository)
			throws DeserializationException {
		try {
			return RProgram.parseFromString(repository, name, true);
		} catch (UnknownRProgramException
				| DynamicComponentInitializationException
				| UnknownDataSetFormatException
				| UnknownRunResultFormatException e) {
			throw new RepositoryObjectDeserializationException(
					IStandaloneProgram.class, this.name, e);
		}
	}

	/**
	 * @return the parameters
	 */
	@Override
	public List<IProgramParameter<?>> getParameters() {
		return parameters;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.r.ISerializableRProgram#getCompatibleDataSetFormats(
	 * )
	 */
	@Override
	public Set<ISerializableDataSetFormat> getCompatibleDataSetFormats() {
		return this.compatibleInputFormats;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.r.ISerializableRProgram#getInvocationFormat()
	 */
	@Override
	public String getInvocationFormat() {
		return this.invocationFormat;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.r.ISerializableRProgram#getRunResultFormat()
	 */
	@Override
	public ISerializableRunResultFormat getRunResultFormat() {
		return this.runResultFormat;
	}
}
