/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.program.r;

import java.util.Map;

import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RserveException;

import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.AbsoluteDataSet;
import de.clusteval.data.dataset.DataMatrix;
import de.clusteval.data.dataset.RelativeDataSet;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.program.IProgramConfig;
import de.clusteval.run.runresult.format.UnknownRunResultFormatException;
import de.clusteval.utils.DynamicComponentInitializationException;
import dk.sdu.imada.compbio.utils.SimilarityMatrix;

/**
 * This class represents R programs, which are compatible to relative and
 * absolute datasets.
 * 
 * @author Christian Wiwie
 * 
 */
public abstract class AbsoluteAndRelativeDataRProgram extends RProgram {

	/**
	 * 
	 */
	private static final long serialVersionUID = 161465770713712988L;

	/**
	 * @param repository
	 * @throws UnknownDataSetFormatException
	 * @throws UnknownRunResultFormatException
	 * @throws DynamicComponentInitializationException
	 */
	public AbsoluteAndRelativeDataRProgram(IRepository repository)
			throws UnknownDataSetFormatException,
			UnknownRunResultFormatException,
			DynamicComponentInitializationException {
		super(repository);
	}

	/**
	 * 
	 * @param other
	 *            The object to clone.
	 * @throws UnknownDataSetFormatException
	 * @throws UnknownRunResultFormatException
	 * @throws DynamicComponentInitializationException
	 * 
	 */
	public AbsoluteAndRelativeDataRProgram(
			AbsoluteAndRelativeDataRProgram other)
			throws UnknownDataSetFormatException,
			UnknownRunResultFormatException,
			DynamicComponentInitializationException {
		super(other);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.r.RProgram#extractDataSetContent(de.clusteval.data
	 * .DataConfig)
	 */
	@Override
	protected Object extractDataSetContent(IDataConfig dataConfig) {
		boolean absoluteData = dataConfig.getDatasetConfig().getDataSet()
				.getOriginalDataSet() instanceof AbsoluteDataSet;
		Object content;
		if (absoluteData) {
			AbsoluteDataSet dataSet = (AbsoluteDataSet) (dataConfig
					.getDatasetConfig().getDataSet().getOriginalDataSet());

			DataMatrix dataMatrix = dataSet.getDataSetContent();
			this.ids = dataMatrix.getIds();
			this.x = dataMatrix.getData();
			content = dataMatrix;
		} else {
			RelativeDataSet dataSet = (RelativeDataSet) (dataConfig
					.getDatasetConfig().getDataSet().getInStandardFormat());
			SimilarityMatrix simMatrix = dataSet.getDataSetContent();
			this.ids = dataSet.getIds().toArray(new String[0]);
			this.x = simMatrix.toArray();
			content = simMatrix;
		}
		return content;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.r.RProgram#initData(de.clusteval.data.IDataConfig,
	 * de.clusteval.program.IProgramConfig, java.lang.String[], java.util.Map,
	 * java.util.Map)
	 */
	@Override
	public void initData(IDataConfig dataConfig, IProgramConfig programConfig,
			String[] invocationLine, Map<String, String> effectiveParams,
			Map<String, String> internalParams)
			throws REngineException, InterruptedException {
		super.initData(dataConfig, programConfig, invocationLine,
				effectiveParams, internalParams);

		boolean absoluteData = dataConfig.getDatasetConfig().getDataSet()
				.getOriginalDataSet() instanceof AbsoluteDataSet;
		if (absoluteData) {
			rEngine.assign("x", x);
			rEngine.voidEval("rownames(x) <- ids");
			rEngine.voidEval("lockBinding('x', .GlobalEnv)");
		} else {
			rEngine.assign("sim", x);
			rEngine.voidEval("sim <- max(sim)-sim");
			rEngine.voidEval("rownames(sim) <- as.character(1:nrow(sim))");
			rEngine.voidEval("colnames(sim) <- as.character(1:nrow(sim))");
			this.convertDistancesToAppropriateDatastructure();
			rEngine.voidEval("lockBinding('sim', .GlobalEnv)");
		}
	}

	protected void convertDistancesToAppropriateDatastructure()
			throws RserveException, InterruptedException {
		rEngine.voidEval("sim <- as.dist(sim)");
	}
}
