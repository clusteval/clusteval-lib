/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.program.r;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration.HierarchicalINIConfiguration;

import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryEvent;
import de.clusteval.framework.repository.RepositoryObjectDumpException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.RepositoryRemoveEvent;
import de.clusteval.framework.repository.RepositoryReplaceEvent;
import de.clusteval.program.IProgram;
import de.clusteval.program.IProgramParameter;
import de.clusteval.program.Program;
import de.clusteval.program.ProgramConfig;
import de.clusteval.run.runresult.format.IRunResultFormat;
import dk.sdu.imada.compbio.utils.StringExt;

/**
 * Objects of this class encapsulate the configuration of rprograms.
 * 
 * <p>
 * In principle this class does the same as a regular {@link ProgramConfig},
 * except that it takes the invocatin formats from the rprogram.
 * 
 * @author Christian Wiwie
 * 
 */
public class RProgramConfig extends ProgramConfig implements IRProgramConfig {

	/**
	 * 
	 */
	private static final long serialVersionUID = 977555023781924368L;

	/**
	 * Instantiates a new rprogram configuration.
	 * 
	 * @param repository
	 *            The repository this program configuration should be registered
	 *            at.
	 * @param changeDate
	 *            The change date of this program configuration is used for
	 *            equality checks.
	 * @param absPath
	 *            The absolute path of this program configuration.
	 * @param program
	 *            The program this program configuration belongs to.
	 * @param optimizableParameters
	 *            The parameters of the program, that can be optimized.
	 * @param expectsNormalizedDataSet
	 *            Whether the encapsulated program requires normalized input.
	 * @param maxExecutionTimeMinutes
	 */
	public RProgramConfig(IRepository repository, long changeDate, File absPath,
			IProgram program, List<IProgramParameter<?>> optimizableParameters,
			boolean expectsNormalizedDataSet, int maxExecutionTimeMinutes) {
		super(repository, changeDate, absPath, program, optimizableParameters,
				expectsNormalizedDataSet, maxExecutionTimeMinutes);
	}

	/**
	 * The copy constructor for RProgram configurations.
	 * 
	 * @param programConfig
	 *            The rprogram configuration to clone.
	 */
	public RProgramConfig(RProgramConfig programConfig) {
		super(programConfig);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.ProgramConfig#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.r.IRProgramConfig#clone()
	 */
	@Override
	public IRProgramConfig clone() {
		return new RProgramConfig(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.ProgramConfig#ensureVersionsForAllComponents(java.
	 * lang.String)
	 */
	@Override
	protected String ensureVersionsForAllComponents(String fileContents) {
		String newfileContents = super.ensureVersionsForAllComponents(
				fileContents);
		newfileContents = ensureVersionForComponent(newfileContents, program);

		return newfileContents;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ProgramConfig#getProgram()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.r.IRProgramConfig#getProgram()
	 */
	@Override
	public IRProgram getProgram() {
		return (IRProgram) super.getProgram();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ProgramConfig#asSerializable()
	 */
	@Override
	public ISerializableRProgramConfig asSerializable()
			throws RepositoryObjectSerializationException {
		return (ISerializableRProgramConfig) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ProgramConfig#asSerializable()
	 */
	@Override
	public ISerializableRProgramConfig asSerializableInternal()
			throws RepositoryObjectSerializationException {
		return new SerializableRProgramConfig(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.DumpableRepositoryObject#
	 * dumpToFileHelper()
	 */
	@Override
	protected void dumpToFileHelper(final HierarchicalINIConfiguration conf)
			throws RepositoryObjectDumpException {
		try {
			conf.addProperty("type", this.getProgram());
			conf.addProperty("optimizationParameters",
					StringExt.paste(",", this.getOptimizableParameters()));
			conf.addProperty("alias", this.getProgramAlias());
			conf.addProperty("expectsNormalizedDataSet",
					Boolean.toString(expectsNormalizedDataSet));
			conf.save(absPath.getAbsolutePath());
		} catch (Exception e) {
			throw new RepositoryObjectDumpException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramConfig#getProgramAlias()
	 */
	@Override
	public String getProgramAlias() {
		return this.getProgram().getAlias();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramConfig#notify(de.clusteval.framework.
	 * repository.RepositoryEvent)
	 */
	@Override
	public void notify(RepositoryEvent e) throws RegisterException {
		if (e instanceof RepositoryReplaceEvent) {
			RepositoryReplaceEvent event = (RepositoryReplaceEvent) e;
			if (event.getOld().equals(this)) {
				super.notify(event);
			} else {
				if (event.getOld().equals(program)) {
					event.getOld().removeListener(this);
					this.log.info("ProgramConfig " + this
							+ ": Program reloaded due to modifications in filesystem");
					event.getReplacement().addListener(this);
					// added 06.07.2012
					this.program = (Program) event.getReplacement();
				}
			}
		} else if (e instanceof RepositoryRemoveEvent) {
			RepositoryRemoveEvent event = (RepositoryRemoveEvent) e;
			if (event.getRemovedObject().equals(this)) {
				super.notify(event);
			} else {
				if (event.getRemovedObject().equals(program)) {
					event.getRemovedObject().removeListener(this);
					this.log.info("ProgramConfig " + this
							+ ": Removed, because Program " + program
							+ " was removed.");
					RepositoryRemoveEvent newEvent = new RepositoryRemoveEvent(
							this);
					this.unregister();
					this.notify(newEvent);
				} else if (this.getProgram().getCompatibleDataSetFormats()
						.contains(event.getRemovedObject())) {
					event.getRemovedObject().removeListener(this);
					this.log.info("ProgramConfig " + this
							+ ": Removed, because DataSetFormat "
							+ event.getRemovedObject() + " has changed.");
					RepositoryRemoveEvent newEvent = new RepositoryRemoveEvent(
							this);
					this.unregister();
					this.notify(newEvent);
				} // the runresult format class changed
				else if (this.getProgram().getRunResultFormat()
						.equals(event.getRemovedObject())) {
					event.getRemovedObject().removeListener(this);
					this.log.info("ProgramConfig " + this
							+ ": Removed, because RunResultFormat "
							+ event.getRemovedObject() + " has changed.");
					RepositoryRemoveEvent newEvent = new RepositoryRemoveEvent(
							this);
					this.unregister();
					this.notify(newEvent);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#register()
	 */
	@Override
	public boolean register() throws RegisterException {
		if (super.register()) {
			this.program.register();
			this.program.addListener(this);

			for (IDataSetFormat dsFormat : this.getProgram()
					.getCompatibleDataSetFormats()) {
				dsFormat.register();
				dsFormat.addListener(this);
			}

			this.getProgram().getRunResultFormat().register();
			this.getProgram().getRunResultFormat().addListener(this);
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramConfig#getCompatibleDataSetFormats()
	 */
	@Override
	public List<IDataSetFormat> getCompatibleDataSetFormats() {
		return new ArrayList<>(this.getProgram().getCompatibleDataSetFormats());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramConfig#getInvocationFormat(boolean)
	 */
	@Override
	public String getInvocationFormat(boolean withoutGoldStandard) {
		return this.getProgram().getInvocationFormat();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramConfig#
	 * getInvocationFormatParameterOptimization(boolean)
	 */
	@Override
	public String getInvocationFormatParameterOptimization(
			boolean withoutGoldStandard) {
		return this.getProgram().getInvocationFormat();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramConfig#getOutputFormat()
	 */
	@Override
	public IRunResultFormat getOutputFormat() {
		return this.getProgram().getRunResultFormat();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramConfig#getParameters()
	 */
	@Override
	public List<IProgramParameter<?>> getParameters() {
		return this.getProgram().getParameters();
	}
}
