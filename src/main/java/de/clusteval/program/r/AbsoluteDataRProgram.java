/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.program.r;

import java.util.Map;

import org.rosuda.REngine.REngineException;

import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.AbsoluteDataSet;
import de.clusteval.data.dataset.DataMatrix;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.framework.RLibraryNotLoadedException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.program.IProgramConfig;
import de.clusteval.run.runresult.format.UnknownRunResultFormatException;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.RNotAvailableException;

/**
 * @author Christian Wiwie
 * 
 */
public abstract class AbsoluteDataRProgram extends RProgram {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1228348149116626165L;

	/**
	 * @param repository
	 *            the repository this program should be registered at.
	 * @throws DynamicComponentInitializationException
	 * @throws UnknownRunResultFormatException
	 * @throws UnknownDataSetFormatException
	 */
	public AbsoluteDataRProgram(IRepository repository)
			throws UnknownDataSetFormatException,
			UnknownRunResultFormatException,
			DynamicComponentInitializationException {
		super(repository);
	}

	/**
	 * The copy constructor for rprograms.
	 * 
	 * @param rProgram
	 *            The object to clone.
	 */
	public AbsoluteDataRProgram(final AbsoluteDataRProgram rProgram) {
		super(rProgram);
	}

	@Override
	public void initData(IDataConfig dataConfig, IProgramConfig programConfig,
			String[] invocationLine, Map<String, String> effectiveParams,
			Map<String, String> internalParams)
			throws REngineException, InterruptedException {
		super.initData(dataConfig, programConfig, invocationLine,
				effectiveParams, internalParams);
		rEngine.assign("x", x);
		rEngine.voidEval("rownames(x) <- ids");
		rEngine.voidEval("lockBinding('x', .GlobalEnv)");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.r.RProgram#extractDataSetContent(de.clusteval.data
	 * .DataConfig)
	 */
	@Override
	protected DataMatrix extractDataSetContent(IDataConfig dataConfig) {
		AbsoluteDataSet dataSet = (AbsoluteDataSet) (dataConfig
				.getDatasetConfig().getDataSet().getOriginalDataSet());
		DataMatrix dataMatrix = dataSet.getDataSetContent();
		this.ids = dataSet.getIds().toArray(new String[0]);
		this.x = dataMatrix.getData();
		return dataMatrix;
	}
}
