/**
 * 
 */
package de.clusteval.program.r;

import java.util.Map;

import org.rosuda.REngine.Rserve.RserveException;

import de.clusteval.data.IDataConfig;
import de.clusteval.program.IProgramConfig;

/**
 * @author Christian Wiwie
 *
 */

public class RProgramThread extends Thread {

	protected Thread poolThread;
	protected Exception ex;
	protected IRProgram rProgram;
	protected IDataConfig dataConfig;
	protected IProgramConfig programConfig;
	protected String[] invocationLine;
	protected Map<String, String> effectiveParams;
	protected Map<String, String> internalParams;

	/**
	 * @param rProgram
	 * @param dataConfig
	 * @param programConfig
	 * @param invocationLine
	 * @param effectiveParams
	 * @param internalParams
	 * @throws RserveException
	 */
	public RProgramThread(final Thread t, final IRProgram rProgram,
			final IDataConfig dataConfig, final IProgramConfig programConfig,
			final String[] invocationLine,
			final Map<String, String> effectiveParams,
			final Map<String, String> internalParams) throws RserveException {
		super();

		this.poolThread = t;
		// we need to clone here, as each thread needs its own object
		this.rProgram = rProgram.clone();
		this.dataConfig = dataConfig;
		this.programConfig = programConfig;
		this.invocationLine = invocationLine;
		this.effectiveParams = effectiveParams;
		this.internalParams = internalParams;
	}

	@Override
	public void run() {
		// we initialize the rEngine here and take the one from the thread pool
		// thread
		try {
			this.rProgram.setRengine(
					this.rProgram.getRepository().getRengine(this.poolThread));
			this.rProgram.initData(dataConfig, programConfig, invocationLine,
					effectiveParams, internalParams);


			this.rProgram.beforeExec(dataConfig, programConfig, invocationLine,
					effectiveParams, internalParams);
			if (this.isInterrupted())
				throw new InterruptedException();
			this.rProgram.doExec(dataConfig, programConfig, invocationLine,
					effectiveParams, internalParams);
			if (this.isInterrupted())
				throw new InterruptedException();
			this.rProgram.afterExec(dataConfig, programConfig, invocationLine,
					effectiveParams, internalParams);
		} catch (Exception e) {
			ex = e;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Thread#interrupt()
	 */
	@Override
	public void interrupt() {
		super.interrupt();
		try {
			if (this.rProgram.getRengine() != null)
				this.rProgram.getRengine().interrupt();
		} finally {
			this.rProgram.getRepository().clearRengine(poolThread);
		}
	}

	public Exception getException() {
		return this.ex;
	}
}