/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
package de.clusteval.program.r;

import java.io.File;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Iterator;

import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.run.runresult.format.UnknownRunResultFormatException;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.DynamicComponentMissingVersionException;
import de.clusteval.utils.IncompatibleClustEvalVersionException;
import de.clusteval.utils.InvalidClustEvalVersionException;
import de.clusteval.utils.InvalidDependencyTargetException;
import de.clusteval.utils.JARFinder;
import de.clusteval.utils.JARFinderClassLoader;
import de.clusteval.utils.MissingClustEvalVersionException;
import dk.sdu.imada.compbio.utils.ArrayIterator;

/**
 * Objects of this class look for new RPrograms in the program-directory defined
 * in the corresponding repository.
 * 
 * @author Christian Wiwie
 * 
 * 
 */
public class RProgramFinder extends JARFinder<IRProgram> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2592712336889748070L;

	/**
	 * Instantiates a new RProgram finder.
	 * 
	 * @param repository
	 *            the repository
	 * @throws RegisterException
	 */
	public RProgramFinder(final IRepository repository)
			throws RegisterException {
		super(repository, IRProgram.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.Finder#checkFile(java.io.File)
	 */
	@Override
	protected boolean checkFile(File file) {
		return file.isFile() && super.checkFile(file);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.Finder#getIterator()
	 */
	@Override
	protected Iterator<File> getIterator() {
		return new ArrayIterator<File>(getBaseDir().listFiles());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.wiwie.wiutils.utils.JARFinder#getURLClassLoader0(java.io.File)
	 */
	@Override
	protected URLClassLoader getURLClassLoader0(File f,
			final ClassLoader parent) throws MalformedURLException {
		URL url = f.toURI().toURL();
		return new RProgramURLClassLoader(this, new URL[]{url}, parent);
	}
}

class RProgramURLClassLoader extends JARFinderClassLoader<IRProgram> {

	/**
	 * @param urls
	 * @param parent
	 * @param loaderParent
	 */
	public RProgramURLClassLoader(RProgramFinder parent, URL[] urls,
			ClassLoader loaderParent) {
		super(parent, urls, loaderParent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.ClassLoader#loadClass(java.lang.String)
	 */
	@Override
	public Class<?> loadClass(String name) throws ClassNotFoundException {
		Class<?> result = super.loadClass(name);

		if (Modifier.isAbstract(result.getModifiers()) || result.isInterface())
			return result;

		if (name.startsWith("de.clusteval.program.r")) {
			if (name.endsWith("RProgram")) {
				@SuppressWarnings("unchecked")
				Class<? extends RProgram> rProgram = (Class<? extends RProgram>) result;

				try {

					// loadClass may have been called not for the newly
					// discovered class of this JAR, but for a parent class
					// which had already been registered before;
					// thus, we only register this class, if it hasn't been
					// registered before
					if (!this.parent.getRepository()
							.isClassRegistered(rProgram)) {
						this.parent.getRepository().registerClass(
								this.parent.getClassToFind(), rProgram);
					}
				} catch (DynamicComponentMissingVersionException
						| DynamicComponentInitializationException
						| IncompatibleClustEvalVersionException
						| MissingClustEvalVersionException
						| InvalidClustEvalVersionException
						| InvalidDependencyTargetException e) {
					handleException(e);
				}

				IRProgram program;
				try {
					program = RProgram.parseFromString(
							this.parent.getRepository(),
							rProgram.getSimpleName(), false);
					program.register();
				} catch (UnknownRProgramException | RegisterException
						| DynamicComponentInitializationException
						| UnknownDataSetFormatException
						| UnknownRunResultFormatException e) {
					handleException(e);
				}
			}
		}
		return result;
	}
}
