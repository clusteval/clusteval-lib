/**
 * 
 */
package de.clusteval.program;

import java.io.File;

import de.clusteval.context.IContext;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import dk.sdu.imada.compbio.file.FileUtils;

/**
 * @author Christian Wiwie
 *
 */
public class SerializableStandaloneProgram
		extends
			SerializableProgram<IStandaloneProgram>
		implements
			ISerializableStandaloneProgram {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8636353889648011653L;

	protected ISerializableWrapperRepositoryObject<IContext> context;

	/**
	 * @param absPath
	 * @param version
	 * @param minorName
	 * @param majorName
	 * @param context
	 */
	public SerializableStandaloneProgram(File absPath, String version,
			String minorName, String majorName,
			ISerializableWrapperRepositoryObject<IContext> context,
			String alias) {
		super(absPath, majorName + "/" + minorName, version, minorName,
				majorName, alias);
		this.context = context;
	}

	/**
	 * @param program
	 * @throws RepositoryObjectSerializationException
	 */
	public SerializableStandaloneProgram(final IStandaloneProgram program) throws RepositoryObjectSerializationException {
		super(program);
		if (program.getContext() != null)
			this.context = program.getContext().asSerializable();
	}

	/**
	 * @return the context
	 */
	@Override
	public ISerializableWrapperRepositoryObject<IContext> getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.framework.repository.SerializableWrapper#deserializeInternal
	 * (de.clusteval.framework.repository.IRepository)
	 */
	@Override
	protected IStandaloneProgram deserializeInternal(IRepository repository)
			throws DeserializationException {
		try {
			return repository.getStaticObjectWithNameAndVersion(
					IStandaloneProgram.class, name + ":" + version);
		} catch (ObjectNotRegisteredException
				| ObjectVersionNotRegisteredException e) {
		}
		return new StandaloneProgram(repository,
				context.deserialize(repository), System.currentTimeMillis(),
				new File(FileUtils.buildPath(
						repository.getBasePath(IProgramConfig.class),
						this.majorName + ".v" + version, this.minorName)),
				alias);
	}
}
