/**
 * 
 */
package de.clusteval.program;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.HierarchicalINIConfiguration;
import org.apache.commons.configuration.SubnodeConfiguration;

import de.clusteval.data.dataset.format.DataSetFormat;
import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryEvent;
import de.clusteval.framework.repository.RepositoryObjectDumpException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.RepositoryRemoveEvent;
import de.clusteval.framework.repository.RepositoryReplaceEvent;
import de.clusteval.run.runresult.format.IRunResultFormat;
import dk.sdu.imada.compbio.utils.StringExt;

/**
 * @author Christian Wiwie
 *
 */
public class StandaloneProgramConfig extends ProgramConfig
		implements
			IStandaloneProgramConfig {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5160997805814701134L;
	/**
	 * A list holding all parameters of the program.
	 */
	protected List<IProgramParameter<?>> params;

	protected String alias;
	protected Map<String, String> envVars;
	/**
	 * This list holds all dataset formats that are compatible with the
	 * encapsulated program, i.e. input formats this program is able to read.
	 */
	protected transient List<IDataSetFormat> compatibleDataSetFormats;
	/**
	 * This is the default invocation line used to invoke the program, when this
	 * program configuration is used together with some data configuration.
	 * 
	 * <p>
	 * This invocation line is used, if
	 * <ul>
	 * <li>there is a goldstandard in the data configuration</li>
	 * <li>the run is not of type parameter optimization</li>
	 * </ul>
	 */
	protected String invocationFormat;
	/**
	 * This invocation line is used, if
	 * <ul>
	 * <li>there is a goldstandard in the data configuration</li>
	 * <li>and the run is of type parameter optimization</li>
	 * </ul>
	 */
	protected String invocationFormatParameterOptimization;
	/**
	 * This invocation line is used, if
	 * <ul>
	 * <li>there is no goldstandard in the data configuration</li>
	 * <li>and the run is of type parameter optimization</li>
	 * </ul>
	 */
	protected String invocationFormatParameterOptimizationWithoutGoldStandard;
	/**
	 * This invocation line is used, if
	 * <ul>
	 * <li>there is no goldstandard in the data configuration</li>
	 * <li>and the run is not of type parameter optimization</li>
	 * </ul>
	 */
	protected String invocationFormatWithoutGoldStandard;
	/**
	 * The output format of the program
	 */
	protected transient IRunResultFormat outputFormat;

	/**
	 * @param repository
	 * @param changeDate
	 * @param absPath
	 * @param program
	 * @param outputFormat
	 *            The output format of the program.
	 * @param compatibleDataSetFormats
	 *            A list of compatible dataset formats of the encapsulated
	 *            program.
	 * @param invocationFormat
	 *            The invocation line for runs with goldstandard and without
	 *            parameter optimization
	 * @param invocationFormatWithoutGoldStandard
	 *            The invocation line for runs without goldstandard and without
	 *            parameter optimization
	 * @param invocationFormatParameterOptimization
	 *            The invocation line for runs with goldstandard and with
	 *            parameter optimization
	 * @param invocationFormatParameterOptimizationWithoutGoldStandard
	 *            The invocation line for runs without goldstandard and with
	 *            parameter optimization
	 * @param optimizableParameters
	 *            The parameters of the program, that can be optimized.
	 * @param expectsNormalizedDataSet
	 * @param maxExecutionTimeMinutes
	 * @param params
	 *            The parameters of the program.
	 * @param alias
	 *            The alias of this program.
	 * @param envVars
	 *            The environmental variables to set when this program is
	 *            executed.
	 */
	public StandaloneProgramConfig(IRepository repository, long changeDate,
			File absPath, IProgram program, IRunResultFormat outputFormat,
			List<IDataSetFormat> compatibleDataSetFormats,
			String invocationFormat, String invocationFormatWithoutGoldStandard,
			String invocationFormatParameterOptimization,
			String invocationFormatParameterOptimizationWithoutGoldStandard,
			List<IProgramParameter<?>> params,
			List<IProgramParameter<?>> optimizableParameters,
			boolean expectsNormalizedDataSet, int maxExecutionTimeMinutes,
			final String alias, final Map<String, String> envVars) {
		super(repository, changeDate, absPath, program, optimizableParameters,
				expectsNormalizedDataSet, maxExecutionTimeMinutes);
		this.params = params;
		this.alias = alias;
		this.envVars = envVars;
		this.outputFormat = outputFormat;
		this.compatibleDataSetFormats = compatibleDataSetFormats;

		this.invocationFormat = invocationFormat;
		this.invocationFormatWithoutGoldStandard = invocationFormatWithoutGoldStandard;
		this.invocationFormatParameterOptimization = invocationFormatParameterOptimization;
		this.invocationFormatParameterOptimizationWithoutGoldStandard = invocationFormatParameterOptimizationWithoutGoldStandard;
	}

	/**
	 * @param programConfig
	 */
	public StandaloneProgramConfig(StandaloneProgramConfig programConfig) {
		super(programConfig);

		this.params = ProgramParameter.cloneParameterList(programConfig.params);
		this.alias = programConfig.alias;
		this.envVars = new HashMap<String, String>(programConfig.envVars);
		this.outputFormat = programConfig.outputFormat.clone();
		this.compatibleDataSetFormats = DataSetFormat
				.cloneDataSetFormats(programConfig.compatibleDataSetFormats);
		this.invocationFormat = programConfig.invocationFormat;
		this.invocationFormatWithoutGoldStandard = programConfig.invocationFormatWithoutGoldStandard;
		this.invocationFormatParameterOptimization = programConfig.invocationFormatParameterOptimization;
		this.invocationFormatParameterOptimizationWithoutGoldStandard = programConfig.invocationFormatParameterOptimizationWithoutGoldStandard;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramConfig#getParams()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IStandaloneProgramConfig#getParams()
	 */
	@Override
	public List<IProgramParameter<?>> getParameters() {
		return params;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#clone()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IStandaloneProgramConfig#clone()
	 */
	@Override
	public IStandaloneProgramConfig clone() {
		return new StandaloneProgramConfig(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ProgramConfig#asSerializable()
	 */
	@Override
	public ISerializableProgramConfig<? extends IProgramConfig> asSerializable()
			throws RepositoryObjectSerializationException {
		return super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ProgramConfig#asSerializable()
	 */
	@Override
	public ISerializableProgramConfig<? extends IProgramConfig> asSerializableInternal()
			throws RepositoryObjectSerializationException {
		return new SerializableStandaloneProgramConfig(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.Program#getAlias()
	 */
	@Override
	public String getProgramAlias() {
		return this.alias;
	}

	/**
	 * @return the envVars
	 */
	@Override
	public Map<String, String> getEnvVars() {
		return envVars;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.DumpableRepositoryObject#
	 * dumpToFileHelper()
	 */
	@Override
	protected void dumpToFileHelper(final HierarchicalINIConfiguration conf)
			throws RepositoryObjectDumpException {
		try {
			conf.addProperty("type", "standalone");
			conf.addProperty("program", this.program.toString());
			conf.addProperty("parameters",
					StringExt.paste(",", this.getParameters()));
			conf.addProperty("optimizationParameters",
					StringExt.paste(",", this.getOptimizableParameters()));
			conf.addProperty("alias", this.getProgramAlias());
			conf.addProperty("expectsNormalizedDataSet",
					Boolean.toString(expectsNormalizedDataSet));
			conf.addProperty("compatibleDataSetFormats",
					StringExt.paste(",", this.getCompatibleDataSetFormats()));
			conf.addProperty("outputFormat", this.getOutputFormat());

			SubnodeConfiguration invocationFormat = conf
					.getSection("invocationFormat");
			invocationFormat.addProperty("invocationFormat",
					this.getInvocationFormat(false));

			for (IProgramParameter<?> p : this.getParameters()) {
				SubnodeConfiguration section = conf.getSection(p.getName());
				section.addProperty("desc", p.getDefault());
				section.addProperty("type", p instanceof IDoubleProgramParameter
						? "2"
						: (p instanceof IIntegerProgramParameter ? "1" : "0"));
				section.addProperty("def", p.getDefault());
				if (p.isMinValueSet())
					section.addProperty("minValue", p.getMinValue());
				if (p.isMaxValueSet())
					section.addProperty("maxValue", p.getMaxValue());
				if (p.isOptionsSet())
					section.addProperty("options",
							StringExt.paste(",", p.getOptions()));
			}

			conf.save(absPath.getAbsolutePath());
		} catch (Exception e) {
			throw new RepositoryObjectDumpException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ProgramConfig#getProgram()
	 */
	@Override
	public IStandaloneProgram getProgram() {
		return (IStandaloneProgram) super.getProgram();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramConfig#getCompatibleDataSetFormats()
	 */
	@Override
	public List<IDataSetFormat> getCompatibleDataSetFormats() {
		return compatibleDataSetFormats;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramConfig#getInvocationFormat(boolean)
	 */
	@Override
	public String getInvocationFormat(boolean withoutGoldStandard) {
		if (withoutGoldStandard)
			return invocationFormatWithoutGoldStandard;
		return invocationFormat;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramConfig#
	 * getInvocationFormatParameterOptimization(boolean)
	 */
	@Override
	public String getInvocationFormatParameterOptimization(
			boolean withoutGoldStandard) {
		if (withoutGoldStandard)
			return invocationFormatParameterOptimizationWithoutGoldStandard;
		return invocationFormatParameterOptimization;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramConfig#getOutputFormat()
	 */
	@Override
	public IRunResultFormat getOutputFormat() {
		return this.outputFormat;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#
	 * ensureVersionsForAllComponents(java.lang.String)
	 */
	@Override
	protected String ensureVersionsForAllComponents(String fileContents) {
		String newfileContents = super.ensureVersionsForAllComponents(
				fileContents);

		for (IRepositoryObject o : this.compatibleDataSetFormats)
			newfileContents = ensureVersionForComponent(newfileContents, o);
		newfileContents = ensureVersionForComponent(newfileContents,
				outputFormat);

		String old = this.program.getFullName();
		String withVersion = this.program.toString();
		newfileContents = newfileContents
				.replaceAll(String.format("%s(?!\\:)", old), withVersion);

		return newfileContents;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IProgramConfig#notify(de.clusteval.framework.
	 * repository.RepositoryEvent)
	 */
	@Override
	public void notify(RepositoryEvent e) throws RegisterException {
		if (e instanceof RepositoryReplaceEvent) {
			RepositoryReplaceEvent event = (RepositoryReplaceEvent) e;
			if (event.getOld().equals(this)) {
				super.notify(event);
			} else {
				if (event.getOld().equals(program)) {
					event.getOld().removeListener(this);
					this.log.info("ProgramConfig " + this
							+ ": Program reloaded due to modifications in filesystem");
					event.getReplacement().addListener(this);
					// added 06.07.2012
					this.program = (Program) event.getReplacement();
				}
			}
		} else if (e instanceof RepositoryRemoveEvent) {
			RepositoryRemoveEvent event = (RepositoryRemoveEvent) e;
			if (event.getRemovedObject().equals(this)) {
				super.notify(event);
			} else {
				if (event.getRemovedObject().equals(program)) {
					event.getRemovedObject().removeListener(this);
					this.log.info("ProgramConfig " + this
							+ ": Removed, because Program " + program
							+ " was removed.");
					RepositoryRemoveEvent newEvent = new RepositoryRemoveEvent(
							this);
					this.unregister();
					this.notify(newEvent);
				}
				// a dataset format class changed
				else if (this.compatibleDataSetFormats
						.contains(event.getRemovedObject())) {
					event.getRemovedObject().removeListener(this);
					this.log.info("ProgramConfig " + this
							+ ": Removed, because DataSetFormat "
							+ event.getRemovedObject() + " has changed.");
					RepositoryRemoveEvent newEvent = new RepositoryRemoveEvent(
							this);
					this.unregister();
					this.notify(newEvent);
				} // the runresult format class changed
				else if (this.outputFormat.equals(event.getRemovedObject())) {
					event.getRemovedObject().removeListener(this);
					this.log.info("ProgramConfig " + this
							+ ": Removed, because RunResultFormat "
							+ event.getRemovedObject() + " has changed.");
					RepositoryRemoveEvent newEvent = new RepositoryRemoveEvent(
							this);
					this.unregister();
					this.notify(newEvent);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#register()
	 */
	@Override
	public boolean register() throws RegisterException {
		if (super.register()) {
			this.program.register();
			this.program.addListener(this);

			for (IDataSetFormat dsFormat : this.compatibleDataSetFormats) {
				dsFormat.register();
				dsFormat.addListener(this);
			}

			outputFormat.register();
			outputFormat.addListener(this);
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.IProgramConfig#supportsInternalParameterOptimization
	 * ()
	 */
	@Override
	public boolean supportsInternalParameterOptimization() {
		return invocationFormatParameterOptimization != null;
	}
}
