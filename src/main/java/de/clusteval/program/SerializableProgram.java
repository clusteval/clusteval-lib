/**
 * 
 */
package de.clusteval.program;

import java.io.File;

import de.clusteval.framework.repository.SerializableWrapperRepositoryObject;

/**
 * @author Christian Wiwie
 *
 */
public abstract class SerializableProgram<P extends IProgram>
		extends
			SerializableWrapperRepositoryObject<P>
		implements
			ISerializableProgram<P> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8128139978137977368L;

	protected String minorName, majorName, alias;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 * @param minorName
	 * @param majorName
	 */
	public SerializableProgram(File absPath, String name, String version,
			String minorName, String majorName, String alias) {
		super(absPath, name, version);
		this.minorName = minorName;
		this.majorName = majorName;
		this.alias = alias;
	}

	/**
	 * @param program
	 */
	public SerializableProgram(final P program) {
		super(program);
		this.minorName = program.getMinorName();
		this.majorName = program.getMajorName();
		this.alias = program.getAlias();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ISerializableProgram#getMajorName()
	 */
	@Override
	public String getMajorName() {
		return majorName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ISerializableProgram#getMinorName()
	 */
	@Override
	public String getMinorName() {
		return minorName;
	}

	/**
	 * @return the alias
	 */
	@Override
	public String getAlias() {
		return alias;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ISerializableProgram#hasAlias()
	 */
	@Override
	public boolean hasAlias() {
		return alias != null;
	}
}
