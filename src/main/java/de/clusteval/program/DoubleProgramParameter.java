/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.program;

import javax.script.ScriptException;

import de.clusteval.data.IDataConfig;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.utils.InternalAttributeException;

/**
 * @author Christian Wiwie
 * 
 */
public class DoubleProgramParameter extends ProgramParameter<Double> implements IDoubleProgramParameter {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8267454587750026710L;

	/**
	 * The constructor for double program parameters.
	 * 
	 * @param name
	 *            The name of the parameter.
	 * @param desc
	 *            The description of the parameter.
	 * @param minValue
	 *            The minimal value of the parameter.
	 * @param maxValue
	 *            The maximal value of the parameter.
	 * @param def
	 *            The default value of the parameter.
	 */
	public DoubleProgramParameter(final String name, final String desc, String minValue, String maxValue, String def) {
		super(name, desc, minValue, maxValue, def);
	}

	/**
	 * The constructor for double program parameters.
	 * 
	 * @param name
	 *            The name of the parameter.
	 * @param desc
	 *            The description of the parameter.
	 * @param options
	 *            The possible values of this parameter.
	 * @param def
	 *            The default value of the parameter.
	 */
	public DoubleProgramParameter(final String name, final String desc, final String[] options, String def) {
		super(name, desc, options, def);
	}

	/**
	 * The copy constructor of double program parameters.
	 * 
	 * @param other
	 *            The object to clone.
	 */
	public DoubleProgramParameter(final DoubleProgramParameter other) {
		super(other);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.ProgramParameter#clone(program.ProgramParameter)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IDoubleProgramParameter#clone()
	 */
	@Override
	public DoubleProgramParameter clone() {
		return new DoubleProgramParameter(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.ProgramParameter#isMinValueSet()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IDoubleProgramParameter#isMinValueSet()
	 */
	@Override
	public boolean isMinValueSet() {
		return !this.minValue.equals("");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.ProgramParameter#isMaxValueSet()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IDoubleProgramParameter#isMaxValueSet()
	 */
	@Override
	public boolean isMaxValueSet() {
		return !this.maxValue.equals("");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.ProgramParameter#evaluateMinValue()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IDoubleProgramParameter#evaluateMinValue(de.
	 * clusteval.data.IDataConfig, de.clusteval.program.IProgramConfig)
	 */
	@Override
	public Double evaluateMinValue(final IRepository repository, final IDataConfig dataConfig,
			final IProgramConfig programConfig) throws InternalAttributeException {

		/*
		 * Parse minValue
		 */
		String newMinValue = repository.evaluateInternalAttributes(minValue, dataConfig, programConfig);

		try {
			newMinValue = repository.evaluateJavaScript(newMinValue);
		} catch (ScriptException e) {
			throw new InternalAttributeException("The expression '" + minValue + "' for parameter attribute "
					+ programConfig + "/" + this.name + "/minValue is invalid");
		}

		return Double.parseDouble(newMinValue);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.ProgramParameter#evaluateMaxValue()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IDoubleProgramParameter#evaluateMaxValue(de.
	 * clusteval.data.IDataConfig, de.clusteval.program.IProgramConfig)
	 */
	@Override
	public Double evaluateMaxValue(final IRepository repository, final IDataConfig dataConfig,
			final IProgramConfig programConfig) throws InternalAttributeException {

		/*
		 * Parse maxValue
		 */
		String newMaxValue = repository.evaluateInternalAttributes(maxValue, dataConfig, programConfig);

		try {
			newMaxValue = repository.evaluateJavaScript(newMaxValue);
		} catch (ScriptException e) {
			throw new InternalAttributeException("The expression '" + maxValue + "' for parameter attribute "
					+ programConfig + "/" + this.name + "/maxValue is invalid");
		}

		return Double.parseDouble(newMaxValue);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see program.ProgramParameter#evaluateDefaultValue()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.IDoubleProgramParameter#evaluateDefaultValue(de.
	 * clusteval.data.IDataConfig, de.clusteval.program.IProgramConfig)
	 */
	@Override
	public Double evaluateDefaultValue(final IRepository repository, final IDataConfig dataConfig,
			final IProgramConfig programConfig) throws InternalAttributeException {

		/*
		 * Parse default
		 */

		String newDefaultValue = repository.evaluateInternalAttributes(def, dataConfig, programConfig);

		try {
			newDefaultValue = repository.evaluateJavaScript(newDefaultValue);
		} catch (ScriptException e) {
			throw new InternalAttributeException("The expression '" + def + "' for parameter attribute " + programConfig
					+ "/" + this.name + "/def is invalid");
		}

		return Double.parseDouble(newDefaultValue);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.ProgramParameter#evaluateOptions(de.clusteval.data
	 * .DataConfig, de.clusteval.program.ProgramConfig)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.IDoubleProgramParameter#evaluateOptions(de.clusteval
	 * .data.IDataConfig, de.clusteval.program.IProgramConfig)
	 */
	@Override
	public Double[] evaluateOptions(final IRepository repository, IDataConfig dataConfig, IProgramConfig programConfig)
			throws InternalAttributeException {
		return new Double[0];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ProgramParameter#isOptionsSet()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.IDoubleProgramParameter#isOptionsSet()
	 */
	@Override
	public boolean isOptionsSet() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.IProgramParameter#withDifferentMin(java.lang.String)
	 */
	@Override
	public IProgramParameter<Double> withDifferentMin(String minValue) {
		return new DoubleProgramParameter(this.name, this.description, minValue, this.maxValue, def);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.IProgramParameter#withDifferentMax(java.lang.String)
	 */
	@Override
	public IProgramParameter<Double> withDifferentMax(String maxValue) {
		return new DoubleProgramParameter(this.name, this.description, this.minValue, maxValue, def);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.IProgramParameter#withDifferentOptions(java.lang.
	 * String[])
	 */
	@Override
	public IProgramParameter<Double> withDifferentOptions(String[] options) {
		return new DoubleProgramParameter(this.name, this.description, options, def);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.IProgramParameter#withDifferentDefault(java.lang.
	 * String)
	 */
	@Override
	public IProgramParameter<Double> withDifferentDefault(String def) {
		if (isOptionsSet())
			return new DoubleProgramParameter(this.name, this.description, options.clone(), def);
		return new DoubleProgramParameter(this.name, this.description, this.minValue, this.maxValue, def);
	}
}
