/**
 * 
 */
package de.clusteval.program;

import java.io.File;
import java.util.List;

import de.clusteval.framework.repository.SerializableWrapperRepositoryObject;

/**
 * @author Christian Wiwie
 *
 */
public abstract class SerializableProgramConfig<PC extends IProgramConfig>
		extends
			SerializableWrapperRepositoryObject<PC>
		implements
			ISerializableProgramConfig<PC> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2739490936142122196L;
	protected List<IProgramParameter<?>> optimizableParameters;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 */
	public SerializableProgramConfig(File absPath, String name, String version,
			List<IProgramParameter<?>> optimizableParameters) {
		super(absPath, name, version);
		this.optimizableParameters = optimizableParameters;
	}

	/**
	 * @param wrappedComponent
	 */
	public SerializableProgramConfig(PC wrappedComponent) {
		super(wrappedComponent);
		this.optimizableParameters = wrappedComponent
				.getOptimizableParameters();
	}

	@Override
	public abstract ISerializableProgram getProgram();

	@Override
	public List<IProgramParameter<?>> getOptimizableParameters() {
		return this.optimizableParameters;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.program.ISerializableStandaloneProgramConfig#
	 * getParameterForName(java.lang.String)
	 */
	@Override
	public IProgramParameter<?> getParameterForName(final String name) {
		IProgramParameter<?> pa = null;
		for (int i = 0; i < this.getParameters().size(); i++) {
			pa = this.getParameters().get(i);
			if (pa.toString().equals(name) || pa.getName().equals(name)) {
				return pa;
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.ISerializableProgramConfig#getParamWithId(java.lang.
	 * String)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.program.ISerializableStandaloneProgramConfig#getParamWithId(
	 * java.lang.String)
	 */
	@Override
	public IProgramParameter<?> getParamWithId(final String id)
			throws UnknownProgramParameterException {
		for (IProgramParameter<?> param : this.wrappedComponent.getParameters())
			if (param.getName().equals(id))
				return param;
		throw new UnknownProgramParameterException(
				"The program parameter with id \"" + id + "\" is unknown.");
	}

}