/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
package de.clusteval.context;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Set;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.Repository;
import de.clusteval.framework.repository.RepositoryObjectDynamicComponent;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.SerializableWrapperDynamicComponent;
import de.clusteval.run.runresult.format.IRunResultFormat;
import de.clusteval.utils.ClassVersion;
import de.clusteval.utils.DynamicComponentInitializationException;
import dk.sdu.imada.compbio.utils.Pair;

/**
 * @author Christian Wiwie
 * 
 */
@ClassVersion(version = "1")
public abstract class Context extends RepositoryObjectDynamicComponent
		implements
			IContext {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4276544753182669023L;

	/**
	 * @param repository
	 * @param contextName
	 * @return A context object of the class with the given simple name
	 * @throws UnknownContextException
	 * @throws DynamicComponentInitializationException
	 */
	public static IContext parseFromString(final IRepository repository,
			final String contextName) throws UnknownContextException,
			DynamicComponentInitializationException {

		Class<? extends IContext> c = repository.resolveAndGetRegisteredClass(
				IContext.class, contextName, true);
		if (c == null) {
			Pair<String, ComparableVersion> fullClassNameAndVersion = repository
					.getFullClassNameAndVersion(IContext.class, contextName,
							true);
			if (fullClassNameAndVersion.getSecond() != null
					&& repository.hasError(IContext.class,
							fullClassNameAndVersion.getFirst(),
							fullClassNameAndVersion.getSecond().toString()))
				throw new UnknownContextException(contextName,
						repository.getErrors(IContext.class,
								fullClassNameAndVersion.getFirst(),
								fullClassNameAndVersion.getSecond().toString())
								.iterator().next());
			else
				throw new UnknownContextException(contextName,
						"Unknown context");
		}
		return getInstance(repository, contextName, c);
	}

	protected static IContext getInstance(final IRepository repository,
			final String contextName, Class<? extends IContext> c)
			throws UnknownContextException,
			DynamicComponentInitializationException {
		Constructor<? extends IContext> constr;
		try {
			constr = c.getConstructor(IRepository.class, long.class,
					File.class);
			return constr.newInstance(repository, System.currentTimeMillis(),
					new File(contextName));
		} catch (NoSuchMethodException e) {
			throw new DynamicComponentInitializationException(IContext.class,
					contextName,
					"The class is missing the correct constructor");
		} catch (Exception e) {
			// this is likely because we passed faked parameters
			return null;
		}
	}

	/**
	 * @param repository
	 * @param changeDate
	 * @param absPath
	 * @throws RegisterException
	 */
	public Context(IRepository repository, long changeDate, File absPath)
			throws RegisterException {
		super(repository, changeDate, absPath);
	}

	/**
	 * @param other
	 * @throws RegisterException
	 */
	public Context(final Context other) throws RegisterException {
		this(other.repository, other.changeDate, other.absPath);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#equals(java.lang.
	 * Object )
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.context.IContext#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Context))
			return false;

		IContext other = (IContext) obj;
		return this.getName().equals(other.getName());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#hashCode()
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.context.IContext#hashCode()
	 */
	@Override
	public int hashCode() {
		return this.getName().hashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.context.IContext#clone()
	 */
	@Override
	public final IRepositoryObject clone() {
		try {
			return this.getClass().getConstructor(this.getClass())
					.newInstance(this);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		this.log.warn("Cloning instance of class "
				+ this.getClass().getSimpleName() + " failed");
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.context.IContext#getName()
	 */
	@Override
	public abstract String getName();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.context.IContext#getRequiredJavaClassFullNames()
	 */
	@Override
	public abstract Set<String> getRequiredJavaClassFullNames();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.context.IContext#getStandardInputFormat()
	 */
	@Override
	public abstract IDataSetFormat getStandardInputFormat();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.context.IContext#getStandardOutputFormat()
	 */
	@Override
	public abstract IRunResultFormat getStandardOutputFormat();

	@Override
	public ComparableVersion getVersion() {
		try {
			return Repository.getVersionOfObjectDynamicClass(this);
		} catch (Exception e) {
			// cannot happen because we loaded the class before;
			e.printStackTrace();
			return new ComparableVersion("1");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObjectDynamicComponent#
	 * asSerializable()
	 */
	@Override
	public SerializableWrapperDynamicComponent<IContext> asSerializable()
			throws RepositoryObjectSerializationException {
		return (SerializableWrapperDynamicComponent<IContext>) super.asSerializable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.framework.repository.RepositoryObject#asSerializable()
	 */
	@Override
	public SerializableWrapperDynamicComponent<IContext> asSerializableInternal() {
		return new SerializableContext(this);
	}
}
