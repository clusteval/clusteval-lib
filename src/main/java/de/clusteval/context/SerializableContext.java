/**
 * 
 */
package de.clusteval.context;

import java.io.File;

import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.IRepositoryObjectDynamicComponent;
import de.clusteval.framework.repository.RepositoryObjectDeserializationException;
import de.clusteval.framework.repository.SerializableWrapperDynamicComponent;
import de.clusteval.utils.DynamicComponentInitializationException;

public class SerializableContext
		extends
			SerializableWrapperDynamicComponent<IContext>
		implements
			ISerializableContext {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8477048991528805537L;

	/**
	 * @param absPath
	 * @param name
	 * @param version
	 */
	public SerializableContext(File absPath, String name, String version,
			final String alias) {
		super(IContext.class, absPath, name, version, alias);
	}

	/**
	 * @param wrappedComponent
	 */
	public SerializableContext(IContext wrappedComponent) {
		super(IContext.class, wrappedComponent);
	}

	@Override
	protected IContext deserializeInternal(final IRepository repository)
			throws DeserializationException {
		try {
			return Context.parseFromString(repository, this.name);
		} catch (UnknownContextException
				| DynamicComponentInitializationException e) {
			throw new RepositoryObjectDeserializationException(IContext.class,
					this.name, e);
		}
	}
}