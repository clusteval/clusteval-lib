# ClustEval core library

Core Java library containing algorithms and data structures used in ClustEval (server and client).

# Master branch:
[![pipeline status](https://gitlab.compbio.sdu.dk/clusteval/clusteval-lib/badges/master/pipeline.svg)](https://gitlab.compbio.sdu.dk/clusteval/clusteval-lib/commits/master)
[![coverage report](https://gitlab.compbio.sdu.dk/clusteval/clusteval-lib/badges/master/coverage.svg)](https://gitlab.compbio.sdu.dk/clusteval/clusteval-lib/commits/master)

# Release 1.8:
[![pipeline status](https://gitlab.compbio.sdu.dk/clusteval/clusteval-lib/badges/1.8/pipeline.svg)](https://gitlab.compbio.sdu.dk/clusteval/clusteval-lib/commits/1.8)
[![coverage report](https://gitlab.compbio.sdu.dk/clusteval/clusteval-lib/badges/1.8/coverage.svg)](https://gitlab.compbio.sdu.dk/clusteval/clusteval-lib/commits/1.8)
